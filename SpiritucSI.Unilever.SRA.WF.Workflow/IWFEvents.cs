﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities;

namespace SpiritucSI.Unilever.SRA.WF
{
    /// <summary>
    /// Declaração do Interface de Eventos a disponibilizar para o workflow
    /// </summary>
    [ExternalDataExchange]
    public interface IWFEvents
    {
        #region EVENTS              
        event EventHandler<WFEventsArgs> OnSendToConcessionaire;
        void SendToConcessionaire(Guid instanceId);

        event EventHandler<WFEventsArgs> OnPrintSRA;
        void PrintSRA(Guid instanceId);

        event EventHandler<WFEventsArgs> OnRefuseDraft;
        void RefuseDraft(Guid instanceId);

        event EventHandler<WFEventsArgs> OnAcceptRenewal;
        void AcceptRenewal(Guid instanceId);

        event EventHandler<WFEventsArgs> OnRefuseRenewal;
        void RefuseRenewal(Guid instanceId);

        event EventHandler<WFEventsArgs> OnUpdateSystemPrinted;
        void UpdateSystemPrinted(Guid instanceId);

        event EventHandler<WFEventsArgs> OnDelegatedUpdateSystem;
        void DelegatedUpdateSystem(Guid instanceId);

        event EventHandler<WFEventsArgs> OnUpdateSystemReceived;
        void UpdateSystemReceived(Guid instanceId);

        event EventHandler<WFEventsArgs> OnSalesReviewSRA;
        void SalesReviewSRA(Guid instanceId);

        event EventHandler<WFEventsArgs> OnReviewSRA;
        void ReviewSRA(Guid instanceId);

        event EventHandler<WFEventsArgs> OnValidateBySupervisor;
        void ValidateBySupervisor(Guid instanceId);

        event EventHandler<WFEventsArgs> OnRejectedBySupervisor;
        void RejectedBySupervisor(Guid instanceId);

        event EventHandler<WFEventsArgs> OnValidateByManager;
        void ValidateByManager(Guid instanceId);

        event EventHandler<WFEventsArgs> OnRejectedByManager;
        void RejectedByManager(Guid instanceId);






        event EventHandler<WFEventsArgs> OnFinantialApproval;
        void FinantialApproval(Guid instanceId);

        event EventHandler<WFEventsArgs> OnSalesApproval;
        void SalesApproval(Guid instanceId);

        event EventHandler<WFEventsArgs> OnRejectedBySales;
        void RejectedBySales(Guid instanceId);

        event EventHandler<WFEventsArgs> OnRejectedByFinantial;
        void RejectedByFinantial(Guid instanceId);

       





        event EventHandler<WFEventsArgs> OnTopApproval;
        void TopApproval(Guid instanceId);

        event EventHandler<WFEventsArgs> OnApproved;
        void Approved(Guid instanceId);

        event EventHandler<WFEventsArgs> OnRejectedByTop;
        void RejectedByTop(Guid instanceId);

        event EventHandler<WFEventsArgs> OnRejected;
        void Rejected(Guid instanceId);
        #endregion
    }
}