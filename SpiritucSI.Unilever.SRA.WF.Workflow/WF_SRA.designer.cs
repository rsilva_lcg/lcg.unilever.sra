﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace SpiritucSI.Unilever.SRA.WF
{
    partial class WF_SRA
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference1 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference2 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference3 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference4 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference5 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference6 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference7 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference8 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference9 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference10 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference11 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference12 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference13 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference14 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference15 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference16 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference17 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference18 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding1 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding2 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding3 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind4 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding4 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind5 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding5 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind6 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding6 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind7 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding7 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind8 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding8 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind9 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding9 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind10 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding10 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind11 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding11 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind12 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding12 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind13 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding13 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind14 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding14 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind15 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding15 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind16 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding16 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind17 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding17 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind18 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding18 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind19 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding19 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind20 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding20 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind21 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding21 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind22 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding22 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind23 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding23 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind24 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding24 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind25 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding25 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind26 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding26 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind27 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding27 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind28 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding28 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind29 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding29 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind30 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding30 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind31 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding31 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind32 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding32 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind33 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding33 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind34 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding34 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind35 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding35 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind36 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding36 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind37 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding37 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind38 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding38 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind39 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding39 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind40 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding40 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind41 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding41 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind42 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding42 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind43 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding43 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind44 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding44 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind45 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding45 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind46 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding46 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind47 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding47 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind48 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding48 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind49 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding49 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind50 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding50 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind51 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding51 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind52 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding52 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind53 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding53 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind54 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding54 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind55 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding55 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind56 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding56 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind57 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding57 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind58 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding58 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind59 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding59 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind60 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding60 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind61 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding61 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind62 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding62 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind63 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding63 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind64 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding64 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind65 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding65 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind66 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding66 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind67 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding67 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind68 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding68 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind69 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding69 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind70 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding70 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind71 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding71 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind72 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding72 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind73 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding73 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind74 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding74 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind75 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding75 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind76 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding76 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind77 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding77 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind78 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding78 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference19 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference20 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference21 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference22 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference23 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference24 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference25 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference26 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference27 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference28 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference29 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference30 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference31 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference32 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding79 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind79 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding80 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind80 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind81 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind82 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding81 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind83 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding82 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind84 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind85 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind86 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding83 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind87 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding84 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind88 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind89 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind90 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding85 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind91 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding86 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind92 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind93 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding87 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind94 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding88 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind95 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind96 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind97 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding89 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind98 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding90 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind99 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding91 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind100 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding92 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind101 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding93 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind102 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding94 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind103 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding95 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind104 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding96 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind105 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind106 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind107 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding97 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind108 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding98 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind109 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind110 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding99 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind111 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding100 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind112 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind113 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind114 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding101 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind115 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding102 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind116 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind117 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind118 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding103 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind119 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding104 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind120 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding105 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind121 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding106 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind122 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind123 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind124 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding107 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind125 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding108 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.ActivityBind activitybind126 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind127 = new System.Workflow.ComponentModel.ActivityBind();
            this.setStateActivity29 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity27 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity24 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity40 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity39 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity38 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity36 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity35 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity34 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity33 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity32 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity31 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity25 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity21 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity20 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity18 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity15 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity17 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity16 = new System.Workflow.Activities.SetStateActivity();
            this.elseSalesRejected = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifSalesApproved = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity1 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity32 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity31 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity17 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity30 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity29 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity28 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity27 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity26 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity25 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity24 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity23 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity22 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity21 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity18 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity20 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity19 = new System.Workflow.Activities.IfElseBranchActivity();
            this.callExternalMethodActivity12 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity42 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.setStateActivity30 = new System.Workflow.Activities.SetStateActivity();
            this.execSetSalesRejected = new System.Workflow.Activities.CodeActivity();
            this.callExternalMethodActivity35 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity43 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity13 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity40 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.ifElseActivity7 = new System.Workflow.Activities.IfElseActivity();
            this.callExternalMethodActivity36 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity41 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity11 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity44 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.ifElseActivity4 = new System.Workflow.Activities.IfElseActivity();
            this.callExternalMethodActivity34 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity45 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity31 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity5 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity49 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity52 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity54 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity53 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity1 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.ifElseActivity15 = new System.Workflow.Activities.IfElseActivity();
            this.ifElseActivity16 = new System.Workflow.Activities.IfElseActivity();
            this.callExternalMethodActivity16 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity15 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity14 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity38 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.setStateActivity19 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity37 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity39 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity7 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity46 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity10 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity56 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.InApprovalRejected = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity33 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity57 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity32 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity9 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity48 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity47 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.setStateActivity23 = new System.Workflow.Activities.SetStateActivity();
            this.RemoveBudget_1 = new System.Workflow.Activities.CodeActivity();
            this.setStateActivity11 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity8 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity30 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.setStateActivity8 = new System.Workflow.Activities.SetStateActivity();
            this.setStateActivity7 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity4 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity50 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity58 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity3 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity51 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity55 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlerActivity34 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity35 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity11 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity10 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity36 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity31 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity33 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity13 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity12 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity32 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity24 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity22 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity9 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity8 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity23 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity5 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity8 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity9 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity5 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity7 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity6 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity4 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity16 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity3 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity39 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity40 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity37 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity12 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity15 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity14 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity38 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity18 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity19 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity20 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity25 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity7 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity6 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity21 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity4 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity15 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity17 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity16 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlersActivity16 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.elseValidationErrors2 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifValidationOK2 = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity14 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ifElseBranchActivity2 = new System.Workflow.Activities.IfElseBranchActivity();
            this.elseValidationErrors_Accepted = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifValidationOK_Accepted = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity10 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity11 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity13 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity2 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity3 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity1 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlersActivity35 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity25 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity36 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity12 = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity24 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity37 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity28 = new System.Workflow.Activities.SetStateActivity();
            this.ValidateAndAuthorize_4 = new System.Workflow.Activities.CodeActivity();
            this.execSetSalesApproved = new System.Workflow.Activities.CodeActivity();
            this.handleExternalEventActivity22 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity32 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity26 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity34 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity13 = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity19 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity33 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity26 = new System.Workflow.Activities.SetStateActivity();
            this.ValidateAndAuthorize_6 = new System.Workflow.Activities.CodeActivity();
            this.handleExternalEventActivity17 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity24 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity24 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity26 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity11 = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity7 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity25 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity9 = new System.Workflow.Activities.IfElseActivity();
            this.setStateActivity4 = new System.Workflow.Activities.SetStateActivity();
            this.ValidateAndAuthorize_2 = new System.Workflow.Activities.CodeActivity();
            this.handleExternalEventActivity5 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity8 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity19 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity9 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity13 = new System.Workflow.Activities.SetStateActivity();
            this.handleExternalEventActivity20 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity5 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity6 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity7 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity6 = new System.Workflow.Activities.SetStateActivity();
            this.handleExternalEventActivity23 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity6 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity22 = new System.Workflow.Activities.SetStateActivity();
            this.handleExternalEventActivity21 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.callExternalMethodActivity2 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity2 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity6 = new System.Workflow.Activities.IfElseActivity();
            this.getAutoRenewalInfo = new System.Workflow.Activities.CodeActivity();
            this.faultHandlersActivity41 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity37 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity29 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity42 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity5 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity28 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity38 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity27 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity40 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity14 = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity18 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity39 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity14 = new System.Workflow.Activities.SetStateActivity();
            this.handleExternalEventActivity12 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity19 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity21 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity20 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity10 = new System.Workflow.Activities.SetStateActivity();
            this.handleExternalEventActivity8 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity21 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity23 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity23 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity10 = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity14 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity22 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity8 = new System.Workflow.Activities.IfElseActivity();
            this.setStateActivity9 = new System.Workflow.Activities.SetStateActivity();
            this.ValidateAndAuthorize_1 = new System.Workflow.Activities.CodeActivity();
            this.handleExternalEventActivity6 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity15 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity22 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity18 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity12 = new System.Workflow.Activities.SetStateActivity();
            this.RemoveBudget_2 = new System.Workflow.Activities.CodeActivity();
            this.handleExternalEventActivity9 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity17 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity3 = new System.Workflow.Activities.IfElseActivity();
            this.callRuleSetValidation2 = new System.Workflow.Activities.CodeActivity();
            this.handleExternalEventActivity4 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity14 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ifElseActivity5 = new System.Workflow.Activities.IfElseActivity();
            this.ifElseActivity2 = new System.Workflow.Activities.IfElseActivity();
            this.callRuleSetValidation = new System.Workflow.Activities.CodeActivity();
            this.ValidateAndAuthorize_3 = new System.Workflow.Activities.CodeActivity();
            this.Cativate_1 = new System.Workflow.Activities.CodeActivity();
            this.callExternalMethodActivity20 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity10 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity18 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity11 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity3 = new System.Workflow.Activities.SetStateActivity();
            this.handleExternalEventActivity2 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.cancellationHandlerActivity2 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.faultHandlersActivity12 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity41 = new System.Workflow.Activities.SetStateActivity();
            this.OnRefuseDraftActivity = new System.Workflow.Activities.HandleExternalEventActivity();
            this.cancellationHandlerActivity1 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.faultHandlersActivity3 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.callExternalMethodActivity17 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.faultHandlersActivity4 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity1 = new System.Workflow.Activities.SetStateActivity();
            this.handleOnPrintSRA = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity1 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity2 = new System.Workflow.Activities.SetStateActivity();
            this.codeActivity13 = new System.Workflow.Activities.CodeActivity();
            this.handleSendToConcessionaire = new System.Workflow.Activities.HandleExternalEventActivity();
            this.init_SalesApproval = new System.Workflow.Activities.StateInitializationActivity();
            this.onRejectedBySales = new System.Workflow.Activities.EventDrivenActivity();
            this.onSalesApproval = new System.Workflow.Activities.EventDrivenActivity();
            this.init_FinantialApproval = new System.Workflow.Activities.StateInitializationActivity();
            this.onRejectedByFinantial = new System.Workflow.Activities.EventDrivenActivity();
            this.onFinantialApproval = new System.Workflow.Activities.EventDrivenActivity();
            this.init_ManagerApproval = new System.Workflow.Activities.StateInitializationActivity();
            this.onRejectedByManager = new System.Workflow.Activities.EventDrivenActivity();
            this.onValidateByManager = new System.Workflow.Activities.EventDrivenActivity();
            this.init_Delegated = new System.Workflow.Activities.StateInitializationActivity();
            this.onDelegatedUpdateSystem = new System.Workflow.Activities.EventDrivenActivity();
            this.init_RenewalDraft = new System.Workflow.Activities.StateInitializationActivity();
            this.onRefuseRenewal = new System.Workflow.Activities.EventDrivenActivity();
            this.onAcceptRenewal = new System.Workflow.Activities.EventDrivenActivity();
            this.init_CheckIsRenewal = new System.Workflow.Activities.StateInitializationActivity();
            this.init_RejectedState = new System.Workflow.Activities.StateInitializationActivity();
            this.init_ApprovedState = new System.Workflow.Activities.StateInitializationActivity();
            this.init_TopApproval = new System.Workflow.Activities.StateInitializationActivity();
            this.onRejectedByTop = new System.Workflow.Activities.EventDrivenActivity();
            this.onTopApproval = new System.Workflow.Activities.EventDrivenActivity();
            this.init_ErrorVerification = new System.Workflow.Activities.StateInitializationActivity();
            this.onReviewSRA = new System.Workflow.Activities.EventDrivenActivity();
            this.init_InApproval = new System.Workflow.Activities.StateInitializationActivity();
            this.onRejectedBySupervisor = new System.Workflow.Activities.EventDrivenActivity();
            this.onValidateBySupervisor = new System.Workflow.Activities.EventDrivenActivity();
            this.init_Received = new System.Workflow.Activities.StateInitializationActivity();
            this.onSalesReviewSRA = new System.Workflow.Activities.EventDrivenActivity();
            this.onUpdateSystemReceived = new System.Workflow.Activities.EventDrivenActivity();
            this.init_RuleSetValidation = new System.Workflow.Activities.StateInitializationActivity();
            this.init_Printed = new System.Workflow.Activities.StateInitializationActivity();
            this.onUpdateSystemPrinted = new System.Workflow.Activities.EventDrivenActivity();
            this.onRefuseDraft = new System.Workflow.Activities.EventDrivenActivity();
            this.init_Draft = new System.Workflow.Activities.StateInitializationActivity();
            this.onPrintSRA = new System.Workflow.Activities.EventDrivenActivity();
            this.onSendToConcessionaire = new System.Workflow.Activities.EventDrivenActivity();
            this.Completed = new System.Workflow.Activities.StateActivity();
            this.SalesApproval = new System.Workflow.Activities.StateActivity();
            this.FinantialApproval = new System.Workflow.Activities.StateActivity();
            this.ManagerApproval = new System.Workflow.Activities.StateActivity();
            this.Delegated = new System.Workflow.Activities.StateActivity();
            this.RenewalDraft = new System.Workflow.Activities.StateActivity();
            this.Started = new System.Workflow.Activities.StateActivity();
            this.Rejected = new System.Workflow.Activities.StateActivity();
            this.Approved = new System.Workflow.Activities.StateActivity();
            this.TopApproval = new System.Workflow.Activities.StateActivity();
            this.ErrorVerification = new System.Workflow.Activities.StateActivity();
            this.InApproval = new System.Workflow.Activities.StateActivity();
            this.Received = new System.Workflow.Activities.StateActivity();
            this.Accepted = new System.Workflow.Activities.StateActivity();
            this.Printed = new System.Workflow.Activities.StateActivity();
            this.Draft = new System.Workflow.Activities.StateActivity();
            // 
            // setStateActivity29
            // 
            this.setStateActivity29.Name = "setStateActivity29";
            this.setStateActivity29.TargetStateName = "Rejected";
            // 
            // setStateActivity27
            // 
            this.setStateActivity27.Name = "setStateActivity27";
            this.setStateActivity27.TargetStateName = "TopApproval";
            // 
            // setStateActivity24
            // 
            this.setStateActivity24.Name = "setStateActivity24";
            this.setStateActivity24.TargetStateName = "Rejected";
            // 
            // setStateActivity40
            // 
            this.setStateActivity40.Name = "setStateActivity40";
            this.setStateActivity40.TargetStateName = "Draft";
            // 
            // setStateActivity39
            // 
            this.setStateActivity39.Name = "setStateActivity39";
            this.setStateActivity39.TargetStateName = "Approved";
            // 
            // setStateActivity38
            // 
            this.setStateActivity38.Name = "setStateActivity38";
            this.setStateActivity38.TargetStateName = "SalesApproval";
            // 
            // setStateActivity36
            // 
            this.setStateActivity36.Name = "setStateActivity36";
            this.setStateActivity36.TargetStateName = "Rejected";
            // 
            // setStateActivity35
            // 
            this.setStateActivity35.Name = "setStateActivity35";
            this.setStateActivity35.TargetStateName = "Accepted";
            // 
            // setStateActivity34
            // 
            this.setStateActivity34.Name = "setStateActivity34";
            this.setStateActivity34.TargetStateName = "TopApproval";
            // 
            // setStateActivity33
            // 
            this.setStateActivity33.Name = "setStateActivity33";
            this.setStateActivity33.TargetStateName = "FinantialApproval";
            // 
            // setStateActivity32
            // 
            this.setStateActivity32.Name = "setStateActivity32";
            this.setStateActivity32.TargetStateName = "ManagerApproval";
            // 
            // setStateActivity31
            // 
            this.setStateActivity31.Name = "setStateActivity31";
            this.setStateActivity31.TargetStateName = "InApproval";
            // 
            // setStateActivity25
            // 
            this.setStateActivity25.Name = "setStateActivity25";
            this.setStateActivity25.TargetStateName = "ErrorVerification";
            // 
            // setStateActivity21
            // 
            this.setStateActivity21.Name = "setStateActivity21";
            this.setStateActivity21.TargetStateName = "Received";
            // 
            // setStateActivity20
            // 
            this.setStateActivity20.Name = "setStateActivity20";
            this.setStateActivity20.TargetStateName = "Delegated";
            // 
            // setStateActivity18
            // 
            this.setStateActivity18.Name = "setStateActivity18";
            this.setStateActivity18.TargetStateName = "Printed";
            // 
            // setStateActivity15
            // 
            this.setStateActivity15.Name = "setStateActivity15";
            this.setStateActivity15.TargetStateName = "RenewalDraft";
            // 
            // setStateActivity17
            // 
            this.setStateActivity17.Name = "setStateActivity17";
            this.setStateActivity17.TargetStateName = "RenewalDraft";
            // 
            // setStateActivity16
            // 
            this.setStateActivity16.Name = "setStateActivity16";
            this.setStateActivity16.TargetStateName = "Draft";
            // 
            // elseSalesRejected
            // 
            this.elseSalesRejected.Activities.Add(this.setStateActivity29);
            this.elseSalesRejected.Name = "elseSalesRejected";
            // 
            // ifSalesApproved
            // 
            this.ifSalesApproved.Activities.Add(this.setStateActivity27);
            ruleconditionreference1.ConditionName = "FinantialApproved2";
            this.ifSalesApproved.Condition = ruleconditionreference1;
            this.ifSalesApproved.Name = "ifSalesApproved";
            // 
            // ifElseBranchActivity1
            // 
            this.ifElseBranchActivity1.Activities.Add(this.setStateActivity24);
            ruleconditionreference2.ConditionName = "True";
            this.ifElseBranchActivity1.Condition = ruleconditionreference2;
            this.ifElseBranchActivity1.Name = "ifElseBranchActivity1";
            // 
            // ifElseBranchActivity32
            // 
            this.ifElseBranchActivity32.Activities.Add(this.setStateActivity40);
            ruleconditionreference3.ConditionName = "StateDraft";
            this.ifElseBranchActivity32.Condition = ruleconditionreference3;
            this.ifElseBranchActivity32.Name = "ifElseBranchActivity32";
            // 
            // ifElseBranchActivity31
            // 
            this.ifElseBranchActivity31.Activities.Add(this.setStateActivity39);
            ruleconditionreference4.ConditionName = "StateApproved";
            this.ifElseBranchActivity31.Condition = ruleconditionreference4;
            this.ifElseBranchActivity31.Name = "ifElseBranchActivity31";
            // 
            // ifElseBranchActivity17
            // 
            this.ifElseBranchActivity17.Activities.Add(this.setStateActivity38);
            ruleconditionreference5.ConditionName = "StateSalesApproval";
            this.ifElseBranchActivity17.Condition = ruleconditionreference5;
            this.ifElseBranchActivity17.Name = "ifElseBranchActivity17";
            // 
            // ifElseBranchActivity30
            // 
            this.ifElseBranchActivity30.Activities.Add(this.setStateActivity36);
            ruleconditionreference6.ConditionName = "StateRejected";
            this.ifElseBranchActivity30.Condition = ruleconditionreference6;
            this.ifElseBranchActivity30.Name = "ifElseBranchActivity30";
            // 
            // ifElseBranchActivity29
            // 
            this.ifElseBranchActivity29.Activities.Add(this.setStateActivity35);
            ruleconditionreference7.ConditionName = "StateAccepted";
            this.ifElseBranchActivity29.Condition = ruleconditionreference7;
            this.ifElseBranchActivity29.Name = "ifElseBranchActivity29";
            // 
            // ifElseBranchActivity28
            // 
            this.ifElseBranchActivity28.Activities.Add(this.setStateActivity34);
            ruleconditionreference8.ConditionName = "StateTopApproval";
            this.ifElseBranchActivity28.Condition = ruleconditionreference8;
            this.ifElseBranchActivity28.Name = "ifElseBranchActivity28";
            // 
            // ifElseBranchActivity27
            // 
            this.ifElseBranchActivity27.Activities.Add(this.setStateActivity33);
            ruleconditionreference9.ConditionName = "StateFinantialApproval";
            this.ifElseBranchActivity27.Condition = ruleconditionreference9;
            this.ifElseBranchActivity27.Name = "ifElseBranchActivity27";
            // 
            // ifElseBranchActivity26
            // 
            this.ifElseBranchActivity26.Activities.Add(this.setStateActivity32);
            ruleconditionreference10.ConditionName = "StateManagerApproval";
            this.ifElseBranchActivity26.Condition = ruleconditionreference10;
            this.ifElseBranchActivity26.Name = "ifElseBranchActivity26";
            // 
            // ifElseBranchActivity25
            // 
            this.ifElseBranchActivity25.Activities.Add(this.setStateActivity31);
            ruleconditionreference11.ConditionName = "StateInApproval";
            this.ifElseBranchActivity25.Condition = ruleconditionreference11;
            this.ifElseBranchActivity25.Name = "ifElseBranchActivity25";
            // 
            // ifElseBranchActivity24
            // 
            this.ifElseBranchActivity24.Activities.Add(this.setStateActivity25);
            ruleconditionreference12.ConditionName = "StateErrorVerification";
            this.ifElseBranchActivity24.Condition = ruleconditionreference12;
            this.ifElseBranchActivity24.Name = "ifElseBranchActivity24";
            // 
            // ifElseBranchActivity23
            // 
            this.ifElseBranchActivity23.Activities.Add(this.setStateActivity21);
            ruleconditionreference13.ConditionName = "StateReceived";
            this.ifElseBranchActivity23.Condition = ruleconditionreference13;
            this.ifElseBranchActivity23.Name = "ifElseBranchActivity23";
            // 
            // ifElseBranchActivity22
            // 
            this.ifElseBranchActivity22.Activities.Add(this.setStateActivity20);
            ruleconditionreference14.ConditionName = "StateDelegated";
            this.ifElseBranchActivity22.Condition = ruleconditionreference14;
            this.ifElseBranchActivity22.Name = "ifElseBranchActivity22";
            // 
            // ifElseBranchActivity21
            // 
            this.ifElseBranchActivity21.Activities.Add(this.setStateActivity18);
            ruleconditionreference15.ConditionName = "StatePrinted";
            this.ifElseBranchActivity21.Condition = ruleconditionreference15;
            this.ifElseBranchActivity21.Name = "ifElseBranchActivity21";
            // 
            // ifElseBranchActivity18
            // 
            this.ifElseBranchActivity18.Activities.Add(this.setStateActivity15);
            ruleconditionreference16.ConditionName = "StateRenewalDraft";
            this.ifElseBranchActivity18.Condition = ruleconditionreference16;
            this.ifElseBranchActivity18.Name = "ifElseBranchActivity18";
            // 
            // ifElseBranchActivity20
            // 
            this.ifElseBranchActivity20.Activities.Add(this.setStateActivity17);
            ruleconditionreference17.ConditionName = "NotRenewal";
            this.ifElseBranchActivity20.Condition = ruleconditionreference17;
            this.ifElseBranchActivity20.Name = "ifElseBranchActivity20";
            // 
            // ifElseBranchActivity19
            // 
            this.ifElseBranchActivity19.Activities.Add(this.setStateActivity16);
            ruleconditionreference18.ConditionName = "IsAutoRenewal";
            this.ifElseBranchActivity19.Condition = ruleconditionreference18;
            this.ifElseBranchActivity19.Name = "ifElseBranchActivity19";
            // 
            // callExternalMethodActivity12
            // 
            this.callExternalMethodActivity12.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity12.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity12.Name = "callExternalMethodActivity12";
            activitybind1.Name = "WF_SRA";
            activitybind1.Path = "WFinstanceId";
            workflowparameterbinding1.ParameterName = "InstanceID";
            workflowparameterbinding1.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            activitybind2.Name = "WF_SRA";
            activitybind2.Path = "WFFault";
            workflowparameterbinding2.ParameterName = "e";
            workflowparameterbinding2.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            this.callExternalMethodActivity12.ParameterBindings.Add(workflowparameterbinding1);
            this.callExternalMethodActivity12.ParameterBindings.Add(workflowparameterbinding2);
            this.callExternalMethodActivity12.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity42
            // 
            this.callExternalMethodActivity42.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity42.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity42.Name = "callExternalMethodActivity42";
            activitybind3.Name = "WF_SRA";
            activitybind3.Path = "WFinstanceId";
            workflowparameterbinding3.ParameterName = "InstanceID";
            workflowparameterbinding3.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            activitybind4.Name = "WF_SRA";
            activitybind4.Path = "WFFault";
            workflowparameterbinding4.ParameterName = "e";
            workflowparameterbinding4.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind4)));
            this.callExternalMethodActivity42.ParameterBindings.Add(workflowparameterbinding3);
            this.callExternalMethodActivity42.ParameterBindings.Add(workflowparameterbinding4);
            this.callExternalMethodActivity42.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // setStateActivity30
            // 
            this.setStateActivity30.Name = "setStateActivity30";
            this.setStateActivity30.TargetStateName = "FinantialApproval";
            // 
            // execSetSalesRejected
            // 
            this.execSetSalesRejected.Name = "execSetSalesRejected";
            this.execSetSalesRejected.ExecuteCode += new System.EventHandler(this.SetSalesRejected);
            // 
            // callExternalMethodActivity35
            // 
            this.callExternalMethodActivity35.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity35.MethodName = "SendBlockException";
            this.callExternalMethodActivity35.Name = "callExternalMethodActivity35";
            activitybind5.Name = "WF_SRA";
            activitybind5.Path = "WFinstanceId";
            workflowparameterbinding5.ParameterName = "InstanceID";
            workflowparameterbinding5.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind5)));
            this.callExternalMethodActivity35.ParameterBindings.Add(workflowparameterbinding5);
            // 
            // callExternalMethodActivity43
            // 
            this.callExternalMethodActivity43.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity43.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity43.Name = "callExternalMethodActivity43";
            activitybind6.Name = "WF_SRA";
            activitybind6.Path = "WFinstanceId";
            workflowparameterbinding6.ParameterName = "InstanceID";
            workflowparameterbinding6.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind6)));
            activitybind7.Name = "WF_SRA";
            activitybind7.Path = "WFFault";
            workflowparameterbinding7.ParameterName = "e";
            workflowparameterbinding7.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind7)));
            this.callExternalMethodActivity43.ParameterBindings.Add(workflowparameterbinding6);
            this.callExternalMethodActivity43.ParameterBindings.Add(workflowparameterbinding7);
            this.callExternalMethodActivity43.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity13
            // 
            this.callExternalMethodActivity13.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity13.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity13.Name = "callExternalMethodActivity13";
            activitybind8.Name = "WF_SRA";
            activitybind8.Path = "WFinstanceId";
            workflowparameterbinding8.ParameterName = "InstanceID";
            workflowparameterbinding8.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind8)));
            activitybind9.Name = "WF_SRA";
            activitybind9.Path = "WFFault";
            workflowparameterbinding9.ParameterName = "e";
            workflowparameterbinding9.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind9)));
            this.callExternalMethodActivity13.ParameterBindings.Add(workflowparameterbinding8);
            this.callExternalMethodActivity13.ParameterBindings.Add(workflowparameterbinding9);
            this.callExternalMethodActivity13.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity40
            // 
            this.callExternalMethodActivity40.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity40.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity40.Name = "callExternalMethodActivity40";
            activitybind10.Name = "WF_SRA";
            activitybind10.Path = "WFinstanceId";
            workflowparameterbinding10.ParameterName = "InstanceID";
            workflowparameterbinding10.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind10)));
            activitybind11.Name = "WF_SRA";
            activitybind11.Path = "WFFault";
            workflowparameterbinding11.ParameterName = "e";
            workflowparameterbinding11.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind11)));
            this.callExternalMethodActivity40.ParameterBindings.Add(workflowparameterbinding10);
            this.callExternalMethodActivity40.ParameterBindings.Add(workflowparameterbinding11);
            this.callExternalMethodActivity40.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // ifElseActivity7
            // 
            this.ifElseActivity7.Activities.Add(this.ifSalesApproved);
            this.ifElseActivity7.Activities.Add(this.elseSalesRejected);
            this.ifElseActivity7.Name = "ifElseActivity7";
            // 
            // callExternalMethodActivity36
            // 
            this.callExternalMethodActivity36.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity36.MethodName = "SendBlockException";
            this.callExternalMethodActivity36.Name = "callExternalMethodActivity36";
            activitybind12.Name = "WF_SRA";
            activitybind12.Path = "WFinstanceId";
            workflowparameterbinding12.ParameterName = "InstanceID";
            workflowparameterbinding12.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind12)));
            this.callExternalMethodActivity36.ParameterBindings.Add(workflowparameterbinding12);
            // 
            // callExternalMethodActivity41
            // 
            this.callExternalMethodActivity41.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity41.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity41.Name = "callExternalMethodActivity41";
            activitybind13.Name = "WF_SRA";
            activitybind13.Path = "WFinstanceId";
            workflowparameterbinding13.ParameterName = "InstanceID";
            workflowparameterbinding13.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind13)));
            activitybind14.Name = "WF_SRA";
            activitybind14.Path = "WFFault";
            workflowparameterbinding14.ParameterName = "e";
            workflowparameterbinding14.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind14)));
            this.callExternalMethodActivity41.ParameterBindings.Add(workflowparameterbinding13);
            this.callExternalMethodActivity41.ParameterBindings.Add(workflowparameterbinding14);
            this.callExternalMethodActivity41.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity11
            // 
            this.callExternalMethodActivity11.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity11.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity11.Name = "callExternalMethodActivity11";
            activitybind15.Name = "WF_SRA";
            activitybind15.Path = "WFinstanceId";
            workflowparameterbinding15.ParameterName = "InstanceID";
            workflowparameterbinding15.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind15)));
            activitybind16.Name = "WF_SRA";
            activitybind16.Path = "WFFault";
            workflowparameterbinding16.ParameterName = "e";
            workflowparameterbinding16.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind16)));
            this.callExternalMethodActivity11.ParameterBindings.Add(workflowparameterbinding15);
            this.callExternalMethodActivity11.ParameterBindings.Add(workflowparameterbinding16);
            this.callExternalMethodActivity11.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity44
            // 
            this.callExternalMethodActivity44.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity44.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity44.Name = "callExternalMethodActivity44";
            activitybind17.Name = "WF_SRA";
            activitybind17.Path = "WFinstanceId";
            workflowparameterbinding17.ParameterName = "InstanceID";
            workflowparameterbinding17.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind17)));
            activitybind18.Name = "WF_SRA";
            activitybind18.Path = "WFFault";
            workflowparameterbinding18.ParameterName = "e";
            workflowparameterbinding18.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind18)));
            this.callExternalMethodActivity44.ParameterBindings.Add(workflowparameterbinding17);
            this.callExternalMethodActivity44.ParameterBindings.Add(workflowparameterbinding18);
            this.callExternalMethodActivity44.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // ifElseActivity4
            // 
            this.ifElseActivity4.Activities.Add(this.ifElseBranchActivity1);
            this.ifElseActivity4.Name = "ifElseActivity4";
            // 
            // callExternalMethodActivity34
            // 
            this.callExternalMethodActivity34.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity34.MethodName = "SendBlockException";
            this.callExternalMethodActivity34.Name = "callExternalMethodActivity34";
            activitybind19.Name = "WF_SRA";
            activitybind19.Path = "WFinstanceId";
            workflowparameterbinding19.ParameterName = "InstanceID";
            workflowparameterbinding19.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind19)));
            this.callExternalMethodActivity34.ParameterBindings.Add(workflowparameterbinding19);
            // 
            // callExternalMethodActivity45
            // 
            this.callExternalMethodActivity45.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity45.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity45.Name = "callExternalMethodActivity45";
            activitybind20.Name = "WF_SRA";
            activitybind20.Path = "WFinstanceId";
            workflowparameterbinding20.ParameterName = "InstanceID";
            workflowparameterbinding20.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind20)));
            activitybind21.Name = "WF_SRA";
            activitybind21.Path = "WFFault";
            workflowparameterbinding21.ParameterName = "e";
            workflowparameterbinding21.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind21)));
            this.callExternalMethodActivity45.ParameterBindings.Add(workflowparameterbinding20);
            this.callExternalMethodActivity45.ParameterBindings.Add(workflowparameterbinding21);
            this.callExternalMethodActivity45.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity31
            // 
            this.callExternalMethodActivity31.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity31.MethodName = "SendProceedException";
            this.callExternalMethodActivity31.Name = "callExternalMethodActivity31";
            activitybind22.Name = "WF_SRA";
            activitybind22.Path = "WFinstanceId";
            workflowparameterbinding22.ParameterName = "InstanceID";
            workflowparameterbinding22.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind22)));
            this.callExternalMethodActivity31.ParameterBindings.Add(workflowparameterbinding22);
            // 
            // callExternalMethodActivity5
            // 
            this.callExternalMethodActivity5.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity5.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity5.Name = "callExternalMethodActivity5";
            activitybind23.Name = "WF_SRA";
            activitybind23.Path = "WFinstanceId";
            workflowparameterbinding23.ParameterName = "InstanceID";
            workflowparameterbinding23.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind23)));
            activitybind24.Name = "WF_SRA";
            activitybind24.Path = "WFFault";
            workflowparameterbinding24.ParameterName = "e";
            workflowparameterbinding24.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind24)));
            this.callExternalMethodActivity5.ParameterBindings.Add(workflowparameterbinding23);
            this.callExternalMethodActivity5.ParameterBindings.Add(workflowparameterbinding24);
            this.callExternalMethodActivity5.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity49
            // 
            this.callExternalMethodActivity49.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity49.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity49.Name = "callExternalMethodActivity49";
            activitybind25.Name = "WF_SRA";
            activitybind25.Path = "WFinstanceId";
            workflowparameterbinding25.ParameterName = "InstanceID";
            workflowparameterbinding25.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind25)));
            activitybind26.Name = "WF_SRA";
            activitybind26.Path = "WFFault";
            workflowparameterbinding26.ParameterName = "e";
            workflowparameterbinding26.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind26)));
            this.callExternalMethodActivity49.ParameterBindings.Add(workflowparameterbinding25);
            this.callExternalMethodActivity49.ParameterBindings.Add(workflowparameterbinding26);
            this.callExternalMethodActivity49.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity52
            // 
            this.callExternalMethodActivity52.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity52.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity52.Name = "callExternalMethodActivity52";
            activitybind27.Name = "WF_SRA";
            activitybind27.Path = "WFinstanceId";
            workflowparameterbinding27.ParameterName = "InstanceID";
            workflowparameterbinding27.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind27)));
            activitybind28.Name = "WF_SRA";
            activitybind28.Path = "WFFault";
            workflowparameterbinding28.ParameterName = "e";
            workflowparameterbinding28.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind28)));
            this.callExternalMethodActivity52.ParameterBindings.Add(workflowparameterbinding27);
            this.callExternalMethodActivity52.ParameterBindings.Add(workflowparameterbinding28);
            this.callExternalMethodActivity52.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity54
            // 
            this.callExternalMethodActivity54.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity54.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity54.Name = "callExternalMethodActivity54";
            activitybind29.Name = "WF_SRA";
            activitybind29.Path = "WFinstanceId";
            workflowparameterbinding29.ParameterName = "InstanceID";
            workflowparameterbinding29.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind29)));
            activitybind30.Name = "WF_SRA";
            activitybind30.Path = "WFFault";
            workflowparameterbinding30.ParameterName = "e";
            workflowparameterbinding30.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind30)));
            this.callExternalMethodActivity54.ParameterBindings.Add(workflowparameterbinding29);
            this.callExternalMethodActivity54.ParameterBindings.Add(workflowparameterbinding30);
            this.callExternalMethodActivity54.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity53
            // 
            this.callExternalMethodActivity53.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity53.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity53.Name = "callExternalMethodActivity53";
            activitybind31.Name = "WF_SRA";
            activitybind31.Path = "WFinstanceId";
            workflowparameterbinding31.ParameterName = "InstanceID";
            workflowparameterbinding31.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind31)));
            activitybind32.Name = "WF_SRA";
            activitybind32.Path = "WFFault";
            workflowparameterbinding32.ParameterName = "e";
            workflowparameterbinding32.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind32)));
            this.callExternalMethodActivity53.ParameterBindings.Add(workflowparameterbinding31);
            this.callExternalMethodActivity53.ParameterBindings.Add(workflowparameterbinding32);
            this.callExternalMethodActivity53.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity1
            // 
            this.callExternalMethodActivity1.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity1.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity1.Name = "callExternalMethodActivity1";
            activitybind33.Name = "WF_SRA";
            activitybind33.Path = "WFinstanceId";
            workflowparameterbinding33.ParameterName = "InstanceID";
            workflowparameterbinding33.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind33)));
            activitybind34.Name = "WF_SRA";
            activitybind34.Path = "WFFault";
            workflowparameterbinding34.ParameterName = "e";
            workflowparameterbinding34.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind34)));
            this.callExternalMethodActivity1.ParameterBindings.Add(workflowparameterbinding33);
            this.callExternalMethodActivity1.ParameterBindings.Add(workflowparameterbinding34);
            this.callExternalMethodActivity1.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // ifElseActivity15
            // 
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity18);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity21);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity22);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity23);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity24);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity25);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity26);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity27);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity28);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity29);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity30);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity17);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity31);
            this.ifElseActivity15.Activities.Add(this.ifElseBranchActivity32);
            this.ifElseActivity15.Name = "ifElseActivity15";
            // 
            // ifElseActivity16
            // 
            this.ifElseActivity16.Activities.Add(this.ifElseBranchActivity19);
            this.ifElseActivity16.Activities.Add(this.ifElseBranchActivity20);
            this.ifElseActivity16.Name = "ifElseActivity16";
            // 
            // callExternalMethodActivity16
            // 
            this.callExternalMethodActivity16.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity16.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity16.Name = "callExternalMethodActivity16";
            activitybind35.Name = "WF_SRA";
            activitybind35.Path = "WFinstanceId";
            workflowparameterbinding35.ParameterName = "InstanceID";
            workflowparameterbinding35.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind35)));
            activitybind36.Name = "WF_SRA";
            activitybind36.Path = "WFFault";
            workflowparameterbinding36.ParameterName = "e";
            workflowparameterbinding36.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind36)));
            this.callExternalMethodActivity16.ParameterBindings.Add(workflowparameterbinding35);
            this.callExternalMethodActivity16.ParameterBindings.Add(workflowparameterbinding36);
            this.callExternalMethodActivity16.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity15
            // 
            this.callExternalMethodActivity15.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity15.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity15.Name = "callExternalMethodActivity15";
            activitybind37.Name = "WF_SRA";
            activitybind37.Path = "WFinstanceId";
            workflowparameterbinding37.ParameterName = "InstanceID";
            workflowparameterbinding37.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind37)));
            activitybind38.Name = "WF_SRA";
            activitybind38.Path = "WFFault";
            workflowparameterbinding38.ParameterName = "e";
            workflowparameterbinding38.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind38)));
            this.callExternalMethodActivity15.ParameterBindings.Add(workflowparameterbinding37);
            this.callExternalMethodActivity15.ParameterBindings.Add(workflowparameterbinding38);
            this.callExternalMethodActivity15.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity14
            // 
            this.callExternalMethodActivity14.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity14.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity14.Name = "callExternalMethodActivity14";
            activitybind39.Name = "WF_SRA";
            activitybind39.Path = "WFinstanceId";
            workflowparameterbinding39.ParameterName = "InstanceID";
            workflowparameterbinding39.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind39)));
            activitybind40.Name = "WF_SRA";
            activitybind40.Path = "WFFault";
            workflowparameterbinding40.ParameterName = "e";
            workflowparameterbinding40.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind40)));
            this.callExternalMethodActivity14.ParameterBindings.Add(workflowparameterbinding39);
            this.callExternalMethodActivity14.ParameterBindings.Add(workflowparameterbinding40);
            this.callExternalMethodActivity14.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity38
            // 
            this.callExternalMethodActivity38.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity38.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity38.Name = "callExternalMethodActivity38";
            activitybind41.Name = "WF_SRA";
            activitybind41.Path = "WFinstanceId";
            workflowparameterbinding41.ParameterName = "InstanceID";
            workflowparameterbinding41.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind41)));
            activitybind42.Name = "WF_SRA";
            activitybind42.Path = "WFFault";
            workflowparameterbinding42.ParameterName = "e";
            workflowparameterbinding42.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind42)));
            this.callExternalMethodActivity38.ParameterBindings.Add(workflowparameterbinding41);
            this.callExternalMethodActivity38.ParameterBindings.Add(workflowparameterbinding42);
            this.callExternalMethodActivity38.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // setStateActivity19
            // 
            this.setStateActivity19.Name = "setStateActivity19";
            this.setStateActivity19.TargetStateName = "Rejected";
            // 
            // callExternalMethodActivity37
            // 
            this.callExternalMethodActivity37.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity37.MethodName = "SendBlockException";
            this.callExternalMethodActivity37.Name = "callExternalMethodActivity37";
            activitybind43.Name = "WF_SRA";
            activitybind43.Path = "WFinstanceId";
            workflowparameterbinding43.ParameterName = "InstanceID";
            workflowparameterbinding43.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind43)));
            this.callExternalMethodActivity37.ParameterBindings.Add(workflowparameterbinding43);
            // 
            // callExternalMethodActivity39
            // 
            this.callExternalMethodActivity39.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity39.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity39.Name = "callExternalMethodActivity39";
            activitybind44.Name = "WF_SRA";
            activitybind44.Path = "WFinstanceId";
            workflowparameterbinding44.ParameterName = "InstanceID";
            workflowparameterbinding44.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind44)));
            activitybind45.Name = "WF_SRA";
            activitybind45.Path = "WFFault";
            workflowparameterbinding45.ParameterName = "e";
            workflowparameterbinding45.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind45)));
            this.callExternalMethodActivity39.ParameterBindings.Add(workflowparameterbinding44);
            this.callExternalMethodActivity39.ParameterBindings.Add(workflowparameterbinding45);
            this.callExternalMethodActivity39.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity7
            // 
            this.callExternalMethodActivity7.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity7.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity7.Name = "callExternalMethodActivity7";
            activitybind46.Name = "WF_SRA";
            activitybind46.Path = "WFinstanceId";
            workflowparameterbinding46.ParameterName = "InstanceID";
            workflowparameterbinding46.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind46)));
            activitybind47.Name = "WF_SRA";
            activitybind47.Path = "WFFault";
            workflowparameterbinding47.ParameterName = "e";
            workflowparameterbinding47.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind47)));
            this.callExternalMethodActivity7.ParameterBindings.Add(workflowparameterbinding46);
            this.callExternalMethodActivity7.ParameterBindings.Add(workflowparameterbinding47);
            this.callExternalMethodActivity7.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity46
            // 
            this.callExternalMethodActivity46.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity46.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity46.Name = "callExternalMethodActivity46";
            activitybind48.Name = "WF_SRA";
            activitybind48.Path = "WFinstanceId";
            workflowparameterbinding48.ParameterName = "InstanceID";
            workflowparameterbinding48.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind48)));
            activitybind49.Name = "WF_SRA";
            activitybind49.Path = "WFFault";
            workflowparameterbinding49.ParameterName = "e";
            workflowparameterbinding49.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind49)));
            this.callExternalMethodActivity46.ParameterBindings.Add(workflowparameterbinding48);
            this.callExternalMethodActivity46.ParameterBindings.Add(workflowparameterbinding49);
            this.callExternalMethodActivity46.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity10
            // 
            this.callExternalMethodActivity10.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity10.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity10.Name = "callExternalMethodActivity10";
            activitybind50.Name = "WF_SRA";
            activitybind50.Path = "WFinstanceId";
            workflowparameterbinding50.ParameterName = "InstanceID";
            workflowparameterbinding50.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind50)));
            activitybind51.Name = "WF_SRA";
            activitybind51.Path = "WFFault";
            workflowparameterbinding51.ParameterName = "e";
            workflowparameterbinding51.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind51)));
            this.callExternalMethodActivity10.ParameterBindings.Add(workflowparameterbinding50);
            this.callExternalMethodActivity10.ParameterBindings.Add(workflowparameterbinding51);
            this.callExternalMethodActivity10.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity56
            // 
            this.callExternalMethodActivity56.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity56.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity56.Name = "callExternalMethodActivity56";
            activitybind52.Name = "WF_SRA";
            activitybind52.Path = "WFinstanceId";
            workflowparameterbinding52.ParameterName = "InstanceID";
            workflowparameterbinding52.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind52)));
            activitybind53.Name = "WF_SRA";
            activitybind53.Path = "WFFault";
            workflowparameterbinding53.ParameterName = "e";
            workflowparameterbinding53.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind53)));
            this.callExternalMethodActivity56.ParameterBindings.Add(workflowparameterbinding52);
            this.callExternalMethodActivity56.ParameterBindings.Add(workflowparameterbinding53);
            this.callExternalMethodActivity56.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // InApprovalRejected
            // 
            this.InApprovalRejected.Name = "InApprovalRejected";
            this.InApprovalRejected.TargetStateName = "Rejected";
            // 
            // callExternalMethodActivity33
            // 
            this.callExternalMethodActivity33.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity33.MethodName = "SendBlockException";
            this.callExternalMethodActivity33.Name = "callExternalMethodActivity33";
            activitybind54.Name = "WF_SRA";
            activitybind54.Path = "WFinstanceId";
            workflowparameterbinding54.ParameterName = "InstanceID";
            workflowparameterbinding54.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind54)));
            this.callExternalMethodActivity33.ParameterBindings.Add(workflowparameterbinding54);
            // 
            // callExternalMethodActivity57
            // 
            this.callExternalMethodActivity57.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity57.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity57.Name = "callExternalMethodActivity57";
            activitybind55.Name = "WF_SRA";
            activitybind55.Path = "WFinstanceId";
            workflowparameterbinding55.ParameterName = "InstanceID";
            workflowparameterbinding55.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind55)));
            activitybind56.Name = "WF_SRA";
            activitybind56.Path = "WFFault";
            workflowparameterbinding56.ParameterName = "e";
            workflowparameterbinding56.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind56)));
            this.callExternalMethodActivity57.ParameterBindings.Add(workflowparameterbinding55);
            this.callExternalMethodActivity57.ParameterBindings.Add(workflowparameterbinding56);
            this.callExternalMethodActivity57.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity32
            // 
            this.callExternalMethodActivity32.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity32.MethodName = "SendProceedException";
            this.callExternalMethodActivity32.Name = "callExternalMethodActivity32";
            activitybind57.Name = "WF_SRA";
            activitybind57.Path = "WFinstanceId";
            workflowparameterbinding57.ParameterName = "InstanceID";
            workflowparameterbinding57.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind57)));
            this.callExternalMethodActivity32.ParameterBindings.Add(workflowparameterbinding57);
            // 
            // callExternalMethodActivity9
            // 
            this.callExternalMethodActivity9.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity9.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity9.Name = "callExternalMethodActivity9";
            activitybind58.Name = "WF_SRA";
            activitybind58.Path = "WFinstanceId";
            workflowparameterbinding58.ParameterName = "InstanceID";
            workflowparameterbinding58.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind58)));
            activitybind59.Name = "WF_SRA";
            activitybind59.Path = "WFFault";
            workflowparameterbinding59.ParameterName = "e";
            workflowparameterbinding59.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind59)));
            this.callExternalMethodActivity9.ParameterBindings.Add(workflowparameterbinding58);
            this.callExternalMethodActivity9.ParameterBindings.Add(workflowparameterbinding59);
            this.callExternalMethodActivity9.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity48
            // 
            this.callExternalMethodActivity48.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity48.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity48.Name = "callExternalMethodActivity48";
            activitybind60.Name = "WF_SRA";
            activitybind60.Path = "WFinstanceId";
            workflowparameterbinding60.ParameterName = "InstanceID";
            workflowparameterbinding60.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind60)));
            activitybind61.Name = "WF_SRA";
            activitybind61.Path = "WFFault";
            workflowparameterbinding61.ParameterName = "e";
            workflowparameterbinding61.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind61)));
            this.callExternalMethodActivity48.ParameterBindings.Add(workflowparameterbinding60);
            this.callExternalMethodActivity48.ParameterBindings.Add(workflowparameterbinding61);
            this.callExternalMethodActivity48.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity47
            // 
            this.callExternalMethodActivity47.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity47.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity47.Name = "callExternalMethodActivity47";
            activitybind62.Name = "WF_SRA";
            activitybind62.Path = "WFinstanceId";
            workflowparameterbinding62.ParameterName = "InstanceID";
            workflowparameterbinding62.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind62)));
            activitybind63.Name = "WF_SRA";
            activitybind63.Path = "WFFault";
            workflowparameterbinding63.ParameterName = "e";
            workflowparameterbinding63.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind63)));
            this.callExternalMethodActivity47.ParameterBindings.Add(workflowparameterbinding62);
            this.callExternalMethodActivity47.ParameterBindings.Add(workflowparameterbinding63);
            this.callExternalMethodActivity47.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // setStateActivity23
            // 
            this.setStateActivity23.Name = "setStateActivity23";
            this.setStateActivity23.TargetStateName = "ErrorVerification";
            // 
            // RemoveBudget_1
            // 
            this.RemoveBudget_1.Name = "RemoveBudget_1";
            this.RemoveBudget_1.ExecuteCode += new System.EventHandler(this.RemoveBudget);
            // 
            // setStateActivity11
            // 
            this.setStateActivity11.Name = "setStateActivity11";
            this.setStateActivity11.TargetStateName = "InApproval";
            // 
            // callExternalMethodActivity8
            // 
            this.callExternalMethodActivity8.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity8.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity8.Name = "callExternalMethodActivity8";
            activitybind64.Name = "WF_SRA";
            activitybind64.Path = "WFinstanceId";
            workflowparameterbinding64.ParameterName = "InstanceID";
            workflowparameterbinding64.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind64)));
            activitybind65.Name = "WF_SRA";
            activitybind65.Path = "WFFault";
            workflowparameterbinding65.ParameterName = "e";
            workflowparameterbinding65.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind65)));
            this.callExternalMethodActivity8.ParameterBindings.Add(workflowparameterbinding64);
            this.callExternalMethodActivity8.ParameterBindings.Add(workflowparameterbinding65);
            this.callExternalMethodActivity8.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity30
            // 
            this.callExternalMethodActivity30.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity30.MethodName = "SendProceedException";
            this.callExternalMethodActivity30.Name = "callExternalMethodActivity30";
            activitybind66.Name = "WF_SRA";
            activitybind66.Path = "WFinstanceId";
            workflowparameterbinding66.ParameterName = "InstanceID";
            workflowparameterbinding66.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind66)));
            this.callExternalMethodActivity30.ParameterBindings.Add(workflowparameterbinding66);
            // 
            // setStateActivity8
            // 
            this.setStateActivity8.Name = "setStateActivity8";
            this.setStateActivity8.TargetStateName = "Received";
            // 
            // setStateActivity7
            // 
            this.setStateActivity7.Name = "setStateActivity7";
            this.setStateActivity7.TargetStateName = "InApproval";
            // 
            // callExternalMethodActivity4
            // 
            this.callExternalMethodActivity4.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity4.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity4.Name = "callExternalMethodActivity4";
            activitybind67.Name = "WF_SRA";
            activitybind67.Path = "WFinstanceId";
            workflowparameterbinding67.ParameterName = "InstanceID";
            workflowparameterbinding67.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind67)));
            activitybind68.Name = "WF_SRA";
            activitybind68.Path = "WFFault";
            workflowparameterbinding68.ParameterName = "e";
            workflowparameterbinding68.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind68)));
            this.callExternalMethodActivity4.ParameterBindings.Add(workflowparameterbinding67);
            this.callExternalMethodActivity4.ParameterBindings.Add(workflowparameterbinding68);
            this.callExternalMethodActivity4.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity50
            // 
            this.callExternalMethodActivity50.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity50.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity50.Name = "callExternalMethodActivity50";
            activitybind69.Name = "WF_SRA";
            activitybind69.Path = "WFinstanceId";
            workflowparameterbinding69.ParameterName = "InstanceID";
            workflowparameterbinding69.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind69)));
            activitybind70.Name = "WF_SRA";
            activitybind70.Path = "WFFault";
            workflowparameterbinding70.ParameterName = "e";
            workflowparameterbinding70.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind70)));
            this.callExternalMethodActivity50.ParameterBindings.Add(workflowparameterbinding69);
            this.callExternalMethodActivity50.ParameterBindings.Add(workflowparameterbinding70);
            this.callExternalMethodActivity50.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity58
            // 
            this.callExternalMethodActivity58.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity58.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity58.Name = "callExternalMethodActivity58";
            activitybind71.Name = "WF_SRA";
            activitybind71.Path = "WFinstanceId";
            workflowparameterbinding71.ParameterName = "InstanceID";
            workflowparameterbinding71.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind71)));
            activitybind72.Name = "WF_SRA";
            activitybind72.Path = "WFFault";
            workflowparameterbinding72.ParameterName = "e";
            workflowparameterbinding72.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind72)));
            this.callExternalMethodActivity58.ParameterBindings.Add(workflowparameterbinding71);
            this.callExternalMethodActivity58.ParameterBindings.Add(workflowparameterbinding72);
            this.callExternalMethodActivity58.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity3
            // 
            this.callExternalMethodActivity3.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity3.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity3.Name = "callExternalMethodActivity3";
            activitybind73.Name = "WF_SRA";
            activitybind73.Path = "WFinstanceId";
            workflowparameterbinding73.ParameterName = "InstanceID";
            workflowparameterbinding73.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind73)));
            activitybind74.Name = "WF_SRA";
            activitybind74.Path = "WFFault";
            workflowparameterbinding74.ParameterName = "e";
            workflowparameterbinding74.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind74)));
            this.callExternalMethodActivity3.ParameterBindings.Add(workflowparameterbinding73);
            this.callExternalMethodActivity3.ParameterBindings.Add(workflowparameterbinding74);
            this.callExternalMethodActivity3.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity51
            // 
            this.callExternalMethodActivity51.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity51.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity51.Name = "callExternalMethodActivity51";
            activitybind75.Name = "WF_SRA";
            activitybind75.Path = "WFinstanceId";
            workflowparameterbinding75.ParameterName = "InstanceID";
            workflowparameterbinding75.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind75)));
            activitybind76.Name = "WF_SRA";
            activitybind76.Path = "WFFault";
            workflowparameterbinding76.ParameterName = "e";
            workflowparameterbinding76.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind76)));
            this.callExternalMethodActivity51.ParameterBindings.Add(workflowparameterbinding75);
            this.callExternalMethodActivity51.ParameterBindings.Add(workflowparameterbinding76);
            this.callExternalMethodActivity51.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // callExternalMethodActivity55
            // 
            this.callExternalMethodActivity55.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity55.MethodName = "SendOnWorkflowException";
            this.callExternalMethodActivity55.Name = "callExternalMethodActivity55";
            activitybind77.Name = "WF_SRA";
            activitybind77.Path = "WFinstanceId";
            workflowparameterbinding77.ParameterName = "InstanceID";
            workflowparameterbinding77.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind77)));
            activitybind78.Name = "WF_SRA";
            activitybind78.Path = "WFFault";
            workflowparameterbinding78.ParameterName = "e";
            workflowparameterbinding78.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind78)));
            this.callExternalMethodActivity55.ParameterBindings.Add(workflowparameterbinding77);
            this.callExternalMethodActivity55.ParameterBindings.Add(workflowparameterbinding78);
            this.callExternalMethodActivity55.MethodInvoking += new System.EventHandler(this.SetFaultForHandlers);
            // 
            // faultHandlerActivity34
            // 
            this.faultHandlerActivity34.Activities.Add(this.callExternalMethodActivity12);
            this.faultHandlerActivity34.FaultType = typeof(System.Exception);
            this.faultHandlerActivity34.Name = "faultHandlerActivity34";
            // 
            // faultHandlerActivity35
            // 
            this.faultHandlerActivity35.Activities.Add(this.callExternalMethodActivity42);
            this.faultHandlerActivity35.FaultType = typeof(System.Exception);
            this.faultHandlerActivity35.Name = "faultHandlerActivity35";
            // 
            // ifElseBranchActivity11
            // 
            this.ifElseBranchActivity11.Activities.Add(this.execSetSalesRejected);
            this.ifElseBranchActivity11.Activities.Add(this.setStateActivity30);
            this.ifElseBranchActivity11.Name = "ifElseBranchActivity11";
            // 
            // ifElseBranchActivity10
            // 
            this.ifElseBranchActivity10.Activities.Add(this.callExternalMethodActivity35);
            ruleconditionreference19.ConditionName = "IsAuthorized";
            this.ifElseBranchActivity10.Condition = ruleconditionreference19;
            this.ifElseBranchActivity10.Name = "ifElseBranchActivity10";
            // 
            // faultHandlerActivity36
            // 
            this.faultHandlerActivity36.Activities.Add(this.callExternalMethodActivity43);
            this.faultHandlerActivity36.FaultType = typeof(System.Exception);
            this.faultHandlerActivity36.Name = "faultHandlerActivity36";
            // 
            // faultHandlerActivity31
            // 
            this.faultHandlerActivity31.Activities.Add(this.callExternalMethodActivity13);
            this.faultHandlerActivity31.FaultType = typeof(System.Exception);
            this.faultHandlerActivity31.Name = "faultHandlerActivity31";
            // 
            // faultHandlerActivity33
            // 
            this.faultHandlerActivity33.Activities.Add(this.callExternalMethodActivity40);
            this.faultHandlerActivity33.FaultType = typeof(System.Exception);
            this.faultHandlerActivity33.Name = "faultHandlerActivity33";
            // 
            // ifElseBranchActivity13
            // 
            this.ifElseBranchActivity13.Activities.Add(this.ifElseActivity7);
            this.ifElseBranchActivity13.Name = "ifElseBranchActivity13";
            // 
            // ifElseBranchActivity12
            // 
            this.ifElseBranchActivity12.Activities.Add(this.callExternalMethodActivity36);
            ruleconditionreference20.ConditionName = "IsAuthorized";
            this.ifElseBranchActivity12.Condition = ruleconditionreference20;
            this.ifElseBranchActivity12.Name = "ifElseBranchActivity12";
            // 
            // faultHandlerActivity32
            // 
            this.faultHandlerActivity32.Activities.Add(this.callExternalMethodActivity41);
            this.faultHandlerActivity32.FaultType = typeof(System.Exception);
            this.faultHandlerActivity32.Name = "faultHandlerActivity32";
            // 
            // faultHandlerActivity24
            // 
            this.faultHandlerActivity24.Activities.Add(this.callExternalMethodActivity11);
            this.faultHandlerActivity24.FaultType = typeof(System.Exception);
            this.faultHandlerActivity24.Name = "faultHandlerActivity24";
            // 
            // faultHandlerActivity22
            // 
            this.faultHandlerActivity22.Activities.Add(this.callExternalMethodActivity44);
            this.faultHandlerActivity22.FaultType = typeof(System.Exception);
            this.faultHandlerActivity22.Name = "faultHandlerActivity22";
            // 
            // ifElseBranchActivity9
            // 
            this.ifElseBranchActivity9.Activities.Add(this.ifElseActivity4);
            this.ifElseBranchActivity9.Name = "ifElseBranchActivity9";
            // 
            // ifElseBranchActivity8
            // 
            this.ifElseBranchActivity8.Activities.Add(this.callExternalMethodActivity34);
            ruleconditionreference21.ConditionName = "IsAuthorized";
            this.ifElseBranchActivity8.Condition = ruleconditionreference21;
            this.ifElseBranchActivity8.Name = "ifElseBranchActivity8";
            // 
            // faultHandlerActivity23
            // 
            this.faultHandlerActivity23.Activities.Add(this.callExternalMethodActivity45);
            this.faultHandlerActivity23.FaultType = typeof(System.Exception);
            this.faultHandlerActivity23.Name = "faultHandlerActivity23";
            // 
            // ifElseBranchActivity5
            // 
            this.ifElseBranchActivity5.Activities.Add(this.callExternalMethodActivity31);
            ruleconditionreference22.ConditionName = "IsNotAuthorized";
            this.ifElseBranchActivity5.Condition = ruleconditionreference22;
            this.ifElseBranchActivity5.Name = "ifElseBranchActivity5";
            // 
            // faultHandlerActivity8
            // 
            this.faultHandlerActivity8.Activities.Add(this.callExternalMethodActivity5);
            this.faultHandlerActivity8.FaultType = typeof(System.Exception);
            this.faultHandlerActivity8.Name = "faultHandlerActivity8";
            // 
            // faultHandlerActivity9
            // 
            this.faultHandlerActivity9.Activities.Add(this.callExternalMethodActivity49);
            this.faultHandlerActivity9.FaultType = typeof(System.Exception);
            this.faultHandlerActivity9.Name = "faultHandlerActivity9";
            // 
            // faultHandlerActivity5
            // 
            this.faultHandlerActivity5.Activities.Add(this.callExternalMethodActivity52);
            this.faultHandlerActivity5.FaultType = typeof(System.Exception);
            this.faultHandlerActivity5.Name = "faultHandlerActivity5";
            // 
            // faultHandlerActivity7
            // 
            this.faultHandlerActivity7.Activities.Add(this.callExternalMethodActivity54);
            this.faultHandlerActivity7.FaultType = typeof(System.Exception);
            this.faultHandlerActivity7.Name = "faultHandlerActivity7";
            // 
            // faultHandlerActivity6
            // 
            this.faultHandlerActivity6.Activities.Add(this.callExternalMethodActivity53);
            this.faultHandlerActivity6.FaultType = typeof(System.Exception);
            this.faultHandlerActivity6.Name = "faultHandlerActivity6";
            // 
            // faultHandlerActivity4
            // 
            this.faultHandlerActivity4.Activities.Add(this.callExternalMethodActivity1);
            this.faultHandlerActivity4.FaultType = typeof(System.Exception);
            this.faultHandlerActivity4.Name = "faultHandlerActivity4";
            // 
            // ifElseBranchActivity16
            // 
            this.ifElseBranchActivity16.Activities.Add(this.ifElseActivity15);
            ruleconditionreference23.ConditionName = "HasState";
            this.ifElseBranchActivity16.Condition = ruleconditionreference23;
            this.ifElseBranchActivity16.Name = "ifElseBranchActivity16";
            // 
            // ifElseBranchActivity3
            // 
            this.ifElseBranchActivity3.Activities.Add(this.ifElseActivity16);
            ruleconditionreference24.ConditionName = "NormalPath";
            this.ifElseBranchActivity3.Condition = ruleconditionreference24;
            this.ifElseBranchActivity3.Name = "ifElseBranchActivity3";
            // 
            // faultHandlerActivity39
            // 
            this.faultHandlerActivity39.Activities.Add(this.callExternalMethodActivity16);
            this.faultHandlerActivity39.FaultType = typeof(System.Exception);
            this.faultHandlerActivity39.Name = "faultHandlerActivity39";
            // 
            // faultHandlerActivity40
            // 
            this.faultHandlerActivity40.Activities.Add(this.callExternalMethodActivity15);
            this.faultHandlerActivity40.FaultType = typeof(System.Exception);
            this.faultHandlerActivity40.Name = "faultHandlerActivity40";
            // 
            // faultHandlerActivity37
            // 
            this.faultHandlerActivity37.Activities.Add(this.callExternalMethodActivity14);
            this.faultHandlerActivity37.FaultType = typeof(System.Exception);
            this.faultHandlerActivity37.Name = "faultHandlerActivity37";
            // 
            // faultHandlerActivity12
            // 
            this.faultHandlerActivity12.Activities.Add(this.callExternalMethodActivity38);
            this.faultHandlerActivity12.FaultType = typeof(System.Exception);
            this.faultHandlerActivity12.Name = "faultHandlerActivity12";
            // 
            // ifElseBranchActivity15
            // 
            this.ifElseBranchActivity15.Activities.Add(this.setStateActivity19);
            this.ifElseBranchActivity15.Name = "ifElseBranchActivity15";
            // 
            // ifElseBranchActivity14
            // 
            this.ifElseBranchActivity14.Activities.Add(this.callExternalMethodActivity37);
            ruleconditionreference25.ConditionName = "IsAuthorized";
            this.ifElseBranchActivity14.Condition = ruleconditionreference25;
            this.ifElseBranchActivity14.Name = "ifElseBranchActivity14";
            // 
            // faultHandlerActivity38
            // 
            this.faultHandlerActivity38.Activities.Add(this.callExternalMethodActivity39);
            this.faultHandlerActivity38.FaultType = typeof(System.Exception);
            this.faultHandlerActivity38.Name = "faultHandlerActivity38";
            // 
            // faultHandlerActivity18
            // 
            this.faultHandlerActivity18.Activities.Add(this.callExternalMethodActivity7);
            this.faultHandlerActivity18.FaultType = typeof(System.Exception);
            this.faultHandlerActivity18.Name = "faultHandlerActivity18";
            // 
            // faultHandlerActivity19
            // 
            this.faultHandlerActivity19.Activities.Add(this.callExternalMethodActivity46);
            this.faultHandlerActivity19.FaultType = typeof(System.Exception);
            this.faultHandlerActivity19.Name = "faultHandlerActivity19";
            // 
            // faultHandlerActivity20
            // 
            this.faultHandlerActivity20.Activities.Add(this.callExternalMethodActivity10);
            this.faultHandlerActivity20.FaultType = typeof(SpiritucSI.Unilever.SRA.WF.WFException);
            this.faultHandlerActivity20.Name = "faultHandlerActivity20";
            // 
            // faultHandlerActivity25
            // 
            this.faultHandlerActivity25.Activities.Add(this.callExternalMethodActivity56);
            this.faultHandlerActivity25.FaultType = typeof(System.Exception);
            this.faultHandlerActivity25.Name = "faultHandlerActivity25";
            // 
            // ifElseBranchActivity7
            // 
            this.ifElseBranchActivity7.Activities.Add(this.InApprovalRejected);
            this.ifElseBranchActivity7.Name = "ifElseBranchActivity7";
            // 
            // ifElseBranchActivity6
            // 
            this.ifElseBranchActivity6.Activities.Add(this.callExternalMethodActivity33);
            ruleconditionreference26.ConditionName = "IsAuthorized";
            this.ifElseBranchActivity6.Condition = ruleconditionreference26;
            this.ifElseBranchActivity6.Name = "ifElseBranchActivity6";
            // 
            // faultHandlerActivity21
            // 
            this.faultHandlerActivity21.Activities.Add(this.callExternalMethodActivity57);
            this.faultHandlerActivity21.FaultType = typeof(System.Exception);
            this.faultHandlerActivity21.Name = "faultHandlerActivity21";
            // 
            // ifElseBranchActivity4
            // 
            this.ifElseBranchActivity4.Activities.Add(this.callExternalMethodActivity32);
            ruleconditionreference27.ConditionName = "IsNotAuthorized";
            this.ifElseBranchActivity4.Condition = ruleconditionreference27;
            this.ifElseBranchActivity4.Name = "ifElseBranchActivity4";
            // 
            // faultHandlerActivity15
            // 
            this.faultHandlerActivity15.Activities.Add(this.callExternalMethodActivity9);
            this.faultHandlerActivity15.FaultType = typeof(System.Exception);
            this.faultHandlerActivity15.Name = "faultHandlerActivity15";
            // 
            // faultHandlerActivity17
            // 
            this.faultHandlerActivity17.Activities.Add(this.callExternalMethodActivity48);
            this.faultHandlerActivity17.FaultType = typeof(System.Exception);
            this.faultHandlerActivity17.Name = "faultHandlerActivity17";
            // 
            // faultHandlerActivity16
            // 
            this.faultHandlerActivity16.Activities.Add(this.callExternalMethodActivity47);
            this.faultHandlerActivity16.FaultType = typeof(System.Exception);
            this.faultHandlerActivity16.Name = "faultHandlerActivity16";
            // 
            // faultHandlersActivity16
            // 
            this.faultHandlersActivity16.Name = "faultHandlersActivity16";
            // 
            // elseValidationErrors2
            // 
            this.elseValidationErrors2.Activities.Add(this.RemoveBudget_1);
            this.elseValidationErrors2.Activities.Add(this.setStateActivity23);
            ruleconditionreference28.ConditionName = "ValidationErrors";
            this.elseValidationErrors2.Condition = ruleconditionreference28;
            this.elseValidationErrors2.Name = "elseValidationErrors2";
            // 
            // ifValidationOK2
            // 
            this.ifValidationOK2.Activities.Add(this.setStateActivity11);
            ruleconditionreference29.ConditionName = "isValidationOK";
            this.ifValidationOK2.Condition = ruleconditionreference29;
            this.ifValidationOK2.Name = "ifValidationOK2";
            // 
            // faultHandlerActivity14
            // 
            this.faultHandlerActivity14.Activities.Add(this.callExternalMethodActivity8);
            this.faultHandlerActivity14.FaultType = typeof(System.Exception);
            this.faultHandlerActivity14.Name = "faultHandlerActivity14";
            // 
            // ifElseBranchActivity2
            // 
            this.ifElseBranchActivity2.Activities.Add(this.callExternalMethodActivity30);
            ruleconditionreference30.ConditionName = "IsNotAuthorized";
            this.ifElseBranchActivity2.Condition = ruleconditionreference30;
            this.ifElseBranchActivity2.Name = "ifElseBranchActivity2";
            // 
            // elseValidationErrors_Accepted
            // 
            this.elseValidationErrors_Accepted.Activities.Add(this.setStateActivity8);
            ruleconditionreference31.ConditionName = "ValidationErrors_Accepted";
            this.elseValidationErrors_Accepted.Condition = ruleconditionreference31;
            this.elseValidationErrors_Accepted.Name = "elseValidationErrors_Accepted";
            // 
            // ifValidationOK_Accepted
            // 
            this.ifValidationOK_Accepted.Activities.Add(this.setStateActivity7);
            ruleconditionreference32.ConditionName = "isValidationOK_Accepted";
            this.ifValidationOK_Accepted.Condition = ruleconditionreference32;
            this.ifValidationOK_Accepted.Name = "ifValidationOK_Accepted";
            // 
            // faultHandlerActivity10
            // 
            this.faultHandlerActivity10.Activities.Add(this.callExternalMethodActivity4);
            this.faultHandlerActivity10.FaultType = typeof(System.Exception);
            this.faultHandlerActivity10.Name = "faultHandlerActivity10";
            // 
            // faultHandlerActivity11
            // 
            this.faultHandlerActivity11.Activities.Add(this.callExternalMethodActivity50);
            this.faultHandlerActivity11.FaultType = typeof(System.Exception);
            this.faultHandlerActivity11.Name = "faultHandlerActivity11";
            // 
            // faultHandlerActivity13
            // 
            this.faultHandlerActivity13.Activities.Add(this.callExternalMethodActivity58);
            this.faultHandlerActivity13.FaultType = typeof(System.Exception);
            this.faultHandlerActivity13.Name = "faultHandlerActivity13";
            // 
            // faultHandlerActivity2
            // 
            this.faultHandlerActivity2.Activities.Add(this.callExternalMethodActivity3);
            this.faultHandlerActivity2.FaultType = typeof(System.Exception);
            this.faultHandlerActivity2.Name = "faultHandlerActivity2";
            // 
            // faultHandlerActivity3
            // 
            this.faultHandlerActivity3.Activities.Add(this.callExternalMethodActivity51);
            this.faultHandlerActivity3.FaultType = typeof(System.Exception);
            this.faultHandlerActivity3.Name = "faultHandlerActivity3";
            // 
            // faultHandlerActivity1
            // 
            this.faultHandlerActivity1.Activities.Add(this.callExternalMethodActivity55);
            this.faultHandlerActivity1.FaultType = typeof(System.Exception);
            this.faultHandlerActivity1.Name = "faultHandlerActivity1";
            // 
            // faultHandlersActivity35
            // 
            this.faultHandlersActivity35.Activities.Add(this.faultHandlerActivity34);
            this.faultHandlersActivity35.Name = "faultHandlersActivity35";
            // 
            // callExternalMethodActivity25
            // 
            this.callExternalMethodActivity25.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity25.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity25.Name = "callExternalMethodActivity25";
            workflowparameterbinding79.ParameterName = "State";
            workflowparameterbinding79.Value = "SalesApproval";
            activitybind79.Name = "WF_SRA";
            activitybind79.Path = "WFinstanceId";
            workflowparameterbinding80.ParameterName = "InstanceID";
            workflowparameterbinding80.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind79)));
            this.callExternalMethodActivity25.ParameterBindings.Add(workflowparameterbinding79);
            this.callExternalMethodActivity25.ParameterBindings.Add(workflowparameterbinding80);
            // 
            // faultHandlersActivity36
            // 
            this.faultHandlersActivity36.Activities.Add(this.faultHandlerActivity35);
            this.faultHandlersActivity36.Name = "faultHandlersActivity36";
            // 
            // ifElseActivity12
            // 
            this.ifElseActivity12.Activities.Add(this.ifElseBranchActivity10);
            this.ifElseActivity12.Activities.Add(this.ifElseBranchActivity11);
            this.ifElseActivity12.Name = "ifElseActivity12";
            activitybind80.Name = "WF_SRA";
            activitybind80.Path = "SalesDirector";
            // 
            // handleExternalEventActivity24
            // 
            this.handleExternalEventActivity24.EventName = "OnRejectedBySales";
            this.handleExternalEventActivity24.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity24.Name = "handleExternalEventActivity24";
            this.handleExternalEventActivity24.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind80)));
            // 
            // faultHandlersActivity37
            // 
            this.faultHandlersActivity37.Activities.Add(this.faultHandlerActivity36);
            this.faultHandlersActivity37.Name = "faultHandlersActivity37";
            // 
            // setStateActivity28
            // 
            this.setStateActivity28.Name = "setStateActivity28";
            this.setStateActivity28.TargetStateName = "FinantialApproval";
            // 
            // ValidateAndAuthorize_4
            // 
            this.ValidateAndAuthorize_4.Name = "ValidateAndAuthorize_4";
            this.ValidateAndAuthorize_4.ExecuteCode += new System.EventHandler(this.ValidateAndAuthorize);
            // 
            // execSetSalesApproved
            // 
            this.execSetSalesApproved.Name = "execSetSalesApproved";
            this.execSetSalesApproved.ExecuteCode += new System.EventHandler(this.SetSalesApproved);
            activitybind81.Name = "WF_SRA";
            activitybind81.Path = "SalesDirector";
            // 
            // handleExternalEventActivity22
            // 
            this.handleExternalEventActivity22.EventName = "OnSalesApproval";
            this.handleExternalEventActivity22.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity22.Name = "handleExternalEventActivity22";
            this.handleExternalEventActivity22.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind81)));
            // 
            // faultHandlersActivity32
            // 
            this.faultHandlersActivity32.Activities.Add(this.faultHandlerActivity31);
            this.faultHandlersActivity32.Name = "faultHandlersActivity32";
            // 
            // callExternalMethodActivity26
            // 
            this.callExternalMethodActivity26.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity26.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity26.Name = "callExternalMethodActivity26";
            activitybind82.Name = "FinantialApproval";
            activitybind82.Path = "Name";
            workflowparameterbinding81.ParameterName = "State";
            workflowparameterbinding81.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind82)));
            activitybind83.Name = "WF_SRA";
            activitybind83.Path = "WFinstanceId";
            workflowparameterbinding82.ParameterName = "InstanceID";
            workflowparameterbinding82.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind83)));
            this.callExternalMethodActivity26.ParameterBindings.Add(workflowparameterbinding81);
            this.callExternalMethodActivity26.ParameterBindings.Add(workflowparameterbinding82);
            // 
            // faultHandlersActivity34
            // 
            this.faultHandlersActivity34.Activities.Add(this.faultHandlerActivity33);
            this.faultHandlersActivity34.Name = "faultHandlersActivity34";
            // 
            // ifElseActivity13
            // 
            this.ifElseActivity13.Activities.Add(this.ifElseBranchActivity12);
            this.ifElseActivity13.Activities.Add(this.ifElseBranchActivity13);
            this.ifElseActivity13.Name = "ifElseActivity13";
            activitybind84.Name = "WF_SRA";
            activitybind84.Path = "FinantialDirector";
            // 
            // handleExternalEventActivity19
            // 
            this.handleExternalEventActivity19.EventName = "OnRejectedByFinantial";
            this.handleExternalEventActivity19.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity19.Name = "handleExternalEventActivity19";
            this.handleExternalEventActivity19.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind84)));
            // 
            // faultHandlersActivity33
            // 
            this.faultHandlersActivity33.Activities.Add(this.faultHandlerActivity32);
            this.faultHandlersActivity33.Name = "faultHandlersActivity33";
            // 
            // setStateActivity26
            // 
            this.setStateActivity26.Name = "setStateActivity26";
            this.setStateActivity26.TargetStateName = "TopApproval";
            // 
            // ValidateAndAuthorize_6
            // 
            this.ValidateAndAuthorize_6.Name = "ValidateAndAuthorize_6";
            this.ValidateAndAuthorize_6.ExecuteCode += new System.EventHandler(this.ValidateAndAuthorize);
            activitybind85.Name = "WF_SRA";
            activitybind85.Path = "FinantialDirector";
            // 
            // handleExternalEventActivity17
            // 
            this.handleExternalEventActivity17.EventName = "OnFinantialApproval";
            this.handleExternalEventActivity17.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity17.Name = "handleExternalEventActivity17";
            this.handleExternalEventActivity17.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind85)));
            // 
            // faultHandlersActivity24
            // 
            this.faultHandlersActivity24.Activities.Add(this.faultHandlerActivity24);
            this.faultHandlersActivity24.Name = "faultHandlersActivity24";
            // 
            // callExternalMethodActivity24
            // 
            this.callExternalMethodActivity24.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity24.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity24.Name = "callExternalMethodActivity24";
            activitybind86.Name = "ManagerApproval";
            activitybind86.Path = "Name";
            workflowparameterbinding83.ParameterName = "State";
            workflowparameterbinding83.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind86)));
            activitybind87.Name = "WF_SRA";
            activitybind87.Path = "WFinstanceId";
            workflowparameterbinding84.ParameterName = "InstanceID";
            workflowparameterbinding84.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind87)));
            this.callExternalMethodActivity24.ParameterBindings.Add(workflowparameterbinding83);
            this.callExternalMethodActivity24.ParameterBindings.Add(workflowparameterbinding84);
            // 
            // faultHandlersActivity26
            // 
            this.faultHandlersActivity26.Activities.Add(this.faultHandlerActivity22);
            this.faultHandlersActivity26.Name = "faultHandlersActivity26";
            // 
            // ifElseActivity11
            // 
            this.ifElseActivity11.Activities.Add(this.ifElseBranchActivity8);
            this.ifElseActivity11.Activities.Add(this.ifElseBranchActivity9);
            this.ifElseActivity11.Name = "ifElseActivity11";
            activitybind88.Name = "WF_SRA";
            activitybind88.Path = "RegManager";
            // 
            // handleExternalEventActivity7
            // 
            this.handleExternalEventActivity7.EventName = "OnRejectedByManager";
            this.handleExternalEventActivity7.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity7.Name = "handleExternalEventActivity7";
            this.handleExternalEventActivity7.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.handleExternalEventActivity7_Invoked);
            this.handleExternalEventActivity7.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind88)));
            // 
            // faultHandlersActivity25
            // 
            this.faultHandlersActivity25.Activities.Add(this.faultHandlerActivity23);
            this.faultHandlersActivity25.Name = "faultHandlersActivity25";
            // 
            // ifElseActivity9
            // 
            this.ifElseActivity9.Activities.Add(this.ifElseBranchActivity5);
            this.ifElseActivity9.Name = "ifElseActivity9";
            // 
            // setStateActivity4
            // 
            this.setStateActivity4.Name = "setStateActivity4";
            this.setStateActivity4.TargetStateName = "SalesApproval";
            // 
            // ValidateAndAuthorize_2
            // 
            this.ValidateAndAuthorize_2.Name = "ValidateAndAuthorize_2";
            this.ValidateAndAuthorize_2.ExecuteCode += new System.EventHandler(this.ValidateAndAuthorize);
            activitybind89.Name = "WF_SRA";
            activitybind89.Path = "RegManager";
            // 
            // handleExternalEventActivity5
            // 
            this.handleExternalEventActivity5.EventName = "OnValidateByManager";
            this.handleExternalEventActivity5.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity5.Name = "handleExternalEventActivity5";
            this.handleExternalEventActivity5.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind89)));
            // 
            // faultHandlersActivity8
            // 
            this.faultHandlersActivity8.Activities.Add(this.faultHandlerActivity8);
            this.faultHandlersActivity8.Name = "faultHandlersActivity8";
            // 
            // callExternalMethodActivity19
            // 
            this.callExternalMethodActivity19.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity19.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity19.Name = "callExternalMethodActivity19";
            activitybind90.Name = "Delegated";
            activitybind90.Path = "Name";
            workflowparameterbinding85.ParameterName = "State";
            workflowparameterbinding85.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind90)));
            activitybind91.Name = "WF_SRA";
            activitybind91.Path = "WFinstanceId";
            workflowparameterbinding86.ParameterName = "InstanceID";
            workflowparameterbinding86.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind91)));
            this.callExternalMethodActivity19.ParameterBindings.Add(workflowparameterbinding85);
            this.callExternalMethodActivity19.ParameterBindings.Add(workflowparameterbinding86);
            // 
            // faultHandlersActivity9
            // 
            this.faultHandlersActivity9.Activities.Add(this.faultHandlerActivity9);
            this.faultHandlersActivity9.Name = "faultHandlersActivity9";
            // 
            // setStateActivity13
            // 
            this.setStateActivity13.Name = "setStateActivity13";
            this.setStateActivity13.TargetStateName = "Accepted";
            activitybind92.Name = "WF_SRA";
            activitybind92.Path = "ProcessCreatorGroup";
            // 
            // handleExternalEventActivity20
            // 
            this.handleExternalEventActivity20.EventName = "OnDelegatedUpdateSystem";
            this.handleExternalEventActivity20.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity20.Name = "handleExternalEventActivity20";
            this.handleExternalEventActivity20.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind92)));
            // 
            // faultHandlersActivity5
            // 
            this.faultHandlersActivity5.Activities.Add(this.faultHandlerActivity5);
            this.faultHandlersActivity5.Name = "faultHandlersActivity5";
            // 
            // callExternalMethodActivity6
            // 
            this.callExternalMethodActivity6.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity6.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity6.Name = "callExternalMethodActivity6";
            activitybind93.Name = "RenewalDraft";
            activitybind93.Path = "Name";
            workflowparameterbinding87.ParameterName = "State";
            workflowparameterbinding87.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind93)));
            activitybind94.Name = "WF_SRA";
            activitybind94.Path = "WFinstanceId";
            workflowparameterbinding88.ParameterName = "InstanceID";
            workflowparameterbinding88.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind94)));
            this.callExternalMethodActivity6.ParameterBindings.Add(workflowparameterbinding87);
            this.callExternalMethodActivity6.ParameterBindings.Add(workflowparameterbinding88);
            // 
            // faultHandlersActivity7
            // 
            this.faultHandlersActivity7.Activities.Add(this.faultHandlerActivity7);
            this.faultHandlersActivity7.Name = "faultHandlersActivity7";
            // 
            // setStateActivity6
            // 
            this.setStateActivity6.Name = "setStateActivity6";
            this.setStateActivity6.TargetStateName = "Rejected";
            activitybind95.Name = "WF_SRA";
            activitybind95.Path = "ProcessCreatorGroup";
            // 
            // handleExternalEventActivity23
            // 
            this.handleExternalEventActivity23.EventName = "OnRefuseRenewal";
            this.handleExternalEventActivity23.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity23.Name = "handleExternalEventActivity23";
            this.handleExternalEventActivity23.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind95)));
            // 
            // faultHandlersActivity6
            // 
            this.faultHandlersActivity6.Activities.Add(this.faultHandlerActivity6);
            this.faultHandlersActivity6.Name = "faultHandlersActivity6";
            // 
            // setStateActivity22
            // 
            this.setStateActivity22.Name = "setStateActivity22";
            this.setStateActivity22.TargetStateName = "Accepted";
            activitybind96.Name = "WF_SRA";
            activitybind96.Path = "ProcessCreatorGroup";
            // 
            // handleExternalEventActivity21
            // 
            this.handleExternalEventActivity21.EventName = "OnAcceptRenewal";
            this.handleExternalEventActivity21.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity21.Name = "handleExternalEventActivity21";
            this.handleExternalEventActivity21.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind96)));
            // 
            // callExternalMethodActivity2
            // 
            this.callExternalMethodActivity2.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity2.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity2.Name = "callExternalMethodActivity2";
            activitybind97.Name = "Started";
            activitybind97.Path = "Name";
            workflowparameterbinding89.ParameterName = "State";
            workflowparameterbinding89.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind97)));
            activitybind98.Name = "WF_SRA";
            activitybind98.Path = "WFinstanceId";
            workflowparameterbinding90.ParameterName = "InstanceID";
            workflowparameterbinding90.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind98)));
            this.callExternalMethodActivity2.ParameterBindings.Add(workflowparameterbinding89);
            this.callExternalMethodActivity2.ParameterBindings.Add(workflowparameterbinding90);
            // 
            // faultHandlersActivity2
            // 
            this.faultHandlersActivity2.Activities.Add(this.faultHandlerActivity4);
            this.faultHandlersActivity2.Name = "faultHandlersActivity2";
            // 
            // ifElseActivity6
            // 
            this.ifElseActivity6.Activities.Add(this.ifElseBranchActivity3);
            this.ifElseActivity6.Activities.Add(this.ifElseBranchActivity16);
            this.ifElseActivity6.Name = "ifElseActivity6";
            // 
            // getAutoRenewalInfo
            // 
            this.getAutoRenewalInfo.Description = "Obtém info se o processo é auto-renewal.";
            this.getAutoRenewalInfo.Name = "getAutoRenewalInfo";
            this.getAutoRenewalInfo.ExecuteCode += new System.EventHandler(this.getAutoRenewalInfoFromDB);
            // 
            // faultHandlersActivity41
            // 
            this.faultHandlersActivity41.Activities.Add(this.faultHandlerActivity39);
            this.faultHandlersActivity41.Name = "faultHandlersActivity41";
            // 
            // setStateActivity37
            // 
            this.setStateActivity37.Name = "setStateActivity37";
            this.setStateActivity37.TargetStateName = "Completed";
            // 
            // callExternalMethodActivity29
            // 
            this.callExternalMethodActivity29.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity29.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity29.Name = "callExternalMethodActivity29";
            activitybind99.Name = "Rejected";
            activitybind99.Path = "Name";
            workflowparameterbinding91.ParameterName = "State";
            workflowparameterbinding91.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind99)));
            activitybind100.Name = "WF_SRA";
            activitybind100.Path = "WFinstanceId";
            workflowparameterbinding92.ParameterName = "InstanceID";
            workflowparameterbinding92.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind100)));
            this.callExternalMethodActivity29.ParameterBindings.Add(workflowparameterbinding91);
            this.callExternalMethodActivity29.ParameterBindings.Add(workflowparameterbinding92);
            // 
            // faultHandlersActivity42
            // 
            this.faultHandlersActivity42.Activities.Add(this.faultHandlerActivity40);
            this.faultHandlersActivity42.Name = "faultHandlersActivity42";
            // 
            // setStateActivity5
            // 
            this.setStateActivity5.Name = "setStateActivity5";
            this.setStateActivity5.TargetStateName = "Completed";
            // 
            // callExternalMethodActivity28
            // 
            this.callExternalMethodActivity28.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity28.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity28.Name = "callExternalMethodActivity28";
            activitybind101.Name = "Approved";
            activitybind101.Path = "Name";
            workflowparameterbinding93.ParameterName = "State";
            workflowparameterbinding93.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind101)));
            activitybind102.Name = "WF_SRA";
            activitybind102.Path = "WFinstanceId";
            workflowparameterbinding94.ParameterName = "InstanceID";
            workflowparameterbinding94.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind102)));
            this.callExternalMethodActivity28.ParameterBindings.Add(workflowparameterbinding93);
            this.callExternalMethodActivity28.ParameterBindings.Add(workflowparameterbinding94);
            // 
            // faultHandlersActivity38
            // 
            this.faultHandlersActivity38.Activities.Add(this.faultHandlerActivity37);
            this.faultHandlersActivity38.Name = "faultHandlersActivity38";
            // 
            // callExternalMethodActivity27
            // 
            this.callExternalMethodActivity27.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity27.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity27.Name = "callExternalMethodActivity27";
            activitybind103.Name = "TopApproval";
            activitybind103.Path = "Name";
            workflowparameterbinding95.ParameterName = "State";
            workflowparameterbinding95.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind103)));
            activitybind104.Name = "WF_SRA";
            activitybind104.Path = "WFinstanceId";
            workflowparameterbinding96.ParameterName = "InstanceID";
            workflowparameterbinding96.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind104)));
            this.callExternalMethodActivity27.ParameterBindings.Add(workflowparameterbinding95);
            this.callExternalMethodActivity27.ParameterBindings.Add(workflowparameterbinding96);
            // 
            // faultHandlersActivity40
            // 
            this.faultHandlersActivity40.Activities.Add(this.faultHandlerActivity12);
            this.faultHandlersActivity40.Name = "faultHandlersActivity40";
            // 
            // ifElseActivity14
            // 
            this.ifElseActivity14.Activities.Add(this.ifElseBranchActivity14);
            this.ifElseActivity14.Activities.Add(this.ifElseBranchActivity15);
            this.ifElseActivity14.Name = "ifElseActivity14";
            activitybind105.Name = "WF_SRA";
            activitybind105.Path = "ManagingDirector";
            // 
            // handleExternalEventActivity18
            // 
            this.handleExternalEventActivity18.EventName = "OnRejectedByTop";
            this.handleExternalEventActivity18.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity18.Name = "handleExternalEventActivity18";
            this.handleExternalEventActivity18.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind105)));
            // 
            // faultHandlersActivity39
            // 
            this.faultHandlersActivity39.Activities.Add(this.faultHandlerActivity38);
            this.faultHandlersActivity39.Name = "faultHandlersActivity39";
            // 
            // setStateActivity14
            // 
            this.setStateActivity14.Name = "setStateActivity14";
            this.setStateActivity14.TargetStateName = "Approved";
            activitybind106.Name = "WF_SRA";
            activitybind106.Path = "ManagingDirector";
            // 
            // handleExternalEventActivity12
            // 
            this.handleExternalEventActivity12.EventName = "OnTopApproval";
            this.handleExternalEventActivity12.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity12.Name = "handleExternalEventActivity12";
            this.handleExternalEventActivity12.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind106)));
            // 
            // faultHandlersActivity19
            // 
            this.faultHandlersActivity19.Activities.Add(this.faultHandlerActivity18);
            this.faultHandlersActivity19.Name = "faultHandlersActivity19";
            // 
            // callExternalMethodActivity21
            // 
            this.callExternalMethodActivity21.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity21.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity21.Name = "callExternalMethodActivity21";
            activitybind107.Name = "ErrorVerification";
            activitybind107.Path = "Name";
            workflowparameterbinding97.ParameterName = "State";
            workflowparameterbinding97.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind107)));
            activitybind108.Name = "WF_SRA";
            activitybind108.Path = "WFinstanceId";
            workflowparameterbinding98.ParameterName = "InstanceID";
            workflowparameterbinding98.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind108)));
            this.callExternalMethodActivity21.ParameterBindings.Add(workflowparameterbinding97);
            this.callExternalMethodActivity21.ParameterBindings.Add(workflowparameterbinding98);
            // 
            // faultHandlersActivity20
            // 
            this.faultHandlersActivity20.Activities.Add(this.faultHandlerActivity19);
            this.faultHandlersActivity20.Name = "faultHandlersActivity20";
            // 
            // setStateActivity10
            // 
            this.setStateActivity10.Name = "setStateActivity10";
            this.setStateActivity10.TargetStateName = "Accepted";
            activitybind109.Name = "WF_SRA";
            activitybind109.Path = "ProcessCreatorGroup";
            // 
            // handleExternalEventActivity8
            // 
            this.handleExternalEventActivity8.EventName = "OnReviewSRA";
            this.handleExternalEventActivity8.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity8.Name = "handleExternalEventActivity8";
            this.handleExternalEventActivity8.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind109)));
            // 
            // faultHandlersActivity21
            // 
            this.faultHandlersActivity21.Activities.Add(this.faultHandlerActivity20);
            this.faultHandlersActivity21.Name = "faultHandlersActivity21";
            // 
            // callExternalMethodActivity23
            // 
            this.callExternalMethodActivity23.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity23.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity23.Name = "callExternalMethodActivity23";
            activitybind110.Name = "InApproval";
            activitybind110.Path = "Name";
            workflowparameterbinding99.ParameterName = "State";
            workflowparameterbinding99.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind110)));
            activitybind111.Name = "WF_SRA";
            activitybind111.Path = "WFinstanceId";
            workflowparameterbinding100.ParameterName = "InstanceID";
            workflowparameterbinding100.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind111)));
            this.callExternalMethodActivity23.ParameterBindings.Add(workflowparameterbinding99);
            this.callExternalMethodActivity23.ParameterBindings.Add(workflowparameterbinding100);
            // 
            // faultHandlersActivity23
            // 
            this.faultHandlersActivity23.Activities.Add(this.faultHandlerActivity25);
            this.faultHandlersActivity23.Name = "faultHandlersActivity23";
            // 
            // ifElseActivity10
            // 
            this.ifElseActivity10.Activities.Add(this.ifElseBranchActivity6);
            this.ifElseActivity10.Activities.Add(this.ifElseBranchActivity7);
            this.ifElseActivity10.Name = "ifElseActivity10";
            activitybind112.Name = "WF_SRA";
            activitybind112.Path = "Supervisor";
            // 
            // handleExternalEventActivity14
            // 
            this.handleExternalEventActivity14.EventName = "OnRejectedBySupervisor";
            this.handleExternalEventActivity14.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity14.Name = "handleExternalEventActivity14";
            this.handleExternalEventActivity14.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind112)));
            // 
            // faultHandlersActivity22
            // 
            this.faultHandlersActivity22.Activities.Add(this.faultHandlerActivity21);
            this.faultHandlersActivity22.Name = "faultHandlersActivity22";
            // 
            // ifElseActivity8
            // 
            this.ifElseActivity8.Activities.Add(this.ifElseBranchActivity4);
            this.ifElseActivity8.Name = "ifElseActivity8";
            // 
            // setStateActivity9
            // 
            this.setStateActivity9.Name = "setStateActivity9";
            this.setStateActivity9.TargetStateName = "ManagerApproval";
            // 
            // ValidateAndAuthorize_1
            // 
            this.ValidateAndAuthorize_1.Name = "ValidateAndAuthorize_1";
            this.ValidateAndAuthorize_1.ExecuteCode += new System.EventHandler(this.ValidateAndAuthorize);
            activitybind113.Name = "WF_SRA";
            activitybind113.Path = "Supervisor";
            // 
            // handleExternalEventActivity6
            // 
            this.handleExternalEventActivity6.EventName = "OnValidateBySupervisor";
            this.handleExternalEventActivity6.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity6.Name = "handleExternalEventActivity6";
            this.handleExternalEventActivity6.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind113)));
            // 
            // faultHandlersActivity15
            // 
            this.faultHandlersActivity15.Activities.Add(this.faultHandlerActivity15);
            this.faultHandlersActivity15.Name = "faultHandlersActivity15";
            // 
            // callExternalMethodActivity22
            // 
            this.callExternalMethodActivity22.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity22.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity22.Name = "callExternalMethodActivity22";
            activitybind114.Name = "Received";
            activitybind114.Path = "Name";
            workflowparameterbinding101.ParameterName = "State";
            workflowparameterbinding101.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind114)));
            activitybind115.Name = "WF_SRA";
            activitybind115.Path = "WFinstanceId";
            workflowparameterbinding102.ParameterName = "InstanceID";
            workflowparameterbinding102.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind115)));
            this.callExternalMethodActivity22.ParameterBindings.Add(workflowparameterbinding101);
            this.callExternalMethodActivity22.ParameterBindings.Add(workflowparameterbinding102);
            // 
            // faultHandlersActivity18
            // 
            this.faultHandlersActivity18.Activities.Add(this.faultHandlerActivity17);
            this.faultHandlersActivity18.Name = "faultHandlersActivity18";
            // 
            // setStateActivity12
            // 
            this.setStateActivity12.Name = "setStateActivity12";
            this.setStateActivity12.TargetStateName = "ErrorVerification";
            // 
            // RemoveBudget_2
            // 
            this.RemoveBudget_2.Name = "RemoveBudget_2";
            this.RemoveBudget_2.ExecuteCode += new System.EventHandler(this.RemoveBudget);
            activitybind116.Name = "WF_SRA";
            activitybind116.Path = "SMI";
            // 
            // handleExternalEventActivity9
            // 
            this.handleExternalEventActivity9.EventName = "OnSalesReviewSRA";
            this.handleExternalEventActivity9.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity9.Name = "handleExternalEventActivity9";
            this.handleExternalEventActivity9.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind116)));
            // 
            // faultHandlersActivity17
            // 
            this.faultHandlersActivity17.Activities.Add(this.faultHandlerActivity16);
            this.faultHandlersActivity17.Name = "faultHandlersActivity17";
            // 
            // ifElseActivity3
            // 
            this.ifElseActivity3.Activities.Add(this.ifValidationOK2);
            this.ifElseActivity3.Activities.Add(this.elseValidationErrors2);
            this.ifElseActivity3.Activities.Add(this.faultHandlersActivity16);
            this.ifElseActivity3.Name = "ifElseActivity3";
            // 
            // callRuleSetValidation2
            // 
            this.callRuleSetValidation2.Name = "callRuleSetValidation2";
            this.callRuleSetValidation2.ExecuteCode += new System.EventHandler(this.RuleSetValidationReceived);
            activitybind117.Name = "WF_SRA";
            activitybind117.Path = "SMI";
            // 
            // handleExternalEventActivity4
            // 
            this.handleExternalEventActivity4.EventName = "OnUpdateSystemReceived";
            this.handleExternalEventActivity4.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity4.Name = "handleExternalEventActivity4";
            this.handleExternalEventActivity4.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind117)));
            // 
            // faultHandlersActivity14
            // 
            this.faultHandlersActivity14.Activities.Add(this.faultHandlerActivity14);
            this.faultHandlersActivity14.Name = "faultHandlersActivity14";
            // 
            // ifElseActivity5
            // 
            this.ifElseActivity5.Activities.Add(this.ifElseBranchActivity2);
            this.ifElseActivity5.Name = "ifElseActivity5";
            // 
            // ifElseActivity2
            // 
            this.ifElseActivity2.Activities.Add(this.ifValidationOK_Accepted);
            this.ifElseActivity2.Activities.Add(this.elseValidationErrors_Accepted);
            this.ifElseActivity2.Name = "ifElseActivity2";
            // 
            // callRuleSetValidation
            // 
            this.callRuleSetValidation.Name = "callRuleSetValidation";
            this.callRuleSetValidation.ExecuteCode += new System.EventHandler(this.RuleSetValidationReceived);
            // 
            // ValidateAndAuthorize_3
            // 
            this.ValidateAndAuthorize_3.Name = "ValidateAndAuthorize_3";
            this.ValidateAndAuthorize_3.ExecuteCode += new System.EventHandler(this.ValidateAndAuthorize);
            // 
            // Cativate_1
            // 
            this.Cativate_1.Name = "Cativate_1";
            this.Cativate_1.ExecuteCode += new System.EventHandler(this.Cativate);
            // 
            // callExternalMethodActivity20
            // 
            this.callExternalMethodActivity20.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity20.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity20.Name = "callExternalMethodActivity20";
            activitybind118.Name = "Accepted";
            activitybind118.Path = "Name";
            workflowparameterbinding103.ParameterName = "State";
            workflowparameterbinding103.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind118)));
            activitybind119.Name = "WF_SRA";
            activitybind119.Path = "WFinstanceId";
            workflowparameterbinding104.ParameterName = "InstanceID";
            workflowparameterbinding104.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind119)));
            this.callExternalMethodActivity20.ParameterBindings.Add(workflowparameterbinding103);
            this.callExternalMethodActivity20.ParameterBindings.Add(workflowparameterbinding104);
            // 
            // faultHandlersActivity10
            // 
            this.faultHandlersActivity10.Activities.Add(this.faultHandlerActivity10);
            this.faultHandlersActivity10.Name = "faultHandlersActivity10";
            // 
            // callExternalMethodActivity18
            // 
            this.callExternalMethodActivity18.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity18.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity18.Name = "callExternalMethodActivity18";
            activitybind120.Name = "Printed";
            activitybind120.Path = "Name";
            workflowparameterbinding105.ParameterName = "State";
            workflowparameterbinding105.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind120)));
            activitybind121.Name = "WF_SRA";
            activitybind121.Path = "WFinstanceId";
            workflowparameterbinding106.ParameterName = "InstanceID";
            workflowparameterbinding106.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind121)));
            this.callExternalMethodActivity18.ParameterBindings.Add(workflowparameterbinding105);
            this.callExternalMethodActivity18.ParameterBindings.Add(workflowparameterbinding106);
            // 
            // faultHandlersActivity11
            // 
            this.faultHandlersActivity11.Activities.Add(this.faultHandlerActivity11);
            this.faultHandlersActivity11.Name = "faultHandlersActivity11";
            // 
            // setStateActivity3
            // 
            this.setStateActivity3.Name = "setStateActivity3";
            this.setStateActivity3.TargetStateName = "Accepted";
            activitybind122.Name = "WF_SRA";
            activitybind122.Path = "ProcessCreatorGroup";
            // 
            // handleExternalEventActivity2
            // 
            this.handleExternalEventActivity2.EventName = "OnUpdateSystemPrinted";
            this.handleExternalEventActivity2.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleExternalEventActivity2.Name = "handleExternalEventActivity2";
            this.handleExternalEventActivity2.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind122)));
            // 
            // cancellationHandlerActivity2
            // 
            this.cancellationHandlerActivity2.Name = "cancellationHandlerActivity2";
            // 
            // faultHandlersActivity12
            // 
            this.faultHandlersActivity12.Activities.Add(this.faultHandlerActivity13);
            this.faultHandlersActivity12.Name = "faultHandlersActivity12";
            // 
            // setStateActivity41
            // 
            this.setStateActivity41.Name = "setStateActivity41";
            this.setStateActivity41.TargetStateName = "Rejected";
            activitybind123.Name = "WF_SRA";
            activitybind123.Path = "ProcessCreatorGroup";
            // 
            // OnRefuseDraftActivity
            // 
            this.OnRefuseDraftActivity.Description = "Rejeitar Draft";
            this.OnRefuseDraftActivity.EventName = "OnRefuseDraft";
            this.OnRefuseDraftActivity.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.OnRefuseDraftActivity.Name = "OnRefuseDraftActivity";
            this.OnRefuseDraftActivity.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind123)));
            // 
            // cancellationHandlerActivity1
            // 
            this.cancellationHandlerActivity1.Name = "cancellationHandlerActivity1";
            // 
            // faultHandlersActivity3
            // 
            this.faultHandlersActivity3.Activities.Add(this.faultHandlerActivity2);
            this.faultHandlersActivity3.Name = "faultHandlersActivity3";
            // 
            // callExternalMethodActivity17
            // 
            this.callExternalMethodActivity17.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWorkflowManager);
            this.callExternalMethodActivity17.MethodName = "SendInstanceStateChange";
            this.callExternalMethodActivity17.Name = "callExternalMethodActivity17";
            activitybind124.Name = "Draft";
            activitybind124.Path = "Name";
            workflowparameterbinding107.ParameterName = "State";
            workflowparameterbinding107.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind124)));
            activitybind125.Name = "WF_SRA";
            activitybind125.Path = "WFinstanceId";
            workflowparameterbinding108.ParameterName = "InstanceID";
            workflowparameterbinding108.SetBinding(System.Workflow.ComponentModel.WorkflowParameterBinding.ValueProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind125)));
            this.callExternalMethodActivity17.ParameterBindings.Add(workflowparameterbinding107);
            this.callExternalMethodActivity17.ParameterBindings.Add(workflowparameterbinding108);
            // 
            // faultHandlersActivity4
            // 
            this.faultHandlersActivity4.Activities.Add(this.faultHandlerActivity3);
            this.faultHandlersActivity4.Name = "faultHandlersActivity4";
            // 
            // setStateActivity1
            // 
            this.setStateActivity1.Name = "setStateActivity1";
            this.setStateActivity1.TargetStateName = "Printed";
            activitybind126.Name = "WF_SRA";
            activitybind126.Path = "ProcessCreatorGroup";
            // 
            // handleOnPrintSRA
            // 
            this.handleOnPrintSRA.Description = "Imprimir Contracto";
            this.handleOnPrintSRA.EventName = "OnPrintSRA";
            this.handleOnPrintSRA.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleOnPrintSRA.Name = "handleOnPrintSRA";
            this.handleOnPrintSRA.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind126)));
            // 
            // faultHandlersActivity1
            // 
            this.faultHandlersActivity1.Activities.Add(this.faultHandlerActivity1);
            this.faultHandlersActivity1.Name = "faultHandlersActivity1";
            // 
            // setStateActivity2
            // 
            this.setStateActivity2.Name = "setStateActivity2";
            this.setStateActivity2.TargetStateName = "Delegated";
            // 
            // codeActivity13
            // 
            this.codeActivity13.Name = "codeActivity13";
            this.codeActivity13.ExecuteCode += new System.EventHandler(this.SendEmailtoConcessionaire);
            activitybind127.Name = "WF_SRA";
            activitybind127.Path = "ProcessCreatorGroup";
            // 
            // handleSendToConcessionaire
            // 
            this.handleSendToConcessionaire.Description = "Delegar Concessionario";
            this.handleSendToConcessionaire.EventName = "OnSendToConcessionaire";
            this.handleSendToConcessionaire.InterfaceType = typeof(SpiritucSI.Unilever.SRA.WF.IWFEvents);
            this.handleSendToConcessionaire.Name = "handleSendToConcessionaire";
            this.handleSendToConcessionaire.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind127)));
            // 
            // init_SalesApproval
            // 
            this.init_SalesApproval.Activities.Add(this.callExternalMethodActivity25);
            this.init_SalesApproval.Activities.Add(this.faultHandlersActivity35);
            this.init_SalesApproval.Name = "init_SalesApproval";
            // 
            // onRejectedBySales
            // 
            this.onRejectedBySales.Activities.Add(this.handleExternalEventActivity24);
            this.onRejectedBySales.Activities.Add(this.ifElseActivity12);
            this.onRejectedBySales.Activities.Add(this.faultHandlersActivity36);
            this.onRejectedBySales.Description = "Rejeitar";
            this.onRejectedBySales.Name = "onRejectedBySales";
            // 
            // onSalesApproval
            // 
            this.onSalesApproval.Activities.Add(this.handleExternalEventActivity22);
            this.onSalesApproval.Activities.Add(this.execSetSalesApproved);
            this.onSalesApproval.Activities.Add(this.ValidateAndAuthorize_4);
            this.onSalesApproval.Activities.Add(this.setStateActivity28);
            this.onSalesApproval.Activities.Add(this.faultHandlersActivity37);
            this.onSalesApproval.Description = "Aprovar";
            this.onSalesApproval.Name = "onSalesApproval";
            // 
            // init_FinantialApproval
            // 
            this.init_FinantialApproval.Activities.Add(this.callExternalMethodActivity26);
            this.init_FinantialApproval.Activities.Add(this.faultHandlersActivity32);
            this.init_FinantialApproval.Name = "init_FinantialApproval";
            // 
            // onRejectedByFinantial
            // 
            this.onRejectedByFinantial.Activities.Add(this.handleExternalEventActivity19);
            this.onRejectedByFinantial.Activities.Add(this.ifElseActivity13);
            this.onRejectedByFinantial.Activities.Add(this.faultHandlersActivity34);
            this.onRejectedByFinantial.Description = "Rejeitar";
            this.onRejectedByFinantial.Name = "onRejectedByFinantial";
            // 
            // onFinantialApproval
            // 
            this.onFinantialApproval.Activities.Add(this.handleExternalEventActivity17);
            this.onFinantialApproval.Activities.Add(this.ValidateAndAuthorize_6);
            this.onFinantialApproval.Activities.Add(this.setStateActivity26);
            this.onFinantialApproval.Activities.Add(this.faultHandlersActivity33);
            this.onFinantialApproval.Description = "Aprovar";
            this.onFinantialApproval.Name = "onFinantialApproval";
            // 
            // init_ManagerApproval
            // 
            this.init_ManagerApproval.Activities.Add(this.callExternalMethodActivity24);
            this.init_ManagerApproval.Activities.Add(this.faultHandlersActivity24);
            this.init_ManagerApproval.Name = "init_ManagerApproval";
            // 
            // onRejectedByManager
            // 
            this.onRejectedByManager.Activities.Add(this.handleExternalEventActivity7);
            this.onRejectedByManager.Activities.Add(this.ifElseActivity11);
            this.onRejectedByManager.Activities.Add(this.faultHandlersActivity26);
            this.onRejectedByManager.Description = "Rejeitar";
            this.onRejectedByManager.Name = "onRejectedByManager";
            // 
            // onValidateByManager
            // 
            this.onValidateByManager.Activities.Add(this.handleExternalEventActivity5);
            this.onValidateByManager.Activities.Add(this.ValidateAndAuthorize_2);
            this.onValidateByManager.Activities.Add(this.setStateActivity4);
            this.onValidateByManager.Activities.Add(this.ifElseActivity9);
            this.onValidateByManager.Activities.Add(this.faultHandlersActivity25);
            this.onValidateByManager.Description = "Aprovar";
            this.onValidateByManager.Name = "onValidateByManager";
            // 
            // init_Delegated
            // 
            this.init_Delegated.Activities.Add(this.callExternalMethodActivity19);
            this.init_Delegated.Activities.Add(this.faultHandlersActivity8);
            this.init_Delegated.Name = "init_Delegated";
            // 
            // onDelegatedUpdateSystem
            // 
            this.onDelegatedUpdateSystem.Activities.Add(this.handleExternalEventActivity20);
            this.onDelegatedUpdateSystem.Activities.Add(this.setStateActivity13);
            this.onDelegatedUpdateSystem.Activities.Add(this.faultHandlersActivity9);
            this.onDelegatedUpdateSystem.Description = "Confirmar dados CC";
            this.onDelegatedUpdateSystem.Name = "onDelegatedUpdateSystem";
            // 
            // init_RenewalDraft
            // 
            this.init_RenewalDraft.Activities.Add(this.callExternalMethodActivity6);
            this.init_RenewalDraft.Activities.Add(this.faultHandlersActivity5);
            this.init_RenewalDraft.Name = "init_RenewalDraft";
            // 
            // onRefuseRenewal
            // 
            this.onRefuseRenewal.Activities.Add(this.handleExternalEventActivity23);
            this.onRefuseRenewal.Activities.Add(this.setStateActivity6);
            this.onRefuseRenewal.Activities.Add(this.faultHandlersActivity7);
            this.onRefuseRenewal.Description = "Rejeitar renovação";
            this.onRefuseRenewal.Name = "onRefuseRenewal";
            // 
            // onAcceptRenewal
            // 
            this.onAcceptRenewal.Activities.Add(this.handleExternalEventActivity21);
            this.onAcceptRenewal.Activities.Add(this.setStateActivity22);
            this.onAcceptRenewal.Activities.Add(this.faultHandlersActivity6);
            this.onAcceptRenewal.Description = "Aceitar renovação";
            this.onAcceptRenewal.Name = "onAcceptRenewal";
            // 
            // init_CheckIsRenewal
            // 
            this.init_CheckIsRenewal.Activities.Add(this.getAutoRenewalInfo);
            this.init_CheckIsRenewal.Activities.Add(this.ifElseActivity6);
            this.init_CheckIsRenewal.Activities.Add(this.faultHandlersActivity2);
            this.init_CheckIsRenewal.Activities.Add(this.callExternalMethodActivity2);
            this.init_CheckIsRenewal.Name = "init_CheckIsRenewal";
            // 
            // init_RejectedState
            // 
            this.init_RejectedState.Activities.Add(this.callExternalMethodActivity29);
            this.init_RejectedState.Activities.Add(this.setStateActivity37);
            this.init_RejectedState.Activities.Add(this.faultHandlersActivity41);
            this.init_RejectedState.Name = "init_RejectedState";
            // 
            // init_ApprovedState
            // 
            this.init_ApprovedState.Activities.Add(this.callExternalMethodActivity28);
            this.init_ApprovedState.Activities.Add(this.setStateActivity5);
            this.init_ApprovedState.Activities.Add(this.faultHandlersActivity42);
            this.init_ApprovedState.Name = "init_ApprovedState";
            // 
            // init_TopApproval
            // 
            this.init_TopApproval.Activities.Add(this.callExternalMethodActivity27);
            this.init_TopApproval.Activities.Add(this.faultHandlersActivity38);
            this.init_TopApproval.Name = "init_TopApproval";
            // 
            // onRejectedByTop
            // 
            this.onRejectedByTop.Activities.Add(this.handleExternalEventActivity18);
            this.onRejectedByTop.Activities.Add(this.ifElseActivity14);
            this.onRejectedByTop.Activities.Add(this.faultHandlersActivity40);
            this.onRejectedByTop.Description = "Rejeitar";
            this.onRejectedByTop.Name = "onRejectedByTop";
            // 
            // onTopApproval
            // 
            this.onTopApproval.Activities.Add(this.handleExternalEventActivity12);
            this.onTopApproval.Activities.Add(this.setStateActivity14);
            this.onTopApproval.Activities.Add(this.faultHandlersActivity39);
            this.onTopApproval.Description = "Aprovar";
            this.onTopApproval.Name = "onTopApproval";
            // 
            // init_ErrorVerification
            // 
            this.init_ErrorVerification.Activities.Add(this.callExternalMethodActivity21);
            this.init_ErrorVerification.Activities.Add(this.faultHandlersActivity19);
            this.init_ErrorVerification.Name = "init_ErrorVerification";
            // 
            // onReviewSRA
            // 
            this.onReviewSRA.Activities.Add(this.handleExternalEventActivity8);
            this.onReviewSRA.Activities.Add(this.setStateActivity10);
            this.onReviewSRA.Activities.Add(this.faultHandlersActivity20);
            this.onReviewSRA.Description = "Reenviar";
            this.onReviewSRA.Name = "onReviewSRA";
            // 
            // init_InApproval
            // 
            this.init_InApproval.Activities.Add(this.callExternalMethodActivity23);
            this.init_InApproval.Activities.Add(this.faultHandlersActivity21);
            this.init_InApproval.Name = "init_InApproval";
            // 
            // onRejectedBySupervisor
            // 
            this.onRejectedBySupervisor.Activities.Add(this.handleExternalEventActivity14);
            this.onRejectedBySupervisor.Activities.Add(this.ifElseActivity10);
            this.onRejectedBySupervisor.Activities.Add(this.faultHandlersActivity23);
            this.onRejectedBySupervisor.Description = "Rejeitar";
            this.onRejectedBySupervisor.Name = "onRejectedBySupervisor";
            // 
            // onValidateBySupervisor
            // 
            this.onValidateBySupervisor.Activities.Add(this.handleExternalEventActivity6);
            this.onValidateBySupervisor.Activities.Add(this.ValidateAndAuthorize_1);
            this.onValidateBySupervisor.Activities.Add(this.setStateActivity9);
            this.onValidateBySupervisor.Activities.Add(this.ifElseActivity8);
            this.onValidateBySupervisor.Activities.Add(this.faultHandlersActivity22);
            this.onValidateBySupervisor.Description = "Aprovar";
            this.onValidateBySupervisor.Name = "onValidateBySupervisor";
            // 
            // init_Received
            // 
            this.init_Received.Activities.Add(this.callExternalMethodActivity22);
            this.init_Received.Activities.Add(this.faultHandlersActivity15);
            this.init_Received.Name = "init_Received";
            // 
            // onSalesReviewSRA
            // 
            this.onSalesReviewSRA.Activities.Add(this.handleExternalEventActivity9);
            this.onSalesReviewSRA.Activities.Add(this.RemoveBudget_2);
            this.onSalesReviewSRA.Activities.Add(this.setStateActivity12);
            this.onSalesReviewSRA.Activities.Add(this.faultHandlersActivity18);
            this.onSalesReviewSRA.Description = "Devolver";
            this.onSalesReviewSRA.Name = "onSalesReviewSRA";
            // 
            // onUpdateSystemReceived
            // 
            this.onUpdateSystemReceived.Activities.Add(this.handleExternalEventActivity4);
            this.onUpdateSystemReceived.Activities.Add(this.callRuleSetValidation2);
            this.onUpdateSystemReceived.Activities.Add(this.ifElseActivity3);
            this.onUpdateSystemReceived.Activities.Add(this.faultHandlersActivity17);
            this.onUpdateSystemReceived.Description = "Aceitar";
            this.onUpdateSystemReceived.Name = "onUpdateSystemReceived";
            // 
            // init_RuleSetValidation
            // 
            this.init_RuleSetValidation.Activities.Add(this.callExternalMethodActivity20);
            this.init_RuleSetValidation.Activities.Add(this.Cativate_1);
            this.init_RuleSetValidation.Activities.Add(this.ValidateAndAuthorize_3);
            this.init_RuleSetValidation.Activities.Add(this.callRuleSetValidation);
            this.init_RuleSetValidation.Activities.Add(this.ifElseActivity2);
            this.init_RuleSetValidation.Activities.Add(this.ifElseActivity5);
            this.init_RuleSetValidation.Activities.Add(this.faultHandlersActivity14);
            this.init_RuleSetValidation.Name = "init_RuleSetValidation";
            // 
            // init_Printed
            // 
            this.init_Printed.Activities.Add(this.callExternalMethodActivity18);
            this.init_Printed.Activities.Add(this.faultHandlersActivity10);
            this.init_Printed.Name = "init_Printed";
            // 
            // onUpdateSystemPrinted
            // 
            this.onUpdateSystemPrinted.Activities.Add(this.handleExternalEventActivity2);
            this.onUpdateSystemPrinted.Activities.Add(this.setStateActivity3);
            this.onUpdateSystemPrinted.Activities.Add(this.faultHandlersActivity11);
            this.onUpdateSystemPrinted.Description = "Confirmar dados";
            this.onUpdateSystemPrinted.Name = "onUpdateSystemPrinted";
            // 
            // onRefuseDraft
            // 
            this.onRefuseDraft.Activities.Add(this.OnRefuseDraftActivity);
            this.onRefuseDraft.Activities.Add(this.setStateActivity41);
            this.onRefuseDraft.Activities.Add(this.faultHandlersActivity12);
            this.onRefuseDraft.Activities.Add(this.cancellationHandlerActivity2);
            this.onRefuseDraft.Description = "Rejeitar Draft";
            this.onRefuseDraft.Name = "onRefuseDraft";
            // 
            // init_Draft
            // 
            this.init_Draft.Activities.Add(this.callExternalMethodActivity17);
            this.init_Draft.Activities.Add(this.faultHandlersActivity3);
            this.init_Draft.Activities.Add(this.cancellationHandlerActivity1);
            this.init_Draft.Name = "init_Draft";
            // 
            // onPrintSRA
            // 
            this.onPrintSRA.Activities.Add(this.handleOnPrintSRA);
            this.onPrintSRA.Activities.Add(this.setStateActivity1);
            this.onPrintSRA.Activities.Add(this.faultHandlersActivity4);
            this.onPrintSRA.Description = "Para negociar (impresso)";
            this.onPrintSRA.Name = "onPrintSRA";
            // 
            // onSendToConcessionaire
            // 
            this.onSendToConcessionaire.Activities.Add(this.handleSendToConcessionaire);
            this.onSendToConcessionaire.Activities.Add(this.codeActivity13);
            this.onSendToConcessionaire.Activities.Add(this.setStateActivity2);
            this.onSendToConcessionaire.Activities.Add(this.faultHandlersActivity1);
            this.onSendToConcessionaire.Description = "Delegar no concessionário";
            this.onSendToConcessionaire.Name = "onSendToConcessionaire";
            // 
            // Completed
            // 
            this.Completed.Name = "Completed";
            // 
            // SalesApproval
            // 
            this.SalesApproval.Activities.Add(this.onSalesApproval);
            this.SalesApproval.Activities.Add(this.onRejectedBySales);
            this.SalesApproval.Activities.Add(this.init_SalesApproval);
            this.SalesApproval.Name = "SalesApproval";
            // 
            // FinantialApproval
            // 
            this.FinantialApproval.Activities.Add(this.onFinantialApproval);
            this.FinantialApproval.Activities.Add(this.onRejectedByFinantial);
            this.FinantialApproval.Activities.Add(this.init_FinantialApproval);
            this.FinantialApproval.Name = "FinantialApproval";
            // 
            // ManagerApproval
            // 
            this.ManagerApproval.Activities.Add(this.onValidateByManager);
            this.ManagerApproval.Activities.Add(this.onRejectedByManager);
            this.ManagerApproval.Activities.Add(this.init_ManagerApproval);
            this.ManagerApproval.Name = "ManagerApproval";
            // 
            // Delegated
            // 
            this.Delegated.Activities.Add(this.onDelegatedUpdateSystem);
            this.Delegated.Activities.Add(this.init_Delegated);
            this.Delegated.Name = "Delegated";
            // 
            // RenewalDraft
            // 
            this.RenewalDraft.Activities.Add(this.onAcceptRenewal);
            this.RenewalDraft.Activities.Add(this.onRefuseRenewal);
            this.RenewalDraft.Activities.Add(this.init_RenewalDraft);
            this.RenewalDraft.Name = "RenewalDraft";
            // 
            // Started
            // 
            this.Started.Activities.Add(this.init_CheckIsRenewal);
            this.Started.Name = "Started";
            // 
            // Rejected
            // 
            this.Rejected.Activities.Add(this.init_RejectedState);
            this.Rejected.Name = "Rejected";
            // 
            // Approved
            // 
            this.Approved.Activities.Add(this.init_ApprovedState);
            this.Approved.Name = "Approved";
            // 
            // TopApproval
            // 
            this.TopApproval.Activities.Add(this.onTopApproval);
            this.TopApproval.Activities.Add(this.onRejectedByTop);
            this.TopApproval.Activities.Add(this.init_TopApproval);
            this.TopApproval.Name = "TopApproval";
            // 
            // ErrorVerification
            // 
            this.ErrorVerification.Activities.Add(this.onReviewSRA);
            this.ErrorVerification.Activities.Add(this.init_ErrorVerification);
            this.ErrorVerification.Description = "SRA Reviewed";
            this.ErrorVerification.Name = "ErrorVerification";
            // 
            // InApproval
            // 
            this.InApproval.Activities.Add(this.onValidateBySupervisor);
            this.InApproval.Activities.Add(this.onRejectedBySupervisor);
            this.InApproval.Activities.Add(this.init_InApproval);
            this.InApproval.Name = "InApproval";
            // 
            // Received
            // 
            this.Received.Activities.Add(this.onUpdateSystemReceived);
            this.Received.Activities.Add(this.onSalesReviewSRA);
            this.Received.Activities.Add(this.init_Received);
            this.Received.Name = "Received";
            // 
            // Accepted
            // 
            this.Accepted.Activities.Add(this.init_RuleSetValidation);
            this.Accepted.Name = "Accepted";
            // 
            // Printed
            // 
            this.Printed.Activities.Add(this.onUpdateSystemPrinted);
            this.Printed.Activities.Add(this.init_Printed);
            this.Printed.Name = "Printed";
            // 
            // Draft
            // 
            this.Draft.Activities.Add(this.onSendToConcessionaire);
            this.Draft.Activities.Add(this.onPrintSRA);
            this.Draft.Activities.Add(this.init_Draft);
            this.Draft.Activities.Add(this.onRefuseDraft);
            this.Draft.Name = "Draft";
            // 
            // WF_SRA
            // 
            this.Activities.Add(this.Draft);
            this.Activities.Add(this.Printed);
            this.Activities.Add(this.Accepted);
            this.Activities.Add(this.Received);
            this.Activities.Add(this.InApproval);
            this.Activities.Add(this.ErrorVerification);
            this.Activities.Add(this.TopApproval);
            this.Activities.Add(this.Approved);
            this.Activities.Add(this.Rejected);
            this.Activities.Add(this.Started);
            this.Activities.Add(this.RenewalDraft);
            this.Activities.Add(this.Delegated);
            this.Activities.Add(this.ManagerApproval);
            this.Activities.Add(this.FinantialApproval);
            this.Activities.Add(this.SalesApproval);
            this.Activities.Add(this.Completed);
            this.CompletedStateName = "Completed";
            this.DynamicUpdateCondition = null;
            this.InitialStateName = "Started";
            this.Name = "WF_SRA";
            this.CanModifyActivities = false;

        }

        #endregion

        private CallExternalMethodActivity callExternalMethodActivity2;
        private SetStateActivity setStateActivity38;
        private IfElseBranchActivity ifElseBranchActivity17;
        private SetStateActivity setStateActivity39;
        private IfElseBranchActivity ifElseBranchActivity31;
        private SetStateActivity setStateActivity40;
        private IfElseBranchActivity ifElseBranchActivity32;
        private CancellationHandlerActivity cancellationHandlerActivity1;
        private CallExternalMethodActivity callExternalMethodActivity58;
        private FaultHandlerActivity faultHandlerActivity13;
        private FaultHandlersActivity faultHandlersActivity12;
        private SetStateActivity setStateActivity41;
        private HandleExternalEventActivity OnRefuseDraftActivity;
        private EventDrivenActivity onRefuseDraft;
        private CancellationHandlerActivity cancellationHandlerActivity2;
        private CallExternalMethodActivity callExternalMethodActivity57;
        private CallExternalMethodActivity callExternalMethodActivity56;
        private CallExternalMethodActivity callExternalMethodActivity55;
        private CallExternalMethodActivity callExternalMethodActivity50;
        private CallExternalMethodActivity callExternalMethodActivity52;
        private CallExternalMethodActivity callExternalMethodActivity54;
        private CallExternalMethodActivity callExternalMethodActivity53;
        private CallExternalMethodActivity callExternalMethodActivity51;
        private CallExternalMethodActivity callExternalMethodActivity1;
        private CallExternalMethodActivity callExternalMethodActivity5;
        private CallExternalMethodActivity callExternalMethodActivity4;
        private CallExternalMethodActivity callExternalMethodActivity3;
        private CallExternalMethodActivity callExternalMethodActivity12;
        private CallExternalMethodActivity callExternalMethodActivity13;
        private CallExternalMethodActivity callExternalMethodActivity11;
        private CallExternalMethodActivity callExternalMethodActivity16;
        private CallExternalMethodActivity callExternalMethodActivity15;
        private CallExternalMethodActivity callExternalMethodActivity14;
        private CallExternalMethodActivity callExternalMethodActivity7;
        private CallExternalMethodActivity callExternalMethodActivity10;
        private CallExternalMethodActivity callExternalMethodActivity9;
        private CallExternalMethodActivity callExternalMethodActivity8;
        private CallExternalMethodActivity callExternalMethodActivity6;
        private CallExternalMethodActivity callExternalMethodActivity17;
        private CallExternalMethodActivity callExternalMethodActivity19;
        private CallExternalMethodActivity callExternalMethodActivity18;
        private CallExternalMethodActivity callExternalMethodActivity21;
        private CallExternalMethodActivity callExternalMethodActivity23;
        private CallExternalMethodActivity callExternalMethodActivity22;
        private CallExternalMethodActivity callExternalMethodActivity20;
        private CallExternalMethodActivity callExternalMethodActivity25;
        private CallExternalMethodActivity callExternalMethodActivity26;
        private CallExternalMethodActivity callExternalMethodActivity24;
        private CallExternalMethodActivity callExternalMethodActivity29;
        private CallExternalMethodActivity callExternalMethodActivity28;
        private CallExternalMethodActivity callExternalMethodActivity27;
        private CallExternalMethodActivity callExternalMethodActivity30;
        private CallExternalMethodActivity callExternalMethodActivity35;
        private CallExternalMethodActivity callExternalMethodActivity36;
        private CallExternalMethodActivity callExternalMethodActivity34;
        private CallExternalMethodActivity callExternalMethodActivity31;
        private CallExternalMethodActivity callExternalMethodActivity37;
        private CallExternalMethodActivity callExternalMethodActivity33;
        private CallExternalMethodActivity callExternalMethodActivity32;
        private CallExternalMethodActivity callExternalMethodActivity42;
        private CallExternalMethodActivity callExternalMethodActivity43;
        private CallExternalMethodActivity callExternalMethodActivity40;
        private CallExternalMethodActivity callExternalMethodActivity41;
        private CallExternalMethodActivity callExternalMethodActivity44;
        private CallExternalMethodActivity callExternalMethodActivity45;
        private CallExternalMethodActivity callExternalMethodActivity38;
        private CallExternalMethodActivity callExternalMethodActivity39;
        private CallExternalMethodActivity callExternalMethodActivity46;
        private CallExternalMethodActivity callExternalMethodActivity48;
        private CallExternalMethodActivity callExternalMethodActivity47;
        private FaultHandlerActivity faultHandlerActivity12;
        private CallExternalMethodActivity callExternalMethodActivity49;
        private SetStateActivity setStateActivity33;
        private SetStateActivity setStateActivity32;
        private SetStateActivity setStateActivity31;
        private SetStateActivity setStateActivity25;
        private SetStateActivity setStateActivity21;
        private SetStateActivity setStateActivity20;
        private SetStateActivity setStateActivity18;
        private IfElseBranchActivity ifElseBranchActivity27;
        private IfElseBranchActivity ifElseBranchActivity26;
        private IfElseBranchActivity ifElseBranchActivity25;
        private IfElseBranchActivity ifElseBranchActivity24;
        private IfElseBranchActivity ifElseBranchActivity23;
        private IfElseBranchActivity ifElseBranchActivity22;
        private IfElseBranchActivity ifElseBranchActivity21;
        private SetStateActivity setStateActivity34;
        private IfElseBranchActivity ifElseBranchActivity28;
        private SetStateActivity setStateActivity36;
        private SetStateActivity setStateActivity35;
        private IfElseBranchActivity ifElseBranchActivity30;
        private IfElseBranchActivity ifElseBranchActivity29;
        private SetStateActivity setStateActivity37;
        private SetStateActivity setStateActivity5;
        private StateActivity Completed;
        private IfElseBranchActivity ifElseBranchActivity3;
        private IfElseActivity ifElseActivity6;
        private IfElseBranchActivity ifElseBranchActivity16;
        private IfElseBranchActivity ifElseBranchActivity18;
        private IfElseActivity ifElseActivity15;
        private SetStateActivity setStateActivity15;
        private SetStateActivity setStateActivity17;
        private SetStateActivity setStateActivity16;
        private IfElseBranchActivity ifElseBranchActivity20;
        private IfElseBranchActivity ifElseBranchActivity19;
        private IfElseActivity ifElseActivity16;
        private CodeActivity Cativate_1;
        private CodeActivity RemoveBudget_2;
        private CodeActivity ValidateAndAuthorize_4;
        private CodeActivity ValidateAndAuthorize_6;
        private CodeActivity RemoveBudget_1;
        private CodeActivity ValidateAndAuthorize_3;
        private CodeActivity ValidateAndAuthorize_2;
        private IfElseBranchActivity ifElseBranchActivity5;
        private IfElseActivity ifElseActivity9;
        private CodeActivity ValidateAndAuthorize_1;
        private IfElseBranchActivity ifElseBranchActivity2;
        private IfElseActivity ifElseActivity5;
        private CodeActivity codeActivity13;
        private IfElseBranchActivity ifElseBranchActivity4;
        private IfElseActivity ifElseActivity8;
        private IfElseBranchActivity ifElseBranchActivity7;
        private IfElseBranchActivity ifElseBranchActivity6;
        private IfElseActivity ifElseActivity10;
        private IfElseBranchActivity ifElseBranchActivity10;
        private IfElseBranchActivity ifElseBranchActivity13;
        private IfElseBranchActivity ifElseBranchActivity12;
        private IfElseBranchActivity ifElseBranchActivity9;
        private IfElseBranchActivity ifElseBranchActivity8;
        private IfElseActivity ifElseActivity12;
        private IfElseActivity ifElseActivity13;
        private IfElseActivity ifElseActivity11;
        private IfElseBranchActivity ifElseBranchActivity11;
        private IfElseBranchActivity ifElseBranchActivity15;
        private IfElseBranchActivity ifElseBranchActivity14;
        private IfElseActivity ifElseActivity14;
        private IfElseBranchActivity ifElseBranchActivity1;
        private IfElseActivity ifElseActivity4;
        private CodeActivity execSetSalesRejected;
        private CodeActivity execSetSalesApproved;
        private SetStateActivity setStateActivity3;
        private SetStateActivity setStateActivity4;
        private FaultHandlerActivity faultHandlerActivity1;
        private FaultHandlersActivity faultHandlersActivity1;
        private FaultHandlerActivity faultHandlerActivity5;
        private FaultHandlerActivity faultHandlerActivity7;
        private FaultHandlerActivity faultHandlerActivity6;
        private FaultHandlerActivity faultHandlerActivity4;
        private FaultHandlerActivity faultHandlerActivity2;
        private FaultHandlerActivity faultHandlerActivity3;
        private FaultHandlersActivity faultHandlersActivity5;
        private FaultHandlersActivity faultHandlersActivity7;
        private FaultHandlersActivity faultHandlersActivity6;
        private FaultHandlersActivity faultHandlersActivity2;
        private FaultHandlersActivity faultHandlersActivity3;
        private FaultHandlersActivity faultHandlersActivity4;
        private FaultHandlerActivity faultHandlerActivity8;
        private FaultHandlerActivity faultHandlerActivity9;
        private FaultHandlerActivity faultHandlerActivity10;
        private FaultHandlerActivity faultHandlerActivity11;
        private FaultHandlersActivity faultHandlersActivity8;
        private FaultHandlersActivity faultHandlersActivity9;
        private FaultHandlersActivity faultHandlersActivity10;
        private FaultHandlersActivity faultHandlersActivity11;
        private FaultHandlerActivity faultHandlerActivity15;
        private FaultHandlerActivity faultHandlerActivity17;
        private FaultHandlerActivity faultHandlerActivity16;
        private FaultHandlersActivity faultHandlersActivity16;
        private FaultHandlerActivity faultHandlerActivity14;
        private FaultHandlersActivity faultHandlersActivity15;
        private FaultHandlersActivity faultHandlersActivity18;
        private FaultHandlersActivity faultHandlersActivity17;
        private FaultHandlersActivity faultHandlersActivity14;
        private FaultHandlerActivity faultHandlerActivity18;
        private FaultHandlerActivity faultHandlerActivity19;
        private FaultHandlerActivity faultHandlerActivity20;
        private FaultHandlerActivity faultHandlerActivity21;
        private FaultHandlersActivity faultHandlersActivity19;
        private FaultHandlersActivity faultHandlersActivity20;
        private FaultHandlersActivity faultHandlersActivity21;
        private FaultHandlersActivity faultHandlersActivity23;
        private FaultHandlersActivity faultHandlersActivity22;
        private FaultHandlerActivity faultHandlerActivity24;
        private FaultHandlerActivity faultHandlerActivity22;
        private FaultHandlerActivity faultHandlerActivity23;
        private FaultHandlerActivity faultHandlerActivity25;
        private FaultHandlersActivity faultHandlersActivity24;
        private FaultHandlersActivity faultHandlersActivity26;
        private FaultHandlersActivity faultHandlersActivity25;
        private FaultHandlerActivity faultHandlerActivity34;
        private FaultHandlerActivity faultHandlerActivity35;
        private FaultHandlerActivity faultHandlerActivity36;
        private FaultHandlerActivity faultHandlerActivity31;
        private FaultHandlerActivity faultHandlerActivity33;
        private FaultHandlerActivity faultHandlerActivity32;
        private FaultHandlerActivity faultHandlerActivity39;
        private FaultHandlerActivity faultHandlerActivity40;
        private FaultHandlerActivity faultHandlerActivity37;
        private FaultHandlerActivity faultHandlerActivity38;
        private FaultHandlersActivity faultHandlersActivity35;
        private FaultHandlersActivity faultHandlersActivity36;
        private FaultHandlersActivity faultHandlersActivity37;
        private FaultHandlersActivity faultHandlersActivity32;
        private FaultHandlersActivity faultHandlersActivity34;
        private FaultHandlersActivity faultHandlersActivity33;
        private FaultHandlersActivity faultHandlersActivity41;
        private FaultHandlersActivity faultHandlersActivity42;
        private FaultHandlersActivity faultHandlersActivity38;
        private FaultHandlersActivity faultHandlersActivity40;
        private FaultHandlersActivity faultHandlersActivity39;
        private StateInitializationActivity init_Draft;
        private StateInitializationActivity init_RenewalDraft;
        private StateInitializationActivity init_Delegated;
        private StateInitializationActivity init_ErrorVerification;
        private StateInitializationActivity init_InApproval;
        private StateInitializationActivity init_Received;
        private StateInitializationActivity init_Printed;
        private StateInitializationActivity init_ManagerApproval;
        private StateInitializationActivity init_SalesApproval;
        private StateInitializationActivity init_FinantialApproval;
        private StateInitializationActivity init_TopApproval;
        private StateInitializationActivity init_ApprovedState;
        private StateInitializationActivity init_RejectedState;
        private SetStateActivity setStateActivity13;
        private HandleExternalEventActivity handleExternalEventActivity20;
        private EventDrivenActivity onDelegatedUpdateSystem;
        private StateActivity Delegated;
        private StateActivity SalesApproval;
        private StateActivity FinantialApproval;
        private HandleExternalEventActivity handleExternalEventActivity24;
        private HandleExternalEventActivity handleExternalEventActivity22;
        private HandleExternalEventActivity handleExternalEventActivity19;
        private HandleExternalEventActivity handleExternalEventActivity17;
        private EventDrivenActivity onRejectedBySales;
        private EventDrivenActivity onSalesApproval;
        private EventDrivenActivity onRejectedByFinantial;
        private EventDrivenActivity onFinantialApproval;
        private IfElseBranchActivity elseSalesRejected;
        private IfElseBranchActivity ifSalesApproved;
        private IfElseActivity ifElseActivity7;
        private SetStateActivity setStateActivity26;
        private SetStateActivity setStateActivity19;
        private HandleExternalEventActivity handleExternalEventActivity18;
        private EventDrivenActivity onRejectedByTop;
        private SetStateActivity setStateActivity30;
        private SetStateActivity setStateActivity29;
        private SetStateActivity setStateActivity27;
        private SetStateActivity setStateActivity28;
        private HandleExternalEventActivity handleExternalEventActivity4;
        private SetStateActivity setStateActivity24;
        private HandleExternalEventActivity handleExternalEventActivity7;
        private HandleExternalEventActivity handleExternalEventActivity5;
        private EventDrivenActivity onRejectedByManager;
        private EventDrivenActivity onValidateByManager;
        private StateActivity ManagerApproval;
        private SetStateActivity setStateActivity8;
        private SetStateActivity setStateActivity7;
        private IfElseBranchActivity elseValidationErrors_Accepted;
        private IfElseBranchActivity ifValidationOK_Accepted;
        private IfElseActivity ifElseActivity2;
        private CodeActivity callRuleSetValidation;
        private StateInitializationActivity init_RuleSetValidation;
        private SetStateActivity setStateActivity23;
        private SetStateActivity setStateActivity11;
        private IfElseBranchActivity elseValidationErrors2;
        private IfElseBranchActivity ifValidationOK2;
        private IfElseActivity ifElseActivity3;
        private CodeActivity callRuleSetValidation2;
        private SetStateActivity setStateActivity6;
        private HandleExternalEventActivity handleExternalEventActivity23;
        private StateActivity Printed;
        private EventDrivenActivity onPrintSRA;
        private EventDrivenActivity onSendToConcessionaire;
        private EventDrivenActivity onUpdateSystemPrinted;
        private SetStateActivity setStateActivity1;
        private SetStateActivity setStateActivity2;
        private HandleExternalEventActivity handleExternalEventActivity2;
        private HandleExternalEventActivity handleOnPrintSRA;
        private HandleExternalEventActivity handleSendToConcessionaire;
        private StateActivity Accepted;
        private StateActivity Received;
        private StateActivity InApproval;
        private EventDrivenActivity onValidateBySupervisor;
        private HandleExternalEventActivity handleExternalEventActivity6;
        private SetStateActivity setStateActivity10;
        private HandleExternalEventActivity handleExternalEventActivity8;
        private EventDrivenActivity onReviewSRA;
        private EventDrivenActivity onSalesReviewSRA;
        private EventDrivenActivity onUpdateSystemReceived;
        private StateActivity ErrorVerification;
        private SetStateActivity setStateActivity12;
        private HandleExternalEventActivity handleExternalEventActivity9;
        private SetStateActivity setStateActivity9;
        private StateActivity Rejected;
        private StateActivity Approved;
        private StateActivity TopApproval;
        private EventDrivenActivity onTopApproval;
        private EventDrivenActivity onRejectedBySupervisor;
        private SetStateActivity setStateActivity14;
        private HandleExternalEventActivity handleExternalEventActivity12;
        private SetStateActivity InApprovalRejected;
        private HandleExternalEventActivity handleExternalEventActivity14;
        private CodeActivity getAutoRenewalInfo;
        private StateInitializationActivity init_CheckIsRenewal;
        private StateActivity Started;
        private SetStateActivity setStateActivity22;
        private HandleExternalEventActivity handleExternalEventActivity21;
        private EventDrivenActivity onAcceptRenewal;
        private StateActivity RenewalDraft;
        private EventDrivenActivity onRefuseRenewal;
        private StateActivity Draft;














































































































































































































































































































































































































































































































































































































































































































































































































































































    }
}
