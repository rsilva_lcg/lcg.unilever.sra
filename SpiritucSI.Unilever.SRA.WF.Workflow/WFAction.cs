﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities;

namespace SpiritucSI.Unilever.SRA.WF
{
    [Serializable]
    public class WFAction
    {
        public ExternalDataEventArgs Args { get; set; }
        public Delegate Deleg { get; set; }
        public WFEvents Servs { get; set; }
        public string Text { get; set; }
        public Guid Id { get; set; }
        public bool Visibility { get; set; }
        public string Custom { get; set; }
    }
}
