﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
using System.Workflow.Runtime.Hosting;
using System.Configuration;
using SpiritucSI.Unilever.SRA.Business.Budget;
using System.Data;
using System.Diagnostics;
using System.Collections.Generic;

namespace SpiritucSI.Unilever.SRA.WF
{
    public sealed partial class WF_SRA : System.Workflow.Activities.StateMachineWorkflowActivity
    {
        bool salesApproved;     //entra no estado FinantialApproval
        bool isAutoRenewal;     //
        bool isRuleSetValidated;//
        bool isRuleSetValidatedReceived;//
        bool firstTime;         //for DEBUGGING purpuses ONLY!!!
        private string originalState = "";

        public WF_SRA()
        {
            InitializeComponent();
            firstTime = true;
        }


        public static DependencyProperty WFinstanceIdProperty = DependencyProperty.Register("WFinstanceId", typeof(System.Guid), typeof(WF_SRA));

        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Visible)]
        [BrowsableAttribute(true)]
        [CategoryAttribute("Parameters")]
        public System.Guid WFinstanceId
        {
            get
            {
                if (this.CurrentStateName != null && !this.CurrentStateName.Equals(CompletedStateName))
                {
                    Guid wfID = (System.Guid)base.GetValue(WF_SRA.WFinstanceIdProperty);
                    if (wfID.Equals(Guid.Empty) && WorkflowInstanceId != null)
                        return WorkflowInstanceId;
                    else
                        return (System.Guid)base.GetValue(WF_SRA.WFinstanceIdProperty);
                }
                return Guid.Empty;
            }
            set { base.SetValue(WF_SRA.WFinstanceIdProperty, value); }
        }


        public static DependencyProperty WFFaultProperty = DependencyProperty.Register("WFFault", typeof(System.Exception), typeof(WF_SRA));

        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Visible)]
        [BrowsableAttribute(true)]
        [CategoryAttribute("Handlers")]
        public WFException WFFault
        {
            get
            {
                return ((WFException)(base.GetValue(WFFaultProperty)));
            }
            set
            {
                base.SetValue(WFFaultProperty, value);
            }
        }

        //private void codeActivity2_ExecuteCode(object sender, EventArgs e)
        //{
        //    //throw new WFException("ERRO: XPTO", new Exception("ERRO: XPTO"));
        //    throw new Exception("ERRO: XPTO", new Exception("ERRO: XPTO"));
        //}


        private void SetFaultForHandlers(object sender, EventArgs e)
        {
            if (sender is Activity && (((Activity)sender).Parent) is FaultHandlerActivity)
                this.WFFault = new WFException(((FaultHandlerActivity)((Activity)sender).Parent).Fault.Message, ((FaultHandlerActivity)((Activity)sender).Parent).Fault);
        }

        #region PRIVATE METHODS

        public string OriginalState { get { return originalState; } set { originalState = value; } }

        protected bool IsAuthorized()
        {
            try
            {
                SRADBInterface.SRA DBsra = new SRADBInterface.SRA(ConfigurationSettings.AppSettings["DBCS"].ToString());
                return DBsra.IsContratoAuthorized(this.WorkflowInstanceId);
            }
            catch (Exception ex)
            {
                throw new WFException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Declaração dos métodos disponíveis dentro do workflow
        /// Estes métodos podem servir para:
        ///     - lançar eventos que são depois apanhados no código onde se trata da execução do workflow
        ///     - executar uma série de outras acções (ex: consultar uma BD ou enviar um email)
        /// </summary>
        private void RuleSetValidation(object sender, EventArgs e)
        {
            //isRuleSetValidated = true; //PASSA DIRECTAMENTE PARA INAPPROVAL, SEM PASSAR PELO RECEIVED (SMI)
            isRuleSetValidated = false; //VaI PARA RECEIVED (SMI)
        }

        private void RuleSetValidationReceived(object sender, EventArgs e)
        {
            //isRuleSetValidated = true; //SE SEMPRE FALSE, NÃO SAIA DO ESTADO RECEIVED
            isRuleSetValidatedReceived = true;
        }

        private void TerminateContrato(object sender, EventArgs e)
        {
            try
            {
                SRADBInterface.SRA DBsra = new SRADBInterface.SRA(ConfigurationSettings.AppSettings["DBCS"].ToString());
                DataTable contrato = DBsra.getContrato("(INSTANCE_ID) = '" + this.WorkflowInstanceId + "'");
                if (contrato.Rows.Count > 0)
                {
                    int ID_Contrato = Convert.ToInt32(contrato.Rows[0]["ID_CONTRATO"]);
                    //DBsra.UpdateContratoStatus(ID_Contrato, "T", "REJECTED BY WORKFLOW", DateTime.Now, "", WF_Auxiliar.GetCurrentUsername());
                    string msg = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " PROCESSO TERMINADO" + "\n";
                    //DBsra.AppendContratoObsMotivo(((InstanceStateChangeEventArgs)e).InstanceID, msg);
                    DBsra.UpdateContratoStatus(ID_Contrato, "T", contrato.Rows[0]["OBSERVACOES_MOTIVO"].ToString(), DateTime.Now, "", WF_Auxiliar.GetCurrentUsername());
                }
            }
            catch (Exception ex)
            {
                throw new WFException(ex.Message, ex);
            }
        }

        private void SendEmailtoConcessionaire(object sender, EventArgs e)
        {
            try
            {
                SRADBInterface.SRA DBsra = new SRADBInterface.SRA(ConfigurationSettings.AppSettings["DBCS"].ToString());
                DataTable contrato = DBsra.getContrato("(INSTANCE_ID) = '" + this.WorkflowInstanceId + "'");
                if (contrato.Rows.Count > 0)
                {
                    int ID_Contrato = Convert.ToInt32(contrato.Rows[0]["ID_CONTRATO"]);
                    DBsra.SendMail(ID_Contrato);
                }
            }
            catch (Exception ex)
            {
                throw new WFException(ex.Message, ex);
            }
        }

        private void getAutoRenewalInfoFromDB(object sender, EventArgs e)
        {
            try
            {
                SRADBInterface.SRA a = new SRADBInterface.SRA(ConfigurationSettings.AppSettings["DBCS"].ToString());
                isAutoRenewal = a.IsContratoAutoRenewal(this.WorkflowInstanceId);
            }
            catch (Exception ex)
            {
                throw new WFException(ex.Message, ex);
            }
        }
        private void SetSalesApproved(object sender, EventArgs e)
        {
            salesApproved = true;
        }

        private void SetSalesRejected(object sender, EventArgs e)
        {
            salesApproved = false;
        }

        //private void RaiseWorkflowException(object sender, EventArgs e)
        //{
        //    FaultHandlerActivity faultHandler = ((Activity)sender).Parent as FaultHandlerActivity;
        //    //WF_SRA.WorkflowException(faultHandler.Fault);
        //    WorkflowManager wfManager = new WorkflowManager();
        //    wfManager.SendOnWorkflowException(this.WorkflowInstanceId, faultHandler.Fault);
        //}

        //private void RaiseInstanceStateChange(object sender, EventArgs e)
        //{
        //    Activity c = (Activity)sender;
        //    while (c != null && c.GetType() != typeof(StateActivity))
        //    {
        //        c = c.Parent;
        //    }

        //    string stateName = c.Name;
        //    //WF_SRA.InstanceStateChange(this.WorkflowInstanceId, (StateActivity)c);
        //    WorkflowManager wfManager = new WorkflowManager();
        //    wfManager.SendInstanceStateChange(this.WorkflowInstanceId, stateName);
        //}

        //private void RaiseProceedExeption(object sender, EventArgs e)
        //{
        //    throw new WFProceedConditions("Action will proceed with reservations EXCOD_01");
        //}

        /// <summary>
        /// Declaração de um método para aplicar a lógicas de condição (IfElse, ConditionedActivityGroup, etc)
        /// A resposta deve ser enviado na propriedade Result do argumento e
        /// </summary>
        private void CanProcessProceed(object sender, ConditionalEventArgs e)
        {
            ////e.Result = true;
            //WorkflowConditionsBPN b = new WorkflowConditionsBPN();
            //e.Result = b.canProcessProceed(this.WorkflowInstanceId);
        }

        private void CanProcessBackward(object sender, ConditionalEventArgs e)
        {
            ////e.Result = true;
            //WorkflowConditionsBPN b = new WorkflowConditionsBPN();
            //e.Result = b.canProcessBackward(this.WorkflowInstanceId);
        }

        private void Cativate(object sender, EventArgs e)
        {
            try
            {
                Activity c = (Activity)sender;
                while (c != null && c.GetType() != typeof(StateActivity))
                {
                    c = c.Parent;
                }
                string state = c.Name.ToUpper();

                SRADBInterface.SRA a = new SRADBInterface.SRA(ConfigurationSettings.AppSettings["DBCS"].ToString());
                DataSet contrato = a.getContratoBy_WF_ID(this.WorkflowInstanceId);
                DataTable contratoWeight = a.getContratos_LE_By_Weight(Convert.ToInt32(contrato.Tables["CONTRATO"].Rows[0]["ID_CONTRATO"]));
                Budget bllBudget = new Budget(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
                double Duration = 1;        //ContratoTime
                double InvestInicial = 0;   //Total
                double ValorPrimAno = 0;    //TotalPrev
                double InvestimentoTotal = 0;
                bllBudget.GetInterfaceValues(contrato, out Duration, out ValorPrimAno, out InvestInicial, out InvestimentoTotal);
                double ValorTotal = Duration * ValorPrimAno;

                foreach (DataRow row in contratoWeight.Rows)
                {
                    bllBudget.UpdateBudgetAndCapture(row["COD_CONCESSIONARIO"].ToString(), Convert.ToDouble(row["WEIGHT"]) * InvestInicial);
                }
            }
            catch (Exception ex)
            {
                throw new WFException(ex.Message, ex);
            }
        }

        private void RemoveBudget(object sender, EventArgs e)
        {
            try
            {
                Activity c = (Activity)sender;
                while (c != null && c.GetType() != typeof(StateActivity))
                {
                    c = c.Parent;
                }
                string state = c.Name.ToUpper();

                SRADBInterface.SRA a = new SRADBInterface.SRA(ConfigurationSettings.AppSettings["DBCS"].ToString());
                DataSet contrato = a.getContratoBy_WF_ID(this.WorkflowInstanceId);
                DataTable contratoWeight = a.getContratos_LE_By_Weight(Convert.ToInt32(contrato.Tables["CONTRATO"].Rows[0]["ID_CONTRATO"]));
                Budget bllBudget = new Budget(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
                double Duration = 1;        //ContratoTime
                double InvestInicial = 0;   //Total
                double ValorPrimAno = 0;    //TotalPrev
                double InvestimentoTotal = 0;
                bllBudget.GetInterfaceValues(contrato, out Duration, out ValorPrimAno, out InvestInicial, out InvestimentoTotal);
                double ValorTotal = Duration * ValorPrimAno;

                if (IsAuthorized())
                {
                    foreach (DataRow row in contratoWeight.Rows)
                    {
                        bllBudget.UpdateBudgetAndAuthorize(row["COD_CONCESSIONARIO"].ToString(), -Convert.ToDouble(row["WEIGHT"]) * InvestInicial);
                        bllBudget.UpdateBudgetAndCapture(row["COD_CONCESSIONARIO"].ToString(), -Convert.ToDouble(row["WEIGHT"]) * InvestInicial);
                    }
                    a.UpdateContrato_IS_AUTHORIZED(this.WorkflowInstanceId, false);
                }
                else
                {
                    foreach (DataRow row in contratoWeight.Rows)
                    {
                        bllBudget.UpdateBudgetAndCapture(row["COD_CONCESSIONARIO"].ToString(), -Convert.ToDouble(row["WEIGHT"]) * InvestInicial);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new WFException(ex.Message, ex);
            }
        }

        private void ValidateAndAuthorize(object sender, EventArgs e)
        {
            try
            {
                Activity c = (Activity)sender;
                while (c != null && c.GetType() != typeof(StateActivity))
                {
                    c = c.Parent;
                }
                string state = c.Name.ToUpper();
                //if ((!state.Equals("STARTED")) && (!state.Equals("DRAFT")) && (!state.Equals("RENEWALDRAFT"))
                //    && (!state.Equals("DELEGATED")) && (!state.Equals("PRINTED")) && (!state.Equals("RECEIVED"))
                //    && (!state.Equals("INAPPROVAL")))
                //{
                //((HandleExternalEventActivity) ((EventDrivenActivity) ((StateActivity) c).Activities[1]).EventActivity).Roles
                SRADBInterface.SRA a = new SRADBInterface.SRA(ConfigurationSettings.AppSettings["DBCS"].ToString());
                DataSet contrato = a.getContratoBy_WF_ID(this.WorkflowInstanceId);
                DataTable contratoWeight = a.getContratos_LE_By_Weight(Convert.ToInt32(contrato.Tables["CONTRATO"].Rows[0]["ID_CONTRATO"]));
                Budget bllBudget = new Budget(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
                double Duration = 1;        //ContratoTime
                double InvestInicial = 0;   //Total
                double ValorPrimAno = 0;    //TotalPrev
                double InvestimentoTotal = 0;
                bllBudget.GetInterfaceValues(contrato, out Duration, out ValorPrimAno, out InvestInicial, out InvestimentoTotal);
                double ValorTotal = Duration * ValorPrimAno;
                bool budgetOk = true;
                string username = WF_Auxiliar.GetCurrentUsername();

                //if (state.Equals("ERRORVERIFICATION"))
                //{
                //}
                //else
                //{
                if (!IsAuthorized())
                {

                    //foreach (DataRow row in contratoWeight.Rows)
                    //{
                    //    if (!bllBudget.BudgetCheckByZone(row["COD_CONCESSIONARIO"].ToString(), Convert.ToDouble(row["WEIGHT"]) * InvestInicial))
                    //    {
                    //        budgetOk = false;
                    //        break;
                    //    }
                    //}
                    //if (budgetOk)
                    if (bllBudget.BudgetCheck(WF_Auxiliar.GetCurrentUsername(), InvestInicial))
                    {

                        foreach (DataRow row in contratoWeight.Rows)
                        {
                            if (!bllBudget.BudgetCheckByZone(row["COD_CONCESSIONARIO"].ToString(), Convert.ToDouble(row["WEIGHT"]) * InvestInicial))
                            {
                                budgetOk = false;
                                break;
                            }
                        }


                        if (
                            budgetOk && bllBudget.AuthorizationCheck(username, Duration, InvestInicial, ValorPrimAno, ValorTotal, InvestimentoTotal))
                        {
                            foreach (DataRow row in contratoWeight.Rows)
                            {
                                bllBudget.UpdateBudgetAndAuthorize(row["COD_CONCESSIONARIO"].ToString(), Convert.ToDouble(row["WEIGHT"]) * InvestInicial);
                            }
                            a.UpdateContrato_IS_AUTHORIZED(this.WorkflowInstanceId, true);
                        }
                    }
                }
                //}
                //}
            }
            catch (Exception ex)
            {
                throw new WFException(ex.Message, ex);
            }
        }
        #endregion

        #region EVENTS HANDLING
        /// <summary>
        /// Declaração dos event handlers do workflow 
        /// Posteriormente, no código onde se tratar da execução deste workflow
        ///  estes eventos devem ser registados para que possam ser apanhados caso sejam lançados
        /// </summary>
        //public static event EventHandler<ExceptionEventArgs> OnWorkflowException;
        //public static void WorkflowException(Exception e)
        //{
        //    if (OnWorkflowException != null)
        //        OnWorkflowException(null, new ExceptionEventArgs(e));
        //}

        //public static event EventHandler<InstanceStateChangeEventArgs> OnInstanceStateChange;
        //public static void InstanceStateChange(Guid InstanceID, StateActivity State)
        //{
        //    if (OnInstanceStateChange != null)
        //        OnInstanceStateChange(null, new InstanceStateChangeEventArgs(InstanceID, State));
        //}

        #region PUBLIC METHODS
        //public Guid StartWorkflow(WorkflowRuntime wfr)
        //{
        //    Guid uid = Guid.Empty;

        //    try
        //    {
        //        WorkflowInstance wfInstance = wfr.CreateWorkflow(typeof(WF_SRA));
        //        uid = wfInstance.InstanceId;

        //        ManualWorkflowSchedulerService manualWorkflowSchedulerService = wfr.GetService<ManualWorkflowSchedulerService>();

        //        wfInstance.Start();
        //        manualWorkflowSchedulerService.RunWorkflow(wfInstance.InstanceId);

        //        wfInstance.Unload();
        //    }
        //    catch (Exception ex)
        //    {
        //        WF_SRA.WorkflowException(ex);
        //    }

        //    return uid;
        //}

        //public Guid StartWorkflow(WorkflowRuntime wfr, string originalStateStr)
        //{
        //    Guid uid = Guid.Empty;
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();

        //    parameters.Add("OriginalState", originalStateStr);

        //    try
        //    {
        //        WorkflowInstance wfInstance = wfr.CreateWorkflow(typeof(WF_SRA), parameters);
        //        uid = wfInstance.InstanceId;

        //        ManualWorkflowSchedulerService manualWorkflowSchedulerService = wfr.GetService<ManualWorkflowSchedulerService>();

        //        wfInstance.Start();
        //        manualWorkflowSchedulerService.RunWorkflow(wfInstance.InstanceId);

        //        wfInstance.Unload();
        //    }
        //    catch (Exception ex)
        //    {
        //        WF_SRA.WorkflowException(ex);
        //    }

        //    return uid;
        //}
        #endregion

        private void handleExternalEventActivity7_Invoked(object sender, ExternalDataEventArgs e)
        {

        }

        #endregion

        #region ROLE COLLECTIONS
        /// <summary>
        /// Declaração da(s) WorkflowRoleCollection(s) 
        /// A estas WorkflowRoleCollections são adicionados WebWorkflowRoles que representam 
        ///  cada Role existente na aplicação.
        /// Posteriormente em Design podem configurar-se os HandleExternalEventActivity para 
        ///  que só possam ser executados pelos Roles presentes numa dada WorkflowRoleCollection
        /// </summary>
        //private WorkflowRoleCollection WrcRole1 = new WorkflowRoleCollection();
        //public WorkflowRoleCollection Role1
        //{
        //    get
        //    {
        //        // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
        //        //  de uma qualquer classe do business
        //        this.WrcRole1.Add(new WebWorkflowRole("NOME-DO-ROLE"));
        //        return this.WrcRole1;
        //    }
        //}

        private WorkflowRoleCollection WrcProcessCreatorGroup = new WorkflowRoleCollection();
        public WorkflowRoleCollection ProcessCreatorGroup
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcProcessCreatorGroup.Add(new WebWorkflowRole("SalesRep"));
                this.WrcProcessCreatorGroup.Add(new WebWorkflowRole("Supervisor"));
                this.WrcProcessCreatorGroup.Add(new WebWorkflowRole("RegManager"));

                return this.WrcProcessCreatorGroup;
            }
        }

        private WorkflowRoleCollection WrcSalesRep = new WorkflowRoleCollection();
        public WorkflowRoleCollection SalesRep
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSalesRep.Add(new WebWorkflowRole("SalesRep"));

                return this.WrcSalesRep;
            }
        }

        private WorkflowRoleCollection WrcSMI = new WorkflowRoleCollection();
        public WorkflowRoleCollection SMI
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSMI.Add(new WebWorkflowRole("SMI"));

                return this.WrcSMI;
            }
        }

        private WorkflowRoleCollection WrcSupervisor = new WorkflowRoleCollection();
        public WorkflowRoleCollection Supervisor
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSupervisor.Add(new WebWorkflowRole("Supervisor"));

                return this.WrcSupervisor;
            }
        }

        private WorkflowRoleCollection WrcRegManager = new WorkflowRoleCollection();
        public WorkflowRoleCollection RegManager
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcRegManager.Add(new WebWorkflowRole("RegManager"));

                return this.WrcRegManager;
            }
        }

        private WorkflowRoleCollection WrcSalesDirector = new WorkflowRoleCollection();
        public WorkflowRoleCollection SalesDirector
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSalesDirector.Add(new WebWorkflowRole("SalesDirector"));

                return this.WrcSalesDirector;
            }
        }

        private WorkflowRoleCollection WrcFinantialDirector = new WorkflowRoleCollection();
        public WorkflowRoleCollection FinantialDirector
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcFinantialDirector.Add(new WebWorkflowRole("FinantialDirector"));

                return this.WrcFinantialDirector;
            }
        }

        private WorkflowRoleCollection WrcManagingDirector = new WorkflowRoleCollection();
        public WorkflowRoleCollection ManagingDirector
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcManagingDirector.Add(new WebWorkflowRole("ManagingDirector"));

                return this.WrcManagingDirector;
            }
        }
        #endregion

        private void handleExternalEventActivity21_Invoked(object sender, ExternalDataEventArgs e)
        {

        }

        private void callExternalMethodActivity25_MethodInvoking(object sender, EventArgs e)
        {

        }


    }
}
