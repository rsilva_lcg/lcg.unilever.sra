﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace SpiritucSI.Unilever.SRA.WF
{
    public sealed partial class WF_SRA 
	{            
        #region ROLE COLLECTIONS
        /// <summary>
        /// Declaração da(s) WorkflowRoleCollection(s) 
        /// A estas WorkflowRoleCollections são adicionados WebWorkflowRoles que representam 
        ///  cada Role existente na aplicação.
        /// Posteriormente em Design podem configurar-se os HandleExternalEventActivity para 
        ///  que só possam ser executados pelos Roles presentes numa dada WorkflowRoleCollection
        /// </summary>
        //private WorkflowRoleCollection WrcRole1 = new WorkflowRoleCollection();
        //public WorkflowRoleCollection Role1
        //{
        //    get
        //    {
        //        // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
        //        //  de uma qualquer classe do business
        //        this.WrcRole1.Add(new WebWorkflowRole("NOME-DO-ROLE"));
        //        return this.WrcRole1;
        //    }
        //}

        private WorkflowRoleCollection WrcProcessCreatorGroup = new WorkflowRoleCollection();
        public WorkflowRoleCollection ProcessCreatorGroup
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcProcessCreatorGroup.Add(new WebWorkflowRole("SalesRep"));
                this.WrcProcessCreatorGroup.Add(new WebWorkflowRole("Supervisor"));
                this.WrcProcessCreatorGroup.Add(new WebWorkflowRole("RegManager"));

                return this.WrcProcessCreatorGroup;
            }
        }

        private WorkflowRoleCollection WrcSalesRep = new WorkflowRoleCollection();
        public WorkflowRoleCollection SalesRep
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSalesRep.Add(new WebWorkflowRole("SalesRep"));

                return this.WrcSalesRep;
            }
        }

        private WorkflowRoleCollection WrcSMI = new WorkflowRoleCollection();
        public WorkflowRoleCollection SMI
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSMI.Add(new WebWorkflowRole("SMI"));

                return this.WrcSMI;
            }
        }

        private WorkflowRoleCollection WrcSupervisor = new WorkflowRoleCollection();
        public WorkflowRoleCollection Supervisor
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSupervisor.Add(new WebWorkflowRole("Supervisor"));

                return this.WrcSupervisor;
            }
        }

        private WorkflowRoleCollection WrcRegManager = new WorkflowRoleCollection();
        public WorkflowRoleCollection RegManager
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcRegManager.Add(new WebWorkflowRole("RegManager"));

                return this.WrcRegManager;
            }
        }

        private WorkflowRoleCollection WrcSalesDirector = new WorkflowRoleCollection();
        public WorkflowRoleCollection SalesDirector
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSalesDirector.Add(new WebWorkflowRole("SalesDirector"));

                return this.WrcSalesDirector;
            }
        }

        private WorkflowRoleCollection WrcFinantialDirector = new WorkflowRoleCollection();
        public WorkflowRoleCollection FinantialDirector
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcFinantialDirector.Add(new WebWorkflowRole("FinantialDirector"));

                return this.WrcFinantialDirector;
            }
        }

        private WorkflowRoleCollection WrcManagingDirector = new WorkflowRoleCollection();
        public WorkflowRoleCollection ManagingDirector
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcManagingDirector.Add(new WebWorkflowRole("ManagingDirector"));

                return this.WrcManagingDirector;
            }
        }
        #endregion
    }
}
