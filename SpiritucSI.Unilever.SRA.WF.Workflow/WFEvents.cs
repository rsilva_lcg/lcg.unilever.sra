﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities;

namespace SpiritucSI.Unilever.SRA.WF
{
    /// <summary>
    /// Implementação do interface abaixo com a lista de eventos disponíveis para o workflow
    /// </summary>
    [Serializable]
    public class WFEvents : IWFEvents
    {
        #region IWFEvents Members
        public event EventHandler<WFEventsArgs> OnSendToConcessionaire;
        public void SendToConcessionaire(Guid instanceId)
        {
            if (OnSendToConcessionaire != null)
                OnSendToConcessionaire(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnPrintSRA;
        public void PrintSRA(Guid instanceId)
        {
            if (OnPrintSRA != null)
                OnPrintSRA(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnRefuseDraft;
        public void RefuseDraft(Guid instanceId)
        {
            if (OnRefuseDraft != null)
                OnRefuseDraft(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnAcceptRenewal;
        public void AcceptRenewal(Guid instanceId)
        {
            if (OnAcceptRenewal != null)
                OnAcceptRenewal(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnRefuseRenewal;
        public void RefuseRenewal(Guid instanceId)
        {
            if (OnRefuseRenewal != null)
                OnRefuseRenewal(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnUpdateSystemPrinted;
        public void UpdateSystemPrinted(Guid instanceId)
        {
            if (OnUpdateSystemPrinted != null)
                OnUpdateSystemPrinted(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnDelegatedUpdateSystem;
        public void DelegatedUpdateSystem(Guid instanceId)
        {
            if (OnDelegatedUpdateSystem != null)
                OnDelegatedUpdateSystem(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnUpdateSystemReceived;
        public void UpdateSystemReceived(Guid instanceId)
        {
            if (OnUpdateSystemReceived != null)
                OnUpdateSystemReceived(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnSalesReviewSRA;
        public void SalesReviewSRA(Guid instanceId)
        {
            if (OnSalesReviewSRA != null)
                OnSalesReviewSRA(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnReviewSRA;
        public void ReviewSRA(Guid instanceId)
        {
            if (OnReviewSRA != null)
                OnReviewSRA(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnValidateBySupervisor;
        public void ValidateBySupervisor(Guid instanceId)
        {
            if (OnValidateBySupervisor != null)
                OnValidateBySupervisor(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnRejectedBySupervisor;
        public void RejectedBySupervisor(Guid instanceId)
        {
            if (OnRejectedBySupervisor != null)
                OnRejectedBySupervisor(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnValidateByManager;
        public void ValidateByManager(Guid instanceId)
        {
            if (OnValidateByManager != null)
                OnValidateByManager(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnRejectedByManager;
        public void RejectedByManager(Guid instanceId)
        {
            if (OnRejectedByManager != null)
                OnRejectedByManager(null, new WFEventsArgs(instanceId));
        }






        public event EventHandler<WFEventsArgs> OnFinantialApproval;
        public void FinantialApproval(Guid instanceId)
        {
            if (OnFinantialApproval != null)
                OnFinantialApproval(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnSalesApproval;
        public void SalesApproval(Guid instanceId)
        {
            if (OnSalesApproval != null)
                OnSalesApproval(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnRejectedBySales;
        public void RejectedBySales(Guid instanceId)
        {
            if (OnRejectedBySales != null)
                OnRejectedBySales(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnRejectedByFinantial;
        public void RejectedByFinantial(Guid instanceId)
        {
            if (OnRejectedByFinantial != null)
                OnRejectedByFinantial(null, new WFEventsArgs(instanceId));
        }



        public event EventHandler<WFEventsArgs> OnTopApproval;
        public void TopApproval(Guid instanceId)
        {
            if (OnTopApproval != null)
                OnTopApproval(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnApproved;
        public void Approved(Guid instanceId)
        {
            if (OnApproved != null)
                OnApproved(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnRejectedByTop;
        public void RejectedByTop(Guid instanceId)
        {
            if (OnRejectedByTop != null)
                OnRejectedByTop(null, new WFEventsArgs(instanceId));
        }

        public event EventHandler<WFEventsArgs> OnRejected;
        public void Rejected(Guid instanceId)
        {
            if (OnRejected != null)
                OnRejected(null, new WFEventsArgs(instanceId));
        }
        #endregion
    }
}
