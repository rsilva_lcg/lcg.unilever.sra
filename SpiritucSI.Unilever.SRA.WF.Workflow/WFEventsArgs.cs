﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities;

namespace SpiritucSI.Unilever.SRA.WF
{
    /// <summary>
    /// Declaração dos argumentos de eventos do workflow (WFEvents)
    /// </summary>
    [Serializable]
    public class WFEventsArgs : ExternalDataEventArgs
    {
        public WFEventsArgs(Guid instanceId)
            : base(instanceId)
        {
        }
    }
    /// <summary>
    /// Declaração dos argumentos de eventos de excepções do workflow
    /// </summary>
    //public class ExceptionEventArgs : EventArgs
    //{
    //    public ExceptionEventArgs(Exception e)
    //    {
    //        this.Exception = e;
    //    }

    //    public Exception Exception { get; set; }
    //}

    [Serializable]
    public class ExceptionEventArgs : ExternalDataEventArgs
    {
        public ExceptionEventArgs(Guid InstanceID, Exception e)
            : base(InstanceID)
        {
            this.InstanceID = InstanceID;
            this.Exception = e;
        }

        public Guid InstanceID { get; set; }
        public Exception Exception { get; set; }
    }
    /// <summary>
    /// Declaração dos argumentos do evento de mudança de estado do workflow
    /// </summary>
    /// 

    [Serializable]
    public class InstanceStateChangeEventArgs : ExternalDataEventArgs
    {
        public InstanceStateChangeEventArgs(Guid InstanceID, string State)
            : base(InstanceID)
        {
            this.InstanceID = InstanceID;
            this.State = State;
        }


        public Guid InstanceID { get; set; }
        public string State { get; set; }
    }



    //public class InstanceStateChangeEventArgs : EventArgs
    //{
    //    public InstanceStateChangeEventArgs(Guid InstanceID, StateActivity State, params object[] nome)
    //    {
    //        this.InstanceID = InstanceID;
    //        this.State = State;
    //    }

    //    public Guid InstanceID { get; set; }
    //    public StateActivity State { get; set; }
    //}

}
