﻿
using System;
using System.Workflow.Activities;
using System.Collections.Generic;

namespace SpiritucSI.Unilever.SRA.WF
{
    [ExternalDataExchange]
    public interface IWorkflowManager
    {
        event EventHandler<EventArgs> WorkflowException;
        event EventHandler<EventArgs> InstanceStateChange;
        //event EventHandler<EventArgs> InstanceTerminated;
        //void SendInstanceTerminated(Guid InstanceID, string State);
        void SendInstanceStateChange(Guid InstanceID, string State);
        void SendOnWorkflowException(Guid InstanceID, Exception e);
        void SendProceedException(Guid InstanceID, Exception e);
        void SendBlockException(Guid InstanceID, Exception e);
        Guid StartWorkflow(System.Workflow.Runtime.WorkflowRuntime wfRuntime);
        string[] WFGetEvents();
        
    }

    #region WORKFLOW CLASS ACTION
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class WorkflowAction 
    {
        public ExternalDataEventArgs Args { get; set; }
        public Delegate Deleg { get; set; }
        public WFEvents Servs { get; set; }
        public string Text { get; set; }
        public Guid Id { get; set; }
        public bool Visibility { get; set; }
        public string Custom { get; set; }
    }
    #endregion



}
