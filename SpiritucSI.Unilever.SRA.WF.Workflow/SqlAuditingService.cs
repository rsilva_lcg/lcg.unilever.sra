﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Runtime.Tracking;
using System.Workflow.ComponentModel;
using System.Web;
using System.Threading;

namespace SpiritucSI.Unilever.SRA.WF
{
    public class SqlAuditingService : TrackingService
    {
        #region FIELDS
        private TrackingProfile _defaultProfile;
        private string _Connection = "";
        #endregion


        public SqlAuditingService(string connectionString)
            : base()
        {
            this._Connection = connectionString;
            _defaultProfile = BuildDefaultProfile();
        }

        protected override TrackingChannel GetTrackingChannel(TrackingParameters parameters)
        {
            try
            {
                return new SqlAuditingChannel();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        protected override TrackingProfile GetProfile(Guid workflowInstanceId)
        {
            return _defaultProfile;
        }
        protected override TrackingProfile GetProfile(Type workflowType, Version profileVersionId)
        {
            return _defaultProfile;
        }
        protected override bool TryGetProfile(Type workflowType, out TrackingProfile profile)
        {
            profile = _defaultProfile;
            return true;
        }
        protected override bool TryReloadProfile(Type workflowType, Guid workflowInstanceId, out TrackingProfile profile)
        {
            profile = null;
            return false;
        }
        private TrackingProfile BuildDefaultProfile()
        {
            try
            {
                TrackingProfile profile = new TrackingProfile();
                WorkflowTrackPoint workflowPoint = new WorkflowTrackPoint();

                List<TrackingWorkflowEvent> workflowEvents = new List<TrackingWorkflowEvent>();
                workflowEvents.AddRange(Enum.GetValues(typeof(TrackingWorkflowEvent)) as IEnumerable<TrackingWorkflowEvent>);
                WorkflowTrackingLocation workflowLocation = new WorkflowTrackingLocation(workflowEvents);
                workflowPoint.MatchingLocation = workflowLocation;
                profile.WorkflowTrackPoints.Add(workflowPoint);

                ActivityTrackPoint activityPoint = new ActivityTrackPoint();
                List<ActivityExecutionStatus> activityStatus = new List<ActivityExecutionStatus>();
                activityStatus.AddRange(Enum.GetValues(typeof(ActivityExecutionStatus)) as IEnumerable<ActivityExecutionStatus>);
                ActivityTrackingLocation activityLocation = new ActivityTrackingLocation(typeof(Activity), true, activityStatus);
                activityPoint.MatchingLocations.Add(activityLocation);
                profile.ActivityTrackPoints.Add(activityPoint);

                UserTrackPoint userPoint = new UserTrackPoint();
                UserTrackingLocation userLocation = new UserTrackingLocation(typeof(Object), typeof(Activity));
                userLocation.MatchDerivedActivityTypes = true;
                userLocation.MatchDerivedArgumentTypes = true;
                userPoint.MatchingLocations.Add(userLocation);
                profile.UserTrackPoints.Add(userPoint);

                profile.Version = new Version("3.0.0.0");
                return profile;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }

    public class SqlAuditingChannel : TrackingChannel
    {
        protected override void InstanceCompletedOrTerminated()
        {
            return;
        }
        protected override void Send(TrackingRecord record)
        {
            try
            {
                WorkflowAudit WA = new WorkflowAudit();

                if (record is WorkflowTrackingRecord)
                {
                }
                else if (record is ActivityTrackingRecord)
                {
                    ActivityTrackingRecord actRecord = record as ActivityTrackingRecord;
                    WA.Insert(actRecord.ContextGuid, record.EventOrder, WF_Auxiliar.GetCurrentUsername(), record.EventDateTime);
                }
                else if (record is UserTrackingRecord)
                {
                    UserTrackingRecord userRecord = record as UserTrackingRecord;
                    WA.Insert(userRecord.ContextGuid, record.EventOrder, WF_Auxiliar.GetCurrentUsername(), record.EventDateTime);
                }
            }
            catch (Exception ex)
            {
                throw new WFInvalidActionException("Não foi possível registar a acção de workflow", ex);
            }
        }
    }
}
