﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities;
using System.Workflow.Runtime;
using System.Workflow.Runtime.Hosting;
using System.Workflow.ComponentModel;

namespace SpiritucSI.Unilever.SRA.WF
{
    [Serializable]
	public class WorkflowManager : IWorkflowManager 
	{
        private Guid _uid;
        public WorkflowManager() { }

        #region MANAGER METHODS

        public Guid StartWorkflow(WorkflowRuntime wfRuntime)
        {
            Guid workflowId = Guid.Empty;

            try
            {
                Dictionary<string, Object> parametros = new Dictionary<string, Object>();
                WorkflowInstance wfInstance = wfRuntime.CreateWorkflow(typeof(WF_SRA));
                workflowId = wfInstance.InstanceId;

                //System.Workflow.ComponentModel.DependencyProperty WFinstanceIdProperty = System.Workflow.ComponentModel.DependencyProperty.FromName("WFinstanceId", typeof(SpiritucSI.ContentManager.Workflow.StateMachine));

                //WorkflowChanges wfChanges = new WorkflowChanges(wfInstance.GetWorkflowDefinition());

                //wfChanges.TransientWorkflow.SetValue(WFinstanceIdProperty, wfInstance.InstanceId);
                //wfChanges.Validate();

                //wfInstance.ApplyWorkflowChanges(wfChanges);
                
                ManualWorkflowSchedulerService manualWorkflowSchedulerService = wfRuntime.GetService<ManualWorkflowSchedulerService>();
                WFEvents servicoWorkflow = wfRuntime.GetService<WFEvents>();
                wfInstance.Start();
                
                manualWorkflowSchedulerService.RunWorkflow(wfInstance.InstanceId);

                wfInstance.Unload();
            }

            catch (Exception ex)
            {
                 SendOnWorkflowException(ex);
            }

            return workflowId;
        }


        public Guid StartWorkflow(WorkflowRuntime wfr, string originalStateStr)
        {
            Guid uid = Guid.Empty;
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("OriginalState", originalStateStr);

            try
            {
                WorkflowInstance wfInstance = wfr.CreateWorkflow(typeof(WF_SRA), parameters);
                uid = wfInstance.InstanceId;

                ManualWorkflowSchedulerService manualWorkflowSchedulerService = wfr.GetService<ManualWorkflowSchedulerService>();

                wfInstance.Start();
                manualWorkflowSchedulerService.RunWorkflow(wfInstance.InstanceId);

                wfInstance.Unload();
            }
            catch (Exception ex)
            {
                this.SendOnWorkflowException(uid, ex);
            }

            return uid;
        }
        
        public string[] WFGetEvents()
        {
            try
            {
                Type type = typeof(WFEvents);
                List<string> li = new List<string>();
                if (type != null)
                {
                    var events = type.GetEvents();
                    foreach (var e in events)
                        li.Add(e.Name);
                }
                return li.ToArray<string>();

            }
            catch (Exception ex)
            {
                SendOnWorkflowException(ex);
                return null;
            }

        }
        #endregion

        #region EVENTS HANDLING
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<EventArgs> WorkflowException;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void SendOnWorkflowException(Guid InstanceID, Exception e)
        {
            if (WorkflowException != null)
                WorkflowException(null, new ExceptionEventArgs(InstanceID, e));
        }
        public void SendOnWorkflowException(Exception e)
        {
            if (WorkflowException != null)
                WorkflowException(null, new ExceptionEventArgs(Guid.Empty, e));
        }

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<EventArgs> InstanceStateChange;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="InstanceID"></param>
        public void SendInstanceStateChange(Guid InstanceID, string State)
        {
            if (InstanceStateChange != null)
                InstanceStateChange(null, new InstanceStateChangeEventArgs(InstanceID, State));
        }

        public void SendProceedException(Guid InstanceID, Exception e)
        {
            if (WorkflowException != null)
                WorkflowException(null, new ExceptionEventArgs(InstanceID, new WFProceedConditions("Action will proceed with reservations EXCOD_01")));
        }

        public void SendBlockException(Guid InstanceID, Exception e)
        {
            if (WorkflowException != null)
                WorkflowException(null, new ExceptionEventArgs(InstanceID, new WFProceedConditions("Cannot proceeed with action EXCOD_02")));
        }


        #endregion

        #region EVENT ARGS
        //public class ExceptionEventArgs : EventArgs
        //{
        //    public ExceptionEventArgs(Exception e)
        //    {
        //        this.Exception = e;
        //    }

        //    public Exception Exception { get; set; }
        //}
        //public class InstanceStateTerminatedEventArgs : ExternalDataEventArgs
        //{
        //    public InstanceStateTerminatedEventArgs(Guid InstanceID, StateActivity State):base(InstanceID)
        //    {
        //        this.InstanceID = InstanceID;
        //        this.State = State;
        //    }

        //    public Guid InstanceID { get; set; }
        //    public StateActivity State { get; set; }
        //}
        #endregion

    }


    //#region WORKFLOW CLASS ACTION
    ///// <summary>
    ///// 
    ///// </summary>
    //[Serializable]
    //public class WorkflowAction : IWorkflowAction
    //{
    //    public ExternalDataEventArgs Args { get; set; }
    //    public Delegate Deleg { get; set; }
    //    public WFEvents Servs { get; set; }
    //    public string Text { get; set; }
    //    public Guid Id { get; set; }
    //    public bool Visibility { get; set; }
    //    public string Custom { get; set; }
    //}
    //#endregion
}




