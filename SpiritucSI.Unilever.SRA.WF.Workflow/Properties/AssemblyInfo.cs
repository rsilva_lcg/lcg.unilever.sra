﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SpiritucSI.Unilever.SRA.WF")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("SpiritucSI.Unilever.SRA.WF")]
[assembly: AssemblyCopyright("Copyright ©  2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using '*'.
[assembly: AssemblyVersion("2.0.0.1")]
[assembly: ComVisible(false)]


//NOTE: When updating the namespaces in the project please add new or update existing the XmlnsDefinitionAttribute
//You can add additional attributes in order to map any additional namespaces you have in the project
//[assembly: System.Workflow.ComponentModel.Serialization.XmlnsDefinition("http://schemas.com/SpiritucSI.Unilever.SRA.WF", "SpiritucSI.Unilever.SRA.WF")]
[assembly: AssemblyFileVersionAttribute("2.0.0.1")]
