﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading;

namespace SpiritucSI.Unilever.SRA.WF
{
	public static class WF_Auxiliar
	{
        public static string GetCurrentUsername()
        {
            string login = string.Empty;
            if (HttpContext.Current != null)
            {
                login = HttpContext.Current.User.Identity.Name;
            }
            else
            {
                login = Thread.CurrentPrincipal.Identity.Name;
            }
            return login;

        }
	}
}
