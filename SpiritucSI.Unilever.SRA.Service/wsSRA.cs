﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SpiritucSI.Unilever.SRA.Business.FileMonitor.Business;
using SpiritucSI.Unilever.SRA.Service.Properties;

namespace SpiritucSI.Unilever.SRA.Service
{
    public partial class wsSRA : ServiceBase
    {
        string[] lstFolders;
        MainFileMonitor mainFileMon;

        public wsSRA()
        {
            InitializeComponent();
        }

        public static void Main()
        {
            //wsSRA service = new wsSRA();
            //service.OnStart(new string[3]);
            //System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] { new wsSRA() };
            ServiceBase.Run(ServicesToRun);
        }

        protected override void OnStart(string[] args)
        {
            //MINUTS
            //service_timer.Interval = Convert.ToInt32(Settings.Default.wsTimeInterval) * 60000;
            service_timer.Interval = 1000;

            //SECONDS
            //service_timer.Interval = Convert.ToInt32(Settings.Default.wsTimeInterval) * 1000;

            Functions.Functions functions = new Functions.Functions();
            functions.MapNetworkDrive(Settings.Default.InputFolders, Settings.Default.NetworkUsers, Settings.Default.NetworkPwds,
                Settings.Default.NetworkLetters, ref lstFolders);

            mainFileMon = new MainFileMonitor();

            string strParams = string.Empty;
            strParams += "OnStart Intervalo: " + service_timer.Interval.ToString() + "ms\n";
            strParams += "Intervalo: " + (Convert.ToInt32(Settings.Default.wsTimeInterval) * 60000).ToString() +"ms\n";
            strParams += "Pastas: " + Settings.Default.InputFolders.ToString() + "\n";
            strParams += "Letras de rede: " + Settings.Default.NetworkLetters.ToString() + "\n";
            strParams += "Conn Oracle: " + System.Configuration.ConfigurationManager.AppSettings["DBCS"] + "\n";
            strParams += "Separador letras (nome ficheiros): " + Settings.Default.FilenameSeparator.ToString() + "\n";
            strParams += "Modo SMTP debug?: " + System.Configuration.ConfigurationManager.AppSettings["SMTP_DEBUG"] + "\n";
            strParams += "Ficheiro log email: " + System.Configuration.ConfigurationManager.AppSettings["MailLog_File"] + "\n";
            strParams += "SMTP Server: " + System.Configuration.ConfigurationManager.AppSettings["SMTP_SERVER"] + "\n";            
            
            EventLog.WriteEntry("wsSRA-CONFIG", strParams, EventLogEntryType.Information);

            //ExecuteCode();
        }

        private void service_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            service_timer.Stop();
            service_timer.Interval = Convert.ToInt32(Settings.Default.wsTimeInterval) * 60000;
            EventLog.WriteEntry("wsSRA", "Tick starting..", EventLogEntryType.Information);
            ExecuteCode();
            EventLog.WriteEntry("wsSRA", "Tick ending :-)", EventLogEntryType.Information);
            service_timer.Start();
        }

        private void ExecuteCode()
        {
            try
            {
                mainFileMon.ProcessAllFiles(lstFolders, Settings.Default.FilenameSeparator);
                mainFileMon.CheckValidated();
                mainFileMon.ContratosRenewal("wsSRA");//antes do Close, por causa do NumProcSeguinte
                mainFileMon.ContratosClose();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("wsSRA", ex.Message, EventLogEntryType.Error);
            }
        }

        protected override void OnStop()
        {
        }
    }
}
