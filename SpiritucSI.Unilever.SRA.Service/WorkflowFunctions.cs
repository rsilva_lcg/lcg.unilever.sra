﻿using System.Workflow.Runtime;
using System.Workflow.Runtime.Hosting;
using System;
using System.Configuration;
using SpiritucSI.Unilever.SRA.WF;

namespace SpiritucSI.Unilever.SRA.Service
{
    public class WorkflowFunctions
    {
        public WorkflowFunctions()
        {
            WFRuntime = new WorkflowRuntime();
            this.StartWorkflow();
        }

        #region WORKFLOW PROPERTIES
        WorkflowRuntime WFRuntime;
        int _newID_Contrato;
        #endregion

        #region WORKFLOW METHODS

        /// <summary>
        /// Serve para correr o workflow sobre uma determinada (uid) instância
        /// </summary>
        /// <param name="uid">guid da instância do workflow</param>
        protected void RunWorkflow(Guid uid)
        {
            ManualWorkflowSchedulerService scheduler = this.WFRuntime.GetService<ManualWorkflowSchedulerService>();
            scheduler.RunWorkflow(uid);
        }
        /// <summary>
        /// Sem parâmetros serve para iniciar uma nova instância do workflow
        /// </summary>
        /// <returns>guid da instância recém criada</returns>
        protected Guid RunWorkflow(int newID_Contrato)
        {
            this._newID_Contrato = newID_Contrato;
            WorkflowManager wfManager = new WorkflowManager();

            Guid newuid = wfManager.StartWorkflow(this.WFRuntime);

            return newuid;
        }
        #endregion
        #region WORKFLOW EVENTS
        /// <summary>
        /// Método executado pelo workflow quando no momento do ManualWorkflowSchedulerService.RunWorkflow(GUID) 
        ///  seja despoletado o evento de mudança de estado
        /// </summary>
        void WF_SRA_OnInstanceStateChange(object sender, InstanceStateChangeEventArgs e)
        {
            //try
            //{
            //    SRA bllSRA = new SRA(ConfigurationManager.AppSettings["DBCS"]);

            //    //PARA SINCRONIZAR EVENTOS, O INSTANCE ID TEM DE SER MANUAL NO ORACLE
            //    if (e.State.Name.ToUpper().Equals("STARTED"))
            //    {
            //        bllSRA.UpdateContratoWF_ID(this._newID_Contrato, e.InstanceID);
            //    }
            //    else
            //    {
            //        bllSRA.UpdateContratoWFState(e.InstanceID, e.State.Name);
            //    }

            //    string msg = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> NOVO ESTADO: " + e.State.Name + "\n";
            //    bllSRA.AppendContratoObsMotivo(e.InstanceID, msg);
            //}
            //catch (Exception ex)
            //{
            //    this.wfException = new Exception("Não foi possível enviar o email", ex);
            //    //ExceptionPolicy.HandleException(ex, "LogOnly");
            //}
        }

        /// <summary>
        /// Método executado pelo workflow quando no momento do ManualWorkflowSchedulerService.RunWorkflow(GUID)
        ///  seja lançada uma excepção
        /// </summary>
        void WF_SRA_OnWorkflowException(object sender, ExceptionEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                //ExceptionPolicy.HandleException(ex, "GlobalPolicy");
            }
        }

        private void StartWorkflow()
        {
            //// Get the connection string from Web.config.
            String connectionString = ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString;

            // Create the workflow runtime.
            System.Workflow.Runtime.WorkflowRuntime workflowRuntime = new System.Workflow.Runtime.WorkflowRuntime("WorkflowRuntime");

            System.Workflow.Runtime.Hosting.SqlWorkflowPersistenceService swps = new System.Workflow.Runtime.Hosting.SqlWorkflowPersistenceService(connectionString);
            workflowRuntime.AddService(swps);

            System.Workflow.Runtime.Tracking.SqlTrackingService sts = new System.Workflow.Runtime.Tracking.SqlTrackingService(connectionString);
            workflowRuntime.AddService(sts);

            System.Workflow.Runtime.Hosting.ManualWorkflowSchedulerService mwss = new System.Workflow.Runtime.Hosting.ManualWorkflowSchedulerService();
            workflowRuntime.AddService(mwss);

            SpiritucSI.Unilever.SRA.WF.SqlAuditingService taas = new SpiritucSI.Unilever.SRA.WF.SqlAuditingService(connectionString);
            workflowRuntime.AddService(taas);

            // Add the communication service.
            System.Workflow.Activities.ExternalDataExchangeService dataService = new System.Workflow.Activities.ExternalDataExchangeService();
            workflowRuntime.AddService(dataService);
            dataService.AddService(new SpiritucSI.Unilever.SRA.WF.WFEvents());

            // Start the workflow runtime.
            // (The workflow runtime starts automatically when workflows are started, but will not start automatically when tracking data is requested)
            workflowRuntime.StartRuntime();
        }

        public void StopWorkflow()
        {
            WFRuntime.StopRuntime();
        }

        #endregion
    }
}