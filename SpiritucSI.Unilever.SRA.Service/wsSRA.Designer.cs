﻿namespace SpiritucSI.Unilever.SRA.Service
{
    partial class wsSRA
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.service_timer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.service_timer)).BeginInit();
            // 
            // service_timer
            // 
            this.service_timer.Enabled = true;
            this.service_timer.Interval = 5000;
            this.service_timer.Elapsed += new System.Timers.ElapsedEventHandler(this.service_timer_Elapsed);
            // 
            // wsSRA
            // 
            this.ServiceName = "wsSRA";
            ((System.ComponentModel.ISupportInitialize)(this.service_timer)).EndInit();

        }

        #endregion

        private System.Timers.Timer service_timer;

    }
}
