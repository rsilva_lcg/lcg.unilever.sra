﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SpiritucSI.Unilever.SRA.Business.FileMonitor")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Unilever")> 
<Assembly: AssemblyProduct("SpiritucSI.Unilever.SRA.Business.FileMonitor")> 
<Assembly: AssemblyCopyright("Copyright © Unilever 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("339ae385-6b3b-4716-9df8-203c6290490b")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
