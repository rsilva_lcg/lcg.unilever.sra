Imports System.IO
Imports System.Collections
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

Namespace Business
    Public Module Functions
        Public Class Functions
            Public Sub Fill_FileList(ByRef FileList As ArrayList, ByVal inputFolder As String)
                Dim files As FileInfo() = (New DirectoryInfo(inputFolder)).GetFiles()

                For Each f As FileInfo In files
                    'If f.Extension.ToUpper().Equals(".DAT") Then
                    'If Directory.Exists(f.FullName.Remove(f.FullName.Length - 4)) Then
                    FileList.Add(f.FullName)
                    'End If
                    'End If
                Next

                files = Nothing
            End Sub

            Public Sub SendFileToDB(ByVal newFileInfo As FileInfo, ByVal idContract As String)
                Dim idFile As Integer
                Dim sraDB As SRADBInterface.SRA = New SRADBInterface.SRA(System.Configuration.ConfigurationManager.AppSettings("DBCS").ToString())
                Dim newFilePath = newFileInfo.FullName.Replace(newFileInfo.Name, "")
                sraDB.SaveContratoFiles(idFile, idContract, newFilePath, "wsSRA", newFileInfo.Name)
                'Dim dt As DataTable = sraDB.getContratoFiles(24802)
                'Dim aaa As Integer = sraDB.delContratoFiles(1)
            End Sub

            Public Sub MapNetworkDrive(ByVal inputFolder As String, ByVal username As String, ByVal password As String, ByVal letter As String, _
                                       ByRef folders As String())

                folders = inputFolder.Split(";")
                Dim users As String() = username.Split(";")
                Dim pwds As String() = password.Split(";")
                Dim letters As String() = letter.Split(";")

                If (folders.Length <> users.Length) Or (folders.Length <> pwds.Length) Or (folders.Length <> letters.Length) Then
                    Throw New Exception("Invalid parameters: InputFolders, NetworkUsers, NetworkPwds.")
                End If

                Dim _netDrive As cNetworkDrive = New cNetworkDrive()
                'CustomLogging.AuditAction(AuditType.DefaultCategory, String.Empty, "INFO: Nr of Mapped Drives - " + _netDrive.MappedDrives.Length.ToString())

                Dim i As Integer
                For i = 0 To folders.Length - 1
                    Try
                        Dim containsDrive As Boolean = False

                        For Each sDrive As String In _netDrive.MappedDrives
                            If (sDrive.ToUpper().StartsWith(letters(i).ToUpper())) Then
                                containsDrive = True
                                Exit For
                            End If
                        Next

                        'CustomLogging.AuditAction(AuditType.DefaultCategory, String.Empty, "INFO: Contain drive at position " + i.ToString() + "? " + containsDrive.ToString())

                        If Not containsDrive Then
                            _netDrive.LocalDrive = letters(i)
                            _netDrive.Persistent = True
                            _netDrive.SaveCredentials = True
                            '_netDrive.Force = True
                            _netDrive.ShareName = folders(i)
                            _netDrive.MapDrive(users(i), pwds(i))
                            '_netDrive.RestoreDrive(_netDrive.LocalDrive)
                        End If
                        '_netDrive.UnMapDrive()
                        'Dim files As FileInfo() = (New DirectoryInfo(My.MySettings.Default.InputFolder)).GetFiles()
                    Catch x As Exception
                        'ON ERROR RESUME NEXT
                    End Try
                Next i
            End Sub

            Public Function GetContractIdFromFilename(ByVal filename As FileInfo, ByVal sepChar As String) As String
                If filename.Name.Contains(sepChar) Then
                    Return filename.Name.Substring(0, filename.Name.IndexOf(sepChar))
                ElseIf filename.Name.Contains(".") Then
                    Return filename.Name.Replace(filename.Extension, "")
                Else
                    Throw New Exception("Nome de ficheiro inv�lido.")
                End If
            End Function

            Public Sub CheckContractExists(ByVal ContractId As String)
                Dim sraDB As SRADBInterface.SRA = New SRADBInterface.SRA(System.Configuration.ConfigurationManager.AppSettings("DBCS").ToString())
                Dim contrato As DataTable = sraDB.getContrato("ID_CONTRATO = " + ContractId)

                If contrato.Rows.Count.Equals(0) Then
                    Throw New Exception("Contracto inexistente no sistema.")
                End If
            End Sub

            Public Sub CopyFile(ByVal filePath As String, ByVal newFile As String)
                File.SetAttributes(filePath, FileAttributes.Normal)
                If (Not File.Exists(newFile)) Then
                    File.Copy(filePath, newFile, False)
                Else
                    Throw New Exception("J� existe um ficheiro com esse nome.")
                End If
            End Sub

            Public Sub ForceDelete(ByVal _file_ As String)
                Try
                    File.SetAttributes(_file_, FileAttributes.Normal)
                Catch ex As Exception
                End Try

                Try
                    File.Delete(_file_)
                Catch ex As Exception
                End Try

                Try
                    If (File.Exists(_file_)) Then
                        Dim pr As Process
                        Dim args As New ProcessStartInfo("cmd.exe")
                        With args
                            .RedirectStandardInput = True
                            .RedirectStandardOutput = True
                            .UseShellExecute = False
                            .WindowStyle = ProcessWindowStyle.Hidden
                            .CreateNoWindow = True
                        End With

                        pr = Process.Start(args)
                        '/// now lets delete all files with a .txt extension from the folder C:\testing\
                        '/// you would obviously use *.bak instead of *.txt here ...
                        pr.StandardInput.WriteLine("del """ & _file_ & """" & Convert.ToChar(13))  '/// chr(13) being Enter
                        pr.CloseMainWindow()
                    End If

                Catch ex As Exception
                    'ON ERROR RESUME NEXT
                End Try
            End Sub
        End Class
    End Module
End Namespace