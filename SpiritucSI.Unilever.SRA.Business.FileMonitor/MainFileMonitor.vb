﻿Imports System.IO
Imports System.Collections
Imports System.Runtime.InteropServices
Imports SpiritucSI.Unilever.SRA.Service

Namespace Business
    Public Class MainFileMonitor
        Dim bllFunctions As Functions.Functions

        Public Sub New()
            bllFunctions = New Functions.Functions()
        End Sub

        Public Sub ProcessAllFiles(ByVal lstFolders As String(), ByVal sepChar As String)
            Dim FileList As ArrayList = New ArrayList()
            Dim filesProcessing As ArrayList = New ArrayList()
            Dim i = -1

            Try
                For Each folder As String In lstFolders
                    FileList.Clear()

                    bllFunctions.Fill_FileList(FileList, folder)

                    For Each file As String In FileList
doevents:
                        Try
                            ProcessFile(file, folder, sepChar)
                        Catch ex As Exception
                            EventLog.WriteEntry("wsSRA", "Error in File " + file + ": " + ex.Message, EventLogEntryType.Error)
                        End Try
                        'para garantir q não há timestamps duplicados
                        System.Threading.Thread.Sleep(1000)
enddoevents:
                    Next
                Next
            Catch ex As Exception
                EventLog.WriteEntry("wsSRA", ex.Message, EventLogEntryType.Error)
            End Try
        End Sub

        Public Sub ProcessFile(ByVal file As String, ByVal folder As String, ByVal sepChar As String)
            Dim dtStamp As String = DateTime.Now.ToString("yyyMMdd") + "_" + DateTime.Now.ToString("HHmmss")

            If (Not folder.LastIndexOf("\").Equals(folder.Length - 1)) Then folder += "\"

            Dim errorFolder As String = folder & "Error\"
            If (Not Directory.Exists(errorFolder)) Then
                Directory.CreateDirectory(errorFolder)
            End If
            Dim processedFolder As String = folder & "Processed\"
            If (Not Directory.Exists(processedFolder)) Then
                Directory.CreateDirectory(processedFolder)
            End If

            Dim newFilename As String = String.Empty
            Dim idContract As String = String.Empty
            Try
                Dim myFileInfo As FileInfo = New FileInfo(file)
                idContract = bllFunctions.GetContractIdFromFilename(myFileInfo, sepChar)
                newFilename = processedFolder + idContract + "_" + dtStamp + myFileInfo.Extension

                bllFunctions.CheckContractExists(idContract)
                bllFunctions.CopyFile(file, newFilename)
                bllFunctions.ForceDelete(file)
                bllFunctions.SendFileToDB(New FileInfo(newFilename), idContract)
            Catch ex As Exception
                newFilename = errorFolder + idContract + "_" + dtStamp + New FileInfo(file).Extension
                EventLog.WriteEntry("wsSRA", ex.Message + ", FILE: " + New FileInfo(newFilename).Name, EventLogEntryType.Error)
                bllFunctions.CopyFile(file, newFilename)
                bllFunctions.ForceDelete(file)
            End Try
        End Sub

        Public Sub DeleteFile(ByVal file As String)
            bllFunctions.ForceDelete(file)
        End Sub

        Public Sub CheckValidated()
            Dim sraDB_serv As SRA_Service = New SRA_Service(System.Configuration.ConfigurationManager.AppSettings("DBCS").ToString())

            sraDB_serv.CheckValidated()
        End Sub

        Public Sub ContratosRenewal(ByVal username As String)
            Dim sraDB_serv As SRA_Service = New SRA_Service(System.Configuration.ConfigurationManager.AppSettings("DBCS").ToString())

            sraDB_serv.ContratosRenewal(username)
        End Sub

        Public Sub ContratosClose()
            Dim sraDB_serv As SRA_Service = New SRA_Service(System.Configuration.ConfigurationManager.AppSettings("DBCS").ToString())

            sraDB_serv.ContratosClose()
        End Sub

    End Class
End Namespace