﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateTimePicker.ascx.cs" Inherits="SpiritucSI.UserControls.GenericControls.DateTimePicker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>
<div style="text-align: left;">
    <!-- CALENDÁRIO -->
    <asp:UpdatePanel ID="ttt" runat="server" RenderMode="Inline">
        <ContentTemplate>
            <div style="position: absolute; z-index: 500;" id="Calendar_box" runat="server">
                <%-- SELECCIONAR MES/ANO --%>
                <div style="position: absolute; background-color: #FFE88C; border: solid 1px #36406b;" id="Year_box"
                    visible="false" runat="server">
                    <table width="160px" style="color: #00008B; background-color: #FFE88C;" cellpadding="0" cellspacing="0">
                        <%-- CABEÇALHO --%>
                        <tr>
                            <td align="center">
                                <asp:ImageButton ID="C_btn_YearDown" runat="server" meta:resourcekey="C_btn_YearDown" OnClick="C_btn_YearDown_Click" />
                            </td>
                            <td style="background-color: White; font-size: 2px;">
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:ImageButton ID="C_btn_Close" runat="server" meta:resourcekey="C_btn_Close" Width="12px" OnClick="C_btn_Close_Click" />
                            </td>
                        </tr>
                        <%-- MESES --%>
                        <tr>
                            <td align="left">
                                <asp:RadioButtonList ID="C_rbl_Year" runat="server" Font-Size="XX-Small" RepeatColumns="1" CellPadding="0"
                                    CellSpacing="0">
                                </asp:RadioButtonList>
                            </td>
                            <td style="font-size: 2px; background-color: White;">
                                &nbsp;
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="C_rbl_Month" runat="server" Font-Size="XX-Small" RepeatColumns="2" CellPadding="0"
                                    CellSpacing="0">
                                    <asp:ListItem Value="1" meta:resourcekey="C_rbl_Month_1"></asp:ListItem>
                                    <asp:ListItem Value="2" meta:resourcekey="C_rbl_Month_2"></asp:ListItem>
                                    <asp:ListItem Value="3" meta:resourcekey="C_rbl_Month_3"></asp:ListItem>
                                    <asp:ListItem Value="4" meta:resourcekey="C_rbl_Month_4"></asp:ListItem>
                                    <asp:ListItem Value="5" meta:resourcekey="C_rbl_Month_5"></asp:ListItem>
                                    <asp:ListItem Value="6" meta:resourcekey="C_rbl_Month_6"></asp:ListItem>
                                    <asp:ListItem Value="7" meta:resourcekey="C_rbl_Month_7"></asp:ListItem>
                                    <asp:ListItem Value="8" meta:resourcekey="C_rbl_Month_8"></asp:ListItem>
                                    <asp:ListItem Value="9" meta:resourcekey="C_rbl_Month_9"></asp:ListItem>
                                    <asp:ListItem Value="10" meta:resourcekey="C_rbl_Month_10"></asp:ListItem>
                                    <asp:ListItem Value="11" meta:resourcekey="C_rbl_Month_11"></asp:ListItem>
                                    <asp:ListItem Value="12" meta:resourcekey="C_rbl_Month_12"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <%-- BOTÕES --%>
                        <tr>
                            <td align="center" valign="top">
                                <asp:ImageButton ID="C_btn_YearUp" runat="server" meta:resourcekey="C_btn_YearUp" OnClick="C_btn_YearUp_Click" />
                            </td>
                            <td style="font-size: 2px; background-color: White;">
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:ImageButton ID="C_btn_Select" runat="server" meta:resourcekey="C_btn_Select" OnClick="C_btn_Select_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <%-- CALENDARIO --%>
                <table width="160px" style="color: #00008B; background-color: #FFFFD5; border: solid 1px #36406b; padding: 2px 2px 2px 2px;"
                    cellpadding="0" cellspacing="0">
                    <%-- CABEÇALHO --%>
                    <tr style="background-color:#FFE88C;">
                        <td align="left">
                            <asp:ImageButton ID="C_btn_PrevMonth" runat="server" meta:resourcekey="C_btn_PrevMonth" OnClick="C_btn_PrevMonth_Click" />
                        </td>
                        <td align="center">
                            <asp:LinkButton ID="C_btn_Mes" runat="server" CssClass="button" meta:resourcekey="C_btn_Mes" OnClick="C_btn_Mes_Click"></asp:LinkButton>
                        </td>
                        <td align="right">
                            <asp:ImageButton ID="C_btn_NextMonth" runat="server" meta:resourcekey="C_btn_NextMonth" OnClick="C_btn_NextMonth_Click" />
                        </td>
                    </tr>
                    <%-- CALENDÁRIO --%>
                    <tr>
                        <td colspan="3">
                            <asp:Calendar ID="C_ui_Calendar" runat="server" OnSelectionChanged="C_ui_Calendar_Select" OnVisibleMonthChanged="C_ui_Calendar_VisibleMonthChanged"
                                OnDayRender="C_ui_Calendar_DayRender"></asp:Calendar>
                        </td>
                    </tr>
                    <%-- TIMER --%>
                    <tr>
                        <td colspan="3" align="right">
                            <table cellpadding="0" cellspacing="0">
                                <tr id="C_pnl_Edit" runat="server">
                                    <td>
                                        <asp:Image ID="c_img" runat="server" ImageUrl="~/img/Clock.ico" AlternateText="" />&nbsp;
                                    </td>
                                    <td id="C_pnl_hour" runat="server" valign="top" align="center">
                                        <asp:TextBox ID="C_txt_hour" runat="server" MaxLength="2" Width="20px" CausesValidation="True"
                                            ValidationGroup="TimePicker" OnTextChanged="C_txt_hour_TextChanged" meta:resourcekey="C_txt_hour"></asp:TextBox>
                                    </td>
                                    <td id="sep1" runat="server" valign="top" style="font-size: larger" align="center">
                                        :
                                    </td>
                                    <td id="c_pnl_Minutes" runat="server" valign="top" align="center">
                                        <asp:TextBox ID="C_txt_Minutes" runat="server" MaxLength="2" Width="20px" OnTextChanged="C_txt_Minutes_TextChanged"
                                            CausesValidation="True" ValidationGroup="TimePicker" meta:resourcekey="C_txt_Minutes"></asp:TextBox>
                                    </td>
                                    <td id="sep2" runat="server" valign="top" style="font-size: larger" align="center">
                                        :
                                    </td>
                                    <td id="C_pnl_Seconds" runat="server" valign="top" align="center">
                                        <asp:TextBox ID="C_txt_Seconds" runat="server" MaxLength="2" Width="20px" OnTextChanged="C_txt_Seconds_TextChanged"
                                            CausesValidation="True" ValidationGroup="TimePicker" meta:resourcekey="C_txt_Seconds"></asp:TextBox>
                                    </td>
                                    <td id="C_pnl_HourMode" runat="server" valign="top">
                                        <asp:LinkButton ID="C_btn_AM" runat="server" Font-Size="5pt" Text="AM" OnClick="C_btn_Mode_Click" meta:resourcekey="C_btn_AM"
                                            CausesValidation="true" ValidationGroup="TimePicker" /><br />
                                        <asp:LinkButton ID="C_btn_PM" runat="server" Font-Size="5pt" Text="PM" OnClick="C_btn_Mode_Click" meta:resourcekey="C_btn_PM"
                                            CausesValidation="true" ValidationGroup="TimePicker" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td align="center">
                                        <AJAX:MaskedEditExtender ID="C_Mask_Hour" InputDirection="RightToLeft" ClearMaskOnLostFocus="False" runat="server"
                                            Mask="99" PromptCharacter="H" TargetControlID="C_txt_hour" MaskType="Number">
                                        </AJAX:MaskedEditExtender>
                                        <asp:RangeValidator ID="C_Val_Hour" runat="server" ControlToValidate="C_txt_hour" Display="Dynamic" ErrorMessage="*"
                                            ToolTip="Valor Inválido" MaximumValue="12" MinimumValue="0" Type="Integer" ValidationGroup="TimePicker"
                                            meta:resourcekey="C_Val_Hour"></asp:RangeValidator>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="center">
                                        <AJAX:MaskedEditExtender ID="C_Mask_Minute" InputDirection="RightToLeft" ClearMaskOnLostFocus="False"
                                            runat="server" Mask="99" PromptCharacter="M" TargetControlID="C_txt_Minutes" MaskType="Number">
                                        </AJAX:MaskedEditExtender>
                                        <asp:RangeValidator ID="C_Val_Minutes" runat="server" ControlToValidate="C_txt_Minutes" Display="Dynamic"
                                            ToolTip="Valor Inválido" ErrorMessage="*" MaximumValue="59" MinimumValue="0" Type="Integer" ValidationGroup="TimePicker"
                                            meta:resourcekey="C_Val_Minutes"></asp:RangeValidator>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="center">
                                        <AJAX:MaskedEditExtender ID="C_Mask_Second" InputDirection="RightToLeft" ClearMaskOnLostFocus="False"
                                            runat="server" Mask="99" PromptCharacter="S" TargetControlID="C_txt_Seconds" MaskType="Number">
                                        </AJAX:MaskedEditExtender>
                                        <asp:RangeValidator ID="C_Val_Seconds" runat="server" ControlToValidate="C_txt_Seconds" ToolTip="Valor Inválido"
                                            Display="Dynamic" ErrorMessage="*" MaximumValue="59" MinimumValue="0" Type="Integer" ValidationGroup="TimePicker"
                                            meta:resourcekey="C_Val_Seconds"></asp:RangeValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%-- HOJE --%>
                    <tr>
                        <td colspan="3" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="C_btn_Clear" runat="server" CssClass="button" meta:resourcekey="C_btn_Clear" OnClick="C_btn_Clear_Click"></asp:LinkButton>
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="C_btn_Today" runat="server" CssClass="button" meta:resourcekey="C_btn_Today" OnClick="C_btn_Today_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- DATA SELECCIONADA -->
    <input type="hidden" id='<%= this.ID %>' value='<%= this.C_txt_Date.Text %>' />
    <!-- DATA APRESENTADA -->
    <table cellpadding="0" cellspacing="0" id="TextBox_box" runat="server">
        <tr>
            <td valign="middle">
                <asp:TextBox ReadOnly="true" ID="C_txt_Date" runat="server" CssClass="textbox" Style="cursor: pointer;"
                    Width="140px"></asp:TextBox>
            </td>
            <td valign="middle">
                &nbsp;<asp:ImageButton ID="C_btn_calendar" runat="server" ImageUrl="~/img/Calendar.ico" OnClick="C_btn_calendar_Click" />
            </td>
        </tr>
    </table>
</div>
