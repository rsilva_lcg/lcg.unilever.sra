using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace SpiritucSI.BaseTypes
{
    /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
    /// <summary>
    /// 
    /// </summary>
    public abstract class SPUserControlBase : System.Web.UI.UserControl
    {
        #region PAGE EVENTS
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        virtual protected void Page_Init(object sender, EventArgs e){ }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        virtual protected void Page_Load(object sender, EventArgs e) 
        { 
            if (IsPostBack) LoadFields(); 
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        virtual protected void Page_PreRender(object sender, EventArgs e)
        {
            SaveFields();
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        virtual protected void Page_UnLoad(object sender, EventArgs e) { }
        #endregion

        #region METHODS
       
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_resourceName"></param>
        /// <returns></returns>
        protected string GetLocalResource(string _resourceName)
        {
            object obj = this.GetLocalResourceObject(_resourceName);
            return obj != null ? obj.ToString() : "";
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="IsValid"></param>
        protected void SetControlValidateState( WebControl control, bool IsValid )
        {
            if (this.Page is SPPageBase)
            {
                ((SPPageBase)this.Page).SetControlValidateState(control, IsValid);
            }
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="IsValid"></param>
        /// <param name="Replace"></param>
        protected void SetControlValidateState(WebControl control, bool IsValid, bool Replace)
        {
            if (this.Page is SPPageBase)
            {
                ((SPPageBase)this.Page).SetControlValidateState(control, IsValid, Replace);
            }
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="IsValid"></param>
        protected void SetControlValidateState( UserControl control, bool IsValid )
        {
            if (this.Page is SPPageBase)
            {
                ((SPPageBase)this.Page).SetControlValidateState(control, IsValid);
            }
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
       /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="IsValid"></param>
        /// <param name="Replace"></param>
        protected void SetControlValidateState(UserControl control, bool IsValid, bool replace)
        {
            if (this.Page is SPPageBase)
            {
                ((SPPageBase)this.Page).SetControlValidateState(control, IsValid,replace);
            }
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        virtual protected void SaveFields() { }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        virtual protected void LoadFields() { }
        #endregion

        #region PROPERTIES
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        public object SessionDataSource
        {
            get { return Session["DataSource"] != null ? Session["DataSource"] : null; }
            set { Session["DataSource"] = value; }
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        public string SortField
        {
            get { return ViewState["SortField"] != null ? ViewState["SortField"].ToString() : ""; }
            set { ViewState["SortField"] = value; }
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        public SortDirection SortDir
        {
            get { return ViewState["SortDirection"] != null ? (SortDirection)ViewState["SortDirection"] : SortDirection.Ascending; }
            set { ViewState["SortDirection"] = value; }
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        public string GetSortExpression()
        {
            if (SortField.Length > 0)
            {
                return SortField + (SortDir == SortDirection.Ascending ? " ASC" : " DESC");
            }
            return "";
        }
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        public string GetSortExpressionChangeDir()
        {
            string s = GetSortExpression();
            this.SortDir = this.SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
            return s;
        }
        
        /// TODO: 99 - PREENCHER INFORMA플O PARA DOCUMENTA플O
        /// <summary>
        /// 
        /// </summary>
        public Control FocusControl
        {
            get 
            {
                if (this.Page is SPPageBase)
                    return ((SPPageBase)this.Page).FocusControl;
                return null;
            }
            set 
            { 
                if (this.Page is SPPageBase)
                    ((SPPageBase)this.Page).FocusControl = value; 
            }
        }
        #endregion
    }
}

