﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Reflection;
using SpiritucSI.BaseTypes;

namespace SpiritucSI.UserControls.GenericControls
{
    /// TODO_HIGH: 99 - Terminar o desenvolvimento para paquetizar
    /// <summary>
    /// 
    /// </summary>

    public partial class ColorPicker : SPUserControlBase
    {
        public enum ColorDisplayMode { Picker, Pallete }

        #region FIELDS
        protected bool _Selectable = false;
        protected ColorDisplayMode _DisplayMode = ColorDisplayMode.Picker;
        #endregion

        #region METHODS
        protected Color Negative( Color c )
        {
            return Color.FromArgb((c.R + 128) % 256, (c.G + 128) % 256, (c.B + 128) % 256);
        }



        //protected void FillYear( int year )
        //{
        //    this.C_rbl_Year.Items.Clear();
        //    for (int i = year - 2; i <= year + 3; ++i)
        //    {
        //        this.C_rbl_Year.Items.Add(new ListItem(i.ToString(), i.ToString()));
        //    }
        //    this.C_rbl_Year.SelectedValue = year.ToString();
        //    this.C_rbl_Year.Attributes["year"] = year.ToString();
        //}
        //public void Clear()
        //{
        //    this.C_ui_Calendar.VisibleDate = DateTime.Now;
        //    this.C_ui_Calendar.SelectedDate = DateTime.Now;
        //    this.C_txt_Date.Text = "";
        //    if (this.SelectionChanged != null)
        //        this.SelectionChanged(this, new EventArgs());
        //}
        //protected override void LoadFields()
        //{
        //    base.LoadFields();
        //    if (this.ViewState[this.ClientID + "_DisplayMode"] != null)
        //    {
        //        this._DisplayMode = (CalendarDisplayMode)this.ViewState[this.ClientID + "_DisplayMode"];
        //    }
        //}
        //protected override void SaveFields()
        //{
        //    base.SaveFields();
        //    this.ViewState[this.ClientID + "_DisplayMode"] = this._DisplayMode;
        //}
        #endregion

        #region PAGE EVENTS
        
        protected override void Page_Load( object sender, EventArgs e )
        {
            base.Page_Load(sender, e);
            this._Selectable = false;

           
               
        }
        protected override void Page_PreRender( object sender, EventArgs e )
        {
            C_Color.Items.Clear();
            Color c = new Color();
            foreach (PropertyInfo p in typeof(Color).GetProperties())
            {
                object o = p.GetValue(c, null);
                if (o.GetType() == typeof(Color))
                {
                    Color cc = (Color)o;
                    ListItem li = new ListItem(cc.Name, cc.Name);

                    li.Attributes["style"] = "Color:"+cc.Name+";Background-Color:" + cc.Name;
                    C_Color.Items.Add(li);
                }
            } 
            if (this.Enabled)
            {
                this.C_Displayer.Attributes["onclick"] = "javascript:document.getElementById('" + this.C_btn_Picker.ClientID + "').click();";
            }

            if (this.DisplayMode == ColorDisplayMode.Picker)
            {
                this.C_Displayer_Box.Visible = true;
                this.C_Picker_box.Style["margin-top"] = this.Height.Value + 2 + "px";
                this.C_Picker_box.Style["position"] = "absolute";
            }
            else
            {
                this.C_Displayer_Box.Visible = false;
                this.C_Picker_box.Style["margin-top"] = "";
                this.C_Picker_box.Style["position"] = "";
            }
            if (this.SelectedColor == Color.Empty)
            {
                this.C_Displayer.Attributes["InnerHtml"] = "Sem Cor";
            }
            if (this.SelectedColor == Color.Transparent)
            {
                this.C_Displayer.Attributes["InnerHtml"] = "Transparente";
            }
            else 
            {
                this.C_Displayer.Attributes["InnerHtml"] = "&nbsp;";
            }
            this.C_Picker_box.Visible = true;
            //this.C_Picker_box.Visible = _Selectable || _DisplayMode == ColorDisplayMode.Pallete;
            base.Page_PreRender(sender, e);
        }
        #endregion

        #region CONTROL EVENTS
        protected void C_btn_Picker_Click( object sender, ImageClickEventArgs e )
        {
            if (!this.C_Picker_box.Visible)
                this.C_Color.SelectedValue = this.C_Displayer.BackColor.Name;
            _Selectable = !this.C_Picker_box.Visible;
        }
        protected void C_Color_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.C_Displayer.BackColor = Color.FromName(this.C_Color.SelectedValue);
            this._Selectable = false;
            if (this.SelectionChanged != null)
                this.SelectionChanged(this, new EventArgs());
        }
        //protected void C_ui_Calendar_VisibleMonthChanged( object sender, MonthChangedEventArgs e )
        //{
        //    this._Selectable = true;
        //}
        //protected void C_btn_NextMonth_Click( object sender, ImageClickEventArgs e )
        //{
        //    this.C_ui_Calendar.VisibleDate = this.C_ui_Calendar.VisibleDate.AddMonths(1);
        //    this._Selectable = true;
        //}
        //protected void C_btn_PrevMonth_Click( object sender, ImageClickEventArgs e )
        //{
        //    this.C_ui_Calendar.VisibleDate = this.C_ui_Calendar.VisibleDate.AddMonths(-1);
        //    this._Selectable = true;
        //}
        //protected void C_btn_Mes_Click( object sender, EventArgs e )
        //{
        //    this.C_rbl_Month.SelectedValue = this.C_ui_Calendar.VisibleDate.Month.ToString();
        //    this.FillYear(this.C_ui_Calendar.VisibleDate.Year);
        //    this.Year_box.Visible = true;
        //    this._Selectable = true;
        //}
        //protected void C_btn_Today_Click( object sender, EventArgs e )
        //{
        //    this.C_ui_Calendar.VisibleDate = DateTime.Now;
        //    this._Selectable = true;
        //}
        //protected void C_btn_Clear_Click( object sender, EventArgs e )
        //{
        //    this.Clear();
        //    this._Selectable = false;
        //}
        //protected void C_btn_Select_Click( object sender, EventArgs e )
        //{
        //    this.Year_box.Visible = false;
        //    this._Selectable = true;
        //    this.C_ui_Calendar.VisibleDate = new DateTime(int.Parse(this.C_rbl_Year.SelectedValue), int.Parse(this.C_rbl_Month.SelectedValue), 1);
        //}
        //protected void C_btn_Close_Click( object sender, EventArgs e )
        //{
        //    this.Year_box.Visible = false;
        //    this._Selectable = true;
        //}
        //protected void C_btn_YearUp_Click( object sender, EventArgs e )
        //{
        //    this.FillYear(int.Parse(this.C_rbl_Year.Attributes["year"]) + 6);
        //    this._Selectable = true;
        //}
        //protected void C_btn_YearDown_Click( object sender, EventArgs e )
        //{
        //    this.FillYear(int.Parse(this.C_rbl_Year.Attributes["year"]) - 6);
        //    this._Selectable = true;
        //}
        #endregion

        #region PROPERTIES
        public Color SelectedColor
        {
            get { return this.C_Displayer.BackColor; }
            set { this.C_Displayer.BackColor= value; }
        }
        public bool Enabled
        {
            get { return this.C_btn_Picker.Enabled; }
            set
            {
                this.C_btn_Picker.Enabled = value;
                this.C_btn_Picker.Visible = value;
            }
        }
        public string ToolTip
        {
            get { return this.C_Displayer.ToolTip; }
            set { this.C_Displayer.ToolTip = value; }
        }
        public ColorDisplayMode DisplayMode
        {
            get { return this._DisplayMode; }
            set { this._DisplayMode = value; }
        }
        public Unit Width
        {
            get { return this.C_Displayer.Width; }
            set 
            { 
                this.C_Displayer_Box.Width = value.ToString();
                this.C_Displayer.Width = value;
            }
        }
        public Unit Height
        {
            get { return this.C_Displayer.Height; }
            set 
            {
                this.C_Displayer_Box.Height = value.ToString();
                this.C_Displayer.Height = value;
            }
        }
        #endregion

        #region REGITERED EVENTS
        public event EventHandler SelectionChanged;
        #endregion

        

    }
}