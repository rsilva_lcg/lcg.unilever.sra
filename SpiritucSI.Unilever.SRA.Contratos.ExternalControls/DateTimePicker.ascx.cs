﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.Drawing;
using SpiritucSI.BaseTypes;

namespace SpiritucSI.UserControls.GenericControls
{
    public partial class DateTimePicker : SPUserControlBase
    {
        public enum CalendarDisplayMode { TextBox, Calendar }
        /// <summary>
        /// 
        /// </summary>
        public enum HourMode { HOUR12, HOUR24 };
        /// <summary>
        /// 
        /// </summary>
        public enum TimeMode { Hours, Minutes, Seconds, HourMinutes, MinutesSeconds, HourMinutesSeconds };


        #region FIELDS
        protected bool _Selectable = false;
        protected List<DateTime> _DisabledDates = new List<DateTime>();
        protected DateTime _MinEnabledDate = DateTime.MinValue;
        protected DateTime _MaxEnabledDate = DateTime.MaxValue;
        protected List<DateTime> _EventDates = new List<DateTime>();
        /// <summary>
        /// 
        /// </summary>
        protected DateTime _Current = DateTime.Now;
        /// <summary>
        /// 
        /// </summary>
        protected HourMode _HourMode = HourMode.HOUR24;
        /// <summary>
        /// 
        /// </summary>
        protected TimeMode _EditMode = TimeMode.HourMinutes;
        /// <summary>
        /// 
        /// </summary>
        protected TimeMode _VisibleMode = TimeMode.HourMinutes;
        protected string C_lbl_time = "";

        #endregion

        #region METHODS
        protected void FillYear( int year )
        {
            this.C_rbl_Year.Items.Clear();
            for (int i = year - 2; i <= year + 3; ++i)
            {
                if (i >= MinEnabledDate.Year && i <= MaxEnabledDate.Year)
                {
                    ListItem li = new ListItem(i.ToString(), i.ToString());
                    if (i == year)
                        li.Selected = true;
                    this.C_rbl_Year.Items.Add(li);
                }
            }
            if (this.C_rbl_Year.SelectedValue.Length == 0)
                this.C_rbl_Year.SelectedIndex = 0;
            this.C_rbl_Year.Attributes["year"] = year.ToString();
        }
        protected void FillTime()
        {
            /// FORMAT
            string format = "";
            switch (this._VisibleMode)
            {
                case TimeMode.Hours:
                    format = "HH";
                    break;
                case TimeMode.HourMinutes:
                    format = "HH:mm";
                    break;
                case TimeMode.HourMinutesSeconds:
                    format = "HH:mm:ss";
                    break;
                case TimeMode.Minutes:
                    format = "mm";
                    break;
                case TimeMode.MinutesSeconds:
                    format = "mm:ss";
                    break;
                case TimeMode.Seconds:
                    format = "ss";
                    break;
            }

            if (this._HourMode == HourMode.HOUR24)
            {
                this.C_pnl_HourMode.Visible = false;
                this.C_txt_hour.Text = this._Current.ToString("HH");
                this.C_Val_Hour.MaximumValue = "23";
                this.C_lbl_time = this._Current.ToString(format);
            }
            else
            {
                this.C_btn_AM.ForeColor = this._Current.Hour > 12 ? Color.FromName("#D0D0D0") : Color.FromName("#16204b");
                this.C_btn_PM.ForeColor = this._Current.Hour < 13 ? Color.FromName("#D0D0D0") : Color.FromName("#16204b");
                this.C_pnl_HourMode.Visible = true;
                this.C_txt_hour.Text = this._Current.ToString("hh");
                this.C_Val_Hour.MaximumValue = "12";
                this.C_lbl_time = this._Current.ToString(format.ToLower()) + (this.C_txt_hour.Visible ? (this._Current.Hour > 12 ? " PM" : " AM") : "");
            }
            this.C_txt_Minutes.Text = this._Current.ToString("mm");
            this.C_txt_Seconds.Text = this._Current.ToString("ss");
        
        }
        public void Clear()
        {
            this.C_ui_Calendar.VisibleDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            this.C_ui_Calendar.SelectedDate = DateTime.Now;
            this.C_txt_Date.Text = "";
            this.sendEvents();
        }
        protected override void LoadFields()
        {
            base.LoadFields();
            if (this.ViewState[this.ClientID + "_DisplayMode"] != null)
            {
                this.DisplayMode = (CalendarDisplayMode)this.ViewState[this.ClientID + "_DisplayMode"];
            }
            if (this.ViewState[this.ClientID + "_DisabledDates"] != null)
            {
                this._DisabledDates = (List<DateTime>)this.ViewState[this.ClientID + "_DisabledDates"];
            }
            if (this.ViewState[this.ClientID + "_MinEnabledDate"] != null)
            {
                this._MinEnabledDate = (DateTime)this.ViewState[this.ClientID + "_MinEnabledDate"];
            }
            if (this.ViewState[this.ClientID + "_MaxEnabledDate"] != null)
            {
                this._MaxEnabledDate = (DateTime)this.ViewState[this.ClientID + "_MaxEnabledDate"];
            }
            if (this.ViewState[this.ClientID + "_EventDates"] != null)
            {
                this._EventDates = (List<DateTime>)this.ViewState[this.ClientID + "_EventDates"];
            }
            if (this.ViewState[this.ClientID + "_Current"] != null)
            {
                this._Current = (DateTime)this.ViewState[this.ClientID + "_Current"];
            }
            if (this.ViewState[this.ClientID + "_HourMode"] != null)
            {
                this._HourMode = (HourMode)this.ViewState[this.ClientID + "_HourMode"];
            }
            if (this.ViewState[this.ClientID + "_EditMode"] != null)
            {
                this._EditMode = (TimeMode)this.ViewState[this.ClientID + "_EditMode"];
            }
            if (this.ViewState[this.ClientID + "_VisibleMode"] != null)
            {
                this._VisibleMode = (TimeMode)this.ViewState[this.ClientID + "_VisibleMode"];
            }
        }
        protected override void SaveFields()
        {
            base.SaveFields();
            this.ViewState[this.ClientID + "_DisplayMode"] = this.DisplayMode;
            this.ViewState[this.ClientID + "_DisabledDates"] = this._DisabledDates;
            this.ViewState[this.ClientID + "_MinEnabledDate"] = this._MinEnabledDate;
            this.ViewState[this.ClientID + "_MaxEnabledDate"] = this._MaxEnabledDate;
            this.ViewState[this.ClientID + "_EventDates"] = this._EventDates;
            this.ViewState[this.ClientID + "_Current"] = this._Current;
            this.ViewState[this.ClientID + "_HourMode"] = this._HourMode;
            this.ViewState[this.ClientID + "_EditMode"] = this._EditMode;
            this.ViewState[this.ClientID + "_VisibleMode"] = this._VisibleMode;
        }
        public void SelectWeekOfDay( DateTime day )
        {
            DateTime d = day.AddDays(1);
            this.C_ui_Calendar.SelectedDates.Clear();
            do
            {
                d = d.AddDays(-1);
                this.C_ui_Calendar.SelectedDates.Add(d);
            } while (d.DayOfWeek.ToString() != this.C_ui_Calendar.FirstDayOfWeek.ToString());
            d = day;
            while (d.DayOfWeek.ToString() != this.C_ui_Calendar.FirstDayOfWeek.ToString())
            {
                this.C_ui_Calendar.SelectedDates.Add(d);
                d = d.AddDays(1);
            }
            this.C_ui_Calendar.VisibleDate = day;
            this.C_ui_Calendar_Select(this.C_ui_Calendar, new EventArgs());
        }
        public void SelectMonthOfDay( DateTime day )
        {
            DateTime d = new DateTime(day.Year, day.Month, 1);
            this.C_ui_Calendar.SelectedDates.Clear();
            while (d.Month == day.Month)
            {
                this.C_ui_Calendar.SelectedDates.Add(d);
                d = d.AddDays(1);
            }
            this.C_ui_Calendar.VisibleDate = day;
            this.C_ui_Calendar_Select(this.C_ui_Calendar, new EventArgs());
        }
        #endregion

        #region PAGE EVENTS
        protected override void Page_Load( object sender, EventArgs e )
        {
            base.Page_Load(sender, e);
            if (this.SelectedDate == DateTime.MinValue && !IsPostBack)
                this.SelectedDateTime = DateTime.Now;
            this._Selectable = false;
            if (this.C_rbl_Year.Attributes["year"] == null)
            {
                this.C_rbl_Year.Attributes["year"] = this.C_ui_Calendar.SelectedDate.Year.ToString();
            }
            this.Page.Validate("TimePicker");            
        }
        protected override void Page_PreRender( object sender, EventArgs e )
        {
            this.C_txt_Date.Attributes["onclick"] = "javascript:document.getElementById('" + this.C_btn_calendar.ClientID + "').click();";
            this.C_ui_Calendar.DataBind();

            if (this.DisplayMode == CalendarDisplayMode.TextBox)
            {
                this.TextBox_box.Visible = true;
                this.Calendar_box.Style["margin-top"] = "20px";
                this.Calendar_box.Style["position"] = "absolute";
            }
            else
            {
                this.TextBox_box.Visible = false;
                this.Calendar_box.Style["margin-top"] = "";
                this.Calendar_box.Style["position"] = "";
            }

            this.C_btn_PrevMonth.Visible = this.C_ui_Calendar.VisibleDate > MinEnabledDate;
            this.C_btn_NextMonth.Visible = this.C_ui_Calendar.VisibleDate.AddMonths(1) < MaxEnabledDate;
            this.C_btn_YearDown.Visible = int.Parse(this.C_rbl_Year.Attributes["year"]) - 3 >= MinEnabledDate.Year;
            this.C_btn_YearUp.Visible = int.Parse(this.C_rbl_Year.Attributes["year"]) + 4 <= MaxEnabledDate.Year;
            this.Calendar_box.Visible = _Selectable || DisplayMode == CalendarDisplayMode.Calendar;
            this.C_btn_Mes.Text = this.C_ui_Calendar.VisibleDate.ToString("MMMM") + " de " + this.C_ui_Calendar.VisibleDate.ToString("yyyy");
            
            switch (this._EditMode)
            {
                case TimeMode.Hours:
                    this._Current = this._Current.Date.AddHours(this._Current.Hour);
                    break;
                case TimeMode.Minutes:
                    this._Current = this._Current.Date.AddMinutes(this._Current.Minute);
                    break;
                case TimeMode.Seconds:
                    this._Current = this._Current.Date.AddSeconds(this._Current.Second);
                    break;
                case TimeMode.HourMinutes:
                    this._Current = this._Current.Date.AddHours(this._Current.Hour).AddMinutes(this._Current.Minute);
                    break;
                case TimeMode.MinutesSeconds:
                    this._Current = this._Current.Date.AddMinutes(this._Current.Minute).AddSeconds(this._Current.Second);
                    break;
                default:
                    break;
            }

            /// HORAS
            this.C_txt_hour.ReadOnly = this._EditMode == TimeMode.Minutes
                || this._EditMode == TimeMode.MinutesSeconds
                || this._EditMode == TimeMode.Seconds;
            if (this.C_txt_hour.ReadOnly)
                this.C_txt_hour.CssClass = "ReadOnly";
            this.C_txt_hour.Visible = this._VisibleMode == TimeMode.HourMinutes
                || this._VisibleMode == TimeMode.HourMinutesSeconds
                || this._VisibleMode == TimeMode.Hours;

            /// MINUTOS
            this.C_txt_Minutes.ReadOnly = this._EditMode == TimeMode.Hours
                || this._EditMode == TimeMode.Seconds;
            if (this.C_txt_Minutes.ReadOnly)
                this.C_txt_Minutes.CssClass = "ReadOnly";
            this.C_txt_Minutes.Visible = this._VisibleMode == TimeMode.HourMinutes
               || this._VisibleMode == TimeMode.HourMinutesSeconds
               || this._VisibleMode == TimeMode.Minutes
               || this._VisibleMode == TimeMode.MinutesSeconds;

            /// SEGUNDOS
            this.C_txt_Seconds.ReadOnly = this._EditMode == TimeMode.HourMinutes
                || this._EditMode == TimeMode.Hours
                || this._EditMode == TimeMode.Minutes;
            if (this.C_txt_Seconds.ReadOnly)
                this.C_txt_Seconds.CssClass = "ReadOnly";
            this.C_txt_Seconds.Visible = this._VisibleMode == TimeMode.HourMinutesSeconds
                            || this._VisibleMode == TimeMode.MinutesSeconds
                            || this._VisibleMode == TimeMode.Seconds;

            this.FillTime();

            this.sep1.Visible = this._VisibleMode == TimeMode.HourMinutes
                || this._VisibleMode == TimeMode.HourMinutesSeconds;
            this.sep2.Visible = this._VisibleMode == TimeMode.HourMinutesSeconds
                || this._VisibleMode == TimeMode.MinutesSeconds;

            if(this.C_txt_Date.Text!= "")
                this.C_txt_Date.Text = this.C_ui_Calendar.SelectedDate.ToString("dd-MM-yyyy") + " - " + this.C_lbl_time;
            base.Page_PreRender(sender, e);
        }
        #endregion

        #region CONTROL EVENTS
        protected void C_btn_calendar_Click( object sender, ImageClickEventArgs e )
        {
            if (!this.Calendar_box.Visible)
                this.C_ui_Calendar.VisibleDate = new DateTime(this.C_ui_Calendar.SelectedDate.Year, this.C_ui_Calendar.SelectedDate.Month, 1);
            _Selectable = !this.Calendar_box.Visible;
            this.Year_box.Visible = false;
        }
        protected void C_ui_Calendar_Select( object sender, EventArgs e )
        {
            var days = (
                from DateTime d in this.C_ui_Calendar.SelectedDates
                where !DisabledDates.Contains(d)
                   && d >= MinEnabledDate
                   && d <= MaxEnabledDate
                select d
            ).ToList();

            if (days.Count == 0)
            {
                if (this.SelectedDate < MinEnabledDate)
                {
                    this.SelectedDate = MinEnabledDate;
                }
                else
                {
                    this.SelectedDate = MaxEnabledDate;
                }
            }
            else
            {
                this.FillTime();
                if (days.Count > 1)
                {
                    this.C_txt_Date.Text = days.First().ToString("(dd")
                        + days.Last().ToString("-dd)-MM-yyyy - " + this.C_lbl_time);
                }
                else
                {
                    this.C_txt_Date.Text = this.C_ui_Calendar.SelectedDate.ToString("dd-MM-yyyy") + " - " + this.C_lbl_time;
                }
                this._Selectable = false;
                this.sendSelectedDateChanged();
                this.sendSelectedDateTimeChanged();
            }
        }
        protected void C_ui_Calendar_VisibleMonthChanged( object sender, MonthChangedEventArgs e )
        {
            this._Selectable = true;
        }
        protected void C_ui_Calendar_DayRender( object sender, DayRenderEventArgs e )
        {
            if (DisabledDates.Contains(e.Day.Date)
                || e.Day.Date < _MinEnabledDate
                || e.Day.Date > _MaxEnabledDate
                || (!OtherMonthDaysEnabled && e.Day.IsOtherMonth))
            {
                e.Day.IsSelectable = false;
                e.Cell.Style["text-decoration"] = "line-through";
                e.Cell.Enabled = false;
            }
            if (!OtherMonthDaysEnabled && e.Day.IsOtherMonth)
                e.Cell.Text = "";
            if (_EventDates.Contains(e.Day.Date))
            {
                e.Cell.Style["font-weight"] = "Bold";
                e.Cell.Style["Font-Size"] = "X-Small";
            }

        }
        protected void C_btn_NextMonth_Click( object sender, ImageClickEventArgs e )
        {
            this.C_ui_Calendar.VisibleDate = this.C_ui_Calendar.VisibleDate.AddMonths(1);
            this._Selectable = true;
        }
        protected void C_btn_PrevMonth_Click( object sender, ImageClickEventArgs e )
        {
            this.C_ui_Calendar.VisibleDate = this.C_ui_Calendar.VisibleDate.AddMonths(-1);
            this._Selectable = true;
        }
        protected void C_btn_Mes_Click( object sender, EventArgs e )
        {
            this.C_rbl_Month.SelectedValue = this.C_ui_Calendar.VisibleDate.Month.ToString();
            this.FillYear(this.C_ui_Calendar.VisibleDate.Year);
            this.Year_box.Visible = true;
            this._Selectable = true;
        }
        protected void C_btn_Today_Click( object sender, EventArgs e )
        {
            this.C_ui_Calendar.VisibleDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            this._Selectable = true;
        }
        protected void C_btn_Clear_Click( object sender, EventArgs e )
        {
            this.Clear();
            this._Selectable = false;
        }
        protected void C_btn_Select_Click( object sender, EventArgs e )
        {
            this.Year_box.Visible = false;
            this._Selectable = true;
            this.C_ui_Calendar.VisibleDate = new DateTime(int.Parse(this.C_rbl_Year.SelectedValue), int.Parse(this.C_rbl_Month.SelectedValue), 1);
        }
        protected void C_btn_Close_Click( object sender, EventArgs e )
        {
            this.Year_box.Visible = false;
            this._Selectable = true;
        }
        protected void C_btn_YearUp_Click( object sender, EventArgs e )
        {
            this.FillYear(int.Parse(this.C_rbl_Year.Attributes["year"]) + 6);
            this._Selectable = true;
        }
        protected void C_btn_YearDown_Click( object sender, EventArgs e )
        {
            this.FillYear(int.Parse(this.C_rbl_Year.Attributes["year"]) - 6);
            this._Selectable = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_btn_Edit_Click( object sender, ImageClickEventArgs e )
        {
            this._Selectable = true;
        }
        protected void C_btn_CloseTime_Click( object sender, ImageClickEventArgs e )
        {
            this.sendSelectedTimeChanged();
            this.sendSelectedDateTimeChanged();
            this._Selectable = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_btn_Mode_Click( object sender, EventArgs e )
        {
            if (this._Current.Hour >= 12)
            {
                this._Current = this._Current.AddHours(-12);
            }
            else
            {
                this._Current = this._Current.AddHours(12);
            }
            this._Selectable = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_txt_hour_TextChanged( object sender, EventArgs e )
        {
            if (this.Page.IsValid)
            {
                int hours = int.Parse(this.C_txt_hour.Text);
                this._Current = this._Current.AddHours(-this._Current.Hour).AddHours(hours);
                this._Selectable = true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_txt_Minutes_TextChanged( object sender, EventArgs e )
        {
            if (this.Page.IsValid)
            {
                int minutes = int.Parse(this.C_txt_Minutes.Text);
                this._Current = this._Current.AddMinutes(-this._Current.Minute).AddMinutes(minutes);
                this._Selectable = true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_txt_Seconds_TextChanged( object sender, EventArgs e )
        {
            if (this.Page.IsValid)
            {
                int Seconds = int.Parse(this.C_txt_Seconds.Text);
                this._Current = this._Current.AddSeconds(-this._Current.Second).AddSeconds(Seconds);
                this._Selectable = true;
            }
        }
        #endregion

        #region PROPERTIES
        public DateTime SelectedDate
        {
            get { return (this.C_txt_Date.Text == "" ? DateTime.MinValue : this.C_ui_Calendar.SelectedDate.Date); }
            set
            {
                this.C_ui_Calendar.VisibleDate = new DateTime(value.Date.Year, value.Date.Month, 1);
                this.C_ui_Calendar.SelectedDate = value.Date;
                //this.C_ui_Calendar.DataBind();
                this.C_txt_Date.Text = value.ToString("dd-MM-yyyy") + " - " + this.C_lbl_time;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public List<DateTime> SelectedDates
        {
            get
            {
                if (this.C_txt_Date.Text == "")
                {
                    return new List<DateTime>();
                }
                return (
                    from DateTime d in this.C_ui_Calendar.SelectedDates
                    where !DisabledDates.Contains(d)
                       && d >= MinEnabledDate
                       && d <= MaxEnabledDate
                    select d
                ).ToList();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public TimeSpan SelectedTime
        {
            get { return (this.C_txt_Date.Text == "" ? DateTime.MinValue.TimeOfDay : this._Current.TimeOfDay); }
            set 
            { 
                this._Current = DateTime.Now.Date.Add(value);
                this.FillTime();
                this.C_txt_Date.Text = this.C_ui_Calendar.SelectedDate.ToString("dd-MM-yyyy") + " - " + this.C_lbl_time;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime SelectedDateTime
        {
            get { return (this.C_txt_Date.Text == "" ? DateTime.MinValue : this.C_ui_Calendar.SelectedDate.Date.Add(this._Current.TimeOfDay)); }
            set
            {
                this.C_ui_Calendar.VisibleDate = new DateTime(value.Date.Year, value.Date.Month, 1);
                this.C_ui_Calendar.SelectedDate = value.Date;
                this._Current = value;
                this.FillTime();
                this.C_txt_Date.Text = value.ToString("dd-MM-yyyy") + " - " + this.C_lbl_time; ;
            }
        }
        public List<DateTime> SelectedDatesTimes
        {
            get
            {
                if (this.C_txt_Date.Text == "")
                {
                    return new List<DateTime>();
                }
                return (
                    from DateTime d in this.C_ui_Calendar.SelectedDates
                    where !DisabledDates.Contains(d)
                       && d >= MinEnabledDate
                       && d <= MaxEnabledDate
                    select d.Date.Add(this._Current.TimeOfDay)
                ).ToList();
            }
        }
        public bool Enabled
        {
            get { return this.C_txt_Date.Enabled; }
            set
            {
                this.C_txt_Date.Enabled = value;
                this.C_ui_Calendar.Enabled = value;
                this.C_btn_calendar.Visible = value;
            }
        }
        public bool AllowClear
        {
            get { return this.C_btn_Clear.Visible; }
            set { this.C_btn_Clear.Visible = value; }
        }
        public string ToolTip
        {
            get { return this.C_txt_Date.ToolTip; }
            set { this.C_txt_Date.ToolTip = value; }
        }
        public CalendarSelectionMode SelectionMode
        {
            set { this.C_ui_Calendar.SelectionMode = value; }
            get { return this.C_ui_Calendar.SelectionMode; }
        }
        public CalendarDisplayMode DisplayMode
        {
            get;
            set;
        }
        public List<DateTime> DisabledDates
        {
            get { return this._DisabledDates; }
            set { this._DisabledDates = value; }
        }
        public List<DateTime> EventDates
        {
            get { return this._EventDates; }
            set { this._EventDates = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime MinEnabledDate
        {
            get { return this._MinEnabledDate; }
            set { this._MinEnabledDate = value.Date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime MaxEnabledDate
        {
            get { return this._MaxEnabledDate; }
            set { this._MaxEnabledDate = value.Date; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(true)]
        public bool OtherMonthDaysEnabled
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public HourMode DisplayHourMode
        {
            get { return this._HourMode; }
            set { this._HourMode = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public TimeMode DisplayEditMode
        {
            get { return this._EditMode; }
            set { this._EditMode = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public TimeMode DisplayVisibleMode
        {
            get { return this._VisibleMode; }
            set { this._VisibleMode = value; }
        }

        #endregion

        #region REGITERED EVENTS
        /// <summary>
        /// 
        /// </summary>
        private void sendEvents()
        {
            this.sendSelectedDateChanged();
            this.sendSelectedDateTimeChanged();
            this.sendSelectedTimeChanged();
        }
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler SelectedDateChanged;
        /// <summary>
        /// 
        /// </summary>
        private void sendSelectedDateChanged()
        {
            if (SelectedDateChanged != null)
                SelectedDateChanged(this, new EventArgs());
        }
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler SelectedTimeChanged;
        /// <summary>
        /// 
        /// </summary>
        private void sendSelectedTimeChanged()
        {
            if (SelectedTimeChanged != null)
                SelectedTimeChanged(this, new EventArgs());
        }
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler SelectedDateTimeChanged;
        /// <summary>
        /// 
        /// </summary>
        private void sendSelectedDateTimeChanged()
        {
            if (SelectedDateTimeChanged != null)
                SelectedDateTimeChanged(this, new EventArgs());
        }
        #endregion

    }
}