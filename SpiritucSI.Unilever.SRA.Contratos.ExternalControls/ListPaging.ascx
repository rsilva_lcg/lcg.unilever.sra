﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListPaging.ascx.cs" Inherits="SpiritucSI.UserControls.GenericControls.ListPaging" %>
<asp:LinkButton ID="C_btn_Paging" runat="server" meta:resourcekey="C_btn_Paging" 
    onclick="C_btn_Paging_Click"></asp:LinkButton>
<asp:LinkButton ID="C_btn_NoPaging" runat="server" meta:resourcekey="C_btn_NoPaging" 
    onclick="C_btn_NoPaging_Click"></asp:LinkButton>