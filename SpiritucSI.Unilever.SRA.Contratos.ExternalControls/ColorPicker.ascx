﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColorPicker.ascx.cs" Inherits="SpiritucSI.UserControls.GenericControls.ColorPicker" %>
<div style="text-align: left;">
    <%-- COLOR PICKER --%>
    <div style="position: absolute; " id="C_Picker_box" runat="server">
        <asp:DropDownList ID="C_Color" runat="server" AutoPostBack="true" onselectedindexchanged="C_Color_SelectedIndexChanged"></asp:DropDownList>
    </div>
    <%-- COLOR DISPLAYER --%>
    <table cellpadding="0" cellspacing="0" id="C_Displayer_Box" height="18px" runat="server" width="50px">
        <tr>
            <td valign="middle" align="right" style="width:99%">
                <asp:Panel id="C_Displayer" runat="server" Width="40px" style="cursor:pointer;" Height="18px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" BackColor="Transparent">&nbsp;</asp:Panel>
            </td>
            <td valign="top">
                &nbsp;<asp:ImageButton ID="C_btn_Picker" runat="server" ImageUrl="~/img/ColorFiller.ico" OnClick="C_btn_Picker_Click" />
            </td>
        </tr>
    </table>
</div>
