﻿using System;
using System.Collections.Generic;
using SpiritucSI.BaseTypes;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpiritucSI.UserControls.GenericControls
{
    public partial class ColumnTextFilter : SPUserControlBase
    {
        #region FIELDS
        protected bool AlwaysVisible = false;
        #endregion

        #region PAGE EVENTS
        protected override void SaveFields()
        {
            base.SaveFields();
            this.ViewState[this.ClientID + "AlwaysVisible"] = AlwaysVisible;
        }
        protected override void LoadFields()
        {
            base.LoadFields();
            if(this.ViewState[this.ClientID + "AlwaysVisible"]!=null)
            {
                AlwaysVisible = (bool)this.ViewState[this.ClientID + "AlwaysVisible"];
            }            
        }
        protected override void Page_PreRender(object sender, EventArgs e)
        {
            base.Page_PreRender(sender, e);
            if (this.Parent.GetType() == typeof(DataControlFieldHeaderCell))
            {
                DataControlFieldHeaderCell cell = (DataControlFieldHeaderCell)this.Parent;
                this.C_V_lbl_Header.Text = cell.ContainingField.HeaderText.Replace(" ", "&nbsp;");
                this.C_H_lbl_Header.Text = cell.ContainingField.HeaderText.Replace(" ", "&nbsp;");
            }

            this.C_H_txt_Filter.Visible = this.C_H_txt_Filter.Visible || AlwaysVisible;
            this.C_V_txt_Filter.Visible = this.C_V_txt_Filter.Visible || AlwaysVisible;

            this.C_V_btn_Filter.Visible = this.C_V_txt_Filter.Text.Length <= 0;
            this.C_H_btn_Filter.Visible = this.C_V_txt_Filter.Text.Length <= 0;
            this.C_V_btn_NoFilter.Visible = this.C_V_txt_Filter.Text.Length > 0;
            this.C_V_btn_NoFilter.Visible = this.C_V_txt_Filter.Text.Length > 0;
        }
        #endregion

        #region CONTROL EVENTS
        /// TODO: 99 - PREENCHER INFORMAÇÃO PARA DOCUMENTAÇÃO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_btn_NoFilter_Click(object sender, EventArgs e)
        {
            try
            {
                this.C_H_txt_Filter.Visible = !this.C_H_txt_Filter.Visible;
                this.C_V_txt_Filter.Visible = !this.C_V_txt_Filter.Visible;
                if (DisplayDirection == Orientation.Vertical && this.C_V_txt_Filter.Visible)
                {
                    FocusControl = this.C_V_txt_Filter;
                }
                if (DisplayDirection == Orientation.Horizontal && this.C_H_txt_Filter.Visible)
                {
                    FocusControl = this.C_H_txt_Filter;
                }               
            }
            catch (Exception ex)
            {
                throw ex;
                ///TODO: 50 - MUDAR O TRATAMENTO DE ERROS ConfigurationMasterLABUS.C_btn_NoFilter_Click
                //string s = " Erro em APP." + this.GetType().Name + ".C_btn_NoFilter_Click()";
                //while (ex != null)
                //{
                //    s +="<br/>"+ ex.Message;
                //    ex = ex.InnerException;
                //}
                //this.ShowMessage(s, MessageType.ErrorBox);
            }
        }
        /// TODO: 99 - PREENCHER INFORMAÇÃO PARA DOCUMENTAÇÃO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_btn_Filter_Click(object sender, EventArgs e)
        {
            try
            {
                this.C_H_txt_Filter.Visible = !this.C_H_txt_Filter.Visible;
                this.C_V_txt_Filter.Visible = !this.C_V_txt_Filter.Visible;
                if (DisplayDirection == Orientation.Vertical && this.C_V_txt_Filter.Visible)
                {
                    FocusControl = this.C_V_txt_Filter;
                }
                if (DisplayDirection == Orientation.Horizontal && this.C_H_txt_Filter.Visible)
                {
                    FocusControl = this.C_H_txt_Filter;
                }          
            }
            catch (Exception ex)
            {
                throw ex;
                ///TODO: 50 - MUDAR O TRATAMENTO DE ERROS ConfigurationMasterLABUS.C_btn_Filter_Click
                //string s = " Erro em APP." + this.GetType().Name + ".C_btn_Filter_Click()";
                //while (ex != null)
                //{
                //    s +="<br/>"+ ex.Message;
                //    ex = ex.InnerException;
                //}
                //this.ShowMessage(s, MessageType.ErrorBox);
            }
        }
        /// TODO: 99 - PREENCHER INFORMAÇÃO PARA DOCUMENTAÇÃO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_txt_Filter_Change(object sender, EventArgs e)
        {
            try
            {
                this.FilterText = ((TextBox)sender).Text;
                this.C_H_txt_Filter.Visible = false;
                this.C_V_txt_Filter.Visible = false;
                if(this.FilterChange!=null)
                    this.FilterChange(this,new CommandEventArgs(this.CommandName,this.CommandArgument));
                if (this.Command != null)
                    this.Command(this, new CommandEventArgs(this.CommandName, this.CommandArgument));
            }
            catch (Exception ex)
            {
                throw ex;
                ///TODO: 50 - MUDAR O TRATAMENTO DE ERROS ConfigurationReceptionMaterials.C_Txt_Filter_TextChanged
                //string s = " Erro em APP." + this.GetType().Name + ".C_Txt_Filter_TextChanged()";
                //while (ex != null)
                //{
                //    s +="<br/>"+ ex.Message;
                //    ex = ex.InnerException;
                //}
                //this.ShowMessage(s, MessageType.ErrorBox);
            }
        }
        #endregion

        #region PROPERTIES
        public Orientation DisplayDirection
        {
            get
            {
                if (this.C_pnl_Vertical.Visible)
                    return Orientation.Vertical;
                else
                    return Orientation.Horizontal;
            }
            set
            {
                this.C_pnl_Vertical.Visible = (value == Orientation.Vertical);
                this.C_pnl_Horizontal.Visible = (value == Orientation.Horizontal);
            }
        }
        public string FilterText
        {
            get
            {
                return this.C_V_txt_Filter.Text;
            }
            set
            {
                this.C_V_txt_Filter.Text = value;
                this.C_H_txt_Filter.Text = value;
            }
        }
        public bool TextBoxAlwaysVisible
        {
            get
            {
                return this.AlwaysVisible;
            }
            set
            {
                this.AlwaysVisible = value;
            }
        }
        public string NoFilterImageUrl
        {
            get 
            {
                return this.C_V_btn_Filter.ImageUrl;
            }
            set 
            {
                this.C_V_btn_Filter.ImageUrl = value;
                this.C_H_btn_Filter.ImageUrl = value;
            }
        }
        public string NoFilterToolTip
        {
            get
            {
                return this.C_V_btn_NoFilter.ToolTip;
            }
            set
            {
                this.C_V_btn_NoFilter.ToolTip = value;
                this.C_H_btn_NoFilter.ToolTip = value;
            }
        }
        public string NoFilterAlternateText
        {
            get
            {
                return this.C_V_btn_NoFilter.AlternateText;
            }
            set
            {
                this.C_V_btn_NoFilter.AlternateText = value;
                this.C_H_btn_NoFilter.AlternateText = value;
            }
        }
        public string FilterImageUrl
        {
            get
            {
                return this.C_V_btn_NoFilter.ImageUrl;
            }
            set
            {
                this.C_V_btn_NoFilter.ImageUrl = value;
                this.C_H_btn_NoFilter.ImageUrl = value;
            }
        }        
        public string FilterToolTip
        {
            get
            {
                return this.C_V_btn_Filter.ToolTip;
            }
            set
            {
                this.C_V_btn_Filter.ToolTip = value;
                this.C_H_btn_Filter.ToolTip = value;
            }
        }
        public string FilterAlternateText
        {
            get
            {
                return this.C_V_btn_Filter.AlternateText;
            }
            set
            {
                this.C_V_btn_Filter.AlternateText = value;
                this.C_H_btn_Filter.AlternateText = value;
            }
        }
        public string CommandName
        {
            get 
            {
                return this.C_V_btn_Filter.Attributes["CN"];
            }
            set
            {
                this.C_V_btn_Filter.Attributes["CN"] = value;
                this.C_H_btn_Filter.Attributes["CN"] = value;
                this.C_V_btn_NoFilter.Attributes["CN"] = value;
                this.C_H_btn_NoFilter.Attributes["CN"] = value;
            }
        }
        public string CommandArgument
        {
            get
            {
                return this.C_V_btn_Filter.Attributes["CA"];
            }
            set
            {
                this.C_V_btn_Filter.Attributes["CA"] = value;
                this.C_H_btn_Filter.Attributes["CA"] = value;
                this.C_V_btn_NoFilter.Attributes["CA"] = value;
                this.C_H_btn_NoFilter.Attributes["CA"] = value;
            }
        }
        public event CommandEventHandler FilterChange;
        public event CommandEventHandler Command;

        #endregion

    }
}