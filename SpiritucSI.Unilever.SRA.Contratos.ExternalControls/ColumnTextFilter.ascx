﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColumnTextFilter.ascx.cs" Inherits="SpiritucSI.UserControls.GenericControls.ColumnTextFilter" %>
<div id="base" runat="server">
    <table id="C_pnl_Vertical" width="100%" runat="server" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="middle">
                <asp:Label ID="C_V_lbl_Header" runat="server"></asp:Label>
            </td>
            <td width="1%" valign="middle" align="right">
                <asp:ImageButton ID="C_V_btn_Filter" runat="server" CausesValidation="false" OnClick="C_btn_Filter_Click" />
                <asp:ImageButton ID="C_V_btn_NoFilter" runat="server" OnClick="C_btn_NoFilter_Click" CausesValidation="false" />
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" colspan="2">
                <asp:TextBox ID="C_V_txt_Filter" runat="server" Width="99%" Visible="false" AutoPostBack="true" OnTextChanged="C_txt_Filter_Change"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table id="C_pnl_Horizontal" width="100%" runat="server" cellpadding="0" cellspacing="0" visible="false">
        <tr>
            <td valign="middle" align="right" width="1%">
                <asp:Label ID="C_H_lbl_Header" runat="server"></asp:Label>
            </td>
            <td align="center" valign="middle" width="98%">
                <asp:TextBox ID="C_H_txt_Filter" runat="server" Width="99%" Visible="false" AutoPostBack="true" OnTextChanged="C_txt_Filter_Change"></asp:TextBox>
            </td>
            <td width="1%" valign="middle" align="right">
                <asp:ImageButton ID="C_H_btn_Filter" runat="server" CausesValidation="false" OnClick="C_btn_Filter_Click" />
                <asp:ImageButton ID="C_H_btn_NoFilter" runat="server" OnClick="C_btn_NoFilter_Click" CausesValidation="false" />
            </td>
        </tr>
    </table>
</div>
