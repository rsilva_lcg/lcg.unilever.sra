﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Web;

namespace SpiritucSI.Unilever.SRA.Business.Budget
{
    public class Budget
    {
        #region FIELDS
        /// <summary>
        /// DataBase ConnectionString To Use In All Commands
        /// </summary>
        protected string ConnectionString;
        #endregion

        #region EVENTS
        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        /// <param name="ConnString">ConnectionString to Source DataBase</param>
        public Budget(string ConnString)
        {
            this.ConnectionString = ConnString;
        }

        #endregion

        #region METHODS
        public bool BudgetCheck(string username, double amount)
        {
            MyMembership mm = new MyMembership(this.ConnectionString);
            DataTable zoneBudget = this.BudgetGet(username);

            return (Convert.ToDouble(zoneBudget.Rows[0]["BudgetAvailable"]) >= amount);
        }

        public bool BudgetCheckByZone(string roleName, double amount)
        {
            MyMembership mm = new MyMembership(this.ConnectionString);
            DataTable zoneBudget = this.BudgetGetByZone(roleName, DateTime.Now.Year);
            double budgetAvailable = Convert.ToDouble(zoneBudget.Rows[0]["BudgetTotal"]) - Convert.ToDouble(zoneBudget.Rows[0]["BudgetCaptured"]) - Convert.ToDouble(zoneBudget.Rows[0]["BudgetAuthorized"]);
            //ALTERADO RRAFAEL 20/07/2011 -> User Profile HARDCODED
            bool IsmanagementApproved = false;
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity != null && mm.GetHighestRoleFromUser(HttpContext.Current.User.Identity.Name) != null && mm.GetHighestRoleFromUser(HttpContext.Current.User.Identity.Name) != "SalesRep")
                //if(HttpContext.Current.User != null && HttpContext.Current.User.Identity != null && (HttpContext.Current.User.IsInRole("SalesDirector") || HttpContext.Current.User.IsInRole("FinantialDirector") || HttpContext.Current.User.IsInRole("ManagingDirector")))
                IsmanagementApproved = true;

            return (budgetAvailable >= amount || IsmanagementApproved);
            //return (budgetAvailable >= amount);
        }

        public DataTable BudgetGet(string username)
        {
            MyMembership mm = new MyMembership(this.ConnectionString);
            DataTable zoneBudget = new DataTable();
            double total = 0, captured = 0, authorized = 0, available = 0;
            List<string> userZones = mm.GetZonesFromUser(username);
            if (!userZones.Count.Equals(0))
            {
                foreach (string zone in mm.GetZonesFromUser(username))
                {
                    zoneBudget = BudgetGetByZone(zone, DateTime.Now.Year);
                    if (zoneBudget.Rows.Count.Equals(0))
                    {
                        throw new Exception("A zona " + zone + " não tem budget associado na BD.");
                    }
                    total += Convert.ToDouble(zoneBudget.Rows[0]["BudgetTotal"]);
                    captured += Convert.ToDouble(zoneBudget.Rows[0]["BudgetCaptured"]);
                    authorized += Convert.ToDouble(zoneBudget.Rows[0]["BudgetAuthorized"]);
                    available += Convert.ToDouble(zoneBudget.Rows[0]["BudgetTotal"]) - Convert.ToDouble(zoneBudget.Rows[0]["BudgetCaptured"]) - Convert.ToDouble(zoneBudget.Rows[0]["BudgetAuthorized"]);
                }

                zoneBudget.Rows[0]["BudgetTotal"] = total;
                zoneBudget.Rows[0]["BudgetCaptured"] = captured;
                zoneBudget.Rows[0]["BudgetAuthorized"] = authorized;
                zoneBudget.Columns.Add("BudgetAvailable", Type.GetType("System.String"));
                zoneBudget.Rows[0]["BudgetAvailable"] = available;
            }
            return zoneBudget;
        }

        public bool AuthorizationCheck(string username, double duration, double invest_Initial,
            double yearOneBilling, double contract_TotalValue, double TotalInvestment)
        {
            MyMembership mm = new MyMembership(this.ConnectionString);
            string highestRoleFromUser = mm.GetHighestRoleFromUser(username);
            if ((highestRoleFromUser.Equals(FunctionalRoleList.FinantialDirector.ToString())) ||
                 (highestRoleFromUser.Equals(FunctionalRoleList.ManagingDirector.ToString())) ||
                 (highestRoleFromUser.Equals(FunctionalRoleList.SalesDirector.ToString())))
            {
                return true;
            }
            else
            {
                DataTable zoneBudget = BudgetMatrixGetByRole(highestRoleFromUser, DateTime.Now.Year);

                //inv. inicial inferior ou igual à facturação prevista para o 1º ano do contrato
                bool initialInvestmentOk = (yearOneBilling >= invest_Initial);

                //duração inferior a X anos
                double roleMaxDuration = Convert.ToDouble(zoneBudget.Rows[0]["ContractDuration"]);
                bool durationOk = (roleMaxDuration >= duration);

                //valor máx. contracto GSV inferior ao valor X
                double roleContractGSV_Max = Convert.ToDouble(zoneBudget.Rows[0]["ContractGSV_Max"]);
                bool maxValueOk = (roleContractGSV_Max >= (TotalInvestment / duration));

                //valor máx.investimento inferior a X% do valor total do contracto
                double roleInvest_MaxPerc = Convert.ToDouble(zoneBudget.Rows[0]["Invest_MaxPerc"]) / 100;
                bool roleInvest_MaxPercOk = (roleInvest_MaxPerc * contract_TotalValue >= TotalInvestment);

                return (initialInvestmentOk && durationOk && maxValueOk && roleInvest_MaxPercOk);
            }
        }

        private DataTable BudgetGetByZone(string roleName, int year)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_Budget_GetByID";
                #endregion

                #region PARAMETROS
                #region RoleName
                SqlParameter pr_role = cm.CreateParameter();
                pr_role.Direction = ParameterDirection.Input;
                pr_role.DbType = DbType.String;
                pr_role.Value = roleName;
                pr_role.ParameterName = "RoleName";
                cm.Parameters.Add(pr_role);
                #endregion

                #region Year
                SqlParameter pr_year = cm.CreateParameter();
                pr_year.Direction = ParameterDirection.Input;
                pr_year.DbType = DbType.Int32;
                pr_year.Value = year;
                pr_year.ParameterName = "Year";
                cm.Parameters.Add(pr_year);
                #endregion

                #endregion

                #region EXECUTAR
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de clientes para o RoleName '" + roleName + "'.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'Clientes.getClientes(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        private DataTable BudgetMatrixGetByRole(string roleName, int year)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_BudgetMatrix_GetByID";
                #endregion

                #region PARAMETROS
                #region RoleName
                SqlParameter pr_role = cm.CreateParameter();
                pr_role.Direction = ParameterDirection.Input;
                pr_role.DbType = DbType.String;
                pr_role.Value = roleName;
                pr_role.ParameterName = "RoleName";
                cm.Parameters.Add(pr_role);
                #endregion

                #region Year
                SqlParameter pr_year = cm.CreateParameter();
                pr_year.Direction = ParameterDirection.Input;
                pr_year.DbType = DbType.Int32;
                pr_year.Value = year;
                pr_year.ParameterName = "Year";
                cm.Parameters.Add(pr_year);
                #endregion

                #endregion

                #region EXECUTAR
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de clientes para o RoleName '" + roleName + "'.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'Clientes.getClientes(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public int UpdateBudgetAndAuthorize(string roleName, double value)
        {
            return UpdateBudget(roleName, DateTime.Now.Year, value, "spSRA_Budget_UpdateAndAuthorize");
        }

        public int UpdateBudgetAndCapture(string roleName, double value)
        {
            return UpdateBudget(roleName, DateTime.Now.Year, value, "spSRA_Budget_UpdateAndCapture");
        }

        private int UpdateBudget(string roleName, int year, double value, string procName)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = procName;
                #endregion

                #region PARAMETROS
                #region RoleName
                SqlParameter pr_role = cm.CreateParameter();
                pr_role.Direction = ParameterDirection.Input;
                pr_role.DbType = DbType.String;
                pr_role.Value = roleName;
                pr_role.ParameterName = "RoleName";
                cm.Parameters.Add(pr_role);
                #endregion

                #region Year
                SqlParameter pr_year = cm.CreateParameter();
                pr_year.Direction = ParameterDirection.Input;
                pr_year.DbType = DbType.Int32;
                pr_year.Value = year;
                pr_year.ParameterName = "Year";
                cm.Parameters.Add(pr_year);
                #endregion

                #region value
                SqlParameter pr_balance = cm.CreateParameter();
                pr_balance.Direction = ParameterDirection.Input;
                pr_balance.DbType = DbType.Double;
                pr_balance.Value = value;
                pr_balance.ParameterName = "value";
                cm.Parameters.Add(pr_balance);
                #endregion

                #region RowCount
                SqlParameter pr_rowcount = cm.CreateParameter();
                pr_rowcount.Direction = ParameterDirection.Output;
                pr_rowcount.DbType = DbType.Int32;
                pr_rowcount.ParameterName = "RowCount";
                cm.Parameters.Add(pr_rowcount);
                #endregion
                #endregion

                #region EXECUTAR
                if (cm.Connection.State.Equals(ConnectionState.Broken) ||
                    cm.Connection.State.Equals(ConnectionState.Closed))
                {
                    cm.Connection.Open();
                }
                cm.ExecuteNonQuery();

                #endregion

                return Convert.ToInt32(pr_rowcount.Value);
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de clientes para o RoleName '" + roleName + "'.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'Clientes.getClientes(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public int UpdateBudgetRAW(int id, double total, double captured, double authorized)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_Budget_Update";
                #endregion

                #region PARAMETROS
                #region id
                SqlParameter pr_role = cm.CreateParameter();
                pr_role.Direction = ParameterDirection.Input;
                pr_role.DbType = DbType.Int32;
                pr_role.Value = id;
                pr_role.ParameterName = "BudgetId";
                cm.Parameters.Add(pr_role);
                #endregion

                #region total
                SqlParameter pr_1 = cm.CreateParameter();
                pr_1.Direction = ParameterDirection.Input;
                pr_1.DbType = DbType.Double;
                pr_1.Value = total;
                pr_1.ParameterName = "total";
                cm.Parameters.Add(pr_1);
                #endregion

                #region captured
                SqlParameter pr_2 = cm.CreateParameter();
                pr_2.Direction = ParameterDirection.Input;
                pr_2.DbType = DbType.Double;
                pr_2.Value = captured;
                pr_2.ParameterName = "captured";
                cm.Parameters.Add(pr_2);
                #endregion

                #region authorized
                SqlParameter pr_3 = cm.CreateParameter();
                pr_3.Direction = ParameterDirection.Input;
                pr_3.DbType = DbType.Double;
                pr_3.Value = authorized;
                pr_3.ParameterName = "authorized";
                cm.Parameters.Add(pr_3);
                #endregion

                #region RowCount
                SqlParameter pr_rowcount = cm.CreateParameter();
                pr_rowcount.Direction = ParameterDirection.Output;
                pr_rowcount.DbType = DbType.Int32;
                pr_rowcount.ParameterName = "RowCount";
                cm.Parameters.Add(pr_rowcount);
                #endregion
                #endregion

                #region EXECUTAR
                if (cm.Connection.State.Equals(ConnectionState.Broken) ||
                    cm.Connection.State.Equals(ConnectionState.Closed))
                {
                    cm.Connection.Open();
                }
                cm.ExecuteNonQuery();

                #endregion

                return Convert.ToInt32(pr_rowcount.Value);
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de clientes para o BudgetId '" + id.ToString() + "'.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'Clientes.getClientes(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public int UpdateBudgetMatrixRAW(int id, double maxPERC, double contMAX, double contDUR)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_BudgetMatrix_Update";
                #endregion

                #region PARAMETROS
                #region id
                SqlParameter pr_role = cm.CreateParameter();
                pr_role.Direction = ParameterDirection.Input;
                pr_role.DbType = DbType.Int32;
                pr_role.Value = id;
                pr_role.ParameterName = "BudgetMatrixId";
                cm.Parameters.Add(pr_role);
                #endregion

                #region maxPERC
                SqlParameter pr_1 = cm.CreateParameter();
                pr_1.Direction = ParameterDirection.Input;
                pr_1.DbType = DbType.Double;
                pr_1.Value = maxPERC;
                pr_1.ParameterName = "Invest_MaxPerc";
                cm.Parameters.Add(pr_1);
                #endregion

                #region contMAX
                SqlParameter pr_2 = cm.CreateParameter();
                pr_2.Direction = ParameterDirection.Input;
                pr_2.DbType = DbType.Double;
                pr_2.Value = contMAX;
                pr_2.ParameterName = "ContractGSV_Max";
                cm.Parameters.Add(pr_2);
                #endregion

                #region contDUR
                SqlParameter pr_3 = cm.CreateParameter();
                pr_3.Direction = ParameterDirection.Input;
                pr_3.DbType = DbType.Double;
                pr_3.Value = contDUR;
                pr_3.ParameterName = "ContractDuration";
                cm.Parameters.Add(pr_3);
                #endregion

                #region RowCount
                SqlParameter pr_rowcount = cm.CreateParameter();
                pr_rowcount.Direction = ParameterDirection.Output;
                pr_rowcount.DbType = DbType.Int32;
                pr_rowcount.ParameterName = "RowCount";
                cm.Parameters.Add(pr_rowcount);
                #endregion
                #endregion

                #region EXECUTAR
                if (cm.Connection.State.Equals(ConnectionState.Broken) ||
                    cm.Connection.State.Equals(ConnectionState.Closed))
                {
                    cm.Connection.Open();
                }
                cm.ExecuteNonQuery();

                #endregion

                return Convert.ToInt32(pr_rowcount.Value);
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de clientes para o BudgetMatrixId '" + id.ToString() + "'.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'Clientes.getClientes(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public int InsertBudget(string role, int year, double total, double captured, double authorized)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_Budget_Insert";
                #endregion

                #region PARAMETROS
                #region role
                SqlParameter pr_role = cm.CreateParameter();
                pr_role.Direction = ParameterDirection.Input;
                pr_role.DbType = DbType.String;
                pr_role.Value = role;
                pr_role.ParameterName = "role";
                cm.Parameters.Add(pr_role);
                #endregion

                #region year
                SqlParameter pr_year = cm.CreateParameter();
                pr_year.Direction = ParameterDirection.Input;
                pr_year.DbType = DbType.Int32;
                pr_year.Value = year;
                pr_year.ParameterName = "year";
                cm.Parameters.Add(pr_year);
                #endregion

                #region total
                SqlParameter pr_1 = cm.CreateParameter();
                pr_1.Direction = ParameterDirection.Input;
                pr_1.DbType = DbType.Double;
                pr_1.Value = total;
                pr_1.ParameterName = "total";
                cm.Parameters.Add(pr_1);
                #endregion

                #region captured
                SqlParameter pr_2 = cm.CreateParameter();
                pr_2.Direction = ParameterDirection.Input;
                pr_2.DbType = DbType.Double;
                pr_2.Value = captured;
                pr_2.ParameterName = "captured";
                cm.Parameters.Add(pr_2);
                #endregion

                #region authorized
                SqlParameter pr_3 = cm.CreateParameter();
                pr_3.Direction = ParameterDirection.Input;
                pr_3.DbType = DbType.Double;
                pr_3.Value = authorized;
                pr_3.ParameterName = "authorized";
                cm.Parameters.Add(pr_3);
                #endregion

                #region RowCount
                SqlParameter pr_rowcount = cm.CreateParameter();
                pr_rowcount.Direction = ParameterDirection.Output;
                pr_rowcount.DbType = DbType.Int32;
                pr_rowcount.ParameterName = "RowCount";
                cm.Parameters.Add(pr_rowcount);
                #endregion
                #endregion

                #region EXECUTAR
                if (cm.Connection.State.Equals(ConnectionState.Broken) ||
                    cm.Connection.State.Equals(ConnectionState.Closed))
                {
                    cm.Connection.Open();
                }
                cm.ExecuteNonQuery();

                #endregion

                return Convert.ToInt32(pr_rowcount.Value);
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de clientes para a zona '" + role + "'.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'Clientes.getClientes(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public int InsertBudgetMatrix(string role, int year, double maxPERC, double contMAX, double contDUR)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_BudgetMatrix_Insert";
                #endregion

                #region PARAMETROS
                #region role
                SqlParameter pr_role = cm.CreateParameter();
                pr_role.Direction = ParameterDirection.Input;
                pr_role.DbType = DbType.String;
                pr_role.Value = role;
                pr_role.ParameterName = "role";
                cm.Parameters.Add(pr_role);
                #endregion

                #region year
                SqlParameter pr_year = cm.CreateParameter();
                pr_year.Direction = ParameterDirection.Input;
                pr_year.DbType = DbType.Int32;
                pr_year.Value = year;
                pr_year.ParameterName = "year";
                cm.Parameters.Add(pr_year);
                #endregion

                #region maxPERC
                SqlParameter pr_1 = cm.CreateParameter();
                pr_1.Direction = ParameterDirection.Input;
                pr_1.DbType = DbType.Double;
                pr_1.Value = maxPERC;
                pr_1.ParameterName = "Invest_MaxPerc";
                cm.Parameters.Add(pr_1);
                #endregion

                #region contMAX
                SqlParameter pr_2 = cm.CreateParameter();
                pr_2.Direction = ParameterDirection.Input;
                pr_2.DbType = DbType.Double;
                pr_2.Value = contMAX;
                pr_2.ParameterName = "ContractGSV_Max";
                cm.Parameters.Add(pr_2);
                #endregion

                #region contDUR
                SqlParameter pr_3 = cm.CreateParameter();
                pr_3.Direction = ParameterDirection.Input;
                pr_3.DbType = DbType.Double;
                pr_3.Value = contDUR;
                pr_3.ParameterName = "ContractDuration";
                cm.Parameters.Add(pr_3);
                #endregion

                #region RowCount
                SqlParameter pr_rowcount = cm.CreateParameter();
                pr_rowcount.Direction = ParameterDirection.Output;
                pr_rowcount.DbType = DbType.Int32;
                pr_rowcount.ParameterName = "RowCount";
                cm.Parameters.Add(pr_rowcount);
                #endregion
                #endregion

                #region EXECUTAR
                if (cm.Connection.State.Equals(ConnectionState.Broken) ||
                    cm.Connection.State.Equals(ConnectionState.Closed))
                {
                    cm.Connection.Open();
                }
                cm.ExecuteNonQuery();

                #endregion

                return Convert.ToInt32(pr_rowcount.Value);
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> role '" + role + "' error: " + exp.Message, exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'Clientes.getClientes(string)'." + exp.Message, exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public int DeleteBudget(int id)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_Budget_DeleteByID";
                #endregion

                #region PARAMETROS
                #region id
                SqlParameter pr_role = cm.CreateParameter();
                pr_role.Direction = ParameterDirection.Input;
                pr_role.DbType = DbType.Int32;
                pr_role.Value = id;
                pr_role.ParameterName = "ID";
                cm.Parameters.Add(pr_role);
                #endregion

                #region RowCount
                SqlParameter pr_rowcount = cm.CreateParameter();
                pr_rowcount.Direction = ParameterDirection.Output;
                pr_rowcount.DbType = DbType.Int32;
                pr_rowcount.ParameterName = "RowCount";
                cm.Parameters.Add(pr_rowcount);
                #endregion
                #endregion

                #region EXECUTAR
                if (cm.Connection.State.Equals(ConnectionState.Broken) ||
                    cm.Connection.State.Equals(ConnectionState.Closed))
                {
                    cm.Connection.Open();
                }
                cm.ExecuteNonQuery();

                #endregion

                return Convert.ToInt32(pr_rowcount.Value);
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de clientes para o BudgetMatrixId '" + id.ToString() + "'.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'Clientes.getClientes(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public int DeleteBudgetMatrix(int id)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_BudgetMatrix_DeleteByID";
                #endregion

                #region PARAMETROS
                #region id
                SqlParameter pr_role = cm.CreateParameter();
                pr_role.Direction = ParameterDirection.Input;
                pr_role.DbType = DbType.Int32;
                pr_role.Value = id;
                pr_role.ParameterName = "ID";
                cm.Parameters.Add(pr_role);
                #endregion
               
                #region RowCount
                SqlParameter pr_rowcount = cm.CreateParameter();
                pr_rowcount.Direction = ParameterDirection.Output;
                pr_rowcount.DbType = DbType.Int32;
                pr_rowcount.ParameterName = "RowCount";
                cm.Parameters.Add(pr_rowcount);
                #endregion
                #endregion

                #region EXECUTAR
                if (cm.Connection.State.Equals(ConnectionState.Broken) ||
                    cm.Connection.State.Equals(ConnectionState.Closed))
                {
                    cm.Connection.Open();
                }
                cm.ExecuteNonQuery();

                #endregion

                return Convert.ToInt32(pr_rowcount.Value);
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de clientes para o BudgetMatrixId '" + id.ToString() + "'.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'Clientes.getClientes(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        #endregion

        #region SPECIAL_FUNCTIONS_FROM_INTERFACE
        public void GetInterfaceValues(DataSet contrato, out double ContratoTime, out double TotalPrevisao, out double Total, out double InvestimentoTotal)
        {
            double TotalPrevisaoGlobal = 0;
            double TotalDescontos = 0;
            double TotalBonus = 0;
            double TotalBonusGlobal = 0;
            double TotalVerba = 0;
            TotalPrevisao = 0;
            ContratoTime = 0;

            #region TEMPO CONTRATO
            //por data atingida
            if (contrato.Tables["CONTRATO"].Rows[0]["QUANTIA_FIM"].ToString().Equals(""))
            {
                DateTime inicio = Convert.ToDateTime(contrato.Tables["CONTRATO"].Rows[0]["DATA_INICIO"]);
                DateTime Fim = Convert.ToDateTime(contrato.Tables["CONTRATO"].Rows[0]["DATA_FIM"]);
                DateTime anos = inicio;// this.Condicoes_txtDataInicio.SelectedDate;
                if (contrato.Tables["CONTRATO"].Rows[0]["ANOS_RENOVACAO"].ToString().Trim().Length > 0)
                {
                    anos = anos.AddYears(Convert.ToInt32(contrato.Tables["CONTRATO"].Rows[0]["ANOS_RENOVACAO"])).AddDays(-1);
                }
                TimeSpan Data = Fim.Subtract(inicio);
                TimeSpan Anos = anos.Subtract(inicio);
                if (Data.TotalDays > Anos.TotalDays)
                    ContratoTime = (double)((Data.TotalDays * 4) / 1461);
                else
                    ContratoTime = (double)((Anos.TotalDays * 4) / 1461);
            }
            //por valor atingido
            else
            {
                foreach (DataRow row in contrato.Tables["CONTRAPARTIDAS"].Rows)
                {
                    TotalPrevisao += Convert.ToDouble(row["PREVISAO_VENDAS"]);
                }
                double limite = Convert.ToDouble(contrato.Tables["CONTRATO"].Rows[0]["QUANTIA_FIM"]);
                ContratoTime = TotalPrevisao == 0 ? 1 : Math.Max(ContratoTime, limite / TotalPrevisao);
            }
            #endregion

            #region BONUS
            TotalPrevisao = 0;
            foreach (DataRow row in contrato.Tables["CONTRAPARTIDAS"].Rows)
            {
                string gama = row["PRODUCT_NAME"].ToString();

                double Previsao = Convert.ToDouble(row["PREVISAO_VENDAS"]);
                double PDesconto = 0;
                if (row["DESCONTO_FACTURA"].ToString().Length > 0)
                    PDesconto = Convert.ToDouble(row["DESCONTO_FACTURA"]);
                double Desconto = Math.Round((Previsao * PDesconto) / 100, 0);
                double filtro = Previsao;
                switch (row["PRAZO_PAGAMENTO"].ToString())
                {
                    case "C":
                        filtro = Math.Round((double)Previsao * ContratoTime, 0);
                        break;
                    case "A":
                        break;
                    case "S":
                        filtro = Math.Round((double)Previsao / 2, 0);
                        break;
                    case "Q":
                        filtro = Math.Round((double)Previsao / 3, 0);
                        break;
                    case "T":
                        filtro = Math.Round((double)Previsao / 4, 0);
                        break;
                    case "B":
                        filtro = Math.Round((double)Previsao / 6, 0);
                        break;
                    case "M":
                        filtro = Math.Round((double)Previsao / 12, 0);
                        break;
                }
                DataRow[] bon = contrato.Tables["BONUS"].Select("ID_CONTRAPARTIDA = " + row["ID_CONTRAPARTIDA"].ToString() + " AND TIPO = 'Acc' AND VALOR_LIMITE_MIN <= " + filtro.ToString(), "VALOR_LIMITE_MIN DESC");
                double Bonus = 0;
                if (bon.Length > 0 && bon[0]["DESCONTO"].ToString().Length > 0)
                {
                    Bonus = Math.Round((Previsao * Convert.ToDouble(bon[0]["DESCONTO"])) / 100, 0);
                }

                TotalPrevisao += Previsao;
                TotalDescontos += Desconto;
                TotalBonus += Bonus;
            }
            #endregion

            #region BONUS GLOBAL
            if (Convert.ToInt32(contrato.Tables["CONTRATO"].Rows[0]["PREVISAO_VENDAS_GLOBAL"]) > 0)
            {
                TotalPrevisaoGlobal = Convert.ToDouble(contrato.Tables["CONTRATO"].Rows[0]["PREVISAO_VENDAS_GLOBAL"]);
                double filtroglobal = TotalPrevisaoGlobal;
                switch (contrato.Tables["CONTRATO"].Rows[0]["PRAZO_PAGAMENTO"].ToString())
                {
                    case "C":
                        filtroglobal = Math.Round(TotalPrevisaoGlobal * ContratoTime, 0);
                        break;
                    case "A":
                        break;
                    case "S":
                        filtroglobal = Math.Round(TotalPrevisaoGlobal / 2, 0);
                        break;
                    case "Q":
                        filtroglobal = Math.Round(TotalPrevisaoGlobal / 3, 0);
                        break;
                    case "T":
                        filtroglobal = Math.Round(TotalPrevisaoGlobal / 4, 0);
                        break;
                    case "B":
                        filtroglobal = Math.Round(TotalPrevisaoGlobal / 6, 0);
                        break;
                    case "M":
                        filtroglobal = Math.Round(TotalPrevisaoGlobal / 12, 0);
                        break;
                }
                DataRow[] bonusGlobal = contrato.Tables["BONUS_GLOBAL"].Select("TIPO = 'Acc' AND VALOR_LIMITE_MIN <= " + filtroglobal.ToString(), "VALOR_LIMITE_MIN DESC");
                if (bonusGlobal.Length > 0)
                {
                    if (bonusGlobal[0]["DESCONTO"].ToString().Length > 0)
                    {
                        TotalBonusGlobal = Math.Round((TotalPrevisaoGlobal * Convert.ToDouble(bonusGlobal[0]["DESCONTO"].ToString())) / 100, 0);
                    }
                }
            }
            #endregion

            #region VERBA
            foreach (DataRow row in contrato.Tables["FASEAMENTO_PAG"].Rows)
            {
                if (Convert.ToDateTime(row["data_pagamento"]).Year.Equals(DateTime.Now.Year))
                {
                    TotalVerba += Convert.ToDouble(row["VALOR_PAGAMENTO"].ToString());
                }
            }
            #endregion

            #region TOTAIS
            double Subt1 = TotalDescontos + TotalBonus;
            double Subt2 = Subt1 + TotalBonusGlobal;
            Total = Subt2 + TotalVerba;
            #endregion

            #region VERBA - NOVO INDICADOR MATRIZ AUTORIZAÇÕES
            InvestimentoTotal = TotalVerba;
            foreach (DataRow row in contrato.Tables["FASEAMENTO_PAG"].Rows)
            {
                if (!Convert.ToDateTime(row["data_pagamento"]).Year.Equals(DateTime.Now.Year))
                {
                    InvestimentoTotal += Convert.ToDouble(row["VALOR_PAGAMENTO"].ToString());
                }
            }
            InvestimentoTotal += (ContratoTime * Subt2);
            #endregion

            ContratoTime = Math.Round(ContratoTime, 2);
        }
        #endregion

        #region DATABASE
        public DataTable RoleBudget_Get()
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_BudgetMatrix_Get";
                #endregion

                #region EXECUTAR
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de spSRA_BudgetMatrix_Get.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'MyMembership.spSRA_BudgetMatrix_Get'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable ZoneBudget_Get()
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_Budget_Get";
                #endregion

                #region EXECUTAR
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de spSRA_Budget_Get.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'MyMembership.spSRA_Budget_Get'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        #endregion
    }
}