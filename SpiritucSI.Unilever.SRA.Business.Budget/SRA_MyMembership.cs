﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;

namespace SpiritucSI.Unilever.SRA.Business.Budget
{
    public class MyMembership
    {
        #region FIELDS
        /// <summary>
        /// DataBase ConnectionString To Use In All Commands
        /// </summary>
        protected string ConnectionString;
        #endregion

        #region EVENTS
        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        /// <param name="ConnString">ConnectionString to Source DataBase</param>
        public MyMembership(string ConnString)
        {
            this.ConnectionString = ConnString;
        }
        #endregion

        #region METHODS

        public string GetUserZonesForQuery(bool IsSMI, bool IsSalesManager, bool IsFinancialManager, bool IsTopManager, System.Security.Principal.IPrincipal user)
        {
            string uRolesQuery = " AND (";
            try
            {
                if (IsSMI || IsSalesManager || IsFinancialManager || IsTopManager)
                {
                    //NÃO FAZ NADA, POIS ASSIM SELECCIONA TODAS AS ZONAS

                    //foreach (DataRow r in m.GetOrganizationalRoles(Roles.GetAllRoles()).Rows)
                    //{
                    //    uRolesQuery += " AND DEFAULT_COD_CONC = " + r[0].ToString();
                    //}
                }
                else
                {
                    foreach (DataRow r in this.GetOrganizationalRoles(Roles.GetAllRoles()).Rows)
                    {
                        if (user.IsInRole(r[0].ToString()))
                        {
                            uRolesQuery += "DEFAULT_COD_CONC = '" + r[0].ToString()+"'";
                            uRolesQuery += " OR ";
                        }
                    }
                }

                if (uRolesQuery == " AND (")
                {
                    return "";
                }
                else
                {
                    uRolesQuery = uRolesQuery.Substring(0, uRolesQuery.LastIndexOf(" OR "));
                    uRolesQuery += ")";
                    return uRolesQuery;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        public List<string> GetZonesFromUser(string username)
        {
            List<string> zones = new List<string>();
            string role = GetHighestRoleFromUser(username);

            if (role == FunctionalRoleList.SystemAdministrator.ToString())
            {
                throw new Exception("O SystemAdministrator não devia ter permissões para aceder à aplicação.");
            }

            if ((role == FunctionalRoleList.ManagingDirector.ToString()) ||
                (role == FunctionalRoleList.FinantialDirector.ToString()) ||
                (role == FunctionalRoleList.SalesDirector.ToString()) ||
                (role == FunctionalRoleList.SMI.ToString()))
            {
                foreach (string zone in Roles.GetAllRoles())
                {
                    if (!IsFunctionalRole(zone))
                    {
                        zones.Add(zone);
                    }
                }
            }
            else
            {
                foreach (string zone in Roles.GetRolesForUser(username))
                {
                    if (!IsFunctionalRole(zone))
                    {
                        zones.Add(zone);
                    }
                }
            }

            return zones;
        }

        public string GetHighestRoleFromUser(string userName)
        {
            if (Roles.IsUserInRole(userName, FunctionalRoleList.ManagingDirector.ToString()))
            {
                return FunctionalRoleList.ManagingDirector.ToString();
            }
            else if (Roles.IsUserInRole(userName, FunctionalRoleList.FinantialDirector.ToString()))
            {
                return FunctionalRoleList.FinantialDirector.ToString();
            }
            else if (Roles.IsUserInRole(userName, FunctionalRoleList.SalesDirector.ToString()))
            {
                return FunctionalRoleList.SalesDirector.ToString();
            }
            else if (Roles.IsUserInRole(userName, FunctionalRoleList.RegManager.ToString()))
            {
                return FunctionalRoleList.RegManager.ToString();
            }
            else if (Roles.IsUserInRole(userName, FunctionalRoleList.Supervisor.ToString()))
            {
                return FunctionalRoleList.Supervisor.ToString();
            }
            else if (Roles.IsUserInRole(userName, FunctionalRoleList.SalesRep.ToString()))
            {
                return FunctionalRoleList.SalesRep.ToString();
            }
            else if (Roles.IsUserInRole(userName, FunctionalRoleList.SMI.ToString()))
            {
                return FunctionalRoleList.SMI.ToString();
            }            

            return string.Empty;
        }

        public string GetRoleUp(string rolename)
        {
            if (rolename == FunctionalRoleList.ManagingDirector.ToString())
            {
                return "IS_TOP";
            }
            else if (rolename == FunctionalRoleList.FinantialDirector.ToString())
            {
                return FunctionalRoleList.ManagingDirector.ToString();
            }
            else if (rolename == FunctionalRoleList.SalesDirector.ToString())
            {
                return FunctionalRoleList.FinantialDirector.ToString();
            }
            else if (rolename == FunctionalRoleList.SMI.ToString())
            {
                return FunctionalRoleList.SalesDirector.ToString();
            }
            else if (rolename == FunctionalRoleList.RegManager.ToString())
            {
                return FunctionalRoleList.SMI.ToString();
            }
            else if (rolename == FunctionalRoleList.Supervisor.ToString())
            {
                return FunctionalRoleList.RegManager.ToString();
            }
            else if (rolename == FunctionalRoleList.SalesRep.ToString())
            {
                return FunctionalRoleList.Supervisor.ToString();
            }

            return string.Empty;
        }

        public string GetRoleDown(string rolename)
        {
            if (rolename == FunctionalRoleList.ManagingDirector.ToString())
            {
                return FunctionalRoleList.FinantialDirector.ToString();
            }
            else if (rolename == FunctionalRoleList.FinantialDirector.ToString())
            {
                return FunctionalRoleList.SalesDirector.ToString();
            }
            else if (rolename == FunctionalRoleList.SalesDirector.ToString())
            {
                return FunctionalRoleList.SMI.ToString();
            }
            else if (rolename == FunctionalRoleList.SMI.ToString())
            {
                return FunctionalRoleList.RegManager.ToString();
            }
            else if (rolename == FunctionalRoleList.RegManager.ToString())
            {
                return FunctionalRoleList.Supervisor.ToString();
            }
            else if (rolename == FunctionalRoleList.Supervisor.ToString())
            {
                return FunctionalRoleList.SalesRep.ToString();
            }
            else if (rolename == FunctionalRoleList.SalesRep.ToString())
            {
                return "IS_BOTTOM";
            }

            return string.Empty;
        }

        public DataTable GetFunctionalRoles(string[] allRoles)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Name", typeof(string));

            foreach (string s in allRoles)
            {
                if (IsFunctionalRole(s))
                {
                    dt.Rows.Add(s);
                }
            }

            return dt;
        }

        public DataTable GetOrganizationalRoles(string[] allRoles)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Name", typeof(string));

            foreach (string s in allRoles)
            {
                if (!IsFunctionalRole(s))
                {
                    dt.Rows.Add(s);
                }
            }

            return dt;
        }

        public DataTable GetOrganizationalRoles(string[] allRoles, DataTable roleDescriptions)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (string s in allRoles)
            {
                if (!IsFunctionalRole(s))
                {
                    DataRow row = dt.NewRow();
                    row[0] = s;
                    row[1] = s;
                    
                    DataRow[] desc = roleDescriptions.Select("RoleName = '" + s + "'");
                    if (desc.Length > 0)
                    {
                        row[1] = desc[0]["Description"].ToString();
                    }
                    dt.Rows.Add(row);
                }
            }

            return dt;
        }

        public bool IsFunctionalRole(string role)
        {
            foreach (FunctionalRoleList r in Enum.GetValues(typeof(FunctionalRoleList)))
            {
                if (role == r.ToString())
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region DATABASE
        public int Users_Delete(Guid userID)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_Users_DeleteByID";
                #endregion

                #region PARAMETROS
                #region UserID
                SqlParameter pr_user = cm.CreateParameter();
                pr_user.Direction = ParameterDirection.Input;
                pr_user.DbType = DbType.Guid;
                pr_user.Value = userID;
                pr_user.ParameterName = "userID";
                cm.Parameters.Add(pr_user);
                #endregion

                #region RowCount
                SqlParameter pr_rowcount = cm.CreateParameter();
                pr_rowcount.Direction = ParameterDirection.Output;
                pr_rowcount.DbType = DbType.Int32;
                pr_rowcount.ParameterName = "RowCount";
                cm.Parameters.Add(pr_rowcount);
                #endregion
                #endregion

                #region EXECUTAR
                if (cm.Connection.State.Equals(ConnectionState.Broken) ||
                                       cm.Connection.State.Equals(ConnectionState.Closed))
                {
                    cm.Connection.Open();
                }
                cm.ExecuteNonQuery();
                #endregion

                return Convert.ToInt32(pr_rowcount.Value);
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de utilizadores.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'MyMembership.GetUsers'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Users_Get()
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_Users_Get";
                #endregion

                #region EXECUTAR
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de utilizadores.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'MyMembership.GetUsers'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Users_GetByName(string username)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_Users_GetByName";
                #endregion

                #region PARAMETROS
                #region RoleName
                SqlParameter pr_user = cm.CreateParameter();
                pr_user.Direction = ParameterDirection.Input;
                pr_user.DbType = DbType.String;
                pr_user.Value = username;
                pr_user.ParameterName = "UserName";
                cm.Parameters.Add(pr_user);
                #endregion
                #endregion

                #region EXECUTAR
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de utilizadores.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'MyMembership.GetUsers'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Roles_Get()
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                SqlCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "spSRA_Roles_Get";
                #endregion

                #region EXECUTAR
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (SqlException exp)
            {
                throw new Exception("DAL -> Não foi possível obter a lista de Roles com Description.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no método 'MyMembership.GetRoles'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        #endregion
    }

    public enum FunctionalRoleList
    {
        SystemAdministrator, SMI, SalesRep, Supervisor, RegManager, SalesDirector, FinantialDirector, ManagingDirector
    }
}