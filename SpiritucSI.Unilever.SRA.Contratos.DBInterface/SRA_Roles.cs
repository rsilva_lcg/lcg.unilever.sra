﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    class Roles
    {
        public bool IsFunctionalRole(string role)
        {
            foreach(RoleList r in Enum.GetValues(typeof(RoleList)))
            {
                if (role == r.ToString())
                {
                    return true;
                }
            }
            return false;
        }
    }


    public enum RoleList
    {
        Administrator, SMI, SalesRep, Supervisor, RegManager, SalesDirector, FinantialDirector, ManagingDirector
    }

