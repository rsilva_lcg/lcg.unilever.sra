using System;
using System.Data;
using Oracle.DataAccess;
using Oracle.DataAccess.Client;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Configuration;
using System.Diagnostics;

namespace SRADBInterface
{
    [Serializable()]
    public class SRA : selectable
    {
        #region FIELDS
        /// <summary>
        /// DataBase ConnectionString To Use In All Commands
        /// </summary>
        protected string ConnectionString;
        protected Clientes Client;
        #endregion

        #region METHODS
        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        /// <param name="ConnString">ConnectionString to Source DataBase</param>
        public SRA(string ConnString)
        {
            this.ConnectionString = ConnString;
            Client = new Clientes(ConnString);
        }

        #region PROCESSO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public int getPreviousContrato(int idContrato)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_GET_PREVIOUS";
                #endregion

                #region PARAMETERS
                #region V_ID_CONTRATO
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.Value = idContrato;
                pr.ParameterName = "V_ID_CONTRATO";
                cm.Parameters.Add(pr);
                #endregion

                #region V_NEW_ID_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.IsNullable = true;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_NEW_ID_CONTRATO";
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                cn.Open();
                cm.ExecuteNonQuery();
                #endregion

                if (cm.Parameters["V_NEW_ID_CONTRATO"].Value == DBNull.Value)
                    return -1;
                else
                    return (int)cm.Parameters["V_NEW_ID_CONTRATO"].Value;
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter o contrato anterior", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getPreviousContrato(int)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public int getNextContrato(int idContrato)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_GET_NEXT";
                #endregion

                #region PARAMETERS
                #region V_ID_CONTRATO
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.Value = idContrato;
                pr.ParameterName = "V_ID_CONTRATO";
                cm.Parameters.Add(pr);
                #endregion

                #region V_NEW_ID_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.IsNullable = true;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_NEW_ID_CONTRATO";
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                cn.Open();
                cm.ExecuteNonQuery();
                #endregion

                if (cm.Parameters["V_NEW_ID_CONTRATO"].Value == DBNull.Value)
                    return -1;
                else
                    return (int)cm.Parameters["V_NEW_ID_CONTRATO"].Value;
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter o contrato seguinte", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getNextContrato(int)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        #endregion

        #region LOCAIS ENTREGA
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataTable ClientesGet()
        {
            return this.Client.ClientesGet("");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Where"></param>
        /// <returns></returns>
        public DataTable ClientesGet(string Where)
        {
            return this.Client.ClientesGet(Where);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContratoID"></param>
        /// <returns></returns>
        public DataTable ClientesGet(int ContratoID)
        {
            return this.Client.ClientesGet(ContratoID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContratoID"></param>
        /// <param name="CodLE"></param>
        /// <returns></returns>
        public int ClientesDel(int ContratoID, string CodLE)
        {
            return this.Client.ClientesDel(ContratoID, CodLE);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContratoID"></param>
        /// <returns></returns>
        public int ClientesDel(int ContratoID)
        {
            return this.Client.ClientesDel(ContratoID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cm"></param>
        /// <param name="ContratoID"></param>
        /// <param name="CodLE"></param>
        /// <returns></returns>
        public int ClientesDel(OracleCommand cm, int ContratoID, string CodLE)
        {
            return this.Client.ClientesDel(cm, ContratoID, CodLE);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cm"></param>
        /// <param name="ContratoID"></param>
        /// <returns></returns>
        public int ClientesDel(OracleCommand cm, int ContratoID)
        {
            return this.Client.ClientesDel(cm, ContratoID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContratoID"></param>
        /// <param name="CodLE"></param>
        /// <param name="CodGrupoEconomico"></param>
        /// <param name="CodSubGrupoEconomico"></param>
        /// <param name="CodCliente"></param>
        /// <returns></returns>
        public int ClientesInsert(int ContratoID, DateTime? StartDate, DateTime? EndDate, string CodLE, string CodGrupoEconomico, string CodSubGrupoEconomico, string CodCliente)
        {
            return this.Client.ClientesInsert(ContratoID, StartDate, EndDate, CodLE, CodGrupoEconomico, CodSubGrupoEconomico, CodCliente);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cm"></param>
        /// <param name="ContratoID"></param>
        /// <param name="CodLE"></param>
        /// <param name="CodGrupoEconomico"></param>
        /// <param name="CodSubGrupoEconomico"></param>
        /// <param name="CodCliente"></param>
        /// <returns></returns>
        public int ClientesInsert(OracleCommand cm, int ContratoID, DateTime? StartDate, DateTime? EndDate, string CodLE, string CodGrupoEconomico, string CodSubGrupoEconomico, string CodCliente)
        {
            return this.Client.ClientesInsert(cm, ContratoID, StartDate, EndDate, CodLE, CodGrupoEconomico, CodSubGrupoEconomico, CodCliente);
        }
        #endregion

        #region GRUPO ECONOMICO
        /// <summary>
        /// Get grupo econ�mico List
        ///    SP_GRUPO_ECONOMICO_GET (CUR OUT SYS_REFCURSOR);
        /// </summary>
        /// <returns></returns>
        public DataTable GrupoEconomicoGet()
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_GRUPO_ECONOMICO_GET";
                #endregion

                #region PARAMETERS
                /// CUR
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.TableMappings.Add("Table", "LK_GRUPO_ECONOMICO");
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos grupos econ�micos.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.GrupoEconomicoGet()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Updates Contrato WF ID   
        ///    PK_SRA.SP_CONTRATO_STATUS_UPD(V_ID_CONTRATO INT
        ///     ,V_INSTANCE_STATE 	                CHAR
        ///     ,V_ALTER_USER               NVARCHAR2) 
        /// </summary>
        /// <param name="idContrato">IdContrato to update</param>
        /// <param name="WF_state">New Contrato WF State</param>
        /// <param name="UserName">Current UserName</param>
        public void UpdateContratoWF_ID(int idContrato, Guid WF_ID)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_WF_ID_UPD";
                #endregion

                #region PARAMETERS

                #region	V_ID_Contrato Varchar2
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.Value = idContrato;
                pr.ParameterName = "V_ID_CONTRATO";
                cm.Parameters.Add(pr);
                #endregion

                #region WF_ID VARCHAR2,
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 36;
                pr.ParameterName = "V_INSTANCE_ID";
                pr.Value = WF_ID;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                cn.Open();
                cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel actualizar o status do processo.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.UpdateContratoStatus(int, Guid)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        
        public void UpdateContratoWFState(Guid WF_ID, string WF_state)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_WF_STATE_UPD";
                #endregion

                #region PARAMETERS

                #region WF_ID VARCHAR2,
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 36;
                pr.ParameterName = "V_INSTANCE_ID";
                pr.Value = WF_ID;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_INSTANCE_STATE Varchar2
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 24;
                pr.ParameterName = "V_INSTANCE_STATE";
                pr.Value = WF_state.ToUpper();
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                cn.Open();
                cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel actualizar o status do processo.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.UpdateContratoStatus(Guid, string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public void AppendContratoObsMotivo(Guid WF_ID, string motivo)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_OBS_MOTIVO_APP";
                #endregion

                #region PARAMETERS

                #region WF_ID VARCHAR2,
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 36;
                pr.ParameterName = "V_INSTANCE_ID";
                pr.Value = WF_ID;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_INSTANCE_STATE Varchar2
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 4000;
                pr.ParameterName = "V_OBS_MOTIVO";
                pr.Value = motivo;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                cn.Open();
                cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel actualizar o status do processo.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.UpdateContratoStatus(Guid, string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        
        /// <summary>
        /// Updates Contrato WF State   
        ///    PK_SRA.SP_CONTRATO_STATUS_UPD(V_ID_CONTRATO INT
        ///     ,V_INSTANCE_STATE 	                CHAR
        ///     ,V_ALTER_USER               NVARCHAR2) 
        /// </summary>
        /// <param name="idContrato">IdContrato to update</param>
        /// <param name="WF_state">New Contrato WF State</param>
        /// <param name="UserName">Current UserName</param>
        public void UpdateContrato_IS_AUTHORIZED(Guid WF_ID, bool is_authorized)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_IS_AUTHORIZED_UPD";
                #endregion

                #region PARAMETERS

                #region WF_ID VARCHAR2,
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 36;
                pr.ParameterName = "V_INSTANCE_ID";
                pr.Value = WF_ID;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_IS_AUTHORIZED
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_AUTHORIZED";
                pr.Value = is_authorized ? "T" : "F";
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                cn.Open();
                cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel actualizar a autoriza��o do processo.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.UpdateContrato_IS_AUTHORIZED(Guid, bool)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public void UpdateContrato_IS_SENTRENEWAL(int id_contrato, bool is_sent_to_renewal)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_SENTRENEWAL_UPD";
                #endregion

                #region PARAMETERS

                #region ID 
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRATO";
                pr.Value = id_contrato;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_IS_AUTHORIZED
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_SENT_TO_RENEWAL";
                pr.Value = is_sent_to_renewal ? "T" : "F";
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                cn.Open();
                cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel actualizar a autoriza��o do processo.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.UpdateContrato_IS_AUTHORIZED(Guid, bool)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        
        /// <summary>
        /// Get Grupo Produto List
        ///    SP_SUBGRUPO_ECONOMICO_GET
        ///    (
        ///        V_ID_GRUPO_ECONOMICO VARCHAR2,    
        ///        CUR OUT SYS_REFCURSOR
        ///    );
        /// </summary>
        /// <param name="GrupoEconomicoID"></param>
        /// <returns></returns>
        public DataTable SubGrupoEconomicoGet(string GrupoEconomicoID)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_SUBGRUPO_ECONOMICO_GET ";
                #endregion

                #region PARAMETERS
                #region V_ID_GRUPO_ECONOMICO
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 3;
                pr.DbType = DbType.String;
                pr.Value = GrupoEconomicoID;
                pr.ParameterName = "V_ID_GRUPO_ECONOMICO";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.TableMappings.Add("Table", "LK_SUBGRUPO_ECONOMICO");
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos sub grupos econ�micos.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.SubGrupoEconomicoGet()'.", exp);
            }
            finally
            {
                cn.Close();
            }

        }
        #endregion

        /// <summary>
        /// Get Material visibilidade List
        ///    SP_MATERIAL_VISIBILIDADE_GET (CUR OUT SYS_REFCURSOR)
        /// </summary>
        /// <returns></returns>
        public DataTable getMaterial()
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_MATERIAL_VISIBILIDADE_GET";
                #endregion

                #region PARAMETERS
                /// CUR
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista do material de visibilidade.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getMaterial()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Get Grupo Produto List
        ///    SP_GRUPO_PRODUTO_GET (CUR OUT SYS_REFCURSOR)
        /// </summary>
        /// <returns></returns>
        public DataTable getGrupoProduto()
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_GRUPO_PRODUTO_GET";
                #endregion

                #region PARAMETERS
                /// CUR
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos grupos de produto.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getGrupoProduto()'.", exp);
            }
            finally
            {
                cn.Close();
            }

        }
        /// <summary>
        /// Get TTS List
        ///    SP_TTS_GET (CUR OUT SYS_REFCURSOR)
        /// </summary>
        /// <returns></returns>
        public DataTable getTTS()
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_TTS_GET";
                #endregion

                #region PARAMETERS
                /// CUR
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos TTS.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getTTS()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Get equipamento List
        ///    SP_EQUIPAMENTO_GET(CUR OUT SYS_REFCURSOR)
        /// </summary>
        /// <returns></returns>
        public DataTable getEquipamentoFrio()
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_EQUIPAMENTO_GET";
                #endregion

                #region PARAMETERS
                /// CUR
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos equipamentos de frio.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getEquipamentoFrio'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Get contrato List
        /// </summary>
        /// <returns></returns>
        public DataTable getContrato()
        {
            return this.getContrato("");
        }
        /// <summary>
        /// Get Contrato list filtered by 'Where' condition
        ///    PK_SRA.SP_CONTRATOS_GET(WHERESTM String,CUR OUT SYS_REFCURSOR)
        /// </summary>
        /// <param name="Where">Filter Conditions</param>
        /// <returns></returns>
        public DataTable getContrato(string Where)
        {
            /// PREPARE CONDITION
            if (Where.Equals(""))
            {
                Where = "1=1";
            }
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATOS_GET";
                #endregion

                #region PARAMENTERS
                /// WHERESTM
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.DbType = DbType.String;
                pr.Value = Where;
                pr.ParameterName = "WHERESTM";
                cm.Parameters.Add(pr);

                /// CUR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter os contratos para o filtro '" + Where + "'", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getContrato(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable getSalesHistory(int idContrato)
        {
            /// PREPARE CONDITION
            string LE_array = "'";
            DataTable clientes = this.Client.ClientesGet(idContrato);
            if (clientes.Rows.Count>0)
            {
                foreach(DataRow row in clientes.Rows)
                {
                    LE_array += row["cod_local_entrega"].ToString() + "','";
                }
                LE_array = LE_array.Substring(0, LE_array.Length-2);
            }

            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_SALES_HISTORY_GET";
                #endregion

                #region PARAMENTERS
                /// WHERESTM
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.DbType = DbType.String;
                pr.Value = LE_array;
                pr.ParameterName = "LE_ARRAY";
                cm.Parameters.Add(pr);

                /// CUR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter os contratos para o filtro '" + LE_array + "'", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getContrato(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        /// <summary>
        /// Get Contrato list with specified ID 
        ///    PK_SRA.SP_CONTRATO_GET(V_ID_CONTRATO INT
        ///     ,CUR                       OUT SYS_REFCURSOR
        ///     ,CUR_ENTIDADE              OUT SYS_REFCURSOR
        ///     ,CUR_EXPLORACAO            OUT SYS_REFCURSOR
        ///     ,CUR_BONUS                 OUT SYS_REFCURSOR
        ///     ,CUR_BONUS_GLOBAL          OUT SYS_REFCURSOR
        ///     ,CUR_CONT_EQUIPAMENTO      OUT SYS_REFCURSOR
        ///     ,CUR_CONT_MAT_VISIBILIDADE OUT SYS_REFCURSOR
        ///     ,CUR_CONTRAPARTIDAS        OUT SYS_REFCURSOR
        ///     ,CUR_FASEAMENTO_PAG        OUT SYS_REFCURSOR
        ///     ,CUR_GAMAS_QUANTIA_FIM     OUT SYS_REFCURSOR
        ///     ,CUR_GAMAS_GLOBAL          OUT SYS_REFCURSOR)
        /// </summary>
        /// <param name="idContrato">IdContrato to get</param>
        /// <returns></returns>
        public DataSet getContrato(int idContrato)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_GET";
                #endregion

                #region PARAMETERS
                #region V_ID_CONTRATO
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.Value = idContrato;
                pr.ParameterName = "V_ID_CONTRATO";
                cm.Parameters.Add(pr);
                #endregion

                #region  CUR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region  CUR_ENTIDADE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_ENTIDADE";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR_EXPLORACAO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_EXPLORACAO";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR_BONUS
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_BONUS";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR_BONUS_GLOBAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_BONUS_GLOBAL";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR_CONT_EQUIPAMENTO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_CONT_EQUIPAMENTO";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR_CONT_MAT_VISIBILIDADE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_CONT_MAT_VISIBILIDADE";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR_CONTRAPARTIDAS
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_CONTRAPARTIDAS";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR_FASEAMENTO_PAG
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_FASEAMENTO_PAG";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR_GAMAS_QUANTIA_FIM
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_GAMAS_QUANTIA_FIM";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR_GAMAS_GLOBAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR_GAMAS_GLOBAL";
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.TableMappings.Add("Table", "CONTRATO");////
                da.TableMappings.Add("Table1", "ENTIDADE");////
                da.TableMappings.Add("Table2", "EXPLORACAO");////
                da.TableMappings.Add("Table3", "BONUS");////
                da.TableMappings.Add("Table4", "BONUS_GLOBAL");////
                da.TableMappings.Add("Table5", "CONT_EQUIPAMENTO");////
                da.TableMappings.Add("Table6", "CONT_MAT_VISIBILIDADE");////
                da.TableMappings.Add("Table7", "CONTRAPARTIDAS");////
                da.TableMappings.Add("Table8", "FASEAMENTO_PAG");////
                da.TableMappings.Add("Table9", "GAMAS_QUANTIA_FIM");////
                da.TableMappings.Add("Table10", "GAMAS_GLOBAL");////
                da.Fill(ds);
                #endregion

                return ds;
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter os dados do contrato com ID=" + idContrato, exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getContrato(int)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataSet getContratoBy_WF_ID(Guid INSTANCE_ID)
        {
            DataTable contrato = getContrato("(INSTANCE_ID) = '" + INSTANCE_ID + "'");
            int idContrato = -1;
            if (contrato.Rows.Count > 0)
            {
                idContrato = Convert.ToInt32(contrato.Rows[0]["ID_CONTRATO"]);
            }
            return getContrato(idContrato);
            //TYPE: DataTable
            //return getContrato("(INSTANCE_ID) = '" + INSTANCE_ID.ToString() + "'");
        }
        /// <summary>
        /// Updates Contrato Status   
        ///    PK_SRA.SP_CONTRATO_STATUS_UPD(V_ID_CONTRATO INT
        ///     ,V_STATUS	                CHAR
        ///     ,V_MOTIVO                   VARCHAR2
        ///     ,V_DATA_FIM_EFECTIVO        DATE
        ///     ,V_NUM_PROCESSO_SEGUINTE    VARCHAR2
        ///     ,V_ALTER_USER               NVARCHAR2) 
        /// </summary>
        /// <param name="idContrato">IdContrato to update</param>
        /// <param name="status">New Contrato Status</param>
        /// <param name="motivo">Status Change Motiv</param>
        /// <param name="EfectiveDate">Last Contracto Efective close date</param>
        /// <param name="ProcessoSeguinte">Next Process Number</param>
        /// <param name="UserName">Current UserName</param>
        public void UpdateContratoStatus(int idContrato, string status, string motivo, DateTime EfectiveDate, string ProcessoSeguinte, string UserName)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_STATUS_UPD";
                #endregion

                #region PARAMETERS

                #region V_ID_CONTRATO INT,
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRATO";
                pr.Value = idContrato;
                cm.Parameters.Add(pr);
                #endregion

                #region V_STATUS
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_STATUS";
                pr.Value = status;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_MOTIVO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 4000;
                pr.ParameterName = "V_MOTIVO";
                if (motivo != null)
                    pr.Value = motivo;
                else
                    pr.Value = DBNull.Value;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_DATA_FIM_EFECTIVO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.ParameterName = "V_DATA_FIM_EFECTIVO";
                if ((status == "I"))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = EfectiveDate.Date;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_NUM_PROCESSO_SEGUINTE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_NUM_PROCESSO_SEGUINTE";
                pr.Value = ProcessoSeguinte;
                cm.Parameters.Add(pr);
                #endregion

                #region V_ALTER_USER
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_ALTER_USER";
                pr.Value = UserName;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                cn.Open();
                cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel actualizar o status do processo.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.UpdateContratoStatus(int, string, string, DateTime, string, string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        // Metodo de compatibilidade com o processo antigo do SRA
        public void UpdateContratoStatus(int idContrato, string status, string motivo, DateTime EfectiveDate, string UserName)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_STATUS_UPD";
                #endregion

                #region PARAMETERS

                #region V_ID_CONTRATO INT,
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRATO";
                pr.Value = idContrato;
                cm.Parameters.Add(pr);
                #endregion

                #region V_STATUS
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_STATUS";
                pr.Value = status;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_MOTIVO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 4000;
                pr.ParameterName = "V_MOTIVO";
                if (motivo != null)
                    pr.Value = motivo;
                else
                    pr.Value = DBNull.Value;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_DATA_FIM_EFECTIVO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.ParameterName = "V_DATA_FIM_EFECTIVO";
                if (status == "I")
                    pr.Value = DBNull.Value;
                else
                    pr.Value = EfectiveDate.Date;
                cm.Parameters.Add(pr);
                #endregion

                #region	V_NUM_PROCESSO_SEGUINTE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_NUM_PROCESSO_SEGUINTE";
                pr.Value = "";
                cm.Parameters.Add(pr);
                #endregion

                #region V_ALTER_USER
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_ALTER_USER";
                pr.Value = UserName;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                cn.Open();
                cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel actualizar o status do processo.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.UpdateContratoStatus(int, string, string, DateTime, string, string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
 
        /// <summary>
        /// Get Contrato with specified PROCESS/SEQUENCE
        /// </summary>
        /// <param name="ProcessNumber">The Contrato PROCESS/SEQUENCE to get</param>
        /// <returns></returns>
        public DataRow getContratoByProcess(string ProcessNumber)
        {
            DataTable r = getContrato("(ID_PROCESSO) = '" + ProcessNumber + "'");
            if (r.Rows.Count == 0)
                return null;
            return r.Rows[0];
        }

        public DataTable getContratos_ToClose()
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                cn.Open();
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 0;
                cm.CommandText = "PK_SRA.SP_CONTRATO_TOCLOSE";
                #endregion

                #region PARAMETERS
                /// CUR
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista do CONTRATOS_TOCLOSE.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getCONTRATOS_TOCLOSE()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable getContratos_ToRenewal()
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                cn.Open();
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 0;
                cm.CommandText = "PK_SRA.SP_CONTRATO_TORENEWAL";
                #endregion

                #region PARAMETERS
                /// CUR
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                //EventLog.WriteEntry("wsSRA - RENEWAL", "conn: " + cm.Connection.State.ToString(), EventLogEntryType.Information);
                da.Fill(ds);
                #endregion


                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista do CONTRATOS_TORENEWAL:" + exp.Message, exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getCONTRATOS_TORENEWAL()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
       
        public DataTable getConcessionarios()
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONCESSIONARIOS_GET_ALL";
                #endregion

                #region PARAMETERS
                /// CUR
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista do material de visibilidade.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getMaterial()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable getZonas()
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_ZONAS_GET_ALL";
                #endregion

                #region PARAMETERS
                /// CUR
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista get_zonas.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'get_zonas'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable get_Contrato_Emails(int idContrato)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_GET_EMAILS";
                #endregion

                #region PARAMETERS
                #region V_ID_CONTRATO INT,
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRATO";
                pr.Value = idContrato;
                cm.Parameters.Add(pr);
                #endregion

                /// CUR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista SP_CONTRATO_GET_EMAILS.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.SP_CONTRATO_GET_EMAILS()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable getContratos_LE_By_Weight(int idContrato)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_LE_By_Weight";
                #endregion

                #region PARAMETERS
                #region V_ID_CONTRATO INT,
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRATO";
                pr.Value = idContrato;
                cm.Parameters.Add(pr);
                #endregion
                /// CUR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista do material de visibilidade.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getMaterial()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="InstanceID"></param>
        /// <returns></returns>
        public bool IsContratoAuthorized(Guid InstanceID)
        {
            DataTable r = getContrato("(INSTANCE_ID) = '" + InstanceID.ToString() + "'");
            if (r.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                if ((r.Rows[0]["IS_AUTHORIZED"] == null) || (r.Rows[0]["IS_AUTHORIZED"].ToString() == ""))
                {
                    return false;
                }
            }

            return r.Rows[0]["IS_AUTHORIZED"].ToString().ToUpper().Equals("T");

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="InstanceID"></param>
        /// <returns></returns>
           
        public bool IsContratoAutoRenewal(Guid InstanceID)
        {
            DataTable r = getContrato("(INSTANCE_ID) = '" + InstanceID.ToString() + "'");

            if (r.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                if ((r.Rows[0]["ANOS_RENOVACAO"] == null) || (r.Rows[0]["ANOS_RENOVACAO"].ToString() == ""))
                {
                    return false;
                }
                if ((r.Rows[0]["NUM_PROCESSO_ANTERIOR"] == null) || (r.Rows[0]["NUM_PROCESSO_ANTERIOR"].ToString() == ""))
                {
                    return false;
                }
            }             

            int yearAutoRenewal = 0;
            if (!int.TryParse(r.Rows[0]["ANOS_RENOVACAO"].ToString(), out yearAutoRenewal))
            {
                return false;
            }

            return (yearAutoRenewal > 0);
        }


        //public bool IsContratoAutoRenewal(Guid InstanceID)
        //{
        //    DataTable r = getContrato("(INSTANCE_ID) = '" + InstanceID.ToString() + "'");
        //    if (r.Rows.Count == 0)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        if ((r.Rows[0]["ANOS_RENOVACAO"] == null) || (r.Rows[0]["ANOS_RENOVACAO"].ToString() == ""))
        //        {
        //            return false;
        //        }
        //    }

        //    return (!r.Rows[0]["IS_AUTHORIZED"].ToString().ToUpper().Equals("0"));
        //}
        /// <summary>
        /// Update existing Contract
        /// TODO: 99 - Preencher documenta��o
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="Contrato_Concessionario"></param>
        /// <param name="Contrato_Num"></param>
        /// <param name="Contrato_NumAnterior"></param>
        /// <param name="Contrato_NumSeguinte"></param>
        /// <param name="Contrato_Classif"></param>
        /// <param name="Contrato_Tipo"></param>
        /// <param name="Contrato_Data"></param>
        /// <param name="Contrato_Nivel"></param>
        /// <param name="Contrato_GrupoEconomico"></param>
        /// <param name="Contrato_SubGrupoEconomico"></param>
        /// <param name="Contrato_Status"></param>
        /// <param name="Contrato_Observacoes"></param>
        /// <param name="Contrato_Observacoes_Motivo"></param>
        /// <param name="PV_NomeFirma"></param>
        /// <param name="PV_MoradaSede"></param>
        /// <param name="PV_Localidade"></param>
        /// <param name="PV_RamoActividade"></param>
        /// <param name="PV_Matricula"></param>
        /// <param name="PV_MatriculaNum"></param>
        /// <param name="PV_CapitalSoial"></param>
        /// <param name="PV_Nif"></param>
        /// <param name="PV_Designacao"></param>
        /// <param name="PV_MoradaPtVenda"></param>
        /// <param name="COND_Objectivo"></param>
        /// <param name="COND_DataInicio"></param>
        /// <param name="COND_DataFim"></param>
        /// <param name="COND_QuandoAtingir"></param>
        /// <param name="COND_AnosRenovacao"></param>
        /// <param name="COND_ISContratoFormal"></param>
        /// <param name="COND_ISPooc"></param>
        /// <param name="COND_ISFornFrio"></param>
        /// <param name="CONT_Previsao"></param>
        /// <param name="CONT_PrazoPagamento"></param>
        /// <param name="GamasGlobal"></param>
        /// <param name="FASE_NIB"></param>
        /// <param name="FASE_BANCO"></param>
        /// <param name="FASE_BANCO_DEPENDENCIA"></param>
        /// <param name="FASE_CompCompras"></param>
        /// <param name="User"></param>
        /// <param name="Clientes"></param>
        /// <param name="Gamas"></param>
        /// <param name="Exploracao"></param>
        /// <param name="Equipamentos"></param>
        /// <param name="Contrapartidas"></param>
        /// <param name="Bonus"></param>
        /// <param name="BonusGlobal"></param>
        /// <param name="Verbas"></param>
        /// <param name="Material"></param>
        /// <returns></returns>
        public string UpdateContrato(
            ref int idContrato,
            string Contrato_Concessionario,
            string Contrato_Num,
            string Contrato_NumAnterior,
            string Contrato_NumSeguinte,
            string Contrato_Classif,
            string Contrato_Tipo,
            DateTime Contrato_Data,
            string Contrato_Nivel,
            string Contrato_GrupoEconomico,
            string Contrato_SubGrupoEconomico,
            string Contrato_Status,
            string Contrato_Observacoes,
            string Contrato_Observacoes_Motivo,
            string PV_NomeFirma,
            string PV_MoradaSede,
            string PV_Localidade,
            string PV_RamoActividade,
            string PV_Matricula,
            string PV_MatriculaNum,
            string PV_CapitalSoial,
            string PV_Nif,
            string PV_Designacao,
            string PV_MoradaPtVenda,
            string COND_Objectivo,
            DateTime COND_DataInicio,
            DateTime COND_DataFim,
            string COND_QuandoAtingir,
            string COND_AnosRenovacao,
            string COND_ISContratoFormal,
            string COND_ISPooc,
            string COND_ISFornFrio,
            string CONT_Previsao,
            string CONT_PrazoPagamento,
            DataTable GamasGlobal,
            string FASE_NIB,
            string FASE_BANCO,
            string FASE_BANCO_DEPENDENCIA,
            string FASE_CompCompras,
            string User,
            DataTable Clientes,
            DataTable Gamas,
            DataTable Exploracao,
            DataTable Equipamentos,
            DataTable Contrapartidas,
            DataTable Bonus,
            DataTable BonusGlobal,
            DataTable Verbas,
            DataTable Material,
            //CHANGED BY RRAFAEL 08/02/2011; ORIGINAL: Guid Instance_ID
            Guid? Instance_ID,
            // END CHANGES
            string INSTANCE_STATE,
            string IS_AUTHORIZED,
            string DEFAULT_COD_CONC,
            string IS_SENT_TO_RENEWAL,
            string IS_ACTIVE,
            string IS_IMPORTADO
            )
        {
            string numContrato = "";
            int oldIdContrato = idContrato;
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            OracleCommand cm = cn.CreateCommand();
            OracleTransaction tr = null;

            try
            {
                #region CONNECTION
                cn.Open();
                tr = cn.BeginTransaction();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                #endregion

                OracleParameter pr = cm.CreateParameter();

                #region APAGAR CONTRATO ANTERIOR
                #region PARAMETROS
                ///  V_ID_CONTRACTO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRATO";
                pr.Value = idContrato;
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTAR
                cm.CommandText = "PK_SRA.SP_CONTRATO_DEL";
                cm.ExecuteNonQuery();
                cm.Parameters.Clear();
                #endregion
                #endregion

                #region CRIAR PARAMETROS DO CONTRATO
                #region DADOS DO CONTRATO

                #region  V_CONCESSIONARIO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_CONCESSIONARIO";
                pr.Value = Contrato_Concessionario;
                cm.Parameters.Add(pr);
                #endregion

                #region V_ID_CONTRACTO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.InputOutput;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRACTO";
                cm.Parameters.Add(pr);
                #endregion

                #region V_ID_PROCESSO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.InputOutput;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_ID_PROCESSO";
                if (Contrato_Num.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = Contrato_Num;
                cm.Parameters.Add(pr);
                #endregion

                #region V_CLASSIFICACAO_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_CLASSIFICACAO_CONTRATO";
                pr.Value = Contrato_Classif;
                cm.Parameters.Add(pr);
                #endregion

                #region V_TIPO_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 3;
                pr.ParameterName = "V_TIPO_CONTRATO";
                pr.Value = Contrato_Tipo;
                cm.Parameters.Add(pr);
                #endregion

                #region V_DATA_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.DbType = DbType.DateTime;
                pr.ParameterName = "V_DATA_CONTRATO";
                pr.Value = Contrato_Data;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_NIVEL_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 3;
                pr.ParameterName = "V_NIVEL_CONTRATO";
                pr.Value = Contrato_Nivel;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_COD_GRUPO_ECONOMICO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 3;
                pr.ParameterName = "V_COD_GRUPO_ECONOMICO";
                pr.Value = Contrato_GrupoEconomico;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_COD_SUBGRUPO_ECONOMICO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 3;
                pr.ParameterName = "V_COD_SUBGRUPO_ECONOMICO";
                pr.Value = Contrato_SubGrupoEconomico;
                cm.Parameters.Add(pr);
                #endregion

                #region V_NUM_PROCESSO_ANTERIOR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_NUM_PROCESSO_ANTERIOR";
                if (Contrato_NumAnterior.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = Contrato_NumAnterior;
                cm.Parameters.Add(pr);
                #endregion

                #region V_NUM_PROCESSO_SEGUINTE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_NUM_PROCESSO_SEGUINTE";
                if (Contrato_NumSeguinte.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = Contrato_NumSeguinte;
                cm.Parameters.Add(pr);
                #endregion

                #region V_STATUS
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_STATUS";
                pr.Value = Contrato_Status;
                cm.Parameters.Add(pr);
                #endregion

                #region V_OBSERVACOES
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 4000;
                pr.ParameterName = "V_OBSERVACOES";
                pr.Value = Contrato_Observacoes;
                cm.Parameters.Add(pr);
                #endregion

                #region V_OBSERVACOES_MOTIVO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 4000;
                pr.ParameterName = "V_OBSERVACOES_MOTIVO";
                pr.Value = Contrato_Observacoes_Motivo;
                cm.Parameters.Add(pr);
                #endregion

                #endregion

                #region DADOS DO PONTO DE VENDA
                #region  V_NOME_FIRMA,
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_NOME_FIRMA";
                pr.Value = PV_NomeFirma;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_MORADA_SEDE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 255;
                pr.ParameterName = "V_MORADA_SEDE";
                pr.Value = PV_MoradaSede;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_LOCALIDADE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_LOCALIDADE";
                pr.Value = PV_Localidade;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_RAMO_ACTIVIDADE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 255;
                pr.ParameterName = "V_RAMO_ACTIVIDADE";
                pr.Value = PV_RamoActividade;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_MATRICULA
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_MATRICULA";
                pr.Value = PV_Matricula;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_MATRICULA_NUM
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_MATRICULA_NUM";
                pr.Value = PV_MatriculaNum;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_CAPITAL_SOCIAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_CAPITAL_SOCIAL";
                pr.Value = PV_CapitalSoial;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_NIF
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.ParameterName = "V_NIF";
                pr.Size = 20;
                pr.Value = PV_Nif;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_DESIGNACAO_PT_VENDA
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 255;
                pr.ParameterName = "V_DESIGNACAO_PT_VENDA";
                pr.Value = PV_Designacao;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_MORADA_PT_VENDA
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 255;
                pr.ParameterName = "V_MORADA_PT_VENDA";
                pr.Value = PV_MoradaPtVenda;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region DADOS DAS CONDI��ES
                #region  V_OBJECTIVO			   VARCHAR2,
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_OBJECTIVO";
                pr.Value = COND_Objectivo;
                cm.Parameters.Add(pr);
                #endregion

                #region V_IS_POOC
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_IS_POOC";
                pr.Value = COND_ISPooc;
                cm.Parameters.Add(pr);
                #endregion

                #region V_DATA_INICIO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.ParameterName = "V_DATA_INICIO";
                pr.Value = COND_DataInicio.Date;
                cm.Parameters.Add(pr);
                #endregion

                #region V_DATA_FIM
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.ParameterName = "V_DATA_FIM";
                if (COND_DataFim.Equals(DateTime.MinValue))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = COND_DataFim.Date;
                cm.Parameters.Add(pr);
                #endregion

                #region V_DATA_FIM_EFECTIVO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.ParameterName = "V_DATA_FIM_EFECTIVO";
                if (COND_DataFim.Equals(DateTime.MinValue))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = (COND_AnosRenovacao.Equals("") ? COND_DataFim : COND_DataFim.AddYears(int.Parse(COND_AnosRenovacao))).Date;
                cm.Parameters.Add(pr);
                #endregion

                #region V_QUANTIA_FIM
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_QUANTIA_FIM";
                if (COND_QuandoAtingir.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = int.Parse(COND_QuandoAtingir);
                cm.Parameters.Add(pr);
                #endregion

                #region V_ANOS_RENOVACAO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ANOS_RENOVACAO";
                if (COND_AnosRenovacao.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = int.Parse(COND_AnosRenovacao);
                cm.Parameters.Add(pr);
                #endregion

                #region V_IS_CONTRATO_FORMAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_IS_CONTRATO_FORMAL";
                pr.Value = COND_ISContratoFormal;
                cm.Parameters.Add(pr);
                #endregion

                #region V_IS_FORN_FRIO_IGLO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_IS_FORN_FRIO_IGLO";
                pr.Value = COND_ISFornFrio;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region DADOS DAS CONTRAPARTIDAS GLOBAIS
                #region  V_PREVISAO_VENDAS_GLOBAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_PREVISAO_VENDAS_GLOBAL";
                if (CONT_Previsao == null || CONT_Previsao.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = int.Parse(CONT_Previsao);
                cm.Parameters.Add(pr);
                #endregion

                #region V_PRAZO_PAGAMENTO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_PRAZO_PAGAMENTO";
                pr.Value = CONT_PrazoPagamento;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region DADOS DO FASEAMENTO
                #region  V_NIB_CLIENTE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.ParameterName = "V_NIB_CLIENTE";
                pr.Size = 50;
                pr.Value = FASE_NIB;
                cm.Parameters.Add(pr);
                #endregion

                #region V_BANCO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.ParameterName = "V_BANCO";
                pr.Size = 100;
                pr.Value = FASE_BANCO;
                cm.Parameters.Add(pr);
                #endregion

                #region V_BANCO_DEPENDENCIA
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.ParameterName = "V_BANCO_DEPENDENCIA";
                pr.Size = 100;
                pr.Value = FASE_BANCO_DEPENDENCIA;
                cm.Parameters.Add(pr);
                #endregion

                #region V_COMP_CONC_COMPRAS
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Decimal;
                pr.DbType = DbType.Decimal;
                pr.ParameterName = "V_COMP_CONC_COMPRAS";
                if (FASE_CompCompras.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = decimal.Parse(FASE_CompCompras);
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region DADOS USER
                ///  V_INSERT_USER			   VARCHAR2
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_INSERT_USER";
                pr.Value = User;
                cm.Parameters.Add(pr);
                #endregion

                #region V_EXISTS
                ///  V_EXISTS			   VARCHAR2
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_EXISTS";
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_INSTANCE_ID
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 36;
                pr.ParameterName = "V_INSTANCE_ID";
                pr.Value = Instance_ID;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_INSTANCE_STATE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 24;
                pr.ParameterName = "V_INSTANCE_STATE";
                pr.Value = INSTANCE_STATE;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_IS_AUTHORIZED
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_AUTHORIZED";
                pr.Value = IS_AUTHORIZED;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_DEFAULT_COD_CONC
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 3;
                pr.ParameterName = "V_DEFAULT_COD_CONC";
                pr.Value = DEFAULT_COD_CONC;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_IS_SENT_TO_RENEWAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_SENT_TO_RENEWAL";
                pr.Value = IS_SENT_TO_RENEWAL;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_IS_ACTIVE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_ACTIVE";
                pr.Value = IS_ACTIVE;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_IS_IMPORTADO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_IMPORTADO";
                pr.Value = IS_IMPORTADO;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region INSERIR CONTRATO
                cm.CommandText = "PK_SRA.SP_CONTRATO_INS";
                cm.ExecuteNonQuery();
                idContrato = (int)cm.Parameters["V_ID_CONTRACTO"].Value;
                numContrato = cm.Parameters["V_ID_PROCESSO"].Value.ToString();
                cm.Parameters.Clear();
                #endregion

                #region INSERIR TABELAS ADICIONAIS
                #region INSERIR CLIENTES (LEs)
                if (Contrato_Nivel == "LE")
                {
                    Clientes.DefaultView.RowFilter = "";
                    Clientes.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                    foreach (DataRowView client in Clientes.DefaultView)
                    {
                        ClientesInsert(
                            cm,
                            idContrato,
                            client["DATA_INICIO"] == null || client["DATA_INICIO"] == DBNull.Value ? null : (DateTime?)client["DATA_INICIO"],
                            client["DATA_FIM"] == null || client["DATA_FIM"] == DBNull.Value ? null : (DateTime?)client["DATA_FIM"],
                            client["COD_LOCAL_ENTREGA"].ToString(),
                            client["COD_GRUPO_ECONOMICO_CLI"].ToString(),
                            client["COD_SUBGRUPO_ECONOMICO_CLI"].ToString(),
                            client["COD_CLIENTE"].ToString()
                        );
                    }
                }
                #endregion

                #region INSERIR CONDI��ES
                #region GAMAS
                cm.CommandText = "PK_SRA.SP_GAMA_INS";
                Gamas.DefaultView.RowFilter = "";
                Gamas.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Gama in Gamas.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETTERS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_COD_GRUPO_PRODUTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 4;
                    pr.ParameterName = "V_COD_GRUPO_PRODUTO";
                    pr.Value = Gama["COD_GRUPO_PRODUTO"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                cm.Parameters.Clear();
                #endregion

                #region EXPLORA��O
                cm.CommandText = "PK_SRA.SP_EXPLORACAO_INS";
                Exploracao.DefaultView.RowFilter = "";
                Exploracao.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Exp in Exploracao.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETERS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_TIPO_EXPLORACAO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 2;
                    pr.ParameterName = "V_TIPO_EXPLORACAO";
                    pr.Value = Exp["COD_EXPLORACAO"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                cm.Parameters.Clear();
                #endregion

                #region EQUIPAMENTO
                cm.CommandText = "PK_SRA.SP_EQUIPAMENTO_INS";
                Equipamentos.DefaultView.RowFilter = "";
                Equipamentos.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Equipamento in Equipamentos.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETERS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COD_EQUIPAMENTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 4;
                    pr.ParameterName = "V_COD_EQUIPAMENTO";
                    pr.Value = Equipamento["COD_TIPO_EQUIPAMENTO"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_QUANTIDADE
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_QUANTIDADE";
                    pr.Value = int.Parse(Equipamento["QUANTIDADE"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                cm.Parameters.Clear();
                #endregion
                #endregion

                #region INSERIR CONTRAPARTIDAS
                Contrapartidas.DefaultView.RowFilter = "";
                Contrapartidas.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Contrapartida in Contrapartidas.DefaultView)
                {
                    cm.CommandText = "PK_SRA.SP_CONTRAPARTIDA_INS";
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_ID_CONTRAPARTIDA
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.InputOutput;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRAPARTIDA";
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COD_GRUPO_PRODUTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 4;
                    pr.ParameterName = "V_COD_GRUPO_PRODUTO";
                    pr.Value = Contrapartida["COD_GRUPO_PRODUTO"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_PREVISAO_VENDAS
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_PREVISAO_VENDAS";
                    if (Contrapartida["PREVISAO_VENDAS"] == DBNull.Value || Contrapartida["PREVISAO_VENDAS"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = int.Parse(Contrapartida["PREVISAO_VENDAS"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_DESCONTO_FACTURA
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_DESCONTO_FACTURA";
                    if (Contrapartida["DESCONTO_FACTURA"] == DBNull.Value || Contrapartida["DESCONTO_FACTURA"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Contrapartida["DESCONTO_FACTURA"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COMP_CONCESSIONARIO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_COMP_CONCESSIONARIO";
                    if (Contrapartida["COMP_CONCESSIONARIO"] == DBNull.Value || Contrapartida["COMP_CONCESSIONARIO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Contrapartida["COMP_CONCESSIONARIO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_PRAZO_PAGAMENTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Char;
                    pr.ParameterName = "V_PRAZO_PAGAMENTO";
                    if (Contrapartida["PRAZO_PAGAMENTO"] == DBNull.Value || Contrapartida["PRAZO_PAGAMENTO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = Contrapartida["PRAZO_PAGAMENTO"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    #region EXECUTAR
                    cm.ExecuteNonQuery();
                    int idContrapartida = int.Parse(cm.Parameters["V_ID_CONTRAPARTIDA"].Value.ToString());
                    #endregion

                    #region INSERIR BONUS
                    cm.CommandText = "PK_SRA.SP_BONUS_INS";
                    foreach (DataRow Bon in Bonus.Select("ID_CONTRAPARTIDA = " + Contrapartida["ID_CONTRAPARTIDA"].ToString(), "", DataViewRowState.CurrentRows))
                    {
                        cm.Parameters.Clear();

                        #region PARAMETROS
                        #region  V_ID_CONTRAPARTIDA
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Int32;
                        pr.DbType = DbType.Int32;
                        pr.ParameterName = "V_ID_CONTRAPARTIDA";
                        pr.Value = idContrapartida;
                        cm.Parameters.Add(pr);
                        #endregion

                        #region  V_VALOR_LIMITE_MIN
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Int32;
                        pr.DbType = DbType.Int32;
                        pr.ParameterName = "V_VALOR_LIMITE_MIN";
                        if (Bon["VALOR_LIMITE_MIN"] == DBNull.Value || Bon["VALOR_LIMITE_MIN"].Equals(""))
                            pr.Value = DBNull.Value;
                        else
                            pr.Value = int.Parse(Bon["VALOR_LIMITE_MIN"].ToString());
                        cm.Parameters.Add(pr);
                        #endregion

                        #region  V_DESCONTO
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Decimal;
                        pr.ParameterName = "V_DESCONTO";
                        if (Bon["DESCONTO"] == DBNull.Value || Bon["DESCONTO"].Equals(""))
                            pr.Value = DBNull.Value;
                        else
                            pr.Value = decimal.Parse(Bon["DESCONTO"].ToString());
                        cm.Parameters.Add(pr);
                        #endregion

                        #region  V_COMP_CONCESSIONARIO
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Decimal;
                        pr.ParameterName = "V_COMP_CONCESSIONARIO";
                        if (Bon["COMP_CONCESSIONARIO"] == DBNull.Value || Bon["COMP_CONCESSIONARIO"].Equals(""))
                            pr.Value = DBNull.Value;
                        else
                            pr.Value = decimal.Parse(Bon["COMP_CONCESSIONARIO"].ToString());
                        cm.Parameters.Add(pr);
                        #endregion

                        #region  V_TIPO
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Char;
                        pr.Size = 3;
                        pr.ParameterName = "V_TIPO";
                        if (Bon["TIPO"] == DBNull.Value || Bon["TIPO"].Equals(""))
                            pr.Value = DBNull.Value;
                        else
                            pr.Value = Bon["TIPO"].ToString();
                        cm.Parameters.Add(pr);
                        #endregion
                        #endregion

                        cm.ExecuteNonQuery();
                    }
                    #endregion
                }
                cm.Parameters.Clear();
                #endregion

                #region INSERIR CONTRAPARTIDAS GLOBAIS
                #region GAMAS
                cm.CommandText = "PK_SRA.SP_GAMA_GLOBAL_INS";
                GamasGlobal.DefaultView.RowFilter = "";
                GamasGlobal.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Gama in GamasGlobal.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COD_GRUPO_PRODUTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 4;
                    pr.ParameterName = "V_COD_GRUPO_PRODUTO";
                    pr.Value = Gama["COD_GRUPO_PRODUTO"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                cm.Parameters.Clear();
                #endregion

                #region BONUS
                cm.CommandText = "PK_SRA.SP_BONUS_GLOBAL_INS";
                BonusGlobal.DefaultView.RowFilter = "";
                BonusGlobal.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Bon in BonusGlobal.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_VALOR_LIMITE_MIN
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_VALOR_LIMITE_MIN";
                    if (Bon["VALOR_LIMITE_MIN"] == DBNull.Value || Bon["VALOR_LIMITE_MIN"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = int.Parse(Bon["VALOR_LIMITE_MIN"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_DESCONTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_DESCONTO";
                    if (Bon["DESCONTO"] == DBNull.Value || Bon["DESCONTO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Bon["DESCONTO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COMP_CONCESSIONARIO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_COMP_CONCESSIONARIO";
                    if (Bon["COMP_CONCESSIONARIO"] == DBNull.Value || Bon["COMP_CONCESSIONARIO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Bon["COMP_CONCESSIONARIO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_TIPO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Char;
                    pr.Size = 3;
                    pr.ParameterName = "V_TIPO";
                    if (Bon["TIPO"] == DBNull.Value || Bon["TIPO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = Bon["TIPO"].ToString();
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                #endregion
                #endregion

                #region INSERIR FASEAMENTO
                cm.CommandText = "PK_SRA.SP_FASEAMENTO_INS";
                Verbas.DefaultView.RowFilter = "";
                Verbas.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Verba in Verbas.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_DATA_PAGAMENTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Date;
                    pr.ParameterName = "V_DATA_PAGAMENTO";
                    pr.Value = (DateTime)Verba["DATA_PAGAMENTO"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_DATA_PAGAMENTO_EFECTUADO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Date;
                    pr.ParameterName = "V_DATA_PAGAMENTO_EFECTUADO";
                    if (Verba["DATA_PAGAMENTO_EFECTUADO"] == DBNull.Value)
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = (DateTime)Verba["DATA_PAGAMENTO_EFECTUADO"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_VALOR_PAGAMENTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.Precision = 10;
                    pr.Scale = 2;
                    pr.ParameterName = "V_VALOR_PAGAMENTO";
                    if (Verba["VALOR_PAGAMENTO"] == DBNull.Value || Verba["VALOR_PAGAMENTO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Verba["VALOR_PAGAMENTO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COMP_CONCESSIONARIO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_COMP_CONCESSIONARIO";
                    if (Verba["COMP_CONCESSIONARIO"] == DBNull.Value || Verba["COMP_CONCESSIONARIO"].ToString().Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Verba["COMP_CONCESSIONARIO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_TIPO_PG
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 2;
                    pr.ParameterName = "V_TIPO_PG";
                    pr.Value = Verba["TIPO_PG"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_TTS
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 20;
                    pr.ParameterName = "V_TTS";
                    pr.Value = Verba["TTS"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                cm.Parameters.Clear();
                #endregion

                #region INSERIR MATERIAL VISIBILIDADE
                cm.CommandText = "PK_SRA.SP_MATERIAL_INS";
                Material.DefaultView.RowFilter = "";
                Material.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Mat in Material.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COD_MATERIAL
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_COD_MATERIAL";
                    if (Mat["COD_MATERIAL"] == DBNull.Value || Mat["COD_MATERIAL"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = int.Parse(Mat["COD_MATERIAL"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_QUANTIDADE
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_QUANTIDADE";
                    if (Mat["QUANTIDADE"] == DBNull.Value || Mat["QUANTIDADE"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = int.Parse(Mat["QUANTIDADE"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_VALOR
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.Precision = 10;
                    pr.Scale = 2;
                    pr.ParameterName = "V_VALOR";
                    if (Mat["VALOR"] == DBNull.Value || Mat["VALOR"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Mat["VALOR"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_OBSERVACOES
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 200;
                    pr.ParameterName = "V_OBSERVACOES";
                    pr.Value = Mat["OBSERVACOES"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                cm.Parameters.Clear();
                #endregion
                #endregion

                #region COPYFILES
                DataTable dtFiles =  this.getContratoFiles(oldIdContrato);
                if(dtFiles != null && dtFiles.Rows.Count >0)
                {
                    foreach (DataRow dr in dtFiles.Rows)
                    {
                        int idFile = 0;
                        this.SaveContratoFiles(ref idFile, idContrato, dr["FILE_PATH"].ToString(), dr["INSERT_USER"].ToString().ToString(), dr["FILE_NAME"].ToString());
                    }
                }
                #endregion

                tr.Commit();
                return numContrato;

            }
            catch (myDBException exp)
            {
                tr.Rollback();
                throw;
            }
            catch (OracleException exp)
            {
                tr.Rollback();
                throw new myDBException("DAL -> N�o foi poss�vel guardar o contrato.", exp);
            }
            catch (Exception exp)
            {
                tr.Rollback();
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.UpdateContrato()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        /// <summary>
        /// Insert new Contract
        /// TODO: 99 - Preencher Documenta��o
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="Contrato_Concessionario"></param>
        /// <param name="Contrato_Num"></param>
        /// <param name="Contrato_NumAnterior"></param>
        /// <param name="Contrato_NumSeguinte"></param>
        /// <param name="Contrato_Classif"></param>
        /// <param name="Contrato_Tipo"></param>
        /// <param name="Contrato_Data"></param>
        /// <param name="Contrato_Nivel"></param>
        /// <param name="Contrato_GrupoEconomico"></param>
        /// <param name="Contrato_SubGrupoEconomico"></param>
        /// <param name="Contrato_Status"></param>
        /// <param name="Contrato_Observacoes"></param>
        /// <param name="Contrato_Observacoes_Motivo"></param>
        /// <param name="PV_NomeFirma"></param>
        /// <param name="PV_MoradaSede"></param>
        /// <param name="PV_Localidade"></param>
        /// <param name="PV_RamoActividade"></param>
        /// <param name="PV_Matricula"></param>
        /// <param name="PV_MatriculaNum"></param>
        /// <param name="PV_CapitalSoial"></param>
        /// <param name="PV_Nif"></param>
        /// <param name="PV_Designacao"></param>
        /// <param name="PV_MoradaPtVenda"></param>
        /// <param name="COND_Objectivo"></param>
        /// <param name="COND_DataInicio"></param>
        /// <param name="COND_DataFim"></param>
        /// <param name="COND_QuandoAtingir"></param>
        /// <param name="COND_AnosRenovacao"></param>
        /// <param name="COND_ISContratoFormal"></param>
        /// <param name="COND_ISPooc"></param>
        /// <param name="COND_ISFornFrio"></param>
        /// <param name="CONT_Previsao"></param>
        /// <param name="CONT_PrazoPagamento"></param>
        /// <param name="GamasGlobal"></param>
        /// <param name="FASE_NIB"></param>
        /// <param name="FASE_BANCO"></param>
        /// <param name="FASE_BANCO_DEPENDENCIA"></param>
        /// <param name="FASE_CompCompras"></param>
        /// <param name="User"></param>
        /// <param name="Clientes"></param>
        /// <param name="Gamas"></param>
        /// <param name="Exploracao"></param>
        /// <param name="Equipamentos"></param>
        /// <param name="Contrapartidas"></param>
        /// <param name="Bonus"></param>
        /// <param name="BonusGlobal"></param>
        /// <param name="Verbas"></param>
        /// <param name="Material"></param>
        /// <returns></returns>
        public string SaveContrato(
            ref int idContrato,
            string Contrato_Concessionario,
            string Contrato_Num,
            string Contrato_NumAnterior,
            string Contrato_NumSeguinte,
            string Contrato_Classif,
            string Contrato_Tipo,
            DateTime Contrato_Data,
            string Contrato_Nivel,
            string Contrato_GrupoEconomico,
            string Contrato_SubGrupoEconomico,
            string Contrato_Status,
            string Contrato_Observacoes,
            string Contrato_Observacoes_Motivo,
            string PV_NomeFirma,
            string PV_MoradaSede,
            string PV_Localidade,
            string PV_RamoActividade,
            string PV_Matricula,
            string PV_MatriculaNum,
            string PV_CapitalSoial,
            string PV_Nif,
            string PV_Designacao,
            string PV_MoradaPtVenda,
            string COND_Objectivo,
            DateTime COND_DataInicio,
            DateTime COND_DataFim,
            string COND_QuandoAtingir,
            string COND_AnosRenovacao,
            string COND_ISContratoFormal,
            string COND_ISPooc,
            string COND_ISFornFrio,
            string CONT_Previsao,
            string CONT_PrazoPagamento,
            DataTable GamasGlobal,
            string FASE_NIB,
            string FASE_BANCO,
            string FASE_BANCO_DEPENDENCIA,
            string FASE_CompCompras,
            string User,
            DataTable Clientes,
            DataTable Gamas,
            DataTable Exploracao,
            DataTable Equipamentos,
            DataTable Contrapartidas,
            DataTable Bonus,
            DataTable BonusGlobal,
            DataTable Verbas,
            DataTable Material,
            Guid Instance_ID,
            string INSTANCE_STATE,
            string IS_AUTHORIZED,
            string DEFAULT_COD_CONC,
            string IS_SENT_TO_RENEWAL,
            string IS_ACTIVE,
            string IS_IMPORTADO
            )
        {
            string numContrato = "";

            OracleConnection cn = new OracleConnection(this.ConnectionString);
            OracleCommand cm = cn.CreateCommand();
            OracleTransaction tr = null;

            try
            {
                #region CONNECTION
                cn.Open();
                tr = cn.BeginTransaction();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                #endregion

                #region CRIAR PARAMETROS DO CONTRATO
                #region DADOS DO CONTRATO
                #region  V_CONCESSIONARIO
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_CONCESSIONARIO";
                pr.Value = Contrato_Concessionario;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_ID_CONTRACTO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.InputOutput;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRACTO";
                cm.Parameters.Add(pr);
                #endregion

                #region  V_ID_PROCESSO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.InputOutput;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_ID_PROCESSO";
                if (Contrato_Num.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = Contrato_Num;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_CLASSIFICACAO_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_CLASSIFICACAO_CONTRATO";
                pr.Value = Contrato_Classif;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_TIPO_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 3;
                pr.ParameterName = "V_TIPO_CONTRATO";
                pr.Value = Contrato_Tipo;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_DATA_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.DbType = DbType.DateTime;
                pr.ParameterName = "V_DATA_CONTRATO";
                pr.Value = Contrato_Data;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_NIVEL_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 3;
                pr.ParameterName = "V_NIVEL_CONTRATO";
                pr.Value = Contrato_Nivel;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_COD_GRUPO_ECONOMICO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 3;
                pr.ParameterName = "V_COD_GRUPO_ECONOMICO";
                pr.Value = Contrato_GrupoEconomico;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_COD_SUBGRUPO_ECONOMICO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 3;
                pr.ParameterName = "V_COD_SUBGRUPO_ECONOMICO";
                pr.Value = Contrato_SubGrupoEconomico;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_NUM_PROCESSO_ANTERIOR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_NUM_PROCESSO_ANTERIOR";
                if (Contrato_NumAnterior.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = Contrato_NumAnterior;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_NUM_PROCESSO_SEGUINTE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_NUM_PROCESSO_SEGUINTE";
                if (Contrato_NumSeguinte.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = Contrato_NumSeguinte;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_STATUS
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_STATUS";
                pr.Value = Contrato_Status;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_OBSERVACOES
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 4000;
                pr.ParameterName = "V_OBSERVACOES";
                pr.Value = Contrato_Observacoes;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_OBSERVACOES_MOTIVO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 4000;
                pr.ParameterName = "V_OBSERVACOES_MOTIVO";
                pr.Value = Contrato_Observacoes_Motivo;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region DADOS DO PONTO DE VENDA
                #region   V_NOME_FIRMA
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_NOME_FIRMA";
                pr.Value = PV_NomeFirma;
                cm.Parameters.Add(pr);
                #endregion

                #region   V_MORADA_SEDE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 255;
                pr.ParameterName = "V_MORADA_SEDE";
                pr.Value = PV_MoradaSede;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_LOCALIDADE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_LOCALIDADE";
                pr.Value = PV_Localidade;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_RAMO_ACTIVIDADE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 255;
                pr.ParameterName = "V_RAMO_ACTIVIDADE";
                pr.Value = PV_RamoActividade;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_MATRICULA
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_MATRICULA";
                pr.Value = PV_Matricula;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_MATRICULA_NUM
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_MATRICULA_NUM";
                pr.Value = PV_MatriculaNum;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_CAPITAL_SOCIAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_CAPITAL_SOCIAL";
                pr.Value = PV_CapitalSoial;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_NIF
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.ParameterName = "V_NIF";
                pr.Size = 20;
                pr.Value = PV_Nif;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_DESIGNACAO_PT_VENDA
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 255;
                pr.ParameterName = "V_DESIGNACAO_PT_VENDA";
                pr.Value = PV_Designacao;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_MORADA_PT_VENDA
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 255;
                pr.ParameterName = "V_MORADA_PT_VENDA";
                pr.Value = PV_MoradaPtVenda;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region DADOS DAS CONDI��ES
                #region  V_OBJECTIVO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_OBJECTIVO";
                pr.Value = COND_Objectivo;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_IS_POOC
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_IS_POOC";
                pr.Value = COND_ISPooc;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_DATA_INICIO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.ParameterName = "V_DATA_INICIO";
                pr.Value = COND_DataInicio.Date;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_DATA_FIM
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.ParameterName = "V_DATA_FIM";
                if (COND_DataFim.Equals(DateTime.MinValue))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = COND_DataFim.Date;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_DATA_FIM_EFECTIVO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Date;
                pr.ParameterName = "V_DATA_FIM_EFECTIVO";
                if (COND_DataFim.Equals(DateTime.MinValue))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = (COND_AnosRenovacao.Equals("") ? COND_DataFim : COND_DataFim.AddYears(int.Parse(COND_AnosRenovacao))).Date;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_QUANTIA_FIM
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_QUANTIA_FIM";
                if (COND_QuandoAtingir.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = int.Parse(COND_QuandoAtingir);
                cm.Parameters.Add(pr);
                #endregion

                #region  V_ANOS_RENOVACAO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ANOS_RENOVACAO";
                if (COND_AnosRenovacao.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = int.Parse(COND_AnosRenovacao);
                cm.Parameters.Add(pr);
                #endregion

                #region  V_IS_CONTRATO_FORMAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_IS_CONTRATO_FORMAL";
                pr.Value = COND_ISContratoFormal;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_IS_FORN_FRIO_IGLO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_IS_FORN_FRIO_IGLO";
                pr.Value = COND_ISFornFrio;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region DADOS DAS CONTRAPARTIDAS GLOBAIS
                #region  V_PREVISAO_VENDAS_GLOBAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_PREVISAO_VENDAS_GLOBAL";
                if (CONT_Previsao == null || CONT_Previsao.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = int.Parse(CONT_Previsao);
                cm.Parameters.Add(pr);
                #endregion

                #region  V_PRAZO_PAGAMENTO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.ParameterName = "V_PRAZO_PAGAMENTO";
                pr.Value = CONT_PrazoPagamento;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region DADOS DO FASEAMENTO
                #region  V_NIB_CLIENTE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.ParameterName = "V_NIB_CLIENTE";
                pr.Size = 50;
                pr.Value = FASE_NIB;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_BANCO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.ParameterName = "V_BANCO";
                pr.Size = 100;
                pr.Value = FASE_BANCO;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_BANCO_DEPENDENCIA
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.ParameterName = "V_BANCO_DEPENDENCIA";
                pr.Size = 100;
                pr.Value = FASE_BANCO_DEPENDENCIA;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_COMP_CONC_COMPRAS
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Decimal;
                pr.ParameterName = "V_COMP_CONC_COMPRAS";
                if (FASE_CompCompras.Equals(""))
                    pr.Value = DBNull.Value;
                else
                    pr.Value = decimal.Parse(FASE_CompCompras);
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region DADOS USER
                /// V_INSERT_USER			   VARCHAR2
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 20;
                pr.ParameterName = "V_INSERT_USER";
                pr.Value = User;
                cm.Parameters.Add(pr);
                #endregion

                #region V_EXISTS
                ///  V_EXISTS			   VARCHAR2
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_EXISTS";
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_INSTANCE_ID
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 36;
                pr.ParameterName = "V_INSTANCE_ID";
                pr.Value = DBNull.Value;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_INSTANCE_STATE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 24;
                pr.ParameterName = "V_INSTANCE_STATE";
                pr.Value = INSTANCE_STATE;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_IS_AUTHORIZED
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_AUTHORIZED";
                pr.Value = IS_AUTHORIZED;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_DEFAULT_COD_CONC
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 3;
                pr.ParameterName = "V_DEFAULT_COD_CONC";
                pr.Value = DEFAULT_COD_CONC;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_IS_SENT_TO_RENEWAL
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_SENT_TO_RENEWAL";
                pr.Value = IS_SENT_TO_RENEWAL;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_IS_ACTIVE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_ACTIVE";
                pr.Value = IS_ACTIVE;
                cm.Parameters.Add(pr);
                #endregion

                #region DADOS V_IS_IMPORTADO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Char;
                pr.Size = 1;
                pr.ParameterName = "V_IS_IMPORTADO";
                pr.Value = IS_IMPORTADO;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region INSERIR CONTRATO
                cm.CommandText = "PK_SRA.SP_CONTRATO_INS";
                cm.ExecuteNonQuery();
                if ((int)cm.Parameters["V_EXISTS"].Value != 0)
                    throw new CodeExistsException("O N� de Processo J� Existe.", null);
                idContrato = (int)cm.Parameters["V_ID_CONTRACTO"].Value;
                numContrato = cm.Parameters["V_ID_PROCESSO"].Value.ToString();
                cm.Parameters.Clear();
                #endregion

                #region INSERIR TABELAS ADICIONAIS
                #region INSERIR CLIENTES
                if (Contrato_Nivel == "LE")
                {
                    Clientes.DefaultView.RowFilter = "";
                    Clientes.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                    foreach (DataRowView client in Clientes.DefaultView)
                    {
                        ClientesInsert(
                            cm,
                            idContrato,
                            client["DATA_INICIO"] == null || client["DATA_INICIO"] == DBNull.Value ? null : (DateTime?)client["DATA_INICIO"],
                            client["DATA_FIM"] == null || client["DATA_FIM"] == DBNull.Value ? null : (DateTime?)client["DATA_FIM"],
                            client["COD_LOCAL_ENTREGA"].ToString(),
                            client["COD_GRUPO_ECONOMICO_CLI"].ToString(),
                            client["COD_SUBGRUPO_ECONOMICO_CLI"].ToString(),
                            client["COD_CLIENTE"].ToString()
                        );
                    }
                }
                #endregion

                #region INSERIR CONDICOES
                #region GAMAS
                cm.CommandText = "PK_SRA.SP_GAMA_INS";
                Gamas.DefaultView.RowFilter = "";
                Gamas.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Gama in Gamas.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COD_GRUPO_PRODUTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 4;
                    pr.ParameterName = "V_COD_GRUPO_PRODUTO";
                    pr.Value = Gama["COD_GRUPO_PRODUTO"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }

                cm.Parameters.Clear();
                #endregion

                #region EXPLORACAO
                cm.CommandText = "PK_SRA.SP_EXPLORACAO_INS";
                Exploracao.DefaultView.RowFilter = "";
                Exploracao.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Exp in Exploracao.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_TIPO_EXPLORACAO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 2;
                    pr.ParameterName = "V_TIPO_EXPLORACAO";
                    if (Exploracao.Columns.Contains("TIPO_EXPLORACAO"))
                    {
                        pr.Value = Exp["TIPO_EXPLORACAO"];
                    }
                    else
                    {
                        pr.Value = Exp["COD_EXPLORACAO"];
                    }
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }

                cm.Parameters.Clear();
                #endregion

                #region EQUIPAMENTO
                cm.CommandText = "PK_SRA.SP_EQUIPAMENTO_INS";
                Equipamentos.DefaultView.RowFilter = "";
                Equipamentos.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Equipamento in Equipamentos.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COD_EQUIPAMENTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 4;
                    pr.ParameterName = "V_COD_EQUIPAMENTO";
                    pr.Value = Equipamento["COD_TIPO_EQUIPAMENTO"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_QUANTIDADE
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_QUANTIDADE";
                    pr.Value = int.Parse(Equipamento["QUANTIDADE"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }

                cm.Parameters.Clear();
                #endregion
                #endregion

                #region INSERIR CONTRAPARTIDAS
                Contrapartidas.DefaultView.RowFilter = "";
                Contrapartidas.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Contrapartida in Contrapartidas.DefaultView)
                {
                    cm.CommandText = "PK_SRA.SP_CONTRAPARTIDA_INS";
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_ID_CONTRAPARTIDA
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.InputOutput;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRAPARTIDA";
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_COD_GRUPO_PRODUTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 4;
                    pr.ParameterName = "V_COD_GRUPO_PRODUTO";
                    pr.Value = Contrapartida["COD_GRUPO_PRODUTO"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_PREVISAO_VENDAS
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_PREVISAO_VENDAS";
                    if (Contrapartida["PREVISAO_VENDAS"] == DBNull.Value || Contrapartida["PREVISAO_VENDAS"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = int.Parse(Contrapartida["PREVISAO_VENDAS"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_DESCONTO_FACTURA
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_DESCONTO_FACTURA";
                    if (Contrapartida["DESCONTO_FACTURA"] == DBNull.Value || Contrapartida["DESCONTO_FACTURA"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Contrapartida["DESCONTO_FACTURA"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_COMP_CONCESSIONARIO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_COMP_CONCESSIONARIO";
                    if (Contrapartida["COMP_CONCESSIONARIO"] == DBNull.Value || Contrapartida["COMP_CONCESSIONARIO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Contrapartida["COMP_CONCESSIONARIO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_PRAZO_PAGAMENTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Char;
                    pr.ParameterName = "V_PRAZO_PAGAMENTO";
                    if (Contrapartida["PRAZO_PAGAMENTO"] == DBNull.Value || Contrapartida["PRAZO_PAGAMENTO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = Contrapartida["PRAZO_PAGAMENTO"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    #region EXECUTAR
                    cm.ExecuteNonQuery();
                    int idContrapartida = int.Parse(cm.Parameters["V_ID_CONTRAPARTIDA"].Value.ToString());
                    #endregion

                    #region INSERIR BONUS
                    cm.CommandText = "PK_SRA.SP_BONUS_INS";
                    foreach (DataRow Bon in Bonus.Select("ID_CONTRAPARTIDA = " + Contrapartida["ID_CONTRAPARTIDA"].ToString(), "", DataViewRowState.CurrentRows))
                    {
                        cm.Parameters.Clear();

                        #region PARAMETROS
                        #region  V_ID_CONTRAPARTIDA
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Int32;
                        pr.DbType = DbType.Int32;
                        pr.ParameterName = "V_ID_CONTRAPARTIDA";
                        pr.Value = idContrapartida;
                        cm.Parameters.Add(pr);
                        #endregion

                        #region  V_VALOR_LIMITE_MIN
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Int32;
                        pr.DbType = DbType.Int32;
                        pr.ParameterName = "V_VALOR_LIMITE_MIN";
                        if (Bon["VALOR_LIMITE_MIN"] == DBNull.Value || Bon["VALOR_LIMITE_MIN"].Equals(""))
                            pr.Value = DBNull.Value;
                        else
                            pr.Value = int.Parse(Bon["VALOR_LIMITE_MIN"].ToString());
                        cm.Parameters.Add(pr);
                        #endregion

                        #region V_DESCONTO
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Decimal;
                        pr.ParameterName = "V_DESCONTO";
                        if (Bon["DESCONTO"] == DBNull.Value || Bon["DESCONTO"].Equals(""))
                            pr.Value = DBNull.Value;
                        else
                            pr.Value = decimal.Parse(Bon["DESCONTO"].ToString());
                        cm.Parameters.Add(pr);
                        #endregion

                        #region V_COMP_CONCESSIONARIO
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Decimal;
                        pr.ParameterName = "V_COMP_CONCESSIONARIO";
                        if (Bon["COMP_CONCESSIONARIO"] == DBNull.Value || Bon["COMP_CONCESSIONARIO"].Equals(""))
                            pr.Value = DBNull.Value;
                        else
                            pr.Value = decimal.Parse(Bon["COMP_CONCESSIONARIO"].ToString());
                        cm.Parameters.Add(pr);
                        #endregion

                        #region  V_TIPO
                        pr = cm.CreateParameter();
                        pr.Direction = ParameterDirection.Input;
                        pr.OracleDbType = OracleDbType.Char;
                        pr.Size = 3;
                        pr.ParameterName = "V_TIPO";
                        if (Bon["TIPO"] == DBNull.Value || Bon["TIPO"].Equals(""))
                            pr.Value = DBNull.Value;
                        else
                            pr.Value = Bon["TIPO"].ToString();
                        cm.Parameters.Add(pr);
                        #endregion
                        #endregion

                        cm.ExecuteNonQuery();
                    }
                    #endregion
                }

                cm.Parameters.Clear();
                #endregion

                #region INSERIR CONTRAPARTIDAS GLOBAIS
                #region GAMAS
                cm.CommandText = "PK_SRA.SP_GAMA_GLOBAL_INS";
                GamasGlobal.DefaultView.RowFilter = "";
                GamasGlobal.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Gama in GamasGlobal.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COD_GRUPO_PRODUTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 4;
                    pr.ParameterName = "V_COD_GRUPO_PRODUTO";
                    pr.Value = Gama["COD_GRUPO_PRODUTO"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                cm.Parameters.Clear();
                #endregion

                #region BONUS
                cm.CommandText = "PK_SRA.SP_BONUS_GLOBAL_INS";
                BonusGlobal.DefaultView.RowFilter = "";
                BonusGlobal.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Bon in BonusGlobal.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_VALOR_LIMITE_MIN
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_VALOR_LIMITE_MIN";
                    if (Bon["VALOR_LIMITE_MIN"] == DBNull.Value || Bon["VALOR_LIMITE_MIN"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = int.Parse(Bon["VALOR_LIMITE_MIN"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_DESCONTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_DESCONTO";
                    if (Bon["DESCONTO"] == DBNull.Value || Bon["DESCONTO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Bon["DESCONTO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_COMP_CONCESSIONARIO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_COMP_CONCESSIONARIO";
                    if (Bon["COMP_CONCESSIONARIO"] == DBNull.Value || Bon["COMP_CONCESSIONARIO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Bon["COMP_CONCESSIONARIO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_TIPO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Char;
                    pr.Size = 3;
                    pr.ParameterName = "V_TIPO";
                    if (Bon["TIPO"] == DBNull.Value || Bon["TIPO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = Bon["TIPO"].ToString();
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                #endregion
                #endregion

                #region INSERIR FASEAMENTO
                cm.CommandText = "PK_SRA.SP_FASEAMENTO_INS";
                Verbas.DefaultView.RowFilter = "";
                Verbas.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Verba in Verbas.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region  V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_DATA_PAGAMENTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Date;
                    pr.ParameterName = "V_DATA_PAGAMENTO";
                    pr.Value = (DateTime)Verba["DATA_PAGAMENTO"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_DATA_PAGAMENTO_EFECTUADO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Date;
                    pr.ParameterName = "V_DATA_PAGAMENTO_EFECTUADO";
                    if (Verba["DATA_PAGAMENTO_EFECTUADO"] == DBNull.Value)
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = (DateTime)Verba["DATA_PAGAMENTO_EFECTUADO"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_VALOR_PAGAMENTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.Precision = 10;
                    pr.Scale = 2;
                    pr.ParameterName = "V_VALOR_PAGAMENTO";
                    if (Verba["VALOR_PAGAMENTO"] == DBNull.Value || Verba["VALOR_PAGAMENTO"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Verba["VALOR_PAGAMENTO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_COMP_CONCESSIONARIO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.ParameterName = "V_COMP_CONCESSIONARIO";
                    if (Verba["COMP_CONCESSIONARIO"] == DBNull.Value || Verba["COMP_CONCESSIONARIO"].ToString().Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Verba["COMP_CONCESSIONARIO"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_TIPO_PG
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 2;
                    pr.ParameterName = "V_TIPO_PG";
                    pr.Value = Verba["TIPO_PG"];
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_TTS
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 20;
                    pr.ParameterName = "V_TTS";
                    pr.Value = Verba["TTS"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                cm.Parameters.Clear();
                #endregion

                #region INSERIR MATERIAL VISIBILIDADE
                cm.CommandText = "PK_SRA.SP_MATERIAL_INS";
                Material.DefaultView.RowFilter = "";
                Material.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView Mat in Material.DefaultView)
                {
                    cm.Parameters.Clear();

                    #region PARAMETROS
                    #region V_ID_CONTRACTO
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_ID_CONTRACTO";
                    pr.Value = idContrato;
                    cm.Parameters.Add(pr);
                    #endregion

                    #region  V_COD_MATERIAL
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_COD_MATERIAL";
                    if (Mat["COD_MATERIAL"] == DBNull.Value || Mat["COD_MATERIAL"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = int.Parse(Mat["COD_MATERIAL"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_QUANTIDADE
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Int32;
                    pr.DbType = DbType.Int32;
                    pr.ParameterName = "V_QUANTIDADE";
                    if (Mat["QUANTIDADE"] == DBNull.Value || Mat["QUANTIDADE"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = int.Parse(Mat["QUANTIDADE"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_VALOR
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Decimal;
                    pr.Precision = 10;
                    pr.Scale = 2;
                    pr.ParameterName = "V_VALOR";
                    if (Mat["VALOR"] == DBNull.Value || Mat["VALOR"].Equals(""))
                        pr.Value = DBNull.Value;
                    else
                        pr.Value = decimal.Parse(Mat["VALOR"].ToString());
                    cm.Parameters.Add(pr);
                    #endregion

                    #region V_OBSERVACOES
                    pr = cm.CreateParameter();
                    pr.Direction = ParameterDirection.Input;
                    pr.OracleDbType = OracleDbType.Varchar2;
                    pr.Size = 200;
                    pr.ParameterName = "V_OBSERVACOES";
                    pr.Value = Mat["OBSERVACOES"];
                    cm.Parameters.Add(pr);
                    #endregion
                    #endregion

                    cm.ExecuteNonQuery();
                }
                cm.Parameters.Clear();
                #endregion
                #endregion

                tr.Commit();
                return numContrato;
            }
            catch (myDBException exp)
            {
                tr.Rollback();
                throw;
            }
            catch (OracleException exp)
            {
                tr.Rollback();
                throw new myDBException("DAL -> N�o foi poss�vel guardar o contrato.", exp);
            }
            catch (Exception exp)
            {
                tr.Rollback();
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.SaveContrato'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public int SaveContratoFiles(
           ref int idFile,
           int idContrato,
           string file_Path,
           string username,
           string file_Name
           )
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            OracleCommand cm = cn.CreateCommand();
            OracleTransaction tr = null;

            try
            {
                #region CONNECTION
                cn.Open();
                tr = cn.BeginTransaction();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                #endregion

                #region CRIAR PARAMETROS DO CONTRATO
                #region  V_ID_FILE
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.InputOutput;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_FILE";
                cm.Parameters.Add(pr);
                #endregion

                #region  V_ID_CONTRATO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRATO";
                pr.Value = idContrato;
                cm.Parameters.Add(pr);
                #endregion

                #region  file_Path
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 500;
                pr.ParameterName = "V_FILE_PATH";
                pr.Value = file_Path;
                cm.Parameters.Add(pr);
                #endregion

                #region  username
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_INSERT_USER";
                pr.Value = username;
                cm.Parameters.Add(pr);
                #endregion

                #region  file_Name
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 50;
                pr.ParameterName = "V_FILE_NAME";
                pr.Value = file_Name;
                cm.Parameters.Add(pr);
                #endregion

                cm.CommandText = "PK_SRA.SP_CONTRATO_FILE_INS";
                cm.ExecuteNonQuery();
                idFile = (int)cm.Parameters["V_ID_FILE"].Value;
                cm.Parameters.Clear();
                #endregion
                #endregion

                tr.Commit();
                return idFile;
            }
            catch (myDBException exp)
            {
                tr.Rollback();
                throw;
            }
            catch (OracleException exp)
            {
                tr.Rollback();
                throw new myDBException("DAL -> N�o foi poss�vel guardar o contrato.", exp);
            }
            catch (Exception exp)
            {
                tr.Rollback();
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.SaveContrato'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable getContratoFiles(int idContrato)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_FILE_GET";
                #endregion

                #region PARAMETERS
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRATO";
                pr.Value = idContrato;
                cm.Parameters.Add(pr);

                /// CUR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista de ficheiros de contrato.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.getContratoFiles()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }

        public int delContratoFiles(int idFile)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_SRA.SP_CONTRATO_FILE_DEL";
                #endregion

                #region PARAMETERS
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_FILE";
                pr.Value = idFile;
                cm.Parameters.Add(pr);
                #endregion

                #region EXECUTE
                cm.Connection.Open();
                return cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel apagar ficheiro.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'SRA.delContratoFiles()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        #region INTERFACES
        #region SELECTABLE
        /// <summary>
        /// Get Data From Source Table (Contrato)
        /// </summary>
        /// <param name="Where">Filter Condition</param>
        /// <returns></returns>    
        public DataTable Select(string Where)
        {
            return this.getContrato(Where);
        }
        #endregion
        #endregion

        #region EMAIL
        public bool SendMail(int idContrato)
        {
            return SendMail(idContrato, false);
        }

        public bool SendMail(int idContrato, bool useSMIAddress)
        {
            try
            {
                SmtpClient c;
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["SMTP_DEBUG"].ToString()))
                {
                    c = new SmtpClient("localhost");
                    c.UseDefaultCredentials = true;
                    c.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    c.PickupDirectoryLocation = @"c:\temp\";
                }
                else
                {
                    string mailServer = ConfigurationManager.AppSettings["SMTP_SERVER"].ToString();
                    if (mailServer.Length == 0)
                        mailServer = "exchange-bh.eu.unilever.com";
                    c = new SmtpClient(mailServer);
                    c.Port = 25;
                }

                string emailAddresses;
                Stream pdfFile;

                SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
                DataTable emails = DBsra.get_Contrato_Emails(idContrato);
                if (emails.Rows.Count > 0)
                {
                    foreach (DataRow emailList in emails.Rows)

                    {
                        if (emailList["email"].ToString().Trim().Equals(""))
                        {
                            emailAddresses = ConfigurationManager.AppSettings["SMTP_DEFAULT_RECEIVER"];
                        }
                        else
                        {
                            emailAddresses = emailList["email"].ToString().Trim();
                        }

                        if (useSMIAddress)
                        {
                            emailAddresses += ", " + ConfigurationManager.AppSettings["SMTP_SMI_ADDRESS"];
                        }

                        string corpo = ConfigurationManager.AppSettings["SMTP_MESSAGE_CC_BODY"].Replace(@"\n", @"<br />");
                        MailMessage mail = new MailMessage(
                            ConfigurationManager.AppSettings["SMTP_FROM"].ToString()
                            , emailAddresses
                            , ConfigurationManager.AppSettings["SMTP_MESSAGE_CC_SUBJECT"]
                            , corpo);
                        mail.IsBodyHtml = true;

                        string filename = "ContratoNum_" + idContrato.ToString() + ".pdf";
                        pdfFile = GetPDF(idContrato);
                        mail.Attachments.Add(new Attachment(pdfFile, filename));
                        c.Send(mail);
                    }
                }
                else //s� para ser usado pelo servi�o
                {
                    emailAddresses = ConfigurationManager.AppSettings["SMTP_SMI_ADDRESS"];

                    string corpo = ConfigurationManager.AppSettings["SMTP_MESSAGE_CC_BODY"].Replace(@"\n", @"<br />");
                    MailMessage mail = new MailMessage(
                        ConfigurationManager.AppSettings["SMTP_FROM"].ToString()
                        , emailAddresses
                        , ConfigurationManager.AppSettings["SMTP_MESSAGE_CC_SUBJECT"]
                        , corpo);
                    mail.IsBodyHtml = true;

                    string filename = "ContratoNum_" + idContrato.ToString() + ".pdf";
                    pdfFile = GetPDF(idContrato);
                    mail.Attachments.Add(new Attachment(pdfFile, filename));
                    c.Send(mail);
                }
                return true;
            }                
            catch (Exception ex)
            {
                ErrorLog(ConfigurationManager.AppSettings["MailLog_File"],"SRA_WF:" + ex.Message);
                return false;
            }
        }

        private Stream GetPDF(int idContrato)
        {
            //string strRequest = @"http://flhsiis01/ReportServer?/SRA/ReportSRA_full";
            string strRequest = System.Configuration.ConfigurationManager.AppSettings["ReportPathFull"].ToString();
            strRequest += @"&rs:format=pdf&rs:Command=Render&PARAM_ID_Contrato=" + idContrato.ToString();

            WebRequest request = WebRequest.Create(strRequest);
            
            string rUser = System.Configuration.ConfigurationManager.AppSettings["ReportUser"].ToString();
            string rPass = System.Configuration.ConfigurationManager.AppSettings["ReportPass"].ToString();
            string rDomain = System.Configuration.ConfigurationManager.AppSettings["ReportDomain"].ToString();
            string rTimeout = System.Configuration.ConfigurationManager.AppSettings["ReportTimeout"].ToString();
            int timeout;
            if(!int.TryParse(rTimeout, out timeout))
                timeout = 36000;
                
            request.Credentials = new NetworkCredential(rUser, rPass, rDomain);
            request.Timeout = timeout; // 6 minutes in milliseconds.
            request.Method = WebRequestMethods.Http.Get;
            request.ContentLength = 0;
            return request.GetResponse().GetResponseStream();
        }

        public void ErrorLog(string sPathName, string sErrMsg)
        {
            string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
            StreamWriter sw = new StreamWriter(sPathName, true);
            sw.WriteLine(sLogFormat + sErrMsg);
            sw.Flush();
            sw.Close();
        }
        #endregion

    }

    public enum wfStates
    {
        Draft, RenewalDraft, Printed, Delegated, Accepted, Received, InApproval, ManagerApproval, SalesApproval, FinantialApproval, TopApproval, Approved, Rejected
    }
}