using System;
using System.Data;
using Oracle.DataAccess;
using Oracle.DataAccess.Client;

[Serializable()]
public class AE : selectable
{
    #region FIELDS
    /// <summary>
    /// DataBase ConnectionString To Use In All Commands
    /// </summary>
    protected string ConnectionString;
    protected AE_Clientes Client;
    #endregion

    #region METHODS
    /// <summary>
    /// CONSTRUCTOR
    /// </summary>
    /// <param name="ConnString">ConnectionString to Source DataBase</param>
    public AE(string ConnString)
    {
        this.ConnectionString = ConnString;
        Client = new AE_Clientes(ConnString);
    }

    #region PRODUTOS
    /// <summary>
    /// Get Agrupamentos List
    ///    SP_AE_AGRUPAMENTO_PRODUTO_GET (CUR OUT SYS_REFCURSOR);
    /// </summary>
    /// <returns></returns>
    public DataTable AgrupamentosGet()
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_AGRUPAMENTO_PRODUTO_GET";
            #endregion

            #region PARAMETERS
            /// CUR
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_AGRUPAMENTO_PRODUTO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos agrupamentos.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.AgrupamentosGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Get Grupo Produto List
    ///    SP_AE_GRUPO_PRODUTO_GET 
    ///    (
    ///        V_ID_AGRUPAMENTO VARCHAR2,
    ///        CUR OUT SYS_REFCURSOR
    ///    );
    /// </summary>
    /// <returns></returns>
    public DataTable GrupoProdutoGet(string AgrupamentoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_GRUPO_PRODUTO_GET ";
            #endregion

            #region PARAMETERS
            #region V_ID_AGRUPAMENTO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 3;
            pr.DbType = DbType.String;
            pr.Value = AgrupamentoID;
            pr.ParameterName = "V_ID_AGRUPAMENTO";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_GRUPO_PRODUTO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos grupos de produto.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.GrupoProdutoGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }

    }
    /// <summary>
    /// Get SubGrupo Produto List
    ///    SP_AE_SUB_GRUPO_PRODUTO_GET 
    ///    (
    ///        V_ID_GRUPO_PRODUTO VARCHAR2,     
    ///        CUR OUT SYS_REFCURSOR
    ///    );
    /// </summary>
    /// <returns></returns>
    public DataTable SubGrupoProdutoGet(string GrupoProdutoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_SUB_GRUPO_PRODUTO_GET ";
            #endregion

            #region PARAMETERS
            #region V_ID_GRUPO_PRODUTO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 3;
            pr.DbType = DbType.String;
            pr.Value = GrupoProdutoID;
            pr.ParameterName = "V_ID_GRUPO_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_SUBGRUPO_PRODUTO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos sub-grupos de produto.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.SubGrupoProdutoGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }

    }
    /// <summary>
    /// Get Sub AGG Produto List
    ///    SP_AE_SUB_AGG_PRODUTO_GET 
    ///    (
    ///        V_ID_GRUPO_PRODUTO VARCHAR2,    
    ///        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
    ///        CUR OUT SYS_REFCURSOR
    ///    );
    /// </summary>
    /// <returns></returns>
    public DataTable SubAggProdutoGet(string GrupoProdutoID, string SubGrupoProdutoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_SUB_AGG_PRODUTO_GET ";
            #endregion

            #region PARAMETERS
            #region V_ID_GRUPO_PRODUTO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 3;
            pr.DbType = DbType.String;
            pr.Value = GrupoProdutoID;
            pr.ParameterName = "V_ID_GRUPO_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_SUB_GRUPO_PRODUTO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 3;
            pr.DbType = DbType.String;
            pr.Value = SubGrupoProdutoID;
            pr.ParameterName = "V_ID_SUB_GRUPO_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_SUB_AGG_PRODUTO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos sub AGG de produto.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.SubAggProdutoGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }

    }
    /// <summary>
    /// Get Sub AGG Produto List
    ///    SP_AE_PRODUTO_GET
    ///    (
    ///        V_ID_GRUPO_PRODUTO VARCHAR2,    
    ///        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
    ///        V_ID_SUB_AGG_PRODUTO VARCHAR2,
    ///        CUR OUT SYS_REFCURSOR
    ///    );
    /// </summary>
    /// <returns></returns>
    public DataTable ProdutoGet(string GrupoProdutoID, string SubGrupoProdutoID, string SubAggProdutoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_PRODUTO_GET ";
            #endregion

            #region PARAMETERS
            #region V_ID_GRUPO_PRODUTO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 3;
            pr.DbType = DbType.String;
            pr.Value = GrupoProdutoID;
            pr.ParameterName = "V_ID_GRUPO_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_SUB_GRUPO_PRODUTO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 3;
            pr.DbType = DbType.String;
            pr.Value = SubGrupoProdutoID;
            pr.ParameterName = "V_ID_SUB_GRUPO_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_SUB_AGG_PRODUTO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 4;
            pr.DbType = DbType.String;
            pr.Value = SubAggProdutoID;
            pr.ParameterName = "V_ID_SUB_AGG_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_PRODUTO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos produtos.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ProdutoGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Get Produto with criteria
    ///   SP_AE_PRODUTO_SEARCH
    ///   (
    ///     V_ID_PRODUTO VARCHAR2,    
    ///     V_NOME_PRODUTO VARCHAR2,
    ///     CUR OUT SYS_REFCURSOR
    ///   );
    /// </summary>
    /// <param name="C�digo"></param>
    /// <param name="Nome"></param>
    /// <returns></returns>
    public DataTable ProdutoSearch(string C�digo, string Nome)
    {   
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_PRODUTO_SEARCH ";
            #endregion

            #region PARAMETERS
            #region V_ID_PRODUTO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 9;
            pr.DbType = DbType.String;
            if(C�digo.Length > 0)
                pr.Value = C�digo;
            pr.ParameterName = "V_ID_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_NOME_PRODUTO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 40;
            pr.DbType = DbType.String;
            if (Nome.Length > 0)
                pr.Value = Nome;
            pr.ParameterName = "V_NOME_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_PRODUTO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos produtos.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ProdutoSearch()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    
    #endregion

    #region RECEP��O
    /// <summary>
    /// Get Agrupamentos List
    ///    SP_AE_EMISSOR_GET(CUR OUT SYS_REFCURSOR);
    /// </summary>
    /// <returns></returns>
    public DataTable EmissorGet()
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_EMISSOR_GET";
            #endregion

            #region PARAMETERS
            /// CUR
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_EMISSOR");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos emissores.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.EmissoreGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Get Agrupamentos List
    ///    SP_AE_CUSTOMER_SERVICE_GET(CUR OUT SYS_REFCURSOR);
    /// </summary>
    /// <returns></returns>
    public DataTable CustomerServiceGet()
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_CUSTOMER_SERVICE_GET";
            #endregion

            #region PARAMETERS
            /// CUR
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_CUSTOMER_SERVICE");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos CustomerServices.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.CustomerServiceGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    #endregion

    #region LOCAIS ENTREGA
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public DataTable ClientesGet()
    {
        return this.Client.ClientesGet("");
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="Where"></param>
    /// <returns></returns>
    public DataTable ClientesGet(string Where)
    {
        return this.Client.ClientesGet(Where);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <returns></returns>
    public DataTable ClientesGet(int ContratoID)
    {
        return this.Client.ClientesGet(ContratoID);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="CodLE"></param>
    /// <returns></returns>
    public int ClientesDel(int ContratoID, string CodLE)
    {
        return this.Client.ClientesDel(ContratoID, CodLE);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <returns></returns>
    public int ClientesDel(int ContratoID)
    {
        return this.Client.ClientesDel(ContratoID);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ContratoID"></param>
    /// <param name="CodLE"></param>
    /// <returns></returns>
    public int ClientesDel(OracleCommand cm, int ContratoID, string CodLE)
    {
        return this.Client.ClientesDel(cm, ContratoID, CodLE);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ContratoID"></param>
    /// <returns></returns>
    public int ClientesDel(OracleCommand cm, int ContratoID)
    {
        return this.Client.ClientesDel(cm, ContratoID);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="CodLE"></param>
    /// <param name="CodGrupoEconomico"></param>
    /// <param name="CodSubGrupoEconomico"></param>
    /// <param name="CodCliente"></param>
    /// <returns></returns>
    public int ClientesInsert(int ContratoID, string CodLE, string CodGrupoEconomico, string CodSubGrupoEconomico, string CodCliente)
    {
        return this.Client.ClientesInsert(ContratoID, CodLE, CodGrupoEconomico, CodSubGrupoEconomico, CodCliente);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ContratoID"></param>
    /// <param name="CodLE"></param>
    /// <param name="CodGrupoEconomico"></param>
    /// <param name="CodSubGrupoEconomico"></param>
    /// <param name="CodCliente"></param>
    /// <returns></returns>
    public int ClientesInsert(OracleCommand cm, int ContratoID, string CodLE, string CodGrupoEconomico, string CodSubGrupoEconomico, string CodCliente)
    {
        return this.Client.ClientesInsert(cm, ContratoID, CodLE, CodGrupoEconomico, CodSubGrupoEconomico, CodCliente);
    }
    #endregion

    #region GRUPO ECONOMICO
    /// <summary>
    /// Get grupo econ�mico List
    ///    SP_AE_GRUPO_ECONOMICO_GET (CUR OUT SYS_REFCURSOR);
    /// </summary>
    /// <returns></returns>
    public DataTable GrupoEconomicoGet()
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_GRUPO_ECONOMICO_GET";
            #endregion

            #region PARAMETERS
            /// CUR
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_GRUPO_ECONOMICO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos grupos econ�micos.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.GrupoEconomicoGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Get Grupo Produto List
    ///    SP_AE_SUBGRUPO_ECONOMICO_GET
    ///    (
    ///        V_ID_GRUPO_ECONOMICO VARCHAR2,    
    ///        CUR OUT SYS_REFCURSOR
    ///    );
    /// </summary>
    /// <param name="GrupoEconomicoID"></param>
    /// <returns></returns>
    public DataTable SubGrupoEconomicoGet(string GrupoEconomicoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_SUBGRUPO_ECONOMICO_GET ";
            #endregion

            #region PARAMETERS
            #region V_ID_GRUPO_ECONOMICO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 3;
            pr.DbType = DbType.String;
            pr.Value = GrupoEconomicoID;
            pr.ParameterName = "V_ID_GRUPO_ECONOMICO";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_SUBGRUPO_ECONOMICO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos sub grupos econ�micos.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.SubGrupoEconomicoGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }

    }
    #endregion

    #region ABASTECIMENTOS
    /// <summary>
    /// Get Agrupamentos List
    ///    SP_AE_CONCESSIONARIO_GET(CUR OUT SYS_REFCURSOR);
    /// </summary>
    /// <returns></returns>
    public DataTable ConcessionarioGet()
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_CONCESSIONARIO_GET";
            #endregion

            #region PARAMETERS
            /// CUR
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_CONCESSIONARIO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos concessionarios.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ConcessionarioGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Devolve a lista de abastecimentos do contrato    
    ///     PROCEDURE SP_AE_ABASTECIMENTO_GET 
    ///     (
    ///         V_ID_CONTRATO   INT,
    ///         CUR             OUT SYS_REFCURSOR
    ///     );
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <returns></returns>
    public DataTable AbastecimentoGet(int ContratoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_ABASTECIMENTO_GET";
            #endregion

            #region PARAMETERS
            /// V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);

            /// CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "T_AE_ABASTECIMENTO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista de abastecimentos do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.AbastecimentoGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="ConcessionarioID"></param>
    /// <param name="Facturacao"></param>
    /// <param name="Distribuicao"></param>
    /// <param name="Observacoes"></param>
    /// <returns></returns>
    public int AbastecimentoUpdate(int ContratoID, string ConcessionarioID, string Facturacao, string Distribuicao, string Observacoes)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.AbastecimentoUpdate(cm, ContratoID, ConcessionarioID, Facturacao, Distribuicao, Observacoes);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel actualizar o abastecimento '" + ConcessionarioID + "' do contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.AbastecimentoUpdate()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Actualiza actividade do contrato    
    ///     PROCEDURE SP_AE_ABASTECIMENTO_UPD
    ///     (
    ///         V_ID_CONTRATO INT,
    ///         V_COD_CONCESSIONARIO VARCHAR2,
    ///         V_FACTURACAO CHAR,
    ///         V_DISTRIBUICAO CHAR,
    ///         V_OBSERVACOES VARCHAR2
    ///     );
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ContratoID"></param>
    /// <param name="ConcessionarioID"></param>
    /// <param name="Facturacao"></param>
    /// <param name="Distribuicao"></param>
    /// <param name="Observacoes"></param>
    /// <returns></returns>
    public int AbastecimentoUpdate(OracleCommand cm, int ContratoID, string ConcessionarioID, string Facturacao, string Distribuicao, string Observacoes)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_ABASTECIMENTO_UPD";
            #endregion

            #region PARAMETERS

            #region V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_COD_CONCESSIONARIO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.Value = ConcessionarioID;
            pr.ParameterName = "V_COD_CONCESSIONARIO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_FACTURACAO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Char;
            pr.DbType = DbType.String;
            pr.Size = 1;
            pr.Value = Facturacao;
            pr.ParameterName = "V_FACTURACAO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_DISTRIBUICAO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Char;
            pr.DbType = DbType.String;
            pr.Size = 1;
            pr.Value = Distribuicao;
            pr.ParameterName = "V_DISTRIBUICAO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_OBSERVACOES
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 255;
            pr.Value = Observacoes;
            pr.ParameterName = "V_OBSERVACOES";
            cm.Parameters.Add(pr);
            #endregion        

            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel actualizar o abastecimento '" + ConcessionarioID + "' do contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.AbastecimentoUpdate()'.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="ConcessionarioID"></param>
    /// <returns></returns>
    public int AbastecimentoDelete(int ContratoID, string ConcessionarioID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.AbastecimentoDelete(cm, ContratoID,ConcessionarioID);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel apagar o abastecimento '" + ConcessionarioID + "' do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.AbastecimentoDelete()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Apagar a actividade do contrato    
    ///    PROCEDURE SP_AE_ABASTECIMENTO_DEL
    ///    (
    ///        V_ID_CONTRATO INT,
    ///        V_COD_CONCESSIONARIO VARCHAR2
    ///    );
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ContratoID"></param>
    /// <param name="ConcessionarioID"></param>
    /// <returns></returns>
    public int AbastecimentoDelete(OracleCommand cm, int ContratoID, string ConcessionarioID)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_ABASTECIMENTO_DEL";
            #endregion

            #region PARAMETERS
            #region V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_COD_CONCESSIONARIO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.Value = ConcessionarioID;
            pr.ParameterName = "V_COD_CONCESSIONARIO";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel apagar o abastecimento '" + ConcessionarioID + "' do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.AbastecimentoDelete()'.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="ConcessionarioID"></param>
    /// <param name="Facturacao"></param>
    /// <param name="Distribuicao"></param>
    /// <param name="Observacoes"></param>
    /// <returns></returns>
    public int AbastecimentoInsert(int ContratoID, string ConcessionarioID, string Facturacao, string Distribuicao, string Observacoes)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.AbastecimentoInsert(cm, ContratoID, ConcessionarioID, Facturacao, Distribuicao,Observacoes);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir o abastecimento no contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.AbastecimentoInsert()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Insere um abastecimento no contrato    
    ///     PROCEDURE SP_AE_ABASTECIMENTO_INS
    ///     (
    ///         V_ID_CONTRATO INT,
    ///         V_COD_CONCESSIONARIO VARCHAR2,
    ///         V_FACTURACAO CHAR,
    ///         V_DISTRIBUICAO CHAR,
    ///         V_OBSERVACOES VARCHAR2
    ///     );
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ContratoID"></param>
    /// <param name="ConcessionarioID"></param>
    /// <param name="Facturacao"></param>
    /// <param name="Distribuicao"></param>
    /// <param name="Observacoes"></param>
    /// <returns></returns>
    public int AbastecimentoInsert(OracleCommand cm, int ContratoID, string ConcessionarioID, string Facturacao, string Distribuicao, string Observacoes)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_ABASTECIMENTO_INS";
            #endregion

            #region PARAMETERS
            #region V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_COD_CONCESSIONARIO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.Value = ConcessionarioID;
            pr.ParameterName = "V_COD_CONCESSIONARIO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_FACTURACAO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Char;
            pr.DbType = DbType.String;
            pr.Size = 1;
            pr.Value = Facturacao;
            pr.ParameterName = "V_FACTURACAO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_DISTRIBUICAO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Char;
            pr.DbType = DbType.String;
            pr.Size = 1;
            pr.Value = Distribuicao;
            pr.ParameterName = "V_DISTRIBUICAO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_OBSERVACOES
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 255;
            pr.Value = Observacoes;
            pr.ParameterName = "V_OBSERVACOES";
            cm.Parameters.Add(pr);
            #endregion

            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir o abastecimento no contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.AbastecimentoInsert()'.", exp);
        }
    }

    #endregion

    #region ACTIVIDADES
    /// <summary>
    /// Get Agrupamentos List
    ///    SP_AE_TIPO_ACTIVIDADE_GET(CUR OUT SYS_REFCURSOR);
    /// </summary>
    /// <returns></returns>
    public DataTable TipoActividadeGet()
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_TIPO_ACTIVIDADE_GET";
            #endregion

            #region PARAMETERS
            /// CUR
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "LK_TIPO_ACTIVIDADE");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista dos tipos de actividade.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.TipoActividadeGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Devolve a lista de actividades do contrato    
    ///     SP_AE_ACTIVIDADE_GET 
    ///     (
    ///         V_ID_CONTRATO   INT,
    ///         CUR             OUT SYS_REFCURSOR
    ///     );
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <returns></returns>
    public DataTable ActividadeGet(int ContratoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_ACTIVIDADE_GET";
            #endregion

            #region PARAMETERS
            /// V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);

            /// CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "T_AE_ACTIVIDADE");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista de actividades do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ActividadeGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ActividadeID"></param>
    /// <param name="ContratoID"></param>
    /// <param name="TipoActividadeID"></param>
    /// <param name="NumAcordo"></param>
    /// <param name="ValorAcordo"></param>
    /// <returns></returns>
    public int ActividadeUpdate(int ActividadeID, int ContratoID, int TipoActividadeID, string NumAcordo, decimal ValorAcordo)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.ActividadeUpdate(cm, ActividadeID, ContratoID, TipoActividadeID, NumAcordo, ValorAcordo);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel actualizar a actividade '" + ActividadeID + "' do contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ActividadeUpdate()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Actualiza actividade do contrato    
    ///    SP_AE_ACTIVIDADE_UPD
    ///    (
    ///        V_ID_ACTIVIDADE                INT,
    ///        V_ID_CONTRATO               INT,
    ///        V_ID_TIPO_ACTIVIDADE        INT,
    ///        V_NUM_ACORDO                VARCHAR2,
    ///        V_VALOR_ACORDO              NUMBER
    ///    );
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ActividadeID"></param>
    /// <param name="ContratoID"></param>
    /// <param name="TipoActividadeID"></param>
    /// <param name="NumAcordo"></param>
    /// <param name="ValorAcordo"></param>
    /// <returns></returns>
    public int ActividadeUpdate(OracleCommand cm, int ActividadeID, int ContratoID, int TipoActividadeID, string NumAcordo, decimal ValorAcordo)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_ACTIVIDADE_UPD";
            #endregion

            #region PARAMETERS

            #region V_ID_ACTIVIDADE
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ActividadeID;
            pr.ParameterName = "V_ID_ACTIVIDADE";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_CONTRATO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_TIPO_ACTIVIDADE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = TipoActividadeID;
            pr.ParameterName = "V_ID_TIPO_ACTIVIDADE";
            cm.Parameters.Add(pr);
            #endregion

            #region V_NUM_ACORDO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 20;
            pr.Value = NumAcordo;
            pr.ParameterName = "V_NUM_ACORDO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_VALOR_ACORDO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 20;
            pr.Precision = 9;
            pr.Value = ValorAcordo;
            pr.ParameterName = "V_VALOR_ACORDO";
            cm.Parameters.Add(pr);
            #endregion

            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel actualizar a actividade '" + ActividadeID + "' do contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ActividadeUpdate()'.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ActividadeID"></param>
    /// <returns></returns>
    public int ActividadeDelete(int ActividadeID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.ActividadeDelete(cm, ActividadeID);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel apagar actividade '" + ActividadeID + "' do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ActividadeDelete()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Apagar a actividade do contrato    
    ///     SP_AE_ACTIVIDADE_DEL 
    ///    (
    ///        V_ID_ACTIVIDADE   INT
    ///    );  
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ActividadeID"></param>
    /// <returns></returns>
    public int ActividadeDelete(OracleCommand cm , int ActividadeID)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_ACTIVIDADE_DEL";
            #endregion

            #region PARAMETERS
            /// V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ActividadeID;
            pr.ParameterName = "V_ID_ACTIVIDADE";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel apagar actividade '" + ActividadeID + "' do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ActividadeDelete()'.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="TipoActividadeID"></param>
    /// <param name="NumAcordo"></param>
    /// <param name="ValorAcordo"></param>
    /// <returns></returns>
    public int ActividadeInsert(int ContratoID, int TipoActividadeID, string NumAcordo, decimal ValorAcordo)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.ActividadeInsert(cm,ContratoID,TipoActividadeID, NumAcordo,ValorAcordo);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir a actividade no contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ActividadeInsert()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Insere uma actividade no contrato    
    ///    SP_AE_ACTIVIDADE_INS
    ///    (
    ///         V_ID_CONTRATO               INT,
    ///         V_ID_TIPO_ACTIVIDADE        INT,
    ///         V_NUM_ACORDO                VARCHAR2,
    ///         V_VALOR_ACORDO              NUMBER
    ///    );
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ContratoID"></param>
    /// <param name="TipoActividadeID"></param>
    /// <param name="NumAcordo"></param>
    /// <param name="ValorAcordo"></param>
    /// <returns></returns>
    public int ActividadeInsert(OracleCommand cm, int ContratoID, int TipoActividadeID, string NumAcordo, decimal ValorAcordo)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_ACTIVIDADE_INS";
            #endregion

            #region PARAMETERS
            #region V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_TIPO_ACTIVIDADE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = TipoActividadeID;
            pr.ParameterName = "V_ID_TIPO_ACTIVIDADE";
            cm.Parameters.Add(pr);
            #endregion

            #region V_NUM_ACORDO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 20;
            pr.Value = NumAcordo;
            pr.ParameterName = "V_NUM_ACORDO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_VALOR_ACORDO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 20;
            pr.Precision = 9;
            pr.Value = ValorAcordo;
            pr.ParameterName = "V_VALOR_ACORDO";
            cm.Parameters.Add(pr);
            #endregion

            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir a actividade no contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ActividadeInsert()'.", exp);
        }
    }
    #endregion

    #region TPR
    /// <summary>
    /// Devolve a lista de TPR do contrato    
    ///     SP_AE_TPR_GET 
    ///    (
    ///        V_ID_CONTRATO   INT,
    ///        CUR             OUT SYS_REFCURSOR
    ///    );
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <returns></returns>
    public DataTable TPRGet(int ContratoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_TPR_GET";
            #endregion

            #region PARAMETERS
            /// V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);

            /// CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "T_AE_TPR");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista de actividades do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ActividadeGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="TPRID"></param>
    /// <param name="ContratoID"></param>
    /// <param name="CodProduto"></param>
    /// <param name="BudgetVendas"></param>
    /// <param name="BudgetMarketing"></param>
    /// <param name="ActividadeInicio"></param>
    /// <param name="ActividadeFim"></param>
    /// <param name="ComprasInicio"></param>
    /// <param name="ComprasFim"></param>
    /// <param name="PVP"></param>
    /// <param name="UnidadeBase"></param>
    /// <param name="PrevisaoQuantidade"></param>
    /// <returns></returns>
    public int TPRUpdate(
        int TPRID,
        int ContratoID,
        string CodProduto,
        decimal BudgetVendas,
        decimal BudgetMarketing,
        DateTime ActividadeInicio,
        DateTime ActividadeFim,
        DateTime ComprasInicio,
        DateTime ComprasFim,
        decimal PVP,
        string UnidadeBase,
        decimal PrevisaoQuantidade
    )
    { 
    OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.TPRUpdate(cm, TPRID, ContratoID, CodProduto, BudgetVendas, BudgetMarketing, ActividadeInicio, ActividadeFim, ComprasInicio, ComprasFim, PVP,UnidadeBase, PrevisaoQuantidade);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel actualizar o TPR '" + TPRID + "' do contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.TPRUpdate()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Actualizar o TPR do contrato
    ///    SP_AE_TPR_UPD
    ///    (
    ///        V_ID_TPR                    INT,
    ///        V_ID_CONTRATO               INT,
    ///        V_COD_PRODUTO               VARCHAR2,
    ///        V_BUDGET_VENDAS             NUMBER,
    ///        V_BUDGET_MARKETING          NUMBER,
    ///        V_ACTIVIDADE_INICIO         DATE,
    ///        V_ACTIVIDADE_FIM            DATE,
    ///        V_COMPRAS_INICIO            DATE,
    ///        V_COMPRAS_FIM               DATE,
    ///        V_PVP                       NUMBER,
    ///        V_UNIDADE_BASE              VARCHAR2,
    ///        V_PREVISAO_QUANTIDADE        NUMBER
    ///    );
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="TPRID"></param>
    /// <param name="ContratoID"></param>
    /// <param name="CodProduto"></param>
    /// <param name="BudgetVendas"></param>
    /// <param name="BudgetMarketing"></param>
    /// <param name="ActividadeInicio"></param>
    /// <param name="ActividadeFim"></param>
    /// <param name="ComprasInicio"></param>
    /// <param name="ComprasFim"></param>
    /// <param name="PVP"></param>
    /// <param name="UnidadeBase"></param>
    /// <param name="PrevisaoQuantidade"></param>
    /// <returns></returns>
    public int TPRUpdate(
        OracleCommand cm,
        int TPRID,
        int ContratoID,
        string CodProduto,
        decimal BudgetVendas,
        decimal BudgetMarketing,
        DateTime ActividadeInicio,
        DateTime ActividadeFim,
        DateTime ComprasInicio,
        DateTime ComprasFim,
        decimal PVP,
        string UnidadeBase,
        decimal PrevisaoQuantidade
    )
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_TPR_UPD";
            #endregion

            #region PARAMETERS
            #region V_ID_TPR
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = TPRID;
            pr.ParameterName = "V_ID_TPR";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_CONTRATO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_COD_PRODUTO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 9;
            pr.Value = CodProduto;
            pr.ParameterName = "V_COD_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_BUDGET_VENDAS
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 11;
            pr.Precision = 3;
            pr.Value = BudgetVendas;
            pr.ParameterName = "V_BUDGET_VENDAS";
            cm.Parameters.Add(pr);
            #endregion

            #region V_BUDGET_MARKETING
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 11;
            pr.Precision = 3;
            pr.Value = BudgetMarketing;
            pr.ParameterName = "V_BUDGET_MARKETING";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ACTIVIDADE_INICIO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = ActividadeInicio;
            pr.ParameterName = "V_ACTIVIDADE_INICIO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ACTIVIDADE_FIM
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = ActividadeFim;
            pr.ParameterName = "V_ACTIVIDADE_FIM";
            cm.Parameters.Add(pr);
            #endregion

            #region V_COMPRAS_INICIO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = ComprasInicio;
            pr.ParameterName = "V_COMPRAS_INICIO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_COMPRAS_FIM
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = ComprasFim;
            pr.ParameterName = "V_COMPRAS_FIM";
            cm.Parameters.Add(pr);
            #endregion

            #region V_PVP
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 20;
            pr.Precision = 9;
            pr.Value = PVP;
            pr.ParameterName = "V_PVP";
            cm.Parameters.Add(pr);
            #endregion

            #region V_UNIDADE_BASE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.Value = UnidadeBase;
            pr.ParameterName = "V_UNIDADE_BASE";
            cm.Parameters.Add(pr);
            #endregion

            #region V_PREVISAO_QUANTIDADE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 20;
            pr.Precision = 9;
            pr.Value = PrevisaoQuantidade;
            pr.ParameterName = "V_PREVISAO_QUANTIDADE";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel actualizar o TPR '" + TPRID + "' do contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.TPRUpdate()'.", exp);
        }        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="TPRID"></param>
    /// <returns></returns>
    public int TPRDelete(int TPRID)
    { 
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.TPRDelete(cm, TPRID);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel apagar o TPR '" + TPRID + "' do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.TPRDelete()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Apagar o TPR do contrato    
    ///     SP_AE_TPR_DEL 
    ///    (
    ///        V_ID_TPR   INT
    ///    );  
    /// </summary>
    /// <param name="TPRID"></param>
    /// <returns></returns>
    public int TPRDelete(OracleCommand cm, int TPRID)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_TPR_DEL";
            #endregion

            #region PARAMETERS
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = TPRID;
            pr.ParameterName = "V_ID_TPR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel apagar o TPR '" + TPRID + "' do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.TPRDelete()'.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="CodProduto"></param>
    /// <param name="BudgetVendas"></param>
    /// <param name="BudgetMarketing"></param>
    /// <param name="ActividadeInicio"></param>
    /// <param name="ActividadeFim"></param>
    /// <param name="ComprasInicio"></param>
    /// <param name="ComprasFim"></param>
    /// <param name="PVP"></param>
    /// <param name="UnidadeBase"></param>
    /// <param name="PrevisaoQuantidade"></param>
    /// <returns></returns>
    public int TPRInsert(
        int ContratoID,
        string CodProduto,
        decimal BudgetVendas,
        decimal BudgetMarketing,
        DateTime ActividadeInicio,
        DateTime ActividadeFim,
        DateTime ComprasInicio,
        DateTime ComprasFim,
        decimal PVP,
        string UnidadeBase,
        decimal PrevisaoQuantidade
    )
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.TPRInsert(cm, ContratoID, CodProduto, BudgetVendas, BudgetMarketing, ActividadeInicio, ActividadeFim, ComprasInicio, ComprasFim, PVP,UnidadeBase, PrevisaoQuantidade);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir o TPR no contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.TPRInsert()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Actualizar o TPR do contrato
    /// SP_AE_TPR_INS
    /// (
    ///    V_ID_CONTRATO               INT,
    ///    V_COD_PRODUTO               VARCHAR2,
    ///    V_BUDGET_VENDAS             NUMBER,
    ///    V_BUDGET_MARKETING          NUMBER,
    ///    V_ACTIVIDADE_INICIO         DATE,
    ///    V_ACTIVIDADE_FIM            DATE,
    ///    V_COMPRAS_INICIO            DATE,
    ///    V_COMPRAS_FIM               DATE,
    ///    V_PVP                       NUMBER,
    ///    V_UNIDADE_BASE              VARCHAR2
    ///    V_PREVISAO_QUANTIDADE        NUMBER
    /// );
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ContratoID"></param>
    /// <param name="CodProduto"></param>
    /// <param name="BudgetVendas"></param>
    /// <param name="BudgetMarketing"></param>
    /// <param name="ActividadeInicio"></param>
    /// <param name="ActividadeFim"></param>
    /// <param name="ComprasInicio"></param>
    /// <param name="ComprasFim"></param>
    /// <param name="PVP"></param>
    /// <param name="UnidadeBase"></param>
    /// <param name="PrevisaoQuantidade"></param>
    /// <returns></returns>
    public int TPRInsert(
        OracleCommand cm,
        int ContratoID,
        string CodProduto,
        decimal BudgetVendas,
        decimal BudgetMarketing,
        DateTime ActividadeInicio,
        DateTime ActividadeFim,
        DateTime ComprasInicio,
        DateTime ComprasFim,
        decimal PVP,
        string UnidadeBase,
        decimal PrevisaoQuantidade
    )
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_TPR_INS";
            #endregion

            #region PARAMETERS
            #region V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_COD_PRODUTO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 9;
            pr.Value = CodProduto;
            pr.ParameterName = "V_COD_PRODUTO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_BUDGET_VENDAS
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 11;
            pr.Precision = 3;
            pr.Value = BudgetVendas;
            pr.ParameterName = "V_BUDGET_VENDAS";
            cm.Parameters.Add(pr);
            #endregion

            #region V_BUDGET_MARKETING
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 11;
            pr.Precision = 3;
            pr.Value = BudgetMarketing;
            pr.ParameterName = "V_BUDGET_MARKETING";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ACTIVIDADE_INICIO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = ActividadeInicio;
            pr.ParameterName = "V_ACTIVIDADE_INICIO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ACTIVIDADE_FIM
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = ActividadeFim;
            pr.ParameterName = "V_ACTIVIDADE_FIM";
            cm.Parameters.Add(pr);
            #endregion

            #region V_COMPRAS_INICIO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = ComprasInicio;
            pr.ParameterName = "V_COMPRAS_INICIO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_COMPRAS_FIM
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = ComprasFim;
            pr.ParameterName = "V_COMPRAS_FIM";
            cm.Parameters.Add(pr);
            #endregion

            #region V_PVP
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 20;
            pr.Precision = 9;
            pr.Value = PVP;
            pr.ParameterName = "V_PVP";
            cm.Parameters.Add(pr);
            #endregion

            #region V_UNIDADE_BASE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.Value = UnidadeBase;
            pr.ParameterName = "V_UNIDADE_BASE";
            cm.Parameters.Add(pr);
            #endregion

            #region V_PREVISAO_QUANTIDADE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 20;
            pr.Precision = 9;
            pr.Value = PrevisaoQuantidade;
            pr.ParameterName = "V_PREVISAO_QUANTIDADE";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir o TPR no contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.TPRInsert()'.", exp);
        }
    }
    #endregion

    #region DEBITOS
    /// <summary>
    /// Devolve a lista de TPR do contrato    
    ///    SP_AE_DEBITO_GET 
    ///    (
    ///        V_ID_CONTRATO   INT,
    ///        CUR             OUT SYS_REFCURSOR
    ///    );
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <returns></returns>
    public DataTable DebitoGet(int ContratoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_DEBITO_GET";
            #endregion

            #region PARAMETERS
            /// V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);

            /// CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "T_AE_DEBITO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista de debitos do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.DebitoGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="DebitoID"></param>
    /// <param name="ContratoID"></param>
    /// <param name="DataDebito"></param>
    /// <param name="ValorDebito"></param>
    /// <returns></returns>
    public int DebitoUpdate(int DebitoID, int ContratoID, DateTime DataDebito, decimal ValorDebito)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.DebitoUpdate(cm, DebitoID, ContratoID, DataDebito, ValorDebito);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel actualizar o debito '" + DebitoID + "' do contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.DebitoUpdate()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Actualiza actividade do contrato    
    ///    SP_AE_DEBITO_UPD
    ///    (
    ///        V_ID_DEBITO                    INT,
    ///        V_ID_CONTRATO               INT,
    ///        V_DATA_DEBITO               DATE,
    ///        V_VALOR_DEBITO              NUMBER
    ///    );
    /// </summary>
    /// <param name="DebitoID"></param>
    /// <param name="ContratoID"></param>
    /// <param name="DataDebito"></param>
    /// <param name="ValorDebito"></param>
    /// <returns></returns>
    public int DebitoUpdate(OracleCommand cm ,int DebitoID, int ContratoID, DateTime DataDebito, decimal ValorDebito)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_DEBITO_UPD";
            #endregion

            #region PARAMETERS

            #region V_ID_DEBITO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = DebitoID;
            pr.ParameterName = "V_ID_DEBITO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_CONTRATO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_DATA_DEBITO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = DataDebito;
            pr.ParameterName = "V_DATA_DEBITO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_VALOR_DEBITO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 20;
            pr.Precision = 9;
            pr.Value = ValorDebito;
            pr.ParameterName = "V_VALOR_DEBITO";
            cm.Parameters.Add(pr);
            #endregion

            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel actualizar o debito '" + DebitoID + "' do contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.DebitoUpdate()'.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="DebitoID"></param>
    /// <returns></returns>
    public int DebitoDelete(int DebitoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.DebitoDelete(cm, DebitoID);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel apagar o d�bito '" + DebitoID + "' do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.DebitoDelete()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Apagar o debito do contrato    
    ///    SP_AE_DEBITO_DEL 
    ///    (
    ///        V_ID_DEBITO   INT
    ///    );    
    /// </summary>
    /// <param name="TPRID"></param>
    /// <returns></returns>
    public int DebitoDelete(OracleCommand cm, int DebitoID)
    {
        try
        {
            #region CONNECTION     
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_DEBITO_DEL";
            #endregion

            #region PARAMETERS
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = DebitoID;
            pr.ParameterName = "V_ID_DEBITO";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel apagar o d�bito '" + DebitoID + "' do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.DebitoDelete()'.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="DataDebito"></param>
    /// <param name="ValorDebito"></param>
    /// <returns></returns>
    public int DebitoInsert(int ContratoID, DateTime DataDebito, decimal ValorDebito)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.DebitoInsert(cm, ContratoID, DataDebito, ValorDebito);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir o debito no contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.DebitoInsert()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Insere um d�bito no contrato    
    /// SP_AE_DEBITO_INS
    /// (
    ///    V_ID_CONTRATO               INT,
    ///    V_DATA_DEBITO               DATE,
    ///    V_VALOR_DEBITO              NUMBER
    /// );
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="DataDebito"></param>
    /// <param name="ValorDebito"></param>
    /// <returns></returns>
    public int DebitoInsert(OracleCommand cm, int ContratoID, DateTime DataDebito, decimal ValorDebito)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_DEBITO_INS";
            #endregion

            #region PARAMETERS

            #region V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_DATA_DEBITO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = DataDebito;
            pr.ParameterName = "V_DATA_DEBITO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_VALOR_DEBITO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Double;
            pr.DbType = DbType.Double;
            pr.Scale = 20;
            pr.Precision = 9;
            pr.Value = ValorDebito;
            pr.ParameterName = "V_VALOR_DEBITO";
            cm.Parameters.Add(pr);
            #endregion

            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir o debito no contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.DebitoInsert()'.", exp);
        }
    }
    #endregion

    #region LOGS
    /// <summary>
    /// Devolve a lista de LOGS do contrato    
    ///    SP_AE_LOG_GET 
    ///    (
    ///        V_ID_CONTRATO   INT,
    ///        CUR             OUT SYS_REFCURSOR
    ///    );    
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <returns></returns>
    public DataTable LogGet(int ContratoID)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_LOG_GET";
            #endregion

            #region PARAMETERS
            /// V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);

            /// CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "T_AE_LOG");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter a lista de logs do contrato.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.LogGet()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoID"></param>
    /// <param name="LogData"></param>
    /// <param name="LogUser"></param>
    /// <param name="LogMessage"></param>
    /// <returns></returns>
    public int LogInsert(int ContratoID, DateTime LogData, string LogUser, string LogMessage)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cn.Open();
            return this.LogInsert(cm, ContratoID, LogData, LogUser, LogMessage);
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir o log no contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.LogInsert()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Actualiza actividade do contrato    
    ///     SP_AE_LOG_INS
    ///     (
    ///         V_ID_CONTRATO            INT,
    ///         V_LOG_DATA               DATE,
    ///         V_LOG_USER               VARCHAR2,
    ///         V_MESSAGE                VARCHAR2
    ///     );
    /// </summary>
    /// <param name="cm"></param>
    /// <param name="ContratoID"></param>
    /// <param name="LogData"></param>
    /// <param name="LogUser"></param>
    /// <param name="LogMessage"></param>
    /// <returns></returns>
    public int LogInsert(OracleCommand cm, int ContratoID, DateTime LogData, string LogUser, string LogMessage)
    {
        try
        {
            #region CONNECTION
            cm.Parameters.Clear();
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_LOG_INS";
            #endregion

            #region PARAMETERS
           
            #region V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = ContratoID;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_LOG_DATA
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.Value = LogData;
            pr.ParameterName = "V_LOG_DATA";
            cm.Parameters.Add(pr);
            #endregion

            #region V_LOG_USER
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 50;
            pr.Value = LogUser;
            pr.ParameterName = "V_LOG_USER";
            cm.Parameters.Add(pr);
            #endregion

            #region V_MESSAGE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 200;
            pr.Value = LogMessage;
            pr.ParameterName = "V_MESSAGE";
            cm.Parameters.Add(pr);
            #endregion

            #endregion

            #region EXECUTE
            return cm.ExecuteNonQuery();
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel inserir o log no contrato '" + ContratoID + "'.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.LogInsert()'.", exp);
        }
    }
    #endregion

    #region CONTRACTO
    public bool ContratoExists(string emissor, string ano, string serie, string numero)
    {
        int i=0;
        
        return ContratoExists(emissor,ano,serie,numero,out i);
    }
    /// <summary>
    /// Get Agrupamentos List
    ///    SP_AE_CONTRATO_EXISTS
    ///    (
    ///        V_ID_EMISSOR               VARCHAR2,
    ///        V_ID_ANO                   VARCHAR2,
    ///        V_ID_NUM_SERIE             VARCHAR2,
    ///        V_ID_NUMERO                VARCHAR2,
    ///        V_EXISTS            OUT    INT
    ///    );
    /// </summary>
    /// <returns></returns>
    public bool ContratoExists(string emissor, string ano, string serie, string numero, out int ContratoId)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_CONTRATO_EXISTS";
            #endregion

            #region PARAMETERS
            #region V_ID_EMISSOR
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 2;
            pr.DbType = DbType.String;
            pr.Value = emissor;
            pr.ParameterName = "V_ID_EMISSOR";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_ANO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 4;
            pr.DbType = DbType.String;
            pr.Value = ano;
            pr.ParameterName = "V_ID_ANO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_NUM_SERIE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 2;
            pr.DbType = DbType.String;
            pr.Value = serie;
            pr.ParameterName = "V_ID_NUM_SERIE";
            cm.Parameters.Add(pr);
            #endregion

            #region V_ID_NUMERO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 4;
            pr.DbType = DbType.String;
            pr.Value = numero;
            pr.ParameterName = "V_ID_NUMERO";
            cm.Parameters.Add(pr);
            #endregion

            #region V_EXISTS
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.ParameterName = "V_EXISTS";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            cn.Open();
            cm.ExecuteNonQuery();
            ContratoId = (int)cm.Parameters["V_EXISTS"].Value ;
            if (ContratoId >= 0)
                return true;
            return false;
            #endregion
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel saber se o contrato existe ou n�o.", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ContratoExists()'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Get contrato List
    /// </summary>
    /// <returns></returns>
    public DataTable ContratoGet()
    {
        return this.ContratoGet("");
    }
    /// <summary>
    /// Get Contrato list filtered by 'Where' condition
    ///    SP_AE_CONTRATOS_GET (wherestm VARCHAR2, CUR OUT SYS_REFCURSOR );
    /// </summary>
    /// <param name="Where">Filter Conditions</param>
    /// <returns></returns>
    public DataTable ContratoGet(string Where)
    {
        /// PREPARE CONDITION
        if (Where.Equals(""))
        {
            Where = "1=1";
        }
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_CONTRATOS_GET";
            #endregion

            #region PARAMENTERS
            /// WHERESTM
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.DbType = DbType.String;
            pr.Value = Where;
            pr.ParameterName = "WHERESTM";
            cm.Parameters.Add(pr);

            /// CUR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR";
            cm.Parameters.Add(pr);
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "T_AE_CONTRATO");
            da.Fill(ds);
            #endregion

            return ds.Tables[0];
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter os contratos para o filtro '" + Where + "'", exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.getContrato(string)'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Get Contrato list with specified ID 
    ///    SP_AE_CONTRATO_GET
    ///    (
    ///        V_ID_CONTRATO               VARCHAR2,
    ///        CUR_CONTRATO                OUT SYS_REFCURSOR,
    ///        CUR_ENTIDADE                OUT SYS_REFCURSOR,
    ///        CUR_ABASTECIMENTO           OUT SYS_REFCURSOR,
    ///        CUR_ACTIVIDADE              OUT SYS_REFCURSOR,
    ///        CUR_TPR                     OUT SYS_REFCURSOR,
    ///        CUR_DEBITOS                 OUT SYS_REFCURSOR,
    ///        CUR_LOG                     OUT SYS_REFCURSOR
    ///    );
    /// </summary>
    /// <param name="idContrato">IdContrato to get</param>
    /// <returns></returns>
    public DataSet ContratoGet(int idContrato)
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        try
        {
            #region CONNECTION
            OracleCommand cm = cn.CreateCommand();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            cm.CommandText = "PK_AE.SP_AE_CONTRATO_GET";
            #endregion

            #region PARAMETERS
            #region V_ID_CONTRATO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = idContrato;
            pr.ParameterName = "V_ID_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region  CUR_CONTRATO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR_CONTRATO";
            cm.Parameters.Add(pr);
            #endregion

            #region  CUR_ENTIDADE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR_ENTIDADE";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR_ABASTECIMENTO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR_ABASTECIMENTO";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR_ACTIVIDADE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR_ACTIVIDADE";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR_TPR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR_TPR";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR_DEBITOS
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR_DEBITOS";
            cm.Parameters.Add(pr);
            #endregion

            #region CUR_LOG
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.RefCursor;
            pr.ParameterName = "CUR_LOG";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cm);
            da.TableMappings.Add("Table", "T_AE_CONTRATO");
            da.TableMappings.Add("Table1", "T_AE_ENTIDADE");
            da.TableMappings.Add("Table2", "T_AE_ABASTECIMENTO");
            da.TableMappings.Add("Table3", "T_AE_ACTIVIDADE");
            da.TableMappings.Add("Table4", "T_AE_TPR");
            da.TableMappings.Add("Table5", "T_AE_DEBITO");
            da.TableMappings.Add("Table6", "T_AE_LOG");
            da.Fill(ds);
            #endregion

            return ds;
        }
        catch (OracleException exp)
        {
            throw new myDBException("DAL -> N�o foi poss�vel obter os dados do contrato com ID=" + idContrato, exp);
        }
        catch (Exception exp)
        {
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ContratoGet(int)'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    /// <summary>
    /// Insert new Contract
    ///     SP_AE_CONTRATO_INS
    ///    (
    ///        V_ID_CONTRATO              IN OUT INT,
    ///        V_ID_EMISSOR               VARCHAR2,
    ///        V_ID_ANO                   VARCHAR2,
    ///        V_ID_NUM_SERIE             VARCHAR2,
    ///        V_ID_NUMERO                VARCHAR2,
    ///        V_DATA_RECEPCAO            DATE,
    ///        V_ID_CUSTOMER_SERVICE      VARCHAR2,
    ///        V_NIVEL_CONTRATO		      VARCHAR2,
    ///        V_GRUPO_ECONOMICO_COD      VARCHAR2,
    ///        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
    ///        V_NOME_FIRMA               VARCHAR2,
    ///        V_MORADA_SEDE              VARCHAR2,
    ///        V_LOCALIDADE               VARCHAR2,
    ///        V_NIF                      VARCHAR2,
    ///        V_OBSERVACOES              VARCHAR2,
    ///        V_INSERT_USER              VARCHAR2,
    ///        V_EXISTS            OUT    INT
    ///    );      
    /// </summary>
    public string ContratoInsert(
            ref int idContrato,
            string Contrato_Emissor,
            string Contrato_Ano,
            string Contrato_NumSerie,
            string Contrato_Numero,
            DateTime Contrato_Recepcao,
            string Contrato_CustomerService,
            string Contrato_Nivel,
            string Contrato_GrupoEconomico,
            string Contrato_SubGrupoEconomico,
            string Contrato_NomeFirma,
            string Contrato_MoradaSede,
            string Contrato_Localidade,
            string Contrato_Nif,
            string Contrato_Observa��es,
            string User,
            DataTable Clientes,
            DataTable Abastecimentos,
            DataTable Actividades,
            DataTable Tpr,
            DataTable Debitos,
            DataTable Log
    )
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        OracleCommand cm = cn.CreateCommand();
        OracleTransaction tr = null;

        try
        {
            #region CONNECTION
            cn.Open();
            tr = cn.BeginTransaction();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            #endregion                        

            #region INSERIR CONTRATO
            #region PARAMETROS DO CONTRATO
            #region DADOS DO CONTRATO
            #region  V_ID_CONTRACTO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.InputOutput;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.ParameterName = "V_ID_CONTRACTO";
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_EMISSOR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 2;
            pr.ParameterName = "V_ID_EMISSOR";
            pr.Value = Contrato_Emissor;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_ANO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 4;
            pr.ParameterName = "V_ID_ANO";
            pr.Value = Contrato_Ano;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_NUM_SERIE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 2;
            pr.ParameterName = "V_ID_NUM_SERIE";
            pr.Value = Contrato_NumSerie;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_NUMERO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 4;
            pr.ParameterName = "V_ID_NUMERO";
            pr.Value = Contrato_Numero;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_DATA_RECEPCAO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.ParameterName = "V_DATA_RECEPCAO";
            pr.Value = Contrato_Recepcao;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_CUSTOMER_SERVICE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.ParameterName = "V_ID_CUSTOMER_SERVICE";
            pr.Value = Contrato_CustomerService;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_NIVEL_CONTRATO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.ParameterName = "V_NIVEL_CONTRATO";
            pr.Value = Contrato_Nivel;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_GRUPO_ECONOMICO_COD
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.ParameterName = "V_GRUPO_ECONOMICO_COD";
            pr.Value = Contrato_GrupoEconomico;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_SUBGRUPO_ECONOMICO_COD
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.ParameterName = "V_SUBGRUPO_ECONOMICO_COD";
            pr.Value = Contrato_SubGrupoEconomico;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_NOME_FIRMA
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 50;
            pr.ParameterName = "V_NOME_FIRMA";
            pr.Value = Contrato_NomeFirma;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_MORADA_SEDE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 255;
            pr.ParameterName = "V_MORADA_SEDE";
            pr.Value = Contrato_MoradaSede;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_LOCALIDADE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 50;
            pr.ParameterName = "V_LOCALIDADE";
            pr.Value = Contrato_Localidade;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_NIF
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 20;
            pr.ParameterName = "V_NIF";
            pr.Value = Contrato_Nif;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_OBSERVACOES
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 4000;
            pr.ParameterName = "V_OBSERVACOES";
            pr.Value = Contrato_Observa��es;
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region DADOS USER
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 20;
            pr.ParameterName = "V_INSERT_USER";
            pr.Value = User;
            cm.Parameters.Add(pr);
            #endregion

            #region V_EXISTS
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.ParameterName = "V_EXISTS";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            cm.CommandText = "PK_AE.SP_AE_CONTRATO_INS";
            cm.ExecuteNonQuery();
            if ((int)cm.Parameters["V_EXISTS"].Value != 0)
                throw new CodeExistsException("O N� de Proposta J� Existe.", null);
            idContrato = (int)cm.Parameters["V_ID_CONTRACTO"].Value;
            cm.Parameters.Clear();
            #endregion
            #endregion

            #region INSERIR TABELAS ADICIONAIS
            #region INSERIR CLIENTES
            if (Contrato_Nivel == "LE")
            {
                Clientes.DefaultView.RowFilter = "";
                Clientes.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView client in Clientes.DefaultView)
                {
                    ClientesInsert(
                        cm,
                        idContrato,
                        client["COD_LOCAL_ENTREGA"].ToString(),
                        client["COD_GRUPO_ECONOMICO_CLI"].ToString(),
                        client["COD_SUBGRUPO_ECONOMICO_CLI"].ToString(),
                        client["COD_CLIENTE"].ToString()
                    );
                }
            }
            #endregion

            #region INSERIR ABASTECIMENTOS
            Abastecimentos.DefaultView.RowFilter = "";
            Abastecimentos.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

            foreach (DataRowView Abastecimento in Abastecimentos.DefaultView)
            {
                AbastecimentoInsert(
                    cm,
                    idContrato,
                    Abastecimento["COD_CONCESSIONARIO"].ToString(),
                    Abastecimento["FACTURACAO"].ToString(),
                    Abastecimento["DISTRIBUICAO"].ToString(),
                    Abastecimento["OBSERVACOES"].ToString()
                );
            }
            
            #endregion

            #region INSERIR ACTIVIDADES
            Actividades.DefaultView.RowFilter = "";
            Actividades.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

            foreach (DataRowView Actividade in Actividades.DefaultView)
            {
                ActividadeInsert(
                    cm,
                    idContrato,
                    int.Parse(Actividade["ID_TIPO_ACTIVIDADE"].ToString()),
                    Actividade["NUM_ACORDO"].ToString(),
                    decimal.Parse(Actividade["VALOR_ACORDO"].ToString().Replace(".",","))
                );
            }
            #endregion

            #region INSERIR TPR
            Tpr.DefaultView.RowFilter = "";
            Tpr.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

            foreach (DataRowView tpr in Tpr.DefaultView)
            {
                TPRInsert(
                    cm,
                    idContrato,
                    tpr["COD_PRODUTO"].ToString(),
                     decimal.Parse(tpr["BUDGET_VENDAS"].ToString().Replace(".",",")),
                     decimal.Parse(tpr["BUDGET_MARKETING"].ToString().Replace(".",",")),
                    (DateTime)tpr["ACTIVIDADE_INICIO"],
                    (DateTime)tpr["ACTIVIDADE_FIM"],
                    (DateTime)tpr["COMPRAS_INICIO"],
                    (DateTime)tpr["COMPRAS_FIM"],
                     decimal.Parse(tpr["PVP"].ToString().Replace(".",",")),
                    tpr["UNIDADE_BASE"].ToString(),
                    decimal.Parse(tpr["PREVISAO_QUANTIDADE"].ToString().Replace(".", ","))
               );
            }
            #endregion

            #region INSERIR DEBITOS
            Debitos.DefaultView.RowFilter = "";
            Debitos.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

            foreach (DataRowView db in Debitos.DefaultView)
            {
                DebitoInsert(
                    cm,
                    idContrato,
                    (DateTime)db["DATA_DEBITO"],
                    decimal.Parse(db["VALOR_DEBITO"].ToString().Replace(".", ","))
                );
            }
            #endregion

            #region INSERIR LOGS
            Log.DefaultView.RowFilter = "";
            Log.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

            foreach (DataRowView lg in Log.DefaultView)
            {
                LogInsert(
                    cm,
                    idContrato,
                    (DateTime)lg["LOG_DATA"],
                    lg["LOG_USER"].ToString(),
                    lg["MESSAGE"].ToString()
                );
            }
            #endregion
            #endregion

            tr.Commit();
            return Contrato_Emissor
                 + Contrato_Ano
                 + Contrato_NumSerie
                 + Contrato_Numero;
        }
        catch (myDBException exp)
        {
            tr.Rollback();
            throw new myDBException("DAL -> N�o foi poss�vel inserir o contrato.", exp);
        }
        catch (OracleException exp)
        {
            tr.Rollback();
            throw new myDBException("DAL -> N�o foi poss�vel guardar o contrato.", exp);
        }
        catch (Exception exp)
        {
            tr.Rollback();
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ContratoInsert'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }      
    /// <summary>
    /// update a Contract
    ///     SP_AE_CONTRATO_UPD
    ///    (
    ///        V_ID_CONTRATO              INT,
    ///        V_ID_EMISSOR               VARCHAR2,
    ///        V_ID_ANO                   VARCHAR2,
    ///        V_ID_NUM_SERIE             VARCHAR2,
    ///        V_ID_NUMERO                VARCHAR2,
    ///        V_DATA_RECEPCAO            DATE,
    ///        V_ID_CUSTOMER_SERVICE      VARCHAR2,
    ///        V_NIVEL_CONTRATO		      VARCHAR2,
    ///        V_GRUPO_ECONOMICO_COD      VARCHAR2,
    ///        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
    ///        V_NOME_FIRMA               VARCHAR2,
    ///        V_MORADA_SEDE              VARCHAR2,
    ///        V_LOCALIDADE               VARCHAR2,
    ///        V_NIF                      VARCHAR2,
    ///        V_OBSERVACOES              VARCHAR2,
    ///        V_INSERT_USER              VARCHAR2,
    ///        V_EXISTS            OUT    INT
    ///    );      
    /// </summary>
    public string ContratoUpdate(
            int idContrato,
            string Contrato_Emissor,
            string Contrato_Ano,
            string Contrato_NumSerie,
            string Contrato_Numero,
            DateTime Contrato_Recepcao,
            string Contrato_CustomerService,
            string Contrato_Nivel,
            string Contrato_GrupoEconomico,
            string Contrato_SubGrupoEconomico,
            string Contrato_NomeFirma,
            string Contrato_MoradaSede,
            string Contrato_Localidade,
            string Contrato_Nif,
            string Contrato_Observa��es,
            string User,
            DataTable Clientes,
            DataTable Abastecimentos,    
            DataTable Actividades,
            DataTable Tpr,
            DataTable Debitos,
            DataTable Log
    )
    {
        OracleConnection cn = new OracleConnection(this.ConnectionString);
        OracleCommand cm = cn.CreateCommand();
        OracleTransaction tr = null;

        try
        {
            #region CONNECTION
            cn.Open();
            tr = cn.BeginTransaction();
            cm.Connection = cn;
            cm.CommandType = CommandType.StoredProcedure;
            #endregion

            #region ACTUALIZAR CONTRATO
            #region PARAMETROS DO CONTRATO
            #region DADOS DO CONTRATO
            #region  V_ID_CONTRACTO
            OracleParameter pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.Value = idContrato;
            pr.ParameterName = "V_ID_CONTRACTO";
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_EMISSOR
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 2;
            pr.ParameterName = "V_ID_EMISSOR";
            pr.Value = Contrato_Emissor;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_ANO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 4;
            pr.ParameterName = "V_ID_ANO";
            pr.Value = Contrato_Ano;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_NUM_SERIE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 2;
            pr.ParameterName = "V_ID_NUM_SERIE";
            pr.Value = Contrato_NumSerie;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_NUMERO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 4;
            pr.ParameterName = "V_ID_NUMERO";
            pr.Value = Contrato_Numero;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_DATA_RECEPCAO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Date;
            pr.DbType = DbType.DateTime;
            pr.ParameterName = "V_DATA_RECEPCAO";
            pr.Value = Contrato_Recepcao;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_ID_CUSTOMER_SERVICE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.ParameterName = "V_ID_CUSTOMER_SERVICE";
            pr.Value = Contrato_CustomerService;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_NIVEL_CONTRATO
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.Size = 3;
            pr.ParameterName = "V_NIVEL_CONTRATO";
            pr.Value = Contrato_Nivel;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_GRUPO_ECONOMICO_COD
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.ParameterName = "V_GRUPO_ECONOMICO_COD";
            pr.Value = Contrato_GrupoEconomico;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_SUBGRUPO_ECONOMICO_COD
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 3;
            pr.ParameterName = "V_SUBGRUPO_ECONOMICO_COD";
            pr.Value = Contrato_SubGrupoEconomico;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_NOME_FIRMA
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 50;
            pr.ParameterName = "V_NOME_FIRMA";
            pr.Value = Contrato_NomeFirma;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_MORADA_SEDE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 255;
            pr.ParameterName = "V_MORADA_SEDE";
            pr.Value = Contrato_MoradaSede;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_LOCALIDADE
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 50;
            pr.ParameterName = "V_LOCALIDADE";
            pr.Value = Contrato_Localidade;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_NIF
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 20;
            pr.ParameterName = "V_NIF";
            pr.Value = Contrato_Nif;
            cm.Parameters.Add(pr);
            #endregion

            #region  V_OBSERVACOES
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 4000;
            pr.ParameterName = "V_OBSERVACOES";
            pr.Value = Contrato_Observa��es;
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region DADOS USER
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Input;
            pr.OracleDbType = OracleDbType.Varchar2;
            pr.DbType = DbType.String;
            pr.Size = 20;
            pr.ParameterName = "V_INSERT_USER";
            pr.Value = User;
            cm.Parameters.Add(pr);
            #endregion

            #region V_EXISTS
            pr = cm.CreateParameter();
            pr.Direction = ParameterDirection.Output;
            pr.OracleDbType = OracleDbType.Int32;
            pr.DbType = DbType.Int32;
            pr.ParameterName = "V_EXISTS";
            cm.Parameters.Add(pr);
            #endregion
            #endregion

            #region EXECUTE
            cm.CommandText = "PK_AE.SP_AE_CONTRATO_UPD";
            cm.ExecuteNonQuery();
            if ((int)cm.Parameters["V_EXISTS"].Value != 0)
                throw new CodeExistsException("O N� de Proposta J� Existe.", null);
            #endregion
            #endregion

            #region INSERIR TABELAS ADICIONAIS
            #region CLIENTES

            ClientesDel(cm, idContrato);
            if (Contrato_Nivel == "LE")
            {
                #region INSERIR NOVOS CLIENTES
                Clientes.DefaultView.RowFilter = "";
                Clientes.DefaultView.RowStateFilter = DataViewRowState.CurrentRows;

                foreach (DataRowView client in Clientes.DefaultView)
                {
                    ClientesInsert(
                        cm,
                        idContrato,
                        client["COD_LOCAL_ENTREGA"].ToString(),
                        client["COD_GRUPO_ECONOMICO_CLI"].ToString(),
                        client["COD_SUBGRUPO_ECONOMICO_CLI"].ToString(),
                        client["COD_CLIENTE"].ToString()
                    );
                }
                #endregion
            }
            else
            {
                ClientesDel(cm, idContrato);
            }
            #endregion

            #region ABASTECIMENTOS
            #region APAGAR ABASTECIMENTOS
            Abastecimentos.DefaultView.RowFilter = "";
            Abastecimentos.DefaultView.RowStateFilter = DataViewRowState.Deleted;

            foreach (DataRowView Abastecimento in Abastecimentos.DefaultView)
            {
                AbastecimentoDelete(cm, idContrato, Abastecimento["COD_CONCESSIONARIO"].ToString());
            }
            #endregion

            #region ACTUALIZAR ABASTECIMENTOS
            Abastecimentos.DefaultView.RowStateFilter = DataViewRowState.ModifiedCurrent;

            foreach (DataRowView Abastecimento in Abastecimentos.DefaultView)
            {
                AbastecimentoUpdate(
                    cm,
                    idContrato,
                    Abastecimento["COD_CONCESSIONARIO"].ToString(),
                    Abastecimento["FACTURACAO"].ToString(),
                    Abastecimento["DISTRIBUICAO"].ToString(),
                    Abastecimento["OBSERVACOES"].ToString()
                 );
            }
            #endregion

            #region INSERIR ABASTECIMENTOS
            Abastecimentos.DefaultView.RowStateFilter = DataViewRowState.Added;

            foreach (DataRowView Abastecimento in Abastecimentos.DefaultView)
            {
                AbastecimentoInsert(
                    cm,
                    idContrato,
                    Abastecimento["COD_CONCESSIONARIO"].ToString(),
                    Abastecimento["FACTURACAO"].ToString(),
                    Abastecimento["DISTRIBUICAO"].ToString(),
                    Abastecimento["OBSERVACOES"].ToString()
                );
            }
            #endregion
            #endregion

            #region ACTIVIDADES
            #region APAGAR ACTIVIDADES
            Actividades.DefaultView.RowFilter = "";
            Actividades.DefaultView.RowStateFilter = DataViewRowState.Deleted;

            foreach (DataRowView Actividade in Actividades.DefaultView)
            {
                ActividadeDelete(cm, int.Parse(Actividade["ID_ACTIVIDADE"].ToString()));                    
            }
            #endregion

            #region ACTUALIZAR ACTIVIDADES 
            Actividades.DefaultView.RowStateFilter = DataViewRowState.ModifiedCurrent;

            foreach (DataRowView Actividade in Actividades.DefaultView)
            {
                ActividadeUpdate(
                    cm,
                    int.Parse(Actividade["ID_ACTIVIDADE"].ToString()),
                    idContrato,
                    int.Parse(Actividade["ID_TIPO_ACTIVIDADE"].ToString().Replace(".", ",")),
                    Actividade["NUM_ACORDO"].ToString(),
                    decimal.Parse(Actividade["VALOR_ACORDO"].ToString().Replace(".", ","))
                );
            }
            #endregion

            #region INSERIR ACTIVIDADES
            Actividades.DefaultView.RowFilter = "";
            Actividades.DefaultView.RowStateFilter = DataViewRowState.Added;

            foreach (DataRowView Actividade in Actividades.DefaultView)
            {
                ActividadeInsert(
                    cm,
                    idContrato,
                    int.Parse(Actividade["ID_TIPO_ACTIVIDADE"].ToString().Replace(".", ",")),
                    Actividade["NUM_ACORDO"].ToString(),
                    decimal.Parse(Actividade["VALOR_ACORDO"].ToString().Replace(".", ","))
                 );
            }
            #endregion
            #endregion

            #region TPR
            #region APAGAR TPR
            Tpr.DefaultView.RowFilter = "";
            Tpr.DefaultView.RowStateFilter = DataViewRowState.Deleted;

            foreach (DataRowView tpr in Tpr.DefaultView)
            {
                TPRDelete(cm, int.Parse(tpr["ID_TPR"].ToString().Replace(".", ",")));
            }
            #endregion

            #region ACTUALIZAR TPR
            Tpr.DefaultView.RowFilter = "";
            Tpr.DefaultView.RowStateFilter = DataViewRowState.ModifiedCurrent;

            foreach (DataRowView tpr in Tpr.DefaultView)
            {
                TPRUpdate(
                    cm,
                    int.Parse(tpr["ID_TPR"].ToString().Replace(".", ",")),
                    idContrato,
                    tpr["COD_PRODUTO"].ToString(),
                    decimal.Parse(tpr["BUDGET_VENDAS"].ToString().Replace(".", ",")),
                    decimal.Parse(tpr["BUDGET_MARKETING"].ToString().Replace(".", ",")),
                    (DateTime)tpr["ACTIVIDADE_INICIO"],
                    (DateTime)tpr["ACTIVIDADE_FIM"],
                    (DateTime)tpr["COMPRAS_INICIO"],
                    (DateTime)tpr["COMPRAS_FIM"],
                    decimal.Parse(tpr["PVP"].ToString().Replace(".", ",")),
                    tpr["V_UNIDADE_BASE"].ToString(),
                    decimal.Parse(tpr["PREVISAO_QUANTIDADE"].ToString().Replace(".", ","))
                );
            }
            #endregion

            #region INSERIR TPR
            Tpr.DefaultView.RowFilter = "";
            Tpr.DefaultView.RowStateFilter = DataViewRowState.Added;

            foreach (DataRowView tpr in Tpr.DefaultView)
            {
                TPRInsert(
                    cm,
                    idContrato,
                    tpr["COD_PRODUTO"].ToString(),
                    decimal.Parse(tpr["BUDGET_VENDAS"].ToString().Replace(".", ",")),
                    decimal.Parse(tpr["BUDGET_MARKETING"].ToString().Replace(".", ",")),
                    (DateTime)tpr["ACTIVIDADE_INICIO"],
                    (DateTime)tpr["ACTIVIDADE_FIM"],
                    (DateTime)tpr["COMPRAS_INICIO"],
                    (DateTime)tpr["COMPRAS_FIM"],
                    decimal.Parse(tpr["PVP"].ToString().Replace(".", ",")),
                    tpr["V_UNIDADE_BASE"].ToString(),
                    decimal.Parse(tpr["PREVISAO_QUANTIDADE"].ToString().Replace(".", ","))
                );
            }
            #endregion
            #endregion

            #region DEBITOS
            #region APAGAR DEBITOS
            Debitos.DefaultView.RowFilter = "";
            Debitos.DefaultView.RowStateFilter = DataViewRowState.Deleted;

            foreach (DataRowView db in Debitos.DefaultView)
            {
                DebitoDelete(cm, int.Parse(db["ID_DEBITO"].ToString().Replace(".", ",")));
            }
            #endregion

            #region ACTUALIZAR DEBITOS
            Debitos.DefaultView.RowFilter = "";
            Debitos.DefaultView.RowStateFilter = DataViewRowState.ModifiedCurrent;

            foreach (DataRowView db in Debitos.DefaultView)
            {
                DebitoUpdate(
                    cm,
                    int.Parse(db["ID_DEBITO"].ToString().Replace(".", ",")),
                    idContrato,
                    (DateTime)db["DATA_DEBITO"],
                    decimal.Parse(db["VALOR_DEBITO"].ToString().Replace(".", ","))
                );
            }
            #endregion

            #region INSERIR DEBITOS
            Debitos.DefaultView.RowFilter = "";
            Debitos.DefaultView.RowStateFilter = DataViewRowState.Added;

            foreach (DataRowView db in Debitos.DefaultView)
            {
                DebitoInsert(
                    cm,
                    idContrato,
                    (DateTime)db["DATA_DEBITO"],
                    decimal.Parse(db["VALOR_DEBITO"].ToString().Replace(".", ","))
                );
            }
            #endregion
            #endregion

            #region INSERIR LOGS
            Log.DefaultView.RowFilter = "";
            Log.DefaultView.RowStateFilter = DataViewRowState.Added;

            foreach (DataRowView lg in Log.DefaultView)
            {
                LogInsert(
                    cm,
                    idContrato,
                    (DateTime)lg["LOG_DATA"],
                    lg["LOG_USER"].ToString(),
                    lg["MESSAGE"].ToString()
                );
            }
            #endregion
            #endregion

            tr.Commit();
            return Contrato_Emissor
                 + Contrato_Ano
                 + Contrato_NumSerie
                 + Contrato_Numero;
        }
        catch (myDBException exp)
        {
            tr.Rollback();
            throw new myDBException("DAL -> N�o foi poss�vel actualizar o contrato '"+idContrato+"'.", exp);
        }
        catch (OracleException exp)
        {
            tr.Rollback();
            throw new myDBException("DAL -> N�o foi poss�vel actualizar o contrato.", exp);
        }
        catch (Exception exp)
        {
            tr.Rollback();
            throw new Exception("DAL -> Erro inesperado no m�todo 'AE.ContratoUpdate'.", exp);
        }
        finally
        {
            cn.Close();
        }
    }
    #endregion

    #endregion

    #region INTERFACES
    #region SELECTABLE
    /// <summary>
    /// Get Data From Source Table (Contrato)
    /// </summary>
    /// <param name="Where">Filter Condition</param>
    /// <returns></returns>    
    public DataTable Select(string Where)
    {
        return this.ContratoGet(Where);
    }
    #endregion
    #endregion
}

