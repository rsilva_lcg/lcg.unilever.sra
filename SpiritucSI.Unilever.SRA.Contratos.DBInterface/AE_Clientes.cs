using System;
using System.Data;
using Oracle.DataAccess;
using Oracle.DataAccess.Client;

[Serializable()]
public class AE_Clientes : selectable
{
    #region FIELDS
        /// <summary>
        /// DataBase ConnectionString To Use In All Commands
        /// </summary>
        protected string ConnectionString;
    #endregion

    #region METHODS
        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        /// <param name="ConnString">ConnectionString to Source DataBase</param>
        public AE_Clientes(string ConnString)
        {            
            this.ConnectionString = ConnString;
        }

        #region LOCAIS ENTREGA
        /// <summary>
        /// Devolve a lista de Clientes LEs
        /// </summary>
        public DataTable ClientesGet()
        {
            return this.ClientesGet("");
        }
        /// <summary>
        /// Devolve a lista de clientes LEs filtrados pela condi��o 'Where'
        ///     SP_AE_ENTIDADES_GET 
        ///     (
        ///         wherestm     VARCHAR2, 
        ///         CUR         OUT SYS_REFCURSOR
        ///     );
        /// </summary>
        /// <param name="Where">Filtro a aplicar</param>
        public DataTable ClientesGet(string Where)
        {
            /// PREPARE CONDITION
            if (Where.Equals(""))
            {
                Where = "1=2";
            }
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_AE.SP_AE_ENTIDADES_GET";
                #endregion

                #region PARAMETROS
                #region WHERESTM
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.DbType = DbType.String;
                pr.Value = Where;
                pr.ParameterName = "WHERESTM";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTAR
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.TableMappings.Add("Table", "MV_LOCAL_ENTREGA");
                cm.Connection = cn;
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista de clientes para o filtro '" + Where + "'.", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'AE_Clientes.ClientesGet(string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Devolve a lista de clientes LEs do contrato com o id 'ContratoID'
        ///    SP_AE_ENTIDADE_GET 
        ///    (
        ///        V_ID_CONTRATO   INT,
        ///        CUR             OUT SYS_REFCURSOR
        ///    );
        /// </summary>
        /// <param name="ContratoID">Id do Contrato</param>
        /// <returns></returns>
        public DataTable ClientesGet(int ContratoID)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                #region CONNECTION
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_AE.SP_AE_ENTIDADE_GET";
                #endregion

                #region PARAMETROS
                #region V_ID_CONTRATO
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.DbType = DbType.Int32;
                pr.OracleDbType = OracleDbType.Int32;
                pr.Value = ContratoID;
                pr.ParameterName = "V_ID_CONTRATO";
                cm.Parameters.Add(pr);
                #endregion

                #region CUR
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Output;
                pr.OracleDbType = OracleDbType.RefCursor;
                pr.ParameterName = "CUR";
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTAR
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cm);
                da.TableMappings.Add("Table", "T_AE_ENTIDADE");
                cm.Connection = cn;
                da.Fill(ds);
                #endregion

                return ds.Tables[0];
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel obter a lista de clientes do contrato", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'AE_Clientes.ClientesGet(int)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContratoID"></param>
        /// <param name="CodLE"></param>
        /// <returns></returns>
        public int ClientesDel(int ContratoID, string CodLE)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cn.Open();
                return this.ClientesDel(cm, ContratoID, CodLE);
            }        
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel apagar o cliente do contrato", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'AE_Clientes.ClientesDel(int, string)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContratoID"></param>
        /// <returns></returns>
        public int ClientesDel(int ContratoID)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cn.Open();
                return this.ClientesDel(cm, ContratoID);
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel apagar os cliente do contrato", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'AE_Clientes.ClientesDel(int)'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Devolve a lista de clientes LEs do contrato com o id 'ContratoID'
        ///    SP_AE_ENTIDADE_DEL 
        ///    (
        ///        V_ID_CONTRATO   INT,
        ///        V_COD_LE             VARCHAR2
        ///    );  
        /// </summary>
        /// <param name="ContratoID">Id do Contrato</param>
        /// <returns></returns>
        public int ClientesDel(OracleCommand cm, int ContratoID, string CodLE)
        {
            try
            {
                #region CONNECTION
                cm.Parameters.Clear();
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_AE.SP_AE_ENTIDADE_DEL";
                #endregion

                #region PARAMETROS
                #region V_ID_CONTRATO
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.DbType = DbType.Int32;
                pr.OracleDbType = OracleDbType.Int32;
                pr.Value = ContratoID;
                pr.ParameterName = "V_ID_CONTRATO";
                cm.Parameters.Add(pr);
                #endregion

                #region V_COD_LE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.DbType = DbType.String;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.Size = 15;
                pr.Value = CodLE;
                pr.ParameterName = "V_COD_LE";
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTAR
                return cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel apagar o cliente do contrato", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'AE_Clientes.ClientesDel(int, string)'.", exp);
            }
        }
        /// <summary>
        /// Devolve a lista de clientes LEs do contrato com o id 'ContratoID'
        ///    SP_AE_ENTIDADES_DEL 
        ///    (
        ///        V_ID_CONTRATO   INT
        ///    );  
        /// </summary>
        /// <param name="ContratoID">Id do Contrato</param>
        /// <returns></returns>
        public int ClientesDel(OracleCommand cm, int ContratoID)
        {
            try
            {
                #region CONNECTION
                cm.Parameters.Clear();
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_AE.SP_AE_ENTIDADES_DEL";
                #endregion

                #region PARAMETROS
                /// V_ID_CONTRATO
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.DbType = DbType.Int32;
                pr.OracleDbType = OracleDbType.Int32;
                pr.Value = ContratoID;
                pr.ParameterName = "V_ID_CONTRATO";
                cm.Parameters.Add(pr);

                #endregion

                #region EXECUTAR
                return cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel apagar os clientes do contrato", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'AE_Clientes.ClientesDel(int)'.", exp);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ContratoID"></param>
        /// <param name="CodLE"></param>
        /// <param name="CodGrupoEconomico"></param>
        /// <param name="CodSubGrupoEconomico"></param>
        /// <param name="CodCliente"></param>
        /// <returns></returns>
        public int ClientesInsert(int ContratoID, string CodLE, string CodGrupoEconomico, string CodSubGrupoEconomico, string CodCliente)
        {
            OracleConnection cn = new OracleConnection(this.ConnectionString);
            try
            {
                OracleCommand cm = cn.CreateCommand();
                cm.Connection = cn;
                cn.Open();
                return this.ClientesInsert(cm, ContratoID, CodLE,CodGrupoEconomico,CodSubGrupoEconomico,CodCliente);
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel inserir o cliente no contrato", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'AE_Clientes.ClientesInsert()'.", exp);
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Inserir um cliente no contrato
        /// SP_AE_ENTIDADE_INS
        /// (
        ///     V_ID_CONTRATO                 INT,
        ///     V_COD_LE                   VARCHAR2,
        ///     V_COD_GRUPO_ECONOMICO      VARCHAR2,
        ///     V_COD_SUB_GRUPO_ECONOMICO  VARCHAR2,
        ///     V_COD_CLIENTE              VARCHAR2
        /// );
        /// </summary>
        /// <param name="ContratoID"></param>
        /// <param name="CodLE"></param>
        /// <param name="CodGrupoEconomico"></param>
        /// <param name="CodSubGrupoEconomico"></param>
        /// <param name="CodCliente"></param>
        /// <returns></returns>
        public int ClientesInsert(OracleCommand cm, int ContratoID, string CodLE,string CodGrupoEconomico,string CodSubGrupoEconomico,string CodCliente)
        {
            try
            {
                #region CONNECTION
                cm.Parameters.Clear();
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "PK_AE.SP_AE_ENTIDADE_INS";
                #endregion
 
                #region PARAMETROS
                #region  V_ID_CONTRACTO
                OracleParameter pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Int32;
                pr.DbType = DbType.Int32;
                pr.ParameterName = "V_ID_CONTRACTO";
                pr.Value = ContratoID;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_COD_LE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 15;
                pr.ParameterName = "V_COD_LE";
                pr.Value = CodLE;
                cm.Parameters.Add(pr);
                #endregion

                #region  V_COD_GRUPO_ECONOMICO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 20;
                pr.ParameterName = "V_COD_GRUPO_ECONOMICO";
                pr.Value = CodGrupoEconomico;
                cm.Parameters.Add(pr);
                #endregion

                #region V_COD_SUB_GRUPO_ECONOMICO
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 20;
                pr.ParameterName = "V_COD_SUB_GRUPO_ECONOMICO";
                pr.Value = CodSubGrupoEconomico;
                cm.Parameters.Add(pr);
                #endregion

                #region V_COD_CLIENTE
                pr = cm.CreateParameter();
                pr.Direction = ParameterDirection.Input;
                pr.OracleDbType = OracleDbType.Varchar2;
                pr.DbType = DbType.String;
                pr.Size = 20;
                pr.ParameterName = "V_COD_CLIENTE";
                pr.Value = CodCliente;
                cm.Parameters.Add(pr);
                #endregion
                #endregion

                #region EXECUTE
                return cm.ExecuteNonQuery();
                #endregion
            }
            catch (OracleException exp)
            {
                throw new myDBException("DAL -> N�o foi poss�vel inserir o cliente no contrato", exp);
            }
            catch (Exception exp)
            {
                throw new Exception("DAL -> Erro inesperado no m�todo 'AE_Clientes.ClientesInsert()'.", exp);
            }
        }
        #endregion
    #endregion

    #region INTERFACES
        #region SELECTABLE
            /// <summary>
            /// Get Data From Source Table (Clientes)
            /// </summary>
            /// <param name="Where">Filtro a aplicar</param>
            public DataTable Select(string Where)
            {
                return this.ClientesGet(Where);
            }
        #endregion
    #endregion
}

