﻿using System;
using System.Collections.Generic;
using System.Text;


public class myDBException : Exception
{
    public myDBException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
public class CodeExistsException : myDBException
{
    public CodeExistsException(string message, Exception inner)
        : base(message, inner)
    {
    }
}