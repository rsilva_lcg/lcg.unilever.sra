using System;
using System.Data;
using Oracle.DataAccess;
using Oracle.DataAccess.Client;

public class LocaisEntrega
{
    #region FIELDS
        /// <summary>
        /// DataBase ConnectionString To Use In All Commands
        /// </summary>
        protected string ConnectionString;
        /// <summary>
        /// Current Logged UserName
        /// </summary>
        protected string User;
    #endregion

    #region METHODS
        /// <summary>
        /// CONSTRUTOR
        /// </summary>
        /// <param name="ConnString">DataBase ConnectionString To Use In All Commands</param>
        /// <param name="User">Current Logged UserName</param>
        public LocaisEntrega(string ConnString, string User)
        {
            this.ConnectionString = ConnString;
            this.User = User;
        }

    

    //    public void deleteLEUnificado(int ID)
    //    {

    //        //INACTIVAR UM LE MAPEADO
    //        OracleConnection cn = new OracleConnection(this.ConnectionString);
    //        OracleCommand cm = cn.CreateCommand();
    //        cm.Connection = cn;
    //        cm.CommandType = CommandType.StoredProcedure;
    //        cm.CommandText = "PK_LE.SP_LE_U_ACTIVE";
    //        try
    //        {
    //            OracleParameter pr = cm.CreateParameter();
    //            pr.Direction = ParameterDirection.Input;
    //            pr.DbType = DbType.Int32;
    //            pr.Value = ID;
    //            pr.ParameterName = "ID_UNIFICADO_LE";
    //            cm.Parameters.Add(pr);

    //            pr = cm.CreateParameter();
    //            pr.Direction = ParameterDirection.Input;
    //            pr.DbType = DbType.String;
    //            pr.Value = "F";
    //            pr.ParameterName = "V_IS_ACTIVE";
    //            cm.Parameters.Add(pr);

    //            pr = cm.CreateParameter();
    //            pr.Direction = ParameterDirection.Input;
    //            pr.DbType = DbType.String;
    //            pr.Value = this.User;
    //            pr.ParameterName = "V_USER";
    //            cm.Parameters.Add(pr);

    //            cn.Open();
    //            cm.ExecuteNonQuery();

    //        }
    //        catch (OracleException exp)
    //        {
    //            throw new myDBException("N�o foi poss�vel inativar o le unificado com ID=" + ID, exp);
    //        }
    //        finally
    //        {
    //            cn.Close();
    //        }

    //    }
    //    public int unificar(DataTable LEs)
    //{
    //    //INSERIR DADOS NA DB
    //    OracleConnection cn = new OracleConnection(this.ConnectionString);
    //    OracleCommand cm = cn.CreateCommand();
    //    OracleTransaction tr = null;
    //    int idLEUn = 0;

    //    try
    //    {
    //        cn.Open();
    //        tr = cn.BeginTransaction();
    //        cm = cn.CreateCommand();
    //        cm.Connection = cn;
    //        cm.CommandType = CommandType.StoredProcedure;
    //        cm.CommandText = "PK_LE.SP_LE_INS";


    //        foreach (DataRow dr in LEs.Rows)
    //        {
    //            cm.Parameters.Clear();

    //            OracleParameter pr = cm.CreateParameter();
    //            pr.Direction = ParameterDirection.InputOutput;
    //            pr.DbType = DbType.Int32;
    //            pr.Value = idLEUn;
    //            pr.ParameterName = "V_ID_LE_UNIFICADO";
    //            cm.Parameters.Add(pr);

    //            pr = cm.CreateParameter();
    //            pr.Direction = ParameterDirection.Input;
    //            pr.DbType = DbType.String;
    //            pr.Value = dr["COD_LOCAL_ENTREGA"];
    //            pr.ParameterName = "V_ID_LE_OLD";
    //            cm.Parameters.Add(pr);

    //            pr = cm.CreateParameter();
    //            pr.Direction = ParameterDirection.Input;
    //            pr.DbType = DbType.Int32;
    //            pr.Value = dr["COD_COMPANHIA"];
    //            pr.ParameterName = "V_ID_EMPRESA";
    //            cm.Parameters.Add(pr);

    //            pr = cm.CreateParameter();
    //            pr.Direction = ParameterDirection.Input;
    //            pr.DbType = DbType.String;
    //            pr.Value = dr["ID_CLIENTE_UN"];
    //            pr.ParameterName = "V_ID_CLIENTE_UNIFICADO";
    //            cm.Parameters.Add(pr);

    //            pr = cm.CreateParameter();
    //            pr.Direction = ParameterDirection.Input;
    //            pr.DbType = DbType.String;
    //            pr.Value = dr["IS_DEFAULT_NAME"];
    //            pr.ParameterName = "V_IS_DEFAULT_NAME";
    //            cm.Parameters.Add(pr);

    //            pr = cm.CreateParameter();
    //            pr.Direction = ParameterDirection.Input;
    //            pr.DbType = DbType.String;
    //            pr.Value = "ESCREVER USER ID";
    //            pr.ParameterName = "V_USER";
    //            cm.Parameters.Add(pr);


    //            cm.ExecuteNonQuery();
    //            idLEUn = int.Parse(cm.Parameters["V_ID_LE_UNIFICADO"].Value.ToString());
    //        }
    //        tr.Commit();
    //        return idLEUn;

    //    }
    //    catch (OracleException exp)
    //    {
    //        tr.Rollback();
    //        throw new myDBException("N�o foi poss�vel unificar os locais de entrega.", exp);
    //    }
    //    catch (Exception ex)
    //    {
    //        tr.Rollback();
    //        throw ex;
    //    }
    //    finally
    //    {
    //        cn.Close();
    //    }
    //}
    #endregion
}

