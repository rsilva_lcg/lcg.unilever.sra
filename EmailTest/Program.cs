﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;

namespace EmailTest
{
    class Program
    {

        protected override void OnStart(string[] args)
        {

            string strParams = string.Empty;
            strParams += "Conn Oracle: " + System.Configuration.ConfigurationManager.AppSettings["DBCS"] + "\n";
            strParams += "Separador letras (nome ficheiros): " + Settings.Default.FilenameSeparator.ToString() + "\n";
            strParams += "Modo SMTP debug?: " + System.Configuration.ConfigurationManager.AppSettings["SMTP_DEBUG"] + "\n";
            strParams += "Ficheiro log email: " + System.Configuration.ConfigurationManager.AppSettings["MailLog_File"] + "\n";
            strParams += "SMTP Server: " + System.Configuration.ConfigurationManager.AppSettings["SMTP_SERVER"] + "\n";

            EventLog.WriteEntry("wsSRA-CONFIG", strParams, EventLogEntryType.Information);

            //ExecuteCode();
        }

        private void service_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            service_timer.Stop();
            service_timer.Interval = Convert.ToInt32(Settings.Default.wsTimeInterval) * 60000;
            EventLog.WriteEntry("wsSRA", "Tick starting..", EventLogEntryType.Information);
            ExecuteCode();
            EventLog.WriteEntry("wsSRA", "Tick ending :-)", EventLogEntryType.Information);
            service_timer.Start();
        }

        private void ExecuteCode()
        {
            try
            {
                mainFileMon.ProcessAllFiles(lstFolders, Settings.Default.FilenameSeparator);
                mainFileMon.CheckValidated();
                mainFileMon.ContratosRenewal("wsSRA");//antes do Close, por causa do NumProcSeguinte
                mainFileMon.ContratosClose();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("wsSRA", ex.Message, EventLogEntryType.Error);
            }
        }

        static void Main(string[] args)
        {
        }
    }
}
