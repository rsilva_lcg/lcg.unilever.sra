﻿function MenuOnMouseOver(divid, seltabid) {
    if (divid != seltabid) {
        if (document.getElementById('div' + seltabid) != null)
            document.getElementById('div' + seltabid).style.display = 'none';
        if (document.getElementById('div' + divid) != null)
            document.getElementById('div' + divid).style.display = 'block';
    }
    else {
        if (document.getElementById('div' + seltabid) != null)
            document.getElementById('div' + seltabid).style.display = 'block';
    }

    if (document.getElementById('topdiv' + divid) != null) {
        document.getElementById('topdiv' + divid).style.color = '#a29061';
        document.getElementById('topdiv' + divid).style.backgroundColor = '#000000';
    }
}

function MenuOnMouseOut(divid, seltabid) {
    if (divid != seltabid) {
        if (document.getElementById('div' + divid) != null) {
            document.getElementById('div' + divid).style.display = 'none';
        }
        if (document.getElementById('topdiv' + divid) != null) {
            document.getElementById('topdiv' + divid).style.color = '#000000';
            document.getElementById('topdiv' + divid).style.backgroundColor = '#FFFFFF';
        }
        if (document.getElementById('div' + seltabid) != null)
            document.getElementById('div' + seltabid).style.display = 'block';
    }
}
function ClearTextBox(obj, bit, msg) {
    var elem = document.getElementById(obj);

    if (bit) { if (elem.value == msg) { elem.value = ''; } }
    else { if (elem.value == '') { elem.value = msg; } else { elem.value; } }
    return false;
}


function DisableTextBox(obj, rowIndx, gvId) {
    var elem = document.getElementById(obj);
    if (elem.type == "checkbox") {

        var gv = document.getElementById(gvId);
        var rowElement = gv.rows[rowIndx];
        
       
        for (i = 0; i < rowElement.cells.length; i++) {
        
            var elList = rowElement.cells[i].getElementsByTagName('input');

            for (j = 0; j < elList.length; j++) {

                if (elList[j].type == 'text') {
                    if (elem.checked == true)
                    { elList[j].value = '0'; elList[j].disabled = true; }
                    else
                    { elList[j].disabled = false; }
                }
            }
        }
    }
    return true;


}


function HideFlow(obj1,obj2, chk) {

    var elem1 = document.getElementById(obj1);
    var elem2 = document.getElementById(obj2);
    var chk = document.getElementById(chk);
    if (chk.type == 'checkbox') {
        if (elem1 != null && elem2 != null) {
            if (chk.checked) {
                elem1.style.visibility = 'hidden';
                elem1.style.display = 'none';
                elem2.style.visibility = 'visible';
                elem2.style.display = 'block';
            }
            else {
                elem1.style.visibility = 'visible';
                elem1.style.display = 'block';
                elem2.style.visibility = 'hidden';
                elem2.style.display = 'none';
            }
        } 
    }
        
    return true;
}

function enableValidators(vals, textValue, defaultValue) {

    var enable = false;
    if (textValue != null && textValue.value != defaultValue) { enable = true } else { enable = false; }
    for (x in vals) {

        ValidatorEnable(document.getElementById(vals[x]), enable);
        document.getElementById(vals[x]).isvalid = true;
        ValidatorUpdateDisplay(document.getElementById(vals[x]));
    }
}


function GetFlowTotal(obj, rowIndx, gvId) {
    var elem = document.getElementById(obj);
    var sum = 0;

        var gv = document.getElementById(gvId);
        var rowElement = gv.rows[rowIndx];
        

        for (i = 0; i < rowElement.cells.length; i++) {

            var elList = rowElement.cells[i].getElementsByTagName('input');

            for (j = 0; j < elList.length; j++) {

                if (elList[j].type == 'text') {
                    if(isNumeric(elList[j].value))
                        sum += elList[j].value;
                }
            }
        }
        elem.value = sum;     

    return true;


}



function DisableValidators(obj, val1, val2, selection, txt1, txt2) {
    var elem = document.getElementById(obj);
    var v1 = document.getElementById(val1);
    var v2 = document.getElementById(val2);
    var t1 = document.getElementById(txt1);
    var t2 = document.getElementById(txt2);
    if (elem.type == "select-one") {
        if(elem.value > parseInt(selection)){
            t1.value = '01-01-1900';
            t2.value = '0';
            t1.style.visibility = 'hidden';
            t1.style.display = 'none';
            t2.style.visibility = 'hidden';
            t2.style.display = 'none';
        }
        else {
            if ((t1.value == '' || t1.value == '01-01-1900') && (t2.value == '' || t2.value == '0')) {
                t1.value = 'Data de Avaliação';
                t2.value = 'Valor de Venda Forçada';
            }
            t1.style.visibility = 'visible';
            t1.style.display = 'block';
            t2.style.visibility = 'visible';
            t2.style.display = 'block';
        }
    }
    return true;
}