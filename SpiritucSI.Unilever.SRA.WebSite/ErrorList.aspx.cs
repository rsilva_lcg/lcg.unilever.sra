using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace SpiritucSI.Unilever.SRA.WF.WebSite
{
    public partial class ErrorList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                bll_admin_RSS bll = new bll_admin_RSS(ConfigurationManager.ConnectionStrings["LoggingDatabase"].ToString());
                DataTable dt = bll.SelectErrorByLogId(Request.QueryString["id"].ToString());

                DataRow row = dt.NewRow();
                if (dt.Rows.Count>0)
                    row = dt.Rows[0];

                lblTitle.Text = "Log Id: " + row["LogId"].ToString() + "  Data: " + ((DateTime)row["Timestamp"]).ToString("dd-MM-yyyy hh:mm:ss");
                lblMessage.Text  = row["Message"].ToString();
                lblDate.Text = ((DateTime)row["Timestamp"]).ToShortDateString();


            }
            else
            {
                LoadRSS();
            }

        }

        private void LoadRSS()
        {
            bll_admin_RSS bll = new bll_admin_RSS(ConfigurationManager.ConnectionStrings["LoggingDatabase"].ToString());
            DataTable dt = bll.SelectErrors();

            RssChannel channel = new RssChannel(new Uri(this.Request.Url.Scheme + @"://" + this.Request.Url.Host + ConfigurationManager.AppSettings["BaseURL"]), "BPN AII: Erros Aplica��o", "Lista de Erros da aplica��o BPN AII");
            RssGuid rg;

            foreach (DataRow row in dt.Rows)
            {
                RssItem item = new RssItem();

                item.Title = "Log Id: " + row["LogId"].ToString() + "  Data: " + ((DateTime)row["Timestamp"]).ToString("dd-MM-yyyy hh:mm:ss")  ;
                
                item.Description = row["FormattedMessage"].ToString();
                item.PublicationDate = (DateTime)row["Timestamp"];
                item.Link = new Uri( this.Request.Url.Scheme + @"://" + this.Request.Url.Host + ConfigurationManager.AppSettings["BaseURL"] + @"/ErrorList.aspx?id=" + row["logId"].ToString() );

                rg = new RssGuid();
                rg.Value = row["LogId"].ToString();
                rg.IsPermanentLink = false;

                item.Guid = rg;

                channel.AddItem(item);
            }

            channel.Link = new Uri(this.Request.Url.Scheme + @"://" + this.Request.Url.Host + ConfigurationManager.AppSettings["BaseURL"] +@"/ErrorList.aspx");
            channel.Language = new System.Globalization.CultureInfo("pt-PT");
            channel.PublicationDate = DateTime.Now;

            RssFeed feed = new RssFeed("BPN AII: Erros Aplica��o");

            feed.Channel = channel;
            Response.ContentType = "text/xml";

            StreamWriter sr = new StreamWriter(Response.OutputStream, Encoding.UTF8 );
            sr.Write(feed.CreateNavigator().OuterXml);
            sr.Flush();
            Response.End();
        }
    }
}
