﻿using System;
using System.IO;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.Adapters;

/// <summary>
/// Summary description for PageStateAdapter
/// </summary>
public class PageStateAdapter : System.Web.UI.Adapters.PageAdapter
{
    public override PageStatePersister GetStatePersister()
    {
        return new SessionPageStatePersister(this.Page);
    }
}
