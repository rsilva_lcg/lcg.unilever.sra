﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SpiritucSI.Fnac.MasterCatalog.Website.Login" Theme="MainTheme" %>
<%@ Register Src="~/UserControls/Login/ucLoginForm.ascx"  TagName="Login" TagPrefix="uc1"%>   

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <uc1:Login ID="ucLoginForm" runat="server"/>
</asp:Content>
