﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.Unilever.SRA.WF.WebSite.Common;

namespace SpiritucSI.Unilever.SRA.WF.WebSite.Masters
{
    public partial class Main : SuperMasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ClientScript.RegisterClientScriptInclude("JQuery1.3", this.BaseUrl + "/JScript/jquery-1.4.min.js");
            this.Page.ClientScript.RegisterClientScriptInclude("MyJQuery", this.BaseUrl + "/JScript/myJQuery.js");
            this.Page.ClientScript.RegisterClientScriptInclude("Utils", this.BaseUrl + "/JScript/Utils.js");
        }

        public void ShowMessage(string message, Exception e, SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common.ResponseMsgPanel.MessageType mType)
        {
            this.ResponseMsgPanel1.ShowMessage(message, e, mType);
        }
    }
}
