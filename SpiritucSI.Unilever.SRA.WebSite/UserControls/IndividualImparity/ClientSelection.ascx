﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientSelection.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.UserControls.IndividualImparity.ClientSelection" %>

    
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top">
        <td align="left">
            <label class="formHeader">
                Selecção de Clientes</label>
            <hr />
        </td>
    </tr>
    <tr valign="top">
        <td align="left">
        <asp:GridView ID= "GV_Clients" runat="server" AllowPaging = "true" AllowSorting ="true" AutoGenerateColumns = "false">
            <Columns>
            <asp:BoundField HeaderText = "Nome" DataField ="Name" />
            <asp:BoundField HeaderText = "Entidade" DataField = "EntityName" />
            <asp:BoundField HeaderText = "Gestor" DataField = "Manager"/>
            <asp:BoundField HeaderText = "ID" DataField = "idClient" />
            <asp:CheckBoxField HeaderText = "Seleccionar" />
            </Columns>
        </asp:GridView>
    </td>
    </tr>
    </table>
