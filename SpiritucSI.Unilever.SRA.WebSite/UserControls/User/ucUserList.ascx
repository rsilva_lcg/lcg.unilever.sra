<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucUserList.ascx.cs" Inherits="SpiritucSI.Fnac.MasterCatalog.Website.UserControls.User.ucUserList" %>
<%@ Register Src="~/UserControls/Common/ucSubTitle.ascx" TagName="SubTitle" TagPrefix="uc1" %>
<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>

<uc1:SubTitle ID="SubTitle1" runat="server" Title='<%$Resources: SubTitle1 %>' Maximized="false" TargetDivId="SearchTemplate"/>
<div id="SearchTemplate" runat="server" class="SubContentCenter">
  <%--  <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>    --%>    
			<fieldset>
				<div class="ThreeColumn">	<div class="Row">
		<div class="SearchText"><asp:Label ID="lblName" runat="server" Text='<%$ Resources:Name %>'></asp:Label></div>
		<div class="SearchTextBoxDiv"><asp:TextBox runat="server" CssClass="SearchTextBox" ID="txtName"></asp:TextBox></div>                
	</div></div>
				<div class="ThreeColumn">	<div class="Row">
		<div class="SearchText"><asp:Label ID="lblUsername" runat="server" Text='<%$ Resources:Username %>'></asp:Label></div>
		<div class="SearchTextBoxDiv"><asp:TextBox runat="server" CssClass="SearchTextBox" ID="txtUsername"></asp:TextBox></div>                
	</div></div><div class="ThreeColumn">	<div class="Row">
    <div class="SearchText"></asp:Label></div>
	<div class="SearchTextBoxDiv">
	    <div class="buttonSearchDivLeft"><asp:Button ID="btnClean" CssClass="button1" runat="server"  meta:resourcekey="Clean" OnClick="btnClean_Click" CausesValidation="false"/></div>
	    <div class="buttonSearchDivRight"><asp:Button ID="btnSearch" CssClass="button1" runat="server"  meta:resourcekey="Search" OnClick="btnSearch_Click" CausesValidation="false" /></div>
    </div>
</div>	<div class="Row">
		<div class="SearchText"><asp:Label ID="lblEmail" runat="server" Text='<%$ Resources:Email %>'></asp:Label></div>
		<div class="SearchTextBoxDiv"><asp:TextBox runat="server" CssClass="SearchTextBox" ID="txtEmail"></asp:TextBox></div>                
	</div></div>
			</fieldset>
<%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
</div>
<uc1:SubTitle ID="SubTitle2" runat="server" Title='<%$Resources:SubTitle2 %>' Maximized="true" TargetDivId="List" Static="true"/>
<div id="List" runat="server" class="SubContentCenterTable">
    <asp:UpdatePanel ID="up2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="float:left; width:100%">
				<obout:Grid id="grid1" Language="pt" AllowPaging="true" AllowFiltering="true" PageSize="10" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false" AllowPageSizeSelection="false" AllowAddingRecords="false" runat="server" FolderStyle="~/App_Themes/MainTheme/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/MainTheme/Grid/localization" DataSourceID="ods1">
    <Columns>
		<obout:Column ID="Column0" Width="270" DataField="Username" HeaderText='<%$ Resources:HeaderUsername %>' runat="server" TemplateID="RegularGridTemplate"></obout:Column><obout:Column ID="Column1" Width="270" DataField="Name" HeaderText='<%$ Resources:HeaderName %>' runat="server" TemplateID="RegularGridTemplate"></obout:Column><obout:Column ID="Column2" Width="270" DataField="Email" HeaderText='<%$ Resources:HeaderEmail %>' runat="server" TemplateID="RegularGridTemplate"></obout:Column>
        <obout:Column ID="ColumnEdit" Width="80" DataField="Id" runat="server" TemplateID="GridTemplateEdit" HeaderText=" "></obout:Column>
        <obout:Column ID="ColumnDelete" Width="80" runat="server" TemplateID="GridTemplateDelete" HeaderText=" "></obout:Column>
    </Columns>
    <Templates>
        <obout:GridTemplate runat="server" id="RegularGridTemplate">
            <Template>
               <asp:Label ID="lbl1" runat="server" Text='<%# Container.Value %>'></asp:Label>
            </Template>
        </obout:GridTemplate>
        <obout:GridTemplate runat="server" id="GridTemplateEdit">
            <Template>
                <div style="float:left; padding-left:1px">
                    <asp:Hyperlink ID="lnk1" runat="server" Text="editar" CssClass="btnHyperlink" NavigateUrl='<%# "~/Admin/UserDetail.aspx?uid="+Container.DataItem["Id"]  %>'></asp:Hyperlink>
                </div>
            </Template>
        </obout:GridTemplate>
        <obout:GridTemplate runat="server" id="GridTemplateDelete">
            <Template>
                <div style="float:left; padding-left:1px">
                    <asp:LinkButton ID="LinkButton1" runat="server" Text="eliminar" CssClass="btnHyperlink" NavigateUrl='<%# "~/UserDetail.aspx?uid="+Container.DataItem["Id"] %>' onclick="lnkDelete_Click" OnClientClick='<%# "javascript:return window.confirm(&#39;" + this.GetGlobalResourceObject("Global","AreYouSure").ToString() + "&#39;);"%>'></asp:LinkButton>
                </div>
            </Template>
        </obout:GridTemplate>
    </Templates>
</obout:Grid>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:AsyncPostBackTrigger ControlID="btnClean" />
        </Triggers>
    </asp:UpdatePanel>
</div>
<%--<div class="CreateButtonDiv" runat="server" id="divButton">
    <asp:Button ID="btnCreate" CssClass="button1" runat="server" 
        meta:resourcekey="Create" PostBackUrl="~/Admin/UserDetail.aspx"/>
</div>--%>
<div style="float:left; width:100%; height:10px">&nbsp;</div>
<asp:ObjectDataSource runat="server" ID="ods1" TypeName="SpiritucSI.Fnac.MasterCatalog.Website.UserControls.User.ucUserList" SelectMethod="Get" EnablePaging="true" SelectCountMethod="Count" SortParameterName="orderBy">
<SelectParameters>
	<asp:ControlParameter ControlID="txtUsername" Name="Username" DbType="String"/><asp:ControlParameter ControlID="txtName" Name="Name" DbType="String"/><asp:ControlParameter ControlID="txtEmail" Name="Email" DbType="String"/>
</SelectParameters>
</asp:ObjectDataSource>