using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using SpiritucSI.BPN.AII.WebSite.Common;


namespace SpiritucSI.Fnac.MasterCatalog.Website.UserControls.User
{
    public partial class ucUserList : SuperControl
    {
        #region PROPERTIES
        public List<SpiritucSI.BPN.AII.Business.Entities.User> Users { get; set; } 
        #endregion

        #region PAGE_EVENTS
        protected void Page_Load(object sender, EventArgs e)
        {
        } 
        #endregion
        
        #region CONTROL_EVENTS
        protected void btnClean_Click(object sender, EventArgs e)
        {
            this.txtUsername.Text = string.Empty;
	        this.txtName.Text = string.Empty;
	        this.txtEmail.Text = string.Empty;
	
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region METHODS
        public int Count(string Username, string Name, string Email)
        {
            return this.Users.Count();
           
        }

        public IList Get(string Username, string Name, string Email, string orderBy, int maximumRows, int startRowIndex)
        {
            this.Users = UserService.GetAll(Username ?? "", Name ?? "", Email ?? "", startRowIndex, maximumRows, orderBy, this.CurrentUser);
            return this.Users;
        } 
        #endregion

        
    }
}