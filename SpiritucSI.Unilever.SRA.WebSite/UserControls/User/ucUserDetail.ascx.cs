using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.Security;
using SpiritucSI.BPN.AII.WebSite.Common;

using SpiritucSI.BPN.AII.WebSite.Masters;
using SpiritucSI.BPN.AII.WebSite.UserControls.Common;


namespace SpiritucSI.Fnac.MasterCatalog.Website.UserControls.User
{
    public partial class ucUserDetail : SuperControl
    {

        #region PROPERTIES
        public Guid UserId
        {
            get
            {
                try{
                    if (this.Request["uid"] != null)
                    {
                        return new Guid(this.Request["uid"]);
                    }
                }
                catch(Exception ex)
                {
                    return Guid.Empty;
                }

                return Guid.Empty;
            }
        }

        public SpiritucSI.BPN.AII.Business.Entities.User User = null;
        public string GetRedirectPage { get; set; }

        #endregion

        #region EVENTS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {               
                this.LoadCheckBoxListFunctionRoles();
                this.LoadCheckBoxListOrganizationRoles();
                if (this.UserId != Guid.Empty)
                {
                    this.Filldata();
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.Save(((Button)sender).CommandName == "Update", false);
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Gets the redirect.
        /// </summary>
        /// <returns></returns>
        public string GetRedirect()
        {
            return this.redirect.ClientID;
        }

        /// <summary>
        /// Fill the data for the product.
        /// </summary>
        private void Filldata()
        {
            this.btnSave.CommandName = "Insert";

            this.User = UserService.GetOne(UserId);
            if (this.User != null)
            {
                this.st1.Title = this.GetLocalResourceObject("Title") + this.User.Name;
                this.txtUsername.Attributes.Add("Rule", "2");

                this.txtUsername.Text = this.User.Username;
                this.txtName.Text = this.User.Name;
                this.txtEmail.Text = this.User.Email;
                this.txtConfirmEmail.Text = this.User.Email;
                this.txtPhone.Text = this.User.Phone;
                this.txtMobile.Text = this.User.Mobile;
                this.txtANotes.Text = this.User.Observations;
                this.chkApproved.Checked = this.User.IsApproved;
                this.chkBlocked.Checked = this.User.IsLockedOut;
                this.chkBlocked.Enabled = this.User.IsLockedOut;


                foreach (ListItem li in this.ckblOrganizationRoles.Items)
                {
                    if (this.User.Roles.Any(x=>x.Name==li.Value))
                    {
                        li.Selected = true;
                    }
                }

                foreach (ListItem li in this.ckblFunctionRoles.Items)
                {
                    if (this.User.Roles.Any(x=>x.Name==li.Value))
                    {
                        li.Selected = true;
                    }
                }
                
                this.btnSave.CommandName = "Update";
            }
        }

        /// <summary>
        /// Saves the specified update.
        /// </summary>
        /// <param name="update">if set to <c>true</c> [update].</param>
        /// <param name="sendToFillPage">if set to <c>true</c> [send to fill page].</param>
        private void Save(bool update, bool sendToFillPage)
        {
            bool checkprocess = false;
            SpiritucSI.BPN.AII.Business.Entities.User entity = null;
            if (update)
            {
                entity = new SpiritucSI.BPN.AII.Business.Entities.User(this.UserId, this.txtEmail.Text, this.txtUsername.Text, string.Empty, this.txtName.Text, this.chkBlocked.Checked, this.chkApproved.Checked, this.txtMobile.Text, this.txtANotes.Text, this.txtPhone.Text);
                foreach (ListItem li in this.ckblFunctionRoles.Items)
                {
                    if (li.Selected)
                    {
                        entity.AddRole(li.Value, RoleType.Functional);
                    }
                }
                foreach (ListItem li in this.ckblOrganizationRoles.Items)
                {
                    if (li.Selected)
                    {
                        entity.AddRole(li.Value, RoleType.Organization);
                    }
                }
                checkprocess = true;
            }
            else
            {
                if (Membership.GetUser(this.txtUsername.Text) == null)
                {
                    entity = new SpiritucSI.BPN.AII.Business.Entities.User(Guid.Empty, this.txtEmail.Text, this.txtUsername.Text, string.Empty, this.txtName.Text, this.chkBlocked.Checked, this.chkApproved.Checked, this.txtMobile.Text, this.txtANotes.Text, this.txtPhone.Text);
                    foreach (ListItem li in this.ckblFunctionRoles.Items)
                    {
                        if (li.Selected)
                        {
                            entity.AddRole(li.Value, RoleType.Functional);
                        }
                    }
                    foreach (ListItem li in this.ckblOrganizationRoles.Items)
                    {
                        if (li.Selected)
                        {
                            entity.AddRole(li.Value, RoleType.Organization);
                        }
                    }
                    checkprocess = true;
                }
            }
            if (checkprocess)
            {
                try
                {
                    string id = UserService.Insert(entity, update);
                    this.GetRedirectPage = "UserList.aspx";
                    this.redirect.Value = "true";
                    ((FrontOffice)this.Page.Master).ShowMessage(this.GetLocalResourceObject("Success").ToString(), null, ResponseMsgPanel.MessageType.Success);
                }
                catch (BLLLoggedException lex)
                {
                    if (lex.Message == "The E-mail address is already in use.")
                    {
                        ((FrontOffice)this.Page.Master).ShowMessage("Esse email j� est� a ser utilizado", lex, ResponseMsgPanel.MessageType.Error);
                    }
                    else
                    {
                        if (!ExceptionPolicy.HandleException(lex, "UIPolicy"))
                        {
                            ((FrontOffice)this.Page.Master).ShowMessage(Resources.Global.Error, lex, ResponseMsgPanel.MessageType.Error);
                        }
                    }
                }
            }
            else
            {
                ((Main)this.Page.Master).ShowMessage(this.GetLocalResourceObject("NotUnique").ToString(), null, ResponseMsgPanel.MessageType.Error);
            }

        }

        /// <summary>
        /// Loads the check box list function roles.
        /// </summary>
        private void LoadCheckBoxListFunctionRoles()
        {
            this.ckblFunctionRoles.DataSource = RoleService.GetAllFunctional(true);
            this.ckblFunctionRoles.DataBind();
        }

        /// <summary>
        /// Loads the check box list organization roles.
        /// </summary>
        private void LoadCheckBoxListOrganizationRoles()
        {
            this.ckblOrganizationRoles.DataSource = RoleService.GetAllOrganizational(true);
            this.ckblOrganizationRoles.DataBind();
        }


        #endregion

    }
}