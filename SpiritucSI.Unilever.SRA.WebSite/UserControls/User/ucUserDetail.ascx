<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucUserDetail.ascx.cs" Inherits="SpiritucSI.Fnac.MasterCatalog.Website.UserControls.User.ucUserDetail" %>
<%@ Register Src="~/UserControls/Common/ucSubTitle.ascx" TagName="SubTitle" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/Common/MyHelper.ascx" TagName="MyHelper" TagPrefix="uc1" %>
<script type="text/javascript">
    function checkBoxClickedFunctionRoles(source, args) {
        args.IsValid = ($("#FunctionRoles input:checked").length > 0);
    }
    function checkBoxClickedOrganizationRoles(source, args) {
        args.IsValid = ($("#OrganizationRoles input:checked").length > 0);
    }       
</script>
<uc1:SubTitle ID="st1" runat="server" Title='<%$ Resources:SubTitle1 %>' Maximized="true" Static="true" TargetDivId="UserDetail" />
<div id="UserDetail" runat="server" class="SubContentCenter">
    <fieldset>
        <div class="Colunn-left2">
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblUsername" runat="server" Text='<%$ Resources:Username %>'></asp:Label></div>
                <div class="form-texbox">
                    <asp:TextBox runat="server" CssClass="TextBoxSmaller" ID="txtUsername"></asp:TextBox></div>
                <uc1:MyHelper ID="ucMhUsername" runat="server" Key="UserCreateUsername"></uc1:MyHelper>
                <div class="form-text-Obrigatorio">
                    <asp:RequiredFieldValidator ID="reqUsername" runat="server" CssClass="lblError" ControlToValidate="txtUsername"
                        ValidationGroup="Save" Display="Dynamic" ErrorMessage="<%$ Resources:Global, RequiredField %>"></asp:RequiredFieldValidator></div>
            </div>
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblName" runat="server" Text='<%$ Resources:Name %>'></asp:Label></div>
                <div class="form-texbox">
                    <asp:TextBox runat="server" CssClass="TextBoxSmaller" ID="txtName"></asp:TextBox></div>
                <uc1:MyHelper ID="ucMhName" runat="server" Key="UserCreateName"></uc1:MyHelper>
                <div class="form-text-Obrigatorio">
                    <asp:RequiredFieldValidator ID="reqName" runat="server" CssClass="lblError" ControlToValidate="txtName"
                        ValidationGroup="Save" Display="Dynamic" ErrorMessage="<%$ Resources:Global, RequiredField %>"></asp:RequiredFieldValidator></div>
            </div>
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblEmail" runat="server" Text='<%$ Resources:Email %>'></asp:Label></div>
                <div class="form-texbox">
                    <asp:TextBox runat="server" CssClass="TextBoxSmaller" ID="txtEmail"></asp:TextBox></div>
                <uc1:MyHelper ID="ucMhEmail" runat="server" Key="UserCreateEmail"></uc1:MyHelper>
                <div class="form-text-Obrigatorio">
                  <%--  <asp:RequiredFieldValidator ID="reqEmail" runat="server" CssClass="lblError" ControlToValidate="txtEmail"
                        ValidationGroup="Save" Display="Dynamic" ErrorMessage="<%$ Resources:Global, RequiredField %>"></asp:RequiredFieldValidator>           --%></div>
                   <%-- <asp:RegularExpressionValidator ID="C_Val_Email1" runat="server" CssClass="lblError" ControlToValidate="txtEmail" ErrorMessage="<%$ Resources:Global, InvalidEmail %>"
                        ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+" ValidationGroup="Save" Display="Dynamic"></asp:RegularExpressionValidator>--%>
 </div>
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblConfirmEmail" runat="server" Text='<%$ Resources:ConfirmEmail %>'></asp:Label></div>
                <div class="form-texbox">
                    <asp:TextBox runat="server" CssClass="TextBoxSmaller" ID="txtConfirmEmail"></asp:TextBox></div>
                <uc1:MyHelper ID="ucMhConfirmEmail" runat="server" Key="UserCreateConfirmEmail">
                </uc1:MyHelper>
                <div class="form-text-Obrigatorio">
                    <%--<asp:RequiredFieldValidator ID="reqConfirmEmail" runat="server" CssClass="lblError"
                        ControlToValidate="txtConfirmEmail" ValidationGroup="Save" Display="Dynamic"
                        ErrorMessage="<%$ Resources:Global, RequiredField %>"></asp:RequiredFieldValidator>
                    <asp:CompareValidator id="valCompare" ValidationGroup="Save" runat="server" ControlToValidate="txtConfirmEmail" ControlToCompare="txtEmail" Operator="Equal" ErrorMessage="<%$ Resources:Global, RequiredDifMail %>" Display="dynamic"></asp:CompareValidator>--%>
                </div>
            </div>
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblPhone" runat="server" Text='<%$ Resources:Phone %>'></asp:Label></div>
                <div class="form-texbox">
                    <asp:TextBox runat="server" CssClass="TextBoxSmaller" ID="txtPhone"></asp:TextBox></div>
                <uc1:MyHelper ID="ucMhPhone" runat="server" Key="UserCreatePhone"></uc1:MyHelper>
            </div>
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblMobile" runat="server" Text='<%$ Resources:Mobile %>'></asp:Label></div>
                <div class="form-texbox">
                    <asp:TextBox runat="server" CssClass="TextBoxSmaller" ID="txtMobile"></asp:TextBox></div>
                <uc1:MyHelper ID="ucMhMobile" runat="server" Key="UserCreateMobile"></uc1:MyHelper>
            </div>
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblNotes" runat="server" Text='<%$ Resources:Notes %>'></asp:Label></div>
                <div class="form-texbox">
                    <asp:TextBox ID="txtANotes" CssClass="TextBoxSmaller" TextMode="MultiLine" Rows="3"
                        runat="server"></asp:TextBox></div>
                <uc1:MyHelper ID="ucMhNotes" runat="server" Key="UserCreateNotes"></uc1:MyHelper>
            </div>
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblApproved" runat="server" Text='<%$ Resources:Approved %>'></asp:Label></div>
                <div class="form-texbox">
                    <asp:CheckBox ID="chkApproved" runat="server" Checked="true"></asp:CheckBox></div>
                <uc1:MyHelper ID="ucMhApproved" runat="server" Key="UserCreateApproved"></uc1:MyHelper>
            </div>
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblBlocked" runat="server" Text='<%$ Resources:Blocked %>'></asp:Label></div>
                <div class="form-texbox">
                    <asp:CheckBox ID="chkBlocked" runat="server"></asp:CheckBox></div>
                <uc1:MyHelper ID="ucMhBlocked" runat="server" Key="UserCreateBlocked"></uc1:MyHelper>
            </div>
            <div class="Row" style="padding-top: 10px;">
                <div class="form-text">
                    <asp:Label ID="lblBlank" runat="server"></asp:Label></div>
                <div style="float: left; width: 210px">
                    <div class="btnLeft">
                        <asp:Button ID="bntCancel" CssClass="button1" runat="server" Text='<%$ Resources:Cancel %>'
                            PostBackUrl="~/Admin/UserList.aspx" />
                    </div>
                    <div class="btnRight">
                       <asp:UpdatePanel ID="upSave" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSave" CssClass="button1" runat="server" ValidationGroup="Save"
                                    Text='<%$ Resources:Save %>' OnClientClick="$(document).find('div .Helper').siblings('div').hide('fast');"
                                    OnClick="btnSave_Click" />
                                <asp:HiddenField ID="redirect" runat="server" Value="false" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div class="Column-right2">
            <div class="Row">
                <div class="form-text">
                    <asp:Label ID="lblFunctionRoles" runat="server" Text='<%$ Resources:FunctionRoles %>'></asp:Label></div>
                <div class="form-texbox" id="FunctionRoles" style="width:140px">
                    <asp:CheckBoxList ID="ckblFunctionRoles" CssClass="mk_chklist" runat="server" DataTextField="Name" DataValueField="Name">
                    </asp:CheckBoxList>
                </div>
                <uc1:MyHelper ID="ucMhFunctionRoles" runat="server" Key="UserCreateFunctionRoles">
                </uc1:MyHelper>
                <div class="form-text-Obrigatorio">
                    <asp:CustomValidator ID="valFunctionRoles" runat="server" ClientValidationFunction="checkBoxClickedFunctionRoles" ValidationGroup="Save" Display="Dynamic" ErrorMessage="<%$ Resources:valRoles %>"></asp:CustomValidator>
                </div>
            </div>
            <div class="Row">
                <div class="form-text" >
                    <asp:Label ID="lblOrganizationRoles" runat="server" Text='<%$ Resources:OrganizationRoles %>'></asp:Label></div>
                <div class="form-texbox" id="OrganizationRoles" style="width:140px">
                    <asp:CheckBoxList CssClass="mk_chklist" ID="ckblOrganizationRoles" runat="server" DataTextField="Name" DataValueField="Name">
                    </asp:CheckBoxList>
                </div>
                <uc1:MyHelper ID="ucMhOrganizationRoles" runat="server" Key="UserCreateOrganizationRoles">
                </uc1:MyHelper>
                <div class="form-text-Obrigatorio">
                    <asp:CustomValidator ID="valOrganizationRoles" runat="server" ClientValidationFunction="checkBoxClickedOrganizationRoles" ValidationGroup="Save" Display="Dynamic" ErrorMessage="<%$ Resources:valRoles %>"></asp:CustomValidator>
                </div>
            </div>
        </div>
    </fieldset>
</div>
