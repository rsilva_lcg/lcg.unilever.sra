﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucUpdateProgress.ascx.cs" Inherits="SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common.ucUpdateProgress" %>
<asp:UpdateProgress ID="UpdateProgress" DisplayAfter="0" DynamicLayout="true" runat="server">
    <ProgressTemplate>
        <div id="progressBackgroundFilter"><div><asp:Image ID="loader1" runat="server" ImageUrl="~/images/common/loading49.gif" AlternateText="Por favor aguarde enquanto o seu pedido é processado" /></div></div>       
    </ProgressTemplate>
</asp:UpdateProgress>