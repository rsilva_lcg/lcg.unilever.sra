﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common
{
    public partial class MainMenu : System.Web.UI.UserControl
    {
        public bool Enabled { get; set; }
        public string SiteMapProv { get; set; }
        public string EnableFor { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

                SiteMapProvider smp = null;
                
                switch (SiteMapProv)
                {
                    case "FrontOffice": smp = SM_FrontOffice.Provider; break;
                    case "BackOffice": smp = SM_Backoffice.Provider; break;
                    case "Analisys": smp = SM_Analisys.Provider; break;
                    default: smp = null; break;
                }


                if (smp != null && smp.RootNode != null && smp.RootNode.ChildNodes != null)
                {
                    this.rptTopMenu.DataSource = (from SiteMapNode obj in smp.RootNode.ChildNodes select obj).ToList();
                    this.rptTopMenu.DataBind();

                }
            }
        }

        protected void rptTopMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                SiteMapNode node = ((SiteMapNode)e.Item.DataItem);
                this.Enabled = (node.Key.ToLower() == this.Request.Url.AbsolutePath.ToLower() || node.Title == EnableFor);

                if (node.ChildNodes.Count == 0)
                { 
                    ((HyperLink)e.Item.FindControl("HyperLinkMenuTab")).NavigateUrl = ((SiteMapNode)e.Item.DataItem).Url;
                }

                ((Repeater)e.Item.FindControl("rptSubMenu")).DataSource = (from SiteMapNode obj in node.ChildNodes select obj).ToList();
                ((Repeater)e.Item.FindControl("rptSubMenu")).DataBind();

                HtmlGenericControl Left = ((HtmlGenericControl)e.Item.FindControl("Left"));
                HtmlGenericControl Center = ((HtmlGenericControl)e.Item.FindControl("Center"));
                HtmlGenericControl Right = ((HtmlGenericControl)e.Item.FindControl("Right"));

                Left.Attributes["class"] = ((this.Enabled) ? "On" : "Off") + Left.ID;
                Center.Attributes["class"] = ((this.Enabled) ? "On" : "Off") + Center.ID;
                Right.Attributes["class"] = ((this.Enabled) ? "On" : "Off") + Right.ID;
            }
        }

        protected void rptSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                this.Enabled = this.Enabled || (((SiteMapNode)e.Item.DataItem).Key.ToLower() == this.Request.Url.AbsolutePath.ToLower());
            }
        }

    //    protected void rptTopMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //    {
    //        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //        {
    //            SiteMapNode node = ((SiteMapNode)e.Item.DataItem);
    //            this.Enabled = (node.Key.ToLower() == this.Request.Url.AbsolutePath.ToLower());

    //            ((Repeater)e.Item.FindControl("rptSubMenu")).DataSource = (from SiteMapNode obj in node.ChildNodes select obj).ToList();
    //            ((Repeater)e.Item.FindControl("rptSubMenu")).DataBind();

    //            HtmlGenericControl Left = ((HtmlGenericControl)e.Item.FindControl("Left"));
    //            HtmlGenericControl Center = ((HtmlGenericControl)e.Item.FindControl("Center"));
    //            HtmlGenericControl Right = ((HtmlGenericControl)e.Item.FindControl("Right"));

    //            Left.Attributes["class"] = ((this.Enabled) ? "On" : "Off") + Left.ID;
    //            Center.Attributes["class"] = ((this.Enabled) ? "On" : "Off") + Center.ID;
    //            Right.Attributes["class"] = ((this.Enabled) ? "On" : "Off") + Right.ID;
    //        }
    //    }

    //    protected void rptSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //    {
    //        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //        {
    //            this.Enabled = this.Enabled || (((SiteMapNode)e.Item.DataItem).Key.ToLower() == this.Request.Url.AbsolutePath.ToLower());
    //        }
    //    }

    }
}