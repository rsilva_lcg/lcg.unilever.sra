﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainMenu.ascx.cs" Inherits="SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common.MainMenu" %>
<asp:SiteMapDataSource ID = "SM_FrontOffice" runat ="server" SiteMapProvider ="XmlSitemapProvider2"  />
<asp:SiteMapDataSource ID = "SM_Backoffice" runat = "server" SiteMapProvider="XmlSitemapProvider" />
<asp:SiteMapDataSource ID = "SM_Analisys" runat = "server" SiteMapProvider="XmlSitemapProvider3" />

<asp:Repeater ID="rptTopMenu" runat="server" onitemdatabound="rptTopMenu_ItemDataBound">
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
        <%--<div id="Menu" class="MenuTab" onmouseover='javascript:MenuOnMouseOver(<%# ((SiteMapNode)Container.DataItem).GetHashCode().ToString() %>, 0);' onmouseout='javascript:MenuOnMouseOut(<%# ((SiteMapNode)Container.DataItem).GetHashCode().ToString() %>, 0);' >
            <div id="Left" runat="server"></div>
            <div id="Center" runat="server"><div><asp:HyperLink ID="HyperLinkMenuTab" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>'></asp:HyperLink></div></div>
            <div id="Right" runat="server"></div>
            <div id='<%# "div" + ((SiteMapNode)Container.DataItem).GetHashCode().ToString() %>' onmouseover='javascript:MenuOnMouseOver(<%# ((SiteMapNode)Container.DataItem).GetHashCode().ToString() %>, 0);' style="display: none; position: absolute; left:0px; top:21px;">
                <asp:Repeater ID="rptSubMenu" runat="server" onitemdatabound="rptSubMenu_ItemDataBound">
                    <HeaderTemplate>
                        <table cellpadding="0" cellspacing="0" border="0" class="SubMenu">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr><td nowrap class='<%# "SubMenuTD" + ((((SiteMapNode)Container.DataItem).Key.ToLower() == this.Request.Url.AbsolutePath.ToLower())? " SubMenuTDSel" : "") %>' onclick='<%# "javascript:location.href=\"" + ((SiteMapNode)Container.DataItem).Url + "\"" %>'>&nbsp;<a class="SubMenuLink" href='<%# ((SiteMapNode)Container.DataItem).Url %>'><%# DataBinder.Eval(Container.DataItem, "Title") %></a>&nbsp;</td></tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>--%>
        <div id="Menu" class="MenuTab" onmouseover='javascript:MenuOnMouseOver(<%# ((SiteMapNode)Container.DataItem).GetHashCode().ToString() %>, 0);' onmouseout='javascript:MenuOnMouseOut(<%# ((SiteMapNode)Container.DataItem).GetHashCode().ToString() %>, 0);' >
            <div id="Left" runat="server"></div>
            <div id="Center" runat="server"><div><asp:HyperLink ID="HyperLinkMenuTab" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>'></asp:HyperLink></div></div>
            <div id="Right" runat="server"></div>
            <div id='<%# "div" + ((SiteMapNode)Container.DataItem).GetHashCode().ToString() %>' onmouseover='javascript:MenuOnMouseOver(<%# ((SiteMapNode)Container.DataItem).GetHashCode().ToString() %>, 0);' style="display: none; position: absolute; left:0px; top:21px;">
                <asp:Repeater ID="rptSubMenu" runat="server" onitemdatabound="rptSubMenu_ItemDataBound">
                    <HeaderTemplate>
                        <table cellpadding="0" cellspacing="0" border="0" class="SubMenu">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr><td nowrap class='<%# "SubMenuTD" + ((((SiteMapNode)Container.DataItem).Key.ToLower() == this.Request.Url.AbsolutePath.ToLower())? " SubMenuTDSel" : "") %>' onclick='<%# "javascript:location.href=\"" + ((SiteMapNode)Container.DataItem).Url + "\"" %>'>&nbsp;<a class="SubMenuLink" href='<%# ((SiteMapNode)Container.DataItem).Url %>'><%# DataBinder.Eval(Container.DataItem, "Title") %></a>&nbsp;</td></tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </ItemTemplate>
    <FooterTemplate>
    </FooterTemplate>
</asp:Repeater>
