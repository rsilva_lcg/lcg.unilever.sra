﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common
{
    public partial class ResponseMsgPanel : System.Web.UI.UserControl
    {
        protected bool Rendered = false;
        public bool Visible
        {
            get
            {
                return this.divMessage.Visible;
            }
            set
            {
                this.divMessage.Visible = value;
            }
        }

        public enum MessageType
        {
            Info,
            Success,
            Warning,
            Error
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void ShowMessage(string message, Exception e, MessageType mType)
        {
            this.C_lbl_Message.Text = message;
            if (e != null)
                this.ExceptionHiddenField.Value = "Exception: " + e.Message + ((e.InnerException != null) ? "; InnerException: " + e.InnerException.Message : "");
            this.divMessage.Attributes["class"] = this.divMessage.Attributes["class"].ToString().Split(' ')[0] + " " + this.GetLocalResourceObject(mType.ToString());
            this.divMessage.Visible = true;

            this.Rendered = true;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.divMessage.Visible = this.Rendered;
        }
    }
}