﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common
{
    public partial class ucUpdateProgress : System.Web.UI.UserControl
    {
        public string MyUpdateProgress
        {
            get
            {
                return this.UpdateProgress.AssociatedUpdatePanelID;
            }
            set
            {
                this.UpdateProgress.AssociatedUpdatePanelID = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}