﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResponseMsgPanel.ascx.cs" Inherits="SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common.ResponseMsgPanel" %>
<div id="ResponseMsgPanelWrapper" class="ResponseMsgPanelWrapper">
    <div id="divMessage" runat="server" class="ResponseMsgPanel">
        <asp:Label ID="C_lbl_Message" runat="server"></asp:Label><asp:HiddenField ID="ExceptionHiddenField" runat="server"/>
    </div>
</div>