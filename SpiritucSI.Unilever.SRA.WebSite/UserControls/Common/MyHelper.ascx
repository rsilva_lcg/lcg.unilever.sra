﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyHelper.ascx.cs" Inherits="SpiritucSI.Fnac.MasterCatalog.Website.UserControls.Common.MyHelper" %>
<div style="float:left; position:relative">
    <div class="Helper"><asp:Image ID="imgHelper" runat="server" ImageUrl="~/Images/help.png"/></div>
    <div id="HelperContent" class="HelperContent" >
        <asp:Label ID="lblContent" runat="server" ></asp:Label>
    </div>
</div>