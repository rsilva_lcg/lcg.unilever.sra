﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpiritucSI.Fnac.MasterCatalog.Website.UserControls.Common
{
    public partial class MyHelper : System.Web.UI.UserControl
    {
        public string Key { get; set; }
        public string Text { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblContent.Text = this.GetGlobalResourceObject("Helper", this.Key) != null ? this.GetGlobalResourceObject("Helper", this.Key).ToString() : this.Text;
            this.Visible = this.GetGlobalResourceObject("Helper", this.Key) != null || !string.IsNullOrEmpty(this.Text);
        }
    }
}