﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucLoginForm.ascx.cs" Inherits="SpiritucSI.Fnac.MasterCatalog.Website.UserControls.Login.ucLoginForm" %>
<fieldset id="login-border">
        <div id="login" >
            <div id="login-title"><asp:Label ID="C_Lbl_title" runat="server" meta:resourcekey="C_Lbl_title" CssClass="lblTitle" ></asp:Label></div>
            <div id="login-subtitle"><asp:Label ID="C_Lbl_subtitle" runat="server" meta:resourcekey="C_Lbl_subtitle" ></asp:Label></div>
            <div id="login-form">      
         <asp:Login runat="server" ID="login1" TitleText="">
 
        <LayoutTemplate>
        <div class="Row">
            <div id="label-position" >
            <asp:Label ID="C_lbl_UserName" runat="server" CssClass="FormFieldText" 
                    AssociatedControlID="UserName" meta:resourcekey="C_lbl_UserName"></asp:Label>
            </div>
            <div id="Textbox-position">
            <asp:TextBox ID="UserName" CssClass="TextBox" runat="server" 
                    meta:resourcekey="UserNameResource1"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                   ControlToValidate="UserName" ValidationGroup="Login1" 
                    meta:resourcekey="RequiredFieldValidator1Resource1">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="Row">
            <div id="label-position">
            <asp:Label ID="C_lbl_password" runat="server" CssClass="FormFieldText" 
                    AssociatedControlID="Password" meta:resourcekey="C_lbl_password"></asp:Label>
            </div>
            <div id="Textbox-position">
            <asp:TextBox ID="Password" CssClass="TextBox" runat="server" TextMode="Password" 
                    meta:resourcekey="PasswordResource1"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" 
                   ControlToValidate="Password" ValidationGroup="Login1" 
                    meta:resourcekey="PasswordRequiredResource1">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="Row" >
            <div id="remember-login">
            <asp:CheckBox ID="RememberMe" CssClass="FormFieldText" runat="server"  
                meta:resourcekey="RememberMeResource1" />  <asp:label  id="c_lbl_RememberMe" meta:resourcekey="c_lbl_RememberMe" runat="server" CssClass="FormFieldText" /> 
        </div>
        </div>
        <div class="Row">
            <div id="Failure-login">
            <asp:Literal ID="FailureText" runat="server" EnableViewState="False" ></asp:Literal>
            </div>
        </div>
         <div class="Row">
            <div id="Login-button">
                <asp:Button ID="LoginButton" CssClass="button1" runat="server" CommandName="Login" 
                 ValidationGroup="Login1" meta:resourcekey="LoginButtonResource1" />   
            </div>
        </div>
        </LayoutTemplate>
                </asp:Login>
            </div>
            <div class="Row">
            <div id="login-info"><asp:HyperLink id="C_hpl_Rec_Pass" runat="server" meta:resourcekey="C_hpl_Rec_Pass" NavigateUrl="~/RetrievePassword.aspx" /> | <a id="A1" runat="server" href='<%$ Resources: Global,Administrator %>'><asp:Label ID="C_Lbl_Cnct_Admin" runat="server" meta:resourcekey="C_Lbl_Cnct_Admin" /></a></div>
        </div></div>
    </fieldset>