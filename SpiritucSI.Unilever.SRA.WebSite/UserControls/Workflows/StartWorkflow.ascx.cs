﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Workflow.Runtime.Hosting;
using SpiritucSI.BPN.AII.WebSite.Common;


namespace Web_BPN_Analysis
{
    public partial class StartWorkflow : SuperControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //SqlWorkflowPersistenceService srv = (SqlWorkflowPersistenceService)this.WFRuntime.GetService<SqlWorkflowPersistenceService>();
                GridBind();
            }


        }

        private void GridBind()
        {
            SqlWorkflowPersistenceService srv = (SqlWorkflowPersistenceService)this.WFRuntime.GetService<SqlWorkflowPersistenceService>();
            this.gvProcess0.DataSource = srv.GetAllWorkflows();
            this.gvProcess0.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BLL bl = new BLL();
            List<PARTYSELECTED> parties = bl.GetPartieSelectedByProcess(bl.GetCurrentProcessId(), 0, 0).ToList();

            foreach (var p in parties)
            {
                Guid idWorflow = this.RunWorkflow();
                bl.UpdatePartyWorkflowIdById(p.idPartySelected, idWorflow);
            }

            this.GridBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            SqlWorkflowPersistenceService srv = (SqlWorkflowPersistenceService)this.WFRuntime.GetService<SqlWorkflowPersistenceService>();
            IEnumerable<SqlPersistenceWorkflowInstanceDescription> sql = srv.GetAllWorkflows().ToList();
            BLL bl = new BLL();
            foreach (SqlPersistenceWorkflowInstanceDescription s in sql)
            {
                try
                {
                    this.WFRuntime.GetWorkflow(s.WorkflowInstanceId).Terminate("Porque Sim");


                }
                catch (InvalidOperationException)
                {

                }
                catch (Exception ex)
                {

                }
                finally
                {
                    List<PARTYSELECTED> parties = bl.GetPartieSelectedByProcess(bl.GetCurrentProcessId(), 0, 0).ToList();
                    parties.ForEach(p => bl.UpdatePartyWorkflowIdById(p.idPartySelected, Guid.Empty));
                }
                this.GridBind();
            }

        }

        protected void Btt_Run_Click(object sender, EventArgs e)
        {
            BLL bl = new BLL();

            string idPartySelected = (string)(sender as Button).CommandArgument;
            int idPs =0;
            if(Int32.TryParse(idPartySelected,out idPs)){
            Guid idWorflow = this.RunWorkflow();
            bl.UpdatePartyWorkflowIdById(idPs, idWorflow);}
            
            //List<PARTYSELECTED> parties = bl.GetPartieSelectedByProcess(bl.GetCurrentProcessId(), 0, 0).ToList();

            //foreach (var p in parties)
            //{
            //    Guid idWorflow = this.RunWorkflow();
            //    bl.UpdatePartyWorkflowIdById(p.idPartySelected, idWorflow);
            //}

            this.GridBind();
        }
    }
}