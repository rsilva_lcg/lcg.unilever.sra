﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StartWorkflow.ascx.cs"
    Inherits="Web_BPN_Analysis.StartWorkflow" %>
<asp:GridView ID="gvProcess" runat="server" CellPadding="4" ForeColor="#333333" 
    GridLines="None">
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <EditRowStyle BackColor="#999999" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
    <asp:TemplateField>
    <ItemTemplate>
    <asp:Button ID = "Btt_Run" runat = "server" Text="Correr Processo" OnClick ="Btt_Run" CommandArgument ='<%#DataBinder.Eval(Container.DataItem, "idPartySelected")%>' />
    </ItemTemplate>
    </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:Button ID="Button1" runat="server" Text="Criar" OnClick="Button1_Click" />
<br />
<asp:GridView ID="gvProcess0" runat="server" CellPadding="4" 
    ForeColor="#333333" GridLines="None">
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <EditRowStyle BackColor="#999999" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

</asp:GridView>
<asp:Button ID="Button2" runat="server" Text="Apagar" OnClick="Button2_Click" />
