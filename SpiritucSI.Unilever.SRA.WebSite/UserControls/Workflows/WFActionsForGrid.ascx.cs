﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.Unilever.SRA.WF.WebSite.Common;
using SpiritucSI.Unilever.SRA.WF;
using System.Workflow.Runtime;
using System.Workflow.Runtime.Hosting;
using System.Workflow.Activities;
using System.Collections.ObjectModel;
using System.Reflection;

namespace SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Workflows
{
    public partial class WFActionsForGrid : SuperControl
    {
        #region PROPERTIES
        public string uid { get; set; }
        public List<WFAction> _wfActions
        {
            get
            {
                if (Session["WorkflowActionsGridList"] == null)
                {
                    Session["WorkflowActionsGridList"] = new List<WFAction>();
                }
                    
                return (List<WFAction>)(Session["WorkflowActionsGridList"]);
            }
            set
            {
                Session["WorkflowActionsGridList"] = value;
            }
        } 
        #endregion

        #region PAGE_EVENTS
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (!this.IsPostBack)
            //{
                this.RenderWFActions();
            //}
        } 
        #endregion

        #region METHODS
        private void RenderWFActions()
        {
            Guid _uid = new Guid(this.uid);

            WorkflowInstance instance = this.WFRuntime.GetWorkflow(_uid);
            StateMachineWorkflowInstance smwi = new StateMachineWorkflowInstance(this.WFRuntime, _uid);
            ReadOnlyCollection<WorkflowQueueInfo> queues = instance.GetWorkflowQueueData();

            Assembly assembly = instance.GetWorkflowDefinition().GetType().Assembly;
            Type type = assembly.GetType("SpiritucSI.Unilever.SRA.WF.WFEvents");

            ExternalDataExchangeService dataService = new ExternalDataExchangeService();
            ExternalDataExchangeService s = (ExternalDataExchangeService)this.WFRuntime.GetService(typeof(ExternalDataExchangeService));
            if (s != null && type != null && s.GetService(type) == null)
            {
                s.AddService(assembly.CreateInstance(type.FullName));
            }
            if (type == null)
                type = typeof(WFEvents);

            List<WFAction> wfActions = new List<WFAction>();
            foreach (var queue in queues)
            {
                EventQueueName queueName = queue.QueueName as EventQueueName;

                if (queueName != null)
                {
                    var handler = (from EventInfo ei in type.GetEvents()
                                   where ei.Name == queueName.MethodName
                                   select ei).FirstOrDefault();

                    var objEventActivity = (from EventDrivenActivity obj in
                                                (from obj2 in smwi.CurrentState.EnabledActivities where obj2 is EventDrivenActivity select obj2).ToList()
                                            where obj.EventActivity is HandleExternalEventActivity && ((HandleExternalEventActivity)obj.EventActivity).EventName == handler.Name
                                            select obj).FirstOrDefault();

                    bool isInRole = true;
                    if (((HandleExternalEventActivity)objEventActivity.EventActivity).Roles != null)
                        isInRole = ((HandleExternalEventActivity)objEventActivity.EventActivity).Roles.IncludesIdentity(this.Context.User.Identity.Name);

                    if (objEventActivity != null && objEventActivity.EventActivity != null
                        && objEventActivity.EventActivity is HandleExternalEventActivity
                        && isInRole)
                    {
                        ExternalDataEventArgs args = new ExternalDataEventArgs(_uid);
                        args.WaitForIdle = true;

                        var wfservice = this.WFRuntime.GetService(type);
                        MulticastDelegate eventDelegate =
                           (MulticastDelegate)type.GetField(handler.Name, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)
                           .GetValue(wfservice);
                        Delegate[] delegates = eventDelegate.GetInvocationList();

                        foreach (Delegate dlg in delegates)
                        {
                            WFAction wfAction = new WFAction();
                            wfAction.Deleg = dlg;
                            wfAction.Args = args;
                            wfAction.Servs = new WFEvents();
                            wfAction.Id = Guid.NewGuid();
                            wfAction.Text = objEventActivity.Description;
                            wfAction.Visibility = objEventActivity.Activities[0].Description.ToLower() != "nobulk";

                            wfActions.Add(wfAction);
                        }
                    }
                }
            }

            this.rpChk.DataSource = wfActions;
            this.rpChk.DataBind();

            this._wfActions.AddRange(wfActions);
        }

        public bool ExecuteAction()
        {
            foreach (RepeaterItem ri in rpChk.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chk");

                if (chk.Checked)
                {
                    WFAction wfAction = (from WFAction obj in this._wfActions
                                         where obj.Id == new Guid(chk.Attributes["ActionID"])
                                         select obj).FirstOrDefault();

                    if (wfAction != null)
                    {
                        try
                        {
                            wfAction.Args.Identity = this.Context.User.Identity.Name;
                            wfAction.Deleg.Method.Invoke(wfAction.Deleg.Target, new object[] { wfAction.Servs, wfAction.Args });

                            this.RunWorkflow(wfAction.Args.InstanceId);

                            return true;
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                    }
                }
            }

            return false;
        }
        #endregion
    }
}