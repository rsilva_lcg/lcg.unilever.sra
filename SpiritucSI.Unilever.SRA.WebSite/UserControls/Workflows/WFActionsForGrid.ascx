﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WFActionsForGrid.ascx.cs" Inherits="SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Workflows.WFActionsForGrid" %>
<asp:Repeater ID="rpChk" runat="server">
    <ItemTemplate>
        <div class="Row mk_chklist">
            <asp:CheckBox ID="chk" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Text") %>' Visible='<%# DataBinder.Eval(Container.DataItem,"Visibility") %>' ActionID='<%# DataBinder.Eval(Container.DataItem,"Id") %>' onclick="var checked =$(this).attr('checked');$(this).parent().parent().parent().find('input').attr('checked',false);$(this).attr('checked',checked);"/>
        </div>
    </ItemTemplate>
</asp:Repeater>