﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Workflow.Runtime;
using System.Workflow.Runtime.Hosting;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Workflow.Activities;
using SpiritucSI.Unilever.SRA.WF;
using SpiritucSI.Unilever.SRA.WF.WebSite.Common;
using SpiritucSI.Unilever.SRA.WF.WebSite.Masters;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Workflow
{
    public partial class WorkflowAcctions : SuperControl
    {
        public string uid { get; set; }
        public int stateid { get; set; }
        public List<WFAction> wfActions
        {
            get
            {
                if (Session["WorkflowActionsList"] != null)
                    return (List<WFAction>)(Session["WorkflowActionsList"]);
                else
                    return new List<WFAction>();
            }
            set
            {
                Session["WorkflowActionsList"] = value;
            }
        }
        private bool showInstructions;

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
               
                this.showInstructions = false; // Call BLL Method
                this.instructionDiv.Visible = this.showInstructions;
                this.instructionDiv.InnerText = this.GetLocalResourceObject("Instruction" + this.stateid.ToString()).ToString();
                this.RenderWFActions();
            }
        }

        private void RenderWFActions()
        {
            Guid _uid = new Guid(this.uid);

            WorkflowInstance instance = this.WFRuntime.GetWorkflow(_uid);
            StateMachineWorkflowInstance smwi = new StateMachineWorkflowInstance(this.WFRuntime, _uid);
            ReadOnlyCollection<WorkflowQueueInfo> queues = instance.GetWorkflowQueueData();
            this.RepeaterWFActions.Visible = false;

            Assembly assembly = instance.GetWorkflowDefinition().GetType().Assembly;
            Type type = assembly.GetType("SpiritucSI.Unilever.SRA.WF.WFEvents");

            ExternalDataExchangeService dataService = new ExternalDataExchangeService();
            ExternalDataExchangeService s = (ExternalDataExchangeService)this.WFRuntime.GetService(typeof(ExternalDataExchangeService));
            if (s != null && type != null && s.GetService(type) == null)
            {
                s.AddService(assembly.CreateInstance(type.FullName));
            }
            if (type == null)
                type = typeof(WFEvents);

            wfActions = new List<WFAction>();
            foreach (var queue in queues)
            {
                EventQueueName queueName = queue.QueueName as EventQueueName;

                if (queueName != null)
                {
                    var handler = (from EventInfo ei in type.GetEvents()
                                   where ei.Name == queueName.MethodName
                                   select ei).FirstOrDefault();

                    var objEventActivity = (from EventDrivenActivity obj in
                                                (from obj2 in smwi.CurrentState.EnabledActivities where obj2 is EventDrivenActivity select obj2).ToList()
                                            where obj.EventActivity is HandleExternalEventActivity && ((HandleExternalEventActivity)obj.EventActivity).EventName == handler.Name
                                            select obj).FirstOrDefault();

                    bool isInRole = true;
                    if (((HandleExternalEventActivity)objEventActivity.EventActivity).Roles != null)
                        isInRole = ((HandleExternalEventActivity)objEventActivity.EventActivity).Roles.IncludesIdentity(this.Context.User.Identity.Name);

                    if (objEventActivity != null && objEventActivity.EventActivity != null
                        && objEventActivity.EventActivity is HandleExternalEventActivity
                        && isInRole)
                    {
                        ExternalDataEventArgs args = new ExternalDataEventArgs(_uid);
                        args.WaitForIdle = true;

                        var wfservice = this.WFRuntime.GetService(type);
                        MulticastDelegate eventDelegate =
                           (MulticastDelegate)type.GetField(handler.Name, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)
                           .GetValue(wfservice);
                        Delegate[] delegates = eventDelegate.GetInvocationList();

                        foreach (Delegate dlg in delegates)
                        {
                            WFAction wfAction = new WFAction();
                            wfAction.Deleg = dlg;
                            wfAction.Args = args;
                            wfAction.Servs = new WFEvents();
                            wfAction.Id = Guid.NewGuid();
                            wfAction.Text = objEventActivity.Description;
                            
                            wfActions.Add(wfAction);
                            this.RepeaterWFActions.Visible = true;
                        }
                    }
                }
            }

            this.RepeaterWFActions.DataSource = wfActions;
            this.RepeaterWFActions.DataBind();


        }

        #region CONTROL EVENTS
        protected void C_Rpt_WFActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Button btn = (Button)e.Item.FindControl("ButtonWFAction");
                
                btn.Visible = !this.showInstructions;
                btn.CommandArgument = ((SpiritucSI.Unilever.SRA.WF.WFAction)e.Item.DataItem).Id.ToString();
                btn.Text = ((SpiritucSI.Unilever.SRA.WF.WFAction)e.Item.DataItem).Text;
            }
        }
        protected void C_Rpt_WFActions_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            WFAction wfAction = (from WFAction obj in this.wfActions
                                 where obj.Id == new Guid(e.CommandArgument.ToString())
                                 select obj).FirstOrDefault();
            if (wfAction != null)
            {
                try
                {
                    wfAction.Args.Identity = this.Context.User.Identity.Name;
                    wfAction.Deleg.Method.Invoke(wfAction.Deleg.Target, new object[] { wfAction.Servs, wfAction.Args });

                    this.RunWorkflow(wfAction.Args.InstanceId);

                    this.redirect.Value = "true";
                    ((FrontOffice)this.Page.Master).ShowMessage("O processo foi actualizado com sucesso", null, SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Success);

                }
                catch (WFProceedConditions ex)
                {
                    ((FrontOffice)this.Page.Master).ShowMessage("Falta informação para que o processo possa mudar de estado", ex, SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Warning);
                }
                catch (Exception ex)
                {
                    throw;
                    //if (ExceptionPolicy.HandleException(ex, "LogOnly"))
                    //{ throw; }

                    //((Master)this.Page.Master).ShowMessage(this.GetGlobalResourceObject("Messages", "ErrorMessage").ToString(), ex, MessageType.ErrorBox);
                }
                finally
                {
                    //this.Load();
                }
            }
        }
        #endregion

        #region WORKFLOW EVENTS

        #endregion

        public string GetRedirect()
        {
            return this.redirect.ClientID;
        }
    }
}