﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorkflowAcctions.ascx.cs"
    Inherits="SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Workflow.WorkflowAcctions" %>
<div style="float: left; width: 990px; padding-top: 10px;">
    <fieldset style="border:solid 1px #dddddd; width:100%">
        <legend>Acções</legend>
        <div id="instructionDiv" style="width:950x;padding:10px; border:solid 1px #dddddd;" runat="server">My instructions</div>
    <asp:Repeater ID="RepeaterWFActions" runat="server" OnItemCommand="C_Rpt_WFActions_ItemCommand"
        OnItemDataBound="C_Rpt_WFActions_ItemDataBound">
        <ItemTemplate>
            <asp:Button ID="ButtonWFAction" runat="server" CommandName="WFActionClick" OnClientClick="return window.confirm('Tem a certeza que deseja alterar o estado do processo?')" />
            </ItemTemplate>
    </asp:Repeater>
     <asp:HiddenField ID="redirect" runat="server" Value="false" />
    </fieldset>
</div>
