﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Workflow.Runtime.Hosting;
using SpiritucSI.Unilever.SRA.WF.WebSite.Common;

namespace Web_BPN_Analysis
{
    public partial class newWorkflow : SuperControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {



                // Target="_blank"



                SqlWorkflowPersistenceService srv = (SqlWorkflowPersistenceService)this.WFRuntime.GetService<SqlWorkflowPersistenceService>();
                this.GridView1.DataSource = srv.GetAllWorkflows();
                this.GridView1.DataBind();

                this.GridView1.AllowSorting = true;
                this.GridView1.Sort("modified", SortDirection.Ascending);
            }
            else
            {
//                Response.Redirect(string.Format("viewworkflow.aspx?id={0}", ID));
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Guid id = this.RunWorkflow();
            this.Page_Load(this, e);

            SqlWorkflowPersistenceService srv = (SqlWorkflowPersistenceService)this.WFRuntime.GetService<SqlWorkflowPersistenceService>();
            this.GridView1.DataSource = srv.GetAllWorkflows();
            this.GridView1.DataBind();
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
        }
    }
}