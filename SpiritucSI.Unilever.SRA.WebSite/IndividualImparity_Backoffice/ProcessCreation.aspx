﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/BackOffice.Master" AutoEventWireup="true" CodeBehind="ProcessCreation.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.ProcessCreation" Theme="MainTheme" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%--<%@ Register Src="/UserControls/Common/ucTitle.ascx" TagName="ucTitle" TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server"> 

    
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top">
        <td align="left"> <label style = " font-size:12px; font-weight:bold;">Novo Processo</label>
            <hr /><br />
        </td>
    </tr>
    <tr valign="top">
        <td align="left" style = "text-align:center;">
        <asp:GridView ID= "GV_Processes" runat="server" AllowPaging = "true" AllowSorting ="true" AutoGenerateColumns = "false" Width= "300px">
            <Columns>
            <asp:TemplateField HeaderText = "Data de Balanço"><ItemTemplate>
            <asp:LinkButton Id = "LB_Name" Text = <%#DataBinder.Eval(Container.DataItem, "balanceSheetDate")%>  runat = "server" OnClick = "LB_Name_Click" CommandArgument = <%#DataBinder.Eval(Container.DataItem, "p.idProcess")%> ></asp:LinkButton>
            
            </ItemTemplate></asp:TemplateField>
            <asp:BoundField HeaderText = "Estado" DataField = "State"/>
            </Columns>
            </asp:GridView>
    </td>
    </tr>
    <tr>
    <td><br />
    <span>Novo Processo</span> <asp:HyperLink ID="HL_NewProcess" runat = "server"   Text = "AAAA"></asp:HyperLink>
    <asp:Button ID = "Create_New_ProcessBtn" runat="server" Text="Criar" 
            onclick="Create_New_ProcessBtn_Click" Width ="50px" /> 
    </td></tr>
    </table>
</asp:Content>
