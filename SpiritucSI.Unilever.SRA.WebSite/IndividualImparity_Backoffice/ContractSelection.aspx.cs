﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;

namespace SpiritucSI.BPN.AII.WebSite
{
    public partial class ContractSelection : System.Web.UI.Page
    {
        #region PROPS
        private int v_idPartySelected;

        public int idPartySelected
        {

            get
            {
                if (v_idPartySelected == 0)
                {
                    int id = 0;
                    if (Cache["idPartyBOffice"] != null)
                    {
                        Int32.TryParse(Cache["idPartyBOffice"].ToString(), out id);
                        if (id != 0)
                            v_idPartySelected = id;
                    }
                    else
                        this.PageRedirect();
                }

                return v_idPartySelected;
            }

            set
            {
                v_idPartySelected = value;

            }
        }


        private void PageRedirect()
        {
            this.Response.Redirect("~/Dashboard.aspx");
        }

        #endregion


        protected void Page_PreRender(object sender, EventArgs e)
        {

            GV_Contracts.DataSource = new BLL().GetContractsByParty(idPartySelected,20,0);
            GV_Contracts.DataBind();

        }

        protected void Btt_Save_Click(object sender, EventArgs e)
        {

        }
    }
}
