﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using System.Configuration;

namespace SpiritucSI.BPN.AII.WebSite
{
    public partial class ClientSelection : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            BLL ds = new BLL();

            int idCompany = ds.GetCompanyId(ConfigurationManager.AppSettings["CompanyName"].ToString());

            GV_Clients.DataSource = ds.GetPartiesByProcess(ds.GetCurrentProcessId(idCompany), 20, 0);
            GV_Clients.DataBind();

        }

        protected void LB_Name_Click(object sender, EventArgs e)
        {

            LinkButton lb = (LinkButton)sender;

            Cache["idPartyBOffice"] = lb.CommandArgument;
            this.Response.Redirect("~/IndividualImparity_Backoffice/ContractSelection.aspx");

        }

    }
}
