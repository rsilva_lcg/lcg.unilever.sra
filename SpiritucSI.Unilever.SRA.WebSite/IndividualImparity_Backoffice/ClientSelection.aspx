﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/BackOffice.Master" AutoEventWireup="true" CodeBehind="ClientSelection.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.ClientSelection" Theme="MainTheme" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%--<%@ Register Src="/UserControls/Common/ucTitle.ascx" TagName="ucTitle" TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">    
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top">
        <td align="left"> <label style = " font-size:12px; font-weight:bold;">Selecção de Clientes</label>
            <hr />
        </td>
    </tr>
    <tr valign="top">
        <td  style = "text-align:center;" >
         <asp:GridView ID= "GV_Clients" runat="server" AllowPaging = "True" 
                AllowSorting ="True" AutoGenerateColumns = "False" Width="100%" >
            <Columns>
            <asp:TemplateField ItemStyle-HorizontalAlign ="Left"><ItemTemplate>
            <asp:LinkButton Id = "LB_Name" Text = <%#DataBinder.Eval(Container.DataItem, "partyName")%>  runat = "server" OnClick = "LB_Name_Click" CommandArgument = <%#DataBinder.Eval(Container.DataItem, "p.idPartySelected")%> ></asp:LinkButton>
            </ItemTemplate></asp:TemplateField>
            <asp:CheckBoxField HeaderText ="Seleccionado" DataField="isSelected" />
            </Columns>
        </asp:GridView>
    </td>
    </tr>
    <tr><td><br />
    <asp:Button ID = "Btt_Save" runat = "server" Text = "Guardar" />
    </td></tr>
    </table>
</asp:Content>
