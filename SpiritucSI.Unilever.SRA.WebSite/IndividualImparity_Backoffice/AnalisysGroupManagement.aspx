﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/BackOffice.Master" AutoEventWireup="true" CodeBehind="AnalisysGroupManagement.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.AnalisysGroupManagement" Theme="MainTheme" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%--<%@ Register Src="/UserControls/Common/ucTitle.ascx" TagName="ucTitle" TagPrefix="uc1" %>--%><asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top">
        <td align="left"> <label style = " font-size:12px; font-weight:bold;">Selecção de grupos de análise</label>
            <hr />
        </td>
    </tr>
    <tr valign="top">
        <td align="left">
        <asp:GridView ID= "GV_AnalisysGroups" runat="server" AllowPaging = "true" AllowSorting ="true" AutoGenerateColumns = "false">
            <Columns>
            <asp:BoundField HeaderText = "Nome" DataField ="Name" />
            <asp:CheckBoxField HeaderText = "Seleccionar" />
            </Columns>
        </asp:GridView>
    </td>
    </tr>
    <tr><td><br /><br />
    <asp:Button ID = "Btt_Save" runat = "server" Text = "Guardar" />
    </td></tr>
    </table>
</asp:Content>
