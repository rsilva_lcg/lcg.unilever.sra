﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/BackOffice.Master" AutoEventWireup="true" CodeBehind="ContractSelection.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.ContractSelection" Theme="MainTheme" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%--<%@ Register Src="/UserControls/Common/ucTitle.ascx" TagName="ucTitle" TagPrefix="uc1" %>--%><asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server"> 
    
    
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top">
        <td align="left"><label style = " font-size:12px; font-weight:bold;">Selecção de Contractos</label>
            <hr />
        </td>
    </tr>
    <tr valign="top">
        <td align="left" style=" text-align:center; ">
        <asp:GridView ID= "GV_Contracts" runat="server" AllowPaging = "true" AllowSorting ="true" AutoGenerateColumns = "false" Width = "100%">
            <Columns>
            <asp:BoundField HeaderText = "Contracto" DataField ="contractRef" />
            <asp:BoundField HeaderText = "Valor" DataField = "ContractValue" />
            <asp:BoundField HeaderText = "Classe" DataField = "productClass"/>
            <asp:CheckBoxField HeaderText = "Seleccionado" DataField = "isSelected" ReadOnly="false"/>
            </Columns>
        </asp:GridView>
    </td>
    </tr>
    <tr>
    <td><br />
    <asp:Button ID = "Btt_Save" runat="server" Text="Guardar" onclick="Btt_Save_Click"/> 
    </td></tr>
    </table>
</asp:Content>

