﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;



namespace SpiritucSI.BPN.AII.WebSite
{
    public partial class ProcessCreation : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            
            GV_Processes.DataSource = new BLL().GetAllProcess();
            GV_Processes.DataBind();


        }

        protected void Create_New_ProcessBtn_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("./ClientSelection.aspx");
        }


        protected void LB_Name_Click(object sender, EventArgs e)
        {

            LinkButton lb = (LinkButton)sender;

            this.Response.Redirect("~/IndividualImparity_Backoffice/ClientSelection.aspx");

        }
    }
}
