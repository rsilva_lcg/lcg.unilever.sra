﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/BackOffice.Master" AutoEventWireup="true" CodeBehind="PreparationReview.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.PreparationReview" Theme="MainTheme" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<asp:Content ID="Cnt_Head" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Cnt_Main" ContentPlaceHolderID="MainContentPlaceHolder" runat="server"> 
    
    
    <label style = " font-size:12px; font-weight:bold;">Processo:AAAA 2010/05</label><br /><br />
    
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top"><td>
    <label style = " font-size:12px; font-weight:bold;">Clientes</label></td></tr>
    <tr valign="top">
        <td align="left" style = "text-align:center;">
        <asp:GridView ID= "GV_Clients" runat="server" AllowPaging = "true" AllowSorting ="true" AutoGenerateColumns = "false" Width = "600px">
            <Columns>
            <asp:BoundField HeaderText = "Nome" DataField ="Name" />
            <asp:BoundField HeaderText = "Entidade" DataField = "EntityName" />
            <asp:BoundField HeaderText = "Gestor" DataField = "Manager"/>
            <asp:CheckBoxField HeaderText = "Seleccionado" DataField = "isSelected" />
            </Columns>
        </asp:GridView><br /><br />
    </td>
    </tr>
    
    <tr valign="top"><td><label style = " font-size:12px; font-weight:bold;">Contractos</label></td></tr>
    <tr valign="top">
    <td align="left" style = "text-align: center;">
            <asp:GridView ID= "GV_Contracts" runat="server" AllowPaging = "true" AllowSorting ="true" AutoGenerateColumns = "false" Width = "600px">
            <Columns>
            <asp:BoundField HeaderText = "Contracto" DataField ="ContractREF" />
            <asp:BoundField HeaderText = "Valor" DataField = "ContractValue" />
            <asp:BoundField HeaderText = "Garantia Real/Pessoal" DataField = "garantyType"/>
            <asp:BoundField HeaderText = "Descrição" DataField = "description" />
            <asp:CheckBoxField HeaderText ="Seleccionado" DataField = "isSelected" />
            </Columns>
        </asp:GridView> <br /><br />
    </td></tr>
    
    
    <tr valign="top"><td><label style = " font-size:12px; font-weight:bold;">Questionarios</label></td></tr>
    <tr valign="top"><td align="left"></td> 
    </tr>
    <tr><td>
    <asp:GridView ID = "GV_Questionnaires" runat = "server" AllowPaging = "true" AllowSorting = "true" AutoGenerateColumns = "false" Width = "600px">
    <Columns>
    <asp:TemplateField HeaderText = "Nº">
        <ItemTemplate> <%#Container.DataItemIndex + 1 %></ItemTemplate>
    </asp:TemplateField>
    <asp:BoundField HeaderText = "Questão" DataField = "Question" />
    </Columns>
    </asp:GridView>&nbsp;<br />
    </td></tr>
    <tr><td>
    <asp:Button ID = "Btt_Confirm" runat="server" Text="Confirmar" 
            onclick="Btt_Confirm_Click"  /> 
    </td></tr>
    </table>
    
    </asp:Content>
    
