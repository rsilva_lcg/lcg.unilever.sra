﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/BackOffice.Master"  AutoEventWireup="true" CodeBehind="QuestionnaireManagement.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.QuestionnaireManagement" Theme="MainTheme" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%--<%@ Register Src="/UserControls/Common/ucTitle.ascx" TagName="ucTitle" TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server"> 
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top">
        <td align="left"><label style = " font-size:12px; font-weight:bold;">Gestão de Questionários</label>
            <hr />
        </td>
    </tr>
    <tr valign="top">
        <td align="left">
        <asp:GridView ID= "GV_Questionnaires" runat="server" AllowPaging = "True" 
                AllowSorting ="True" AutoGenerateColumns = "False" Width="100%">
            <Columns>
            <asp:TemplateField HeaderText = "Nº">
            <ItemTemplate> <%#Container.DataItemIndex + 1 %></ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText = "Questão" DataField = "question" />
                <asp:CommandField ButtonType="Button" ShowEditButton="True" />
            </Columns>
        </asp:GridView>
    </td>
    </tr>
    <tr><td><br />
    <asp:Button ID = "Btt_Save" runat = "server" Text = "Guardar" />
    </td></tr>
    </table>
    
</asp:Content>