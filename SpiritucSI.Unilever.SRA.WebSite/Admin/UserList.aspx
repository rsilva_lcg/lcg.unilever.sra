﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="SpiritucSI.Fnac.MasterCatalog.Website.Admin.UserList" Theme="MainTheme" %>
<%@ Register  Src="~/UserControls/Common/ucTitle.ascx" TagName="Title1" TagPrefix="uc1" %>
<%@ Register src="~/UserControls/User/ucUserList.ascx" tagname="ucUserList" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <uc1:title1 ID="Title1" runat="server" Title='<%$Resources: Title %>' />
    <uc1:ucUserList runat="server" ID="ucUserList1"></uc1:ucUserList>
</asp:Content>
