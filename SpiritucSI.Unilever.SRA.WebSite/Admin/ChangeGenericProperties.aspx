﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="true"
    Theme="MainTheme" CodeBehind="ChangeGenericProperties.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.ChangeGenericProperties" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div style="float: left; width: 100%; padding-top: 100px; height: 200px">
        <div style="float: left; width: 50%">
            <asp:Label ID="lblPartyId" runat="server" Text="Party Reference :"></asp:Label>
            <asp:TextBox ID="txtPartyId" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtPartyId">* Obrigatório</asp:RequiredFieldValidator>
        </div>
        <div style="float: left; width: 50%">
            <asp:DropDownList ID="ddlGroups" runat="server" Width="250px" AppendDataBoundItems="true"
                DataTextField="Code" DataValueField="idGroup">
                <asp:ListItem Text="-- Seleccionar Grupo --" Value="0"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ID="valGroup" ControlToValidate="ddlGroups"
                InitialValue="0">* Obrigatório</asp:RequiredFieldValidator>
        </div>
        <div style="float: left; width: 100%">
            <asp:Button ID="btnSubmit" runat="server" CausesValidation="true" Text="Submeter"
                OnClick="btnSubmit_Click" />
        </div>
        <br />
        <div style="float: left; width: 100%; padding-top:20px">
            <div style="float: left; width: 33%">
                <asp:Label ID="Label1" runat="server" Text="Username :"></asp:Label>
                <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    runat="server" ID="RequiredFieldValidator2" ValidationGroup="User" ControlToValidate="txtUsername">* Obrigatório</asp:RequiredFieldValidator>
            </div>
             <div style="float: left; width: 33%">
                <asp:Label ID="Label2" runat="server" Text="Name :"></asp:Label>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    runat="server" ID="RequiredFieldValidator4" ValidationGroup="User" ControlToValidate="txtName">* Obrigatório</asp:RequiredFieldValidator>
            </div>
            <div style="float: left; width: 33%">
                <div style="float: left; width: 50%">
                    <asp:DropDownList ID="ddlRole" runat="server" Width="250px" AppendDataBoundItems="true"
                       >
                        <asp:ListItem Text="-- Seleccionar Role --" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="ddlRole"
                InitialValue="0" ValidationGroup="User">* Obrigatório</asp:RequiredFieldValidator>
                </div>
                <div style="float: left; width: 50%">
                </div>
            </div>
             <div style="float: left; width: 100%">
            <asp:Button ID="btnCreateUser" runat="server" CausesValidation="true" ValidationGroup="User" Text="Criar" onclick="btnCreateUser_Click"
                />
        </div>
            </div>
            </div>
</asp:Content>
