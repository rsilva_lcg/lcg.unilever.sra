using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.WebSite.Common;


namespace SpiritucSI.Fnac.MasterCatalog.Website
{
    public partial class UserDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), "redirectScript", "if (document.getElementById('" + this.ucUserDetail1.GetRedirect() + "').value == 'true') { " + Utils.RedirectScriptUpdatePanel("UserList.aspx", 1500) + " }", true);
        }
    }
}