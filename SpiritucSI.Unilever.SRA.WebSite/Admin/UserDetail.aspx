<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="True" Theme="MainTheme" CodeBehind="UserDetail.aspx.cs" Inherits="SpiritucSI.Fnac.MasterCatalog.Website.UserDetail" %>
<%@ Register Src="~/UserControls/Common/ucTitle.ascx" TagName="Title" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/User/ucUserDetail.ascx" TagName="ucUserDetail" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <uc1:Title ID="Title1" runat="server" Title='<%$Resources: Title %>' />
    <uc1:ucUserDetail ID="ucUserDetail1" runat="server" />
</asp:Content>