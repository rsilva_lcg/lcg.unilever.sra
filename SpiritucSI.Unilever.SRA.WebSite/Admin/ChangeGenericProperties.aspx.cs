﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business;
using SpiritucSI.BPN.AII.WebSite.Masters;
using System.Web.Security;


namespace SpiritucSI.BPN.AII.WebSite
{
    public partial class ChangeGenericProperties : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                this.LoadGroups();
                this.LoadRoles();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                DB_AII_LINQDataContext db = new DB_AII_LINQDataContext();

                var query = from PARTYSELECTED ps in db.PARTYSELECTEDs
                            where ps.PARTY.PartyReference == this.txtPartyId.Text
                            select ps;

                if (query.Count() > 0)
                {
                    foreach (PARTYSELECTED ps in query.ToList())
                    {
                        ps.idGroup = Convert.ToInt16(this.ddlGroups.SelectedValue);
                    }
                }

                db.SubmitChanges();

                ((FrontOffice)this.Page.Master).ShowMessage("Alteração realizada com sucesso", null, SpiritucSI.BPN.AII.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Success);
            
            }
            catch (Exception ex)
            {
                ((FrontOffice)this.Page.Master).ShowMessage("Erro na alteração do Grupo", ex, SpiritucSI.BPN.AII.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Error);
            
            }
        }

        private void LoadGroups()
        {
             DB_AII_LINQDataContext db = new DB_AII_LINQDataContext();

            var query = from ANALISYSGROUP a in db.ANALISYSGROUPs
                        orderby a.code
                        select a;

            this.ddlGroups.DataSource = query.ToList();
            this.ddlGroups.DataBind();

        }

        private void LoadRoles()
        {
            this.ddlRole.DataSource = Roles.GetAllRoles().OrderBy(x=>x);
            this.ddlRole.DataBind();

        }

        protected void btnCreateUser_Click(object sender, EventArgs e)
        {
            try
            {
                Roles.AddUserToRole(this.txtUsername.Text, this.ddlRole.SelectedValue);
                DB_AII_LINQDataContext db = new DB_AII_LINQDataContext();

                USEREXTENDED user = new USEREXTENDED();
                user.username = this.txtUsername.Text;
                user.name = this.txtName.Text;

                db.USEREXTENDEDs.InsertOnSubmit(user);

                db.SubmitChanges();
                
                ((FrontOffice)this.Page.Master).ShowMessage("User criado com sucesso", null, SpiritucSI.BPN.AII.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Success);
            
            }
            catch (Exception ex)
            {
                ((FrontOffice)this.Page.Master).ShowMessage("Erro na criação de user", ex, SpiritucSI.BPN.AII.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Error);
            
            }

        }
    }
}
