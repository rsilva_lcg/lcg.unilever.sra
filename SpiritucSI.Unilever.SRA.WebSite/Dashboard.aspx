﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.Dashboard" Theme="MainTheme" %>
<%@ Register Src="~/IndividualImparity/userControls/ucDashboard.ascx" TagName="ucDashboard" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
     <uc1:ucDashboard id="ucDashboard" runat="server"></uc1:ucDashboard>
     <script type="text/javascript">
        $("div [id*='ob_grid1TrialDiv']").css('display', 'none');
</script>
</asp:Content>
