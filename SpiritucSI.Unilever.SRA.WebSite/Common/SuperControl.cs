﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using SpiritucSI.Unilever.SRA.WF.WebSite.Masters;
using SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SpiritucSI.Unilever.SRA.WF;
using System.Workflow.Runtime;
using System.Workflow.Runtime.Hosting;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;


namespace SpiritucSI.Unilever.SRA.WF.WebSite.Common 
{
    public class SuperControl : System.Web.UI.UserControl
    {
       //public void ShowMessage(string message, Exception ex, ResponseMsgPanel.MessageType msgType)
        //{
        //    ((Main)this.Page.Master).ShowMessage(message, ex, msgType);
            
        //}
       public string BaseUrl
        {
            get
            {
                return this.Request.Url.Scheme + "://" + this.Request.Url.Host + ((ConfigurationManager.AppSettings["BaseURL"] != null) ? ConfigurationManager.AppSettings["BaseURL"].ToString() : "");
            }
        }
       //public User CurrentUser
       // {
       //     get
       //     {
       //         if (this.Context.Session["CurrentUser"] != null)    
       //             return (User)this.Context.Session["CurrentUser"];
       //         else
       //             this.Response.Redirect("~/Dashboard.aspx"); return null;
       //     }

       //     set
       //     {
       //         this.Context.Session["CurrentUser"] = value;
       //     }
       // }
       public static string RedirectScript(string page, int timer)
        {

            return "<script type=\"text/javascript\">\n" +
                       "window.onload = \n" +
                        "function() {\n" +
                            "setTimeout('window.location = \"" + page + "\"', " + timer + ");\n" +
                        "};\n" +
                       "</script>";
        }
       //public int CurrentVersion(VersionType versionType)
       // {
       //     switch(versionType)
       //     {
       //         case VersionType.Development:
       //             return this.CurrentUser.DevelopmentVersion;
       //             break;
       //         case VersionType.Production:
       //             return 0;
       //             break;
       //     }

       //     return 0;

       // }
       protected void Page_Load(object sender, EventArgs e)
       {
           //RunWorkflow();
       }
       #region WORKFLOW
            #region WORKFLOW PROPERTIES
            public WorkflowRuntime WFRuntime
            {
                get
                {
                    return (WorkflowRuntime)Application["WorkflowRuntime"];
                }
                set
                {
                    Application["WorkflowRuntime"] = value;
                }
            }
            private Exception wfException { get; set; }
            #endregion
            #region WORKFLOW METHODS
            /// <summary>
            /// Serve para correr o workflow sobre uma determinada (uid) instância
            /// </summary>
            /// <param name="uid">guid da instância do workflow</param>
            protected void RunWorkflow(Guid uid)
            {
                WF_SRA.OnInstanceStateChange += new EventHandler<InstanceStateChangeEventArgs>(WF_SRA_OnInstanceStateChange);
                WF_SRA.OnWorkflowException += new EventHandler<ExceptionEventArgs>(WF_SRA_OnWorkflowException);

                this.wfException = null;
                ManualWorkflowSchedulerService scheduler = this.WFRuntime.GetService<ManualWorkflowSchedulerService>();
                scheduler.RunWorkflow(uid);

                //// Faz update do objecto que está debaixo do workflow com o estado actual do mesmo
                //Proposals oProp = new Proposals();
                //oProp.UpdateCurrentState(this.idProposal, this.WFInstance.CurrentState.QualifiedName);

                WF_SRA.OnInstanceStateChange -= new EventHandler<InstanceStateChangeEventArgs>(WF_SRA_OnInstanceStateChange);
                WF_SRA.OnWorkflowException -= new EventHandler<ExceptionEventArgs>(WF_SRA_OnWorkflowException);

                if (this.wfException != null)
                    throw (Exception)this.wfException;
            }
            /// <summary>
            /// Sem parâmetros serve para iniciar uma nova instância do workflow
            /// </summary>
            /// <returns>guid da instância recém criada</returns>
            protected Guid RunWorkflow()
            {
                WF_SRA wfManager = new WF_SRA();
                WF_SRA.OnInstanceStateChange += new EventHandler<InstanceStateChangeEventArgs>(WF_SRA_OnInstanceStateChange);
                WF_SRA.OnWorkflowException += new EventHandler<ExceptionEventArgs>(WF_SRA_OnWorkflowException);

                Guid newuid = wfManager.StartWorkflow(this.WFRuntime);

                //// Faz update do objecto que está debaixo do workflow com o estado inicial do mesmo
                //Proposals oProp = new Proposals();
                //oProp.UpdateCurrentState(this.idProposal, this.WFInstance.CurrentState.QualifiedName);

                WF_SRA.OnInstanceStateChange -= new EventHandler<InstanceStateChangeEventArgs>(WF_SRA_OnInstanceStateChange);
                WF_SRA.OnWorkflowException -= new EventHandler<ExceptionEventArgs>(WF_SRA_OnWorkflowException);
                
                return newuid;
            }
        #endregion
            #region WORKFLOW EVENTS
        /// <summary>
        /// Método executado pelo workflow quando no momento do ManualWorkflowSchedulerService.RunWorkflow(GUID) 
        ///  seja despoletado o evento de mudança de estado
        /// </summary>
        void WF_SRA_OnInstanceStateChange(object sender, InstanceStateChangeEventArgs e)
        {
            try
            {
                //BLL bl = new BLL();
                //bl.UpdatePartyWorkflowIdById(e.InstanceID, e.State.Name);     
                //bl.changeStateOccured(PartyServices.GetSelectedPartyForWorkflowId(e.InstanceID).idPartySelected,e.State.Name,this.CurrentUser.Username);
            }
            catch (Exception ex)
            {
                this.wfException = new Exception("Não foi possível enviar o email", ex);
                //ExceptionPolicy.HandleException(ex, "LogOnly");
            }
        }
        /// <summary>
        /// Método executado pelo workflow quando no momento do ManualWorkflowSchedulerService.RunWorkflow(GUID)
        ///  seja lançada uma excepção
        /// </summary>
        void WF_SRA_OnWorkflowException(object sender, ExceptionEventArgs e)
        {
            try
            {
                this.wfException = e.Exception;
            }
            catch (WFProceedConditions ex)
            {
                ExceptionPolicy.HandleException(ex, "LogOnly");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "LogOnly");
            }
        }
        #endregion
        #endregion
    }
}