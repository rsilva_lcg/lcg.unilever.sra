﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.UI;

namespace SpiritucSI.Unilever.SRA.WF.WebSite.Common
{
    public class SuperMasterPage : System.Web.UI.MasterPage
    {
        public string BaseUrl
        {
            get
            {
                return this.Request.Url.Scheme + "://" + this.Request.Url.Host + ((ConfigurationManager.AppSettings["BaseURL"] != null)? ConfigurationManager.AppSettings["BaseURL"].ToString() : "");
            }
        }
    }
}