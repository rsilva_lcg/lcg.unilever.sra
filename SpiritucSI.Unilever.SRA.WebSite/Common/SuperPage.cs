﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace SpiritucSI.Unilever.SRA.WF.WebSite.Common
{
    public class SuperPage : System.Web.UI.Page
    {
        public string BaseUrl
        {
            get
            {
                return this.Request.Url.Scheme + "://" + this.Request.Url.Host + ((ConfigurationManager.AppSettings["BaseURL"] != null) ? ConfigurationManager.AppSettings["BaseURL"].ToString() : "");
            }
        }
    }
}
