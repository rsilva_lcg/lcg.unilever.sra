﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI;
using eWorld.UI;

namespace SpiritucSI.Unilever.SRA.WF.WebSite.Common
{
    public class Utils
    {
        public static string GetBaseUrl()
        {
            return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + "/" + ConfigurationManager.AppSettings["Root"].ToString() + "/";
        }

        public static List<Guid> RoleIDs
        {
            get
            {
                List<Guid> rids = new List<Guid>();
                //if (HttpContext.Current.Session["RoleIDs"] == null
                //    || (rids = ((List<Guid>)HttpContext.Current.Session["RoleIDs"])).Count == 0)
                //{
                //    rids = (new XeroxRoleManager()).GetRoleIDsForUser(HttpContext.Current.User.Identity.Name);
                //    HttpContext.Current.Session["RoleIDs"] = rids;
                //}

                return rids;
            }
            set
            {
                HttpContext.Current.Session["RoleIDs"] = value;
            }
        }

        public static bool ValidateGuid(string val)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(val, @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");
        }

        public static double? GetNullableDouble(string val)
        {
            double? nVal = null;
            if (val.Equals(""))
                return nVal;
            else
                return double.Parse(val);
        }

        public static int? GetNullableInt(string val)
        {
            int? nVal = null;
            if (val.Equals(""))
                return nVal;
            else
                return int.Parse(val);
        }

        public static string FormatToCurrency(double d)
        {
            return FormatToCurrency(d, 2);
        }

        public static string FormatToCurrency(double d, int numdecimals)
        {
            try
            {
                return string.Format("{0:N" + numdecimals.ToString() + "}", Math.Round(d, numdecimals));
            }
            catch (Exception)
            {
                return "n/a";
            }
        }

        public static void CleanControl(Control c)
        {
            if (c.Controls.Count == 0)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = String.Empty;
                }
                if (c is DropDownList)
                {
                    ((DropDownList)c).SelectedIndex = (((DropDownList)c).Items.Count > 0) ? 0 : -1;
                }
                if (c is CheckBox)
                {
                    ((CheckBox)c).Checked = false;
                }
                if (c is NumericBox)
                {
                    ((NumericBox)c).Text = string.Empty;
                }
                if (c is CalendarPopup)
                {
                    ((CalendarPopup)c).SelectedValue = null;
                }
            }
            else
            {
                if (!(c is GridView) && !(c is DataGrid) && !(c is DataGrid))
                {
                    for (int i = 0; i < c.Controls.Count; i++)
                        Utils.CleanControl(c.Controls[i]);
                }
            }
        }

        public static void SendMail(MailAddress to, string subject, string body)
        {
            Utils.SendMail(new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString()), to, subject, body);
        }
        public static void SendMail(MailAddress to, string subject, string body, AttachmentCollection attachs)
        {
            Utils.SendMail(new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString()), to, subject, body, attachs);
        }
        public static void SendMail(MailAddressCollection to, string subject, string body)
        {
            Utils.SendMail(new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString()), to, subject, body);
        }
        public static void SendMail(MailAddressCollection to, string subject, string body, AttachmentCollection attachs)
        {
            Utils.SendMail(new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString()), to, subject, body, attachs);
        }

        public static void SendMail(MailAddress from, MailAddress to, string subject, string body)
        {
            Utils.SendMail(from, to, subject, body, null);
        }
        public static void SendMail(MailAddress from, MailAddress to, string subject, string body, AttachmentCollection attachs)
        {
            MailMessage oMsg = new MailMessage(from, to);

            oMsg.Subject = subject;
            oMsg.Body = body;
            oMsg.IsBodyHtml = true;

            if (attachs != null && attachs.Count > 0)
            {
                foreach (Attachment at in attachs)
                    oMsg.Attachments.Add(at);
            }

            try
            {
                SmtpClient smtp = new SmtpClient();
                smtp.Host = System.Configuration.ConfigurationManager.AppSettings["SMTP"].ToString();

                smtp.Send(oMsg);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocorreu um problema a enviar um email", ex);
            }
        }
        public static void SendMail(MailAddress from, MailAddressCollection to, string subject, string body)
        {
            Utils.SendMail(from, to, subject, body, null);
        }
        public static void SendMail(MailAddress from, MailAddressCollection to, string subject, string body, AttachmentCollection attachs)
        {
            MailMessage oMsg = new MailMessage();

            oMsg.IsBodyHtml = true;
            oMsg.From = from;
            oMsg.Subject = subject;
            oMsg.Body = body;

            foreach (MailAddress ma in to)
            {
                oMsg.To.Add(ma);
            }

            if (attachs != null && attachs.Count > 0)
            {
                foreach (Attachment at in attachs)
                    oMsg.Attachments.Add(at);
            }

            try
            {
                SmtpClient smtp = new SmtpClient();
                smtp.Host = System.Configuration.ConfigurationManager.AppSettings["SMTP"].ToString();

                smtp.Send(oMsg);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocorreu um problema a enviar um email", ex);
            }
        }

        ///// <summary>
        ///// Uploads a file to a proposal folder
        ///// </summary>
        ///// <param name="fup"></param>
        ///// <param name="IdProposal"></param>
        ///// <param name="path"></param>
        ///// <returns></returns>
        //public static bool UploadFilesToServer(FileUpload fup, Guid IdProposal, string path)
        //{
        //    if (fup.HasFile)
        //    {
        //        string uri = HttpContext.Current.Server.MapPath(path + "/" + IdProposal.ToString());
        //        if (!System.IO.Directory.Exists(uri))
        //        {
        //            System.IO.Directory.CreateDirectory(uri);
        //        }

        //        try
        //        {
        //            fup.SaveAs(HttpContext.Current.Server.MapPath(path + "/" + IdProposal.ToString() + "/" + fup.FileName));
        //        }
        //        catch (Exception ex)
        //        {
        //            return false;
        //        }
        //        return true;
        //    }

        //    return false;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="fup"></param>
        ///// <param name="IdProposal"></param>
        ///// <param name="path"></param>
        ///// <returns></returns>
        //public static bool UploadFilesToServer(AsyncFileUpload fup, Guid IdProposal, string path)
        //{
        //    if (fup.HasFile)
        //    {
        //        string uri = HttpContext.Current.Server.MapPath(path + "/" + IdProposal.ToString());
        //        if (!System.IO.Directory.Exists(uri))
        //        {
        //            System.IO.Directory.CreateDirectory(uri);
        //        }

        //        try
        //        {
        //            fup.SaveAs(HttpContext.Current.Server.MapPath(path + "/" + IdProposal.ToString() + "/" + fup.FileName));
        //        }
        //        catch (Exception ex)
        //        {
        //            return false;
        //        }
        //        return true;
        //    }

        //    return false;
        //}

        public static string ReadFile(string filepath)
        {
            string filecontent = "";

            if (File.Exists(filepath))
            {
                filecontent = File.ReadAllText(filepath, System.Text.Encoding.UTF8);
            }
            return filecontent;
        }

        public static string RedirectScriptUpdatePanel(string page, int timer)
        {

            //return "<script type=\"text/javascript\">\n" +
            return //"window.onload = \n" +
                //    "function() {\n" +
                           "setTimeout('window.location = \"" + page + "\"', " + timer + ");\n";
            //    "};\n";// +
            //"</script>";
        }
    }
}
