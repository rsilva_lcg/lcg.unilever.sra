﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="SpiritucSI.Unilever.SRA.WF.WebSite.ErrorPage" Theme="MainTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Header">        
        <div class="HeaderContent">
            <div><asp:Image ID="Image1" runat="server" ImageUrl="" ToolTip="Client/Project Logo" /></div>
        </div>
        <div class="HeaderContent2">
            <div class="HeaderMenu">
                <div class="DivFloatLeft"></div>
                <div class="DivFloatRight">
                    <div class="MenuTab">
                        <div id="Left" class="OffLeft"></div>
                        <div id="Center" class="OffCenter"><div><asp:LoginStatus ID="LoginStatus1" runat="server" LogoutText="Logout" CssClass="" LogoutAction="RedirectToLoginPage" /></div></div>
                        <div id="Right" class="OffRight"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="HeaderContent3"></div>
        <div class="MainContent1"><h2><asp:Literal ID="LiteralError" runat="server" Text="Ocorreu um erro."></asp:Literal></h2></div>
    </div>
    </form>
</body>
</html>
