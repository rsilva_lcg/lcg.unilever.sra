using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;


using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace SpiritucSI.Unilever.SRA.WF.WebSite
{
    public class bll_admin_RSS
    {
        private SqlConnection _conn_log;

        public bll_admin_RSS(string conn_log)
        {
            try
            {
                _conn_log = new SqlConnection(conn_log);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public DataTable SelectErrorByLogId(string id)
        {
            try
            {
                DataTable dt = new DataTable();

                SqlDataAdapter da = new SqlDataAdapter("SELECT top 1 * FROM Log WHERE LogId = " + id, _conn_log);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return new DataTable();
        }

        public DataTable SelectErrors()
        {
            try
            {
                DataTable dt = new DataTable();

                SqlDataAdapter da = new SqlDataAdapter("SELECT top 20 * FROM Log ORDER by timestamp desc", _conn_log);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return new DataTable();
        }

    }
}
