﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Configuration;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity
{
    public partial class LoadClientList : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                this.Load(0, GV_Clients.PageSize+1); 
        }

        protected void Load(int index, int numrows)
        {

            BLL ds = new BLL();

            int idCompany = ds.GetCompanyId(ConfigurationManager.AppSettings["CompanyName"].ToString());

            int idProcess = ds.GetCurrentProcessId(idCompany);
            GV_Clients.DataSource = new BLL().GetPartiesByProcess(idProcess, 0, 0);
            GV_Clients.DataBind();

        }

        protected void LB_Name_Click(object sender,  EventArgs e)
        {
            
            LinkButton lb = (LinkButton)sender;

            Session["idParty"] = lb.CommandArgument;
            this.Response.Redirect("~/IndividualImparity/ClientDetail.aspx");
            
        }

        protected void GV_Clients_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.GV_Clients.PageIndex = e.NewPageIndex;
                this.Load(0,0);
            }
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
                {
                    //this.ShowMessage(GetLocalResource("Messages.Error.C_gv_List_PageIndexChanging"), MessageType.ErrorBox);
                }
            }
        }


    }
}
