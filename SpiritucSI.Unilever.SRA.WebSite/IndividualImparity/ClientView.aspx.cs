﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.WebSite.UserControls.Workflow;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using SpiritucSI.BPN.AII.Business;
using SpiritucSI.BPN.AII.WebSite.Common;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity
{
    public partial class ClientView : System.Web.UI.Page
    {
        private int v_idPartySelected;

        public int idPartySelected
        {

            get
            {
                if (v_idPartySelected == 0)
                {


                    if (Session["idParty"] == null)
                        this.Response.Redirect("~/Dashboard.aspx");
                    else
                    {
                        int id = 0;
                        Int32.TryParse(Session["idParty"].ToString(), out id);
                        if (id != 0)
                            v_idPartySelected = id;
                    }
                }

                return v_idPartySelected;
            }

            set
            {
                v_idPartySelected = value;

            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //BLL bl = new BLL();
            //List<PARTYSELECTED> p =  bl.GetPartySelectedById(this.idPartySelected).ToList();
            //((WorkflowAcctions)ucWorkflowAcctions).uid = p[0].idWorkflow.ToString() ;

            //ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), "redirectScript", "if (document.getElementById('" + this.ucWorkflowAcctions.GetRedirect() + "').value == 'true') { " + Utils.RedirectScriptUpdatePanel("Dashboard.aspx", 1500) + " }", true);
        }
    }
}
