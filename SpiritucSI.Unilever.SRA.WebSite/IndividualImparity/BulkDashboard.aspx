﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="true" CodeBehind="BulkDashboard.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.BulkDashboard" Theme="MainTheme" %>
<%@ Register Src="~/IndividualImparity/userControls/ucAnalisysSummary.ascx" TagName="ucAnalisysSummary" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <uc1:ucAnalisysSummary id="ucAnalisysSummary" runat="server" HasActionButtons="true" Pagging="false" HasImparity="true" HasBulk="false"></uc1:ucAnalisysSummary>
    <script type="text/javascript">
            $("div [id*='ob_grid1TrialDiv']").css('display', 'none');
    </script>
</asp:Content>
