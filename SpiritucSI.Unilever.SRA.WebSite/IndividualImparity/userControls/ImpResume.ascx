﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImpResume.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ImpResume" %>
<div>
<br />
<asp:Panel ID ="Pnl_Sum" runat ="server">
<label class="lblBold">Totais Perdas</label>
<table CssClass="GridView" style =" width:100%;">
<tr><td style="border-bottom:solid 1px;width:50px;" ><label>Fluxos</label></td><td style=" text-align:right;"><label><%# ImpValues[0].ToString("#,##0.00")%></label></td></tr>
<tr><td style="border-bottom:solid 1px;width:50px;"><label>Garantias</label></td><td style=" text-align:right;"><label><%#ImpValues[1].ToString("#,##0.00")%></label></td></tr>
<tr><td style="border-bottom:solid 1px;width:50px;"><label>Limites</label></td><td style=" text-align:right;"><label><%#ImpValues[2].ToString("#,##0.00")%></label></td></tr>
</table>
</asp:Panel>
</div>