﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucColaterals.ascx.cs"
    Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ucColaterals" %>
<%@ Register Src="../../UserControls/Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="uc1" %>
<div class="SubContentCenter">
    <label class="lblBoldHidd">
        Garantia Real</label>
    <br />
    <table>
        <tr>
            <td>
                <div class="padBottom">
                    <asp:ValidationSummary ID="ValidationSummary2" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Garantias Reais"
                        ShowMessageBox="false" ShowSummary="true" ValidationGroup="InsertReais" runat="server" />
                </div>
                <div class="padBottom">
                    <asp:ValidationSummary ID="ValidationSummary4" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Garantias Reais"
                        ShowMessageBox="false" ShowSummary="true" ValidationGroup="InsertAlternativeReais"
                        runat="server" />
                </div>
                <div class="padBottom">
                    <asp:ValidationSummary ID="ValidationSummary5" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Garantias Reais"
                        ShowMessageBox="false" ShowSummary="true" ValidationGroup="update" runat="server" />
                </div>
                <label class="lblBold">
                    Garantias Reais</label>
                <br />
            </td>
        </tr>
        <tr valign="top">
            <td class="txtCenter">
                <asp:GridView ID="GV_Colaterals_Real" CssClass="GridView" runat="server" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" Width="995px" DataKeyNames="idColateral"
                    OnRowUpdating="GV_Colaterals_Real_RowUpdating" OnRowEditing="GV_Colaterals_Real_RowEditing"
                    OnRowCancelingEdit="GV_Colaterals_Real_RowCancelingEdit" ShowFooter="true" OnDataBound="GV_Colaterals_Real_DataBound"
                    OnRowDataBound="GV_Colaterals_Real_RowDataBound" OnRowDeleting="GV_Colaterals_Real_RowDeleting"
                    OnRowCommand="GV_ColateralsReal_RowCommand">
                    <HeaderStyle CssClass="GridViewHeader" />
                    <RowStyle CssClass="GridViewRow" />
                    <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
                    <SelectedRowStyle CssClass="GridViewSelectedRow" />
                    <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />
                    <EmptyDataTemplate>
                        <table rules="all" cellspacing="0" border="1" style="border-collapse: collapse;width:995px;">
                            <tr align:center;" >


                                <th>
                                    <span >Nº Operação</span>
                                </th>
                                <th>
                                    <span >Tipo</span>
                                </th>
                                <th style ="width:45px;">
                                    <span >Descrição Garantia</span>
                                </th>
                                <th style ="width:45px;">
                                    <span >Valor</span>
                                </th>
                                <th style ="width:45px;">
                                    <span >Data de Avaliação</span>
                                </th>
                                <th style ="width:45px;">
                                    <span >Valor de Venda Forçada</span>
                                </th>
                                <th>
                                    <span>&nbsp;</span>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList Width="165px" ID="DL_ContractsG" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DL_Type" runat="server" Width="380px" >
                                        <asp:ListItem Text="1ª Hipoteca de Habitação Principal" Value="1" Selected ="True" ></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Habitação Secundária" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Imóveis Escritórios/Lojas" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Imóveis Industriais/Armazéns" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Terrenos para Construção" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Terrenos uso agrícola" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Outros Imóveis" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="2ª Hipoteca de Outros Imóveis" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Depósito a Prazo no Grupo BPN" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Depósito a Prazo em OIC" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emitidos pelo Grupo BPN)" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emitidos pelo Grupo SLN)" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emit. por outras entid. privadas)" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emissões de dívida pública)" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Acções SLN" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Outras Acções cotadas em bolsa" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Outras Acções não cotadas em bolsa" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de UP's de Fundos de Investimento" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Viaturas" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Barcos, Aeronaves" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Outros Bens" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="Outras Garantias Reais" Value="22"></asp:ListItem>
                                        </asp:DropDownList>
                                    <%--<asp:TextBox Width="145px" ID="Tbx_type" Text="Tipo" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipo1" runat="server" CssClass="lblError"
                                        InitialValue="Tipo" Display="None" ControlToValidate="Tbx_type" ValidationGroup="InsertAlternativeReais"
                                        ForeColor="" ErrorMessage="Campo Tipo - Preenchimento Obrigatório"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td>
                                    <asp:TextBox Width="145px" ID="Tbx_description" Text="Descrição Garantia" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValidatorGarantia1" runat="server" CssClass="lblError"
                                        InitialValue="Descrição Garantia" Display="None" ControlToValidate="Tbx_description"
                                        ValidationGroup="InsertAlternativeReais" ForeColor="" ErrorMessage="Campo Descrição Garantia - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:TextBox Width ="60px" ID="Tbx_value" Text="Valor" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValidatorValor1" runat="server" CssClass="lblError"
                                        InitialValue="Valor" Display="None" ControlToValidate="Tbx_value" ValidationGroup="InsertAlternativeReais"
                                        ForeColor="" ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator EnableClientScript = "false" ID="RegExValValor" CssClass="lblError" runat="server"
                                        ControlToValidate="Tbx_value" ValidationGroup="InsertAlternativeReais" Display ="None"
                                        ErrorMessage="Campo Valor - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type="Double" ></asp:RangeValidator>
                                </td>
                                <td>
                                    <asp:TextBox Width="60px" ID="Tbx_avalDate" Text="Data de Avaliação" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorData1" runat="server" CssClass="lblError"
                                        InitialValue="Data de Avaliação" Display="None" ControlToValidate="Tbx_avalDate"
                                        ValidationGroup="InsertAlternativeReais" ForeColor="" ErrorMessage="Campo Data de Avaliação - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="RangeValidator2" ErrorMessage="Campo Data - (Ano-mes-dia)" ControlToValidate="Tbx_avalDate" ValidationGroup="InsertAlternativeReais" Type="Date" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="01-01-2100" MinimumValue="01-01-1900"></asp:RangeValidator>
                                </td>
                                <td>
                                    <asp:TextBox Width ="60px" ID="Tbx_forcedSale" Text="Valor de Venda Forçada" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorValorVenda1" runat="server"
                                        CssClass="lblError" InitialValue="Valor de Venda Forçada" Display="None" ControlToValidate="Tbx_forcedSale"
                                        ValidationGroup="InsertAlternativeReais" ForeColor="" ErrorMessage="Campo Valor de Venda Forçada - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator EnableClientScript = "false" ID="RegExValForcedSale" CssClass="lblError" runat="server"
                                        ControlToValidate="Tbx_forcedSale" ValidationGroup="InsertAlternativeReais" Display="none"
                                        ErrorMessage="Campo Valor Forçado - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type="Double"></asp:RangeValidator>
                               </td>
                                <td>
                                    <div class="padLabel1">
                                        <asp:Button ID="Btt_InsertColateral" ValidationGroup="InsertAlternativeReais" runat="server"
                                            Text="Inserir" CommandName="InsertEmptyItem" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField FooterStyle-Width="20px" HeaderText="Nº Operação">
                            <ItemTemplate>
                                <asp:Label Width="165px" ID="DL_ContractsG" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "contractReference")%>'></asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="DL_ContractsG" Width="165px" runat="server">
                                </asp:DropDownList>
                                <asp:HiddenField ID="HF_idContract" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "idContract")%>' />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="DL_ContractsGF" Width="165px" runat="server">
                                </asp:DropDownList>
                                <asp:Button ID="Btt_InsertC" runat="server" ValidationGroup="InsertReais" Text="Inserir"
                                    OnClick="Btt_InsertColateralR_Click" Visible="false" CommandArgument ="BttInsert" CausesValidation ="true" /></FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tipo">
                            <ItemTemplate>
                                    <asp:HiddenField ID = "HF_type" runat = "server" Value = '<%#DataBinder.Eval(Container.DataItem, "c.type")%>' />
                                    <asp:DropDownList ID="DL_Type" runat="server" Width ="365px" >
                                    <asp:ListItem Text="1ª Hipoteca de Habitação Principal" Value="1" Selected ="True"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Habitação Secundária" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Imóveis Escritórios/Lojas" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Imóveis Industriais/Armazéns" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Terrenos para Construção" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Terrenos uso agrícola" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Outros Imóveis" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="2ª Hipoteca de Outros Imóveis" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Depósito a Prazo no Grupo BPN" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Depósito a Prazo em OIC" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emitidos pelo Grupo BPN)" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emitidos pelo Grupo SLN)" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emit. por outras entid. privadas)" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emissões de dívida pública)" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Acções SLN" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Outras Acções cotadas em bolsa" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Outras Acções não cotadas em bolsa" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de UP's de Fundos de Investimento" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Viaturas" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Barcos, Aeronaves" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Outros Bens" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="Outras Garantias Reais" Value="22"></asp:ListItem>
                                        </asp:DropDownList>
                                <%--<asp:Label ID="Label4" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.type")%>'> </asp:Label>--%>
                                </ItemTemplate>
                            <EditItemTemplate>
                                    <asp:HiddenField ID = "HF_type" runat = "server" Value = '<%#DataBinder.Eval(Container.DataItem, "c.type")%>' />
                                    <asp:DropDownList ID="DL_Type" runat="server" Enabled = "true" Width ="365px">
                                    <asp:ListItem Text="1ª Hipoteca de Habitação Principal" Value="1" Selected ="True" ></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Habitação Secundária" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Imóveis Escritórios/Lojas" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Imóveis Industriais/Armazéns" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Terrenos para Construção" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Terrenos uso agrícola" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Outros Imóveis" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="2ª Hipoteca de Outros Imóveis" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Depósito a Prazo no Grupo BPN" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Depósito a Prazo em OIC" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emitidos pelo Grupo BPN)" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emitidos pelo Grupo SLN)" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emit. por outras entid. privadas)" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emissões de dívida pública)" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Acções SLN" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Outras Acções cotadas em bolsa" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Outras Acções não cotadas em bolsa" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de UP's de Fundos de Investimento" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Viaturas" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Barcos, Aeronaves" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Outros Bens" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="Outras Garantias Reais" Value="22"></asp:ListItem>
                                        </asp:DropDownList>
                                <%--<asp:TextBox ID="TextBox1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.type")%>' />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipo2" runat="server" CssClass="lblError"
                                    Display="None" ControlToValidate="TextBox1" ValidationGroup="update" ForeColor=""
                                    ErrorMessage="Campo Tipo - Preenchimento Obrigatório"></asp:RequiredFieldValidator>--%>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:HiddenField ID = "HF_type" runat = "server" Value = '<%#DataBinder.Eval(Container.DataItem, "c.type")%>' />
                                <asp:DropDownList ID="DL_Type" runat="server" Enabled = "true" Width ="365px" >
                                    <asp:ListItem Text="1ª Hipoteca de Habitação Principal" Value="1" Selected ="True" ></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Habitação Secundária" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Imóveis Escritórios/Lojas" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Imóveis Industriais/Armazéns" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Terrenos para Construção" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Terrenos uso agrícola" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="1ª Hipoteca de Outros Imóveis" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="2ª Hipoteca de Outros Imóveis" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Depósito a Prazo no Grupo BPN" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Depósito a Prazo em OIC" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emitidos pelo Grupo BPN)" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emitidos pelo Grupo SLN)" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emit. por outras entid. privadas)" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Títulos de Dívida deposit. no BPN (emissões de dívida pública)" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Acções SLN" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Outras Acções cotadas em bolsa" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de Outras Acções não cotadas em bolsa" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="Penhor de UP's de Fundos de Investimento" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Viaturas" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Barcos, Aeronaves" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="Penhor Mercantil de Outros Bens" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="Outras Garantias Reais" Value="22"></asp:ListItem>
                                        </asp:DropDownList>
                                <%--<asp:TextBox ID="TextBox2" Text="Tipo" runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipo" runat="server" CssClass="lblError"
                                    InitialValue="Tipo" Display="None" ControlToValidate="TextBox2" ValidationGroup="InsertReais"
                                    ForeColor="" ErrorMessage="Campo Tipo - Preenchimento Obrigatório"></asp:RequiredFieldValidator>--%>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descrição Garantia" HeaderStyle-Width="145px" >
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.description")%>'> </asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.description")%>' />
                                <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValidatorGarantia2" runat="server" CssClass="lblError"
                                    Display="None" ControlToValidate="TextBox3" ValidationGroup="update" ForeColor=""
                                    ErrorMessage="Campo Descrição Garantia - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="TextBox4" Text="Descrição Garantia" runat="server" />
                                <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValidatorGarantia" runat="server" CssClass="lblError"
                                    InitialValue="Descrição Garantia" Display="None" ControlToValidate="TextBox4"
                                    ValidationGroup="InsertReais" ForeColor="" ErrorMessage="Campo Descrição Garantia - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor" HeaderStyle-Width="45px" >
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.value", "{0:#,##0.00}")%>'> </asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" Width ="60px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.value")%>' />
                                <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValidatorValor2" runat="server" CssClass="lblError"
                                    Display="None" ControlToValidate="TextBox5" ValidationGroup="update" ForeColor=""
                                    ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                <asp:RangeValidator EnableClientScript = "false" ID="RegExValValue" CssClass="lblError" runat="server"
                                    ControlToValidate="TextBox5" ValidationGroup="update" Display="none"
                                    ErrorMessage="Campo Valor - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type="Double"></asp:RangeValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox Width ="60px" ID="TextBox6" Text="Valor" runat="server" />
                                <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValidatorValor" runat="server" CssClass="lblError"
                                    InitialValue="Valor" Display="None" ControlToValidate="TextBox6" ValidationGroup="InsertReais"
                                    ForeColor="" ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                <asp:RangeValidator EnableClientScript = "false" ID="RegExValValue2" CssClass="lblError" runat="server"
                                    ControlToValidate="TextBox6" ValidationGroup="InsertReais" Display="none"
                                    ErrorMessage="Campo Valor - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type="Double"></asp:RangeValidator>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data de Avaliação" HeaderStyle-Width="45px" >
                            <ItemTemplate>
                                <asp:Label Width="60px" ID="Label7" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.evaluationDate", "{0:dd-MM-yyyy}")%>'> </asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox Width="60px" ID="TextBox7"   runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.evaluationDate", "{0:dd-MM-yyyy}")%>' />
                                <asp:RequiredFieldValidator EnableClientScript = "false"  ID="RequiredFieldValidatorData2" runat="server" CssClass="lblError"
                                    Display="None" ControlToValidate="TextBox7" ValidationGroup="update" ForeColor=""
                                    ErrorMessage="Campo Data de Avaliação - Preenchimento Obrigatório"></asp:RequiredFieldValidator>

                                <%--<asp:RegularExpressionValidator EnableClientScript = "false" ID="RegExValDate" CssClass="lblError" runat="server"
                                    ControlToValidate="TextBox7" ValidationGroup="update" Display="none"
                                    ErrorMessage="Campo Data2 - (Ano-mes-dia)" ValidationExpression="^(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)$"></asp:RegularExpressionValidator>--%>              
                                    <asp:RangeValidator EnableClientScript ="false" Display ="None"  ErrorMessage="Campo Data2 - (Ano-mes-dia)" ControlToValidate="TextBox7" ValidationGroup="update" Type="Date" runat="server" MaximumValue="01-01-2100" MinimumValue="01-01-1900"></asp:RangeValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox Width="60px" ID="TextBox8" Text ="Data de Avaliação" EnableViewState ="true"   runat="server" />
                                <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValidatorData" runat="server" CssClass="lblError"
                                    InitialValue="Data de Avaliação" Display="None" ControlToValidate="TextBox8"
                                    ValidationGroup="InsertReais" ForeColor="" ErrorMessage="Campo Data de Avaliação - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                        <%--        <asp:RegularExpressionValidator EnableClientScript = "false" ID="RegExValDate" CssClass="lblError" runat="server"
                                    ControlToValidate="TextBox8" ValidationGroup="InsertReais" Display="none"
                                    ErrorMessage="Campo Data1 - (Ano-mes-dia)" ValidationExpression="^(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)$"></asp:RegularExpressionValidator>--%>
                                 <asp:RangeValidator EnableClientScript ="false" Display ="None" ID="RangeValidator1"  ErrorMessage="Campo Data - (Ano-mes-dia)" ControlToValidate="TextBox8" ValidationGroup="InsertReais" Type="Date" runat="server" MaximumValue="01-01-2100" MinimumValue="01-01-1900"></asp:RangeValidator>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor de Venda forçada" HeaderStyle-Width="45px">
                            <ItemTemplate>
                                <asp:Label Width="60px" ID="Label8"  runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.forcedSaleValue", "{0:#,##0.00}")%>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox Width="60px" ID="TextBox9"  runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.forcedSaleValue")%>' />
                                <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValidatorValorVenda2" runat="server"
                                    CssClass="lblError" Display="None" ControlToValidate="TextBox9" ValidationGroup="update"
                                    ForeColor="" ErrorMessage="Campo Valor de Venda Forçada - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator EnableClientScript = "false" ID="RegExValSale" CssClass="lblError" runat="server"
                                    ControlToValidate="TextBox9" ValidationGroup="update" Display="none"
                                    ErrorMessage="Campo Valor Venda Forçada - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type="Double"></asp:RangeValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox Width="60px" ID="TextBox9" Text ="Valor de Venda Forçada" EnableViewState ="true"  runat="server" />
                                <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValidatorValorVenda" runat="server"
                                    CssClass="lblError" InitialValue="Valor de Venda Forçada" Display="None" ControlToValidate="TextBox9"
                                    ValidationGroup="InsertReais" ForeColor="" ErrorMessage="Campo Valor de Venda Forçada - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                <asp:RangeValidator EnableClientScript = "false" ID="RegExValSale" CssClass="lblError" runat="server"
                                    ControlToValidate="TextBox9" ValidationGroup="InsertReais" Display="none"
                                    ErrorMessage="Campo Valor Venda Forçada - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type="Double"></asp:RangeValidator>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:CommandField CancelText="Cancelar" ValidationGroup="update" DeleteText="Remover"
                            EditText="Editar" InsertText="Inserir" NewText="Novo" SelectText="Seleccionar"
                            ShowDeleteButton="True" ShowEditButton="True" UpdateText="Actualizar" ItemStyle-Width="100px"/>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <div class="padBottom">
                    <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Garantia Pessoal"
                        ShowMessageBox="false" ShowSummary="true" ValidationGroup="InsertPessoal" runat="server" />
                </div>
                <div class="padBottom">
                    <asp:ValidationSummary ID="ValidationSummary3" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Garantia Pessoal"
                        ShowMessageBox="false" ShowSummary="true" ValidationGroup="InsertalternativePessoal"
                        runat="server" />
                </div>
                <div class="padBottom">
                    <asp:ValidationSummary ID="ValidationSummary6" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Garantia Pessoal"
                        ShowMessageBox="false" ShowSummary="true" ValidationGroup="updatePessoal" runat="server" />
                </div>
                <label class="lblBold">
                    Garantia Pessoal</label>
                <tr valign="top">
                    <td class="txtCenter">
                        <asp:GridView ID="GV_Colaterals_Personal" CssClass="GridView" runat="server" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" Width="995px" DataKeyNames="idColateral"
                            OnRowUpdating="GV_Colaterals_Personal_RowUpdating" OnRowEditing="GV_Colaterals_Personal_RowEditing"
                            OnRowCancelingEdit="GV_Colaterals_Personal_RowCancelingEdit" ShowFooter="true"
                            OnDataBound="GV_Colaterals_Personal_DataBound" OnRowDataBound="GV_Colaterals_Personal_RowDataBound"
                            OnRowDeleting="GV_Colaterals_Personal_RowDeleting" OnRowCommand="GV_ColateralsPersonal_RowCommand">
                            <HeaderStyle CssClass="GridViewHeader" />
                            <RowStyle CssClass="GridViewRow" />
                            <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
                            <SelectedRowStyle CssClass="GridViewSelectedRow" />
                            <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />
                            <EmptyDataTemplate>
                                <table rules="all" cellspacing="0" border="1" style="border-collapse: collapse;">
                                    <tr>
                                        <th>
                                            <span>Nº de Operação</span>
                                        </th>
                                        <th>
                                            <span>Avalistas</span>
                                        </th>
                                        <th>
                                            <span>Valor</span>
                                        </th>
                                        <th>
                                            <span>&nbsp;</span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="DL_ContractsG" Width="165" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Tbx_Evaluators" Text="Avalistas" Width="370" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldAvalistasNew" runat="server" CssClass="lblError"
                                                InitialValue="Avalistas" Display="None" ControlToValidate="Tbx_Evaluators" ValidationGroup="InsertalternativePessoal"
                                                ForeColor="" ErrorMessage="Campo Avalistas - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Tbx_Value" Width="370" Text="Valor" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValorNew" runat="server" CssClass="lblError"
                                                InitialValue="Valor" Display="None" ControlToValidate="Tbx_Value" ValidationGroup="InsertalternativePessoal"
                                                ForeColor="" ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator EnableClientScript = "false" ID="RegExValValue" CssClass="lblError" runat="server"
                                                ControlToValidate="Tbx_Value" ValidationGroup="InsertalternativePessoal" Display="none"
                                                ErrorMessage="Campo Valor - Natureza numérica" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td>
                                            <div class="padLabel1">
                                                <asp:Button ID="Btt_InsertColateralP" ValidationGroup="InsertalternativePessoal"
                                                    runat="server" Text="Inserir" CommandName="InsertEmptyItem" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField FooterStyle-Width="20px" HeaderText="Nº Operação">
                                    <ItemTemplate>
                                        <asp:Label ID="DL_ContractsG" Width="165px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "contractReference")%>'></asp:Label></ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DL_ContractsG" Width="165" runat="server">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="HF_idContract" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "idContract")%>' />
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="DL_ContractsGF" Width="165" runat="server">
                                        </asp:DropDownList>
                                        <asp:Button ID="Btt_InsertCP" ValidationGroup="InsertPessoal" runat="server" Text="Inserir"
                                            OnClick="Btt_InsertColateralP_Click" Visible="false" /></FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Avalistas">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Width="305px" Text='<%#DataBinder.Eval(Container.DataItem, "c.evaluators")%>'> </asp:Label></ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="Txb_Evaluators" Width="335" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "c.evaluators")%>' />
                                        <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldAvalistas2" runat="server" CssClass="lblError"
                                            Display="None" ControlToValidate="Txb_Evaluators" ValidationGroup="updatePessoal"
                                            ForeColor="" ErrorMessage="Campo Avalistas - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBox11" Text="Avalistas" Width="335" runat="server" />
                                        <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldAvalistas" runat="server" CssClass="lblError"
                                            InitialValue="Avalistas" Display="None" ControlToValidate="TextBox11" ValidationGroup="InsertPessoal"
                                            ForeColor="" ErrorMessage="Campo Avalistas - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Valor">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Width="305px" Text='<%#DataBinder.Eval(Container.DataItem, "c.value", "{0:#,##0.00}")%>'> </asp:Label></ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="Txb_Value" runat="server" Width="335" Text='<%#DataBinder.Eval(Container.DataItem, "c.value")%>' />
                                        <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValor2" runat="server" CssClass="lblError"
                                            Display="None" ControlToValidate="Txb_Value" ValidationGroup="updatePessoal"
                                            ForeColor="" ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator EnableClientScript = "false" ID="RegExValValue" CssClass="lblError" runat="server"
                                            ControlToValidate="Txb_Value" ValidationGroup="updatePessoal" Display="none"
                                            ErrorMessage="Campo Valor - Natureza numérica" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBox13" Text="Valor" runat="server" Width="335" />
                                        <asp:RequiredFieldValidator EnableClientScript = "false" ID="RequiredFieldValor" runat="server" CssClass="lblError"
                                            InitialValue="Valor" Display="None" ControlToValidate="TextBox13" ValidationGroup="InsertPessoal"
                                            ForeColor="" ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator EnableClientScript = "false" ID="RegExValValue" CssClass="lblError" runat="server"
                                            ControlToValidate="TextBox13" ValidationGroup="InsertPessoal" Display="none"
                                            ErrorMessage="Campo Valor - Natureza numérica" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:CommandField CancelText="Cancelar" ValidationGroup="updatePessoal" DeleteText="Remover"
                                    EditText="Editar" InsertText="Inserir" NewText="Novo" SelectText="Seleccionar"
                                    ShowDeleteButton="True" ShowEditButton="True" UpdateText="Actualizar" ItemStyle-Width="100px" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
    </table>
    
<div>
<asp:Panel ID ="Pnl_Sum" runat ="server" style=" padding-top:10px;">
<br />
<label class="lblBold">Totais Colaterais</label>
<table CssClass="GridView" style =" width:120px;">
<tr><td style="border-bottom:solid 1px; width:70px;"  ><label >Reais Valor</label></td><td style=" text-align:center;"><label><%# sumValues[0].ToString("#,##0.00")%></label></td></tr>
<tr><td style="border-bottom:solid 1px; width:70px;"  ><label >Reais Venda Forçada</label></td><td style=" text-align:center;"><label><%# sumValues[1].ToString("#,##0.00")%></label></td></tr>
<tr><td style="border-bottom:solid 1px; width:70px;" ><label >Pessoais</label></td><td style=" text-align:center;"><label><%#sumValues[2].ToString("#,##0.00")%></label></td></tr>
</table>
</asp:Panel>
</div>
    
    
    
</div>
<div class="padBottom">
    &nbsp;</div>
