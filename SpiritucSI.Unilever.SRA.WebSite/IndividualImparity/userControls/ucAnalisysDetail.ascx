﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAnalisysDetail.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ucAnalisysDetail" %>

<div style= "clear:both; width:1000px; padding-top : 40px;">
<hr />
<table>
  <tr><td><br /><label style =" font-size:12px; font-weight:bold;">Actualização de fluxos de caixa futuros</label>
<br />
    <tr valign="top">
        <td style = "text-align:center;">
        <asp:GridView ID="GV_Flows" runat="server" AllowPaging = "True" AllowSorting ="True" AutoGenerateColumns = "False" Width="1000px">
            <Columns>
            <asp:BoundField HeaderText = "Nº Operação" DataField = "valuesValue" />
            <asp:BoundField HeaderText = "<3 meses" DataField = "valuesValue" />
            <asp:BoundField HeaderText = "3 a 6 meses" DataField = "valuesValue"/>
            <asp:BoundField HeaderText = "6 a 9 meses" DataField = "valuesValue"/>
            <asp:BoundField HeaderText = "9 a 12 meses" DataField = "valuesValue" />
            <asp:BoundField HeaderText = "1 a 2 anos" DataField = "valuesValue" />
            <asp:BoundField HeaderText = "2 a 3 anos" DataField = "valuesValue" />
            <asp:BoundField HeaderText = "3 a 4 anos" DataField = "valuesValue" />
            <asp:BoundField HeaderText = "4 a 5 anos" DataField = "valuesValue" />
            <asp:BoundField HeaderText = "> 5 anos" DataField = "valuesValue" />
            <asp:BoundField HeaderText = "total" DataField = "valuesValue" />
            </Columns>
        </asp:GridView>
        </td></tr>
          <tr><td><br /><label style =" font-size:12px; font-weight:bold;">Perda por accionamentos de garantias bancárias</label>
<br />
        <tr valign="top"><td style = "text-align:center;">
                <asp:GridView ID="GV_Losses" runat="server" AllowPaging = "True" AllowSorting ="True" AutoGenerateColumns = "False" Width="1000px">
            <Columns>
                <asp:BoundField HeaderText = "Nº Operação" DataField = "valuesValue" />
                <asp:BoundField HeaderText = "Tipo" DataField = "valuesValue" />
                <asp:BoundField HeaderText = "Valor em dívida" DataField = "valuesValue"/>
                <asp:BoundField HeaderText = "Total" DataField = "valuesValue"/>
                <asp:BoundField HeaderText = "Total Fluxos" DataField = "valuesValue" />
                <asp:BoundField HeaderText = "Perda" DataField = "valuesValue" />
                <asp:BoundField HeaderText = "Perda (%)" DataField = "valuesValue" />
            </Columns>
        </asp:GridView>
        </td></tr>
</table>
</div>