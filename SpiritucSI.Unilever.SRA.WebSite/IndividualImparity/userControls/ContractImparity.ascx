﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractImparity.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ContractImparity" %>

<div class="SubContentCenter">
<div class="TitleDiv">
<label class="TitleTxt">Operações</label> 
<br />
<asp:GridView ID = "GV_Operations" runat = "server" CssClass="GridView" ShowHeader = "true" AllowPaging = "true" AllowSorting = "true" AutoGenerateColumns = "false" Width ="1000px" OnPageIndexChanging ="GV_Operations_PageIndexChanging" PagerSettings-Mode="NextPrevious" >
            <HeaderStyle CssClass="GridViewHeader" />
            <RowStyle CssClass="GridViewRow" />
            <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
            <SelectedRowStyle CssClass="GridViewSelectedRow" />
            <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />
<Columns>
<asp:TemplateField HeaderText = "Balcão"><ItemTemplate><asp:Label ID="Label1"  runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.Balcao")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Operação"><ItemTemplate><asp:Label ID="Label2" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.ContractReference")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Classe"><ItemTemplate><asp:Label ID="Label3" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.LossClass")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Moeda"><ItemTemplate><asp:Label ID="Label4" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.Currency")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Capital Vincendo"><ItemTemplate><asp:Label ID="Label5" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.BPN_Val1_CapVin")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Capital Vencido"><ItemTemplate><asp:Label ID="Label6"  runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.BPN_Val2_CapVen")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Juro Vencido"><ItemTemplate><asp:Label ID="Label7" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.BPN_Val5_JrsVen")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Juro Corrido"><ItemTemplate><asp:Label ID="Label8" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.BPN_Val6_JrsAnl")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Extra Patrimonial"><ItemTemplate><asp:Label ID="Label9" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.BPN_Val13_GarPre")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Limite"><ItemTemplate><asp:Label ID="Label10" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "c.UndrawnAmount")%> > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Total"><ItemTemplate><asp:Label ID="Label11" runat = "server" Text =<%#(double)DataBinder.Eval(Container.DataItem, "c.BPN_Val1_CapVin") + (double)DataBinder.Eval(Container.DataItem, "c.BPN_Val2_CapVen")+ (double)DataBinder.Eval(Container.DataItem, "c.BPN_Val5_JrsVen")+ (double)DataBinder.Eval(Container.DataItem, "c.BPN_Val6_JrsAnl")%>> </asp:Label></ItemTemplate></asp:TemplateField>
</Columns>
</asp:GridView>
</div>
</div>