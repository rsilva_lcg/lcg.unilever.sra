﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Questionnaires.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.Questionnaires" %>
<%@ Register src="../../UserControls/Common/MainMenu.ascx" tagname="MainMenu" tagprefix="uc1" %>


<div class="SubContentCenter">
<hr />
<div class="btnSaveDiv">
<table><tr><td>
<asp:Button ID ="Btt_Save" runat="server" Text="Guardar" Width="60px" OnClick="Btt_Save_Click" Visible='<%#!this.IsReadOnly%>' />
</td></tr></table>
</div>
<div class="padBottom">
    <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationSummary" HeaderText="Erro de preenchimento do enquadramento da Entidade - Todas as questões são de preenchimento obrigatório" ShowMessageBox="false" ShowSummary="true" ValidationGroup="Insert" runat="server" /></div>
 
<div class="padBottom">
    <asp:ValidationSummary ID="ValidationSummary2" CssClass="ValidationSummary" HeaderText="Erro de preenchimento no Comportamento do Cliente com a banca e o BPN - Todas as questões são de preenchimento obrigatório" ShowMessageBox="false" ShowSummary="true" ValidationGroup="Insert2" runat="server" /></div>
 
<div class="questionDiv" style ="width:1000px;">
<asp:RequiredFieldValidator ID ="RF_Questions1" runat ="server" ControlToValidate="Txt_Val" ValidationGroup="Insert" Display="None" Text ="Todas as respostas necessitam de ser preenchidas"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID ="RF_Questions2" runat ="server" ControlToValidate="Txt_Val" ValidationGroup="Insert2" Display="None" Text ="Todas as respostas necessitam de ser preenchidas"></asp:RequiredFieldValidator>
<asp:TextBox ID ="Txt_Val" runat ="server" Visible ="false" />
<div >
<div >
<table>
        <tr><td><br /><span class="lblBold" >Enquadramento da Entidade</span></td></tr></span></td></tr>
    <tr valign="top">
        <td >
        <asp:Repeater ID = "R_QuestionnaireEnt" runat="server">
        <ItemTemplate>
        <tr><td align="left" style="width:800px;"><label><%#DataBinder.Eval(Container.DataItem, "q.question1")%></label><asp:HiddenField ID = "HF_question" runat = "server" Value = '<%#DataBinder.Eval(Container.DataItem, "q.idQuestion") %>'/></td><td align="center" style ="width:200px;">
        <asp:RadioButton ID = "RB_AnswerY" runat = "server" Enabled = '<%#!this.IsReadOnly%>' GroupName = "RB_Answer" Text = "Sim" Checked ='<%#(DataBinder.Eval(Container.DataItem, "aj")!= null &&(DataBinder.Eval(Container.DataItem, "aj.answer1").Equals("Yes"))?true:false)%>'/> 
	    <asp:RadioButton ID = "RB_AnswerN" runat = "server" Enabled = '<%#!this.IsReadOnly%>' GroupName ="RB_Answer" Text = "Nao" Checked ='<%#(DataBinder.Eval(Container.DataItem, "aj")!=null &&(DataBinder.Eval(Container.DataItem, "aj.answer1").Equals("No"))?true:false) %>' />
	    <asp:RadioButton ID = "RB_AnswerNA" runat = "server" Enabled = '<%#!this.IsReadOnly%>' GroupName ="RB_Answer" Text = "N/A" Checked ='<%#(DataBinder.Eval(Container.DataItem, "aj")!=null &&(DataBinder.Eval(Container.DataItem, "aj.answer1").Equals("NA"))?true:false) %>' />
	    <asp:HiddenField ID = "HF_answer" runat ="server" Value='<%#((DataBinder.Eval(Container.DataItem, "aj")!= null)?DataBinder.Eval(Container.DataItem, "aj.idAnswer"):0)%>' />
        </td></tr> 
        </ItemTemplate>
        </asp:Repeater>
        </td></tr>
</table>
</div>
<div >
<div >
        <table>
        <tr><td><br /><span class="lblBold" >Comportamento do cliente com a banca e BPN</span></td></tr>
        <tr valign="top">
        <td >
        <asp:Repeater ID = "R_QuestionnaireClnt" runat="server">
        <ItemTemplate>
        <tr><td align ="left" style ="width:800px;"><label><%#DataBinder.Eval(Container.DataItem, "q.question1")%></label><asp:HiddenField ID = "HF_question" runat = "server" Value = '<%#DataBinder.Eval(Container.DataItem, "q.idQuestion") %>'/></td><td align ="center" style="width:200px;">
        <asp:RadioButton ID = "RB_AnswerY" runat = "server" Enabled = '<%#!this.IsReadOnly%>'  GroupName = "RB_Answer" Text = "Sim" Checked ='<%#(DataBinder.Eval(Container.DataItem, "aj")!= null &&(DataBinder.Eval(Container.DataItem, "aj.answer1").Equals("Yes"))?true:false)%>'/> 
	    <asp:RadioButton ID = "RB_AnswerN" runat = "server" Enabled = '<%#!this.IsReadOnly%>' GroupName ="RB_Answer" Text = "Nao" Checked ='<%#(DataBinder.Eval(Container.DataItem, "aj")!=null &&(DataBinder.Eval(Container.DataItem, "aj.answer1").Equals("No"))?true:false) %>' /> 
	    <asp:RadioButton ID = "RB_AnswerNA" runat = "server" Enabled = '<%#!this.IsReadOnly%>' GroupName ="RB_Answer" Text = "N/A" Checked ='<%#(DataBinder.Eval(Container.DataItem, "aj")!=null &&(DataBinder.Eval(Container.DataItem, "aj.answer1").Equals("NA"))?true:false) %>' />
	    <asp:HiddenField ID = "HF_answer" runat ="server" Value='<%#((DataBinder.Eval(Container.DataItem, "aj")!= null)?DataBinder.Eval(Container.DataItem, "aj.idAnswer"):0)%>' />
        </td></tr> 
        </ItemTemplate>
        </asp:Repeater>
        </td></tr>
</table>
</div>
</div>
</div>
<div class="padBottom">&nbsp;</div>