﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using SpiritucSI.BPN.AII.WebSite.UserControls.Workflow;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class ClientDetailEntity : System.Web.UI.UserControl
    {
        public bool isSaveVisible { get; set; }

        #region PROPS
        private int v_idPartySelected;

        public int idPartySelected
        {

            get
            {
                if (v_idPartySelected == 0)
                {
                    int id = 0;
                    if (Session["idParty"] != null)
                    {
                        Int32.TryParse(Session["idParty"].ToString(), out id);
                        if (id != 0)
                            v_idPartySelected = id;
                    }
                    else
                        this.PageRedirect();

                }

                return v_idPartySelected;
            }

            set
            {
                v_idPartySelected = value;

            }
        }

        private void PageRedirect()
        {
            this.Response.Redirect("~/IndividualImparity/Dashboard.aspx");
        }

        protected double[] sumOperations { get; set; }
        protected double[] impValues { get; set; }
         #endregion


        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.Load(0, GV_Operations.PageSize + 1);

        }

        protected void Load(int index, int numrows)
        {
            BLL ds = new BLL();
            var party = ds.GetPartyById(idPartySelected);
            DV_Client.DataSource = party;
            DV_Client.DataBind();
            var operations = (ds.GetContractsByParty(idPartySelected, 0,0));
            impValues= ds.GetPartyImpInfoById(idPartySelected);
            Pnl_impV.DataBind();
            GV_Operations.DataSource = operations;
            sumOperations = ds.GetContractsSumsByParty(idPartySelected);
            GV_Operations.DataBind();

            
        }

        protected void GV_Operations_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.GV_Operations.PageIndex = e.NewPageIndex;
                this.Load(0,0);
            }
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
                {
                    //this.ShowMessage(GetLocalResource("Messages.Error.C_gv_List_PageIndexChanging"), MessageType.ErrorBox);
                }
            }
        }
    }
}