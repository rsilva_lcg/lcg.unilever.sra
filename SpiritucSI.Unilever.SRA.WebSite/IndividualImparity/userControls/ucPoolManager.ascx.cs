﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.WebSite.Common;
using System.Collections;
using SpiritucSI.BPN.AII.Business.Services;
using System.Data;
using SpiritucSI.BPN.AII.Workflow;
using System.Workflow.Activities;
using SpiritucSI.BPN.AII.WebSite.Masters;
using System.Text;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class ucPoolManager : SuperControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                this.LoadGrid();
            }
        }    

        #region CONTROL_EVENTS

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int count = 0;
            int sucess = 0;
            SpiritucSI.BPN.AII.Business.OtherClasses.BLL bll = new SpiritucSI.BPN.AII.Business.OtherClasses.BLL();
            StringBuilder sb = new StringBuilder();
           
            foreach (Obout.Grid.GridRow row in grid1.Rows)
            {
               
                DropDownList ddl = (DropDownList)row.Cells[5].FindControl("ddl");
                if (ddl.SelectedValue != "0")
                {
                    count++;
                    try
                    {                       
                        bll.UpdatePartyAnalistById(Convert.ToInt32(ddl.Attributes["PartyID"]),ddl.SelectedValue);
                        Guid id = bll.GetPartySelectedById(Convert.ToInt32(ddl.Attributes["PartyID"])).FirstOrDefault().idWorkflow.GetValueOrDefault();

                        WFEvents wfEvents  = (WFEvents)this.WFRuntime.GetService(typeof(WFEvents));
                        wfEvents.AllocateAnalyst(id);
                        
                        this.RunWorkflow(id);
                        sucess++;
                    }
                    catch (Exception ex)
                    {
                        sb.Append(string.Format("Erro na alocação do Analista {0} <br />", ddl.SelectedItem.Text));
                    }  
                }
            }

            this.redirect.Value = "true";
            if (count != sucess)
            {
                ((FrontOffice)this.Page.Master).ShowMessage(sb.ToString(), null, SpiritucSI.BPN.AII.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Warning);
            }
            else
            {
                ((FrontOffice)this.Page.Master).ShowMessage("Analistas alocados com sucesso", null, SpiritucSI.BPN.AII.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Success);
            }
            
        }

        protected void grid1_RowDataBound(object sender, Obout.Grid.GridRowEventArgs e)
        {
            string group = ((DataRowView)e.Row.DataItem)["GroupName"].ToString();
            string userName  = ((DataRowView)e.Row.DataItem)["username"].ToString();

            DropDownList ddl = (DropDownList)e.Row.Cells[5].FindControl("ddl");
            ddl.DataSource = UserService.GetUsersByGroupName(group);
            ddl.DataBind();
            if (userName != "")
                ddl.SelectedValue = userName; 

        }

        protected void grid_Select(object sender, Obout.Grid.GridRecordEventArgs e)
        {
            Obout.Grid.Grid grid1 = (Obout.Grid.Grid)sender;

            if (grid1.SelectedRecords != null)
            {
                foreach (Hashtable oRecord in grid1.SelectedRecords)
                {

                    Session["idParty"] = oRecord["Id"];
                    this.Response.Redirect("~/IndividualImparity/ClientDetail.aspx");
                }

            }

        }

        protected void Details_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;

            if (lnk != null)
            {
                Session["idParty"] = lnk.CommandArgument;
                this.Response.Redirect("~/IndividualImparity/ClientDetail.aspx");
                
            }
        }

        #endregion

        #region METHODS
        private void LoadGrid()
        {
            this.grid1.DataSource = PartyServices.GetPartyFor(this.CurrentUser, "InAllocation");
            this.grid1.DataBind();
        }

        public string GetRedirect()
        {
            return this.redirect.ClientID;
        }
        #endregion
    }
}