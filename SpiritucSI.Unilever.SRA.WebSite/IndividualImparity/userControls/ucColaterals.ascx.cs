﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class ucColaterals : System.Web.UI.UserControl
    {

        #region PROPS
        private int v_idPartySelected;

        public int idPartySelected
        {

            get
            {
                if (v_idPartySelected == 0)
                {


                    if (Session["idParty"] == null)
                        this.Response.Redirect("~/Dashboard.aspx");
                    else
                    {
                        int id = 0;
                        Int32.TryParse(Session["idParty"].ToString(), out id);
                        if (id != 0)
                            v_idPartySelected = id;
                    }
                }

                return v_idPartySelected;
            }

            set
            {
                v_idPartySelected = value;

            }
        }

        public bool IsReadOnly { get; set; }

        //public TextBox type { get; set; }
        public TextBox description { get; set; }
        public TextBox value { get; set; }
        public TextBox date { get; set; }
        public TextBox valueSale { get; set; }
        public TextBox evaluators { get; set; }
        public TextBox valuepers { get; set; }
        public TextBox dateEdt{ get; set; }
        public TextBox valuepersEdt { get; set; }


        protected double[] sumValues { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.Load();
            }
        }


        private void LoadScriptPersonal()
        {
            if (this.evaluators != null)
            {
                this.evaluators.Attributes.Add("onfocus", "return ClearTextBox('" + this.evaluators.ClientID + "'," + "true, '" + "Avalistas" + "')");
                this.evaluators.Attributes.Add("onblur", "return ClearTextBox('" + this.evaluators.ClientID + "'," + "false, '" + "Avalistas" + "')");
            }
            if (this.valuepers != null)
            {
                this.valuepers.Attributes.Add("onfocus", "return ClearTextBox('" + this.valuepers.ClientID + "'," + "true, '" + "Valor" + "')");
                this.valuepers.Attributes.Add("onblur", "return ClearTextBox('" + this.valuepers.ClientID + "'," + "false, '" + "Valor" + "')");
            }
        }

        private void LoadScript()
        {
            //if (this.type != null)
            //{
            //    this.type.Attributes.Add("onfocus", "return ClearTextBox('" + this.type.ClientID + "'," + "true, '" + "Tipo" + "')");
            //    this.type.Attributes.Add("onblur", "return ClearTextBox('" + this.type.ClientID + "'," + "false, '" + "Tipo" + "')");
            //}
            if (this.description != null)
            {
                this.description.Attributes.Add("onfocus", "return ClearTextBox('" + this.description.ClientID + "'," + "true, '" + "Descrição Garantia" + "')");
                this.description.Attributes.Add("onblur", "return ClearTextBox('" + this.description.ClientID + "'," + "false, '" + "Descrição Garantia" + "')");
            }
            if (this.value != null)
            {
                this.value.Attributes.Add("onfocus", "return ClearTextBox('" + this.value.ClientID + "'," + "true, '" + "Valor" + "')");
                this.value.Attributes.Add("onblur", "return ClearTextBox('" + this.value.ClientID + "'," + "false, '" + "Valor" + "')");
            }
            if (this.date != null)
            {
                this.date.Attributes.Add("onfocus", "return ClearTextBox('" + this.date.ClientID + "'," + "true, '" + "Data de Avaliação" + "')");
                this.date.Attributes.Add("onblur", "return ClearTextBox('" + this.date.ClientID + "'," + "false, '" + "Data de Avaliação" + "')");
            }
            if (this.valueSale != null)
            {
                this.valueSale.Attributes.Add("onfocus", "return ClearTextBox('" + this.valueSale.ClientID + "'," + "true, '" + "Valor de Venda Forçada" + "')");
                this.valueSale.Attributes.Add("onblur", "return ClearTextBox('" + this.valueSale.ClientID + "'," + "false, '" + "Valor de Venda Forçada" + "')");
            }
            if (this.dateEdt != null)
            {
                this.dateEdt.Attributes.Add("onfocus", "return ClearTextBox('" + this.dateEdt.ClientID + "'," + "true, '" + "Data de Avaliação" + "')");
                this.dateEdt.Attributes.Add("onblur", "return ClearTextBox('" + this.dateEdt.ClientID + "'," + "false, '" + "Data de Avaliação" + "')");
            }
            if (this.valuepersEdt != null)
            {
                this.valuepersEdt.Attributes.Add("onfocus", "return ClearTextBox('" + this.valuepersEdt.ClientID + "'," + "true, '" + "Valor de Venda Forçada" + "')");
                this.valuepersEdt.Attributes.Add("onblur", "return ClearTextBox('" + this.valuepersEdt.ClientID + "'," + "false, '" + "Valor de Venda Forçada" + "')");
            }
        }



        protected void Load()
        {
            BLL ds = new BLL();

            GV_Colaterals_Real.DataSource = ds.GetColateralsRealByParty(idPartySelected, 10, 0);
            GV_Colaterals_Real.DataBind();

            GV_Colaterals_Personal.DataSource = ds.GetColateralsPersonalByParty(idPartySelected, 10, 0);

            sumValues = ds.GetColateralsSumsByParty(idPartySelected);
            Pnl_Sum.DataBind();
            GV_Colaterals_Personal.DataBind();
            if (IsReadOnly)
                LoadReadOnly(this);

        }

        protected void LoadReadOnly(Control c)
        {
            foreach (Control child in c.Controls)
            {
                this.LoadReadOnly(child);
            }

            if (c is CheckBox)
                ((CheckBox)c).Enabled = !this.IsReadOnly;
            if (c is TextBox)
                ((TextBox)c).Enabled = !this.IsReadOnly;
            if (c is RadioButton)
                ((RadioButton)c).Enabled = !this.IsReadOnly;
            if (c is Button)
                ((Button)c).Enabled = !this.IsReadOnly;
            if (c is LinkButton)
                ((LinkButton)c).Enabled = !this.IsReadOnly;

        }


        private object populateContracts()
        {
            BLL ds = new BLL();
            //adicionar logica para retornar todos os contractos
            return ds.GetContractsByParty(idPartySelected, 1000, 0);

        }




        #region GV_ColateralReal

        public void GV_Colaterals_Real_RowEditing(object sender, GridViewEditEventArgs e)
        {
            this.GV_Colaterals_Real.EditIndex = e.NewEditIndex;
            this.Load();
            
        }

        public void GV_Colaterals_Real_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GV_Colaterals_Real.EditIndex = -1;
            this.Load();
        }



        public void GV_Colaterals_Real_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int indx = 0;
            if (int.TryParse(e.RowIndex.ToString(), out indx))
            {
                int idColateral = int.Parse(this.GV_Colaterals_Real.DataKeys[e.RowIndex].Value.ToString());

                GridViewRow gvR = GV_Colaterals_Real.Rows[indx];
                DropDownList DL_Contract = (DropDownList)gvR.Cells[0].Controls[1];
                //TextBox type = ((TextBox)gvR.Cells[1].Controls[1]);
                DropDownList dl_type = (DropDownList)gvR.FindControl("DL_Type");
                TextBox description = ((TextBox)gvR.Cells[2].Controls[1]);
                TextBox evalValue = ((TextBox)gvR.Cells[3].Controls[1]);
                TextBox evalDate = ((TextBox)gvR.Cells[4].Controls[1]);
                TextBox forcedSale = ((TextBox)gvR.Cells[5].Controls[1]);
                


                if (Page.IsValid)
                {
                    double? fSale = null;
                    if (forcedSale.Text != "")
                        fSale = double.Parse(forcedSale.Text);
                    //if (!forcedSale.Attributes["disabled"].Equals("True"))
                    //    fSale = double.Parse(forcedSale.Text);
                    //if (evalDate.Attributes["disabled"].Equals("True"))
                    //    evalDate.Text = "01-01-1900";

                    if (evalDate.Text == "")
                        evalDate.Text = "01-01-1900";
                    try
                    {
                        BLL ds = new BLL();
                        ds.UpdateColateralReal(null, idColateral, Int32.Parse(DL_Contract.SelectedValue), dl_type.SelectedValue, description.Text, double.Parse(evalValue.Text), evalDate.Text != "01-01-1900" ? DateTime.Parse(evalDate.Text) : DateTime.MinValue, fSale);
                    }
                    catch (Exception ex)
                    {
                        if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
                    }
                }
            }
            GV_Colaterals_Real.EditIndex = -1;
            this.Load();
        }

        public void GV_Colaterals_Real_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int indx = 0;
                if (int.TryParse(e.RowIndex.ToString(), out indx))
                {
                    int idColateral = int.Parse(this.GV_Colaterals_Real.DataKeys[e.RowIndex].Value.ToString());
                    BLL ds = new BLL();
                    ds.DeleteColateralReal(idColateral, null);
                    this.Load();

                }
            }
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
            }

        }

        protected void Btt_InsertColateralR_Click(object sender, EventArgs e)
        {
            try
            {
                DropDownList DL_Contract = (DropDownList)GV_Colaterals_Real.FooterRow.Cells[0].Controls[1];
                DropDownList type = (DropDownList)GV_Colaterals_Real.FooterRow.FindControl("DL_Type");
                //TextBox type = ((TextBox)GV_Colaterals_Real.FooterRow.Cells[1].Controls[1]);
                TextBox description = ((TextBox)GV_Colaterals_Real.FooterRow.Cells[2].Controls[1]);
                TextBox evalValue = ((TextBox)GV_Colaterals_Real.FooterRow.Cells[3].Controls[1]);
                TextBox evalDate = ((TextBox)GV_Colaterals_Real.FooterRow.Cells[4].Controls[1]);
                TextBox forcedSale = ((TextBox)GV_Colaterals_Real.FooterRow.Cells[5].Controls[1]);


                if (Page.IsValid)
                {
                    double? fSale = null;
                    if (forcedSale.Text != "")
                        fSale = double.Parse(forcedSale.Text);
                    //if (!forcedSale.Attributes["disabled"].Equals("True"))
                    //    fSale = double.Parse(forcedSale.Text);
                    //if (evalDate.Attributes["disabled"].Equals("True"))
                    //    evalDate.Text = "01-01-1900";

                    if (evalDate.Text == "")
                        evalDate.Text = "01-01-1900";


                if (DL_Contract != null && type != null && description != null)
                {
                        BLL ds = new BLL();
                        ds.InsertColateralReal(null, idPartySelected, Int32.Parse(DL_Contract.SelectedValue), type.SelectedValue, description.Text, double.Parse(evalValue.Text), evalDate.Text!="01-01-1900"?DateTime.Parse(evalDate.Text):DateTime.MinValue, fSale);
                        this.Load();
                    }

                }
                else
                {
                    if (sender != null && sender is Button && ((Button)sender).ID.Equals("Btt_InsertC"))
                    {
                        Button Btt_InsertC = (Button)sender;
                        Btt_InsertC.Visible = true;
                        GV_Colaterals_Real.FooterRow.Cells[6].Controls.Add(Btt_InsertC);
                    }
                    if (type != null)
                    {
                        evalDate.Style["display"] = (int.Parse(type.SelectedValue) > 8 || type.SelectedValue == "") ? "none" : "block";
                        forcedSale.Style["display"] = (int.Parse(type.SelectedValue) > 8 || type.SelectedValue == "") ? "none" : "block";
                    }
                }
            }
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
            }
        }

        protected void GV_Colaterals_Real_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType.Equals(DataControlRowType.Footer))
            {
                Button Btt_InsertC = (Button)e.Row.Cells[0].FindControl("Btt_InsertC");
                Btt_InsertC.Visible = true;
                e.Row.Cells[6].Controls.Add(Btt_InsertC);
            }

            if (e.Row.FindControl("DL_ContractsG") != null && e.Row.FindControl("DL_ContractsG") is DropDownList && (e.Row.RowType.Equals(DataControlRowType.DataRow)||e.Row.RowType.Equals(DataControlRowType.EmptyDataRow)) )
            {
                ((DropDownList)e.Row.FindControl("DL_ContractsG")).DataSource = this.populateContracts();
                ((DropDownList)e.Row.FindControl("DL_ContractsG")).DataValueField = "idContractS";
                ((DropDownList)e.Row.FindControl("DL_ContractsG")).DataTextField = "contractRef";

                ((DropDownList)e.Row.FindControl("DL_ContractsG")).DataBind();
                ListItem li = new ListItem("Garantia genérica","0"); 
                ((DropDownList)e.Row.FindControl("DL_ContractsG")).Items.Insert(0, li);

                if (e.Row.RowType.Equals(DataControlRowType.DataRow) && ((HiddenField)e.Row.FindControl("HF_idContract")).Value != "")
                    ((DropDownList)e.Row.FindControl("DL_ContractsG")).SelectedValue = ((HiddenField)e.Row.FindControl("HF_idContract")).Value;

            }

            if (e.Row.FindControl("DL_Type") != null && e.Row.FindControl("DL_Type") is DropDownList && (e.Row.RowType.Equals(DataControlRowType.DataRow) || e.Row.RowType.Equals(DataControlRowType.EmptyDataRow)))
            {
                if (e.Row.FindControl("HF_Type") != null && e.Row.FindControl("HF_Type") is HiddenField && (e.Row.RowType.Equals(DataControlRowType.DataRow) || e.Row.RowType.Equals(DataControlRowType.EmptyDataRow)))
                {
                    HiddenField hf = (HiddenField)e.Row.FindControl("HF_Type");
                    if (((DropDownList)e.Row.FindControl("DL_Type")).SelectedValue != "")
                    {
                        ((DropDownList)e.Row.FindControl("DL_Type")).SelectedValue = hf.Value;
                        if (e.Row.RowIndex != GV_Colaterals_Real.EditIndex)
                        {
                            ((DropDownList)e.Row.FindControl("DL_Type")).Attributes.Add("disabled", "true");
                        }
                    }
                }
            
            }





            //if (e.Row.FindControl("Tbx_type") != null && e.Row.FindControl("Tbx_type") is TextBox && e.Row.RowType.Equals(DataControlRowType.EmptyDataRow))
            //{
            //    type = (TextBox)e.Row.FindControl("Tbx_type");
            //}
            if (e.Row.FindControl("Tbx_description") != null && e.Row.FindControl("Tbx_description") is TextBox && e.Row.RowType.Equals(DataControlRowType.EmptyDataRow))
            {
                description = (TextBox)e.Row.FindControl("Tbx_description");
            }
            if (e.Row.FindControl("Tbx_value") != null && e.Row.FindControl("Tbx_value") is TextBox && e.Row.RowType.Equals(DataControlRowType.EmptyDataRow))
            {
                value = (TextBox)e.Row.FindControl("Tbx_value");
            }
            if (e.Row.FindControl("Tbx_avalDate") != null && e.Row.FindControl("Tbx_avalDate") is TextBox && e.Row.RowType.Equals(DataControlRowType.EmptyDataRow))
            {
                date = (TextBox)e.Row.FindControl("Tbx_avalDate");
            }
            if (e.Row.FindControl("Tbx_forcedSale") != null && e.Row.FindControl("Tbx_forcedSale") is TextBox && e.Row.RowType.Equals(DataControlRowType.EmptyDataRow))
            {
                valueSale = (TextBox)e.Row.FindControl("Tbx_forcedSale");
            }

            //if (e.Row.FindControl("TextBox2") != null && e.Row.FindControl("TextBox2") is TextBox && e.Row.RowType.Equals(DataControlRowType.Footer))
            //{
            //    type = (TextBox)e.Row.FindControl("TextBox2");
            //}
            if (e.Row.FindControl("TextBox4") != null && e.Row.FindControl("TextBox4") is TextBox && e.Row.RowType.Equals(DataControlRowType.Footer))
            {
                description = (TextBox)e.Row.FindControl("TextBox4");
            }
            if (e.Row.FindControl("TextBox6") != null && e.Row.FindControl("TextBox6") is TextBox && e.Row.RowType.Equals(DataControlRowType.Footer) )
            {
                value = (TextBox)e.Row.FindControl("TextBox6");
            }
            if (e.Row.FindControl("TextBox8") != null && e.Row.FindControl("TextBox8") is TextBox && e.Row.RowType.Equals(DataControlRowType.Footer) )
            {
                date = (TextBox)e.Row.FindControl("TextBox8");
            }
            if (e.Row.FindControl("TextBox9") != null && e.Row.FindControl("TextBox9") is TextBox && e.Row.RowType.Equals(DataControlRowType.Footer) )
            {
                valueSale = (TextBox)e.Row.FindControl("TextBox9");
            }
            if (e.Row.FindControl("TextBox7") != null && e.Row.FindControl("TextBox7") is TextBox && e.Row.RowType.Equals(DataControlRowType.DataRow) && e.Row.RowIndex == GV_Colaterals_Real.EditIndex)
            {
                dateEdt = (TextBox)e.Row.FindControl("TextBox7");
            }
            if (e.Row.FindControl("TextBox9") != null && e.Row.FindControl("TextBox9") is TextBox && e.Row.RowType.Equals(DataControlRowType.DataRow) && e.Row.RowIndex == GV_Colaterals_Real.EditIndex)
            {
                valuepersEdt = (TextBox)e.Row.FindControl("TextBox9");
            }


            if ( this.description != null && this.value != null && this.date != null && this.valueSale != null)
            {
                this.LoadScript();
            }
            if (this.dateEdt != null && this.valuepersEdt != null)
            {
                this.LoadScript();
            }

            if (e.Row.FindControl("DL_Type") != null && e.Row.FindControl("DL_Type") is DropDownList && (e.Row.RowType.Equals(DataControlRowType.DataRow) || e.Row.RowType.Equals(DataControlRowType.EmptyDataRow) || e.Row.RowType.Equals(DataControlRowType.Footer)))
            {
                DropDownList dl = (DropDownList)e.Row.FindControl("DL_Type");
                HiddenField hf = (HiddenField)e.Row.FindControl("HF_Type");
                //EditTemplate
                RequiredFieldValidator rv = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorData2");
                RequiredFieldValidator rv2 = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorValorVenda2");
                TextBox t1 = (TextBox)e.Row.FindControl("TextBox7");
                TextBox tf2 = (TextBox)e.Row.FindControl("TextBox9");
                //FooterTemplate
                RequiredFieldValidator rvf = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorData");
                RequiredFieldValidator rvf2 = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorValorVenda");
                TextBox t1f = (TextBox)e.Row.FindControl("TextBox8");
                TextBox t2f2 = (TextBox)e.Row.FindControl("TextBox9");
                //EmptyTemplate
                RequiredFieldValidator rve = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorData1");
                RequiredFieldValidator rve2 = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorValorVenda1");
                TextBox t1e = (TextBox)e.Row.FindControl("Tbx_avalDate");
                TextBox t2e2 = (TextBox)e.Row.FindControl("Tbx_forcedSale");
                //ItemTemplate
                Label l = (Label)e.Row.FindControl("Label7");
                Label l2 = (Label)e.Row.FindControl("Label8");




                if (l != null && l2 != null)
                {
                    l.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";
                    l2.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";

                }



                if (rv != null && rv2 != null)
                {
                    ((DropDownList)e.Row.FindControl("DL_Type")).Attributes.Add("onchange", "return DisableValidators('" + ((DropDownList)e.Row.FindControl("DL_Type")).ClientID + "','" + rv.ClientID + "','" + rv2.ClientID + "','" + "8" + "','" + t1.ClientID + "','" + tf2.ClientID + "')");

                    //t1.Attributes.Add("style.visibility", (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "hidden" : "visible");
                    t1.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";
                    //tf2.Attributes.Add("style.visibility", (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "hidden" : "visible");
                    tf2.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";

                    if (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "")
                    {
                        t1.Text = "01-01-1900"; 
                        tf2.Text = "0";
                    }
                    else
                    {
                        if (t1.Text == "" || tf2.Text == "")
                        {
                            t1.Text = "Data de Avaliação";
                            tf2.Text = "Valor de Venda Forçada";
                        }
                    }
                }
                if (rvf != null && rvf2 != null)
                {
                    ((DropDownList)e.Row.FindControl("DL_Type")).Attributes.Add("onchange", "return DisableValidators('" + ((DropDownList)e.Row.FindControl("DL_Type")).ClientID + "','" + rvf.ClientID + "','" + rvf2.ClientID + "','" + "8" + "','" + t1f.ClientID + "','" + t2f2.ClientID + "')");
                    //t1f.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "hidden" : "visible");
                    t1f.Style["display"] =  (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";
                    //t2f2.Attributes.Add("style.visibility", (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "hidden" : "visible");
                    t2f2.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";


                    if (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "")
                    {
                        t1f.Text = "01-01-1900";
                        t2f2.Text = "0";
                    }
                    else
                    {
                        t1f.Text = "Data de Avaliação";
                        t2f2.Text = "Valor de Venda Forçada";
                    }
                }
                if (rve != null && rve2 != null)
                {
                    ((DropDownList)e.Row.FindControl("DL_Type")).Attributes.Add("onchange", "return DisableValidators('" + ((DropDownList)e.Row.FindControl("DL_Type")).ClientID + "','" + rve.ClientID + "','" + rve2.ClientID + "','" + "8" + "','" + t1e.ClientID + "','" + t2e2.ClientID + "')");
                    //t1e.Attributes.Add("style.visibility", (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "hidden" : "visible");
                    t1e.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";
                    //t2e2.Attributes.Add("style.visibility", (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "hidden" : "visible");
                    t2e2.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";

                    
                    t1e.DataBind(); t2e2.DataBind();
                        if (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "")
                        {
                            t1e.Text = "01-01-1900";
                            t2e2.Text = "0";
                        }
                        else
                        {
                            t1e.Text = "Data de Avaliação";
                            t2e2.Text = "Valor de Venda Forçada";
                        }


                }

            }
        }


        protected void GV_Colaterals_Real_DataBound(object sender, EventArgs e)
        {
            if (GV_Colaterals_Real.Rows.Count > 0)
            {

                ((DropDownList)GV_Colaterals_Real.FooterRow.FindControl("DL_ContractsGF")).DataSource = this.populateContracts();
                ((DropDownList)GV_Colaterals_Real.FooterRow.FindControl("DL_ContractsGF")).DataValueField = "idContractS";
                ((DropDownList)GV_Colaterals_Real.FooterRow.FindControl("DL_ContractsGF")).DataTextField = "contractRef";

                ((DropDownList)GV_Colaterals_Real.FooterRow.FindControl("DL_ContractsGF")).DataBind();
                ListItem li = new ListItem("Garantia genérica", "0");
                ((DropDownList)GV_Colaterals_Real.FooterRow.FindControl("DL_ContractsGF")).Items.Insert(0, li);

            }

        }


        #endregion



        #region GV_ColateralPersonal

        public void GV_Colaterals_Personal_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GV_Colaterals_Personal.EditIndex = e.NewEditIndex;
            this.Load();
        }

        public void GV_Colaterals_Personal_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GV_Colaterals_Personal.EditIndex = -1;
            this.Load();
        }



        public void GV_Colaterals_Personal_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                int indx = 0;
                if (int.TryParse(e.RowIndex.ToString(), out indx))
                {
                    int idColateral = int.Parse(this.GV_Colaterals_Personal.DataKeys[e.RowIndex].Value.ToString());
                    GridViewRow gvR = GV_Colaterals_Personal.Rows[indx];
                    DropDownList DL_Contract = ((DropDownList)gvR.Cells[0].Controls[1]);
                    TextBox evaluators = ((TextBox)gvR.Cells[1].Controls[1]);
                    TextBox value = ((TextBox)gvR.Cells[2].Controls[1]);

                    BLL ds = new BLL();
                    if (Page.IsValid)
                    {
                        ds.UpdateColateralPersonal(null, idColateral, Int32.Parse(DL_Contract.SelectedValue), evaluators.Text, double.Parse(value.Text));
                    }
                }
                GV_Colaterals_Personal.EditIndex = -1;
                this.Load();
            }
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
            }
        }

        public void GV_Colaterals_Personal_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int indx = 0;
                if (int.TryParse(e.RowIndex.ToString(), out indx))
                {
                    int idColateral = int.Parse(this.GV_Colaterals_Personal.DataKeys[e.RowIndex].Value.ToString());
                    BLL ds = new BLL();
                    ds.DeleteColateralPersonal(idColateral, null);
                    this.Load();

                }
            }
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
            }

        }

        protected void Btt_InsertColateralP_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    DropDownList DL_Contract = ((DropDownList)GV_Colaterals_Personal.FooterRow.Cells[0].Controls[1]);
                    TextBox evaluators = ((TextBox)GV_Colaterals_Personal.FooterRow.Cells[1].Controls[1]);
                    TextBox value = ((TextBox)GV_Colaterals_Personal.FooterRow.Cells[2].Controls[1]);

                    BLL ds = new BLL();
                    ds.InsertColateralPersonal(null, idPartySelected, Int32.Parse(DL_Contract.SelectedValue), evaluators.Text, double.Parse(value.Text));
                    this.Load();
                }
                else
                {
                    if (sender != null && sender is Button && ((Button)sender).ID.Equals("Btt_InsertCP"))
                    {
                        Button Btt_InsertCP = (Button)sender;
                        Btt_InsertCP.Visible = true;
                        GV_Colaterals_Personal.FooterRow.Cells[3].Controls.Add(Btt_InsertCP);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
            }
        }



        protected void GV_Colaterals_Personal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType.Equals(DataControlRowType.Footer))
            {
                Button Btt_InsertP = (Button)e.Row.Cells[0].FindControl("Btt_InsertCP");
                Btt_InsertP.Visible = true;
                e.Row.Cells[3].Controls.Add(Btt_InsertP);
            }

            if (e.Row.FindControl("DL_ContractsG") != null && e.Row.FindControl("DL_ContractsG") is DropDownList && (e.Row.RowType.Equals(DataControlRowType.DataRow)|| e.Row.RowType.Equals(DataControlRowType.EmptyDataRow)))
            {
                ((DropDownList)e.Row.FindControl("DL_ContractsG")).DataSource = this.populateContracts();
                ((DropDownList)e.Row.FindControl("DL_ContractsG")).DataValueField = "idContractS";
                ((DropDownList)e.Row.FindControl("DL_ContractsG")).DataTextField = "contractRef";

                ((DropDownList)e.Row.FindControl("DL_ContractsG")).DataBind();
                if (e.Row.RowType.Equals(DataControlRowType.DataRow) && ((HiddenField)e.Row.FindControl("HF_idContract")).Value != "")
                    ((DropDownList)e.Row.FindControl("DL_ContractsG")).SelectedValue = ((HiddenField)e.Row.FindControl("HF_idContract")).Value;
                
                ListItem li = new ListItem("Garantia genérica", "0");
                ((DropDownList)e.Row.FindControl("DL_ContractsG")).Items.Insert(0, li);

            }

            if (e.Row.FindControl("Tbx_Evaluators") != null && e.Row.FindControl("Tbx_Evaluators") is TextBox && e.Row.RowType.Equals(DataControlRowType.EmptyDataRow))
            {
                evaluators = (TextBox)e.Row.FindControl("Tbx_Evaluators");
            }
            if (e.Row.FindControl("Tbx_Value") != null && e.Row.FindControl("Tbx_Value") is TextBox && e.Row.RowType.Equals(DataControlRowType.EmptyDataRow))
            {
                valuepers = (TextBox)e.Row.FindControl("Tbx_Value");
            }

            if (e.Row.FindControl("TextBox11") != null && e.Row.FindControl("TextBox11") is TextBox && e.Row.RowType.Equals(DataControlRowType.Footer))
            {
                evaluators = (TextBox)e.Row.FindControl("TextBox11");
            }
            if (e.Row.FindControl("TextBox13") != null && e.Row.FindControl("TextBox13") is TextBox && e.Row.RowType.Equals(DataControlRowType.Footer))
            {
                valuepers = (TextBox)e.Row.FindControl("TextBox13");
            }

            if (this.evaluators != null && this.valuepers != null)
            {
                this.LoadScriptPersonal();
            }
        }

        protected void GV_Colaterals_Personal_DataBound(object sender, EventArgs e)
        {
            if (GV_Colaterals_Personal.Rows.Count > 0)
            {

                ((DropDownList)GV_Colaterals_Personal.FooterRow.FindControl("DL_ContractsGF")).DataSource = this.populateContracts();
                ((DropDownList)GV_Colaterals_Personal.FooterRow.FindControl("DL_ContractsGF")).DataValueField = "idContractS";
                ((DropDownList)GV_Colaterals_Personal.FooterRow.FindControl("DL_ContractsGF")).DataTextField = "contractRef";


                ((DropDownList)GV_Colaterals_Personal.FooterRow.FindControl("DL_ContractsGF")).DataBind();
                ListItem li = new ListItem("Garantia genérica", "0");
                ((DropDownList)GV_Colaterals_Personal.FooterRow.FindControl("DL_ContractsGF")).Items.Insert(0, li);

            }
        }


        protected void GV_ColateralsReal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("InsertEmptyItem"))
            {
                
                GridViewRow row = (GridViewRow)((e.CommandSource as Button).NamingContainer);
                DropDownList contract = (DropDownList)row.FindControl("DL_ContractsG");
                DropDownList type = (DropDownList)row.FindControl("DL_Type");
                //TextBox type = (TextBox)row.FindControl("Tbx_type");
                
                TextBox description = ((TextBox)row.FindControl("Tbx_description"));
                TextBox value = ((TextBox)row.FindControl("Tbx_value"));
                TextBox avalDate = ((TextBox)row.FindControl("Tbx_avalDate"));
                TextBox forcedSale = ((TextBox)row.FindControl("Tbx_forcedSale"));


                if (Page.IsValid)
                {
                    double? fSale = null;
                    if (forcedSale.Text != "")
                        fSale = double.Parse(forcedSale.Text);
                    //if (!forcedSale.Attributes["disabled"].Equals("True"))
                    //    fSale = double.Parse(forcedSale.Text);
                    //if (avalDate.Attributes["disabled"].Equals("True"))
                    //    avalDate.Text = "01-01-1900";

                    if (avalDate.Text == "")
                        avalDate.Text = "01-01-1900";

                if (contract != null && type != null && description != null && value != null && avalDate != null)
                {
                        try
                        {

                            BLL ds = new BLL();
                            ds.InsertColateralReal(null, idPartySelected, Int32.Parse(contract.SelectedValue), type.SelectedValue, description.Text, double.Parse(value.Text), avalDate.Text != "01-01-1900" ? DateTime.Parse(avalDate.Text) : DateTime.MinValue, fSale);
                            this.Load();
                        }
                        catch (Exception ex)
                        {
                            if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
                        }
                    }
                }
                else
                {
                    if (sender != null && sender is Button && ((Button)sender).ID.Equals("Btt_InsertC"))
                    {
                        Button Btt_InsertCP = (Button)sender;
                        Btt_InsertCP.Visible = true;
                        GV_Colaterals_Personal.FooterRow.Cells[3].Controls.Add(Btt_InsertCP);
                    }
                    if (type != null)
                    {
                        avalDate.Style["display"] = (int.Parse(type.SelectedValue) > 8 || type.SelectedValue == "") ? "none" : "block";
                        forcedSale.Style["display"] = (int.Parse(type.SelectedValue) > 8 || type.SelectedValue == "") ? "none" : "block";
                    }
                }
            }

            if (e.CommandName.Equals("Update"))
            {
                GridViewRow row = (GridViewRow)((e.CommandSource as LinkButton).NamingContainer);
                DropDownList dl = ((DropDownList)row.FindControl("DL_Type"));
                TextBox t1 = (TextBox)row.FindControl("TextBox7");
                TextBox tf2 = (TextBox)row.FindControl("TextBox9");
                t1.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";
                tf2.Style["display"] = (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "") ? "none" : "block";

                //if (int.Parse(dl.SelectedValue) > 8 || dl.SelectedValue == "")
                //{
                //    t1.Text = "01-01-1900";
                //    tf2.Text = "0";
                //}
                //else
                //{
                //    t1.Text = "Data de Avaliação";
                //    tf2.Text = "Valor de Venda Forçada";
                //}
            }
        }


        protected void GV_ColateralsPersonal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("InsertEmptyItem"))
            {
                GridViewRow row = (GridViewRow)((e.CommandSource as Button).NamingContainer);
                DropDownList contract = (DropDownList)row.FindControl("DL_ContractsG");
                TextBox evaluators = (TextBox)row.FindControl("Tbx_Evaluators");
                TextBox value = ((TextBox)row.FindControl("Tbx_Value"));

                if (Page.IsValid)
                {
                    try
                    {
                        BLL ds = new BLL();
                        ds.InsertColateralPersonal(null, idPartySelected, Int32.Parse(contract.SelectedValue), evaluators.Text, double.Parse(value.Text));
                        this.Load();
                    }
                    catch (Exception ex)
                    {
                        if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
                    }
                }
            }
        }

        #endregion


        //protected void DL_Type_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DropDownList dl = (DropDownList)sender;
        //        if (dl != null)
        //        {
        //            //EditTemplate
        //            RequiredFieldValidator rv = (RequiredFieldValidator)((GridViewRow)dl.NamingContainer).FindControl("RequiredFieldValidatorData2");
        //            RequiredFieldValidator rv2 = (RequiredFieldValidator)((GridViewRow)dl.NamingContainer).FindControl("RequiredFieldValidatorValorVenda2");
        //            //FooterTemplate
        //            RequiredFieldValidator rvf = (RequiredFieldValidator)((GridViewRow)dl.NamingContainer).FindControl("RequiredFieldValidatorData");
        //            RequiredFieldValidator rvf2 = (RequiredFieldValidator)((GridViewRow)dl.NamingContainer).FindControl("RequiredFieldValidatorValorVenda");
        //            //EmptyTemplate
        //            RequiredFieldValidator rve = (RequiredFieldValidator)((GridViewRow)dl.NamingContainer).FindControl("RequiredFieldValidatorData1");
        //            RequiredFieldValidator rve2 = (RequiredFieldValidator)((GridViewRow)dl.NamingContainer).FindControl("RequiredFieldValidatorValorVenda1");



        //            if (rv != null && rv2 != null)
        //            {
        //                if (dl.SelectedIndex > 8)
        //                { rv.Enabled = false; rv.IsValid = true; rv.DataBind(); rv2.Enabled = false; rv2.IsValid = true; rv2.DataBind(); }
        //                else
        //                { rv.Enabled = true; rv2.Enabled = true; }
        //            }
        //            if (rvf != null && rvf2 != null)
        //            {
        //                if (dl.SelectedIndex < 8)
        //                { rvf.IsValid = true; rvf.Enabled = false; rvf.DataBind(); rvf2.IsValid = true; rvf2.Enabled = false; rvf2.DataBind(); }
        //                else
        //                { rvf.Enabled = true; rvf2.Enabled = true; }
        //            }
        //            if (rve != null && rve2 != null)
        //            {
        //                if (dl.SelectedIndex < 8)
        //                { rve.Enabled = false; rve.IsValid = true; rve.DataBind(); rve2.Enabled = false; rve2.IsValid = true; rve.DataBind(); }
        //                else
        //                { rve.Enabled = true; rve2.Enabled = true; }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
        //    }
        //}


    }
}
