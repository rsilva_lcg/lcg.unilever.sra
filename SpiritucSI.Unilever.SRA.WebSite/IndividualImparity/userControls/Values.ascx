﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Values.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.Values" %>
<%@ Register Src="../../UserControls/Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="uc1" %>
<div class="SubContentCenter">

    <label class="lblBoldHidd">
        Dados</label>
    <br />
       
<div style = "float:left; width:1000px;">
     <div class="padBottom">
    <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Entidade" ShowMessageBox="false" ShowSummary="true" ValidationGroup="Insert" runat="server" /></div>
 <div class="padBottom">
    <asp:ValidationSummary ID="ValidationSummary4" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Entidade" ShowMessageBox="false" ShowSummary="true" ValidationGroup="InsertNew" runat="server" /></div>

    <table>
        <tr>
            <td>
                <label class="lblBold">
                    Entidade</label>
                <br />
            </td>
        </tr>
        <tr valign="top">
            <td class="txtCenter">
                <asp:GridView ID="GV_ValuesParty" runat="server" CssClass="GridView" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" Width="995px" DataKeyNames="idValue"
                    OnRowUpdating="GV_ValuesParty_RowUpdating" OnRowEditing="GV_ValuesParty_RowEditing"
                    OnRowCancelingEdit="GV_ValuesParty_RowCancelingEdit" ShowFooter="true" OnRowDeleting="GV_ValuesParty_RowDeleting"
                    OnRowDataBound="GV_ValuesParty_RowDataBound" OnRowCommand="GV_ValuesAval_RowCommand">
                    <HeaderStyle CssClass="GridViewHeader" />
                    <RowStyle CssClass="GridViewRow" />
                    <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
                    <SelectedRowStyle CssClass="GridViewSelectedRow" />
                    <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />
                    <EmptyDataTemplate>
                        <table rules="all" cellspacing="0" border="1" style="width: 600px; border-collapse: collapse;">
                            <tr>
                                <th>
                                    <span>Descrição</span>
                                </th>
                                <th>
                                    <span>Valor</span>
                                </th>
                                <th>
                                    <span>Onus</span>
                                </th>
                                <th>
                                    <span>&nbsp;</span>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="Tbx_emptyDescription" MaxLength="100" Text="Descrição" Width="600" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescript" InitialValue="Descrição" Text=" " runat="server" CssClass="lblError" ControlToValidate="Tbx_emptyDescription" ValidationGroup="InsertNew" ForeColor="" Display="None" ErrorMessage="Campo Descrição - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="Tbx_value" MaxLength="12" runat="server" Text="Valor" Width="150"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorValor" runat="server" InitialValue="Valor" Text=" " CssClass="lblError" ControlToValidate="Tbx_value" ValidationGroup="InsertNew" ForeColor="" Display="None" ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID ="RegExValValor2"  CssClass ="lblError" runat = "server" ControlToValidate = "Tbx_value" ValidationGroup ="InsertNew"  Display ="none" ErrorMessage="Campo Valor - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type ="Double"></asp:RangeValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="Tbx_bond" runat="server" MaxLength="12" Text="Onus" Width="150"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorOnus" runat="server" CssClass="lblError" InitialValue="Onus" Display="None" ControlToValidate="Tbx_bond" ValidationGroup="InsertNew" ForeColor="" ErrorMessage="Campo Onus - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID ="RegExValOnus"  CssClass ="lblError" runat = "server" ControlToValidate = "Tbx_bond" ValidationGroup ="InsertNew"  Display ="none" ErrorMessage="Campo Onus - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type ="Double"></asp:RangeValidator>
                                </td>
                                <td>
                                <div class="padLabel1">
                                    <asp:Button ID="Btt_InsertAval" runat="server" Text="Inserir" CommandName="InsertEmptyItem"
                                        CommandArgument="Party" ValidationGroup ="InsertNew" />
                                        </div>  
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Descrição">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.description")%>'> </asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" Width="600"  MaxLength="100" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.description")%>' /></EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtdescript" Width="600" MaxLength="100" runat="server" Text="Descrição" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescript" InitialValue="Descrição" Text=" " runat="server" CssClass="lblError" ControlToValidate="txtdescript" ValidationGroup="Insert" ForeColor="" Display="None" ErrorMessage="Campo Descrição - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                <asp:Button ID="Btt_Insert" runat="server" ValidationGroup="Insert" Text="Inserir" OnClick="Btt_InsertValue_Click"
                                    Visible="false" /></FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.value1", "{0:#,##0.00}")%>'> </asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox7" Width="150" MaxLength="12" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.value1")%>' /></EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtValor" runat="server" MaxLength="12" Width="150" Text="Valor" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorValor" runat="server" InitialValue="Valor" Text=" " CssClass="lblError" ControlToValidate="txtValor" ValidationGroup="Insert" ForeColor="" Display="None" ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID ="RegExValValor3"  CssClass ="lblError" runat = "server" ControlToValidate = "txtValor" ValidationGroup ="Insert"  Display ="none" ErrorMessage="Campo Valor - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type ="Double"></asp:RangeValidator>
                                </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Onus">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.bond", "{0:#,##0.00}")%>'> </asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox8" Width="150" MaxLength="12" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.bond")%>' /></EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtOnus" runat="server" Width="150" MaxLength="12" Text="Onus" />
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidatorOnus" runat="server" CssClass="lblError" InitialValue="Onus" Display="None" ControlToValidate="txtOnus" ValidationGroup="Insert" ForeColor="" ErrorMessage="Campo Onus - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                 <asp:RangeValidator ID ="RegExValOnus" CssClass ="lblError" runat = "server" ControlToValidate = "txtOnus" ValidationGroup ="Insert" Display ="none" ErrorMessage="Campo Onus - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type ="Double"></asp:RangeValidator>
                                </FooterTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowDeleteButton="true" EditText="Editar" DeleteText="Remover"
                            CausesValidation="false" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <div class="padBottom">
    <asp:ValidationSummary ID="ValidationSummary2" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Avalistas" ShowMessageBox="false" ShowSummary="true" ValidationGroup="InsertAvalistas" runat="server" /></div>
<div class="padBottom">
    <asp:ValidationSummary ID="ValidationSummary3" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Avalistas" ShowMessageBox="false" ShowSummary="true" ValidationGroup="InsertNewAvalistas" runat="server" /></div>
            </td>
        </tr>
        <tr>
            <td>
                <label class="lblBold">
                    Avalistas</label>
                <br />
            </td>
        </tr>
        <tr valign="top">
            <td class="txtCenter">
                <asp:GridView ID="GV_ValuesAval" runat="server" CssClass="GridView" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" Width="995px" DataKeyNames="idValue"
                    OnRowUpdating="GV_ValuesAval_RowUpdating" OnRowEditing="GV_ValuesAval_RowEditing"
                    OnRowCancelingEdit="GV_ValuesAval_RowCancelingEdit" ShowFooter="true" OnRowDeleting="GV_ValuesAval_RowDeleting"
                    OnRowDataBound="GV_ValuesAval_RowDataBound" EmptyDataText="NoVAlues" ShowHeader="true"
                    OnRowCommand="GV_ValuesAval_RowCommand">
                    <HeaderStyle CssClass="GridViewHeader" />
                    <RowStyle CssClass="GridViewRow" />
                    <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
                    <SelectedRowStyle CssClass="GridViewSelectedRow" />
                    <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />
                    <EmptyDataTemplate>
                        <table rules="all" cellspacing="0" border="1" style="width: 600px; border-collapse: collapse;">
                            <tr>
                                <th>
                                    <span>Descrição</span>
                                </th>
                                <th>
                                    <span>Valor</span>
                                </th>
                                <th>
                                    <span>Onus</span>
                                </th>
                                <th>
                                    <span>&nbsp;</span>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="Tbx_emptyDescription" Text="Descrição" MaxLength="100" Width="600" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescriptionAvalistas1" runat="server" CssClass="lblError" InitialValue="Descrição" Display="None" ControlToValidate="Tbx_emptyDescription" ValidationGroup="InsertNewAvalistas" ForeColor="" ErrorMessage="Campo Descrição - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="Tbx_value" Width="150" Text="Valor" MaxLength="12" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorValorAvalistas1" runat="server" CssClass="lblError" InitialValue="Valor" Display="None" ControlToValidate="Tbx_value" ValidationGroup="InsertNewAvalistas" ForeColor="" ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID ="RegExValValor4"  CssClass ="lblError" runat = "server" ControlToValidate = "Tbx_value" ValidationGroup ="InsertNewAvalistas"  Display ="none" ErrorMessage="Campo Valor - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type ="Double"></asp:RangeValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="Tbx_bond" Width="150" Text="Onus" MaxLength="12" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorOnusAvalistas" runat="server" CssClass="lblError" InitialValue="Onus" Display="None" ControlToValidate="Tbx_bond" ValidationGroup="InsertNewAvalistas" ForeColor="" ErrorMessage="Campo Onus - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID ="RegExValOnus"  CssClass ="lblError" runat = "server" ControlToValidate = "Tbx_bond" ValidationGroup ="InsertNewAvalistas"  Display ="none" ErrorMessage="Campo Onus - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type ="Double"></asp:RangeValidator>
                                </td>
                                <td>
                                <div class="padLabel1">
                                    <asp:Button ID="Btt_InsertAval" ValidationGroup="InsertNewAvalistas" runat="server" Text="Inserir" CommandName="InsertEmptyItem"
                                        CommandArgument="Aval" />
                                        </div>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Descrição">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.description")%>'> </asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox Width="600" MaxLength="100" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.description")%>' /></EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="TextBox1" Width="600" MaxLength="100" runat="server" Text="Descrição" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescriptionAvalistas" runat="server" CssClass="lblError" InitialValue="Descrição" Display="None" ControlToValidate="TextBox1" ValidationGroup="InsertAvalistas" ForeColor="" ErrorMessage="Campo Descrição - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                <asp:Button ID="Btt_InsertAval" ValidationGroup="InsertAvalistas" runat="server" Text="Inserir" OnClick="Btt_InsertValueAval_Click"
                                    Visible="false" /></FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.value1", "{0:#,##0.00}")%>'> </asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" Width="150" MaxLength="12" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.value1")%>' /></EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="TextBox2" Width="150" MaxLength="12" runat="server" Text="Valor" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorValorAvalistas" runat="server" CssClass="lblError" InitialValue="Valor" Display="None" ControlToValidate="TextBox2" ValidationGroup="InsertAvalistas" ForeColor="" ErrorMessage="Campo Valor - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID ="RegExValValor5"  CssClass ="lblError" runat = "server" ControlToValidate = "TextBox2" ValidationGroup ="InsertAvalistas"  Display ="none" ErrorMessage="Campo Valor - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type ="Double"></asp:RangeValidator>                                
                                </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Onus">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.bond", "{0:#,##0.00}")%>'> </asp:Label></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" Width="150" MaxLength="12" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "v.bond")%>' /></EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="TextBox3" Width="150" MaxLength="12" runat="server" Text="Onus" />
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidatorOnusAvalistas" runat="server" CssClass="lblError" InitialValue="Onus" Display="None" ControlToValidate="TextBox3" ValidationGroup="InsertAvalistas" ForeColor="" ErrorMessage="Campo Onus - Preenchimento Obrigatório"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID ="RegExValValor1"  CssClass ="lblError" runat = "server" ControlToValidate = "TextBox3" ValidationGroup ="InsertAvalistas"  Display ="none" ErrorMessage="Campo Onus - Natureza numérica" MaximumValue="10000000000" MinimumValue="0" Type ="Double"></asp:RangeValidator>                                
                                </FooterTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowDeleteButton="true" EditText="Editar" DeleteText="Remover"
                            CausesValidation="false" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    
</div>
<div style="float:left; clear:both; width:200px;">
<asp:Panel ID ="Pnl_Sum" runat ="server" style=" padding-top:10px;">
<br />
<label class="lblBold">Totais Património</label>
<table CssClass="GridView" style =" width:200px;">
<tr><th></th><th style="text-align:right;"><label>Entidade</label></th><th style="text-align:right;"><label>Avalistas</label></th></tr>
<tr><td style="border-bottom:solid 1px;" ><label >Valor</label></td><td style=" text-align:right;"><label><%# sumValues[0].ToString("#,##0.00")%></label></td><td style=" text-align:right;"><label><%# sumValues[2].ToString("#,##0.00")%></label></td></tr>
<tr><td style="border-bottom:solid 1px;"><label >Onus</label></td><td style=" text-align:right;"><label><%#sumValues[1].ToString("#,##0.00")%></label></td><td style=" text-align:right;"><label><%# sumValues[3].ToString("#,##0.00")%></label></td></tr>
</table>
</asp:Panel>
</div>
</div>


<div class="padBottom">
    &nbsp;</div>                           
