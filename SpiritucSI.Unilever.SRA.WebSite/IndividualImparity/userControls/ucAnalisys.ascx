﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAnalisys.ascx.cs"
    Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ucAnalisys" %>
<%@ Register Src="../../UserControls/Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="uc1" %>
<%@ Register src="ImpResume.ascx" tagname="ImpResume" tagprefix="uc2" %>
<div class="SubContentCenter">
    <hr />
    <label class="lblBoldHidd">
        Actualização de Fluxos de caixa futuros</label>
    <br />
    <div>
        <asp:ValidationSummary ID="ValidationSummary" CssClass="ValidationSummary" HeaderText="Erros de Preenchimento - Analise"
            ShowMessageBox="false" ShowSummary="true" ValidationGroup="Insert" runat="server" />
    </div>
    <table>
        <tr>
            <td>
                <span>Fluxo Global</span>&nbsp;<asp:CheckBox ID="Chk_GF" runat="server" Checked= <%#this.isGlobalFlow %> />
            </td>
        </tr>
        <tr>
            <td>
                <label class="lblBold">
                    Actualização de Fluxos de caixa futuros</label>
                <br />
            </td>
        </tr>
        <tr valign="top">
            <td class="txtCenter">
                <asp:Panel ID="Pnl_Flow" runat="server" >
                    <asp:GridView ID="GV_Flows" CssClass="GridView" runat="server" AllowPaging="False"
                        AllowSorting="True" AutoGenerateColumns="False" Width="750px" DataKeyNames="idFlow"
                        OnRowEditing="Gv_Flows_RowEditing" OnRowCancelingEdit="GV_FLows_RowCancelingEdit"
                        OnDataBound="GV_Flows_DataBound" OnRowDataBound="GV_Flows_ItemDataBound" OnRowDeleting="GV_FLows_RowDeleting">
                        <HeaderStyle CssClass="GridViewHeader" />
                        <RowStyle CssClass="GridViewRow" />
                        <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
                        <SelectedRowStyle CssClass="GridViewSelectedRow" />
                        <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />
                        <EmptyDataTemplate>
                            <span>Não existem operações associadas</span></EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Nº Operação">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "contractRef")%>' /><asp:HiddenField
                                        ID="HF_idContractS" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "idContractS")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Plano Financeiro">
                                <ItemTemplate>
                                    <asp:CheckBox ID="Chk_PF" runat="server" AutoPostBack="false" Enabled='<%#DataBinder.Eval(Container.DataItem,"hasFP") %>' Checked ='<%#DataBinder.Eval(Container.DataItem,"isFP") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<3 meses">
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtF1" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f1")).ToString("0.00")%>' />
                                    <asp:RangeValidator ID="RV_F1" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="TxtF1" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="3 a 6 meses">
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtF2" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f2")).ToString("0.00")%>'  />
                                    <asp:RangeValidator ID="RV_F2" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="TxtF2" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="6 a 9 meses">
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtF3" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f3")).ToString("0.00")%>'  />
                                    <asp:RangeValidator ID="RV_F3" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="TxtF3" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000000" MinimumValue="0"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="9 a 12 meses">
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtF4" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f4")).ToString("0.00")%>'  />
                                     <asp:RangeValidator ID="RV_F4" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="TxtF4" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="1 a 2 anos">
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtF5" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f5")).ToString("0.00")%>'  />
                                    <asp:RangeValidator ID="RV_F5" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="TxtF5" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="2 a 3 anos">
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtF6" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f6")).ToString("0.00")%>'  />
                                    <asp:RangeValidator ID="RV_F6" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="TxtF6" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="3 a 4 anos">
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtF7" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f7")).ToString("0.00")%>'  />
                                    <asp:RangeValidator ID="RV_F7" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="TxtF7" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                              </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="4 a 5 anos">
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtF8" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f8")).ToString("0.00")%>'  />
                                    <asp:RangeValidator ID="RV_F8" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="TxtF8" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="> 5 anos">
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtF9" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f9")).ToString("0.00")%>'  />
                                    <asp:RangeValidator ID="RV_F9" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="TxtF9" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                 </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="total" ItemStyle-Wrap ="true" ItemStyle-HorizontalAlign ="Right">
                                <ItemTemplate>
                                    <asp:Label ID ="Lbl_total" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "total")).ToString("#,##0.00")%>' /></EditItemTemplate></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr valign="top">
            <td class="txtCenter">
                <asp:Panel ID="Pnl_GF_Insert" runat="server"  >
                    <asp:DetailsView ID="DV_GF" BorderStyle="None" runat="server" AutoGenerateRows="false">
                        <Fields>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <table cssclass="GridView" style="width: 750px;">
                                        <tr>
                                            <th scope="col">
                                                <3 meses
                                            </th>
                                            <th scope="col">
                                                3 a 6 meses
                                            </th>
                                            <th scope="col">
                                                6 a 9 meses
                                            </th>
                                            <th scope="col">
                                                9 a 12 meses
                                            </th>
                                            <th scope="col">
                                                1 a 2 anos
                                            </th>
                                            <th scope="col">
                                                2 a 3 anos
                                            </th>
                                            <th scope="col">
                                                3 a 4 anos
                                            </th>
                                            <th scope="col">
                                                4 a 5 anos
                                            </th>
                                            <th scope="col">
                                                > 5 anos
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="Txt_less3" runat="server" Width="58px" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "monthLess3")).ToString("0.00")%>'></asp:TextBox>
                                                <asp:RangeValidator ID="RV_E1" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="Txt_less3" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_3to6" runat="server" Width="58px" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "month3to6")).ToString("0.00")%>'></asp:TextBox>
                                            <asp:RangeValidator ID="RV_E2" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="Txt_3to6" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_6to9" runat="server" Width="58px" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "month6to9")).ToString("0.00")%>'></asp:TextBox>
                                            <asp:RangeValidator ID="RV_E3" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="Txt_6to9" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_9to12" runat="server" Width="58px" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "month9to12")).ToString("0.00")%>'></asp:TextBox>
                                            <asp:RangeValidator ID="RV_E4" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="Txt_9to12" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_1to2" runat="server" Width="58px" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "year1to2")).ToString("0.00")%>'></asp:TextBox>
                                            <asp:RangeValidator ID="RV_E5" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="Txt_1to2" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_2to3" runat="server" Width="58px" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "year2to3")).ToString("0.00")%>'></asp:TextBox>
                                            <asp:RangeValidator ID="RV_E6" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="Txt_2to3" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_3to4" runat="server" Width="58px" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "year3to4")).ToString("0.00")%>'></asp:TextBox>
                                            <asp:RangeValidator ID="RV_E7" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="Txt_3to4" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_4to5" runat="server" Width="58px" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "year4to5")).ToString("0.00")%>'></asp:TextBox>
                                            <asp:RangeValidator ID="RV_E8" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="Txt_4to5" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_more5" runat="server" Width="58px" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "yearMore5")).ToString("0.00")%>'></asp:TextBox>
                                            <asp:RangeValidator ID="RV_E9" ErrorMessage="Campo Fluxo Balanço" ControlToValidate="Txt_more5" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="10000000000" MinimumValue="0"></asp:RangeValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </asp:Panel>
            </td>
        </tr>
</table>
<div>
<div style ="width: 160px; float: right; padding-left: 36px; padding-right: 4px;">
<uc2:ImpResume ID ="ucImp" runat ="server" />
</div>
<div style ="width:750px; float:left;">
<table>
        <tr>
            <td>
                <br />
                <label class="lblBold">
                    Perda por accionamentos de garantias bancárias</label>
                <br />
            </td>
        </tr>
        <tr valign="top">
            <td class="txtCenter">
                <asp:GridView ID="GV_GARANTIES" runat="server" CssClass="GridView" PageSize="10000"
                    AllowSorting="True" AutoGenerateColumns="False" Width="750px" DataKeyNames="idFlow"
                    OnRowEditing="Gv_Garanties_RowEditing" OnRowCancelingEdit="GV_Garanties_RowCancelingEdit"
                    OnDataBound="GV_Garanties_DataBound" OnRowDataBound="GV_Garanties_ItemDataBound"
                    OnRowDeleting="GV_Garanties_RowDeleting" OnDataBinding="GV_Garanties_DataBinding">
                    <HeaderStyle CssClass="GridViewHeader" />
                    <RowStyle CssClass="GridViewRow" />
                    <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
                    <SelectedRowStyle CssClass="GridViewSelectedRow" />
                    <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />
                    <EmptyDataTemplate>
                        <span>Não existem operações associadas</span></EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Nº Operação">
                            <ItemTemplate>
                                <asp:Label ID="Lbl_ContractRef" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "contractRef")%>'></asp:Label>
                                <asp:HiddenField ID="HF_ContractS" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "idContractS")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor">
                            <ItemTemplate>
                                <asp:Label ID ="Lbl_1" Width ="58px" runat  ="server" Text = '<%#((Double)DataBinder.Eval(Container.DataItem, "f1")).ToString("#,##0.00")%>'></asp:Label>   
                         </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data de inicio">
                            <ItemTemplate>
                                <asp:Label ID = "Txt_G2" Width="58px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "sd", "{0:dd-MM-yyyy}")%>' />
                                </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data de fim">
                            <ItemTemplate>
                                <asp:Label ID = "Txt_G3" Width="58px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ed", "{0:dd-MM-yyyy}")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tipo">
                            <ItemTemplate>
                            <asp:HiddenField ID = "HF_type" runat = "server" Value = '<%#DataBinder.Eval(Container.DataItem, "fg.type")%>' />                                
                            <asp:DropDownList ID="DL_GType" Width="74px" runat="server">
                            <asp:ListItem Text = "Técnica" Value="T" Selected= "True"></asp:ListItem>
                            <asp:ListItem Text = "Financeira" Value="F"></asp:ListItem>
                            </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Probabilidade da garantia ser accionada e o BPN ter de avançar com os fundos (%)">
                            <ItemTemplate>
                                <asp:TextBox ID = "Txt_G4" Width="70px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f2")).ToString("#,##0.00")%>' />
                                <asp:RangeValidator ID="RangeValidatorG4" ErrorMessage="Campo Probabilidade Garantia" ControlToValidate="Txt_G4" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="100" MinimumValue="0"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Probabilidade de incumprimento do cliente caso o BPN tenha de avançar com os  fundos (%)">
                            <ItemTemplate>
                                <asp:TextBox ID ="Txt_G5" Width="70px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f3")).ToString("#,##0.00")%>' />
                                <asp:RangeValidator ID="RangeValidatorG5" ErrorMessage="Campo Probabilidade Garantia" ControlToValidate="Txt_G5" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="100" MinimumValue="0"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="total" ItemStyle-Wrap ="true" ItemStyle-HorizontalAlign = "Right">
                            <ItemTemplate>
                                <asp:Label Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f1")*((Double)DataBinder.Eval(Container.DataItem, "f2")/100)*((Double)DataBinder.Eval(Container.DataItem, "f3")/100)).ToString("#,##0.00") %>' /></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <label class="lblBold">
                    Perda por limites</label>
                <br />
            </td>
        </tr>
        <tr valign="top">
            <td class="txtCenter">
                <asp:GridView ID="GV_Limits" runat="server" CssClass="GridView" PageSize ="10000"
                    AllowSorting="True" AutoGenerateColumns="False" Width="750px" DataKeyNames="idFlow"
                    OnRowEditing="Gv_Garanties_RowEditing" OnRowCancelingEdit="GV_Garanties_RowCancelingEdit"
                    OnDataBound="GV_Garanties_DataBound" OnRowDataBound="GV_Garanties_ItemDataBound"
                    OnRowDeleting="GV_Garanties_RowDeleting" OnDataBinding="GV_Garanties_DataBinding">
                    <HeaderStyle CssClass="GridViewHeader" />
                    <RowStyle CssClass="GridViewRow" />
                    <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
                    <SelectedRowStyle CssClass="GridViewSelectedRow" />
                    <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />
                    <EmptyDataTemplate>
                        <span>Não existem operações associadas</span></EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Nº Operação">
                            <ItemTemplate>
                                <asp:Label ID="Lbl_ContractRef" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "contractRef")%>'></asp:Label><asp:HiddenField
                                    ID="HF_ContractS" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "idContractS")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor">
                            <ItemTemplate>
                                <asp:Label ID ="Lbl_L1" runat ="server" Text ='<%#((Double)DataBinder.Eval(Container.DataItem, "f1")).ToString("#,##0.00")%>'></asp:Label>
                                </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data de inicio">
                            <ItemTemplate>
                                <asp:Label ID="Txt_L2" Width="58px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "sd", "{0:dd-MM-yyyy}")%>' />
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data de fim">
                            <ItemTemplate>
                                <asp:Label ID="Txt_L3" Width="58px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ed", "{0:dd-MM-yyyy}")%>' />
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Probabilidade de utilização do limite pelo cliente(%)">
                            <ItemTemplate>
                                <asp:TextBox ID="Txt_L5" Width="70px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f2")).ToString("#,##0.00")%>' />
                                <asp:RangeValidator ID="RangeValidatorL5" ErrorMessage="Campo Probabilidade Limite" ControlToValidate="Txt_L5" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="100" MinimumValue="0"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Probabilidade de incumprimento do cliente caso o limite seja utilizado (%)">
                            <ItemTemplate>
                                <asp:TextBox ID="Txt_L6" Width="70px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f3")).ToString("#,##0.00")%>' />
                                <asp:RangeValidator ID="RangeValidatorL6" ErrorMessage="Campo Probabilidade Limite" ControlToValidate="Txt_L6" ValidationGroup="Insert" Type="Double" Display ="None" EnableClientScript ="false" runat="server" MaximumValue="100" MinimumValue="0"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="total" ItemStyle-Wrap ="true" ItemStyle-HorizontalAlign = "Right">
                            <ItemTemplate>
                                <asp:Label ID="Label1" Width="58px" runat="server" Text='<%#((Double)DataBinder.Eval(Container.DataItem, "f1")*((Double)DataBinder.Eval(Container.DataItem, "f2")/100)*((Double)DataBinder.Eval(Container.DataItem, "f3")/100)).ToString("#,##0.00") %>' /></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr valign="top">
            <td class="txtLeft">
                <asp:Button ID="Btt_Save" runat="server" Text="Calcular Imparidade" OnClick="Btt_Save_Click" ValidationGroup= "Insert" />
                <asp:Button ID="Btt_ImpCalc" runat="server" Text="Calcular" OnClick="Btt_ImpCalc_Click"
                    Visible="false" />
            </td>
        </tr>
    </table>
</div>
</div>
</div>
<div class="padBottom">&nbsp;</div>
