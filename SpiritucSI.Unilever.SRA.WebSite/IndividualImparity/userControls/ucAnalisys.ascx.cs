﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class ucAnalisys : System.Web.UI.UserControl
    {

        #region PROPS
        private int v_idPartySelected;

        public int idPartySelected
        {

            get
            {
                if (v_idPartySelected == 0)
                {
                    int id = 0;

                    if (Session["idParty"] == null)
                        this.Response.Redirect("~/Dashboard.aspx");
                    else
                    {
                        Int32.TryParse(Session["idParty"].ToString(), out id);
                        if (id != 0)
                            v_idPartySelected = id;
                    }
                }

                return v_idPartySelected;
            }

            set 
            {
                v_idPartySelected = value;
            
            }
        }


        public bool IsReadOnly { get; set; }

        private bool v_isGlobalFlow;

        public bool isGlobalFlow
        {

            get
            {
                if (v_isGlobalFlow == null)
                {
                    BLL ds = new BLL();
                    v_isGlobalFlow = ds.GetPartyIsGlobalFlowById(idPartySelected);
                }

                return v_isGlobalFlow;
            }

            set
            {
                v_isGlobalFlow = value;

            }
        }
        
        
        
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!this.IsPostBack)
            {
                this.Chk_GF.Attributes.Add("onclick", "return HideFlow('" + Pnl_Flow.ClientID+ "','" + Pnl_GF_Insert.ClientID + "','" +Chk_GF.ClientID + "')");
                this.Load();
            }
        }

        protected void Page_PreRender ()
        {
            this.Pnl_Flow.Attributes.Add("style",isGlobalFlow?"display:none;visibility:hidden;":"display:block;visibility:visible;");

            this.Pnl_GF_Insert.Attributes.Add("style", !isGlobalFlow ? "display:none;visibility:hidden;" : "display:block;visibility:visible;");
        }


        protected void Load()
        {
            BLL ds = new BLL();
            
            GV_Flows.DataSource = ds.GetFlowFutureByParty(idPartySelected, 1000,0);
            GV_Flows.DataBind();
            var gs = ds.GetFlowGarantyByParty(idPartySelected, 1000, 0);
            GV_GARANTIES.DataSource = gs;
            GV_GARANTIES.DataBind();
            DV_GF.DataSource = ds.GetFlowFutureGlobalByParty(idPartySelected);
            DV_GF.DataBind();
            GV_Limits.DataSource = ds.GetFlowLimitByParty(idPartySelected, 1000, 0);
            GV_Limits.DataBind();
            if (IsReadOnly)
                LoadReadOnly(this);
        }

        protected void LoadReadOnly(Control c)
        {
            foreach (Control child in c.Controls)
            {
                this.LoadReadOnly(child);
            }

            if (c is CheckBox)
                ((CheckBox)c).Enabled = !this.IsReadOnly;
            if (c is TextBox)
                ((TextBox)c).Enabled = !this.IsReadOnly;
            if (c is RadioButton)
                ((RadioButton)c).Enabled = !this.IsReadOnly;
            if (c is Button)
                ((Button)c).Enabled = !this.IsReadOnly;
            if (c is LinkButton)
                ((LinkButton)c).Enabled = !this.IsReadOnly;

        }

        private void PageRedirect()
        {
            this.Response.Redirect("~/Dashboard.aspx");
        }

        private object populateContracts()
        {
            BLL ds = new BLL();
            //adicionar logica para retornar todos os contractos
            return ds.GetContractsByParty(idPartySelected, 1000, 0);

        }

        private void saveFutureFlows()
        {

            BLL ds = new BLL();
            ds.UpdatePartyIsGlobalFlowById(idPartySelected, Chk_GF.Checked);
            if (!Chk_GF.Checked)
            {
                foreach (GridViewRow gvR in GV_Flows.Rows)
                {

                    int idFlow = 0;
                    idFlow = int.Parse(this.GV_Flows.DataKeys[gvR.DataItemIndex].Value.ToString());

                    HiddenField contract = ((HiddenField)gvR.FindControl("HF_idContractS"));

                    CheckBox financePlan = ((CheckBox)gvR.Cells[1].FindControl("Chk_PF"));
                    
                    TextBox less3, three6, six9, nine12, year1to2, year2to3, year3to4,year4to5, more5;
                    if (!financePlan.Checked)
                    {
                        less3 = ((TextBox)gvR.Cells[2].FindControl("TxtF1"));
                        three6 = ((TextBox)gvR.Cells[2].FindControl("TxtF2"));
                        six9 = ((TextBox)gvR.Cells[2].FindControl("TxtF3"));
                        nine12 = ((TextBox)gvR.Cells[2].FindControl("TxtF4"));
                        year1to2 = ((TextBox)gvR.Cells[2].FindControl("TxtF5"));
                        year2to3 = ((TextBox)gvR.Cells[2].FindControl("TxtF6"));
                        year3to4 = ((TextBox)gvR.Cells[2].FindControl("TxtF7"));
                        year4to5 = ((TextBox)gvR.Cells[2].FindControl("TxtF8"));
                        more5 = ((TextBox)gvR.Cells[2].FindControl("TxtF9"));

                        if (idFlow != 0)
                            ds.UpdateFlowFuture(null, idFlow, Int32.Parse(contract.Value), double.Parse(less3.Text), double.Parse(three6.Text), double.Parse(six9.Text), double.Parse(nine12.Text), double.Parse(year1to2.Text), double.Parse(year2to3.Text), double.Parse(year3to4.Text), double.Parse(year4to5.Text), double.Parse(more5.Text), financePlan.Checked);
                        else
                            ds.InsertFlowFuture(null, Int32.Parse(contract.Value), double.Parse(less3.Text), double.Parse(three6.Text), double.Parse(six9.Text), double.Parse(nine12.Text), double.Parse(year1to2.Text), double.Parse(year2to3.Text), double.Parse(year3to4.Text), double.Parse(year4to5.Text), double.Parse(more5.Text), financePlan.Checked);
               
                    
                    }
                    else
                    {
                        if (idFlow != 0)
                            ds.UpdateFlowFuture(null, idFlow, Int32.Parse(contract.Value), 0, 0, 0, 0, 0, 0, 0, 0, 0, financePlan.Checked);
                        else
                            ds.InsertFlowFuture(null, Int32.Parse(contract.Value), 0, 0, 0, 0, 0, 0, 0, 0, 0, financePlan.Checked);
                                                
                    }
                }
            }
            else
            {
                //Logica de fluxos globais

                TextBox less3 = ((TextBox)DV_GF.FindControl("Txt_less3"));
                TextBox three6 = ((TextBox)DV_GF.FindControl("Txt_3to6"));
                TextBox six9 = ((TextBox)DV_GF.FindControl("Txt_6to9"));
                TextBox nine12 = ((TextBox)DV_GF.FindControl("Txt_9to12"));
                TextBox year1to2 = ((TextBox)DV_GF.FindControl("Txt_1to2"));
                TextBox year2to3 = ((TextBox)DV_GF.FindControl("Txt_2to3"));
                TextBox year3to4 = ((TextBox)DV_GF.FindControl("Txt_3to4"));
                TextBox year4to5 = ((TextBox)DV_GF.FindControl("Txt_4to5"));
                TextBox more5 = ((TextBox)DV_GF.FindControl("Txt_more5"));



                ds.InsertGlobalFlow(null, idPartySelected, double.Parse(less3.Text), double.Parse(three6.Text), double.Parse(six9.Text), double.Parse(nine12.Text), double.Parse(year1to2.Text), double.Parse(year2to3.Text), double.Parse(year3to4.Text),double.Parse(year4to5.Text), double.Parse( more5.Text));
            }
        }
        private void saveGarantyFlows()
        {
            foreach (GridViewRow gvR in this.GV_GARANTIES.Rows)
            {

                int idFlow = 0;
                idFlow = int.Parse(this.GV_GARANTIES.DataKeys[gvR.DataItemIndex].Value.ToString());

                HiddenField contract = ((HiddenField)gvR.FindControl("HF_ContractS"));
                //TextBox value = ((TextBox)gvR.Cells[2].FindControl("Txt_G1"));
                //TextBox startDate = ((TextBox)gvR.Cells[2].FindControl("Txt_G2"));
                //TextBox endDate = ((TextBox)gvR.Cells[2].FindControl("Txt_G3"));
                //TextBox type = ((TextBox)gvR.Cells[2].FindControl("Txt_G6"));
                DropDownList type = ((DropDownList)gvR.FindControl("DL_GType"));
                
                TextBox bankGarantyProb = ((TextBox)gvR.Cells[2].FindControl("Txt_G4"));
                TextBox clientUnfllProb = ((TextBox)gvR.Cells[2].FindControl("Txt_G5"));


                BLL ds = new BLL();
                if (contract.Value !="" && bankGarantyProb.Text != "" && clientUnfllProb.Text != "")
                {
                    if (idFlow != 0)
                        ds.UpdateFlowGaranty(null, idFlow, Int32.Parse(contract.Value), type.SelectedValue, double.Parse(bankGarantyProb.Text), double.Parse(clientUnfllProb.Text));
                    else
                        ds.InsertFlowGaranty(null, Int32.Parse(contract.Value), type.SelectedValue, double.Parse(bankGarantyProb.Text), double.Parse(clientUnfllProb.Text));
                }

                else
                {
                    if (idFlow != 0)
                        ds.UpdateFlowGaranty(null, idFlow, Int32.Parse(contract.Value), "", 0, 0);
                    else
                        ds.InsertFlowGaranty(null, Int32.Parse(contract.Value), "", 0,0);
                }
            }

        }


        private void saveLimitFlows()
        {
            foreach (GridViewRow gvR in this.GV_Limits.Rows)
            {

                int idFlow = 0;
                idFlow = int.Parse(this.GV_Limits.DataKeys[gvR.DataItemIndex].Value.ToString());

                HiddenField contract = ((HiddenField)gvR.FindControl("HF_ContractS"));
                //TextBox value = ((TextBox)gvR.Cells[2].FindControl("Txt_L1"));
                //TextBox startDate = ((TextBox)gvR.Cells[2].FindControl("Txt_L2"));
                //TextBox endDate = ((TextBox)gvR.Cells[2].FindControl("Txt_L3"));
                //TextBox type = ((TextBox)gvR.Cells[2].FindControl("Txt_L4"));
                TextBox bankGarantyProb = ((TextBox)gvR.Cells[2].FindControl("Txt_L5"));
                TextBox clientUnfllProb = ((TextBox)gvR.Cells[2].FindControl("Txt_L6"));

                BLL ds = new BLL();

                if (contract.Value != "" && bankGarantyProb.Text != "" && clientUnfllProb.Text != "")
                {

                    if (idFlow != 0)
                        ds.UpdateFlowLimit(null, idFlow, Int32.Parse(contract.Value), double.Parse(bankGarantyProb.Text), double.Parse(clientUnfllProb.Text));
                    else
                        ds.InsertFlowLimit(null, Int32.Parse(contract.Value), double.Parse(bankGarantyProb.Text), double.Parse(clientUnfllProb.Text));
                }

                else
                {
                    if (idFlow != 0)
                        ds.UpdateFlowLimit(null, idFlow, Int32.Parse(contract.Value), 0, 0);
                    else
                        ds.InsertFlowLimit(null, Int32.Parse(contract.Value), 0, 0);
                }
            }

        }



        #region GV_FLOWS

        protected void GV_Flows_ItemDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow) 
            {
                CheckBox chk = (CheckBox)e.Row.FindControl("Chk_PF");
                if(chk != null)
                    chk.Attributes.Add("onclick", "return DisableTextBox('" + chk.ClientID+"','"+(e.Row.RowIndex +1).ToString()+"','"+GV_Flows.ClientID +"')");
                if (chk.Checked)
                {
                    TextBox txt1 = (TextBox)e.Row.FindControl("TxtF1"); txt1.Attributes.Add("disabled", "true");
                    TextBox txt2 = (TextBox)e.Row.FindControl("TxtF2"); txt2.Attributes.Add("disabled", "true");
                    TextBox txt3 = (TextBox)e.Row.FindControl("TxtF3"); txt3.Attributes.Add("disabled", "true");
                    TextBox txt4 = (TextBox)e.Row.FindControl("TxtF4"); txt4.Attributes.Add("disabled", "true");
                    TextBox txt5 = (TextBox)e.Row.FindControl("TxtF5"); txt5.Attributes.Add("disabled", "true");
                    TextBox txt6 = (TextBox)e.Row.FindControl("TxtF6"); txt6.Attributes.Add("disabled", "true");
                    TextBox txt7 = (TextBox)e.Row.FindControl("TxtF7"); txt7.Attributes.Add("disabled", "true");
                    TextBox txt8 = (TextBox)e.Row.FindControl("TxtF8"); txt8.Attributes.Add("disabled", "true");
                    TextBox txt9 = (TextBox)e.Row.FindControl("TxtF9"); txt9.Attributes.Add("disabled", "true");
                }
                //Label lbl = (Label)e.Row.FindControl("Lbl_total");
                //if (lbl != null)
                //    lbl.Attributes.Add("value", "return GetFlowTotal('" + lbl.ClientID + "','" + (e.Row.RowIndex + 1).ToString() + "','" + GV_Flows.ClientID + "')");
                
            }
        }

        protected void GV_Flows_DataBound(object sender, EventArgs e)
        {
        }



        public void Gv_Flows_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GV_Flows.EditIndex = e.NewEditIndex;
            this.Load();
        }

        public void GV_FLows_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GV_Flows.EditIndex = -1;
            this.Load();
        }



        
        public void GV_FLows_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
            int indx = 0;
            if (int.TryParse(e.RowIndex.ToString(), out indx))
            {
                int idContractS = int.Parse(this.GV_Flows.DataKeys[e.RowIndex].Value.ToString());
                BLL ds = new BLL();
                ds.DeleteFlowFuture(idContractS, null);
                this.Load();

            }

        }

        //protected void Btt_InsertFlow_Click(object sender, EventArgs e)
        //{
        //    HiddenField contractS = ((HiddenField)GV_Flows.FooterRow.Cells[0].Controls[1]);
        //    CheckBox financePlan = ((CheckBox)GV_Flows.FooterRow.Cells[1].Controls[0]);
        //    TextBox less3 = ((TextBox)GV_Flows.FooterRow.Cells[2].Controls[0]);
        //    TextBox three6 = ((TextBox)GV_Flows.FooterRow.Cells[3].Controls[0]);
        //    TextBox six9 = ((TextBox)GV_Flows.FooterRow.Cells[4].Controls[0]);
        //    TextBox nine12 = ((TextBox)GV_Flows.FooterRow.Cells[5].Controls[0]);
        //    TextBox year1to2 = ((TextBox)GV_Flows.FooterRow.Cells[6].Controls[0]);
        //    TextBox year2to3 = ((TextBox)GV_Flows.FooterRow.Cells[7].Controls[0]);
        //    TextBox year3to4 = ((TextBox)GV_Flows.FooterRow.Cells[8].Controls[0]);
        //    TextBox year4to5 = ((TextBox)GV_Flows.FooterRow.Cells[9].Controls[0]);
        //    TextBox more5 = ((TextBox)GV_Flows.FooterRow.Cells[10].Controls[0]);

        //    BLL ds = new BLL();
        //    ds.InsertFlowFuture(null, Int32.Parse(contractS.Value), double.Parse(less3.Text), double.Parse(three6.Text), double.Parse(six9.Text), double.Parse(nine12.Text), double.Parse(year1to2.Text), double.Parse(year2to3.Text), double.Parse(year3to4.Text), double.Parse(year4to5.Text), double.Parse(more5.Text), financePlan.Checked);
        //    this.Load();
        //}

        


        protected void GV_Flows_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }

        #endregion

        #region GV_GARANTIES

        protected void GV_Garanties_ItemDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.FindControl("DL_GType") != null && e.Row.FindControl("DL_GType") is DropDownList && e.Row.FindControl("HF_type") != null && e.Row.FindControl("HF_type") is HiddenField)
            {
                DropDownList dl = (DropDownList)e.Row.FindControl("DL_GType");
                dl.SelectedValue = ((HiddenField)e.Row.FindControl("HF_type")).Value;
            }

        }

        protected void GV_Garanties_DataBound(object sender, EventArgs e)
        {
        }


        protected void GV_Garanties_DataBinding(object sender, EventArgs e)
        {
        }


        public void Gv_Garanties_RowEditing(object sender, GridViewEditEventArgs e)
        {

            GV_GARANTIES.EditIndex = e.NewEditIndex;
            this.Load();
        }

        public void GV_Garanties_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GV_GARANTIES.EditIndex = -1;
            this.Load();
        }



        public void GV_Garanties_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int indx = 0;
            if (int.TryParse(e.RowIndex.ToString(), out indx))
            {
                //Passar para memoria/session
                int idFlow = 0;
                idFlow = int.Parse(this.GV_Flows.DataKeys[e.RowIndex].Value.ToString());
                int idContractS = int.Parse(this.GV_GARANTIES.DataKeys[e.RowIndex].Value.ToString());

                GridViewRow gvR = GV_GARANTIES.Rows[indx];

                //TextBox value = ((TextBox)gvR.Cells[1].Controls[0]);
                //TextBox startDate = ((TextBox)gvR.Cells[2].Controls[0]);
                //TextBox endDate = ((TextBox)gvR.Cells[3].Controls[0]);
                //TextBox type = ((TextBox)gvR.Cells[4].Controls[0]);
                DropDownList type = ((DropDownList)gvR.FindControl("DL_GType"));
                TextBox bankGarantyProb = ((TextBox)gvR.Cells[5].Controls[0]);
                TextBox clientUnfllProb = ((TextBox)gvR.Cells[6].Controls[0]);

                BLL ds = new BLL();
                ds.UpdateFlowGaranty(null,idFlow, idContractS, type.SelectedValue, double.Parse(bankGarantyProb.Text), double.Parse(clientUnfllProb.Text));

            }
            GV_GARANTIES.EditIndex = -1;
            this.Load();
        }

        public void GV_Garanties_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            int indx = 0;
            if (int.TryParse(e.RowIndex.ToString(), out indx))
            {
                int idContractS = int.Parse(this.GV_GARANTIES.DataKeys[e.RowIndex].Value.ToString());
                BLL ds = new BLL();
                ds.DeleteFlowGaranty(idContractS, null);
                this.Load();

            }

        }

        protected void Btt_InsertGaranty_Click(object sender, EventArgs e)
        {
            HiddenField contract = ((HiddenField)GV_GARANTIES.FooterRow.Cells[0].Controls[1]);
            //TextBox value = ((TextBox)GV_GARANTIES.FooterRow.Cells[1].Controls[0]);
            //TextBox startDate = ((TextBox)GV_GARANTIES.FooterRow.Cells[2].Controls[0]);
            //TextBox endDate = ((TextBox)GV_GARANTIES.FooterRow.Cells[3].Controls[0]);
            //TextBox type = ((TextBox)GV_GARANTIES.FooterRow.Cells[4].Controls[0]);
            TextBox bankGarantyProb = ((TextBox)GV_GARANTIES.FooterRow.Cells[5].Controls[0]);
            TextBox clientUnfllProb = ((TextBox)GV_GARANTIES.FooterRow.Cells[6].Controls[0]);

            BLL ds = new BLL();
            ds.InsertFlowGaranty(null, Int32.Parse(contract.Value),"", double.Parse(bankGarantyProb.Text), double.Parse(clientUnfllProb.Text) );
            this.Load();
        }




        #endregion

        public void Btt_Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    this.saveFutureFlows();
                    this.saveGarantyFlows();
                    this.saveLimitFlows();
                    IndividualImparityCalc imp = new IndividualImparityCalc();
                    imp.GetPartyTotalImparity(idPartySelected);
                    this.Load();
                }
            }
            
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
            }
        }


        public void Btt_ImpCalc_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                this.saveFutureFlows();
                this.saveGarantyFlows();
                this.saveLimitFlows();
                IndividualImparityCalc imp = new IndividualImparityCalc();
                imp.GetPartyTotalImparity(idPartySelected);
                this.Load();
            }
        }
    }
}