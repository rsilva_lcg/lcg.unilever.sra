﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnalisysSubmenu.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.AnalisysSubmenu" %>
<%@ Register Src="./Questionnaires.ascx" TagName="ucQuestionnaire" TagPrefix="uc1" %>
<%@ Register Src="./Values.ascx" TagName="ucValues" TagPrefix="uc2" %>
<%@ Register Src="./ucComments.ascx" TagName="ucComments" TagPrefix="uc3" %>
<%@ Register Src="./ucColaterals.ascx" TagName="ucColaterals" TagPrefix="uc4" %>
<%@ Register Src="./ucAnalisys.ascx" TagName="ucAnalisys" TagPrefix="uc5" %>

<asp:SiteMapDataSource ID = "SM_Analisys" runat = "server" SiteMapProvider="XmlSitemapProvider3" />


<div class="HeaderMenu" style = "clear:both;  padding-top : 40px;">
<div class="DivFloatLeft">
<asp:Repeater ID="rptTopMenu" runat="server" onitemdatabound="rptTopMenu_ItemDataBound">
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
        <div id="Menu" class="MenuTab">
            <div id="Left" runat="server"></div>
            <div id="Center" runat="server"><div><asp:LinkButton ID = "LB_option" runat = "server" OnClick = "LB_Option_Click" CommandArgument = <%# ((SiteMapNode)Container.DataItem).Url%> Text=<%# DataBinder.Eval(Container.DataItem, "Title") %> OnClientClick='<%# "javascript:return window.confirm(&#39;" + this.GetGlobalResourceObject("Global","TabChange").ToString() + "&#39;);"%>'></asp:LinkButton>&nbsp;</div></div> 
            <div id="Right" runat="server"></div>
        </div>
    </ItemTemplate>
    <FooterTemplate>
    </FooterTemplate>
</asp:Repeater>
</div>
<div class="DivFloatRight" style= "visibility:hidden;"></div>
<asp:Panel ID = "SubMenuCnt" runat = "server"></asp:Panel>
</div> 



