﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucComments.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ucComments" %>
<%@ Register src="../../UserControls/Common/MainMenu.ascx" tagname="MainMenu" tagprefix="uc1" %>


<div class="SubContentCenter">

<label class="lblBoldHidd">Dados</label>
<br />
<table>
    <tr valign="top">
        <td class="txtCenter" align ="left" >
        <asp:GridView ID= "GV_Comments" CssClass="GridView" runat="server" AllowPaging = "True" AllowSorting ="True" AutoGenerateColumns = "False" Width = "995px" >
            <HeaderStyle CssClass="GridViewHeader" />
            <RowStyle CssClass="GridViewRow" />
            <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
            <SelectedRowStyle CssClass="GridViewSelectedRow" />
            <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" /> 
            <Columns>
            <asp:TemplateField HeaderText = "Data"><ItemTemplate><asp:Label Width="120px" runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.date", "{0:dd-MM-yyyy}")%>'> </asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText = "Utilizador"><ItemTemplate><asp:Label Width="200px" runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "userName")%>'> </asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText = "Comentário" ItemStyle-Wrap="true"><ItemTemplate><asp:Label Width="640px" runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.comment1").ToString()%>' ToolTip='<%#DataBinder.Eval(Container.DataItem, "c.comment1").ToString()%>'  > </asp:Label></ItemTemplate></asp:TemplateField>
            </Columns>
        </asp:GridView>
        </td></tr>
    <tr valign="top">
        <td align="left">
        <br /><label class="lblBold">Inserir comentário</label><br />
        <asp:TextBox ID="TxtB_Comments" runat="server" TextMode ="MultiLine" Width = "995px" Height ="60px"></asp:TextBox><br />
        <div class="PadTop">
        <asp:Button ID ="Btt_InsertComment" runat = "server" Text = "Inserir" 
                onclick="Btt_InsertComment_Click" /></div>
        </td></tr>
</table>
</div>

<div class="padBottom">&nbsp;</div>
