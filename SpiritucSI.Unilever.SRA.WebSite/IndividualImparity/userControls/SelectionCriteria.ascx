﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectionCriteria.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.SelectionCriteria" %>
<br />
<label class="lblBold" style="text-align:center;">Critérios de Selecção</label>
<div style="padding-left:15px;">
<ul>
<asp:DataList ID="DL_SM" runat="server" RepeatDirection="Vertical" >
<ItemTemplate>
<li><asp:Label ID = "CS" Text = '<%#DataBinder.Eval(Container.DataItem,"code")%>' runat = "server" ToolTip ='<%#DataBinder.Eval(Container.DataItem,"description")%>' ></asp:Label>
<asp:Label ID = "Label1" runat ="server" Text= '<%#DataBinder.Eval(Container.DataItem,"description")%>' ></asp:Label>
</li>
</ItemTemplate>
</asp:DataList>
</ul>
</div>
