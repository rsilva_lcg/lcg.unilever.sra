﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class Questionnaires : System.Web.UI.UserControl
    {
        #region PROPS
        private int v_idPartySelected;

        public int idPartySelected
        {

            get
            {
                if (v_idPartySelected == 0)
                {
                    int id = 0;


                    if (Session["idParty"] == null)
                        this.Response.Redirect("~/IndividualImparity/Dashboard.aspx");
                    else
                    {
                        Int32.TryParse(Session["idParty"].ToString(), out id);
                        if (id != 0)
                            v_idPartySelected = id;
                    }
                }

                return v_idPartySelected;
            }

            set
            {
                v_idPartySelected = value;

            }
        }

        private bool v_IsReadOnly;

        public bool IsReadOnly { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!this.IsPostBack)
            {
                this.Load();
            }

        }

        protected void Load()
        {
            BLL ds = new BLL();

            R_QuestionnaireEnt.DataSource = ds.GetQuestionsByParty(idPartySelected, true, 50, 0);
            R_QuestionnaireEnt.DataBind();

            R_QuestionnaireClnt.DataSource = ds.GetQuestionsByParty(idPartySelected, false, 50, 0);
            R_QuestionnaireClnt.DataBind();
            Btt_Save.DataBind();

        }

        protected void Btt_Save_Click(object sender, EventArgs e)
        {
            int countAnsE = 0;
            int countAnsC = 0;

            foreach (RepeaterItem ri in R_QuestionnaireEnt.Items)
            {
                RadioButton rby = (RadioButton)ri.FindControl("RB_AnswerY");
                RadioButton rbn = (RadioButton)ri.FindControl("RB_AnswerN");
                RadioButton rbna = (RadioButton)ri.FindControl("RB_AnswerNA");

                if (rby.Checked || rbn.Checked || rbna.Checked)
                    countAnsE++;
            }

            foreach (RepeaterItem ri in R_QuestionnaireClnt.Items)
            {
                RadioButton rby = (RadioButton)ri.FindControl("RB_AnswerY");
                RadioButton rbn = (RadioButton)ri.FindControl("RB_AnswerN");
                RadioButton rbna = (RadioButton)ri.FindControl("RB_AnswerNA");

                if (rby.Checked || rbn.Checked || rbna.Checked)
                    countAnsC++;

            }

            if (countAnsE < R_QuestionnaireEnt.Items.Count)
            { RF_Questions1.IsValid = false; RF_Questions1.DataBind(); }

            else
            {
                if (countAnsC < R_QuestionnaireClnt.Items.Count)
                { RF_Questions2.IsValid = false; RF_Questions2.DataBind(); }

                else
                {

                    foreach (RepeaterItem ri in R_QuestionnaireEnt.Items)
                    {
                        HiddenField hf_a = (HiddenField)ri.FindControl("HF_answer");
                        HiddenField hf_q = (HiddenField)ri.FindControl("HF_question");
                        //TextBox ans = (TextBox)ri.FindControl("Answer_txtB");
                        RadioButton rby = (RadioButton)ri.FindControl("RB_AnswerY");
                        RadioButton rbn = (RadioButton)ri.FindControl("RB_AnswerN");
                        RadioButton rbna = (RadioButton)ri.FindControl("RB_AnswerNA");
                        BLL ds = new BLL();

                        if (!rby.Checked && !rbn.Checked && !rbna.Checked)
                        { RF_Questions1.IsValid = false; RF_Questions1.DataBind(); break; }
                        else
                        {
                            if (hf_a.Value == "0")
                                ds.InsertAnswers(null, idPartySelected, Int32.Parse(hf_q.Value), (rby.Checked ? "Yes" : (rbn.Checked ? "No" : (rbna.Checked ? "NA" : ""))));
                            else
                                ds.UpdateAnswers(null, Int32.Parse(hf_a.Value), (rby.Checked ? "Yes" : (rbn.Checked ? "No" : (rbna.Checked ? "NA" : ""))));
                        }
                    }

                    foreach (RepeaterItem ri in R_QuestionnaireClnt.Items)
                    {
                        HiddenField hf_a = (HiddenField)ri.FindControl("HF_answer");
                        HiddenField hf_q = (HiddenField)ri.FindControl("HF_question");
                        //TextBox ans = (TextBox)ri.FindControl("Answer_txtB");
                        RadioButton rby = (RadioButton)ri.FindControl("RB_AnswerY");
                        RadioButton rbn = (RadioButton)ri.FindControl("RB_AnswerN");
                        RadioButton rbna = (RadioButton)ri.FindControl("RB_AnswerNA");
                        BLL ds = new BLL();

                        if (!rby.Checked && !rbn.Checked && !rbna.Checked)
                        { RF_Questions2.IsValid = false; RF_Questions2.DataBind(); break; }
                        else
                        {

                            if (hf_a.Value == "0")
                                ds.InsertAnswers(null, idPartySelected, Int32.Parse(hf_q.Value), (rby.Checked ? "Yes" : (rbn.Checked ? "No" : (rbna.Checked ? "NA" : ""))));
                            else
                                ds.UpdateAnswers(null, Int32.Parse(hf_a.Value), (rby.Checked ? "Yes" : (rbn.Checked ? "No" : (rbna.Checked ? "NA" : ""))));
                        }
                    }


                    this.Load();
                }
            }
        }
    }
}