﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.Services;
using SpiritucSI.BPN.AII.WebSite.Masters;
using SpiritucSI.BPN.AII.WebSite.Common;
using System.Collections;
using SpiritucSI.BPN.AII.WebSite.UserControls.Workflows;
using System.Data;
using System.Text;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class ucAnalisysSummary : SuperControl
    {
        #region PROPERTIES
        public bool HasActionButtons { get; set; }
        public bool HasBulk { get; set; }
        public bool Pagging { get; set; }
        public string State { get; set; }
        public bool HasImparity { get; set; }
        private Business.PARTYSELECTED_STATE CurrentState;

        #endregion

        #region PAGE_EVENTS
        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.CurrentState = PartyServices.GetState(this.State);
            string stateName = this.CurrentState != null ? this.CurrentState.stateDescription : string.Empty;
            this.SubTitle2.Title = string.Format(this.GetLocalResourceObject("SubTitle1").ToString(), stateName);

            if (!this.Page.IsPostBack)
            {
               
                this.actionButtons.Visible = this.HasActionButtons;
                this.grid1.AllowPaging = this.Pagging;

                this.bntCancel.PostBackUrl = this.BaseUrl + "/Dashboard.aspx";

                this.HasImparity = this.CurrentState.hasImparity.GetValueOrDefault();

                this.grid1.Columns["Reference"].Visible = !this.HasImparity;
                this.grid1.Columns["Balcao"].Visible = !this.HasImparity;
                this.grid1.Columns["GroupName"].Visible = !this.HasImparity;

                this.grid1.Columns["ImparityTotal"].Visible = this.HasImparity;
                this.grid1.Columns["ImparityPercentage"].Visible = this.HasImparity;
                this.grid1.Columns["PreviousImparityTotal"].Visible = this.HasImparity;
                this.grid1.Columns["PreviousImparityPercentage"].Visible = this.HasImparity;
                
            }

            this.GridBind();


        }

        #endregion

        #region CONTROL_EVENTS
        protected void grid1_RowDataBound(object sender, Obout.Grid.GridRowEventArgs e)
        {
            string wfId = ((DataRowView)e.Row.DataItem)["WorkflowID"].ToString();

            WFActionsForGrid wfa = (WFActionsForGrid)e.Row.Cells[8].FindControl("WFActionsForGrid");
            if (wfa != null)
            {
                wfa.uid = wfId;
            }

        }

        protected void Details_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;

            if (lnk != null)
            {
                Session["idParty"] = lnk.CommandArgument;

                if (this.State == "InAllocation" ||
                   (this.State == "InAnalisys" && !this.CurrentUser.IsAnalyst()) ||
                   (this.State == "InSupervision" && !this.CurrentUser.IsSupervisor()) ||
                   (this.State == "InValidation" && !this.CurrentUser.IsProcessManager()) ||
                   (this.State == "InConclusion" && !this.CurrentUser.IsProcessManager()) ||
                    this.State == "Finish" ||
                    (this.CurrentUser.IsDirector() && this.CurrentUser.Roles.Where(x => x.RoleType == SpiritucSI.BPN.AII.Business.Enumerators.RoleType.Functional).Count() == 1))
                {
                    this.Response.Redirect("~/IndividualImparity/ClientView.aspx");
                }
                else
                {
                    this.Response.Redirect("~/IndividualImparity/ClientDetail.aspx");
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
                        int count = 0;
            int sucess = 0;
            SpiritucSI.BPN.AII.Business.OtherClasses.BLL bll = new SpiritucSI.BPN.AII.Business.OtherClasses.BLL();
            StringBuilder sb = new StringBuilder();

            foreach (Obout.Grid.GridRow row in grid1.Rows)
            {

                WFActionsForGrid wfa = (WFActionsForGrid)row.Cells[8].FindControl("WFActionsForGrid");
                //LinkButton lbl = (LinkButton)row.Cells[0].FindControl("lnk1");
                                
                if (wfa != null)
                {

                    count++;
                    try
                    {
                        wfa.ExecuteAction();
                        sucess++;
                        
                    }
                    catch (Exception ex)
                    {
                        sb.Append(string.Format(Resources.Global.BulkMessageError, ""));
                    }
                }
            }


            this.redirect.Value = "true";
            if (count != sucess)
            {
                ((FrontOffice)this.Page.Master).ShowMessage(sb.ToString(), null, SpiritucSI.BPN.AII.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Warning);
            }
            else
            {
                string message = string.Empty;

                if (this.State == "InSupervision") 
                { message = Resources.Global.BulkMessageSucessSupervision;}
                else if (this.State == "InValidation")
                { message = Resources.Global.BulkMessageSucessValidation;}
                else if (this.State == "InConclusion")
                { message = Resources.Global.BulkMessageSucessCompletion; }


                    ((FrontOffice)this.Page.Master).ShowMessage(message, null, SpiritucSI.BPN.AII.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Success);
            }
        }




        #endregion

        #region METHODS
        private void GridBind()
        {
            this.grid1.DataSource = PartyServices.GetPartyFor(this.CurrentUser, this.State);
            this.grid1.DataBind();
        }

        public string GetRedirect()
        {
            return this.redirect.ClientID;
        }
        #endregion


    }
}