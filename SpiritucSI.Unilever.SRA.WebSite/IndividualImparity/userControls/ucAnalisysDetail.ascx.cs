﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class ucAnalisysDetail : System.Web.UI.UserControl
    {

        public int idPartySelected { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            BLL ds = new BLL();

            int id = 0;
            if (idPartySelected == 0 && this.Request.QueryString["idParty"] != "")
                Int32.TryParse(this.Request.QueryString["idParty"], out id);
            idPartySelected = id;

            //GV_Flows.DataSource = ds.GetALLClientValues();
            //GV_Flows.DataBind();
            //GV_Losses.DataSource = ds.GetALLClientValues();
            //GV_Losses.DataBind();
        }
    }
}