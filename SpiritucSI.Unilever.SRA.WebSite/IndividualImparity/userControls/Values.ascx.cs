﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SpiritucSI.BPN.AII.Business.OtherClasses;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class Values : System.Web.UI.UserControl
    {
        #region PROPS
        private int v_idPartySelected;

        public int idPartySelected
        {

            get
            {
                if (v_idPartySelected == 0)
                {
                    int id = 0;


                    if (Session["idParty"] == null)
                        this.Response.Redirect("~/Dashboard.aspx");
                    else
                    {
                        Int32.TryParse(Session["idParty"].ToString(), out id);
                        if (id != 0)
                            v_idPartySelected = id;
                    }
                }

                return v_idPartySelected;
            }

            set
            {
                v_idPartySelected = value;

            }
        }

        public bool IsReadOnly { get; set; }

        public TextBox onus { get; set; }
        public TextBox description { get; set; }
        public TextBox value { get; set; }
        public RequiredFieldValidator reqDescription { get; set; }
        public RequiredFieldValidator reqValue { get; set; }
        public RequiredFieldValidator reqOnus { get; set; }
        //public RegularExpressionValidator reqRegValue { get; set; }
        //public RegularExpressionValidator reqRegOnus { get; set; }

        protected double[] sumValues { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.Load();
            }
        }



        protected void Load()
        {
            BLL ds = new BLL();

            GV_ValuesParty.DataSource = ds.GetValuePartyByParty(idPartySelected, 10, 0);
            GV_ValuesParty.DataBind();
            GV_ValuesAval.DataSource = ds.GetValueEvaluatorsByParty(idPartySelected, 10, 0);
            sumValues = ds.GetValuesSumByParty(idPartySelected);
            Pnl_Sum.DataBind();
            GV_ValuesAval.DataBind();
            if (IsReadOnly)
                LoadReadOnly(this);
        }

        protected void LoadReadOnly(Control c)
        {
            foreach (Control child in c.Controls)
            {
                this.LoadReadOnly(child);
            }

            if (c is CheckBox)
                ((CheckBox)c).Enabled = !this.IsReadOnly;
            if (c is TextBox)
                ((TextBox)c).Enabled = !this.IsReadOnly;
            if (c is RadioButton)
                ((RadioButton)c).Enabled = !this.IsReadOnly;
            if (c is Button)
                ((Button)c).Enabled = !this.IsReadOnly;
            if (c is LinkButton)
                ((LinkButton)c).Enabled = !this.IsReadOnly;

        }

        private void LoadScriptPersonal()
        {
            if (this.description != null)
            {
                this.description.Attributes.Add("onfocus", "return ClearTextBox('" + this.description.ClientID + "'," + "true, '" + "Descrição" + "')");
                this.description.Attributes.Add("onblur", "return ClearTextBox('" + this.description.ClientID + "'," + "false, '" + "Descrição" + "')");
            }
            if (this.value != null)
            {
                this.value.Attributes.Add("onfocus", "return ClearTextBox('" + this.value.ClientID + "'," + "true, '" + "Valor" + "')");
                this.value.Attributes.Add("onblur", "return ClearTextBox('" + this.value.ClientID + "'," + "false, '" + "Valor" + "')");
            }
            if (this.onus != null)
            {
                this.onus.Attributes.Add("onfocus", "return ClearTextBox('" + this.onus.ClientID + "'," + "true, '" + "Onus" + "')");
                this.onus.Attributes.Add("onblur", "return ClearTextBox('" + this.onus.ClientID + "'," + "false, '" + "Onus" + "')");
            }
        }


        #region GV_Values_Party

        public void GV_ValuesParty_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GV_ValuesParty.EditIndex = e.NewEditIndex;
            this.Load();
        }

        public void GV_ValuesParty_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GV_ValuesParty.EditIndex = -1;
            this.Load();
        }

        public void GV_ValuesParty_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int indx = 0;
            if (int.TryParse(e.RowIndex.ToString(), out indx))
            {
                int idValue = int.Parse(this.GV_ValuesParty.DataKeys[e.RowIndex].Value.ToString());

                GridViewRow gvR = GV_ValuesParty.Rows[indx];
                TextBox description = ((TextBox)gvR.Cells[0].Controls[0]);
                TextBox value = ((TextBox)gvR.Cells[1].Controls[0]);
                TextBox bond = ((TextBox)gvR.Cells[2].Controls[0]);

                BLL ds = new BLL();
                ds.UpdateValueParty(null, idValue, description.Text, double.Parse(value.Text), double.Parse(bond.Text), true);

            }
            GV_ValuesParty.EditIndex = -1;
            this.Load();
        }

        public void GV_ValuesParty_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            int indx = 0;
            if (int.TryParse(e.RowIndex.ToString(), out indx))
            {
                int idValue = int.Parse(this.GV_ValuesParty.DataKeys[e.RowIndex].Value.ToString());
                BLL ds = new BLL();
                ds.DeleteValue(idValue, null);
                this.Load();

            }

        }

        protected void Btt_InsertValue_Click(object sender, EventArgs e)
        {
            TextBox description = ((TextBox)GV_ValuesParty.FooterRow.Cells[0].Controls[1]);
            TextBox value = ((TextBox)GV_ValuesParty.FooterRow.Cells[1].Controls[1]);
            TextBox bond = ((TextBox)GV_ValuesParty.FooterRow.Cells[2].Controls[1]);

            BLL ds = new BLL();
            ds.InsertValue(null, idPartySelected, description.Text, double.Parse(value.Text), double.Parse(bond.Text), true);
            this.Load();
        }

        protected void GV_ValuesParty_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType.Equals(DataControlRowType.Footer))
            {
                Button Btt_InsertP = (Button)e.Row.Cells[0].FindControl("Btt_Insert");
                Btt_InsertP.Visible = true;
                e.Row.Cells[3].Controls.Add(Btt_InsertP);
            }
            if (e.Row.RowType.Equals(DataControlRowType.EmptyDataRow))
            {
                if (e.Row.FindControl("Tbx_emptyDescription") != null && e.Row.FindControl("Tbx_emptyDescription") is TextBox)
                {
                    description = (TextBox)e.Row.FindControl("Tbx_emptyDescription");
                    reqDescription = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorDescript");

                }
                if (e.Row.FindControl("Tbx_value") != null && e.Row.FindControl("Tbx_value") is TextBox)
                {
                    value = (TextBox)e.Row.FindControl("Tbx_value");
                    reqValue = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorValor");

                }
                if (e.Row.FindControl("Tbx_bond") != null && e.Row.FindControl("Tbx_bond") is TextBox)
                {
                    onus = (TextBox)e.Row.FindControl("Tbx_bond");
                    reqOnus = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorOnus");

                }
            }
            if (e.Row.RowType.Equals(DataControlRowType.Footer))
            {
                if (e.Row.FindControl("txtdescript") != null && e.Row.FindControl("txtdescript") is TextBox)
                {
                    description = (TextBox)e.Row.FindControl("txtdescript");
                    reqDescription = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorDescript");
                }
                if (e.Row.FindControl("txtValor") != null && e.Row.FindControl("txtValor") is TextBox)
                {
                    value = (TextBox)e.Row.FindControl("txtValor");
                    reqValue = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorValor");
                    //reqRegValue = (RegularExpressionValidator)e.Row.FindControl("RegExValValor");

                }
                if (e.Row.FindControl("txtOnus") != null && e.Row.FindControl("txtOnus") is TextBox)
                {
                    onus = (TextBox)e.Row.FindControl("txtOnus");
                    reqOnus = (RequiredFieldValidator)e.Row.FindControl("RequiredFieldValidatorOnus");
                    //reqRegOnus = (RegularExpressionValidator)e.Row.FindControl("RegExValOnus");
                }
            }
            if (this.description != null && this.value != null && this.onus != null)
            {
                this.LoadScriptPersonal();
            }

        }


        #endregion



        #region GV_ValuesAval

        public void GV_ValuesAval_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GV_ValuesAval.EditIndex = e.NewEditIndex;
            this.Load();
        }

        public void GV_ValuesAval_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GV_ValuesAval.EditIndex = -1;
            this.Load();
        }



        public void GV_ValuesAval_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int indx = 0;
            if (int.TryParse(e.RowIndex.ToString(), out indx))
            {
                int idValue = int.Parse(this.GV_ValuesAval.DataKeys[e.RowIndex].Value.ToString());
                GridViewRow gvR = GV_ValuesAval.Rows[indx];
                TextBox description = ((TextBox)gvR.Cells[0].Controls[0]);
                TextBox value = ((TextBox)gvR.Cells[1].Controls[0]);
                TextBox bond = ((TextBox)gvR.Cells[2].Controls[0]);

                BLL ds = new BLL();
                ds.UpdateValueParty(null, idValue, description.Text, double.Parse(value.Text), double.Parse(bond.Text), false);

            }
            GV_ValuesAval.EditIndex = -1;
            this.Load();
        }

        public void GV_ValuesAval_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            int indx = 0;
            if (int.TryParse(e.RowIndex.ToString(), out indx))
            {
                int idValue = int.Parse(this.GV_ValuesAval.DataKeys[e.RowIndex].Value.ToString());
                BLL ds = new BLL();
                ds.DeleteValue(idValue, null);
                this.Load();

            }

        }

        protected void Btt_InsertValueAval_Click(object sender, EventArgs e)
        {
            TextBox description = (TextBox)GV_ValuesAval.FooterRow.FindControl("TextBox1");
            TextBox value = ((TextBox)GV_ValuesAval.FooterRow.FindControl("TextBox2"));
            TextBox bond = ((TextBox)GV_ValuesAval.FooterRow.FindControl("TextBox3"));

            BLL ds = new BLL();
            ds.InsertValue(null, idPartySelected, description.Text, double.Parse(value.Text), double.Parse(bond.Text), false);
            this.Load();
        }



        protected void GV_ValuesAval_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType.Equals(DataControlRowType.Footer))
            {
                Button Btt_InsertA = (Button)e.Row.Cells[0].FindControl("Btt_InsertAval");
                Btt_InsertA.Visible = true;
                e.Row.Cells[3].Controls.Add(Btt_InsertA);
            }

            if (e.Row.RowType.Equals(DataControlRowType.EmptyDataRow))
            {
                if (e.Row.FindControl("Tbx_emptyDescription") != null && e.Row.FindControl("Tbx_emptyDescription") is TextBox)
                {
                    description = (TextBox)e.Row.FindControl("Tbx_emptyDescription");
                }
                if (e.Row.FindControl("Tbx_value") != null && e.Row.FindControl("Tbx_value") is TextBox)
                {
                    value = (TextBox)e.Row.FindControl("Tbx_value");
                }
                if (e.Row.FindControl("Tbx_bond") != null && e.Row.FindControl("Tbx_bond") is TextBox)
                {
                    onus = (TextBox)e.Row.FindControl("Tbx_bond");
                }
            }
            if (e.Row.RowType.Equals(DataControlRowType.Footer))
            {
                if (e.Row.FindControl("TextBox1") != null && e.Row.FindControl("TextBox1") is TextBox)
                {
                    description = (TextBox)e.Row.FindControl("TextBox1");
                }
                if (e.Row.FindControl("TextBox2") != null && e.Row.FindControl("TextBox2") is TextBox)
                {
                    value = (TextBox)e.Row.FindControl("TextBox2");
                }
                if (e.Row.FindControl("TextBox3") != null && e.Row.FindControl("TextBox3") is TextBox)
                {
                    onus = (TextBox)e.Row.FindControl("TextBox3");
                }
            }
            if (this.description != null && this.value != null && this.onus != null)
            {
                this.LoadScriptPersonal();
            }

        }


        protected void GV_ValuesAval_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("InsertEmptyItem"))
            {
                GridViewRow row = (GridViewRow)((e.CommandSource as Button).NamingContainer);
                TextBox description = (TextBox)row.FindControl("Tbx_emptyDescription");
                TextBox value = ((TextBox)row.FindControl("Tbx_value"));
                TextBox bond = ((TextBox)row.FindControl("Tbx_bond"));

                if (this.Page.IsValid)
                {
                    BLL ds = new BLL();
                    if (e.CommandArgument.Equals("Aval"))
                        ds.InsertValue(null, idPartySelected, description.Text, double.Parse(value.Text), double.Parse(bond.Text), false);
                    else
                        ds.InsertValue(null, idPartySelected, description.Text, double.Parse(value.Text), double.Parse(bond.Text), true);
                    this.Load();
                }
            }
        }




        #endregion




    }
}