﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucAnalisysSummary.ascx.cs"
    Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ucAnalisysSummary" %>
<%@ Register Src="~/UserControls/Common/ucSubTitle.ascx" TagName="SubTitle" TagPrefix="uc1" %>
<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<%@ Register Src="~/UserControls/Workflows/WFActionsForGrid.ascx" TagName="WFActionsForGrid" TagPrefix="uc1" %>
<uc1:SubTitle ID="SubTitle2" runat="server" Title='<%$Resources: SubTitle1 %>' Maximized="true"
    TargetDivId="List" Static="true" />
<div id="List" runat="server" class="SubContentCenterTable">
    <div style="float:right; width:auto; padding-bottom:5px">
        <a href="#" onclick="$('.ob_gCc1').find('input').attr('checked',false); $.each( $('.ob_gCc1'), function() {$(this).find('input').first().attr('checked',true) } ); return false;">select all</a>&nbsp;/&nbsp;
        <a href="#" onclick="$('.ob_gCc1').find('input').attr('checked',false); return false;">unselect all</a>
    </div>
    <div style="float: left; width: 100%">
        <obout:Grid ID="grid1" Width="100%" Language="pt" AutoPostBackOnSelect="true" CallbackMode="false" Serialize="false"
            AllowPaging="true" PageSize="2000" AutoGenerateColumns="false" AllowMultiRecordSelection="false"
            AllowRecordSelection="false" AllowFiltering="false"
            AllowPageSizeSelection="false" AllowSorting="false"
            AllowAddingRecords="false" runat="server" FolderStyle="~/App_Themes/MainTheme/Grid/Styles/Style_5"
            FolderLocalization="~/App_Themes/MainTheme/Grid/localization" 
            OnRowDataBound="grid1_RowDataBound">
            <Columns>
                    <obout:Column ID="Column1" Width="100%" DataField="Description" HeaderText='Entidade' runat="server" TemplateID="GridTemplateLnk1"></obout:Column>
                    <obout:Column ID="Column2" Width="80" DataField="Reference" HeaderText='Ref.' runat="server" TemplateID="GridTemplate1"></obout:Column>
                    <obout:Column ID="Column3" Width="90" DataField="Balcao" HeaderText='Balcão' runat="server" TemplateID="GridTemplate1"></obout:Column>
                    <obout:Column ID="Column4" Width="200" DataField="GroupName" HeaderText='Grupo de Análise' runat="server" TemplateID="GridTemplate1"></obout:Column>
                    <obout:Column ID="Column5" Width="100" DataField="ImparityTotal" HeaderText='Imp. Total' HeaderStyle-Wrap="true" runat="server" TemplateID="GridTemplateCurrency"></obout:Column>
                    <obout:Column ID="Column6" Width="100" DataField="ImparityPercentage" HeaderText='Imp.(%)' HeaderStyle-Wrap="true" runat="server" TemplateID="GridTemplatePercentage"></obout:Column>   
                    <obout:Column ID="Column7" Width="100" DataField="PreviousImparityTotal" HeaderText='Imp. Ant. Total' HeaderStyle-Wrap="true" runat="server" TemplateID="GridTemplateCurrency"></obout:Column>
                    <obout:Column ID="Column8" Width="100" DataField="PreviousImparityPercentage" HeaderText='Imp. Ant.(%)' HeaderStyle-Wrap="true" runat="server" TemplateID="GridTemplatePercentage"></obout:Column>   
                    <obout:Column ID="ColumnActions" Width="200" runat="server" DataField="Id" TemplateID="GridTemplateActions" HeaderText="Acções"></obout:Column>
                    <obout:Column ID="Column10" Width="100%" DataField="WorkflowID" HeaderText=' ' runat="server" TemplateID="GridTemplate1" Visible="false"></obout:Column>
            </Columns>
            <Templates>
                <obout:GridTemplate runat="server" id="GridTemplate1">
                        <Template>
                           <asp:Label ID="lbl1" runat="server" Text='<%# Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" id="GridTemplateCurrency">
                        <Template>
                           <asp:Label ID="lbl1" runat="server" Text='<%# Container.Value != "0" ? string.Format(" {0:#,##0.00}",Container.Value!=""?Double.Parse(Container.Value):0) : "0" %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" id="GridTemplatePercentage">
                        <Template>
                           <asp:Label ID="lbl1" runat="server" Text='<%# Container.Value != "0" ? string.Format("{0:N2}%",Container.Value): "0" %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplateLnk1">
                        <Template>
                            <asp:LinkButton Id="lnk1" runat="server" Text='<%# Container.Value %>' OnClick="Details_Click" CommandArgument='<%#  Container.DataItem["Id"] %>' CommandName="gridFinish"></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" id="GridTemplateActions">
                        <Template>
                            <uc1:WFActionsForGrid ID="WFActionsForGrid" runat="server" />
                        </Template>
                    </obout:GridTemplate>
            </Templates>
        </obout:Grid>
    </div>
    <div id="actionButtons" runat="server" class="Row" style="padding-top: 10px;">
        <div class="form-text">
            <asp:Label ID="lbl7" runat="server"></asp:Label></div>
        <div style="float: left; width: 100%">
            <div class="btnLeft">
                <asp:Button ID="bntCancel" CssClass="button1" runat="server" Text='cancelar' />
            </div>
            <div class="btnRight">
                <asp:UpdatePanel ID="up1" runat="server"><ContentTemplate>
                 <asp:Button ID="btnSave" CssClass="button1" runat="server" Text="executar" OnClick="btnSave_Click" />
                <asp:HiddenField ID="redirect" runat="server" Value="false" />
                </ContentTemplate></asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
<div style="float: left; height: 10px; width: 100%">
    &nbsp;</div>

