﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientDetailEntity.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ClientDetailEntity" %>

<%@ Register src="SelectionCriteria.ascx" tagname="SelectionCriteria" tagprefix="uc1" %>

<div class="SubContentCenter">
<div>
<div class="clientInfo">
<br /><br />
<asp:DetailsView ID = "DV_Client" BorderStyle="None" BorderWidth="0" runat = "server" AutoGenerateRows ="false">
<Fields>
<asp:TemplateField HeaderText =" Processo: " HeaderStyle-CssClass="lblBold"><ItemTemplate><span><%# Eval("balanceSheetDate","{0:MM-yyyy}")%></span></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText =" Entidade: " HeaderStyle-CssClass="lblBold"><ItemTemplate><span><%# Eval("partyName") + " (" + Eval("partyRef")+")"%></span></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText ="Grupo Económico: " HeaderStyle-CssClass="lblBold"><ItemTemplate><span><%# Eval("partyGroup")%></span></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText ="Actividade: " HeaderStyle-CssClass="lblBold"><ItemTemplate><span><%# Eval("partyActivityS")%></span></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText ="Estado Análise: " HeaderStyle-CssClass="lblBold"><ItemTemplate><span><%# Eval("partyState")%></span></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText ="Analista: " HeaderStyle-CssClass="lblBold"><ItemTemplate><span><%# Eval("user")%></span></ItemTemplate></asp:TemplateField>
</Fields>
</asp:DetailsView>
</div>

<div class="impValue"><br />
<asp:Panel ID = "Pnl_impV" runat ="server">
<label class="lblBold">Imparidade</label>
&nbsp;&nbsp;<table width ="100%">
<tr><td class="itemTableSmall">Total</td><td align="right">&nbsp;<%# this.impValues[0].ToString("#,##0.00")%></td></tr>
<tr><td class="itemTableSmall">%</td><td align="right">&nbsp;<%# this.impValues[1].ToString("#,##0.00")%></td></tr>
<tr><td class="itemTableSmall">Total (anterior)</td><td align="right">&nbsp;<%# this.impValues[2].ToString("#,##0.00")%></td></tr>
<tr><td class="itemTableSmall">% (anterior)</td><td align="right">&nbsp;<%# this.impValues[3].ToString("#,##0.00")%></td></tr>
</table>
</asp:Panel>
</div>

<div style ="float:right; width: 220px; padding-right: 100px;" >
<uc1:SelectionCriteria ID="SelectionCriteria1" runat="server" />
</div>
<div class="TitleDiv">
<label class="TitleTxt">Operações</label> 
<br />
<asp:GridView ID = "GV_Operations" runat = "server" CssClass="GridView" ShowHeader = "true" ShowFooter ="true" AllowPaging = "true" AllowSorting = "true" AutoGenerateColumns = "false" Width ="1000px" OnPageIndexChanging ="GV_Operations_PageIndexChanging" PagerSettings-Mode="NumericFirstLast" PagerStyle-HorizontalAlign="Center" FooterStyle-CssClass="GridViewRow" >
            <HeaderStyle CssClass="GridViewHeader" />
            <RowStyle CssClass="GridViewRow" />
            <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
            <SelectedRowStyle CssClass="GridViewSelectedRow" />
            <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />
<Columns>
<asp:TemplateField HeaderText = "Balcão"><ItemTemplate><asp:Label  runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.Balcao")%>' > </asp:Label></ItemTemplate>
<FooterTemplate><label class="lblBold">Totais:</label></FooterTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Operação"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.ContractReference")%>' > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Classe"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.LossClass")%>' > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Moeda"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.Currency")%>' > </asp:Label></ItemTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Capital Vincendo" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign ="Right"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.BPN_Val1_CapVin","{0:#,##0.00}")%>' > </asp:Label></ItemTemplate>
<FooterTemplate><asp:Label class="lblBold" runat ="server" Text='<%# this.sumOperations[0].ToString("#,##0.00")%>'></asp:Label></FooterTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Capital Vencido" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"><ItemTemplate><asp:Label  runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.BPN_Val2_CapVen", "{0:#,##0.00}")%>' > </asp:Label></ItemTemplate>
<FooterTemplate><asp:Label class="lblBold" runat ="server" Text='<%# this.sumOperations[1].ToString("#,##0.00")%>'></asp:Label></FooterTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Juro Vencido" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.BPN_Val5_JrsVen", "{0:#,##0.00}")%>' > </asp:Label></ItemTemplate>
<FooterTemplate><asp:Label class="lblBold" runat ="server" Text='<%# this.sumOperations[2].ToString("#,##0.00")%>'></asp:Label></FooterTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Juro Corrido" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.AccruedInterestAtBSD", "{0:#,##0.00}")%>' > </asp:Label></ItemTemplate>
<FooterTemplate><asp:Label class="lblBold" runat ="server" Text='<%# this.sumOperations[3].ToString("#,##0.00")%>'></asp:Label></FooterTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Garantias prestadas" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.BPN_Val13_GarPre", "{0:#,##0.00}")%>' > </asp:Label></ItemTemplate>
<FooterTemplate><asp:Label class="lblBold" runat ="server" Text='<%# this.sumOperations[4].ToString("#,##0.00")%>'></asp:Label></FooterTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Limite não utilizado" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "c.UndrawnAmount","{0:#,##0.00}")%>' > </asp:Label></ItemTemplate>
<FooterTemplate><asp:Label class="lblBold" runat ="server" Text='<%# this.sumOperations[5].ToString("#,##0.00")%>'></asp:Label></FooterTemplate></asp:TemplateField>
<asp:TemplateField HeaderText = "Total" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"><ItemTemplate><asp:Label runat = "server" Text ='<%#((double)DataBinder.Eval(Container.DataItem, "c.BPN_Val1_CapVin") + (double)DataBinder.Eval(Container.DataItem, "c.BPN_Val2_CapVen")+ (double)DataBinder.Eval(Container.DataItem, "c.BPN_Val5_JrsVen")+ (double)DataBinder.Eval(Container.DataItem, "c.AccruedInterestAtBSD")+ (double)DataBinder.Eval(Container.DataItem, "c.BPN_Val13_GarPre")+ (double)DataBinder.Eval(Container.DataItem, "c.UndrawnAmount")).ToString("#,##0.00")%>'> </asp:Label></ItemTemplate>
<FooterTemplate><asp:Label class="lblBold" runat ="server" Text ='<%# this.sumOperations[6].ToString("#,##0.00")%>'></asp:Label></FooterTemplate></asp:TemplateField>
</Columns>
</asp:GridView>
</div>
</div>
</div>