﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using SpiritucSI.BPN.AII.WebSite.Common;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class ucComments : SuperControl
    {
        #region PROPS
        private int v_idPartySelected;

        public int idPartySelected
        {

            get
            {
                if (v_idPartySelected == 0)
                {


                    if (Session["idParty"] == null)
                        this.Response.Redirect("~/Dashboard.aspx");
                    else
                    {
                        int id = 0;
                        Int32.TryParse(Session["idParty"].ToString(), out id);
                        if (id != 0)
                            v_idPartySelected = id;
                    }
                }

                return v_idPartySelected;
            }

            set
            {
                v_idPartySelected = value;

            }
        }


        public bool IsReadOnly { get; set; }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this.Load();
            }
        }


        protected void Load()
        {
            BLL ds = new BLL();
            GV_Comments.DataSource = ds.GetCommentsByParty(idPartySelected, 10,0);
            GV_Comments.DataBind();
            TxtB_Comments.Text = "";
            TxtB_Comments.DataBind();
            this.DataBind();
            if (IsReadOnly)
                LoadReadOnly(this);

        }

        protected void LoadReadOnly(Control c)
        {
            foreach (Control child in c.Controls)
            {
                this.LoadReadOnly(child);
            }

            if (c is CheckBox)
                ((CheckBox)c).Enabled = !this.IsReadOnly;
            if (c is TextBox)
                ((TextBox)c).Enabled = !this.IsReadOnly;
            if (c is RadioButton)
                ((RadioButton)c).Enabled = !this.IsReadOnly;
            if (c is Button)
                ((Button)c).Enabled = !this.IsReadOnly;
            if (c is LinkButton)
                ((LinkButton)c).Enabled = !this.IsReadOnly;

        }

        protected void Btt_InsertComment_Click(object sender, EventArgs e)
        {
            if (TxtB_Comments.Text.Length > 0)
            {
                BLL ds = new BLL();
                ds.InsertComment(idPartySelected, this.CurrentUser.Username, TxtB_Comments.Text);
                this.Load();
            }
                

        }
    }
}