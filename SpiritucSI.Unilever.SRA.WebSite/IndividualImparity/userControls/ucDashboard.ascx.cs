﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.Services;
using System.Web.Security;
using SpiritucSI.BPN.AII.WebSite.Common;
using System.Collections;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class ucDashboard : SuperControl
    {
        #region PROPERTIES

        #endregion

        #region PAGE_EVENTS
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                this.CurrentUser = UserService.GetOneByUserName(Context.User.Identity.Name);


                Business.PARTYSELECTED_STATE InAllocationState = PartyServices.GetState("InAllocation");
                Business.PARTYSELECTED_STATE InAnalisysState = PartyServices.GetState("InAnalisys");
                Business.PARTYSELECTED_STATE InSupervisionState = PartyServices.GetState("InSupervision");
                Business.PARTYSELECTED_STATE InValidationState = PartyServices.GetState("InValidation");
                Business.PARTYSELECTED_STATE InConclusionState = PartyServices.GetState("InConclusion");
                Business.PARTYSELECTED_STATE FinishState = PartyServices.GetState("Finish");

                string InAllocationStateName = InAllocationState != null ? InAllocationState.stateDescription : string.Empty;
                string InAnalisysStateName = InAnalisysState != null ? InAnalisysState.stateDescription : string.Empty;
                string InSupervisionStateName = InSupervisionState != null ? InSupervisionState.stateDescription : string.Empty;
                string InConclusionStateName = InConclusionState != null ? InConclusionState.stateDescription : string.Empty;
                string InValidationStateName = InValidationState != null ? InValidationState.stateDescription : string.Empty;
                
                string FinishStateName = FinishState != null ? FinishState.stateDescription : string.Empty;

                this.SubTitle1.Title = string.Format(this.GetLocalResourceObject("SubTitle1").ToString(), InAllocationStateName);
                this.SubTitle2.Title = string.Format(this.GetLocalResourceObject("SubTitle1").ToString(), InAnalisysStateName);
                this.SubTitle3.Title = string.Format(this.GetLocalResourceObject("SubTitle1").ToString(), InSupervisionStateName);
                this.SubTitle4.Title = string.Format(this.GetLocalResourceObject("SubTitle1").ToString(), InValidationStateName);
                this.SubTitle6.Title = string.Format(this.GetLocalResourceObject("SubTitle1").ToString(), InConclusionStateName);
                this.SubTitle5.Title = string.Format(this.GetLocalResourceObject("SubTitle1").ToString(), FinishStateName);

                this.InAllocation.Visible = (this.CurrentUser.IsPoolManager() || this.CurrentUser.IsProcessManager() || this.CurrentUser.IsDirector());
                this.btnBulkInAllocation.Visible = this.CurrentUser.IsPoolManager();
                if (this.InAllocation.Visible)
                {
                    this.gridInAllocation.DataSource = PartyServices.GetPartyFor(this.CurrentUser, InAllocationState.stateName);
                    this.gridInAllocation.DataBind();
                }

                this.InAnalisys.Visible = (this.CurrentUser.IsAnalyst() || this.CurrentUser.IsSupervisor() || this.CurrentUser.IsProcessManager() || this.CurrentUser.IsDirector());
                if (this.InAnalisys.Visible)
                {
                    this.gridInAnalisys.DataSource = PartyServices.GetPartyFor(this.CurrentUser, InAnalisysState.stateName);
                    this.gridInAnalisys.DataBind();
                }

                this.InSupervision.Visible = (this.CurrentUser.IsSupervisor() || this.CurrentUser.IsProcessManager() || this.CurrentUser.IsDirector());
                this.btnBulkInSupervision.Visible = this.CurrentUser.IsSupervisor();
                if (this.InSupervision.Visible)
                {
                    this.gridInSupervision.DataSource = PartyServices.GetPartyFor(this.CurrentUser, InSupervisionState.stateName);
                    this.gridInSupervision.DataBind();
                }

                this.InValidation.Visible = (this.CurrentUser.IsProcessManager() || this.CurrentUser.IsDirector());
                this.btnBulkInValidation.Visible = this.CurrentUser.IsProcessManager();
                if (this.InValidation.Visible)
                {
                    this.gridInValidation.DataSource = PartyServices.GetPartyFor(this.CurrentUser, InValidationState.stateName);
                    this.gridInValidation.DataBind();
                }

                this.InConclusion.Visible = (this.CurrentUser.IsProcessManager() || this.CurrentUser.IsDirector());
                this.btnBulkInConclusion.Visible = this.CurrentUser.IsProcessManager();
                if (this.CurrentUser.IsProcessManager() || this.CurrentUser.IsDirector())
                {
                    this.gridInConclusion.DataSource = PartyServices.GetPartyFor(this.CurrentUser, InConclusionState.stateName);
                    this.gridInConclusion.DataBind();
                }

                this.InFinish.Visible = this.CurrentUser.IsProcessManager() || this.CurrentUser.IsDirector();
                if (this.CurrentUser.IsProcessManager() || this.CurrentUser.IsDirector())
                {
                    this.gridFinish.DataSource = PartyServices.GetPartyFor(this.CurrentUser, FinishState.stateName);
                    this.gridFinish.DataBind();
                }
            }

        }
        #endregion

        #region CONTROL_EVENTS
        protected void grid_Select(object sender, Obout.Grid.GridRecordEventArgs e)
        {
            Obout.Grid.Grid grid1 = (Obout.Grid.Grid)sender;

            if (grid1.SelectedRecords != null)
            {
                foreach (Hashtable oRecord in grid1.SelectedRecords)
                {

                    Session["idParty"] = oRecord["Id"];

                    if (grid1.ID == "gridInAllocation" || (this.CurrentUser.IsDirector() && this.CurrentUser.Roles.Where(x => x.RoleType == SpiritucSI.BPN.AII.Business.Enumerators.RoleType.Functional).Count() == 1))
                    {
                        this.Response.Redirect("~/IndividualImparity/ClientView.aspx");
                    }
                    else
                    {
                        this.Response.Redirect("~/IndividualImparity/ClientDetail.aspx");
                    }
                }

            }

        }

        protected void Details_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;

            if (lnk != null)
            {
                Session["idParty"] = lnk.CommandArgument;

                if (lnk.CommandName == "gridInAllocation" ||
                   (lnk.CommandName == "gridInAnalisys" && !this.CurrentUser.IsAnalyst()) ||
                   (lnk.CommandName == "gridInSupervision" && !this.CurrentUser.IsSupervisor()) ||
                   (lnk.CommandName == "gridInValidation" && !this.CurrentUser.IsProcessManager()) ||
                   (lnk.CommandName == "gridInConclusion" && !this.CurrentUser.IsProcessManager()) || 
                    lnk.CommandName == "gridFinish" ||
                    (this.CurrentUser.IsDirector() && this.CurrentUser.Roles.Where(x => x.RoleType == SpiritucSI.BPN.AII.Business.Enumerators.RoleType.Functional).Count() == 1))
                {
                    this.Response.Redirect("~/IndividualImparity/ClientView.aspx");
                }            
                else
                {
                    this.Response.Redirect("~/IndividualImparity/ClientDetail.aspx");
                }
            }
        }

        #endregion



    }
}