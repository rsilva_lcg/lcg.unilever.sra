﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucPoolManager.ascx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ucPoolManager" %>
<%@ Register Src="~/UserControls/Common/ucSubTitle.ascx" TagName="SubTitle" TagPrefix="uc1" %>
<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<uc1:SubTitle ID="SubTitle2" runat="server" Title='<%$Resources: SubTitle1 %>' Maximized="true"
    TargetDivId="List" Static="false" />

<div id="List" runat="server" class="SubContentCenterTable">
    <div style="float: left; width: 100%">
        <obout:Grid ID="grid1" Width="100%" Language="pt" AutoPostBackOnSelect="true"
            AllowPaging="false" PageSize="2000" AutoGenerateColumns="false" AllowMultiRecordSelection="false"
            AllowRecordSelection="false" AllowFiltering="false"  CallbackMode="false" Serialize="false"
            AllowPageSizeSelection="false" AllowSorting="false"
            AllowAddingRecords="false" runat="server" FolderStyle="~/App_Themes/MainTheme/Grid/Styles/Style_5"
            FolderLocalization="~/App_Themes/MainTheme/Grid/localization" OnRowDataBound="grid1_RowDataBound" 
            onselect="grid_Select" >
            <Columns>
                    <obout:Column ID="Column1" Width="100%" DataField="Description" HeaderText='Entidade' runat="server" TemplateID="GridTemplateLnk1"></obout:Column>
                    <obout:Column ID="Column2" Width="100" DataField="Reference" HeaderText='Referência' runat="server" TemplateID="GridTemplate1"></obout:Column>
                    <obout:Column ID="Column3" Width="95" DataField="Balcao" HeaderText='Balcão' runat="server" TemplateID="GridTemplate1"></obout:Column>
                    <obout:Column ID="Column4" Width="200" DataField="GroupName" HeaderText='Grupo de Análise' runat="server" TemplateID="GridTemplate1"></obout:Column>
                    <obout:Column ID="Column5" Width="200" DataField="GroupId" runat="server" TemplateID="GridTemplate1" Visible="false"></obout:Column>
					<obout:Column ID="ColumnDdl" Width="275" runat="server" DataField="Id" TemplateID="GridTemplateDdl" HeaderText="Analista"></obout:Column>
					<obout:Column ID="Column6" Width="0" DataField="username" runat="server" Visible ="false"></obout:Column>                  
            </Columns>
            <Templates>
                <obout:GridTemplate runat="server" id="GridTemplate1">
                        <Template>
                           <asp:Label ID="lbl1" runat="server" Text='<%# Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" id="GridTemplateDdl">
                        <Template>
							<asp:DropDownList ID="ddl" runat="server" PartyID='<%# Container.Value %>' Width="250px" AppendDataBoundItems="true" DataTextField="name" DataValueField="username"><asp:ListItem Text="-- seleccione um analista --" Value="0"></asp:ListItem></asp:DropDownList>
                        </Template>
                    </obout:GridTemplate>
                     <obout:GridTemplate runat="server" ID="GridTemplateLnk1">
                        <Template>
                            <asp:LinkButton Id="lnk1" runat="server" Text='<%# Container.Value %>' OnClick="Details_Click" CommandArgument='<%#  Container.DataItem["Id"] %>' CommandName="gridFinish"></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
            </Templates>
        </obout:Grid>
    </div>
    <div id="actionButtons" runat="server" class="Row" style="padding-top: 10px;">
        <div class="form-text">
            <asp:Label ID="lbl7" runat="server"></asp:Label></div>
        <div style="float: left; width: 100%">
            <div class="btnLeft">
                <asp:Button ID="bntCancel" CssClass="button1" runat="server" Text='cancelar' PostBackUrl="~/Dashboard.aspx" />
            </div>
            <asp:UpdatePanel ID="up1" runat="server"><ContentTemplate>
            <div class="btnRight">
                <asp:Button ID="btnSave" CssClass="button1" runat="server" Text="executar" OnClick="btnSave_Click" />
                <asp:HiddenField ID="redirect" runat="server" Value="false" />
            </div>
            </ContentTemplate></asp:UpdatePanel>
        </div>
    </div>
</div>

<div style="float: left; height: 10px; width: 100%">
    &nbsp;</div>