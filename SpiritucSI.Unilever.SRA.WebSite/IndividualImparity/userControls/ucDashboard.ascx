﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucDashboard.ascx.cs"
    Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls.ucDashboard" %>
<%@ Register Src="~/UserControls/Common/ucSubTitle.ascx" TagName="SubTitle" TagPrefix="uc1" %>
<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<%@ Register src="~/UserControls/Common/ucTitle.ascx" TagPrefix="uc1" TagName="ucTitle" %>
<%@ Register Src="~/UserControls/Common/MyHelper.ascx" TagPrefix="uc1" TagName="ucMyHelper" %>
<uc1:ucTitle ID="Title" runat="server"  Title='<%$Resources: Title1 %>' />
<div id="InAllocation" runat="server" style="float:left; position:relative">
    <div style="position:absolute; top:10px;right:10px"><uc1:ucMyHelper ID="ucMyHelper1" runat="server" Key="DashboardInAllocation"  /></div>
    <uc1:SubTitle ID="SubTitle1" runat="server" Title='<%$Resources: SubTitle1 %>' Maximized="true"
        TargetDivId="List1" Static="false" />
    <div id="List1" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 100%">
            <obout:Grid ID="gridInAllocation" Width="100%" Language="pt" AutoPostBackOnSelect="true"
                Serialize="true" AllowPaging="true" PageSize="15" AutoGenerateColumns="false"
                AllowMultiRecordSelection="false" AllowRecordSelection="false" AllowFiltering="true"
                AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/MainTheme/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/MainTheme/Grid/localization"
                OnSelect="grid_Select">
                <Columns>
                    <obout:Column ID="Column1" Width="100%" DataField="Description" HeaderText='Entidade'
                        runat="server" TemplateId="GridTemplate2">
                    </obout:Column>
                    <obout:Column ID="Column2" Width="80" DataField="Reference" HeaderText='Ref.' runat="server"
                        TemplateId="GridTemplate1">
                    </obout:Column>
                    <obout:Column ID="Column3" Width="90" DataField="Balcao" HeaderText='Balcão' runat="server"
                        TemplateId="GridTemplate1">
                    </obout:Column>
                    <obout:Column ID="Column4" Width="200" DataField="GroupName" HeaderText='Grupo de Análise'
                        runat="server" TemplateId="GridTemplate1">
                    </obout:Column>
                    <obout:Column ID="Column9" Width="100%" DataField="Id" HeaderText=' ' runat="server"
                        TemplateId="GridTemplate1" Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="GridTemplate1">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%# Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplate2">
                        <Template>
                            <asp:LinkButton Id="lnk1" runat="server" Text='<%# Container.Value %>' OnClick="Details_Click" CommandArgument='<%#  Container.DataItem["Id"] %>' CommandName="gridInAllocation"></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
        <div class="Row" style="padding-top: 10px;">
            <div class="form-text">
                <asp:Label ID="Label1" runat="server"></asp:Label></div>
            <div style="float: left; width: 100%">
                <div class="btnRight">
                    <asp:Button ID="btnBulkInAllocation" CssClass="button1" runat="server" Text='gestão em bulk'
                        PostBackUrl="~/IndividualImparity/PoolManager.aspx" />
                </div>
            </div>
        </div>
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="InAnalisys" runat="server">
    <uc1:SubTitle ID="SubTitle2" runat="server" Title='<%$Resources: SubTitle1 %>' Maximized="true"
        TargetDivId="List2" Static="false" />
    <div id="List2" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 100%">
            <obout:Grid ID="gridInAnalisys" Width="100%" Language="pt" AutoPostBackOnSelect="true"
                Serialize="true" AllowPaging="true" PageSize="15" AutoGenerateColumns="false"
                AllowMultiRecordSelection="false" AllowRecordSelection="false" AllowFiltering="true"
                AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/MainTheme/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/MainTheme/Grid/localization"
                OnSelect="grid_Select">
                <Columns>
                    <obout:Column ID="Column41" Width="100%" DataField="Description" HeaderText='Entidade'
                        runat="server" TemplateId="GridTemplate12">
                    </obout:Column>
                    <obout:Column ID="Column42" Width="80" DataField="Reference" HeaderText='Ref.' runat="server"
                        TemplateId="GridTemplate11">
                    </obout:Column>
                    <obout:Column ID="Column43" Width="90" DataField="Balcao" HeaderText='Balcão' runat="server"
                        TemplateId="GridTemplate11">
                    </obout:Column>
                    <obout:Column ID="Column44" Width="200" DataField="GroupName" HeaderText='Grupo de Análise'
                        runat="server" TemplateId="GridTemplate11">
                    </obout:Column>
                    <obout:Column ID="Column49" Width="100%" DataField="Id" HeaderText=' ' runat="server"
                        TemplateId="GridTemplateLink11" Visible="false">
                    </obout:Column>
                    
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="GridTemplate11">
                        <Template>
                            <asp:Label ID="lbl11" runat="server" Text='<%# Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplate12">
                        <Template>
                            <asp:LinkButton Id="lnk11" runat="server" Text='<%# Container.Value %>' OnClick="Details_Click" CommandArgument='<%#  Container.DataItem["Id"] %>' CommandName="gridInAnalisys"></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
        <%--<div class="Row" style="padding-top: 10px;">
            <div class="form-text">
                <asp:Label ID="Label10" runat="server"></asp:Label></div>
            <div style="float: left; width: 100%">
                <div class="btnRight">
                    <asp:Button ID="btnBulkInAnalisys" CssClass="button1" runat="server" Text='gestão em bulk'
                        PostBackUrl="~/IndividualImparity/BulkDashboard.aspx?status=InAnalisys" />
                </div>
            </div>
        </div>--%>
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="InSupervision" runat="server">
    <uc1:SubTitle ID="SubTitle3" runat="server" Title='<%$Resources: SubTitle1 %>' Maximized="true"
        TargetDivId="List3" Static="false" />
    <div id="List3" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 100%">
            <obout:Grid ID="gridInSupervision" Width="100%" Language="pt" AutoPostBackOnSelect="true"
                Serialize="true" AllowPaging="true" PageSize="15" AutoGenerateColumns="false"
                AllowMultiRecordSelection="false" AllowRecordSelection="false" AllowFiltering="true"
                AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/MainTheme/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/MainTheme/Grid/localization"
                OnSelect="grid_Select">
                <Columns>
                    <obout:Column ID="Column11" Width="100%" DataField="Description" HeaderText='Entidade'
                        runat="server" TemplateId="GridTemplate212">
                    </obout:Column>
                    <obout:Column ID="Column12" Width="80" DataField="Reference" HeaderText='Ref.' runat="server"
                        TemplateId="GridTemplate21">
                    </obout:Column>
                    <obout:Column ID="Column13" Width="90" DataField="Balcao" HeaderText='Balcão' runat="server"
                        TemplateId="GridTemplate21">
                    </obout:Column>
                    <obout:Column ID="Column14" Width="200" DataField="GroupName" HeaderText='Grupo de Análise'
                        runat="server" TemplateId="GridTemplate21">
                    </obout:Column>
                    <obout:Column ID="Column15" Width="100" ItemStyle-HorizontalAlign="Right" DataField="ImparityTotal" HeaderText='Imp. Total'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplateCurrency21">
                    </obout:Column>
                    <obout:Column ID="Column16" Width="100" ItemStyle-HorizontalAlign="Right" DataField="ImparityPercentage" HeaderText='Imp.(%)'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplatePercentage21">
                    </obout:Column>
                    <obout:Column ID="Column17" Width="100" ItemStyle-HorizontalAlign="Right" DataField="PreviousImparityTotal" HeaderText='Imp. Ant. Total'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplateCurrency21">
                    </obout:Column>
                    <obout:Column ID="Column18" Width="100" ItemStyle-HorizontalAlign="Right" DataField="PreviousImparityPercentage" HeaderText='Imp. Ant.(%)'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplatePercentage21">
                    </obout:Column>
                    <obout:Column ID="Column19" Width="100%" DataField="Id" HeaderText=' ' runat="server"
                        TemplateId="GridTemplate21" Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="GridTemplate21">
                        <Template>
                            <asp:Label ID="lbl21" runat="server" Text='<%# Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplateCurrency21">
                        <Template>
                            <asp:Label ID="lbl210" runat="server" Text='<%# Container.Value != "0" ? string.Format(" {0:#,##0.00}",Container.Value!=""?Double.Parse(Container.Value):0) : "0" %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplatePercentage21">
                        <Template>
                            <asp:Label ID="lbl211" runat="server" Text='<%# Container.Value != "0" ? string.Format("{0:N2}%",Container.Value): "0" %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplate212">
                        <Template>
                            <asp:LinkButton Id="lnk212" runat="server" Text='<%# Container.Value %>' OnClick="Details_Click" CommandArgument='<%#  Container.DataItem["Id"] %>' CommandName="gridInSupervision"></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
        <div class="Row" style="padding-top: 10px;">
            <div class="form-text">
                <asp:Label ID="Label12" runat="server"></asp:Label></div>
            <div style="float: left; width: 100%">
                <div class="btnRight">
                    <asp:Button ID="btnBulkInSupervision" CssClass="button1" runat="server" Text='gestão em bulk'
                        PostBackUrl="~/IndividualImparity/BulkDashboard.aspx?status=InSupervision" />
                </div>
            </div>
        </div>
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="InValidation" runat="server">
    <uc1:SubTitle ID="SubTitle4" runat="server" Title='<%$Resources: SubTitle1 %>' Maximized="true"
        TargetDivId="List4" Static="false" />
    <div id="List4" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 100%">
            <obout:Grid ID="gridInValidation" Width="100%" Language="pt" AutoPostBackOnSelect="true"
                Serialize="true" AllowPaging="true" PageSize="15" AutoGenerateColumns="false"
                AllowMultiRecordSelection="false" AllowRecordSelection="false" AllowFiltering="true"
                AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/MainTheme/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/MainTheme/Grid/localization"
                OnSelect="grid_Select">
                <Columns>
                    <obout:Column ID="Column21" Width="100%" DataField="Description" HeaderText='Entidade'
                        runat="server" TemplateId="GridTemplate3">
                    </obout:Column>
                    <obout:Column ID="Column22" Width="80" DataField="Reference" HeaderText='Ref.' runat="server"
                        TemplateId="GridTemplate31">
                    </obout:Column>
                    <obout:Column ID="Column23" Width="90" DataField="Balcao" HeaderText='Balcão' runat="server"
                        TemplateId="GridTemplate31">
                    </obout:Column>
                    <obout:Column ID="Column24" Width="200" DataField="GroupName" HeaderText='Grupo de Análise'
                        runat="server" TemplateId="GridTemplate31">
                    </obout:Column>
                    <obout:Column ID="Column25" Width="100" DataField="ImparityTotal" HeaderText='Imp. Total'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplateCurrency31">
                    </obout:Column>
                    <obout:Column ID="Column26" Width="100" DataField="ImparityPercentage" HeaderText='Imp.(%)'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplatePercentage31">
                    </obout:Column>
                    <obout:Column ID="Column27" Width="100" DataField="PreviousImparityTotal" HeaderText='Imp. Ant. Total'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplateCurrency31">
                    </obout:Column>
                    <obout:Column ID="Column28" Width="100" DataField="PreviousImparityPercentage" HeaderText='Imp. Ant.(%)'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplatePercentage31">
                    </obout:Column>
                    <obout:Column ID="Column29" Width="100%" DataField="Id" HeaderText=' ' runat="server"
                        TemplateId="GridTemplate31" Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="GridTemplate31">
                        <Template>
                            <asp:Label ID="lbl31" runat="server" Text='<%# Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplateCurrency31">
                        <Template>
                            <asp:Label ID="lbl310" runat="server" Text='<%# Container.Value != "0" ? string.Format("{0:#,##0.00}",Container.Value!=""?Double.Parse(Container.Value):0) : "0" %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplatePercentage31">
                        <Template>
                            <asp:Label ID="lbl311" runat="server" Text='<%# Container.Value != "0" ? string.Format("{0:N}%",Container.Value): "0" %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplate3">
                        <Template>
                            <asp:LinkButton Id="lnk312" runat="server" Text='<%# Container.Value %>' OnClick="Details_Click" CommandArgument='<%#  Container.DataItem["Id"] %>' CommandName="gridInValidation"></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
        <div class="Row" style="padding-top: 10px;">
            <div class="form-text">
                <asp:Label ID="Label11" runat="server"></asp:Label></div>
            <div style="float: left; width: 100%">
                <div class="btnRight">
                    <asp:Button ID="btnBulkInValidation" CssClass="button1" runat="server" Text='gestão em bulk'
                        PostBackUrl="~/IndividualImparity/BulkDashboard.aspx?status=InValidation" />
                </div>
            </div>
        </div>
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="InConclusion" runat="server">
    <uc1:SubTitle ID="SubTitle6" runat="server" Title='<%$Resources: SubTitle1 %>' Maximized="true"
        TargetDivId="List6" Static="false" />
    <div id="List6" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 100%">
            <obout:Grid ID="gridInConclusion" Width="100%" Language="pt" AutoPostBackOnSelect="true"
                Serialize="true" AllowPaging="true" PageSize="15" AutoGenerateColumns="false"
                AllowMultiRecordSelection="false" AllowRecordSelection="false" AllowFiltering="true"
                AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/MainTheme/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/MainTheme/Grid/localization"
                OnSelect="grid_Select">
                <Columns>
                    <obout:Column ID="Column5" Width="100%" DataField="Description" HeaderText='Entidade'
                        runat="server" TemplateId="GridTemplate7">
                    </obout:Column>
                    <obout:Column ID="Column6" Width="80" DataField="Reference" HeaderText='Ref.' runat="server"
                        TemplateId="GridTemplate4">
                    </obout:Column>
                    <obout:Column ID="Column7" Width="90" DataField="Balcao" HeaderText='Balcão' runat="server"
                        TemplateId="GridTemplate4">
                    </obout:Column>
                    <obout:Column ID="Column8" Width="200" DataField="GroupName" HeaderText='Grupo de Análise'
                        runat="server" TemplateId="GridTemplate4">
                    </obout:Column>
                    <obout:Column ID="Column10" Width="100" DataField="ImparityTotal" HeaderText='Imp. Total'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplate5">
                    </obout:Column>
                    <obout:Column ID="Column20" Width="100" DataField="ImparityPercentage" HeaderText='Imp.(%)'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplate6">
                    </obout:Column>
                    <obout:Column ID="Column30" Width="100" DataField="PreviousImparityTotal" HeaderText='Imp. Ant. Total'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplate5">
                    </obout:Column>
                    <obout:Column ID="Column39" Width="100" DataField="PreviousImparityPercentage" HeaderText='Imp. Ant.(%)'
                        HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplate6">
                    </obout:Column>
                    <obout:Column ID="Column40" Width="100%" DataField="Id" HeaderText=' ' runat="server"
                        TemplateId="GridTemplate31" Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="GridTemplate4">
                        <Template>
                            <asp:Label ID="lbl31" runat="server" Text='<%# Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplate5">
                        <Template>
                            <asp:Label ID="lbl310" runat="server" Text='<%# Container.Value != "0" ? string.Format("{0:#,##0.00}",Container.Value!=""?Double.Parse(Container.Value):0) : "0" %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplate6">
                        <Template>
                            <asp:Label ID="lbl311" runat="server" Text='<%# Container.Value != "0" ? string.Format("{0:N2}%",Container.Value): "0" %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="GridTemplate7">
                        <Template>
                            <asp:LinkButton Id="lnk312" runat="server" Text='<%# Container.Value %>' OnClick="Details_Click" CommandArgument='<%#  Container.DataItem["Id"] %>' CommandName="gridInValidation"></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
        <div class="Row" style="padding-top: 10px;">
            <div class="form-text">
                <asp:Label ID="Label2" runat="server"></asp:Label></div>
            <div style="float: left; width: 100%">
                <div class="btnRight">
                    <asp:Button ID="btnBulkInConclusion" CssClass="button1" runat="server" Text='gestão em bulk'
                        PostBackUrl="~/IndividualImparity/BulkDashboard.aspx?status=InConclusion" />
                </div>
            </div>
        </div>
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="InFinish" runat="server">
<uc1:SubTitle ID="SubTitle5" runat="server" Title='<%$Resources: SubTitle1 %>' Maximized="true"
    TargetDivId="List5" Static="false" />
<div id="List5" runat="server" class="SubContentCenterTable">
    <div style="float: left; width: 100%">
        <obout:Grid ID="gridFinish" Width="100%" Language="pt" AutoPostBackOnSelect="true"
            Serialize="true" AllowPaging="true" PageSize="15" AutoGenerateColumns="false"
            AllowMultiRecordSelection="false" AllowRecordSelection="false" AllowFiltering="true"
            AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
            runat="server" FolderStyle="~/App_Themes/MainTheme/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/MainTheme/Grid/localization"
            OnSelect="grid_Select">
            <Columns>
                <obout:Column ID="Column31" Width="100%" DataField="Description" HeaderText='Entidade'
                    runat="server" TemplateId="GridTemplate42">
                </obout:Column>
                <obout:Column ID="Column32" Width="80" DataField="Reference" HeaderText='Ref.' runat="server"
                    TemplateId="GridTemplate41">
                </obout:Column>
                <obout:Column ID="Column33" Width="90" DataField="Balcao" HeaderText='Balcão' runat="server"
                    TemplateId="GridTemplate41">
                </obout:Column>
                <obout:Column ID="Column34" Width="200" DataField="GroupName" HeaderText='Grupo de Análise'
                    runat="server" TemplateId="GridTemplate41">
                </obout:Column>
                <obout:Column ID="Column35" Width="100" DataField="ImparityTotal" HeaderText='Imp. Total'
                    HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplateCurrency41">
                </obout:Column>
                <obout:Column ID="Column36" Width="100" DataField="ImparityPercentage" HeaderText='Imp.(%)'
                    HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplatePercentage41">
                </obout:Column>
                <obout:Column ID="Column37" Width="100" DataField="PreviousImparityTotal" HeaderText='Imp. Ant. Total'
                    HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplateCurrency41">
                </obout:Column>
                <obout:Column ID="Column38" Width="100" DataField="PreviousImparityPercentage" HeaderText='Imp. Ant.(%)'
                    HeaderStyle-Wrap="true" runat="server" TemplateId="GridTemplatePercentage41">
                </obout:Column>
                <obout:Column ID="Column391" Width="100%" DataField="Id" HeaderText=' ' runat="server"
                        TemplateId="GridTemplate41" Visible="false">
                    </obout:Column>
            </Columns>
            <Templates>
                <obout:GridTemplate runat="server" ID="GridTemplate41">
                    <Template>
                        <asp:Label ID="lbl41" runat="server" Text='<%# Container.Value %>'></asp:Label>
                    </Template>
                </obout:GridTemplate>
                <obout:GridTemplate runat="server" ID="GridTemplateCurrency41">
                    <Template>
                        <asp:Label ID="lbl410" runat="server" Text='<%# Container.Value != "0" ? string.Format("{0:#,##0.00}",Container.Value!=""?Double.Parse(Container.Value):0) : "0" %>'></asp:Label>
                    </Template>
                </obout:GridTemplate>
                <obout:GridTemplate runat="server" ID="GridTemplatePercentage41">
                    <Template>
                        <asp:Label ID="lbl411" runat="server" Text='<%# Container.Value != "0" ? string.Format("{0:N2}%",Container.Value): "0" %>'></asp:Label>
                    </Template>
                </obout:GridTemplate>
                <obout:GridTemplate runat="server" ID="GridTemplate42">
                        <Template>
                            <asp:LinkButton Id="lnk412" runat="server" Text='<%# Container.Value %>' OnClick="Details_Click" CommandArgument='<%#  Container.DataItem["Id"] %>' CommandName="gridFinish"></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
            </Templates>
        </obout:Grid>
    </div>
</div>
<div style="float: left; height: 10px; width: 100%">
    &nbsp;</div>
    </div>
