﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class ImpResume : System.Web.UI.UserControl
    {
        #region PROPS
        private int v_idPartySelected;

        public int idPartySelected
        {

            get
            {
                if (v_idPartySelected == 0)
                {
                    int id = 0;
                    if (Session["idParty"] != null)
                    {
                        Int32.TryParse(Session["idParty"].ToString(), out id);
                        if (id != 0)
                            v_idPartySelected = id;
                    }
                    else
                        this.PageRedirect();

                }

                return v_idPartySelected;
            }

            set
            {
                v_idPartySelected = value;

            }
        }

        private void PageRedirect()
        {
            this.Response.Redirect("~/Dashboard.aspx");
        }


        public double[] ImpValues{get; set;}

        #endregion


        protected void Page_PreRender(object sender, EventArgs e)
        {
                IndividualImparityCalc ds  = new IndividualImparityCalc();

                //R_ImpSummary.DataSource = ds.GetImpPartyResume(idPartySelected);
                //R_ImpSummary.DataBind();
                this.ImpValues = ds.GetImpPartyResume(idPartySelected);
                Pnl_Sum.DataBind();

        }

    }
}