﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity.userControls
{
    public partial class AnalisysSubmenu : System.Web.UI.UserControl
    {
        private string v_SubMenu;
        public bool Enabled { get; set; }
        public string SubMenu { get; set; }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (true)
            {
                SiteMapProvider smp = SM_Analisys.Provider;
                if (SubMenu == null)
                    if (this.Request.QueryString["SubMenu"] != null && this.Request.QueryString["SubMenu"] !="")
                        SubMenu = this.Request.QueryString["SubMenu"];
                    else
                        SubMenu = "Questionnaires";

                if (smp != null && smp.RootNode != null && smp.RootNode.ChildNodes != null)
                {
                    this.rptTopMenu.DataSource = (from SiteMapNode obj in smp.RootNode.ChildNodes select obj).ToList();
                    this.rptTopMenu.DataBind();
                }

                Control hdrCtl = null;
                try
                {
                    switch (SubMenu)
                    {
                        case "Questionnaires":
                            {
                                hdrCtl = LoadControl("./Questionnaires.ascx");
                                SubMenuCnt.Controls.Add(hdrCtl);
                                break;
                            }
                        case "Values":
                            {
                                hdrCtl = LoadControl("./Values.ascx");
                                SubMenuCnt.Controls.Add(hdrCtl);
                                break;
                            }
                        case "Comments":
                            {
                                hdrCtl = LoadControl("./ucComments.ascx");
                                SubMenuCnt.Controls.Add(hdrCtl);
                                break;
                            }

                        case "Colaterals":
                            {
                                hdrCtl = LoadControl("./ucColaterals.ascx");
                                SubMenuCnt.Controls.Add(hdrCtl);
                                break;
                            }
                        case "Analisys":
                            {
                                hdrCtl = LoadControl("./ucAnalisys.ascx");
                                SubMenuCnt.Controls.Add(hdrCtl);
                                break;
                            }
                        default: break;

                    }
                }
                catch { }
            }

 
        }

        protected void rptTopMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                SiteMapNode node = ((SiteMapNode)e.Item.DataItem);
                this.Enabled = (node.Key.ToLower() == this.Request.Url.PathAndQuery.ToLower() || (SubMenu!=null && node.Key.Contains(SubMenu.ToLower())));


                HtmlGenericControl Left = ((HtmlGenericControl)e.Item.FindControl("Left"));
                HtmlGenericControl Center = ((HtmlGenericControl)e.Item.FindControl("Center"));
                HtmlGenericControl Right = ((HtmlGenericControl)e.Item.FindControl("Right"));

                Left.Attributes["class"] = ((this.Enabled) ? "On" : "Off") + Left.ID;
                Center.Attributes["class"] = ((this.Enabled) ? "On" : "Off") + Center.ID;
                Right.Attributes["class"] = ((this.Enabled) ? "On" : "Off") + Right.ID;                
            }
        }



        protected void LB_Option_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            this.Response.Redirect(lb.CommandArgument);

        }



    }
}