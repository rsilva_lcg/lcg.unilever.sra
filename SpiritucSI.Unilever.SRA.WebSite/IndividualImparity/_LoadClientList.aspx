﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="true"
 CodeBehind="_LoadClientList.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.LoadClientList" Theme="MainTheme" meta:resourcekey="PageResource1" uiculture="auto" %>
<%@ Register Src="../UserControls/Common/ucPageTitle.ascx" TagName="ucPageTitle" TagPrefix="uc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
<uc1:ucPageTitle ID = "UCTitle" runat ="server" Title ="Lista de Entidades" />
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top">
        <td align = "left" class="txtCenter">
        <br />
        <asp:GridView ID="GV_Clients" runat="server" AllowPaging="True" CssClass="GridView" AllowSorting="True" AutoGenerateColumns="False" Width="1000px" PageSize="20" PagerSettings-Mode = "NumericFirstLast" PagerStyle-HorizontalAlign ="Center" OnPageIndexChanging ="GV_Clients_PageIndexChanging" >
            <HeaderStyle CssClass="GridViewHeader" />
            <RowStyle CssClass="GridViewRow" />
            <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
            <SelectedRowStyle CssClass="GridViewSelectedRow" />
            <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />                 
            <Columns>
            <asp:TemplateField ItemStyle-HorizontalAlign ="Left" HeaderText = "Entidade" >
            <ItemTemplate>
            <asp:LinkButton Id = "LB_Name" Text = <%#DataBinder.Eval(Container.DataItem, "p2.PartyDescription")%>  runat = "server" OnClick = "LB_Name_Click" CommandArgument = '<%#DataBinder.Eval(Container.DataItem, "p.idPartySelected")%>' ></asp:LinkButton>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText = "Referencia"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "partyReference")%>' > </asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText = "Balcão"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "p2.Balcao")%>' > </asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText = "Estado"><ItemTemplate><asp:Label runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "stateName")%>' > </asp:Label></ItemTemplate></asp:TemplateField>
            </Columns>
        </asp:GridView> 
        </td>

    </tr>
    </table>
</asp:Content>

