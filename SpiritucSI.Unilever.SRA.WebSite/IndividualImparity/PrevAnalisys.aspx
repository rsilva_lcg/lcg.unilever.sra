﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" 
AutoEventWireup="true" CodeBehind="PrevAnalisys.aspx.cs" 
Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.PrevAnalisys" 
Theme="MainTheme" meta:resourcekey="PageResource1" validateRequest="false"  %>
<%@ Register Src="../UserControls/Common/ucPageTitle.ascx" TagName="ucTitle" TagPrefix="uc1" %>
<%@ Register Src="./UserControls/ClientDetailEntity.ascx" TagName="ucClient" TagPrefix="uc2" %>
<%@ Register Src="./UserControls/Questionnaires.ascx" TagName="ucQuestionnaires" TagPrefix="uc3" %>
<%@ Register Src="./UserControls/AnalisysSubMenu.ascx" TagName="ucAnalisysSubMenu" TagPrefix="uc4" %>


<asp:Content ID="Content3" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
<div class="padBottom">
<uc1:ucTitle ID = "UCTitle" runat ="server" Title = "Detalhes de Cliente" />
</div>
<uc2:ucClient ID = "UCClient" runat ="server" isSaveVisible ="true" EnableViewState = "true"/>
<uc4:ucAnalisysSubMenu ID ="UcAnalisys" runat = "server" EnableViewState ="true" />
</asp:Content>
