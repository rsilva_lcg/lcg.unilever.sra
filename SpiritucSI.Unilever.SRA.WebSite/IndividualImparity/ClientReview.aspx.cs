﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using System.Configuration;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity
{
    public partial class ClientReview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BLL ds = new BLL();

            int idCompany = ds.GetCompanyId(ConfigurationManager.AppSettings["CompanyName"].ToString());

            GV_ClientReview.DataSource = ds.GetPartiesReviewInfoByProcess(ds.GetCurrentProcessId(idCompany));
            GV_ClientReview.DataBind();


        }
    }
}
