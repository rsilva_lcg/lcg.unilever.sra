﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="true" CodeBehind="LoadPoolList.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.LoadPoolList" Theme="MainTheme" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%@ Register Src="../UserControls/Common/ucPageTitle.ascx" TagName="ucPageTitle" TagPrefix="uc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
<uc1:ucPageTitle ID = "UCTitle" runat ="server" Title ="Atribuição de Análise de Entidades" />
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top">
        <td align = "left" style = " text-align: center;">
        <br />
        <asp:GridView ID= "GV_Clients" CssClass="GridView" runat="server" AllowPaging = "True" 
                AllowSorting ="True" AutoGenerateColumns = "False" DataKeyNames="idPallocation" Width = "1000px" PageSize="20" OnRowDataBound= "GV_Clients_RowDataBound" >
                        <HeaderStyle CssClass="GridViewHeader" />
            <RowStyle CssClass="GridViewRow" />
            <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
            <SelectedRowStyle CssClass="GridViewSelectedRow" />
            <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />  
            <Columns>
            <asp:TemplateField ItemStyle-HorizontalAlign ="Left" HeaderText = "Entidade" >
            <ItemTemplate>
            <asp:LinkButton Id = "LB_Name" Text = '<%#DataBinder.Eval(Container.DataItem, "p.PartyDescription")%>'  runat = "server" OnClick = "LB_Name_Click" CommandArgument = <%#DataBinder.Eval(Container.DataItem, "ps.idPartySelected")%> ></asp:LinkButton>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText = "Balcão"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "p.Balcao")%>' > </asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText = "Estado"><ItemTemplate><asp:Label ID="Label2" runat = "server" Text ='<%#DataBinder.Eval(Container.DataItem, "stateName")%>' > </asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="20px" HeaderText = "Analista"><ItemTemplate><asp:DropDownList ID ="DL_Users" Width="180" runat ="server"></asp:DropDownList><asp:HiddenField ID = "HF_idUsr" runat = "server" Value =<%#DataBinder.Eval(Container.DataItem, "idUser")%> /> </ItemTemplate> </asp:TemplateField>
            </Columns>
        </asp:GridView> 
        </td>

    </tr>
    <tr><td>
    <div class="PadTop">
        <asp:Button ID ="Btt_Save" runat = "server" Text ="Guardar" OnClick="LB_Save_Click" /></td></tr>
        </div>
    </table>
</asp:Content>

