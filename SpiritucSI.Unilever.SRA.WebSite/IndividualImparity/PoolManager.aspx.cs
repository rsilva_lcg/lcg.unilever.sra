﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.WebSite.Common;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity
{
    public partial class PoolManager : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), "redirectScript", "if (document.getElementById('" + this.ucPoolManager.GetRedirect() + "').value == 'true') { " + Utils.RedirectScriptUpdatePanel("../Dashboard.aspx", 1500) + " }", true);
        }

    }
}
