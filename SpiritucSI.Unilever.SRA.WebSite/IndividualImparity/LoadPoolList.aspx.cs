﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.Business.OtherClasses;
using System.Configuration;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity
{
    public partial class LoadPoolList : System.Web.UI.Page
    {


        private object populateUsers()
        {
            BLL ds = new BLL();
            //adicionar logica para gupos de utilizador
            //return ds.GetUsersByGroup(1);
            return null;

        }


        private void SaveUserSelection()
        {
            BLL ds = new BLL();
            foreach (GridViewRow gr in GV_Clients.Rows)
            {
                 DropDownList dlU = (DropDownList)gr.Cells[3].FindControl("DL_Users");
                 int idPallocation = Int32.Parse((string)GV_Clients.DataKeys[gr.DataItemIndex].Value.ToString());
                 if (idPallocation == 0)
                     ds.InsertPartyUsers(null, idPallocation, Int32.Parse(dlU.SelectedValue));
                 else
                     ds.UpdatePartyUsers(null, idPallocation, Int32.Parse(dlU.SelectedValue));
            }
            this.Load();
            
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
                this.Load();
        }

        protected void Load()
        {
            BLL ds = new BLL();

            int idCompany = ds.GetCompanyId(ConfigurationManager.AppSettings["CompanyName"].ToString());

            int idProcess = ds.GetCurrentProcessId(idCompany);

            //Logica de utilizador: Grupo
            GV_Clients.DataSource = new BLL().GetPartySUsersByGroup(idProcess, 1, 20, 0);
            GV_Clients.DataBind();

        }

        protected void LB_Name_Click(object sender,  EventArgs e)
        {
            
            LinkButton lb = (LinkButton)sender;

            Session["idParty"] = lb.CommandArgument;
            this.Response.Redirect("~/IndividualImparity/ClientDetail.aspx");
            
        }

        protected void GV_Clients_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.FindControl("DL_Users") != null && e.Row.FindControl("DL_Users") is DropDownList && e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                ((DropDownList)e.Row.FindControl("DL_Users")).DataSource = this.populateUsers();
                ((DropDownList)e.Row.FindControl("DL_Users")).DataValueField = "idUser";
                ((DropDownList)e.Row.FindControl("DL_Users")).DataTextField = "userName";
                if (((HiddenField)e.Row.FindControl("HF_idUsr")).Value != "0")
                    ((DropDownList)e.Row.FindControl("DL_Users")).SelectedValue = ((HiddenField)e.Row.FindControl("HF_idUsr")).Value;
                ((DropDownList)e.Row.FindControl("DL_Users")).DataBind();
            }

        }

        protected void LB_Save_Click(object sender, EventArgs e)
        {
            this.SaveUserSelection();

        }


    }

    
}
