﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="true" CodeBehind="ClientReview.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.ClientReview" Theme="MainTheme" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%@ Register Src="../UserControls/Common/ucPageTitle.ascx" TagName="ucTitle" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/Common/ucPageTitle.ascx" TagName="ucPageTitle" TagPrefix="uc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
<uc1:ucPageTitle ID = "UCTitle" runat ="server" Title ="Análises efectuadas" />
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr valign="top">
        <td align = "left" class="txtCenter">
        <br />
        <asp:GridView ID= "GV_ClientReview" runat="server" AllowPaging = "True" AllowSorting ="True" AutoGenerateColumns = "False" Width = "1000px">
            <HeaderStyle CssClass="GridViewHeader" />
            <RowStyle CssClass="GridViewRow" />
            <AlternatingRowStyle CssClass="GridViewAlternatingRow" />
            <SelectedRowStyle CssClass="GridViewSelectedRow" />
            <EmptyDataRowStyle CssClass="GridViewEmptyDataRow" />                 
            <Columns>
            <asp:TemplateField HeaderText ="Balcão" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "p.Balcao")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Entidade" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "p.PartyReference")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Nome" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "p.PartyDescription")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Valor (Anterior)" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "PrevAnalisysV")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Imparidade ($) (Anterior)" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "PrevAnalisysImp")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Imparidade (%) (Anterior)" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "PrevAnalisysImpR")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Valor (Corrente)" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "AnalisysV")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Imparidade ($) (Corrente)" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "AnalisysImp")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Imparidade (%) (Corrente)" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#DataBinder.Eval(Container.DataItem, "AnalisysImpR")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <%--<asp:TemplateField HeaderText ="Variação Valor" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#(double)DataBinder.Eval(Container.DataItem, "PrevAnalisysV")- (double)DataBinder.Eval(Container.DataItem, "AnalisysV")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Variação Imparidade ($)" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%# (double)DataBinder.Eval(Container.DataItem, "AnalisysImp")- (double)DataBinder.Eval(Container.DataItem, "AnalisysImp")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Variação Imparidade (%)" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =<%#(double)DataBinder.Eval(Container.DataItem, "PrevAnalisysImpR")- (double)DataBinder.Eval(Container.DataItem, "PrevAnalisysImpR")%> ></asp:Label></ItemTemplate></asp:TemplateField>
            --%><asp:TemplateField HeaderText ="Situação" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text =""></asp:Label></ItemTemplate></asp:TemplateField>
            <asp:TemplateField HeaderText ="Decisão" HeaderStyle-VerticalAlign ="Middle"><ItemTemplate><asp:Label ID="Label1" runat = "server" Text ="" ></asp:Label></ItemTemplate></asp:TemplateField>
            
            <asp:TemplateField>
            <ItemTemplate>
            <asp:Button ID = "Btt_Validate" runat = "server" Text = "Validar" Width = "60px" />
            </ItemTemplate>
            </asp:TemplateField>
            
            
            <asp:TemplateField>
            <ItemTemplate>
            <asp:Button ID = "Btt_Return" runat = "server" Text = "Devolver" Width = "60px" />
            </ItemTemplate>
            </asp:TemplateField>
            
            </Columns>
        </asp:GridView>
        </td>
    </tr>
    </table>
</asp:Content>
