﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpiritucSI.BPN.AII.WebSite.Masters;
using SpiritucSI.BPN.AII.WebSite.Common;

namespace SpiritucSI.BPN.AII.WebSite.IndividualImparity
{
    public partial class BulkDashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.QueryString["status"] == null)
            {
                this.Response.Redirect("~/Dashboard.aspx");
            }
            this.ucAnalisysSummary.State = this.Request.QueryString["status"];
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), "redirectScript", "if (document.getElementById('" + this.ucAnalisysSummary.GetRedirect() + "').value == 'true') { " + Utils.RedirectScriptUpdatePanel("../Dashboard.aspx", 1500) + " }", true);
        }
    }
}
