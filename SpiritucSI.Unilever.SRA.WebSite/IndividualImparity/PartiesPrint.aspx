﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/FrontOffice.Master" AutoEventWireup="true"
    CodeBehind="PartiesPrint.aspx.cs" Inherits="SpiritucSI.BPN.AII.WebSite.IndividualImparity.PartiesPrint"
    Theme="MainTheme" meta:resourcekey="PageResource1" ValidateRequest="false" %>

<%@ Register Src="../UserControls/Common/ucPageTitle.ascx" TagName="ucTitle" TagPrefix="uc1" %>
<%@ Register Src="./UserControls/ClientDetailEntity.ascx" TagName="ucClient" TagPrefix="uc2" %>
<%@ Register Src="./UserControls/Questionnaires.ascx" TagName="ucQuestionnaire" TagPrefix="uc3" %>
<%@ Register Src="./UserControls/Values.ascx" TagName="ucValues" TagPrefix="uc4" %>
<%@ Register Src="./UserControls/ucComments.ascx" TagName="ucComments" TagPrefix="uc5" %>
<%@ Register Src="./UserControls/ucColaterals.ascx" TagName="ucColaterals" TagPrefix="uc6" %>
<%@ Register Src="./UserControls/ucAnalisys.ascx" TagName="ucAnalisys" TagPrefix="uc7" %>
    
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div class="padBottom">
        <uc1:ucTitle ID="UCTitle" runat="server" Title="Detalhes de Cliente" />
    </div>
    <uc2:ucClient ID="UCClient" runat="server" isSaveVisible="true" EnableViewState="true" />
    <uc3:ucQuestionnaire ID = "ucQuest" runat = "server" IsReadOnly ="true" />
    <uc4:ucValues ID = "ucValues" runat = "server" IsReadOnly ="true" />
    <uc5:ucComments ID = "ucCom" runat = "server" IsReadOnly ="true" />
    <uc6:ucColaterals ID = "ucCol" runat = "server" IsReadOnly ="true" />
    <uc7:ucAnalisys ID = "ucAnalysis" runat = "server" IsReadOnly ="true" />
</asp:Content>