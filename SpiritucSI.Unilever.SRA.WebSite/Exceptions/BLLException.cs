using System;
using System.Collections.Generic;
using System.Text;

namespace SpiritucSI.Unilever.SRA.Website.Exceptions
{
    public class BLLException : System.Exception
    {
        public BLLException():base()
        {
        }
        
        public BLLException(string _message)
            : base(_message)
        {
        }

        public BLLException(string _message, Exception _ex)
            : base(_message,_ex)
        {
        }
        
    }
}
