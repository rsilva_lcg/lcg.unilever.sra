using System;
using System.Collections.Generic;
using System.Text;

namespace SpiritucSI.Unilever.SRA.Website.Exceptions
{
    public class BLLLoggedException : System.Exception
    {
        public BLLLoggedException()
            : base()
        {
        }

        public BLLLoggedException(string _message)
            : base(_message)
        {
        }

        public BLLLoggedException(string _message, Exception _ex)
            : base(_message, _ex)
        {
        }
    }
}
