﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Workflow.Runtime;
using System.Configuration;
using SpiritucSI.Unilever.SRA.WF;
using SpiritucSI.Unilever.SRA.Website;
using SpiritucSI.Unilever.SRA.Website.Exceptions;
using System.Workflow.Activities;
using System.Workflow.Runtime.Hosting;
using System.Workflow.Runtime.Tracking;

namespace SpiritucSI.Unilever.SRA.WF.WebSite
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            this.StartWorkflow();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            if (ex is HttpUnhandledException) 
            {
                ex = ex.InnerException; 
            } 
 
            ExceptionPolicy.HandleException(ex, "GlobalPolicy");

            //Response.Redirect("ErrorPage.aspx");
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            this.StopWorkflow();
        }


        #region WORKFLOW EVENTS
        private void StartWorkflow()
        {
            //// Get the connection string from Web.config.
            String connectionString = ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString;

            // Create the workflow runtime.
            WorkflowRuntime workflowRuntime = new WorkflowRuntime("WorkflowRuntime");

            workflowRuntime.WorkflowAborted += new EventHandler<WorkflowEventArgs>(workflowRuntime_WorkflowAborted);
            workflowRuntime.WorkflowTerminated += new EventHandler<WorkflowTerminatedEventArgs>(workflowRuntime_WorkflowTerminated);
            workflowRuntime.WorkflowUnloaded += new EventHandler<WorkflowEventArgs>(workflowRuntime_WorkflowUnloaded);
            workflowRuntime.WorkflowPersisted += new EventHandler<WorkflowEventArgs>(workflowRuntime_WorkflowPersisted);

            SqlWorkflowPersistenceService swps = new SqlWorkflowPersistenceService(connectionString);
            workflowRuntime.AddService(swps);

            SqlTrackingService sts = new SqlTrackingService(connectionString);
            workflowRuntime.AddService(sts);

            ManualWorkflowSchedulerService mwss = new ManualWorkflowSchedulerService();
            workflowRuntime.AddService(mwss);

            SqlAuditingService taas = new SqlAuditingService(connectionString);
            workflowRuntime.AddService(taas);

            // Add the communication service.
            ExternalDataExchangeService dataService = new ExternalDataExchangeService();
            workflowRuntime.AddService(dataService);
            dataService.AddService(new WFEvents());

            // Start the workflow runtime.
            // (The workflow runtime starts automatically when workflows are started, but will not start automatically when tracking data is requested)
            workflowRuntime.StartRuntime();

            // Add the runtime to the application.
            Application["WorkflowRuntime"] = workflowRuntime;
        }

        private void StopWorkflow()
        {
            WorkflowRuntime wr = (WorkflowRuntime)Application["WorkflowRuntime"];
            wr.StopRuntime();
        }

        private void workflowRuntime_WorkflowPersisted(object sender, WorkflowEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void workflowRuntime_WorkflowUnloaded(object sender, WorkflowEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void workflowRuntime_WorkflowTerminated(object sender, WorkflowTerminatedEventArgs e)
        {

        }

        private void workflowRuntime_WorkflowAborted(object sender, WorkflowEventArgs e)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}