﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

[ValidationProperty("Value")]
public partial class TextCalendar : System.Web.UI.UserControl
{
    #region FIELDS
        protected bool _Selectable = false;
    #endregion

    #region METHODS
        protected void FillYear(int year)
        {
            this.C_rbl_Year.Items.Clear();
            for (int i = year - 2; i <= year + 3; ++i)
            {
                this.C_rbl_Year.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            this.C_rbl_Year.SelectedValue = year.ToString();
            this.C_rbl_Year.Attributes["year"] = year.ToString();
        }
        public void Clear()
        {
            this.C_ui_Calendar.VisibleDate = DateTime.Now;
            this.C_ui_Calendar.SelectedDate = DateTime.Now;
            this.C_txt_Date.Text = "";
        }
    #endregion

    #region PAGE EVENTS
        protected void Page_Load(object sender, EventArgs e)
        {   
            this._Selectable = false;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.C_txt_Date.Attributes["onclick"] = "javascript:document.getElementById('" + this.C_btn_calendar.ClientID + "').click();";
            this.C_ui_Calendar.DataBind();
            
            this.Calendar_box.Visible = _Selectable;
            this.C_btn_Mes.Text = this.C_ui_Calendar.VisibleDate.ToString("MMMM") + " de " + this.C_ui_Calendar.VisibleDate.ToString("yyyy");
        }
    #endregion

    #region CONTROL EVENTS
        protected void C_btn_calendar_Click(object sender, ImageClickEventArgs e)
        {
            if (!this.Calendar_box.Visible)
                this.C_ui_Calendar.VisibleDate = this.C_ui_Calendar.SelectedDate;
            _Selectable = !this.Calendar_box.Visible;
            this.Year_box.Visible = false;            
        }
        protected void C_ui_Calendar_Select(object sender, EventArgs e)
        {
            this.C_txt_Date.Text = this.C_ui_Calendar.SelectedDate.ToString("dd-MM-yyyy");
            this._Selectable = false;
        }
        protected void C_ui_Calendar_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            this._Selectable = true;
        }
        protected void C_btn_NextMonth_Click(object sender, ImageClickEventArgs e)
        {
            this.C_ui_Calendar.VisibleDate = this.C_ui_Calendar.VisibleDate.AddMonths(1);
            this._Selectable = true;
        }
        protected void C_btn_PrevMonth_Click(object sender, ImageClickEventArgs e)
        {
            this.C_ui_Calendar.VisibleDate = this.C_ui_Calendar.VisibleDate.AddMonths(-1);
            this._Selectable = true;
        }
        protected void C_btn_Mes_Click(object sender, EventArgs e)
        {
            this.C_rbl_Month.SelectedValue = this.C_ui_Calendar.VisibleDate.Month.ToString();
            this.FillYear(this.C_ui_Calendar.VisibleDate.Year);
            this.Year_box.Visible = true;
            this._Selectable = true;
        }
        protected void C_btn_Today_Click(object sender, EventArgs e)
        {
            this.C_ui_Calendar.VisibleDate = DateTime.Now;
            this._Selectable = true;
        }
        protected void C_btn_Clear_Click(object sender, EventArgs e)
        {
            this.Clear();
            this._Selectable = false;
        }
        protected void C_btn_Select_Click(object sender, EventArgs e)
        {
            this.Year_box.Visible = false;
            this._Selectable = true;
            this.C_ui_Calendar.VisibleDate = new DateTime(int.Parse(this.C_rbl_Year.SelectedValue), int.Parse(this.C_rbl_Month.SelectedValue), 1);
        }
        protected void C_btn_Close_Click(object sender, EventArgs e)
        {
            this.Year_box.Visible = false;
            this._Selectable = true;
        }
        protected void C_btn_YearUp_Click(object sender, EventArgs e)
        {
            this.FillYear(int.Parse(this.C_rbl_Year.Attributes["year"])+6);
            this._Selectable = true;
        }
        protected void C_btn_YearDown_Click(object sender, EventArgs e)
        {
            this.FillYear(int.Parse(this.C_rbl_Year.Attributes["year"]) - 6);
            this._Selectable = true;
        }
    #endregion

    #region PROPERTIES
        public DateTime SelectedDate
        {
            get { return (this.C_txt_Date.Text=="" ? DateTime.MinValue : this.C_ui_Calendar.SelectedDate); }
            set
            {
                if (value.Equals(DateTime.MinValue))
                {
                    this.Clear();
                }
                else
                {
                    this.C_ui_Calendar.VisibleDate = value;
                    this.C_ui_Calendar.SelectedDate = value;
                    this.C_txt_Date.Text = value.ToString("dd-MM-yyyy");
                }
            }
        }
        public bool Enabled
        {
            get { return this.C_txt_Date.Enabled; }
            set {
                this.C_txt_Date.Enabled = value;
                this.C_ui_Calendar.Enabled = value;
                this.C_btn_calendar.Visible = value;
               }
        }
        public string Value
        {
            get { return this.C_txt_Date.Text; } 
        }
        public string CssClass
        {
            get { return this.C_txt_Date.CssClass; }
            set { this.C_txt_Date.CssClass = value; }
        }
    #endregion
       
}
