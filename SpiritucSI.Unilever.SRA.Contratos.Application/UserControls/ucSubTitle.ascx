﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSubTitle.ascx.cs" 
Inherits="ucSubTitle" %>
<div class="SubTitle">
    <div class="CSSSubTitle">
        <div class="Img">
            <asp:Image ID="SubTitleIconImage" runat="server" ImageUrl="~/images/grey_icons/minimize.gif" /></div>
        <a href="javascript:void(0);">
            <div class="Txt">
                <h3>
                    <%=this.Title %>
                </h3>
            </div>
        </a>
    </div>
</div>
