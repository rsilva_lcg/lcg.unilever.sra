using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Resources;
using System.Web.UI;

public partial class Search : System.Web.UI.UserControl
{

    #region FIELDS
    /// <summary>
    /// Modo de funcionamento (TRUE=Avan�ado; FALSE=Normal)
    /// </summary>
    protected bool ADVMode = false;
    /// <summary>
    /// Titulo do controlo
    /// </summary>
    protected string title = "";
    /// <summary>
    /// Fonte de dados a pesquisar
    /// </summary>
    protected DataTable inputData;
    /// <summary>
    /// Provider para a fonte de dados
    /// </summary>
    protected selectable inputDataObject;
    /// <summary>
    /// Resultados da pesquisa
    /// </summary>
    protected DataTable outputData;
    /// <summary>
    /// Resultados publicos da pesquisa
    /// </summary>
    protected DataTable resultData;
    /// <summary>
    /// Disponibilidade do Modo Avan�ado (TRUE=Dispon�vel; FALSE=Indisponivel)
    /// </summary>
    protected bool advModeEnabled = false;
    /// <summary>
    /// Apresentar resultados (TRUE=SIM; FALSE=N�o)
    /// </summary>
    protected bool showSearchResult = false;
    /// <summary>
    /// Modo de apresenta��o (TRUE=Aberto; FALSE=Fechado)
    /// </summary>
    protected bool opened = false;
    /// <summary>
    /// Sentido de ordena��o dos resultados (TRUE=Ascendente; FALSE=descendente)
    /// </summary>
    protected bool sortASC = false;
    /// <summary>
    /// Apagar a linha seleccionada da lista (TRUE = sim; FALSE = n�o)
    /// </summary>
    protected bool deleteOnSelect = false;
    /// <summary>
    /// Lista de colunas dispon�veis para pesquisa
    /// </summary>
    protected string[] searchHeaders = new string[0];
    /// <summary>
    /// Lista de colunas a apresentar nos resultados
    /// </summary>
    protected string[] resultHeaders = new string[0];
    #endregion

    #region PROPERTIES
    /// <summary>
    /// Titulo do controlo
    /// </summary>
    public string Title
    {
        set { this.title = value; }
    }
    /// <summary>
    /// Fonte de dados a pesquisar
    /// </summary>
    public DataTable InputData
    {
        set { this.inputData = value; }
    }
    /// <summary>
    /// Provider para a fonte de dados
    /// </summary>
    public selectable InputDataObject
    {
        set
        {
            this.inputDataObject = value;
            this.inputData = inputDataObject.Select("1 = 0");
        }
    }
    /// <summary>
    /// Resultados da pesquisa
    /// </summary>
    public DataTable OutputData
    {
        get { return this.outputData; }
    }
    /// <summary>
    /// Disponibilidade do Modo Avan�ado (TRUE=Dispon�vel; FALSE=Indisponivel)
    /// </summary>
    public bool ADVModeEnabled
    {
        set { this.advModeEnabled = value; }
        get { return this.advModeEnabled; }
    }
    /// <summary>
    /// Apresentar resultados (TRUE=SIM; FALSE=N�o)
    /// </summary>
    public bool ShowSearchResult
    {
        set { this.showSearchResult = value; }
        get { return this.showSearchResult; }
    }
    /// <summary>
    /// Modo de apresenta��o (TRUE=Aberto; FALSE=Fechado)
    /// </summary>
    public bool Opened
    {
        set { this.opened = value; }
        get { return this.opened; }
    }
    /// <summary>
    /// Apagar a linha seleccionada da lista (TRUE = sim; FALSE = n�o)
    /// </summary>
    public bool DeleteOnSelect
    {
        set { this.deleteOnSelect = value; }
        get { return this.deleteOnSelect; }
    }
    /// <summary>
    /// Lista de colunas dispon�veis para pesquisa
    /// </summary>
    public string[] SearchHeaders
    {
        set
        {
            this.searchHeaders = new string[value.Length >= 5 ? 5 : value.Length];
            for (int i = 0; i < value.Length && i < this.searchHeaders.Length; ++i)
            {
                this.searchHeaders[i] = value[i];
            }
        }
        get { return this.searchHeaders; }
    }
    /// <summary>
    /// Lista de colunas a apresentar nos resultados
    /// </summary>
    public string[] ResultHeaders
    {
        set { this.resultHeaders = value; }
        get { return this.resultHeaders; }
    }
    #endregion

    #region EVENTS
    public delegate void SearchHandler(Object sender, DataTable data);
    public delegate void SelectHandler(Object sender, DataRow data);

    /// <summary>
    /// Ocorre ap�s ser obtido o resultado da pesquisa
    /// </summary>
    public event SearchHandler SearchResult;
    /// <summary>
    /// Ocorre quando � seleccionado um dos resultados da pesquisa
    /// </summary>
    public event SelectHandler SelectResult;
    #endregion

    #region METHODS
    /// <summary>
    /// Carregar o estado anterior do controlo
    /// </summary>
    private void LoadState()
    {
        this.lblErrorMessage.InnerHtml = "";
        this.lblErrorMessage.InnerHtml = "";
        if (IsPostBack)
        {
            this.ADVMode = (bool)this.ViewState["Mode"];
            this.title = (string)this.ViewState["Title"];
            this.sortASC = (bool)this.ViewState["Sort"];
            this.inputData = (DataTable)this.Session["InputData"];
            this.inputDataObject = (selectable)this.ViewState["InputDataObject"];
            this.outputData = (DataTable)this.Session["OutputData"];
            this.resultData = (DataTable)this.Session["resultData"];
            this.advModeEnabled = (bool)this.ViewState["AdvModeEnabled"];
            this.showSearchResult = (bool)this.ViewState["ShowSearchResult"];
            this.opened = (bool)this.ViewState["Opened"];
            this.searchHeaders = (string[])this.ViewState["SearchHeaders"];
            this.resultHeaders = (string[])this.ViewState["ResultHeaders"];


            this.Session["InputData"] = null;
            this.Session["OutputData"] = null;
            this.Session["resultData"] = null;

            //#region READ VARIABLES
            //if (this.Session[ClientID + "Mode"] != null)
            //{
            //    this.ADVMode = (bool)this.Session[ClientID + "Mode"];
            //}
            //if (this.Session[ClientID + "Title"] != null)
            //{
            //    this.title = this.Session[ClientID + "Title"].ToString();
            //}
            //if (this.Session[ClientID + "Sort"] != null)
            //{
            //    this.sortASC = (bool)this.Session[ClientID + "Sort"];
            //}
            //if (this.Session[ClientID + "InputData"] != null)
            //{
            //    this.inputData = (DataTable)this.Session["InputData"];
            //}
            //if (this.Session[ClientID + "InputDataObject"] != null)
            //{
            //    this.inputDataObject = (selectable)this.Session[ClientID + "InputDataObject"];
            //}
            //if (this.Session[ClientID + "OutputData"] != null)
            //{
            //    this.outputData = (DataTable)this.Session["OutputData"];
            //}
            //if (this.Session[ClientID + "resultData"] != null)
            //{
            //    this.resultData = (DataTable)this.Session["resultData"];
            //}
            //if (this.Session[ClientID + "AdvModeEnabled"] != null)
            //{
            //    this.advModeEnabled = (bool)this.Session[ClientID + "AdvModeEnabled"];
            //}
            //if (this.Session[ClientID + "ShowSearchResult"] != null)
            //{
            //    this.showSearchResult = (bool)this.Session[ClientID + "ShowSearchResult"];
            //}
            //if (this.Session[ClientID + "Opened"] != null)
            //{
            //    this.opened = (bool)this.Session[ClientID + "Opened"];
            //}
            //if (this.Session[ClientID + "SearchHeaders"] != null)
            //{
            //    this.searchHeaders = (string[])this.Session[ClientID + "SearchHeaders"];
            //}
            //if (this.Session[ClientID + "ResultHeaders"] != null)
            //{
            //    this.resultHeaders = (string[])this.Session[ClientID + "ResultHeaders"];
            //}
            //#endregion

            //#region CLEAR SESSION
            //this.Session[ClientID + "Mode"] = null;
            //this.Session[ClientID + "Title"] = null;
            //this.Session[ClientID + "Sort"] = null;
            //this.Session[ClientID + "InputData"] = null;
            //this.Session[ClientID + "InputDataObject"] = null;
            //this.Session[ClientID + "OutputData"] = null;
            //this.Session[ClientID + "ResultData"] = null;
            //this.Session[ClientID + "AdvModeEnabled"] = null;
            //this.Session[ClientID + "ShowSearchResult"] = null;
            //this.Session[ClientID + "Opened"] = null;
            //this.Session[ClientID + "SearchHeaders"] = null;
            //this.Session[ClientID + "ResultHeaders"] = null;
            //#endregion
        }
        else
        {
            this.Limpar();
        }
    }
    /// <summary>
    /// Guardar o estado actual do controlo
    /// </summary>
    private void SaveState()
    {
        //this.Session[ClientID + "Mode"] = this.ADVMode;
        //this.Session[ClientID + "Title"] = this.title;
        //this.Session[ClientID + "Sort"] = this.sortASC;
        //this.Session[ClientID + "InputData"] = this.inputData;
        //this.Session[ClientID + "InputDataObject"] = this.inputDataObject;
        //this.Session[ClientID + "OutputData"] = this.outputData;
        //this.Session[ClientID + "ResultData"] = this.resultData;
        //this.Session[ClientID + "AdvModeEnabled"] = this.advModeEnabled;
        //this.Session[ClientID + "ShowSearchResult"] = this.showSearchResult;
        //this.Session[ClientID + "Opened"] = this.opened;
        //this.Session[ClientID + "SearchHeaders"] = this.searchHeaders;
        //this.Session[ClientID + "ResultHeaders"] = this.resultHeaders;

        this.ViewState["Mode"] = this.ADVMode;
        this.ViewState["Title"] = this.title;
        this.ViewState["Sort"] = this.sortASC;
        this.Session["InputData"] = this.inputData;
        this.ViewState["InputDataObject"] = this.inputDataObject;
        this.Session["OutputData"] = this.outputData;
        this.Session["ResultData"] = this.resultData;
        this.ViewState["AdvModeEnabled"] = this.advModeEnabled;
        this.ViewState["ShowSearchResult"] = this.showSearchResult;
        this.ViewState["Opened"] = this.opened;
        this.ViewState["SearchHeaders"] = this.searchHeaders;
        this.ViewState["ResultHeaders"] = this.resultHeaders;
    }
    /// <summary>
    /// Limpar o estado do controlo
    /// </summary>
    public void Limpar()
    {
        if (this.outputData != null)
        {
            this.outputData.Clear();
        }
        if (this.resultData != null)
        {
            this.resultData.Clear();
        }

        this.txtCampo1.Text = "";
        this.txtCampo2.Text = "";
        this.txtCampo3.Text = "";
        this.txtCampo4.Text = "";
        this.txtCampo5.Text = "";
        this.lbCampos.SelectedIndex = -1;
        this.txtCampoValue.Text = "";
        this.ckBoxList.SelectedIndex = 0;
        this.txtSelectString.Text = "";
        this.C_btn_Add.Visible = true;
        this.C_btn_And.Visible = false;
        this.C_btn_Or.Visible = false;
        this.boxResultados.Visible = false;
        this.dgResultados.CurrentPageIndex = 0;
    }
    /// <summary>
    /// Actualizar controlos com os novos valores das propriedades
    /// </summary>
    private void ApplyProperties()
    {
        this.lblTitulo.InnerText = "Procurar " + this.title;
        this.tblPesquisa.Visible = this.opened;
        this.C_btn_Visibility.Text = this.opened ? "�" : "�";
        this.C_btn_ChangeMode.Visible = this.advModeEnabled;
        this.C_hr_Modo.Visible = this.advModeEnabled;
        this.C_btn_ChangeMode.Text = "&nbsp;VER&nbsp;PROCURA&nbsp;" + (ADVMode == true ? "NORMAL" : "AVAN�ADA");
        this.C_btn_ChangeMode.ImageUrl = "~/images/" + (ADVMode == true ? "Advanced.ico" : "Normal.ico");

        int l = 0;
        if (this.inputData != null)
        {
            switch (this.searchHeaders.Length)
            {
                case 5:
                    this.lblCampo5Title.Text = this.searchHeaders[4].ToUpper();
                    l = this.inputData.Columns[this.searchHeaders[4].ToUpper()].MaxLength;
                    if (l > 0)
                        this.txtCampo5.MaxLength = l;
                    goto case 4;
                case 4:
                    this.lblCampo4Title.Text = this.searchHeaders[3].ToUpper();
                    l = this.inputData.Columns[this.searchHeaders[3].ToUpper()].MaxLength;
                    if (l > 0)
                        this.txtCampo4.MaxLength = l;
                    goto case 3;
                case 3:
                    this.lblCampo3Title.Text = this.searchHeaders[2].ToUpper();
                    l = this.inputData.Columns[this.searchHeaders[2].ToUpper()].MaxLength;
                    if (l > 0)
                        this.txtCampo3.MaxLength = l;
                    goto case 2;
                case 2:
                    this.lblCampo2Title.Text = this.searchHeaders[1].ToUpper();
                    l = this.inputData.Columns[this.searchHeaders[1].ToUpper()].MaxLength;
                    if (l > 0)
                        this.txtCampo2.MaxLength = l;
                    goto case 1;
                case 1:
                    this.lblCampo1Title.Text = this.searchHeaders[0].ToUpper();
                    l = this.inputData.Columns[this.searchHeaders[0].ToUpper()].MaxLength;
                    if (l > 0)
                        this.txtCampo1.MaxLength = l;
                    break;
                default:
                    break;
            }
        }
        this.boxCampo1.Visible = !lblCampo1Title.Text.Equals("");
        this.boxCampo2.Visible = !lblCampo2Title.Text.Equals("");
        this.boxCampo3.Visible = !lblCampo3Title.Text.Equals("");
        this.boxCampo4.Visible = !lblCampo4Title.Text.Equals("");
        this.boxCampo5.Visible = !lblCampo5Title.Text.Equals("");
    }
    public override void Focus()
    {
        base.Focus();
        string FocusID = "";
        if (this.ADVMode)
        {
            FocusID = this.txtCampoValue.ID;
        }
        else
        {
            FocusID = this.txtCampo1.ID;
        }
        this.Page.Form.DefaultFocus = FocusID;
    }
    #endregion

    #region PAGE EVENTS
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Visible)
        {
            this.LoadState();
            this.ApplyProperties();
            if (this.showSearchResult && this.resultData != null)
            {
                this.dgResultados.DataSource = this.resultData.DefaultView;
                this.dgResultados.DataBind();
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.SaveState();
        this.ApplyProperties();
        if (this.Page.Form.DefaultFocus.Length == 0)
            this.Focus();
        if (this.showSearchResult && this.resultData != null)
        {
            this.dgResultados.DataSource = this.resultData.DefaultView;
            this.dgResultados.DataBind();
        }
    }
    #endregion

    #region CONTROL EVENTS
    protected void C_btn_ChangeMode_OnClick(object sender, System.EventArgs e)
    {
        this.ADVMode = !this.ADVMode;
        this.SearchTable.Visible = !ADVMode;
        this.ADVSearchTable.Visible = ADVMode;
        if (this.ADVMode)
        {
            if (this.inputData != null)
            {
                this.lbCampos.Items.Clear();
                foreach (DataColumn column in this.inputData.Columns)
                {
                    this.lbCampos.Items.Add(column.ColumnName);
                }
            }

        }
    }
    protected void C_btn_Add_OnClick(object sender, System.EventArgs e)
    {
        this.txtSelectString.Text = this.lbCampos.SelectedValue + " " +
            this.ckBoxList.SelectedValue + " '%" +
            this.txtCampoValue.Text.ToUpper() + "%'";
        if (!(this.ckBoxList.SelectedValue.Equals("LIKE") | this.ckBoxList.SelectedValue.Equals("NOT LIKE")))
        {
            this.txtSelectString.Text = this.txtSelectString.Text.Replace("%", "");
        }
        this.lbCampos.SelectedIndex = -1;
        this.txtCampoValue.Text = "";
        this.C_btn_Add.Visible = false;
        this.C_btn_And.Visible = true;
        this.C_btn_Or.Visible = true;
    }
    protected void C_btn_And_OnClick(object sender, System.EventArgs e)
    {

        string str = " AND\n" +
            this.lbCampos.SelectedValue + " " +
            this.ckBoxList.SelectedValue + " '%" +
            this.txtCampoValue.Text.ToUpper() + "%'";
        if (!(this.ckBoxList.SelectedValue.Equals("LIKE") | this.ckBoxList.SelectedValue.Equals("NOT LIKE")))
        {
            str = str.Replace("%", "");
        }
        this.txtSelectString.Text += str;
        this.lbCampos.SelectedIndex = -1;
        this.txtCampoValue.Text = "";

    }
    protected void C_btn_Or_OnClick(object sender, System.EventArgs e)
    {
        string str = " OR\n" +
            this.lbCampos.SelectedValue + " " +
            this.ckBoxList.SelectedValue + " '%" +
            this.txtCampoValue.Text.ToUpper() + "%'";
        if (!(this.ckBoxList.SelectedValue.Equals("LIKE") | this.ckBoxList.SelectedValue.Equals("NOT LIKE")))
        {
            str = str.Replace("%", "");
        }
        this.txtSelectString.Text += str;
        this.lbCampos.SelectedIndex = -1;
        this.txtCampoValue.Text = "";
    }
    protected void C_btn_Visibility_OnClick(object sender, System.EventArgs e)
    {
        this.opened = !this.opened;
        this.tblPesquisa.Visible = this.opened;
        this.C_btn_Visibility.Text = this.opened ? "�" : "�";
    }
    protected void C_btn_Procurar_OnClick(object sender, System.EventArgs e)
    {
        if (this.inputData == null)
        {
            this.lblErrorMessage.InnerHtml += "N�O EXISTEM DADOS PARA PESQUISAR.";
            return;
        }
        string SelectString = "";
        if (this.advModeEnabled && this.ADVMode)
        {
            SelectString = this.txtSelectString.Text;
        }
        else
        {
            switch (this.searchHeaders.Length)
            {
                case 5:
                    if (this.txtCampo5.Text != "")
                    {
                        if (SelectString != "") { SelectString += " AND "; }
                        SelectString += this.searchHeaders[4] + " = '" + this.txtCampo5.Text + "'";
                    }
                    goto case 4;
                case 4:
                    if (this.txtCampo4.Text != "")
                    {
                        if (SelectString != "") { SelectString += " AND "; }
                        SelectString += this.searchHeaders[3] + " = '" + this.txtCampo4.Text + "'";
                    }
                    goto case 3;
                case 3:
                    if (this.txtCampo3.Text != "")
                    {
                        if (SelectString != "") { SelectString += " AND "; }
                        SelectString += this.searchHeaders[2] + " = '" + this.txtCampo3.Text + "'";
                    }
                    goto case 2;
                case 2:
                    if (this.txtCampo2.Text != "")
                    {
                        if (SelectString != "") { SelectString += " AND "; }
                        SelectString += this.searchHeaders[1] + " = '" + this.txtCampo2.Text + "'";
                    }
                    goto case 1;
                case 1:
                    if (this.txtCampo1.Text != "")
                    {
                        if (SelectString != "") { SelectString += " AND "; }
                        SelectString += this.searchHeaders[0] + " = '" + this.txtCampo1.Text + "'";
                    }
                    break;
                default:
                    break;
            }
        }
        string stringtest = SelectString.ToUpper();
        if ((stringtest.IndexOf(" DELETE ") +
            stringtest.IndexOf(" DROP ") +
            stringtest.IndexOf(" UPDATE ") +
            stringtest.IndexOf(" INSERT ") +
            stringtest.IndexOf(" CREATE ")) > 0)
        {
            this.lblErrorMessage.InnerHtml += "A STRING DE SELEC��O INTRODUZIDA N�O � PERMITIDA.";
            return;
        }
        try
        {
            this.outputData = this.inputData.Clone();
            if (this.inputData.Rows.Count == 0 && this.inputDataObject != null)
            {
                this.outputData = this.inputDataObject.Select(SelectString);
            }
            else
            {
                DataRow[] result = this.inputData.Select(SelectString);
                foreach (DataRow row in result)
                {
                    this.outputData.Rows.Add(row.ItemArray);
                }
            }
            this.resultData = this.outputData.Copy();
        }
        catch (SyntaxErrorException)
        {
            this.lblErrorMessage.InnerHtml += "A STRING DE SELEC��O CONT�M ERROS DE SINTAXE.";
            return;
        }
        catch (myDBException)
        {
            this.lblErrorMessage.InnerHtml += "N�O FOI POSS�VEL OBTER OS DADOS DA BASE DE DADOS.<br /> A STRING DE SELEC��O PODE CONTER ERROS DE SINTAXE...";
            return;
        }
        catch (Exception)
        {
            this.lblErrorMessage.InnerHtml += "OCORREU UM ERRO INESPERADO.";
            return;
        }
        if (this.showSearchResult)
        {
            if (this.resultHeaders.Length > 0 && !this.resultHeaders[0].Equals(""))
            {

                ArrayList idxs = new ArrayList();
                foreach (string column in this.resultHeaders)
                {
                    idxs.Add(this.resultData.Columns.IndexOf(column));
                }
                for (int idx = this.resultData.Columns.Count - 1; idx >= 0; --idx)
                {
                    if (!idxs.Contains(idx))
                    {
                        this.resultData.Columns.RemoveAt(idx);
                    }
                }
            }
            this.lblResultados.InnerText = this.resultData.Rows.Count.ToString();
            this.resultData.DefaultView.Sort = this.resultData.Columns[0].ColumnName + " ASC";
            this.outputData.DefaultView.Sort = this.resultData.Columns[0].ColumnName + " ASC";
            this.dgResultados.CurrentPageIndex = 0;
            this.boxResultados.Visible = true;
        }
        else
        {
            if (this.SearchResult != null)
            {
                this.SearchResult(this, this.outputData.Copy());
            }
            this.resultData = null;
            this.outputData = null;
        }
    }
    protected void C_btn_Limpar_OnClick(object sender, System.EventArgs e)
    {
        this.Limpar();
    }
    protected void dgResultados_DataBinding(object source, EventArgs e)
    {
        if (dgResultados.Columns.Count > 1)
            return;
        TemplateColumn bc = new TemplateColumn();
        bc.HeaderStyle.ForeColor = Color.Transparent;
        bc.HeaderStyle.BackColor = Color.Transparent;
        bc.ItemStyle.VerticalAlign = VerticalAlign.Middle;
        bc.ItemStyle.BackColor = Color.Transparent;
        bc.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
        bc.ItemStyle.Width = new Unit("1%");
        bc.ItemTemplate = new DataGridTemplate(ListItemType.Item);
        dgResultados.Columns.Add(bc);

        DataView dv = (DataView)dgResultados.DataSource;
        foreach (DataColumn c in dv.Table.Columns)
        {
            BoundColumn dc = new BoundColumn();
            dc.HeaderText = c.ColumnName.ToUpper();
            dc.SortExpression = c.ColumnName;
            dc.DataField = c.ColumnName;

            dgResultados.Columns.Add(dc);
        }
    }
    protected void dgResultados_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Select"))
        {
            DataGrid dg = (DataGrid)source;
            int idx = ((dg.CurrentPageIndex) * dg.PageSize) + e.Item.ItemIndex;
            if (this.SelectResult != null)
            {
                this.SelectResult(this, this.outputData.DefaultView[idx].Row);
            }
            if (deleteOnSelect)
            {
                this.resultData.DefaultView[idx].Row.Delete();
                this.outputData.DefaultView[idx].Row.Delete();
            }
            if (this.dgResultados.CurrentPageIndex > 0 && ((float)this.resultData.DefaultView.Count / (float)this.dgResultados.PageSize) <= ((float)this.dgResultados.CurrentPageIndex))
            {
                --this.dgResultados.CurrentPageIndex;
            }
        }
    }
    protected void dgResultados_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DataGrid dg = (DataGrid)source;
        dg.CurrentPageIndex = e.NewPageIndex;
    }
    protected void dgResultados_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        this.resultData.DefaultView.Sort = e.SortExpression + (this.sortASC == true ? " ASC" : " DESC");
        this.outputData.DefaultView.Sort = e.SortExpression + (this.sortASC == true ? " ASC" : " DESC");
        this.sortASC = !this.sortASC;
    }
    #endregion
}

public class DataGridTemplate : ITemplate
{
    ListItemType templateType;

    public DataGridTemplate(ListItemType type)
    {
        templateType = type;
    }

    public void InstantiateIn(System.Web.UI.Control container)
    {
        ImageButton C_btn_dgResultados_Select = new ImageButton();
        C_btn_dgResultados_Select.ID = "C_btn_dgResultados_Select";
        C_btn_dgResultados_Select.CommandName = "Select";
        C_btn_dgResultados_Select.CausesValidation = false;
        C_btn_dgResultados_Select.ToolTip = "Seleccionar a linha";
        C_btn_dgResultados_Select.ImageUrl = "~/images/TipBig.ico";
        container.Controls.Add(C_btn_dgResultados_Select);
    }
}
