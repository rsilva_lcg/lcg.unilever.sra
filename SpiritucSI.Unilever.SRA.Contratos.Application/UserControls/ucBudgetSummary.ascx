﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucBudgetSummary.ascx.cs"
    Inherits="UserControls_ucBudgetSummary" %>
<table>
    <tr align="left">
        <td colspan ="7">
            <asp:Label ID="lbl_Title" runat="server" Text="BUDGET STATUS"></asp:Label>
        </td>
    </tr>
    <tr align="left">
        <td>
            <asp:Label ID="lbl_Total" runat="server" Text="Total:"></asp:Label>
            <asp:TextBox ID="txb_Total" runat="server" Enabled="false"></asp:TextBox>
        </td>
        <td style="width:5%"></td>
        <td>
            <asp:Label ID="lbl_Available" runat="server" Text="Disponível:"></asp:Label>
            <asp:TextBox ID="txb_Available" runat="server" Enabled="false"></asp:TextBox>
        </td>
        <td style="width:5%"></td>
        <td>
            <asp:Label ID="lbl_Captured" runat="server" Text="Cativo:"></asp:Label>
            <asp:TextBox ID="txb_Captured" runat="server" Enabled="false"></asp:TextBox>
        </td>
        <td style="width:5%"></td>
        <td>
            <asp:Label ID="lbl_Authorized" runat="server" Text="Autorizado:"></asp:Label>
            <asp:TextBox ID="txb_Authorized" runat="server" Enabled="false"></asp:TextBox>
        </td>
    </tr>
</table>
