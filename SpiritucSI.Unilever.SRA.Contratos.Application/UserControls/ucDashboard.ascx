﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDashboard.ascx.cs" Inherits="UserControls_ucDashboard" %>
<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<div id="Drafted" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
        <asp:Label ID="Label1" Style="font-size: 14px; font-weight: bold;" runat="server" Text='<%$Resources:Title1 %>'></asp:Label></div>
    <div id="Div3" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridDrafted" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                OnSelect="grid_Select" AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column13" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link2"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column15" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label2"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column1" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label2"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column28" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label2" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column46" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label2"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column47" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label2" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column48" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label2"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column49" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo' runat="server"
                        TemplateId="t_label2" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column50" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label2" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column51" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label2" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column3" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label2"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label2">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%#Container.Value %>' Style=""></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link2">
                        <Template>
                            <asp:LinkButton ID="lnk1" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString() %>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="btnBulkValidation" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=DRAFT"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="RenewalDrafted" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
        <asp:Label ID="Label2" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title2 %>' /></div>
    <div id="Div4" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridRenewalDrafted" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column2" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link3"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column4" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label3"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column5" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label3"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column6" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label3" Align="left">
                    </obout:Column>
                   <obout:Column ID="Column53" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label3"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column54" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label3" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column55" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label3"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column56" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo' runat="server"
                        TemplateId="t_label3" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column57" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label3" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column58" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label3" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column59" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label3"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label3">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link3">
                        <Template>
                            <asp:LinkButton ID="lnk1" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button1" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=RENEWALDRAFT"  Width="100%"/> 
    </div>
        <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="Printed" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
        <asp:Label ID="Label3" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title3 %>' /></div>
    <div id="Div5" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridPrinted" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column7" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link4"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column8" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label4"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column9" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label4"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column61" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label4" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column63" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label4"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column64" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label4" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column65" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label4"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column66" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo' runat="server"
                        TemplateId="t_label4" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column67" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label4" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column68" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label4" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column69" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label4"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label4">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link4">
                        <Template>
                            <asp:LinkButton ID="lnk1" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button2" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=PRINTED"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="Delegated" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
        <asp:Label ID="Label4" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title4 %>' />
    </div>
    <div id="Div6" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridDelegated" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column10" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link5"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column11" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label5"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column12" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label5"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column71" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label5" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column73" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label5"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column74" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label5" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column75" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label5"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column76" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo' runat="server"
                        TemplateId="t_label5" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column77" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label5" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column78" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label5" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column79" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label5"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label5">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link5">
                        <Template>
                            <asp:LinkButton ID="lnk1" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button3" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=DELEGATED"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="Received" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
            <asp:Label ID="Label5" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title5 %>' />
    </div>
    <div id="Div8" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridReceived" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column16" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link7"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column17" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label7"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column18" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label7"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column81" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label7" Align="left">
                    </obout:Column>

                    <obout:Column ID="Column83" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label7"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column84" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label7" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column85" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label7"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column86" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo' runat="server"
                        TemplateId="t_label7" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column87" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label7" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column88" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label7" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column89" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label7"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label7">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link7">
                        <Template>
                            <asp:LinkButton ID="lnk1" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button4" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=RECEIVED"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="ErrorVerfication" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
            <asp:Label ID="Label6" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title6 %>' />
    </div>
    <div id="Div9" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridErrorVerification" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column19" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link8"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column20" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label8"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column21" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label8"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column91" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label8" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column93" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label8"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column94" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label8" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column95" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label8"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column96" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo' runat="server"
                        TemplateId="t_label8" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column97" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label8" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column98" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label8" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column99" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label8"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label8">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link8">
                        <Template>
                            <asp:LinkButton ID="lnk1" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button5" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=ERRORVERIFICATION"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="InApproval" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
            <asp:Label ID="Label7" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title7 %>' />
    </div>
    <div id="Div10" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridInApproval" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column22" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link9"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column23" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label9"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column24" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label9"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column101" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label9" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column103" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label9"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column104" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label9" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column105" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label9"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column106" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo'
                        runat="server" TemplateId="t_label9" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column107" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label9" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column108" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label9" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column109" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label9"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label9">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link9">
                        <Template>
                            <asp:LinkButton ID="lnk1" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button6" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=INAPPROVAL"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="ManagerApproval" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
            <asp:Label ID="Label8" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title8 %>' />
    </div>
    <div id="Div11" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridManagerApproval" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column25" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link10"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column26" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label10"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column29" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label10"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column111" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label10" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column113" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label10"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column114" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label10" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column115" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label10"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column116" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo'
                        runat="server" TemplateId="t_label10" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column117" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label10" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column118" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label10" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column119" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label10"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label10">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link10">
                        <Template>
                            <asp:LinkButton ID="lnk1" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button7" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=MANAGERAPPROVAL"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="SalesApproval" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
            <asp:Label ID="Label9" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title9 %>' />
    </div>
    <div id="Div12" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridSalesApproval" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column30" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link11"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column31" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label11"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column32" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label11"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column121" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label11" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column123" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label11"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column124" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label11" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column125" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label11"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column126" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo'
                        runat="server" TemplateId="t_label11" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column127" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label11" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column128" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label11" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column129" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label11"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label11">
                        <Template>
                            <asp:Label ID="lbl11" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link11">
                        <Template>
                            <asp:LinkButton ID="lnk11" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button8" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=SALESAPPROVAL"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="FinancialApproval" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
            <asp:Label ID="Label10" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title10 %>' />
    </div>
    <div id="Div13" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridFinancialApproval" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column33" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link12"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column34" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label12"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column35" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label12"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column131" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label12" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column133" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label12"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column134" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label12" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column135" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label12"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column136" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo'
                        runat="server" TemplateId="t_label12" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column137" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label12" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column138" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label12" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column139" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label12"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label12">
                        <Template>
                            <asp:Label ID="lbl12" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link12">
                        <Template>
                            <asp:LinkButton ID="lnk12" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button9" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=FINANTIALAPPROVAL"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="TopApproval" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
            <asp:Label ID="Label11" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title11 %>' />
    </div>
    <div id="Div14" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridTopApproval" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column36" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link13"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column37" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label13"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column39" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label13"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column141" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label13" Align="left">
                    </obout:Column>
                     <obout:Column ID="Column143" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label13"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column144" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label13" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column145" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label13"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column146" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo'
                        runat="server" TemplateId="t_label13" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column147" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label13" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column148" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label13" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column149" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label13"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label13">
                        <Template>
                            <asp:Label ID="lbl13" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link13">
                        <Template>
                            <asp:LinkButton ID="lnk13" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float:left; padding-left:10px; padding-top:5px; width:200px;" >
    <asp:Button ID ="Button10" runat ="server" Text="Gerir Registos em Simultâneo" PostBackUrl="~/SRA_BulkManager.aspx?state=TOPAPPROVAL"  Width="100%"/> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="Approved" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
            <asp:Label ID="Label12" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title12 %>' />
    </div>
    <div id="Div15" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridApproved" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column40" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link14"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column41" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label14"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column42" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label14"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column151" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label14" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column153" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label14"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column154" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label14" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column155" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label14"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column156" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo'
                        runat="server" TemplateId="t_label14" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column157" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label14" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column158" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label14" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column159" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label14"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label14">
                        <Template>
                            <asp:Label ID="lbl14" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link14">
                        <Template>
                            <asp:LinkButton ID="lnk14" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
<div id="Rejected" runat="server" >
    <div style="text-align: left; padding-bottom: 2px; padding-left: 10px;">
            <asp:Label ID="Label13" runat="server" Style="font-size: 14px; font-weight: bold;" Text='<%$Resources:Title13 %>' />
    </div>
    <div id="Div16" runat="server" class="SubContentCenterTable">
        <div style="float: left; width: 1500px; padding-left: 10px; padding-right: 10px;">
            <obout:Grid ID="gridRejected" Width="100%" Language="pt" AutoPostBackOnSelect="true" Serialize="true"
                AllowPaging="true" PageSize="15" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="true">
                <Columns>
                    <obout:Column ID="Column43" Width="120" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link15"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column44" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label15"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column45" Width="200" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label15"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column161" Width="80" DataField="LOCALIDADE" HeaderText='Localidade' runat="server"
                        TemplateId="t_label15" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column163" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label15"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column164" Width="90" DataField="DATA_INICIO" HeaderText='Data Inicio' runat="server"
                        TemplateId="t_label15" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column165" Width="90" DataField="DATA_FIM" HeaderText='Data Fim' runat="server" TemplateId="t_label15"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column166" Width="60" DataField="DATA_FIM_EFECTIVO" HeaderText='Data Fim Efectivo'
                        runat="server" TemplateId="t_label15" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column167" Width="80" DataField="QUANTIA_FIM" HeaderText='Quantia Fim' runat="server"
                        TemplateId="t_label15" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column168" Width="90" DataField="ANOS_REVOGACAO" HeaderText='Anos Revogação' runat="server"
                        TemplateId="t_label15" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column169" Width="100%" DataField="ID_CONTRATO" HeaderText=' ' runat="server" TemplateId="t_label15"
                        Visible="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label15">
                        <Template>
                            <asp:Label ID="lbl15" runat="server" Text='<%#Container.Value %>'></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link15">
                        <Template>
                            <asp:LinkButton ID="lnk15" runat="server" Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>
        </div>
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>
