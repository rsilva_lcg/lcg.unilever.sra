<%@ Control Language="c#" AutoEventWireup="true" CodeFile="Search.ascx.cs" Inherits="Search" EnableViewState="true" %>
<%@ Register TagPrefix="UC1" TagName="ImageTextButton" Src="~/UserControls/ImageTextButton.ascx" %>
<asp:Panel ID="C_pnl_Box" runat="server" meta:resourcekey="C_pnl_Box" DefaultButton="C_btn_Procurar">
    <%-- TITULO ########################################################################################## --%>
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f0f0f0">
        <tr>
            <td class="titulo_peq tituloBarra" width="5%">
                <asp:LinkButton ID="C_btn_Visibility" runat="server" meta:resourcekey="C_btn_Visibility" Style="font-size: 12pt;
                    color: white; font-family: Symbol" CausesValidation="False" OnClick="C_btn_Visibility_OnClick">
                </asp:LinkButton>
            </td>
            <td class="titulo_peq tituloBarra" align="center" colspan="2">
                <label  id="lblTitulo" runat="server">
                </label>
            </td>
        </tr>
    </table>
    <%-- PESQUISA ######################################################################################## --%>
    <table id="tblPesquisa" width="100%" bgcolor="#f0f0f0" runat="server">
        <%-- MODO --%>
        <tr runat="server">
            <td runat="server" valign="middle">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <%-- BOT�O --%>
                        <td valign="middle" width="45px">
                            <UC1:ImageTextButton ID="C_btn_ChangeMode" runat="server" TextCssClass="button" CausesValidation="False"
                                TextPosition="Rigth" meta:resourcekey="C_btn_ChangeMode" OnClick="C_btn_ChangeMode_OnClick"/>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <%-- MENSAGENS ERRO--%>
                        <td>
                            <label class="errorLabel" id="lblErrorMessage" runat="server">
                            </label>
                        </td>
                    </tr>
                </table>
                <hr id="C_hr_Modo" noshade="noshade" runat="server" />
            </td>
        </tr>
        <%-- CONDI��ES --%>
        <tr runat="server">
            <td runat="server">
                <%-- MODO NORMAL --%>
                <table id="SearchTable" width="100%" runat="server">
                    <tr id="boxCampo1" runat="server">
                        <td align="right" runat="server">
                            <asp:Label CssClass="label" ID="lblCampo1Title" runat="server"></asp:Label>
                        </td>
                        <td width="85%" runat="server">
                            <asp:TextBox ID="txtCampo1" runat="server" CssClass="textbox" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="boxCampo2" runat="server">
                        <td align="right" runat="server">
                            <asp:Label CssClass="label" ID="lblCampo2Title" runat="server"></asp:Label>
                        </td>
                        <td runat="server">
                            <asp:TextBox ID="txtCampo2" runat="server" CssClass="textbox" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="boxCampo3" runat="server">
                        <td align="right" runat="server">
                            <asp:Label CssClass="label" ID="lblCampo3Title" runat="server"></asp:Label>
                        </td>
                        <td runat="server">
                            <asp:TextBox ID="txtCampo3" runat="server" CssClass="textbox" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="boxCampo4" runat="server">
                        <td align="right" runat="server">
                            <asp:Label CssClass="label" ID="lblCampo4Title" runat="server"></asp:Label>
                        </td>
                        <td runat="server">
                            <asp:TextBox ID="txtCampo4" runat="server" CssClass="textbox" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="boxCampo5" runat="server">
                        <td align="right" runat="server">
                            <asp:Label CssClass="label" ID="lblCampo5Title" runat="server"></asp:Label>
                        </td>
                        <td runat="server">
                            <asp:TextBox ID="txtCampo5" runat="server" CssClass="textbox" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <%-- MODO AVAN�ADO --%>
                <table id="ADVSearchTable" width="100%" runat="server" visible="False">
                    <tr runat="server">
                        <td align="left" width="25%" runat="server">
                            <label class="label">
                                Campo</label>
                            <br />
                            <asp:ListBox ID="lbCampos" runat="server" Width="100%" Rows="6"></asp:ListBox>
                            <asp:RequiredFieldValidator ID="lbCampos_Val" runat="server" ValidationGroup="Advanced" Display="Dynamic"
                                ControlToValidate="lbCampos" EnableClientScript="true" ErrorMessage="Seleccione um campo">Seleccione um campo</asp:RequiredFieldValidator>
                            <br />
                            <label class="label">
                                Valor</label><br />
                            <asp:TextBox ID="txtCampoValue" runat="server" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="txtCampoValue_val" runat="server" ValidationGroup="Advanced" Display="Dynamic"
                                ControlToValidate="txtCampoValue" EnableClientScript="true" ErrorMessage="Indique um valor">Indique um valor</asp:RequiredFieldValidator>
                        </td>
                        <td width="12%" runat="server">
                            <asp:RadioButtonList ID="ckBoxList" runat="server" Width="100%">
                                <asp:ListItem Selected="True" Value="=">Igual</asp:ListItem>
                                <asp:ListItem Value="<>">Diferente</asp:ListItem>
                                <asp:ListItem Value="LIKE">Cont�m</asp:ListItem>
                                <asp:ListItem Value="NOT LIKE">N�o Cont�m</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td width="5%" runat="server" align="right">
                            <UC1:ImageTextButton ID="C_btn_Add" runat="server" ValidationGroup="Advanced"  meta:resourcekey="C_btn_Add" CausesValidation="true"  OnClick="C_btn_Add_OnClick"/>
                            <UC1:ImageTextButton ID="C_btn_Or" runat="server" ValidationGroup="Advanced" TextCssClass="button" CausesValidation="true" TextPosition="Left"  meta:resourcekey="C_btn_Or"  OnClick="C_btn_Or_OnClick" Visible="false"/>
                            <br />
                            <UC1:ImageTextButton ID="C_btn_And" runat="server" ValidationGroup="Advanced" TextCssClass="button" CausesValidation="true" TextPosition="Left"  meta:resourcekey="C_btn_And"  OnClick="C_btn_And_OnClick" Visible="false"/>
                        </td>
                        <td width="1%">&nbsp;&nbsp;</td>
                        <td runat="server">
                            <asp:TextBox ID="txtSelectString" runat="server" Width="100%" Rows="8" TextMode="MultiLine"></asp:TextBox>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%-- BOT�ES --%>
        <tr runat="server">
            <td runat="server">
                <hr noshade="noshade" />
                <table>
                    <tr>
                        <td>
                            <UC1:ImageTextButton ID="C_btn_Procurar" runat="server" TextCssClass="button" TextPosition="Rigth" CausesValidation="False"
                                meta:resourcekey="C_btn_Procurar" OnClick="C_btn_Procurar_OnClick" />
                        </td>
                        <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            <UC1:ImageTextButton ID="C_btn_Limpar" runat="server" TextCssClass="button" TextPosition="Rigth" CausesValidation="False"
                                meta:resourcekey="C_btn_Limpar" OnClick="C_btn_Limpar_OnClick" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%-- RESULTADOS --%>
        <tr id="boxResultados" runat="server">
            <td runat="server">
                <table id="Table2" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f0f0f0">
                    <%-- HEADER --%>
                    <tr>
                        <td class="titulo_peq tituloBarra">
                            <label class="titulo" id="lblResultados" runat="server">
                            </label>
                            &nbsp;
                            <label class="titulo" id="Label1" runat="server">
                                Resultados</label>
                        </td>
                    </tr>
                    <%-- LISTA --%>
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dgResultados" runat="server" ShowHeader="true" UseAccessibleHeader="true" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="false" BorderStyle="None" GridLines="None" CellSpacing="2"
                                OnItemCommand="dgResultados_ItemCommand" OnPageIndexChanged="dgResultados_PageIndexChanged" OnDataBinding="dgResultados_DataBinding"
                                OnSortCommand="dgResultados_SortCommand" PageSize="15" Width="100%">
                                <HeaderStyle ForeColor="#00008B" BackColor="#99CCFF" Height="30px" Wrap="true" />
                                <ItemStyle BackColor="#E0E0E0" ForeColor="Black" />
                                <AlternatingItemStyle BackColor="#C0C0C0" ForeColor="Black" />
                                <PagerStyle ForeColor="#00008B" BackColor="Transparent" PageButtonCount="50" HorizontalAlign="Left" Mode="NumericPages" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
