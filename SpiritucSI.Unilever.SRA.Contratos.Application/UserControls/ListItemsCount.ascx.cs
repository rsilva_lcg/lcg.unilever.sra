﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SpiritucSI.UserControls.GenericControls
{
    public partial class ListItemsCount : UserControl
    {
        #region FIELDS
        protected int _TotalCount = 0;
        protected int _PageSize = 0;
        protected int _PageIndex = 0;
        #endregion

        #region PAGE EVENTS
        protected void SaveFields()
        {
            this.ViewState[this.ClientID + "_TotalCount"] = _TotalCount;
            this.ViewState[this.ClientID + "_PageSize"] = _PageSize;
            this.ViewState[this.ClientID + "_PageIndex"] = _PageIndex;
        }
        protected void LoadFields()
        {
            if (this.ViewState[this.ClientID + "_TotalCount"] != null)
            {
                _TotalCount = (int)this.ViewState[this.ClientID + "_TotalCount"];
            }
            if (this.ViewState[this.ClientID + "_PageSize"] != null)
            {
                _PageSize = (int)this.ViewState[this.ClientID + "_PageSize"];
            }
            if (this.ViewState[this.ClientID + "_PageIndex"] != null)
            {
                _PageIndex = (int)this.ViewState[this.ClientID + "_PageIndex"];
            }
        }
        protected void Page_PreRender( object sender, EventArgs e )
        {
            string message = this.GetLocalResourceObject("Message.Template").ToString();
            int min = Math.Min((_PageSize * _PageIndex) + 1, _TotalCount);
            int max = Math.Min((_PageSize * _PageIndex) + _PageSize, _TotalCount);
            this.C_lbl_Info.Text = message.Replace("[MIN]", min.ToString()).Replace("[MAX]", max.ToString()).Replace("[TOTAL]", _TotalCount.ToString());
            this.SaveFields();            
        }
        #endregion

        #region PROPERTIES
        /// <summary>
        /// 
        /// </summary>
        public int TotalCount
        {
            get { return this._TotalCount; }
            set { this._TotalCount = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize
        {
            get{return this._PageSize;}
            set{this._PageSize = value;}
        }
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex
        {
            get { return this._PageIndex; }
            set { this._PageIndex = value; }
        }
        #endregion
    }
}