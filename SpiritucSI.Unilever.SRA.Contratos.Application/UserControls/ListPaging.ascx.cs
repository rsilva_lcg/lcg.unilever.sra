﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace SpiritucSI.UserControls.GenericControls
{
    public partial class ListPaging : UserControl
    {
        #region REGISTERED EVENTS
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler PagerChange;
        /// <summary>
        /// 
        /// </summary>
        public void SendPagerChange()
        {
            if (PagerChange != null)
                PagerChange(this, new EventArgs());
        }
        #endregion

        #region PAGE EVENTS
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender( object sender, EventArgs e )
        {
            this.C_btn_Paging.Visible = !this.AllowPaging;
            this.C_btn_NoPaging.Visible = this.AllowPaging; 

        }
        #endregion

        #region CONTROL EVENTS
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_btn_Paging_Click( object sender, EventArgs e )
        {
            this.AllowPaging = true;
            this.SendPagerChange();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void C_btn_NoPaging_Click( object sender, EventArgs e )
        {
            this.AllowPaging = false;
            this.SendPagerChange();
        }
        #endregion
       
        #region PROPERTIES
        /// <summary>
        /// 
        /// </summary>
        public string SourceControlID{get; set;}
        /// <summary>
        /// 
        /// </summary>
        protected Control SourceControl
        {
            get 
            {
                return this.Parent.FindControl(this.SourceControlID);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected bool AllowPaging
        {
            get 
            {
                if (this.SourceControl != null && this.SourceControl.GetType().GetProperty("AllowPaging") != null)
                {
                    return (bool)this.SourceControl.GetType().GetProperty("AllowPaging").GetValue(this.SourceControl,null);
                }
                return false;
            }
            set 
            {
                if (this.SourceControl != null && this.SourceControl.GetType().GetProperty("AllowPaging") != null)
                {
                    this.SourceControl.GetType().GetProperty("AllowPaging").SetValue(this.SourceControl, value, null);
                    if(this.SourceControl.GetType().GetProperty("PageIndex") != null)
                        this.SourceControl.GetType().GetProperty("PageIndex").SetValue(this.SourceControl, 0, null);
                    if (this.SourceControl.GetType().GetProperty("CurrentPageIndex") != null)
                        this.SourceControl.GetType().GetProperty("CurrentPageIndex").SetValue(this.SourceControl, 0, null);
                }
            }
        }
        #endregion        

    }
}