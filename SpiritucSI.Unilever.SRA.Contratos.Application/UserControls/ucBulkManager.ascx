﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucBulkManager.ascx.cs" Inherits="SpiritucSI.Unilever.SRA.UserControls.ucBulkManager" %>
<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<%@ Register TagPrefix="uc1" TagName="ucWFBulk" Src="./ucActionsForBulk.ascx" %>

<div id="BulkManager" runat="server" >
    <div id="Div3" runat="server" class="SubContentCenterTable">
    <div style="text-align: left; padding-bottom: 10px; padding-left: 10px;">
        <asp:Label ID="Label1" Style="font-size: 14px; font-weight: bold;" runat="server" Text='<%$Resources:Title1 %>'></asp:Label>
        <asp:Label ID="Lbl_State" Style="font-size: 14px; font-weight: bold;" runat="server" Text='<%#this.state!=""? "para o estado "+this.state:""%>' ></asp:Label>
    </div>

        <asp:Label ID ="Lbl_Success" runat ="server" Visible ="false" Text="Alterações executadas com sucesso" style=" color:#008000; padding-bottom:5px; font-size:11px;"></asp:Label>
        <asp:Label ID ="Lbl_Error" runat ="server" Visible ="false" Text="Ocorreu um erro ao realizar as alterações" style=" color:#FF0000; padding-bottom:5px; font-size:11px;"></asp:Label>

<%--    <div style="float:right; width:auto; padding-bottom:5px">
      <a href="#" onclick="$('.ob_gCc1').find('input').attr('checked',false); $.each( $('.ob_gCc1'), function() {$(this).find('input').first().attr('checked',true) } ); return false;">seleccionar todos</a>&nbsp;/&nbsp;
        <a href="#" onclick="$('.ob_gCc1').find('input').attr('checked',false); return false;">desseleccionar todos</a>
        <asp:LinkButton ID ="LB_select" runat ="server" Text="seleccionar todos"></asp:LinkButton>&nbsp;/&nbsp;
        <asp:LinkButton ID ="LB_deselect" runat ="server" Text="desseleccionar todos"></asp:LinkButton>
    </div>--%>
    <div style="text-align:left;"> <span>Seleccionar todos:</span> 
    <asp:Repeater ID="rpChk" runat="server" EnableViewState="true">
    <ItemTemplate>
            <asp:CheckBox ID="chk" runat="server" style="top:2px; font-size:11px;" EnableViewState="true" CausesValidation="true"  Text='<%# DataBinder.Eval(Container.DataItem,"Text") %>' Visible='<%# DataBinder.Eval(Container.DataItem,"Visibility") %>' OnDataBinding = "RChk_Databound"  />
            <%--OnCheckedChanged=<%#"BulkSelectAll("+ this.gridBulk.ClientID + ",$(this).attr('checked',checked),"+ DataBinder.Eval(Container.DataItem,"Text")+")" %>--%>
    </ItemTemplate>
    </asp:Repeater>
    </div>
        <div style=" width: 100%; clear:both;"> 
                <obout:Grid ID="gridBulk" Width="100%" Language="pt" AutoPostBackOnSelect="false" Serialize="false"
                AllowPaging="false" PageSize="10000" AutoGenerateColumns="false" AllowMultiRecordSelection="false" AllowRecordSelection="false"
                AllowFiltering="true" AllowPageSizeSelection="false" AllowSorting="true" AllowAddingRecords="false"
                runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                AllowManualPaging="false" OnRowDataBound ="gridBulk_Rowdatabound" >
                <Columns>
                    <obout:Column ID="Column4"  Width="100%" HeaderText='Acções' runat="server"
                        TemplateId="t_wfBulk" Align="left" HeaderStyle-Wrap="true">
                    </obout:Column>
                    <obout:Column ID="Column13" Width="140" DataField="ID_PROCESSO" HeaderText='Processo' runat="server" TemplateId="t_link2"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column2" Width="420" DataField="NOME_FIRMA" HeaderText='Firma' runat="server" TemplateId="t_label2"
                        Align="left">
                    </obout:Column>
                    <obout:Column ID="Column15" Width="90" DataField="DATA_CONTRATO" HeaderText='Data' runat="server" TemplateId="t_label2"
                        Align="left">
                    </obout:Column>
<%--                    <obout:Column ID="Column38" Width="120" DataField="RAMO_ACTIVIDADE" HeaderText='Ramo' runat="server"
                        TemplateId="t_label2" Align="left">
                    </obout:Column>
                    <obout:Column ID="Column46" Width="80" DataField="NIF" HeaderText='NIF' runat="server" TemplateId="t_label2"
                        Align="left">
                    </obout:Column>--%>
                    <obout:Column ID="Column1" Width="10" DataField="INSTANCE_ID" HeaderText='' runat="server"
                        TemplateId="t_label2" Align="left" HeaderStyle-Wrap="true" Visible="false">
                    </obout:Column>
                    <obout:Column ID="Column5" Width="10" DataField="ID_CONTRATO" HeaderText='' runat="server"
                        TemplateId="t_label2" Align="left" HeaderStyle-Wrap="true" Visible ="false">
                    </obout:Column>
                </Columns>
                <Templates>
                    <obout:GridTemplate runat="server" ID="t_label2">
                        <Template>
                            <asp:Label ID="lbl1" runat="server" Text='<%#Container.Value %>' Style=""></asp:Label>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_link2">
                        <Template>
                            <asp:LinkButton ID="lnk1" EnableViewState="true" runat="server" CommandArgument='<%#Container.DataItem["ID_PROCESSO"].ToString()%>' Text='<%# Container.Value %>' PostBackUrl='<%#"~/contrato.aspx?idContrato="+Container.DataItem["ID_CONTRATO"].ToString()%>'></asp:LinkButton>
                        </Template>
                    </obout:GridTemplate>
                    <obout:GridTemplate runat="server" ID="t_wfBulk">
                        <Template>
                            <uc1:ucWFBulk ID ="WFBulk" runat ="server" EnableViewState ="true" OnOnWFLoaded="wf_Loaded" />
                        </Template>
                    </obout:GridTemplate>
                </Templates>
            </obout:Grid>

        </div> 
    <div Id ="actionButtons" runat ="server" style="padding-top:10px;">
    <div style="float:left;">
    <div style ="float:left;"><asp:Button ID="Btt_Cancel" runat="server" Text="Cancelar" PostBackUrl="../SRA_Dashboard.aspx" /></div>
    <div style="float:right;"><asp:Button ID="Btt_Save" runat ="server" Text="Executar" OnClick="bntSave_click" /></div>
    </div>
    </div> 
    </div>
    <div style="float: left; height: 10px; width: 100%">
        &nbsp;</div>
</div>

