﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class ImageTextButton : System.Web.UI.UserControl,IButtonControl
{
    public enum ButtonTextPosition { Left, Rigth, Top, Bottom };

    #region FIELDS
        protected ButtonTextPosition textPosition = ButtonTextPosition.Rigth;
        protected string imageSrc = "";
        protected string buttonText = "";
        protected string tooltip = "";
        protected string ClientClick = "";
        protected string TextcssClass = "";
        protected bool causeValidation = true;
        protected string commandArgument = "";
        protected string commandName = "";
        protected string postBackUrl = "";
        protected string validationGroup = "";
    #endregion

    #region PROPERTIES
        public ButtonTextPosition TextPosition
        {
            get { return this.textPosition; }
            set { this.textPosition = value; }
        }
        public string ImageUrl
        {
            get { return this.imageSrc; }
            set { this.imageSrc = value; }
        }
        public string ToolTip
        {
            get { return this.tooltip; }
            set { this.tooltip = value; }
        }
        public string TextCssClass
        {
            get { return this.TextcssClass; }
            set { this.TextcssClass = value; }
        }
        public override string ClientID
        {
            get
            {
                return this.C_btn_Image.ClientID;
            }
        }
        public string OnClientClick
        {
            get { return this.ClientClick; }
            set { this.ClientClick = value; }
        }
    #endregion

    #region PAGE EVENTS
        protected void Page_PreRender(object sender, EventArgs e)
        {
            #region IMAGE_BUTTON
                if (imageSrc != "")
                {
                    this.C_btn_Image.ImageUrl = imageSrc;
                    this.C_btn_Image.ToolTip=tooltip;
                    this.C_btn_Image.OnClientClick = ClientClick;
                    this.C_btn_Image.CausesValidation= causeValidation;
                    this.C_btn_Image.CommandArgument=commandArgument;
                    this.C_btn_Image.CommandName = commandName;
                    this.C_btn_Image.PostBackUrl = postBackUrl;
                    this.C_btn_Image.ValidationGroup=validationGroup;
                }
                this.C_btn_Image.Visible = imageSrc != "";
            #endregion

            #region TEXT BUTTONS
                if (buttonText != "")
                {
                    #region TEXT 
                        this.C_btn_Top.Text     = buttonText;
                        this.C_btn_Bottom.Text  = buttonText;    
                        this.C_btn_Left.Text    = buttonText + "&nbsp;";
                        this.C_btn_Rigth.Text   = "&nbsp;" + buttonText;                        
                    #endregion

                    #region TOOLTIP
                        this.C_btn_Top.ToolTip = tooltip;
                        this.C_btn_Bottom.ToolTip = tooltip;
                        this.C_btn_Left.ToolTip = tooltip;
                        this.C_btn_Rigth.ToolTip = tooltip;
                    #endregion

                    #region CLIENTCLICK
                        this.C_btn_Top.OnClientClick = ClientClick;
                        this.C_btn_Bottom.OnClientClick = ClientClick;
                        this.C_btn_Left.OnClientClick = ClientClick;
                        this.C_btn_Rigth.OnClientClick = ClientClick;
                    #endregion

                    #region CSSCLASS
                        this.C_btn_Top.CssClass=TextcssClass;
                        this.C_btn_Bottom.CssClass=TextcssClass;
                        this.C_btn_Left.CssClass=TextcssClass;
                        this.C_btn_Rigth.CssClass=TextcssClass;
                    #endregion

                    #region CAUSE VALIDATION
                        this.C_btn_Top.CausesValidation=causeValidation;
                        this.C_btn_Bottom.CausesValidation=causeValidation;
                        this.C_btn_Left.CausesValidation=causeValidation;
                        this.C_btn_Rigth.CausesValidation=causeValidation;
                    #endregion

                    #region COMMAND ARGUMENT
                        this.C_btn_Top.CommandArgument=commandArgument;
                        this.C_btn_Bottom.CommandArgument=commandArgument;
                        this.C_btn_Left.CommandArgument=commandArgument;
                        this.C_btn_Rigth.CommandArgument=commandArgument;
                    #endregion

                    #region COMMAND NAME
                        this.C_btn_Top.CommandName=commandName;
                        this.C_btn_Bottom.CommandName=commandName;
                        this.C_btn_Left.CommandName=commandName;
                        this.C_btn_Rigth.CommandName=commandName;
                    #endregion
                    
                    #region POSTBACK URL
                        this.C_btn_Top.PostBackUrl=postBackUrl;
                        this.C_btn_Bottom.PostBackUrl=postBackUrl;
                        this.C_btn_Left.PostBackUrl=postBackUrl;
                        this.C_btn_Rigth.PostBackUrl=postBackUrl;
                    #endregion

                    #region VALIDATION GROUP
                        this.C_btn_Top.ValidationGroup=validationGroup;
                        this.C_btn_Bottom.ValidationGroup=validationGroup;
                        this.C_btn_Left.ValidationGroup=validationGroup;
                        this.C_btn_Rigth.ValidationGroup = validationGroup;
                    #endregion
                }
                #region VISIBILITY
                    this.C_btn_Top.Visible      = textPosition == ButtonTextPosition.Top && buttonText != "";
                    this.C_btn_Bottom.Visible   = textPosition == ButtonTextPosition.Bottom && buttonText != "";
                    this.C_btn_Left.Visible     = textPosition == ButtonTextPosition.Left && buttonText != "";
                    this.C_btn_Rigth.Visible    = textPosition == ButtonTextPosition.Rigth && buttonText != "";
                #endregion
            #endregion

            #region ATTRIBUTES
                foreach (string key in this.Attributes.Keys)
                {
                    this.C_btn_Image.Attributes[key] = this.Attributes[key];
                    this.C_btn_Top.Attributes[key] = this.Attributes[key];
                    this.C_btn_Bottom.Attributes[key] = this.Attributes[key];
                    this.C_btn_Left.Attributes[key] = this.Attributes[key];
                    this.C_btn_Rigth.Attributes[key] = this.Attributes[key];                    
                }
            #endregion
        }
    #endregion

    #region CONTROL EVENTS
        protected void ButtonClick(object sender, EventArgs e)
        {
            if (this.Click != null)
            {
                this.Click(sender, e);
            }        
        }
    #endregion

    #region IButtonControl Members
        /// <summary>
        /// 
        /// </summary>
        public bool CausesValidation
        {
            get { return this.causeValidation; }
            set { this.causeValidation = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler Click;
        /// <summary>
        /// 
        /// </summary>
        event EventHandler IButtonControl.Click
        {
            add { lock (Click) { Click += value; } }
            remove { lock (Click) { Click -= value; } }
        }
        /// <summary>
        /// 
        /// </summary>
        public event CommandEventHandler Command;
        /// <summary>
        /// 
        /// </summary>
        event CommandEventHandler IButtonControl.Command
        {
            add { lock (Command) { Command += value; } }
            remove { lock (Command) { Command -= value; } }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CommandArgument
        {
            get { return this.commandArgument; }
            set { this.commandArgument = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CommandName
        {
            get { return this.commandName; }
            set { this.commandName = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string PostBackUrl
        {
            get { return this.postBackUrl; }
            set { this.postBackUrl = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Text
        {
            get { return this.buttonText; }
            set { this.buttonText = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ValidationGroup
        {
            get { return this.validationGroup; }
            set { this.validationGroup = value; }
        }
    #endregion
}
