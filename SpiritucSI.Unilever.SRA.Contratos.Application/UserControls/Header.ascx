<%@ Control Language="c#" AutoEventWireup="True" CodeFile="Header.ascx.cs" Inherits="Header" %>
<table class="table" cellpadding="0" cellspacing="0" align="center" id="Table1" style="border-bottom:solid 1px gray;">
    <tr>
        <td width="10%" align="left" valign="middle" rowspan="2">
            <a id="lnkHome" runat="server" href="~/Default.aspx"><asp:Image ID="C_img_Logo" runat="server" meta:resourcekey="C_img_Logo"/></a>
        </td>
        <td>&nbsp;&nbsp;</td>
        <td colspan="4" height="30" align="center">
            <asp:Label ID="C_lbl_header" runat="server" meta:resourcekey="C_lbl_header" 
                BorderColor="#333399"  ForeColor="#333399" /> <!-- ForeColor="#333399"-->
        </td>
    </tr>
    
    <tr>
        <td></td>
        <td colspan="2" align="left" style="width:60%" valign="bottom" ><!-- bgcolor="#333399"-->
            <asp:Menu ID="Menu1" runat="server" DataSourceID="menu" StaticDisplayLevels="2" Orientation="Horizontal"
                 DynamicHorizontalOffset="0" StaticSubMenuIndent="0" 
                StaticEnableDefaultPopOutImage="False" Height="20px" 
                onmenuitemdatabound="Menu1_MenuItemDataBound">
                <DynamicMenuItemStyle CssClass="Menubutton" ItemSpacing="4" Width="100%" VerticalPadding="3" />
                <DynamicMenuStyle BackColor="LightGray" BorderColor="DarkGray" HorizontalPadding="4" />
                <StaticMenuItemStyle CssClass="Menubutton" VerticalPadding="5" Width="100%" ItemSpacing="5" />                
            </asp:Menu>
        </td>
        <td colspan="2" align="right" valign="bottom" ><!-- bgcolor="#333399"-->
            <asp:SiteMapPath ID="SiteMapPath1" runat="server" PathDirection="RootToCurrent" 
                ForeColor="White">
                <NodeStyle ForeColor="White" />
            </asp:SiteMapPath>
        </td>
    </tr>    
</table>
<asp:SiteMapDataSource ID="menu" runat="server" SiteMapProvider="XmlSitemapProvider" />
