﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucActionsForBulk.ascx.cs" Inherits="SpiritucSI.Unilever.SRA.UserControls.ucActionsForBulk" %>
<table style="width:100%;"><tr><td>
<asp:HiddenField ID ="HF_guid" runat="server" />
<asp:Repeater ID="rpChk" runat="server" EnableViewState="true">
    <ItemTemplate>
            <asp:CheckBox ID="chk" runat="server" style="top:2px; font-size:11px;" EnableViewState="true" CausesValidation="true"  Text='<%# DataBinder.Eval(Container.DataItem,"Text") %>' Visible='<%# DataBinder.Eval(Container.DataItem,"Visibility") %>' ActionID='<%# DataBinder.Eval(Container.DataItem,"Id") %>' onclick="var checked =$(this).attr('checked');$(this).parent().parent().parent().find('input').attr('checked',false);$(this).attr('checked',checked);"/>
    </ItemTemplate>
</asp:Repeater>
</td></tr></table>