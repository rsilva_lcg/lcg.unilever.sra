﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;

namespace SpiritucSI.UserControls.GenericControls
{
    [ValidationProperty("Value")]
    public partial class DatePicker : UserControl
    {
        public enum CalendarDisplayMode { TextBox, Calendar }

        #region FIELDS
        protected bool _Selectable = false;
        //protected CalendarDisplayMode _DisplayMode = CalendarDisplayMode.TextBox;
        protected List<DateTime> _DisabledDates = new List<DateTime>();
        protected DateTime _MinEnabledDate = DateTime.MinValue;
        protected DateTime _MaxEnabledDate = DateTime.MaxValue;
        protected List<DateTime> _EventDates = new List<DateTime>();
        #endregion

        #region METHODS
        protected void FillYear( int year )
        {
            this.C_rbl_Year.Items.Clear();
            for (int i = year - 2; i <= year + 3; ++i)
            {
                if (i >= MinEnabledDate.Year && i <= MaxEnabledDate.Year)
                {
                    ListItem li = new ListItem(i.ToString(), i.ToString());
                    if (i == year)
                        li.Selected = true;
                    this.C_rbl_Year.Items.Add(li);
                }
            }
            if (this.C_rbl_Year.SelectedValue.Length == 0)
                this.C_rbl_Year.SelectedIndex = 0;
            this.C_rbl_Year.Attributes["year"] = year.ToString();
        }
        public void Clear()
        {
            this.C_ui_Calendar.VisibleDate = new DateTime(DateTime.Now.Year,DateTime.Now.Month,1);
            this.C_ui_Calendar.SelectedDate = DateTime.Now;
            this.C_txt_Date.Text = "";
            if (this.SelectionChanged != null)
                this.SelectionChanged(this, new EventArgs());
        }
        protected void LoadFields()
        {
            if (this.ViewState[this.ClientID + "_DisplayMode"] != null)
            {
                this.DisplayMode = (CalendarDisplayMode)this.ViewState[this.ClientID + "_DisplayMode"];
            }
            if (this.ViewState[this.ClientID + "_DisabledDates"] != null)
            {
                this._DisabledDates = (List<DateTime>)this.ViewState[this.ClientID + "_DisabledDates"];
            }
            if (this.ViewState[this.ClientID + "_MinEnabledDate"] != null)
            {
                this._MinEnabledDate = (DateTime)this.ViewState[this.ClientID + "_MinEnabledDate"];
            }
            if (this.ViewState[this.ClientID + "_MaxEnabledDate"] != null)
            {
                this._MaxEnabledDate = (DateTime)this.ViewState[this.ClientID + "_MaxEnabledDate"];
            }
            if (this.ViewState[this.ClientID + "_EventDates"] != null)
            {
                this._EventDates = (List<DateTime>)this.ViewState[this.ClientID + "_EventDates"];
            }
        }
        protected void SaveFields()
        {
            this.ViewState[this.ClientID + "_DisplayMode"] = this.DisplayMode;
            this.ViewState[this.ClientID + "_DisabledDates"] = this._DisabledDates;
            this.ViewState[this.ClientID + "_MinEnabledDate"] = this._MinEnabledDate;
            this.ViewState[this.ClientID + "_MaxEnabledDate"] = this._MaxEnabledDate;
            this.ViewState[this.ClientID + "_EventDates"] = this._EventDates;
        }
        public void SelectWeekOfDay( DateTime day )
        { 
            DateTime d = day.AddDays(1);
            this.C_ui_Calendar.SelectedDates.Clear();
            do
            {
                d = d.AddDays(-1);
                this.C_ui_Calendar.SelectedDates.Add(d);
            } while (d.DayOfWeek.ToString() != this.C_ui_Calendar.FirstDayOfWeek.ToString());
            d = day.AddDays(1);
            while (d.DayOfWeek.ToString() != this.C_ui_Calendar.FirstDayOfWeek.ToString())
            {
                this.C_ui_Calendar.SelectedDates.Add(d);
                d = d.AddDays(1);
            }
            this.C_ui_Calendar.VisibleDate = day;
            this.C_ui_Calendar_Select(this.C_ui_Calendar, new EventArgs());
        }
        public void SelectMonthOfDay( DateTime day )
        {
            DateTime d = new DateTime(day.Year,day.Month,1);
            this.C_ui_Calendar.SelectedDates.Clear();
            while (d.Month==day.Month)
            {
                this.C_ui_Calendar.SelectedDates.Add(d);
                d = d.AddDays(1);
            }
            this.C_ui_Calendar.VisibleDate = day;
            this.C_ui_Calendar_Select(this.C_ui_Calendar, new EventArgs());
        }
        #endregion

        #region PAGE EVENTS
        protected void Page_Load( object sender, EventArgs e )
        {
            if (IsPostBack) LoadFields(); 
            this._Selectable = false;
            if (this.C_rbl_Year.Attributes["year"] ==null)
            {
                this.C_rbl_Year.Attributes["year"] = this.C_ui_Calendar.SelectedDate.Year.ToString();
            }            
        }
        protected void Page_PreRender( object sender, EventArgs e )
        {            
            this.C_txt_Date.Attributes["onclick"] = "javascript:document.getElementById('" + this.C_btn_calendar.ClientID + "').click();";
            this.C_ui_Calendar.DataBind();

            if (this.DisplayMode == CalendarDisplayMode.TextBox)
            {
                this.TextBox_box.Visible = true;
                this.Calendar_box.Style["margin-top"] = "20px";
                this.Calendar_box.Style["position"] = "absolute";
            }
            else
            {
                this.TextBox_box.Visible = false;
                this.Calendar_box.Style["margin-top"] = "";
                this.Calendar_box.Style["position"] = "";
            }
            
            this.C_btn_PrevMonth.Visible = this.C_ui_Calendar.VisibleDate > MinEnabledDate ;
            this.C_btn_NextMonth.Visible = this.C_ui_Calendar.VisibleDate.AddMonths(1) < MaxEnabledDate ;
            this.C_btn_YearDown.Visible = int.Parse(this.C_rbl_Year.Attributes["year"]) - 3 >= MinEnabledDate.Year;
            this.C_btn_YearUp.Visible = int.Parse(this.C_rbl_Year.Attributes["year"]) + 4 <= MaxEnabledDate.Year;
            this.Calendar_box.Visible = _Selectable || DisplayMode == CalendarDisplayMode.Calendar;
            this.C_btn_Mes.Text = this.C_ui_Calendar.VisibleDate.ToString("MMMM") + " de " + this.C_ui_Calendar.VisibleDate.ToString("yyyy");
            this.SaveFields();
        }
        #endregion

        #region CONTROL EVENTS
        protected void C_btn_calendar_Click( object sender, ImageClickEventArgs e )
        {
            if (this.IsClear)
                this.C_ui_Calendar.SelectedDate = DateTime.Now;
            if (!this.Calendar_box.Visible)
                this.C_ui_Calendar.VisibleDate = new DateTime(this.C_ui_Calendar.SelectedDate.Year,this.C_ui_Calendar.SelectedDate.Month,1);
            _Selectable = !this.Calendar_box.Visible;
            this.Year_box.Visible = false;
        }
        protected void C_ui_Calendar_Select( object sender, EventArgs e )
        {                     
            var days = ( 
                from DateTime d in this.C_ui_Calendar.SelectedDates
               where !DisabledDates.Contains(d)
                  && d >= MinEnabledDate
                  && d <= MaxEnabledDate
               select d
            ).ToList();

            if (days.Count==0)
            {
                if (this.SelectedDate < MinEnabledDate)
                {
                    this.SelectedDate = MinEnabledDate;
                }
                else
                {
                    this.SelectedDate = MaxEnabledDate;
                }
            }
            else
            {
                if (days.Count > 1)
                {
                    this.C_txt_Date.Text = days.First().ToString("(dd")
                        + days.Last().ToString("-dd)-MM-yyyy");
                }
                else
                {
                    this.C_txt_Date.Text = this.C_ui_Calendar.SelectedDate.ToString("dd-MM-yyyy");
                }
                this._Selectable = false;
                if (this.SelectionChanged != null)
                    this.SelectionChanged(this, new EventArgs());
            }
        }
        protected void C_ui_Calendar_VisibleMonthChanged( object sender, MonthChangedEventArgs e )
        {
            this._Selectable = true;
        }
        protected void C_ui_Calendar_DayRender( object sender, DayRenderEventArgs e )
        {
            if (DisabledDates.Contains(e.Day.Date)
                || e.Day.Date < _MinEnabledDate
                || e.Day.Date > _MaxEnabledDate
                || (!OtherMonthDaysEnabled && e.Day.IsOtherMonth))
            {
                e.Day.IsSelectable = false;
                e.Cell.Style["text-decoration"] = "line-through";
                e.Cell.Enabled = false;
            }
            if (!OtherMonthDaysEnabled && e.Day.IsOtherMonth)
                e.Cell.Text = "";
            if (_EventDates.Contains(e.Day.Date))
            {
                e.Cell.Style["font-weight"] = "Bold";
                e.Cell.Style["Font-Size"] = "X-Small";
            }
                
        }
        protected void C_btn_NextMonth_Click( object sender, ImageClickEventArgs e )
        {
            this.C_ui_Calendar.VisibleDate = this.C_ui_Calendar.VisibleDate.AddMonths(1);
            this._Selectable = true;
        }
        protected void C_btn_PrevMonth_Click( object sender, ImageClickEventArgs e )
        {
            this.C_ui_Calendar.VisibleDate = this.C_ui_Calendar.VisibleDate.AddMonths(-1);
            this._Selectable = true;
        }
        protected void C_btn_Mes_Click( object sender, EventArgs e )
        {
            this.C_rbl_Month.SelectedValue = this.C_ui_Calendar.VisibleDate.Month.ToString();
            this.FillYear(this.C_ui_Calendar.VisibleDate.Year);
            this.Year_box.Visible = true;
            this._Selectable = true;
        }
        protected void C_btn_Today_Click( object sender, EventArgs e )
        {
            this.C_ui_Calendar.VisibleDate = new DateTime(DateTime.Now.Year,DateTime.Now.Month,1);
            this._Selectable = true;
        }
        protected void C_btn_Clear_Click( object sender, EventArgs e )
        {
            this.Clear();
            this._Selectable = false;
        }
        protected void C_btn_Select_Click( object sender, EventArgs e )
        {
            this.Year_box.Visible = false;
            this._Selectable = true;
            this.C_ui_Calendar.VisibleDate = new DateTime(int.Parse(this.C_rbl_Year.SelectedValue), int.Parse(this.C_rbl_Month.SelectedValue), 1);
        }
        protected void C_btn_Close_Click( object sender, EventArgs e )
        {
            this.Year_box.Visible = false;
            this._Selectable = true;
        }
        protected void C_btn_YearUp_Click( object sender, EventArgs e )
        {
            this.FillYear(int.Parse(this.C_rbl_Year.Attributes["year"]) + 6);
            this._Selectable = true;
        }
        protected void C_btn_YearDown_Click( object sender, EventArgs e )
        {
            this.FillYear(int.Parse(this.C_rbl_Year.Attributes["year"]) - 6);
            this._Selectable = true;
        }
        #endregion

        #region PROPERTIES
        public DateTime SelectedDate
        {
            get { return (this.C_txt_Date.Text == "" ? DateTime.MinValue : this.C_ui_Calendar.SelectedDate); }
            set
            {
                this.C_ui_Calendar.VisibleDate = new DateTime(value.Date.Year,value.Date.Month,1);
                this.C_ui_Calendar.SelectedDate = value.Date;
                //this.C_ui_Calendar.DataBind();
                this.C_txt_Date.Text = value == DateTime.MinValue ? "" : value.ToString("dd-MM-yyyy");
            }
        }
        public List<DateTime> SelectedDates
        {
            get
            {
                if (this.C_txt_Date.Text == "")
                {
                    return new List<DateTime>();
                }
                return (
                    from DateTime d in this.C_ui_Calendar.SelectedDates
                    where !DisabledDates.Contains(d)
                       && d >= MinEnabledDate
                       && d <= MaxEnabledDate
                    select d
                ).ToList();
            }
        }
        public FirstDayOfWeek FirstDayofWeek
        {
            get { return this.C_ui_Calendar.FirstDayOfWeek; }
            set { this.C_ui_Calendar.FirstDayOfWeek = value; }
        }
        public bool Enabled
        {
            get { return this.C_txt_Date.Enabled; }
            set
            {
                this.C_txt_Date.Enabled = value;
                this.C_ui_Calendar.Enabled = value;
                this.C_btn_calendar.Visible = value;
            }
        }
        public bool AllowClear
        {
            get { return this.C_btn_Clear.Visible; }
            set { this.C_btn_Clear.Visible = value; }
        }
        public string ToolTip
        {
            get { return this.C_txt_Date.ToolTip; }
            set { this.C_txt_Date.ToolTip = value; }
        }
        public string TextCssClass
        {
            get { return this.C_txt_Date.CssClass; }
            set { this.C_txt_Date.CssClass = value; }
        }
        public CalendarSelectionMode SelectionMode
        {
            set { this.C_ui_Calendar.SelectionMode = value; }
            get { return this.C_ui_Calendar.SelectionMode; }
        }
        public CalendarDisplayMode DisplayMode
        {
            get;
            set;
        }
        public List<DateTime> DisabledDates
        {
            get { return this._DisabledDates; }
            set { this._DisabledDates = value; }
        }
        public List<DateTime> EventDates
        {
            get { return this._EventDates; }
            set { this._EventDates = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime MinEnabledDate
        {
            get { return this._MinEnabledDate; }
            set { this._MinEnabledDate = value.Date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime MaxEnabledDate
        {
            get { return this._MaxEnabledDate; }
            set { this._MaxEnabledDate = value.Date; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(true)]
        public bool OtherMonthDaysEnabled
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsClear
        {
            get { return this.C_txt_Date.Text == ""; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Value
        {
            get{return this.C_txt_Date.Text;}
        }
        #endregion

        #region REGITERED EVENTS
        public event EventHandler SelectionChanged;
        #endregion

    }
}