﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class ProductTreeDetails : System.Web.UI.UserControl
{
    #region FIELDS
   
    #endregion

    #region METHODS
    public override void RenderControl(HtmlTextWriter writer)
    {
        if (this.SourceTreeNode != null)
        {
            TextWriter tw = new StringWriter();
            tw.NewLine = "";
            HtmlTextWriter htw = new HtmlTextWriter(tw);
            htw.NewLine = "";
            base.RenderControl(htw);
            string beginTags = "</a></td></tr><tr><td></td>[TAGS]";

            TreeNode tt = this.SourceTreeNode;
            for (int i = this.SourceTreeNode.Depth; i > 0; --i)
            {
                if (tt.Parent.ChildNodes.IndexOf(tt) < tt.Parent.ChildNodes.Count - 1)
                {
                    beginTags = beginTags.Replace("[TAGS]", "[TAGS]<td class='TreeNodeLines'>&nbsp;</td>");
                }
                else
                {
                    beginTags = beginTags.Replace("[TAGS]", "[TAGS]<td></td>");
                }
                tt = tt.Parent;
            }
            beginTags = beginTags.Replace("[TAGS]", "");
            this.SourceTreeNode.Text = this.SourceTreeNode.Text
                                     + beginTags
                                     + "<td width='100%'>" + tw.ToString() + "</td></tr><tr><td><a>";

        }
    }
    #endregion

    #region PAGE EVENTS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (isProduto)
        {
            if (this.Session["ActividadeInicio"] != null)
            {
                this.ActividadeInicio = (DateTime)this.Session["ActividadeInicio"];
            }
            if (this.Session["ActividadeFim"] != null)
            {
                this.ActividadeFim = (DateTime)this.Session["ActividadeFim"];
            }
            if (this.Session["ComprasInicio"] != null)
            {
                this.ComprasInicio = (DateTime)this.Session["ComprasInicio"];
            }
            if (this.Session["ComprasFim"] != null)
            {
                this.ComprasFim = (DateTime)this.Session["ComprasFim"];
            }
        }
        this.C_txt_BudgetMarketing.CssClass.Replace(" errorField", "");
        this.C_txt_BudgetVendas.CssClass.Replace(" errorField", "");
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (isProduto)
        {
            if (this.ActividadeInicio == DateTime.MinValue)
            {
                this.ActividadeInicio = DateTime.Now;
            }
            if(this.ActividadeFim  == DateTime.MinValue)
            {
                this.ActividadeFim = DateTime.Now.AddDays(1);
            }
            if(this.ComprasInicio  == DateTime.MinValue)
            {
                this.ComprasInicio = DateTime.Now;
            }
            if(this.ComprasFim  == DateTime.MinValue)
            {
                this.ComprasFim = DateTime.Now.AddDays(1);
            }
        }
        else
        {
            string group = "TPR_Apply";
            this.C_btn_Apply.ValidationGroup = group;
            this.C_val_BudgetMarketing_Empty.ValidationGroup = group;
            this.C_val_BudgetMarketing.ValidationGroup = group;
            this.C_val_BudgetVendas_Empty.ValidationGroup = group;
            this.C_val_BudgetVendas.ValidationGroup = group;
            this.C_val_ActividadeDatas.ValidationGroup = group;
            this.C_val_ComprasDatas.ValidationGroup = group;
            this.C_val_PVP_Empty.ValidationGroup = group;
            this.C_val_PVP.ValidationGroup = group;
            this.C_val_Qtd.ValidationGroup = group;
            this.C_val_Qtd_Empty.ValidationGroup = group;
        }
        this.C_box_UnidadeHeader.Visible = this.isProduto;
        this.C_box_UnidadeValor.Visible = this.isProduto;
        this.C_cb_QtdCopy.Visible = !this.isProduto;
        this.C_cb_PVPCopy.Visible = !this.isProduto;
        this.C_btn_Apply.Visible = !this.isProduto;
        this.C_dp_ActividadeInicio.AllowClear = !this.isProduto;
        this.C_dp_ActividadeFim.AllowClear = !this.isProduto;
        this.C_dp_ComprasInicio.AllowClear = !this.isProduto;
        this.C_dp_ComprasFim.AllowClear = !this.isProduto;
        this.C_btn_Apply.Attributes["onclick"] = "javascript:return confirm('APLICAR VALORES\\n\\nTodos os produtos da hierarquia serão actualizados.\\n\\n\\nDeseja continuar?');";
        this.C_btn_Insert.Attributes["onclick"] = "javascript:return confirm('INSERIR PRODUTOS\\n\\nTodos os produtos marcados serão inseridos ou actualizados na lista de produtos.\\n\\n\\nDeseja continuar?');";

        if (isProduto)
        {
            this.Session["ActividadeInicio"] = this.ActividadeInicio;
            this.Session["ActividadeFim"] = this.ActividadeFim;
            this.Session["ComprasInicio"] = this.ComprasInicio;
            this.Session["ComprasFim"] = this.ComprasFim;
        }

    }
    #endregion

    #region CONTROL EVENTS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txt_Empty_Validate(object sender, ServerValidateEventArgs e)
    {
        if (this.isProduto)
        {
            CustomValidator cv = (CustomValidator)sender;
            WebControl c = (WebControl)this.FindControl(cv.ControlToValidate);
            if (e.Value.Length == 0)
            {
                e.IsValid = false;
                if (!c.CssClass.Contains("errorField"))
                {
                    c.CssClass = c.CssClass + " errorField";
                }
            }
            else
            {
                e.IsValid = true;
                c.CssClass = c.CssClass.Replace(" errorField", "");
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txt_Percent_validate(object sender, ServerValidateEventArgs e)
    {
            CustomValidator cv = (CustomValidator)sender;
            WebControl c = (WebControl)this.FindControl(cv.ControlToValidate);
            double d = 0;
            if (double.TryParse(e.Value.Replace(".", ","), out d) && d>=0 && d<=100)
            {
                e.IsValid = true;
                c.CssClass = c.CssClass.Replace(" errorField", "");
            }
            else
            {
                e.IsValid = false;
                if (!c.CssClass.Contains("errorField"))
                {
                    c.CssClass = c.CssClass + " errorField";
                }
            }
        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txt_Value_validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        WebControl c = (WebControl)this.FindControl(cv.ControlToValidate);
        double d = 0;
        if (double.TryParse(e.Value.Replace(".", ","), out d) && d >= 0 && d < 1000000)
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
        }
        else
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }

    }
    protected void C_val_ActividadeDatas_Validate(object sender, ServerValidateEventArgs e)
    {
        string css = this.C_dp_ActividadeInicio.TextCssClass;
        if (!(this.C_dp_ActividadeInicio.IsClear || this.C_dp_ActividadeFim.IsClear) && this.C_dp_ActividadeInicio.SelectedDate > this.C_dp_ActividadeFim.SelectedDate)
        {
            e.IsValid = false;
            if (!css.Contains("errorField"))
            {
                css = css + " errorField";
            }
        }
        else
        {
            e.IsValid = true;
             css = css.Replace(" errorField", "");
        }
        this.C_dp_ActividadeInicio.TextCssClass = css;
        this.C_dp_ActividadeFim.TextCssClass = css;
    }
    protected void C_val_ComprasDatas_Validate(object sender, ServerValidateEventArgs e)
    {
        string css = this.C_dp_ComprasInicio.TextCssClass;
        if (!(this.C_dp_ComprasInicio.IsClear || this.C_dp_ComprasFim.IsClear) && this.C_dp_ComprasInicio.SelectedDate > this.C_dp_ComprasFim.SelectedDate)
        {
            e.IsValid = false;
            if (!css.Contains("errorField"))
            {
                css = css + " errorField";
            }
        }
        else
        {
            e.IsValid = true;
            css = css.Replace(" errorField", "");
        }
        this.C_dp_ComprasInicio.TextCssClass = css;
        this.C_dp_ComprasFim.TextCssClass = css;
    }  
    protected void C_btn_Apply_Click(object sender, EventArgs e)
    {
        if (this.onDataApply != null)
        {
            onDataApply(this, new EventArgs());
        }
    }
    protected void C_btn_Insert_Click(object sender, EventArgs e)
    {
        if (this.onDataSave != null)
        {
            onDataSave(this, new EventArgs());
        }
    }
    protected void C_dp_ActividadeInicio_Changed(object sender, EventArgs e)
    {
        this.C_dp_ComprasInicio.SelectedDate = this.C_dp_ActividadeInicio.SelectedDate;
    }
    protected void C_dp_ActividadeFim_Changed(object sender, EventArgs e)
    {
        this.C_dp_ComprasFim.SelectedDate = this.C_dp_ActividadeFim.SelectedDate;
    }
    #endregion

    #region PROPERTIES
    public TreeNode SourceTreeNode { get; set; }
    public bool isProduto { get; set; }
    public DateTime ActividadeInicio
    {
        get { return this.C_dp_ActividadeInicio.SelectedDate; }
        set { this.C_dp_ActividadeInicio.SelectedDate = value; }
    }
    public DateTime ActividadeFim
    {
        get { return this.C_dp_ActividadeFim.SelectedDate; }
        set { this.C_dp_ActividadeFim.SelectedDate = value; }
    }
    public DateTime ComprasInicio
    {
        get { return this.C_dp_ComprasInicio.SelectedDate; }
        set { this.C_dp_ComprasInicio.SelectedDate = value; }
    }
    public DateTime ComprasFim
    {
        get { return this.C_dp_ComprasFim.SelectedDate; }
        set { this.C_dp_ComprasFim.SelectedDate = value; }
    }
    public double BudgetVendas
    {
        get
        {
            double d = 0;
            if (double.TryParse(this.C_txt_BudgetVendas.Text.Replace(".", ","), out d))
            {
                return d;
            }
            return double.NaN;
        }
        set 
        {
            this.C_txt_BudgetVendas.Text = double.IsNaN(value) ? "" : Math.Round(value, 2).ToString();
        }
    }
    public double BudgetMarketing
    {
        get
        {
            double d = 0;
            if (double.TryParse(this.C_txt_BudgetMarketing.Text.Replace(".", ","), out d))
            {
                return d;
            }
            return double.NaN;
        }
        set { this.C_txt_BudgetMarketing.Text = double.IsNaN(value) ? "" : Math.Round(value, 2).ToString(); }
    }
    public double PVP
    {
        get
        {
            double d = 0;
            if (double.TryParse(this.C_txt_PVP.Text.Replace(".", ","), out d))
            {
                return d;
            }
            return double.NaN;
        }
        set { this.C_txt_PVP.Text = double.IsNaN(value) ? "" : Math.Round(value, 3).ToString(); }
    }
    public double Quantidade
    {
        get
        {
            double d = 0;
            if (double.TryParse(this.C_txt_Qtd.Text.Replace(".", ","), out d))
            {
                return d;
            }
            return double.NaN;
        }
        set { this.C_txt_Qtd.Text = double.IsNaN(value) ? "" : Math.Round(value, 3).ToString(); }
    }
    public bool PVPCopy
    {
        get { return !this.C_cb_PVPCopy.Checked; }
    }
    public bool QuantidadeCopy
    {
        get { return !this.C_cb_QtdCopy.Checked; }
    }
    public string UnidadeBase
    {
        get { return this.C_txt_Unidade.Text; }
        set { this.C_txt_Unidade.Text = value; }
    }
    #endregion

    #region REGITERED EVENTS
    public event EventHandler onDataApply;
    public event EventHandler onDataSave;
    #endregion
}
