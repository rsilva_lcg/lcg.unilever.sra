﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SpiritucSI.Unilever.SRA.Business.Budget;

public partial class UserControls_ucBudgetSummary : System.Web.UI.UserControl
{
    public string Username { get; set; }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        Budget bllBudget = new Budget(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        DataTable budgetSummary = bllBudget.BudgetGet(this.Username);
        if (budgetSummary.Rows.Count.Equals(0))
        {
           this.txb_Total.Text = "N/A";
           this.txb_Available.Text = "N/A";
           this.txb_Captured.Text = "N/A";
           this.txb_Authorized.Text = "N/A";
        }
        else
        {
            this.txb_Total.Text = budgetSummary.Rows[0]["BudgetTotal"].ToString();
            this.txb_Available.Text = budgetSummary.Rows[0]["BudgetAvailable"].ToString();
            this.txb_Captured.Text = budgetSummary.Rows[0]["BudgetCaptured"].ToString();
            this.txb_Authorized.Text = budgetSummary.Rows[0]["BudgetAuthorized"].ToString();
        }
    }
}
