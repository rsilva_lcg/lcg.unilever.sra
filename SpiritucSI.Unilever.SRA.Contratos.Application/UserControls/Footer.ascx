<%@ Control Language="c#" AutoEventWireup="True" CodeFile="Footer.ascx.cs" Inherits="Footer" %>
<%@ Register TagPrefix="UC1" TagName="ImageTextButton" Src="~/UserControls/ImageTextButton.ascx" %>
<table class="table" id="Table1">
    <tr>
        <td colspan="3" align="center" width="100%">
            <br />
            <hr />
        </td>
    </tr>
    <tr>
        <td align="left" valign="middle" width="33%">
            <UC1:ImageTextButton ID="C_Btn_Logoff" runat="server" TextCssClass="button" CausesValidation="false" TextPosition="Rigth" meta:resourcekey="C_Btn_Logoff" OnClick="C_Btn_Logoff_OnClick" />            
        </td>
        <td align="center" width="33%">
        </td>
        <td align="right" valign="middle"  width="33%">
            <asp:Label ID="C_lbl_CopyRigth" runat="server" 
                meta:resourcekey="C_lbl_CopyRigth" ForeColor="#333399"></asp:Label>
            <!--<a class="link" href="http://www.spirituc.com">Spirituc </A>-->
        </td>
    </tr>
</table>

<script type="text/javascript" language="javascript">
    history.forward();
</script>

