﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

public partial class UserControls_ucDashboardManager : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //user is supervisor
            if (true)
            {
                UserControl ucc = (UserControl)LoadControl("~/ucDashboard.ascx");
                this.DashCnt.Controls.Add(ucc);
            }

            this.DashCnt.DataBind();
            
            //user is manager ...
            //if(true)
            //{
            //    UserControl ucc = (UserControl) LoadControl("~/ucDashboard.ascx");
            //    this.Controls.Add(ucc);

            //}
        }
        catch (Exception ex)
        {
            if (ExceptionPolicy.HandleException(ex, "UIPolicy")) throw;
        }

    }

    //A passar para um supercontrol

}
