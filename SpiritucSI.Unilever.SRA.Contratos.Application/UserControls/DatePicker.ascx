﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatePicker.ascx.cs" Inherits="SpiritucSI.UserControls.GenericControls.DatePicker" %>
<div style="text-align: left;">
    <!-- CALENDÁRIO -->
    <%--    <asp:UpdatePanel ID="ttt" runat="server" RenderMode="Inline">
        <ContentTemplate>
--%>
    <div style="position: absolute; z-index: 500;" id="Calendar_box" runat="server">
        <%-- SELECCIONAR MES/ANO --%>
        <div style="position: absolute; background-color: #FFE88C; border: solid 1px #36406b;" id="Year_box"
            visible="false" runat="server">
            <table width="160px" style="color: #00008B; background-color: #FFE88C;" cellpadding="0" cellspacing="0">
                <%-- CABEÇALHO --%>
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="C_btn_YearDown" runat="server" meta:resourcekey="C_btn_YearDown" OnClick="C_btn_YearDown_Click" />
                    </td>
                    <td style="background-color: White; font-size: 2px;">
                        &nbsp;
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="C_btn_Close" runat="server" meta:resourcekey="C_btn_Close" OnClick="C_btn_Close_Click" />
                    </td>
                </tr>
                <%-- MESES --%>
                <tr>
                    <td align="left">
                        <asp:RadioButtonList ID="C_rbl_Year" runat="server" Font-Size="XX-Small" RepeatColumns="1" CellPadding="0"
                            CellSpacing="0">
                        </asp:RadioButtonList>
                    </td>
                    <td style="font-size: 2px; background-color: White;">
                        &nbsp;
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="C_rbl_Month" runat="server" Font-Size="XX-Small" RepeatColumns="2" CellPadding="0"
                            CellSpacing="0">
                            <asp:ListItem Value="1" meta:resourcekey="C_rbl_Month_1"></asp:ListItem>
                            <asp:ListItem Value="2" meta:resourcekey="C_rbl_Month_2"></asp:ListItem>
                            <asp:ListItem Value="3" meta:resourcekey="C_rbl_Month_3"></asp:ListItem>
                            <asp:ListItem Value="4" meta:resourcekey="C_rbl_Month_4"></asp:ListItem>
                            <asp:ListItem Value="5" meta:resourcekey="C_rbl_Month_5"></asp:ListItem>
                            <asp:ListItem Value="6" meta:resourcekey="C_rbl_Month_6"></asp:ListItem>
                            <asp:ListItem Value="7" meta:resourcekey="C_rbl_Month_7"></asp:ListItem>
                            <asp:ListItem Value="8" meta:resourcekey="C_rbl_Month_8"></asp:ListItem>
                            <asp:ListItem Value="9" meta:resourcekey="C_rbl_Month_9"></asp:ListItem>
                            <asp:ListItem Value="10" meta:resourcekey="C_rbl_Month_10"></asp:ListItem>
                            <asp:ListItem Value="11" meta:resourcekey="C_rbl_Month_11"></asp:ListItem>
                            <asp:ListItem Value="12" meta:resourcekey="C_rbl_Month_12"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%-- BOTÕES --%>
                <tr>
                    <td align="center" valign="top">
                        <asp:ImageButton ID="C_btn_YearUp" runat="server" meta:resourcekey="C_btn_YearUp" OnClick="C_btn_YearUp_Click" />
                    </td>
                    <td style="font-size: 2px; background-color: White;">
                        &nbsp;
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="C_btn_Select" runat="server" meta:resourcekey="C_btn_Select" OnClick="C_btn_Select_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- CALENDARIO --%>
        <table width="160px" style="color: #00008B; background-color: #FFFFD5; border: solid 1px #36406b; padding: 2px 2px 2px 2px;"
            cellpadding="0" cellspacing="0">
            <%-- CABEÇALHO --%>
            <tr style="background-color: #FFE88C;">
                <td align="left">
                    <asp:ImageButton ID="C_btn_PrevMonth" runat="server" meta:resourcekey="C_btn_PrevMonth" OnClick="C_btn_PrevMonth_Click" />
                </td>
                <td align="center">
                    <asp:LinkButton ID="C_btn_Mes" runat="server" CssClass="button" meta:resourcekey="C_btn_Mes" OnClick="C_btn_Mes_Click"></asp:LinkButton>
                </td>
                <td align="right">
                    <asp:ImageButton ID="C_btn_NextMonth" runat="server" meta:resourcekey="C_btn_NextMonth" OnClick="C_btn_NextMonth_Click" />
                </td>
            </tr>
            <%-- CALENDÁRIO --%>
            <tr>
                <td colspan="3">
                    <asp:Calendar ID="C_ui_Calendar" runat="server" OnSelectionChanged="C_ui_Calendar_Select" OnVisibleMonthChanged="C_ui_Calendar_VisibleMonthChanged"
                        OnDayRender="C_ui_Calendar_DayRender" FirstDayOfWeek="Monday" CssClass="calendar" Width="150px" DayNameFormat="FirstTwoLetters"
                        ShowTitle="false" BackColor="Transparent" BorderStyle="None" SelectWeekText='<img src="../img/Tip_Small_Right.ico" style="border:none;" />'
                        SelectMonthText='<img src="../img/Tip_Small_Right.ico" style="border:none;" />'>
                        <DayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="black" />
                        <OtherMonthDayStyle BackColor="Transparent" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="#FFFFD5" />
                        <TodayDayStyle BackColor="White" BorderColor="Red" BorderStyle="Dotted" BorderWidth="2px" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" Font-Bold="true" ForeColor="Red" />
                        <DayHeaderStyle BackColor="Transparent" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <WeekendDayStyle BackColor="#FFF4BC" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="#555555" />
                        <SelectedDayStyle BackColor="#00008B" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            Font-Bold="true" ForeColor="White" />
                        <SelectorStyle BackColor="Transparent" />
                    </asp:Calendar>
                </td>
            </tr>
            <%-- HOJE --%>
            <tr>
                <td colspan="3" align="center">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <asp:LinkButton ID="C_btn_Clear" runat="server" CssClass="button" meta:resourcekey="C_btn_Clear" OnClick="C_btn_Clear_Click"></asp:LinkButton>
                            </td>
                            <td align="right">
                                <asp:LinkButton ID="C_btn_Today" runat="server" CssClass="button" meta:resourcekey="C_btn_Today" OnClick="C_btn_Today_Click"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    <!-- DATA SELECCIONADA -->
    <input type="hidden" id='<%= this.ID %>' value='<%= this.C_txt_Date.Text %>' />
    <!-- DATA APRESENTADA -->
    <table cellpadding="0" cellspacing="0" id="TextBox_box" runat="server">
        <tr>
            <td valign="middle">
                <asp:TextBox ReadOnly="true" ID="C_txt_Date" runat="server" CssClass="textbox" Style="cursor: pointer;"
                    Width="100px"></asp:TextBox>
            </td>
            <td valign="middle">
                &nbsp;<asp:ImageButton ID="C_btn_calendar" runat="server" ImageUrl="~/images/Calendar.ico" OnClick="C_btn_calendar_Click" />
            </td>
        </tr>
    </table>
</div>
