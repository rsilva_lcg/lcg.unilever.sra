
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Header : System.Web.UI.UserControl
{
    public string Title
    {
        get { return this.C_lbl_header.Text; }
        set { this.C_lbl_header.Text = value; }
    }

    protected void Menu1_MenuItemDataBound(object sender, MenuEventArgs e)
    {
        //SiteMapNode node = e.Item.DataItem as SiteMapNode;

        //// check for the visible attribute and if false
        //// remove the node from the parent
        //// this allows nodes to appear in the SiteMapPath but not show on the menu
        //if (!string.IsNullOrEmpty(node["visible"]))
        //{
        //    bool isVisible;
        //    if (bool.TryParse(node["visible"], out isVisible))
        //    {
        //        if (!isVisible)
        //        {
        //            e.Item.Parent.ChildItems.Remove(e.Item);
        //        }
        //    }
        //}
    }
}

    //<tr>
    //    <td colspan="2">
    //        <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" SiteMapProvider="XmlSitemapProvider" />
    //        <asp:Menu ID="Menu1" runat="server" DataSourceID="SiteMapDataSource1" BackColor="#F7F6F3" 
    //            DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="0.8em" 
    //            ForeColor="#7C6F57" Orientation="Horizontal" StaticSubMenuIndent="10px" 
    //            Width="100%">
    //            <StaticSelectedStyle BackColor="#5D7B9D" />
    //            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    //            <DynamicHoverStyle BackColor="#7C6F57" ForeColor="White" />
    //            <DynamicMenuStyle BackColor="#F7F6F3" />
    //            <DynamicSelectedStyle BackColor="#5D7B9D" />
    //            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    //            <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
    //        </asp:Menu>
    //    </td>
    //</tr>
