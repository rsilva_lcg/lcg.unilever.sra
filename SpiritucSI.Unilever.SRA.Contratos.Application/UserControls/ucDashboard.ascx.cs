﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web.Security;
using SpiritucSI.Unilever.SRA.Business.Budget;
using SRADBInterface;

public partial class UserControls_ucDashboard : System.Web.UI.UserControl
{

    SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
    MyMembership memb = new MyMembership(ConfigurationManager.AppSettings["Workflow"]);

    //TODOS: PASSAR OS ESTADOS PARA O ENUM; METODODOS PARA CLASSE USER
    //Add UserValidation -> supercontrol user
    protected bool HasPermissionOnState(string state)
    {
        bool valid = false;
        switch (state)
        {
            case("DRAFT"):
                {
                    valid = ((this.Page.User.IsInRole("SalesRep")
                            || this.Page.User.IsInRole("Supervisor")
                            || this.Page.User.IsInRole("RegManager")
                        )
                        );
                break;}
            case ("RENEWALDRAFT"): 
                {
                    valid = ((this.Page.User.IsInRole("SalesRep")
                            || this.Page.User.IsInRole("Supervisor")
                            || this.Page.User.IsInRole("RegManager")
                        )
                        );
                    break; }
            case ("PRINTED"): 
                {

                    valid = ((this.Page.User.IsInRole("SalesRep")
                            || this.Page.User.IsInRole("Supervisor")
                            || this.Page.User.IsInRole("RegManager")
                        )
                        );

                                
                break; }
            case ("DELEGATED"): 
        {

            valid = (
                    (this.Page.User.IsInRole("SalesRep")
                    || this.Page.User.IsInRole("Supervisor")
                    || this.Page.User.IsInRole("RegManager")
                )
                );

            break; }
            case ("RECEIVED"): 
                {
                    valid = (this.Page.User.IsInRole("SMI")                        
                    );

                    break; }
            case ("ERRORVERIFICATION"): 
                {                    
                    valid = ((this.Page.User.IsInRole("SalesRep")
                        || this.Page.User.IsInRole("Supervisor")
                        || this.Page.User.IsInRole("RegManager")
                        )
                    );
                
                    break; }
            case ("INAPPROVAL"): 
                {
                    valid = (this.Page.User.IsInRole("Supervisor")
                    );
                
                    break; }
            case ("MANAGERAPPROVAL"):
                {
                    valid = (this.Page.User.IsInRole("RegManager")
                    );

                    break;
                }
            case ("SALESAPPROVAL"): 
                {

                    valid = (this.Page.User.IsInRole("SalesDirector")
                    );

                    break; }
            case ("FINANTIALAPPROVAL"): 
                {
                    valid = (this.Page.User.IsInRole("FinantialDirector")                       
                    );

                    break; }
            case("TOPAPPROVAL"):
                {
                    valid = (this.Page.User.IsInRole("ManagingDirector")                        
                    );

                    break;}
            case("APPROVED"):
                {
                    valid = (this.Page.User.IsInRole("SalesDirector")
                        || this.Page.User.IsInRole("FinantialDirector")
                        || this.Page.User.IsInRole("ManagingDirector")
                        || this.Page.User.IsInRole("RegManager")
                        || this.Page.User.IsInRole("SMI")
                        || (this.Page.User.IsInRole("SalesRep")
                            || this.Page.User.IsInRole("Supervisor")
                            || this.Page.User.IsInRole("RegManager")
                        )
                        );

                    break;}
            case("REJECTED"):
                {
                    valid = (this.Page.User.IsInRole("SalesDirector")
                        || this.Page.User.IsInRole("FinantialDirector")
                        || this.Page.User.IsInRole("ManagingDirector")
                        || this.Page.User.IsInRole("RegManager")
                        || this.Page.User.IsInRole("SMI")
                        || (this.Page.User.IsInRole("SalesRep")
                            || this.Page.User.IsInRole("Supervisor")
                            || this.Page.User.IsInRole("RegManager")
                        )
                        );

                    break;}
            default: { valid = false; break; }
        }
        return valid;
        
    }

//    protected string GetUserZonesForQuery(bool IsSMI, bool IsSalesManager, bool IsFinancialManager, bool IsTopManager)
//    {
//        string uRolesQuery = " AND (";
//        try
//        {
//            MyMembership m = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
//            if (IsSMI || IsSalesManager || IsFinancialManager || IsTopManager)
//            {
//                //NÃO FAZ NADA, POIS ASSIM SELECCIONA TODAS AS ZONAS

//                //foreach (DataRow r in m.GetOrganizationalRoles(Roles.GetAllRoles()).Rows)
//                //{
//                //    uRolesQuery += " AND DEFAULT_COD_CONC = " + r[0].ToString();
//                //}
//            }
//            else
//            {
//                foreach (DataRow r in m.GetOrganizationalRoles(Roles.GetAllRoles()).Rows)
//                {
//                    if (this.Page.User.IsInRole(r[0].ToString()))
//                    {
//                        uRolesQuery += "DEFAULT_COD_CONC = " + r[0].ToString();
//                        uRolesQuery += " OR ";
//                    }
//                }
//            }

//            if (uRolesQuery == " AND (")
//            {
//                return "";
//            }
//            else
//            {
//                uRolesQuery = uRolesQuery.Substring(0, uRolesQuery.LastIndexOf(" OR "));
//                uRolesQuery += ")";
//                return uRolesQuery;
//            }
//        }
//        catch (Exception ex)
//        {
//            throw;
//        }
    
//}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //States
            string DraftState = "DRAFT";
            string RenewalDraftState = "RENEWALDRAFT";
            string PrintedState = "PRINTED";
            string DelegatedState = "DELEGATED";
            string ReceivedState = "RECEIVED";
            string ErrorVerificationState = "ERRORVERIFICATION";
            string InApprovalState = "INAPPROVAL";
            string ManagerApprovalState = "MANAGERAPPROVAL";
            string FinancialApprovalState = "FINANTIALAPPROVAL";
            string TopApprovalState = "TOPAPPROVAL";
            string ApprovedState = "APPROVED";
            string RejectedState = "REJECTED";
            string SalesApprovalState = "SALESAPPROVAL";

            string zonesQuery = memb.GetUserZonesForQuery(this.Page.User.IsInRole("SMI"), this.Page.User.IsInRole("SalesDirector"), this.Page.User.IsInRole("FinantialDirector"), this.Page.User.IsInRole("ManagingDirector"), this.Page.User);
            string query = "(INSTANCE_STATE) = '@@STATE@@' AND INSTANCE_ID IS NOT NULL" + zonesQuery;

            this.Drafted.Visible = this.HasPermissionOnState(DraftState);
            if (this.Drafted.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", DraftState)); 
                if (contracts.DefaultView.Count > 0)
                    this.gridDrafted.DataSource = contracts.DefaultView;
                this.gridDrafted.DataBind();
            }

            this.RenewalDrafted.Visible = this.HasPermissionOnState(RenewalDraftState);
            if (this.RenewalDrafted.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", RenewalDraftState));
                if (contracts.DefaultView.Count > 0)
                    this.gridRenewalDrafted.DataSource = contracts.DefaultView;
                this.gridRenewalDrafted.DataBind();
            }


            this.Printed.Visible = this.HasPermissionOnState(PrintedState);
            if (this.Printed.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", PrintedState));
                if (contracts.DefaultView.Count > 0)
                    this.gridPrinted.DataSource = contracts.DefaultView;
                this.gridPrinted.DataBind();
            }


            this.Delegated.Visible = this.HasPermissionOnState(DelegatedState);
            if (this.Delegated.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", DelegatedState));
                if (contracts.DefaultView.Count > 0)
                    this.gridDelegated.DataSource = contracts.DefaultView;
                this.gridDelegated.DataBind();
            }


            this.Received.Visible = this.HasPermissionOnState(ReceivedState);
            if (this.Received.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", ReceivedState));
                if (contracts.DefaultView.Count > 0)
                    this.gridReceived.DataSource = contracts.DefaultView;
                this.gridReceived.DataBind();
            }

            this.ErrorVerfication.Visible = this.HasPermissionOnState(ErrorVerificationState);
            if (this.ErrorVerfication.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", ErrorVerificationState));
                if (contracts.DefaultView.Count > 0)
                    this.gridErrorVerification.DataSource = contracts.DefaultView;
                this.gridErrorVerification.DataBind();
            }
            
            this.InApproval.Visible = this.HasPermissionOnState(InApprovalState);
            if (this.InApproval.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", InApprovalState));
                if (contracts.DefaultView.Count > 0)
                    this.gridInApproval.DataSource = contracts.DefaultView;
                this.gridInApproval.DataBind();
            }
            
            this.ManagerApproval.Visible = this.HasPermissionOnState(ManagerApprovalState);
            if (this.ManagerApproval.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", ManagerApprovalState));
                if (contracts.DefaultView.Count > 0)
                    this.gridManagerApproval.DataSource = contracts.DefaultView;
                this.gridManagerApproval.DataBind();
            }

            this.FinancialApproval.Visible = this.HasPermissionOnState(FinancialApprovalState);
            if (this.FinancialApproval.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", FinancialApprovalState));
                if (contracts.DefaultView.Count > 0)
                    this.gridFinancialApproval.DataSource = contracts.DefaultView;
                this.gridFinancialApproval.DataBind();
            }

            this.TopApproval.Visible = this.HasPermissionOnState(TopApprovalState);
            if (this.TopApproval.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", TopApprovalState));
                if (contracts.DefaultView.Count > 0)
                    this.gridTopApproval.DataSource = contracts.DefaultView;
                this.gridTopApproval.DataBind();
            }

            this.Approved.Visible = this.HasPermissionOnState(ApprovedState);
            if (this.Approved.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", ApprovedState));
                if (contracts.DefaultView.Count > 0)
                    this.gridApproved.DataSource = contracts.DefaultView;
                this.gridApproved.DataBind();
            }

            this.Rejected.Visible = this.HasPermissionOnState(RejectedState);
            if (this.Rejected.Visible)
            {
                var contracts = DBsra.getContrato(query.Replace("@@STATE@@", RejectedState));
                if (contracts.DefaultView.Count > 0)
                    this.gridRejected.DataSource = contracts.DefaultView;
                this.gridRejected.DataBind();
            }

            this.SalesApproval.Visible = this.HasPermissionOnState(SalesApprovalState);
            if (this.SalesApproval.Visible)
            {
                    var contracts = DBsra.getContrato(query.Replace("@@STATE@@", SalesApprovalState));
                if (contracts.DefaultView.Count > 0)
                    this.gridSalesApproval.DataSource = contracts.DefaultView;
                this.gridSalesApproval.DataBind();
            }
        }
    }

    protected void C_btn_ChangeMode_OnClick(object sender, System.EventArgs e)
    {
    }

    protected void grid_Select(object sender, Obout.Grid.GridRecordEventArgs e)
    {
        Obout.Grid.Grid grid1 = (Obout.Grid.Grid)sender;
        if(grid1.SelectedRecords != null)
            foreach (Hashtable oRecord in grid1.SelectedRecords)
            { string idcontract = oRecord["ID_CONTRATO"].ToString();
                this.Response.Redirect("/contrato.aspx?idContrato=" + idcontract);
            }
    }
}
