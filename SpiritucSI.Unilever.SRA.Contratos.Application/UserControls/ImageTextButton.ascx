﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageTextButton.ascx.cs" Inherits="ImageTextButton" %>
    <table cellpadding="0" cellspacing="0" border="0">
        <tr style="padding: 0 0 0 0; margin: 0 0 0 0;">
            <td id="C_C_TOP" colspan="3" runat="server" align="center" style="padding: 0 0 0 0; margin: 0 0 0 0;">
                <asp:LinkButton ID="C_btn_Top" runat="server" Visible="false" BorderStyle="None" BackColor="Transparent"
                    OnClick="ButtonClick"></asp:LinkButton>
            </td>
        </tr>
        <tr style="padding: 0 0 0 0; margin: 0 0 0 0;">
            <td id="C_C_LEFT" runat="server" valign="middle" style="padding: 0 0 0 0; margin: 0 0 0 0;">
                <asp:LinkButton ID="C_btn_Left" runat="server" Visible="false" OnClick="ButtonClick"></asp:LinkButton>
            </td>
            <td id="C_C_CENTER" runat="server" valign="middle" align="center" style="padding: 0 0 0 0; margin: 0 0 0 0;">
                <asp:ImageButton ID="C_btn_Image" runat="server" Visible="false" OnClick="ButtonClick" />
            </td>
            <td id="C_C_RIGTH" runat="server" valign="middle" style="padding: 0 0 0 0; margin: 0 0 0 0;">
                <asp:LinkButton ID="C_btn_Rigth" runat="server" Visible="false" OnClick="ButtonClick"></asp:LinkButton>
            </td>
        </tr>
        <tr style="padding: 0 0 0 0; margin: 0 0 0 0;">
            <td id="C_C_BOTTOM" colspan="3" runat="server" align="center" style="padding: 0 0 0 0; margin: 0 0 0 0;">
                <asp:LinkButton ID="C_btn_Bottom" runat="server" Visible="false" BorderStyle="None" BackColor="Transparent"
                    OnClick="ButtonClick"></asp:LinkButton>
            </td>
        </tr>
    </table>
