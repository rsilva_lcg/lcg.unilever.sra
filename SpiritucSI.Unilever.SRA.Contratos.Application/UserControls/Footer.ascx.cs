
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.Security;

public partial class Footer : System.Web.UI.UserControl
{
    #region PAGE EVENTS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.User.Identity.Name.Equals(""))
            {
                this.C_Btn_Logoff.Visible = true;
                this.C_Btn_Logoff.Text = "SAIR&nbsp;DE&nbsp;(" + Page.User.Identity.Name + ")";                
            }
            else
            {
                this.C_Btn_Logoff.Visible = false;
            }
        }   
    #endregion

    #region BUTTON EVENTS
        protected void C_Btn_Logoff_OnClick(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/logon.aspx", true);
        }
    #endregion
}

