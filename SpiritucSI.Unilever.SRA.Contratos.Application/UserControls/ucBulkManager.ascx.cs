﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SpiritucSI.Unilever.SRA.UserControls;
using SpiritucSI.Unilever.SRA.Business.Budget;
using System.Configuration;
using SpiritucSI.Unilever.SRA.WF;

namespace SpiritucSI.Unilever.SRA.UserControls
{

    public partial class ucBulkManager : System.Web.UI.UserControl
    {
        #region PROPS
        SRADBInterface.SRA DBsra = new SRADBInterface.SRA(ConfigurationManager.AppSettings["DBCS"]);
        MyMembership memb = new MyMembership(ConfigurationManager.AppSettings["Workflow"]);

        protected string state
        {
            get 
            {
                string st = this.Request.QueryString["State"]!= null?this.Request.QueryString["State"].ToString():"";
                if (st != "" && (
                    st == "DRAFT" 
                    || st =="RENEWALDRAFT"
                    || st == "PRINTED"
                    || st == "DELEGATED"
                    || st == "RECEIVED"
                    || st ==  "ERRORVERIFICATION"
                    || st ==  "INAPPROVAL"
                    || st == "SALESAPPROVAL"
                    || st == "MANAGERAPPROVAL"
                    || st == "FINANTIALAPPROVAL"
                    || st == "TOPAPPROVAL"
                    || st == "APPROVED"
                    || st == "REJECTED"
                    ))
                    return this.Request.QueryString["State"].ToString();
                else
                    return this.RedirectDashboard();
            }
        }

        protected string stateName { get; set; }
        
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack && Session["WorkflowActionsGridList"] != null)
                Session["WorkflowActionsGridList"] = null;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if(!this.IsPostBack)
                this.GridBind();
                this.Lbl_State.DataBind();    
                //this.LB_select.Attributes.Add("OnClientClick", "return SelectAll(true," + this.gridBulk.ClientID.ToString() + ")");
                //this.LB_deselect.Attributes.Add("OnClientClick", "return SelectAll(false," + this.gridBulk.ClientID.ToString() + ")");

        }

        protected string RedirectDashboard()
        {
            this.Page.Response.Redirect("./SRA_Dashboard.aspx"); return null;
        }

        #region CONTROL_EVENTS
        protected void gridBulk_Rowdatabound(object sender, Obout.Grid.GridRowEventArgs e)
        {
            string wfId = ((DataRowView)e.Row.DataItem)["INSTANCE_ID"].ToString();

            try
            {
                ucActionsForBulk wfa = (ucActionsForBulk)e.Row.Cells[0].FindControl("WFBulk");
                if (wfa != null)
                    wfa.uid = wfId;
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        protected void bntSave_click(object sender, EventArgs e)
        {
            int count = 0;
            int sucess = 0;

            string wfErrors = "";

            foreach (Obout.Grid.GridRow row in gridBulk.Rows)
            {
                string IdProc="";
                ucActionsForBulk wfa = (ucActionsForBulk)row.Cells[0].FindControl("WFBulk");
                TableCell LinkCell = row.Cells[1];
                if (wfa != null)
                {
                    //Erro carregamento controlo Obout; E necessário fazer uma 1a chamada para carregar Controlos
                    try
                    { object x = LinkCell.FindControl("lnk1"); }
                    catch (Exception ex1)
                    { //DO NOTHING
                    }
                    if (LinkCell != null && LinkCell.FindControl("lnk1") != null && LinkCell.FindControl("lnk1") is LinkButton)
                        IdProc = ((LinkButton)LinkCell.FindControl("lnk1")).CommandArgument;

                    count++;
                    try
                    {
                        wfa.ExecuteAction();
                        sucess++;
                    }


                    catch (WFProceedConditions ex2)
                    {
                        if (ex2.Message.Contains("EXCOD_01"))
                            wfErrors += "";
                        else
                        {
                            if (ex2.Message.Contains("EXCOD_02"))
                            {
                                //IdProc = ((DataRowView)row.DataItem)["ID_PROCESSO"] != null ? ((DataRowView)row.DataItem)["ID_PROCESSO"].ToString() : "";
                                wfErrors += "Erro no processo " + IdProc + ": O processo não pode ser rejeitado por já estar autorizado;";
                            }
                            else
                                throw;
                        }
                    }

                    catch (Exception ex)
                    {
                        // Change to Exception Policy/Response Panel message
                        //throw new Exception("WF Error");
                        this.Lbl_Error.Visible = true;
                    }
                }
            }

            if (count == sucess || wfErrors == "")
                this.Lbl_Success.Visible = true;
            else
            {
                this.Lbl_Error.Visible = true;
                if (wfErrors != "")
                    this.Lbl_Error.Text += " " + wfErrors;
            }
            this.GridBind();
            //this.RedirectDashboard();
            //this.Page_Load(null, null);

        }

        #endregion

        #region METHODS

        private void GridBind()
        {
            //States

            string zonesQuery = memb.GetUserZonesForQuery(this.Page.User.IsInRole("SMI"), this.Page.User.IsInRole("SalesDirector"), this.Page.User.IsInRole("FinantialDirector"), this.Page.User.IsInRole("ManagingDirector"), this.Page.User);
            string query = "(INSTANCE_STATE) = '@@STATE@@' AND INSTANCE_ID IS NOT NULL AND (IS_AUTHORIZED) = 'T'" + zonesQuery;

            this.gridBulk.DataSource = DBsra.getContrato(query.Replace("@@STATE@@", this.state));
            this.gridBulk.DataBind();
            //this.SelectAllBind();
        }


        protected void wf_Loaded(object sender, EventArgs e)
        {
            if(!this.IsPostBack)
                this.SelectAllBind();
        }



        public void SelectAllBind()
        {
            try
            {
                if (gridBulk.Rows.Count > 0 && rpChk.DataSource == null)
                {
                    ucActionsForBulk wfa = (ucActionsForBulk)gridBulk.Rows[0].Cells[0].FindControl("WFBulk");
                    if (wfa._wfActions != null && wfa._wfActions.Count > 0)
                    {
                        List<WFAction> li = new List<WFAction>();
                        Guid instanceId = wfa._wfActions[0].Args.InstanceId;
                        li.AddRange(wfa._wfActions.Where(x=>x.Args.InstanceId == instanceId).ToList<WFAction>());
                        this.rpChk.DataSource = li;
                        this.rpChk.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Lbl_Error.Visible = true;
                //this.Lbl_Error.Text = ex.Message;
            }
            
        }

        protected void RChk_Databound(object sender, EventArgs e)
        {
            ((CheckBox)(sender)).Attributes.Add("onClick", "return BulkSelectAll('" + "ucBulk_BulkManager" + "',$(this).attr('checked'),'" + ((CheckBox)sender).Text + "')");
        }


        #endregion

    }                                                                                

}
