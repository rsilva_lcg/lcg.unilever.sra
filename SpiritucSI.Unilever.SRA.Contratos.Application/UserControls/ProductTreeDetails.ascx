﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductTreeDetails.ascx.cs" Inherits="ProductTreeDetails" %>
<%@ Register TagPrefix="UC" TagName="DatePicker" Src="~/UserControls/DatePicker.ascx" %>
<%@ Register TagPrefix="UC" TagName="ImageTextButton" Src="~/UserControls/ImageTextButton.ascx" %>
<table>
    <tr>
        <td class="tituloListagem">
            BUDGET
        </td>
        <td class="tituloListagem">
            PERÍODO&nbsp;ACTIVIDADE
        </td>
        <td class="tituloListagem">
            PERÍODO&nbsp;COMPRAS
        </td>
        <td class="tituloListagem">
            PVP
        </td>
        <td class="tituloListagem" id="C_box_UnidadeHeader" runat="server">
            UN.
        </td>
        <td class="tituloListagem">
            PREVISÃO<br />
            (UN)
        </td>
        <td>
            <UC:ImageTextButton ID="C_btn_Apply" runat="server" TextCssClass="button" ToolTip="Aplicar os valores aos produtos das sub árvores"
                OnClick="C_btn_Apply_Click" ImageUrl="~/images/Refresh.ico" Text="APLICAR" TextPosition="Rigth" ValidationGroup="TPR_Insert" />
        </td>
    </tr>
    <tr class="ReadOnly">
        <td>
            <table>
                <tr>
                    <td align="right" valign="top">
                        VENDAS:
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="C_txt_BudgetVendas" runat="server" CssClass="TextBox" Style="text-align: right;" MaxLength="5"
                            Width="40px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        MARKETING:
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="C_txt_BudgetMarketing" runat="server" CssClass="TextBox" Style="text-align: right;"
                            MaxLength="5" Width="40px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="C_val_BudgetVendas_Empty" runat="server" meta:resourcekey="C_val_BudgetVendas_Empty"
                ControlToValidate="C_txt_BudgetVendas" ValidateEmptyText="true" ValidationGroup="TPR_Insert" Display="Dynamic"
                CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
            <asp:CustomValidator ID="C_val_BudgetVendas" runat="server" meta:resourcekey="C_val_BudgetVendas" ControlToValidate="C_txt_BudgetVendas"
                ValidationGroup="TPR_Insert" Display="Dynamic" CssClass="textValidator" EnableClientScript="false"
                ValidateEmptyText="false" OnServerValidate="C_val_txt_Percent_validate"></asp:CustomValidator>
            <asp:CustomValidator ID="C_val_BudgetMarketing_Empty" runat="server" meta:resourcekey="C_val_BudgetMarketing_Empty"
                ControlToValidate="C_txt_BudgetMarketing" ValidateEmptyText="true" ValidationGroup="TPR_Insert" Display="Dynamic"
                CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
            <asp:CustomValidator ID="C_val_BudgetMarketing" runat="server" meta:resourcekey="C_val_BudgetMarketing"
                ControlToValidate="C_txt_BudgetMarketing" ValidationGroup="TPR_Insert" Display="Dynamic" CssClass="textValidator"
                EnableClientScript="false" ValidateEmptyText="false" OnServerValidate="C_val_txt_Percent_validate"></asp:CustomValidator>
        </td>
        <td>
            <table>
                <tr>
                    <td align="right" valign="top">
                        INÍCIO:
                    </td>
                    <td valign="top">
                        <UC:DatePicker ID="C_dp_ActividadeInicio" runat="server" OnSelectionChanged="C_dp_ActividadeInicio_Changed" />
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        FIM:
                    </td>
                    <td valign="top">
                        <UC:DatePicker ID="C_dp_ActividadeFim" runat="server" OnSelectionChanged="C_dp_ActividadeFim_Changed" />
                    </td>
                </tr>
            </table>
             <asp:CustomValidator ID="C_val_ActividadeDatas" runat="server" meta:resourcekey="C_val_Datas"
                ValidationGroup="TPR_Insert" Display="Dynamic" CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_ActividadeDatas_Validate"></asp:CustomValidator>
        </td>
        <td>
            <table>
                <tr>
                    <td align="right" valign="top">
                        INÍCIO:
                    </td>
                    <td valign="top">
                        <UC:DatePicker ID="C_dp_ComprasInicio" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        FIM:
                    </td>
                    <td valign="top">
                        <UC:DatePicker ID="C_dp_ComprasFim" runat="server" />
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="C_val_ComprasDatas" runat="server" meta:resourcekey="C_val_Datas"
                ValidationGroup="TPR_Insert" Display="Dynamic" CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_ComprasDatas_Validate"></asp:CustomValidator>
        </td>
        <td align="right" valign="top">
            <asp:TextBox ID="C_txt_PVP" runat="server" CssClass="TextBox" Style="text-align: right;" MaxLength="10"
                Width="90px"></asp:TextBox><br />
            <asp:CheckBox ID="C_cb_PVPCopy" runat="server" TextAlign="Left" Text="Dividir" ToolTip="Marcar para dividir o PVP pelos produtos" />
            <asp:CustomValidator ID="C_val_PVP_Empty" runat="server" meta:resourcekey="C_val_PVP_Empty"
                ControlToValidate="C_txt_PVP" ValidateEmptyText="true" ValidationGroup="TPR_Insert" Display="Dynamic"
                CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
            <asp:CustomValidator ID="C_val_PVP" runat="server" meta:resourcekey="C_val_PVP" ControlToValidate="C_txt_PVP"
                ValidationGroup="TPR_Insert" Display="Dynamic" CssClass="textValidator" EnableClientScript="false"
                ValidateEmptyText="false" OnServerValidate="C_val_txt_Value_validate"></asp:CustomValidator>
        </td>
        <td align="right" id="C_box_UnidadeValor" runat="server" valign="top">
            &nbsp;<asp:Label ID="C_txt_Unidade" runat="server" CssClass="titulo_peq"></asp:Label>&nbsp;
        </td>
        <td align="right" valign="top">
            <asp:TextBox ID="C_txt_Qtd" runat="server" CssClass="TextBox" Style="text-align: right;" MaxLength="10"
                Width="90px"></asp:TextBox><br />
            <asp:CheckBox ID="C_cb_QtdCopy" runat="server" TextAlign="Left" AutoPostBack="false" Text="Dividir" ToolTip="Marcar para dividir a quantidade pelos produtos" />
            <asp:CustomValidator ID="C_val_Qtd_Empty" runat="server" meta:resourcekey="C_val_Qtd_Empty"
                ControlToValidate="C_txt_Qtd" ValidateEmptyText="true" ValidationGroup="TPR_Insert" Display="Dynamic"
                CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
            <asp:CustomValidator ID="C_val_Qtd" runat="server" meta:resourcekey="C_val_Qtd" ControlToValidate="C_txt_Qtd"
                ValidationGroup="TPR_Insert" Display="Dynamic" CssClass="textValidator" EnableClientScript="false"
                ValidateEmptyText="false" OnServerValidate="C_val_txt_Value_validate"></asp:CustomValidator>
        </td>
        <td align="center" style="background-color: Transparent;vertical-align:bottom;" valign="bottom">
            <UC:ImageTextButton ID="C_btn_Insert" runat="server" TextCssClass="button" ValidationGroup="TPR_Insert"
                OnClick="C_btn_Insert_Click" ImageUrl="~/images/Save1.ico" Text="INSERIR PRODUTOS" TextPosition="Rigth" />
        </td>
    </tr>
</table>
