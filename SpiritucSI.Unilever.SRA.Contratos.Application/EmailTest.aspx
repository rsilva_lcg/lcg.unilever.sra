﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailTest.aspx.cs" Inherits="EmailTest" Theme = "SRA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="Form2" method="post" runat="server" onsubmit="javascript: return MySubmit();">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnablePartialRendering="true"
        AsyncPostBackTimeout="60000">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px; position: absolute;
                left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF; z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="boxTitulo" runat="server" CssClass="box" meta:resourcekey="boxTituloResource1">
                <UC:Header ID="Header1" runat="server"></UC:Header>
            </asp:Panel>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <asp:Panel ID="boxError" runat="server" CssClass="box" meta:resourcekey="boxErrorResource1">
                <table class="table" align="center">
                    <tr>
                        <td valign="middle" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="errorLabel" meta:resourcekey="lblErrorResource1"></asp:Label><br>
                            <asp:Label ID="lblWarning" runat="server" CssClass="warningLabel" meta:resourcekey="lblWarningResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table width="60%">
                <tr>
                    <td align="center">
                        <br />
                        <br />
                        <div id="List" runat="server" class="SubContentCenterTable">
                            <asp:UpdatePanel ID="up2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: left; width: 100%">
                                        <div style="">
                                            <label>Send Test email to concessionnaire</label>
                                            <asp:TextBox ID="Txt_ID_CONTRACTO" runat ="server" ></asp:TextBox>
                                            <asp:Button ID ="Btt_Reset_One" runat ="server" Text="Enviar Email Concessionario" OnClick="Btt_One_click"  /><br /><br />
                                            <asp:Label id ="lbl" runat="server" Text="Sent" visible="false" />
                                            <br /><br /><asp:Button ID ="BttSendMail" runat ="server" Text="Testar Email" OnClick="Btt_click_send"  /><br /><br />
                                        </div>

                                   </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                </tr>
            </table>
            <%-- FOOTER  ################################################################################### --%>
            <asp:Panel ID="boxFooter" runat="server" CssClass="box" meta:resourcekey="boxFooterResource1">
                <UC:Footer ID="Footer1" runat="server"></UC:Footer>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>



