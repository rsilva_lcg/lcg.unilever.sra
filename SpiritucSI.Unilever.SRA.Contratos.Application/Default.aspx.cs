using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.Security;
using System.Net;
using System.IO;
using System.Text;

public partial class Default : System.Web.UI.Page
{
    #region METHODS
        /// <summary>
        /// APRESENTA UM ERRO/WARNING COM A MENSSAGEM INDICADA
        /// </summary>
        /// <param name="Destiny">LABEL DE DESTINO DA MENSSAGEM</param>
        /// <param name="Message">MENSSAGEM A APRESENTAR</param>
        /// <param name="e">EXCEP��O QUE DEU ORIGEM AO ERRO</param>
        private void _M_ShowError(WebControl Destiny, string Message, Exception e)
        {
            this.MaintainScrollPositionOnPostBack = false;
            ((Label)Destiny).Text = Message;
            Exception ex = e;
            while (ex != null)
            {
                Destiny.ToolTip += ex.Message + "\n";
                ex = ex.InnerException;
            }
        }
        private void LoadState()
        {
            this.lblError.Text = "";
            this.lblWarning.Text = "";
            this.lblError.ToolTip = "";
            this.lblWarning.ToolTip = "";
        }
        private void SaveState()
        {
        }
    #endregion

    #region PAGE EVENTS        
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    throw new Exception();
            //}
            //catch (Exception ex)
            //{
            //    if (ExceptionPolicy.HandleException(ex, "GlobalPolicy"))
            //    {
            //    }
            //}

            //Utilizador user;
            //if (!User.Identity.Name.Equals(""))
            //{
            //    if (!User.Identity.IsValidated)
            //    {
            //        Response.Redirect("~/logon.aspx", true);
            //    }
            //}
            //else
            //{
            //    Response.Redirect("~/logon.aspx", true);
            //}

            ///TODO: APAGAR - Usado apenas para debug #################################
            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "rd", "window.location.href='contrato.aspx';", true);
            /// ###############################################

            this.MaintainScrollPositionOnPostBack = true;
            this.LoadState();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //C_btn_Admin.Visible = User.IsInRole("SystemAdministrator");
            //ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), "sub", "MySubmit();");
            this.SaveState();
        }
  
    #endregion
    
    #region CONTROL EVENTS
       
        protected void lnkInserir_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "redirecionamento", "window.location.href='contrato.aspx';", true);
        }
    #endregion
}

