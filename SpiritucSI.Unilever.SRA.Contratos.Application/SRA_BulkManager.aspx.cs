﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace SpiritucSI.Unilever.SRA
{

    public partial class SRA_BulkManager : SuperPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            this.Page.ClientScript.RegisterClientScriptInclude("JQuery1.3", this.BaseUrl + "/JScript/jquery-1.4.min.js");
            this.Page.ClientScript.RegisterClientScriptInclude("MyJQuery", this.BaseUrl + "/JScript/myJQuery.js");
            this.Page.ClientScript.RegisterClientScriptInclude("Utils", this.BaseUrl + "/JScript/Utils.js");

        }
    }
}