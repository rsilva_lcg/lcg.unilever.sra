﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SpiritucSI.Unilever.SRA.Business.Budget;

public partial class UserList : SuperPage
{
    #region PROPERTIES
    //public List<SpiritucSI.BPN.AII.Business.Entities.User> Users { get; set; }
    #endregion

    #region PAGE_EVENTS
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            this.lblError.Text = "";
            this.lblWarning.Text = "";
            this.lblError.ToolTip = "";
            this.lblWarning.ToolTip = "";
        }
        else
        {
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        btnCreate.DataBind();
    }
    #endregion

    #region CONTROL_EVENTS
    protected void btnClean_Click(object sender, EventArgs e)
    {
        this.txtUsername.Text = "";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        MyMembership ur = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        grid1.DataSource = ur.Users_GetByName(txtUsername.Text.Trim());
        grid1.DataBind();
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        MyMembership m = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        if ((txtUsername.Text.Trim().Length>0) && (m.Users_GetByName(txtUsername.Text.Trim()).Rows.Count > 0))
        {
            lblError.Text = "Já existe um utilizador no sistema com esse nome.";
        }
        else
        {
            Response.Redirect("~/UserDetail.aspx?uid=" + Guid.Empty.ToString() + "&uname=" + txtUsername.Text.Trim());
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        MyMembership ur = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        int rowsAffected = ur.Users_Delete(new Guid(((LinkButton)sender).CommandArgument));
        if (rowsAffected > 0)
        {
            grid1.DataSource = ur.Users_Get();
            grid1.DataBind();
        }
        else
        {
            lblError.Text = "Erro ao apagar utilizador.";
        }

        // ESTE MÉTODO DO MEMBERSHIP NÃO FUNCIONA
        //NOTA: É PRECISO MUDAR O COMMAND ARGUMENT DO LINKBUTTON PARA "UserName" (case sensitive)

        //bool deleteOK = Membership.DeleteUser(((LinkButton)sender).CommandArgument.ToString().Trim());
        //if (deleteOK)
        //{
        //    MyMembership ur = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        //    grid1.DataSource = ur.Users_Get();
        //    grid1.DataBind();
        //}
        //else
        //{
        //    lblError.Text = "Erro ao apagar utilizador.";
        //}       
    }

    public string BuildURL(object uid, object uname)
    {
        return "~/UserDetail.aspx?uid="+uid.ToString()+"&uname="+uname.ToString();
    }
    #endregion


}

