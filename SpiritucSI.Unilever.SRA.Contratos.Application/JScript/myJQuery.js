﻿$(document).ready(
    function() {
        $('div .CSSSubTitle').toggle(
            function() {
                $(this).parent('div').next('div').slideToggle('slow');
                $(this).children(':first').children('img').attr('src', $(this).children(':first').children('img').attr('src').replace('minimize.gif', 'maximize.gif'));
            }, function() {
                $(this).parent('div').next('div').slideToggle('slow');
                $(this).children(':first').children('img').attr('src', $(this).children(':first').children('img').attr('src').replace('maximize.gif', 'minimize.gif'));
            });
        });
    
$(document).click(function(event) {
    $('#ResponseMsgPanelWrapper').fadeOut(1000);
});

var zindex = 100;
$(document).ready(
        function() {
            $('div .Helper').toggle(
                function() {
                    $(this).siblings('div').first('div').toggle('blind', null, 500);
                    $(this).siblings('div').first('div').css('z-index', zindex++);
                },
                function() {
                    $(this).siblings('div').first('div').toggle('blind', null, 500);
                    document.getElementById($(this).siblings('div').first('div').attr('id')).style.zIndex = zindex++;
                });
        });
