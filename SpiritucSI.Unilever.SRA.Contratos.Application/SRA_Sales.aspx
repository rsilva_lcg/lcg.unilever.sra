﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SRA_Sales.aspx.cs" Inherits="SRA_Sales"
    Theme="SRA" %>

<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        
    </title>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnablePartialRendering="true"
        AsyncPostBackTimeout="60000">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px; position: absolute;
                left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF; z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <asp:Panel ID="boxError" runat="server" CssClass="box" meta:resourcekey="boxErrorResource1">
                <table class="table" align="center">
                    <tr>
                        <td align="center">
                            <asp:Label>RESUMO VENDAS ANOS ANTERIORES</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="errorLabel" meta:resourcekey="lblErrorResource1"></asp:Label><br>
                            <asp:Label ID="lblWarning" runat="server" CssClass="warningLabel" meta:resourcekey="lblWarningResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table width="60%">
                <tr>
                    <td align="center">
                        <br />
                        <br />
                        <div id="List" runat="server" class="SubContentCenterTable">
                            <asp:UpdatePanel ID="up2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: left; width: 100%">
                                        <asp:DataGrid ID="C_dg_Entidades" runat="server" AutoGenerateColumns="False" ShowFooter="true"                                            BorderStyle="None" GridLines="None" CellSpacing="2" OnItemCommand="C_dg_Entidades_ItemCommand" OnPageIndexChanged="C_dg_Entidades_PageIndexChanged"
                                            OnItemDataBound="Entidade_ItemDataBound" PageSize="15" Width="100%">
                                            <HeaderStyle ForeColor="#00008B" BackColor="#99CCFF" Height="20px" />
                                            <ItemStyle CssClass="ListRow" />
                                            <AlternatingItemStyle CssClass="AlternateListRow" />
                                            <PagerStyle ForeColor="#00008B" BackColor="Transparent" HorizontalAlign="Left" Mode="NumericPages" />
                                            <Columns>                                           
                                                <asp:TemplateColumn HeaderText="ANO" SortExpression="COD_ANO">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "COD_ANO")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="CÓDIGO LE" SortExpression="COD_LOCAL_ENTREGA">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "COD_LOCAL_ENTREGA")%>
                                                    </ItemTemplate>                                                                                                       
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="GRUPO PRODUTO" SortExpression="DES_GRUPO_PRODUTO">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "DES_GRUPO_PRODUTO")%>
                                                    </ItemTemplate>                                                                                                      
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="GSV" SortExpression="GSV">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "GSV")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                </tr>
            </table>           
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
