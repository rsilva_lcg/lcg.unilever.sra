﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Net.Mail;

public partial class EmailTest : SuperPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void Btt_One_click(object sender, EventArgs e)
    {
        SRADBInterface.SRA DBsra = new SRADBInterface.SRA(ConfigurationManager.AppSettings["DBCS"]);

        int idContrato = 0;
        Int32.TryParse(Txt_ID_CONTRACTO.Text, out idContrato);

        //int id = 0;
        //if (Txt_ID_CONTRACTO.Text != "" && Int32.TryParse(Txt_ID_CONTRACTO.Text, out id) && DBsra.getContrato(id).Tables[0].Rows.Count > 0)
        if (Txt_ID_CONTRACTO.Text != "")
        {
            try
            {
                List<string> conts = new List<string>();
                string txt = Txt_ID_CONTRACTO.Text;
                if (txt.Contains(';'))
                    conts = txt.Split(';').ToList();
                else
                    conts.Add(txt);
                int count = 0;
                //string[] sts ={"Draft", "RenewalDraft", "Printed", "Delegated", "Accepted", "Received", "InApproval", "ManagerApproval", "SalesApproval", "FinantialApproval", "TopApproval", "Approved", "Rejected"};
                foreach (string c in conts)
                {
                    int id = 0;
                    if (Int32.TryParse(c, out id) && DBsra.getContrato(id).Tables[0].Rows.Count > 0)
                    {
                        DBsra.SendMail(idContrato);
                    }
                    count++;
                }
                this.lbl.Visible = true;
                this.lbl.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

    public void Btt_click_send(object sender, EventArgs e)
    {
        this.SendMailTest("");
    }


    public bool SendMailTest(string mailAddress)
    {
        try
        {
            SmtpClient c;
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["SMTP_DEBUG"].ToString()))
            {
                c = new SmtpClient("localhost");
                c.UseDefaultCredentials = true;
                c.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                c.PickupDirectoryLocation = @"c:\temp\";
            }
            else
            {
                string mailServer = ConfigurationManager.AppSettings["SMTP_SERVER"].ToString();
                if (mailServer.Length == 0)
                    mailServer = "exchange-bh.eu.unilever.com";
                c = new SmtpClient(mailServer);
                c.Port = 25;
            }

            string emailAddresses;
            //Stream pdfFile;

            //SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
            //DataTable emails = DBsra.get_Contrato_Emails(idContrato);
            //if (emails.Rows.Count > 0)
            //{
                //foreach (DataRow emailList in emails.Rows)
                //{
                //    if (emailList["email"].ToString().Trim().Equals(""))
                //    {
                //        emailAddresses = ConfigurationManager.AppSettings["SMTP_DEFAULT_RECEIVER"];
                //    }
                //    else
                //    {
                //        emailAddresses = emailList["email"].ToString().Trim();
                //    }

                //    if (useSMIAddress)
                //    {
                //    }

            if (Txt_ID_CONTRACTO.Text.Length > 0)
                emailAddresses = Txt_ID_CONTRACTO.Text;
            else
                 emailAddresses =  ConfigurationManager.AppSettings["SMTP_SMI_ADDRESS"];

                    string corpo = "TESTE: "+ConfigurationManager.AppSettings["SMTP_MESSAGE_CC_BODY"].Replace(@"\n", @"<br />");
                    MailMessage mail = new MailMessage(
                        ConfigurationManager.AppSettings["SMTP_FROM"].ToString()
                        , emailAddresses
                        , ConfigurationManager.AppSettings["SMTP_MESSAGE_CC_SUBJECT"]
                        , corpo);
                    mail.IsBodyHtml = true;

                    //string filename = "ContratoNum_" + idContrato.ToString() + ".pdf";
                    //pdfFile = GetPDF(idContrato);
                    //mail.Attachments.Add(new Attachment(pdfFile, filename));
                    c.Send(mail);
            return true;
        }
        catch (Exception ex)
        {
            throw ex;
            //ErrorLog(ConfigurationManager.AppSettings["MailLog_File"], "SRA_WF:" + ex.Message);
            //return false;
        }
    }
}
