﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserDetail.aspx.cs" Inherits="UserDetail" Theme="SRA"  %>

<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnablePartialRendering="true"
        AsyncPostBackTimeout="60000">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px; position: absolute;
                left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF; z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="boxTitulo" runat="server" CssClass="box" meta:resourcekey="boxTituloResource1">
                <UC:Header ID="Header1" runat="server"></UC:Header>
            </asp:Panel>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <asp:Panel ID="boxError" runat="server" CssClass="box" meta:resourcekey="boxErrorResource1">
                <table class="table" align="center">
                    <tr>
                        <td valign="middle" align="left">
                            <asp:Label ID="lblError" runat="server" Visible="true" CssClass="errorLabel"></asp:Label><br>
                            <asp:Label ID="lblWarning" runat="server" Visible="true" CssClass="warningLabel"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="UserDetailDiv" runat="server" class="SubContentCenter">
                <fieldset>
                    <table>
                        <tr>
                            <td style="width:20%; vertical-align:top;">
                                <div class="Row">
                                    <div class="form-text">
                                        <asp:Label ID="lblUsername" runat="server" Text="Username"></asp:Label></div>
                                    <div class="form-texbox">
                                        <asp:TextBox runat="server" CssClass="TextBoxSmaller" ID="txtUsername"></asp:TextBox></div>
                                    <div class="form-text-Obrigatorio">
                                        <asp:RequiredFieldValidator ID="reqUsername" runat="server" CssClass="lblError" ControlToValidate="txtUsername"
                                            ValidationGroup="Save" Display="Dynamic" ErrorMessage="Campo obrigatório."></asp:RequiredFieldValidator></div>
                                </div>
                                <div class="Row">
                                    <div class="form-text">
                                        <asp:Label ID="lblName" runat="server" Text='Nome'></asp:Label></div>
                                    <div class="form-texbox">
                                        <asp:TextBox Enabled="false" runat="server" CssClass="TextBoxSmaller" ID="txtName"></asp:TextBox></div>
                                </div>
                                <div class="Row">
                                    <div class="form-text">
                                        <asp:Label ID="lblEmail" runat="server" Text='Email'></asp:Label></div>
                                    <div class="form-texbox">
                                        <asp:TextBox Enabled="false" runat="server" CssClass="TextBoxSmaller" ID="txtEmail"></asp:TextBox></div>
                                </div>
                                <div class="Row">
                                    <div class="form-text">
                                        <asp:Label ID="lblNotes" runat="server" Text='Comentários'></asp:Label></div>
                                    <div class="form-texbox">
                                        <asp:TextBox Enabled="false" ID="txtANotes" CssClass="TextBoxSmaller" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox></div>
                                </div>
                                <div class="Row">
                                    <div class="form-text">
                                        <asp:Label ID="lblApproved" runat="server" Text='Aprovado?'></asp:Label></div>
                                    <div class="form-texbox">
                                        <asp:CheckBox Enabled="false" ID="chkApproved" runat="server" Checked="true"></asp:CheckBox></div>
                                </div>
                                <div class="Row">
                                    <div class="form-text">
                                        <asp:Label ID="lblBlocked" runat="server" Text='Bloqueado?'></asp:Label></div>
                                    <div class="form-texbox">
                                        <asp:CheckBox Enabled="false" ID="chkBlocked" runat="server"></asp:CheckBox></div>
                                </div>
                                <div class="Row" style="padding-top: 10px;">
                                    <div class="form-text">
                                        <asp:Label ID="lblBlank" runat="server"></asp:Label></div>
                                    <div class="form-text">
                                        <div>
                                            <asp:Button ID="bntCancel" CssClass="button1" runat="server" Text='Cancelar' PostBackUrl="~/UserList.aspx" />
                                            <asp:Button ID="btnSave" CssClass="button1" runat="server" ValidationGroup="Save" Text='Gravar' OnClick="btnSave_Click" />
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td style="width:20%; vertical-align:top;">
                                <div class="Row">
                                    <div class="form-text">
                                        <asp:Label ID="lblFunctionRoles" runat="server" Text='Roles Funcionais'></asp:Label></div>
                                    <div class="form-texbox" id="FunctionRoles" style="width: 140px; text-align: left">
                                        <asp:CheckBoxList ID="ckblFunctionRoles" CssClass="mk_chklist" runat="server" DataTextField="Name" DataValueField="Name">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </td>
                            <td style="width:60%; vertical-align:top;">
                                <div class="Row">
                                    <div class="form-text">
                                        <asp:Label ID="lblOrganizationRoles" runat="server" Text='Roles de Zona'></asp:Label></div>
                                    <div class="form-texbox" id="OrganizationRoles" style="text-align: left">
                                        <asp:CheckBoxList CssClass="mk_chklist" ID="ckblOrganizationRoles" runat="server" DataTextField="Name"
                                            RepeatColumns="3" Width="100%" DataValueField="Name">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <%-- FOOTER  ################################################################################### --%>
            <asp:Panel ID="boxFooter" runat="server" CssClass="box" meta:resourcekey="boxFooterResource1">
                <UC:Footer ID="Footer1" runat="server"></UC:Footer>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
