﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;

public partial class PrintPDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            string type = Request.QueryString["mode"].ToString();
            string strRequest = string.Empty;

            if (!string.IsNullOrEmpty(type) && type.ToLower() == "full")
            {
                //strRequest = @"http://flhsiis01/ReportServer?/SRA/ReportSRA_full";
                strRequest = System.Configuration.ConfigurationManager.AppSettings["ReportPathFull"].ToString();
            }
            else
            {
                //strRequest = @"http://flhsiis01/ReportServer?/SRA/ReportSRA_header";
                strRequest = System.Configuration.ConfigurationManager.AppSettings["ReportPathHeader"].ToString();
            }
            strRequest += "&PARAM_ID_Contrato=" + Request.QueryString["PARAM_ID_Contrato"];
            strRequest += @"&rs:format=pdf&rs:Command=Render";

            WebRequest request = WebRequest.Create(strRequest);
            int totalSize = 0;
            string rUser = System.Configuration.ConfigurationManager.AppSettings["ReportUser"].ToString();
            string rPass = System.Configuration.ConfigurationManager.AppSettings["ReportPass"].ToString();
            string rDomain = System.Configuration.ConfigurationManager.AppSettings["ReportDomain"].ToString();
            string rTimeout = System.Configuration.ConfigurationManager.AppSettings["ReportTimeout"].ToString();
            int timeout;
            if (!int.TryParse(rTimeout, out timeout))
                timeout = 36000;
            
            request.Credentials = new NetworkCredential(rUser, rPass, rDomain);
            request.Timeout = timeout; // 6 minutes in milliseconds.
            request.Method = WebRequestMethods.Http.Get;
            request.ContentLength = 0;
            WebResponse response = request.GetResponse();
            Response.Clear();
            BinaryReader reader = new BinaryReader(response.GetResponseStream());
            Byte[] buffer = new byte[2048];
            int count = reader.Read(buffer, 0, 2048);
            while (count > 0)
            {
                totalSize += count;
                Response.OutputStream.Write(buffer, 0, count);
                count = reader.Read(buffer, 0, 2048);
            }
            Response.ContentType = "application/pdf";
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.CacheControl = "private";
            Response.Expires = 30;
            Response.AddHeader("Content-Disposition", "attachment; filename=Contrato.pdf");
            Response.AddHeader("Content-Length", totalSize.ToString());
            reader.Close();
            Response.Flush();
            Response.End();
        }
    }
}
