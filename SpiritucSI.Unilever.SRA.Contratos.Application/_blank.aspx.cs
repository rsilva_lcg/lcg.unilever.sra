﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;

public partial class _blank : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    //FUNCIONA BROWSER:
    //http://flhsiis01/ReportServer/Pages/ReportViewer.aspx?%2fSRA%2fReport1&rs:format=pdf&rs:Command=Render&PARAM_ID_Contrato=24922
    //http://flhsiis01/ReportServer?/SRA/ReportSRA&rs:format=pdf&rs:Command=Render&PARAM_ID_Contrato=24922
    //http://flhsiis01/Reports/Pages/Report.aspx?ItemPath=%2fSRA%2fReportSRA
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://flhsiis01/Reports/Pages/Report.aspx?ItemPath=%2fSRA%2fReportSRA");       
        ////cookies to fix multiple re-directions error 
        //CookieContainer cookieContainer = new CookieContainer();
        //request.Credentials = new NetworkCredential("consultor", "Unilever123!", "PTG01"); 
        //request.CookieContainer = cookieContainer;
        //request.Timeout = 360000; // 6 minutes in milliseconds.
        //request.Method = WebRequestMethods.Http.Post;
        //request.ContentLength = 0;
        //HttpWebResponse response = (HttpWebResponse) request.GetResponse(); 
        //Stream stream = response.GetResponseStream(); 
        //string filename = @"C:\temp\TestSRA.pdf"; 
        //byte[] buffer = new byte[2048]; 
        //FileStream outFile = new FileStream(filename, FileMode.Create); 
        //int bytesRead; 
        //while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) != 0) 
        //    outFile.Write(buffer, 0, bytesRead); 
        //outFile.Close(); 
        //response.Close();

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@"http://flhsiis01/ReportServer?/SRA/ReportSRA&rs:format=pdf&rs:Command=Render&PARAM_ID_Contrato=24922");
        //cookies to fix multiple re-directions error 
        CookieContainer cookieContainer = new CookieContainer();
        request.Credentials = new NetworkCredential("consultor", "Unilever123!", "PTG01");
        request.CookieContainer = cookieContainer;
        request.Timeout = 360000; // 6 minutes in milliseconds.
        request.Method = WebRequestMethods.Http.Get;
        request.ContentLength = 0;
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reportStream = new StreamReader(response.GetResponseStream(), new UnicodeEncoding());
        MemoryStream objMemStream = new MemoryStream(new UnicodeEncoding().GetBytes(reportStream.ReadToEnd()));

        Response.Clear();
        Response.AddHeader("Accept-Header", objMemStream.Length.ToString());
        Response.ContentType = "application/pdf";
        Response.OutputStream.Write(objMemStream.ToArray(), 0, Convert.ToInt32(objMemStream.Length));
        Response.Flush();
        reportStream.Close();

        //string filename = @"C:\temp\Test100.pdf";
        //FileStream outFile = new FileStream(filename, FileMode.Create);
        ////byte[] buffer = new byte[Response.OutputStream.Length];
        ////Response.OutputStream.Read(buffer, 0, buffer.Length);
        //outFile.Write(objMemStream.ToArray(), 0, objMemStream.ToArray().Length);
        //outFile.Close();

        //Stream stream = response.GetResponseStream();
        //byte[] buffer = new byte[2048];
        //
        //int bytesRead;
        //while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) != 0)
        //    outFile.Write(buffer, 0, bytesRead);
        //outFile.Close();
        //response.Close();


        //WebRequest request = WebRequest.Create(@"http://flhsiis01/Reports/Pages/Report.aspx?ItemPath=%2fSRA%2fReportSRA");
        //int totalSize = 0;
        //request.Credentials = new NetworkCredential("consultor", "Unilever123!", "PTG01");
        //request.Timeout = 360000; // 6 minutes in milliseconds.
        //request.Method = WebRequestMethods.Http.Post;
        //request.ContentLength = 0;
        //WebResponse response = request.GetResponse();
        //Response.Clear();
        //BinaryReader reader = new BinaryReader(response.GetResponseStream());
        //Byte[] buffer = new byte[2048];
        //int count = reader.Read(buffer, 0, 2048);
        //while (count > 0)
        //{
        //    totalSize += count;
        //    Response.OutputStream.Write(buffer, 0, count);
        //    count = reader.Read(buffer, 0, 2048);
        //}
        //Response.ContentType = "application/pdf";
        //Response.Cache.SetCacheability(HttpCacheability.Private);
        //Response.CacheControl = "private";
        //Response.Expires = 30;
        //Response.AddHeader("Content-Disposition", "attachment; filename=thisStoopidReport.pdf");
        //Response.AddHeader("Content-Length", totalSize.ToString());
        //reader.Close();
        //Response.Flush();
        //Response.End();
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        WebRequest request = WebRequest.Create(@"http://flhsiis01/ReportServer?/SRA/ReportSRA&rs:format=pdf&rs:Command=Render&PARAM_ID_Contrato=24922");
        int totalSize = 0;
        request.Credentials = new NetworkCredential("consultor", "Unilever123!", "PTG01");
        request.Timeout = 360000; // 6 minutes in milliseconds.
        request.Method = WebRequestMethods.Http.Get;
        request.ContentLength = 0;
        WebResponse response = request.GetResponse();
        Response.Clear();
        BinaryReader reader = new BinaryReader(response.GetResponseStream());
        Byte[] buffer = new byte[2048];
        int count = reader.Read(buffer, 0, 2048);
        while (count > 0)
        {
            totalSize += count;
            Response.OutputStream.Write(buffer, 0, count);
            count = reader.Read(buffer, 0, 2048);
        }
        Response.ContentType = "application/pdf";
        Response.Cache.SetCacheability(HttpCacheability.Private);
        Response.CacheControl = "private";
        Response.Expires = 30;
        Response.AddHeader("Content-Disposition", "attachment; filename=Contrato.pdf");
        Response.AddHeader("Content-Length", totalSize.ToString());
        reader.Close();
        Response.Flush();
        Response.End();

    }
}
