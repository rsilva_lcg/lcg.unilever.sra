<%@ Page Language="c#" CodeFile="Default.aspx.cs" AutoEventWireup="true" Inherits="Default"
    meta:resourcekey="Page" Theme="SRA" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="Form1" method="post" runat="server" onsubmit="javascript: return MySubmit();">

    <script language="javascript" type="text/javascript" src="Scripts/SRAScript.js"></script>

    <link href='~/App_Themes/SRA/Unilever.css")' type="text/css" rel="stylesheet" />
    <%-- JANELA DE PROCESSAMENTO  ########################################################################## --%>
    <AJAX:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true">
    </AJAX:ScriptManager>
    <AJAX:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px;
                position: absolute; left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF;
                z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </AJAX:UpdateProgress>
    <%-- CONTE�DO  ######################################################################################### --%>
    <AJAX:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="boxTitulo" runat="server" CssClass="box" meta:resourcekey="boxTituloResource1">
                <UC:Header ID="Header1" runat="server"></UC:Header>
            </asp:Panel>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <asp:Panel ID="boxError" runat="server" CssClass="box" meta:resourcekey="boxErrorResource1">
                <table class="table" align="center">
                    <tr>
                        <td valign="middle" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="errorLabel" meta:resourcekey="lblErrorResource1"></asp:Label><br>
                            <asp:Label ID="lblWarning" runat="server" CssClass="warningLabel" meta:resourcekey="lblWarningResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%-- BOT�ES  ################################################################################### --%>
            <asp:Panel ID="boxBotoes" runat="server" CssClass="box" meta:resourcekey="boxBotoesResource1">
                <br>
                <br>
                <br>
                <br>
                <br>
                <table width="100%" style="visibility:hidden">
                    <tr>
                        <td style="width: 50%;" align="center">
                            <UC:ImageTextButton ID="C_btn_SRA" runat="server" TextCssClass="button tituloMenu"
                                TextPosition="Bottom" meta:resourcekey="C_btn_SRA" CausesValidation="False" PostBackUrl="~/SRA.aspx" />
                        </td>
                        <td style="width: 50%;" align="center">
                            <UC:ImageTextButton ID="C_btn_AE" runat="server" TextCssClass="button tituloMenu"
                                TextPosition="Bottom" meta:resourcekey="C_btn_AE" CausesValidation="False" PostBackUrl="~/AE.aspx" />
                        </td>
                    </tr>
                </table>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </asp:Panel>
            <%-- FOOTER  ################################################################################### --%>
            <asp:Panel ID="boxFooter" runat="server" CssClass="box" meta:resourcekey="boxFooterResource1">
                <UC:Footer ID="Footer1" runat="server"></UC:Footer>
            </asp:Panel>
        </ContentTemplate>
    </AJAX:UpdatePanel>
    </form>
</body>
</html>
