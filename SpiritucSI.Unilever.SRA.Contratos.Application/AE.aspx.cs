using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class AE_Page : System.Web.UI.Page
{
    #region METHODS
        /// <summary>
        /// APRESENTA UM ERRO/WARNING COM A MENSSAGEM INDICADA
        /// </summary>
        /// <param name="Destiny">LABEL DE DESTINO DA MENSSAGEM</param>
        /// <param name="Message">MENSSAGEM A APRESENTAR</param>
        /// <param name="e">EXCEP��O QUE DEU ORIGEM AO ERRO</param>
        private void _M_ShowError(WebControl Destiny, string Message, Exception e)
        {
            this.MaintainScrollPositionOnPostBack = false;
            ((Label)Destiny).Text = Message;
            Exception ex = e;
            while (ex != null)
            {
                Destiny.ToolTip += ex.Message + "\n";
                ex = ex.InnerException;
            }
        }
        private void LoadState()
        {
            this.lblError.Text = "";
            this.lblWarning.Text = "";
            this.lblError.ToolTip = "";
            this.lblWarning.ToolTip = "";

            if (IsPostBack)
            {

            }
            else
            {
                this.boxPesquisa.Visible = true;
                this.lnkInserir.Visible = true;//(((Utilizador)this.Session["User"]).PermissionLevel() >= UserRoles.PowerUser);

                // OP��ES PARA A PESQUISA
                AE DBSra = new AE(ConfigurationManager.AppSettings["DBCS"]);
                string[] SearchColumns = ConfigurationManager.AppSettings["ContratoAESearchColumns"].Replace(" ", "").Split(new char[] { ',' });
                string[] ResultColumns = ConfigurationManager.AppSettings["ContratoAEResultColumns"].Replace(" ", "").Split(new char[] { ',' });
                this.ucPesquisa.SearchHeaders = SearchColumns;
                this.ucPesquisa.ResultHeaders = ResultColumns;
                this.ucPesquisa.InputDataObject = DBSra;
                this.ucPesquisa.Title = "Contratos";
                this.ucPesquisa.ShowSearchResult = true;
                this.ucPesquisa.Opened = true;
                this.ucPesquisa.ADVModeEnabled = true;
            }
        }
        private void SaveState()
        {
        }
    #endregion

    #region PAGE EVENTS
        protected void Page_Load(object sender, EventArgs e)
        {            
            Utilizador user;
            if (User.Identity.IsAuthenticated == false)
            {
                Response.Redirect("~/logon.aspx", true);
            }
            
            this.MaintainScrollPositionOnPostBack = true;
            this.LoadState();
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), "sub", "MySubmit();");
            this.SaveState();
        }
    #endregion
    
    #region CONTROL EVENTS
        protected void ucPesquisa_onSelectResult(Object sender, DataRow data)
        {
            string idContrato = data["ID_CONTRATO"].ToString();
            Response.Redirect("~/contratoAE.aspx?idContrato=" + idContrato, true);
        }
        protected void lnkInserir_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contratoAE.aspx", true);
        }
    #endregion
}

