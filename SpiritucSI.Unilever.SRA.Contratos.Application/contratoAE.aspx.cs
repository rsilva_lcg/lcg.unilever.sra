using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Configuration;
using System.Threading;
using System.Resources;
using System.Reflection;

using SpiritucSI.UserControls.GenericControls;


public partial class contratoAE : System.Web.UI.Page
{
    #region FIELDS
    /// <summary>
    /// 
    /// </summary>
    protected bool is_DBContrato = false;
    /// <summary>
    /// 
    /// </summary>
    protected bool is_Editing = false;
    /// <summary>
    /// 
    /// </summary>
    protected bool ClientesLEs_SortASC = false;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable T_AE_ENTIDADE;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable T_AE_ABASTECIMENTO;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable T_AE_ACTIVIDADE;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable T_AE_TPR;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable T_AE_DEBITO;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable T_AE_LOG;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable LK_TIPO_ACTIVIDADE;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable LK_CONCESSIONARIO;
    /// <summary>
    /// 
    /// </summary>
    protected DateTime ActividadeInicio = DateTime.MinValue;
    /// <summary>
    /// 
    /// </summary>
    protected DateTime ActividadeFim = DateTime.MinValue;
    /// <summary>
    /// 
    /// </summary>
    protected DateTime ComprasInicio = DateTime.MinValue;
    /// <summary>
    /// 
    /// </summary>
    protected DateTime ComprasFim = DateTime.MinValue;

    #endregion

    #region METHODS
    #region AUXILIAR
    /// <summary>APRESENTA UM ERRO/WARNING COM A MENSSAGEM INDICADA</summary>
    /// <param name="Destiny">LABEL DE DESTINO DA MENSSAGEM</param>
    /// <param name="Message">MENSSAGEM A APRESENTAR</param>
    /// <param name="e">EXCEP��O QUE DEU ORIGEM AO ERRO</param>
    private void _M_ShowError(WebControl Destiny, string Message, Exception e)
    {        
        ((Label)Destiny).Text = Message;
        Exception ex = e;
        while (ex != null)
        {
            Destiny.ToolTip += ex.Message + "\n";
            ex = ex.InnerException;
        }
        string script = "<script type='text/Jscript'> alert('" + Message + "'); </script>";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "start", script, false);
        //this.Form.DefaultFocus = this.Dummy.ID;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="ControlID"></param>
    /// <returns></returns>
    private Control FindControlIN(Control source, string ControlID)
    {
        if (source.ID == ControlID)
            return source;
        foreach (Control c in source.Controls)
        {
            Control d = this.FindControlIN(c, ControlID);
            if (d != null)
                return d;
        }
        return null;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="body"></param>
    /// <param name="ClassifContrato"></param>
    /// <returns></returns>
    private bool EnviarMail(string body, string ClassifContrato)
    {
        try
        {
            SmtpClient c = new SmtpClient(ConfigurationManager.AppSettings["SMTP_SERVER"].ToString());
            MailMessage mail = new MailMessage(
                ConfigurationManager.AppSettings["SMTP_FROM"].ToString()
                , ConfigurationManager.AppSettings["SMTP_TO_" + ClassifContrato].ToString()
                , ConfigurationManager.AppSettings["SMTP_SUBJECT"].ToString()
                , body);
            mail.IsBodyHtml = false;
            if (mail.To.Count == 0)
                return true;

            c.Send(mail);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="c"></param>
    /// <param name="readOnly"></param>
    private void SetReadOnly(Control c, bool readOnly)
    {
        Type t = c.GetType();
        PropertyInfo p1 = t.GetProperty("Enabled");
        PropertyInfo p2 = t.GetProperty("ReadOnly");
        PropertyInfo p3 = t.GetProperty("CssClass");
        PropertyInfo p4 = t.GetProperty("class");

        if (p2 != null)
        {
            p2.SetValue(c, readOnly, null);
            if (p3 != null)
            {
                string s = p3.GetValue(c, null).ToString();
                if (readOnly && !s.Contains("ReadOnly"))
                    s += " ReadOnly";
                if (!readOnly && s.Contains("ReadOnly"))
                    s = s.Replace(" ReadOnly", "");
                p3.SetValue(c, s, null);
            }
            if (p4 != null)
            {
                string s = p4.GetValue(c, null).ToString();
                if (readOnly && !s.Contains("ReadOnly"))
                    s += " ReadOnly";
                if (!readOnly && s.Contains("ReadOnly"))
                    s = s.Replace(" ReadOnly", "");
                p4.SetValue(c, s, null);
            }
        }
        else
        {
            if (p1 != null)
            {
                p1.SetValue(c, !readOnly, null);
            }
        }
    }
    #endregion

    #region SESSION
    /// <summary>
    /// 
    /// </summary>
    private void ReadSession()
    {
        #region READ SESSION VARIABLES
        this.is_DBContrato = (bool)this.Session["is_DBContrato"];
        this.is_Editing = (bool)this.Session["is_Editing"];
        this.ClientesLEs_SortASC = (bool)this.Session["ClientesLEs_SortASC"];
        this.T_AE_ENTIDADE = (DataTable)this.Session["T_AE_ENTIDADE"];
        this.T_AE_ABASTECIMENTO = (DataTable)this.Session["T_AE_ABASTECIMENTO"];
        this.T_AE_ACTIVIDADE = (DataTable)this.Session["T_AE_ACTIVIDADE"];
        this.T_AE_TPR = (DataTable)this.Session["T_AE_TPR"];
        this.T_AE_DEBITO = (DataTable)this.Session["T_AE_DEBITO"];
        this.T_AE_LOG = (DataTable)this.Session["T_AE_LOG"];
        this.LK_TIPO_ACTIVIDADE = (DataTable)this.Session["LK_TIPO_ACTIVIDADE"];
        this.LK_CONCESSIONARIO = (DataTable)this.Session["LK_CONCESSIONARIO"];
        #endregion
    
    }
    /// <summary>
    /// 
    /// </summary>
    private void WriteSession()
    {
        this.Session["is_DBContrato"] = this.is_DBContrato;
        this.Session["is_Editing"] = this.is_Editing;
        this.Session["ClientesLEs_SortASC"] = this.ClientesLEs_SortASC;
        this.Session["T_AE_ENTIDADE"] = this.T_AE_ENTIDADE;
        this.Session["T_AE_ABASTECIMENTO"] = this.T_AE_ABASTECIMENTO;
        this.Session["T_AE_ACTIVIDADE"] = this.T_AE_ACTIVIDADE;
        this.Session["T_AE_TPR"] = this.T_AE_TPR;
        this.Session["T_AE_DEBITO"] = this.T_AE_DEBITO;
        this.Session["T_AE_LOG"] = this.T_AE_LOG;
        this.Session["LK_TIPO_ACTIVIDADE"] = this.LK_TIPO_ACTIVIDADE;
        this.Session["LK_CONCESSIONARIO"] = this.LK_CONCESSIONARIO;
    }
    #endregion

    #region INIT
    /// <summary>
    /// 
    /// </summary>
    private void initializeStaticData()
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);

        #region DADOS CONTRATO
        #region PROPOSTA
        this.C_txt_Ano.Text = DateTime.Now.Year.ToString();
        this.C_ddl_Emissor.DataSource = DB_AE.EmissorGet();
        this.C_ddl_Emissor.DataBind();
        this.C_txt_Emissor.Text = this.C_ddl_Emissor.SelectedValue.PadLeft(2, '0');
        #endregion

        #region RECEP��O
        this.C_ddl_CustomerService.DataSource = DB_AE.CustomerServiceGet();
        this.C_ddl_CustomerService.DataBind();
        #endregion
        #endregion

        #region NIVEL
        this.C_cb_Nivel_LE.Checked = true;
        this.C_ddl_GrupoEconomico.DataSource = DB_AE.GrupoEconomicoGet();
        this.C_ddl_GrupoEconomico.DataBind();
        this.C_cb_Nivel_Changed(this.C_cb_Nivel_LE, new EventArgs());
        #endregion

        #region IDENTIFICA��O CONTRATO
        
        #endregion

        #region ABASTECIMENTO
        this.T_AE_ABASTECIMENTO = DB_AE.AbastecimentoGet(0);
        
        #endregion

        #region ACTIVIDADE
        this.T_AE_ACTIVIDADE = DB_AE.ActividadeGet(0);
        
        #endregion

        #region TPR
        this.T_AE_TPR = DB_AE.TPRGet(0);
        TreeNode root = new TreeNode("�RVORE DE PRODUTOS", "");
        root.PopulateOnDemand = true;
        root.NavigateUrl = "";
        root.SelectAction = TreeNodeSelectAction.Expand;
        this.C_TPR_Tree.Nodes.Add(root);
        this.C_TPR_Tree.DataBind();
        #endregion

        #region D�BITO
        this.T_AE_DEBITO = DB_AE.DebitoGet(0);
        #endregion

        #region LOG
        this.T_AE_LOG = DB_AE.LogGet(0);
        DataRow dr = this.T_AE_LOG.NewRow();
        dr["ID_CONTRATO"] = 0;
        dr["LOG_DATA"] = DateTime.Now;
        dr["LOG_USER"] = User.Identity.Name;
        dr["MESSAGE"] = "NOVO CONTRATO";
        this.T_AE_LOG.Rows.Add(dr);
        #endregion

        #region BOT�ES
        this.C_btn_Insert.Visible = false;
        this.C_btn_Save.Visible = false;
        #endregion

    }
    /// <summary>
    /// 
    /// </summary>
    private void initializeContrato()
    {
        this.initializeStaticData();
        this.is_DBContrato = (this.Request["idContrato"] != null && this.LoadContrato(Convert.ToInt32(this.Request["idContrato"])));

        this.RefreshState();

        if (is_DBContrato)
        {
            this.C_btn_Seguinte.Visible = false;
            this.C_btn_Editar.Visible = true;  
            this.LoadPesquisa();
            this.LoadClientes();
            this.LoadAbastecimento();
            this.LoadActividade();
            this.LoadTPR();
            this.LoadLog();
        }
        this.C_box_Recepcao.Visible = this.is_DBContrato;
        this.C_box_Nivel.Visible = this.is_DBContrato;
        this.C_box_Pesquisa.Visible = this.is_DBContrato && this.is_Editing;
        this.C_box_Entidade.Visible = this.is_DBContrato;
        this.C_box_PontoVenda.Visible = this.is_DBContrato;
        this.C_box_Abastecimento.Visible = this.is_DBContrato;
        this.C_box_Actividade.Visible = this.is_DBContrato;
        this.C_box_TPR.Visible = this.is_DBContrato;
        this.C_box_Debito.Visible = this.is_DBContrato;
        this.C_box_Observacoes.Visible = this.is_DBContrato;
        this.C_box_Log.Visible = this.is_DBContrato;        

        this.lbl_SubTitulo.Text = this.GetLocalResourceObject("ContratoEdit." + this.is_DBContrato.ToString()).ToString();
    }
    #endregion

    #region STATE
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected void C_val_Contrato_validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.IsValid;
        Page.Validate("Proposta");
        e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("Entidades");
        e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("PontoVenda");
        e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("Abastecimento");
        e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("Actividade");
        e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("TPR");
        e.IsValid = e.IsValid && this.IsValid;
    }

    private bool ContratoValido()
    {
        Page.Validate("contrato");
        return this.IsValid;
    }
    /// <summary>
    /// 
    /// </summary>
    private void RefreshState()
    {
        UserRoles UserLevel = UserRoles.Administrator; // ((Utilizador)this.Session["User"]).PermissionLevel();

        #region O CONTRATO N�O � EDIT�VEL
        if (this.is_DBContrato || UserLevel <= UserRoles.Viewer)
        {
            ///Desactivar todos os controlos de edi��o
            this.ContratoEnabled(false);
            this.NivelEnabled(false);
            this.PesquisaEnabled(false);
            this.EntidadesEnabled(false);
            this.PontoVendaEnabled(false);
            this.AbastecimentoEnabled(false);
            this.ActividadeEnabled(false);
            this.TPREnabled(false);
            this.DebitoEnabled(false);
            this.ObservacoesEnabled(false);
            this.LogEnabled(false);
        }
        #endregion

        #region O CONTRATO � TOTALMENTE EDIT�VEL
        else if(this.is_Editing)
        {
            this.ContratoEnabled(true);
            this.NivelEnabled(true);
            this.PesquisaEnabled(true);
            this.EntidadesEnabled(true);
            this.PontoVendaEnabled(true);
            this.AbastecimentoEnabled(true);
            this.ActividadeEnabled(true);
            this.TPREnabled(true);
            this.DebitoEnabled(true);
            this.ObservacoesEnabled(true);
            this.LogEnabled(true);
        }
        #endregion
    }
    #endregion

    #region ACTIVA��O DOS BLOCOS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void ContratoEnabled(bool b)
    {
        this.SetReadOnly(this.C_txt_Ano, !b);
        this.SetReadOnly(this.C_txt_Serie, !b);
        this.SetReadOnly(this.C_txt_Numero, !b);
        this.SetReadOnly(this.C_ddl_Emissor, !b);
        this.SetReadOnly(this.C_ddl_CustomerService, !b);
        this.SetReadOnly(this.C_Recepcao_dp_data, !b);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void NivelEnabled(bool b)
    {
        this.SetReadOnly(this.C_cb_Nivel_GE, !b);
        this.SetReadOnly(this.C_cb_Nivel_SGE, !b);
        this.SetReadOnly(this.C_cb_Nivel_LE, !b);
        this.SetReadOnly(this.C_ddl_GrupoEconomico, !b);
        this.SetReadOnly(this.C_ddl_SubGrupoEconomico, !b);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void PesquisaEnabled(bool b)
    {
        this.C_box_Pesquisa.Visible = b && this.C_cb_Nivel_LE.Checked;
        if (this.C_box_Pesquisa.Visible)
            this.LoadPesquisa();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void EntidadesEnabled(bool b)
    {
        this.C_dg_Entidades.Attributes["CanUse"] = b.ToString().ToLower();
        //this.C_Entidades_btn_Clear.Visible = b && this.C_cb_Nivel_LE.Checked;
        if (b)
            this.LoadClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void PontoVendaEnabled(bool b)
    {
        this.SetReadOnly(this.C_PontoVenda_txt_Nome, !b);
        this.SetReadOnly(this.C_PontoVenda_txt_Morada, !b);
        this.SetReadOnly(this.C_PontoVenda_txt_Localidade, !b);
        this.SetReadOnly(this.C_PontoVenda_txt_Contribuinte, !b);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void AbastecimentoEnabled(bool b)
    {
        this.C_box_Abastecimento_Insert.Visible = b;
        this.C_Abastecimento_dg_Lista.Attributes["CanUse"] = b.ToString().ToLower();
        if (b)
            this.LoadAbastecimento();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void ActividadeEnabled(bool b)
    {
        this.C_box_Actividade_Insert.Visible = b;
        this.C_Actividade_dg_Lista.Attributes["CanUse"] = b.ToString().ToLower();
        if (b)
            this.LoadActividade();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void TPREnabled(bool b)
    {
        this.C_box_TPR_Pesquisa.Visible = b;
        this.C_box_TPR_Tree.Visible = b;
        this.C_TPR_dg_Lista.Attributes["CanUse"] = b.ToString().ToLower();
        if (b)
            this.LoadTPR();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void DebitoEnabled(bool b)
    {
        this.C_box_Debito_Insert.Visible = b;
        this.C_Debito_dg_Lista.Attributes["CanUse"] = b.ToString().ToLower();
        if (b)
            this.LoadDebito();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void LogEnabled(bool b)
    {
        this.C_box_Log_Insert.Visible = b;
        this.C_dg_Log.Attributes["CanUse"] = b.ToString().ToLower();
        if (b)
            this.LoadLog();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void ObservacoesEnabled(bool b)
    {
        this.SetReadOnly(this.C_Observacoes_txt, !b);
    }
    #endregion

    #region CONTRATO
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idContrato"></param>
    /// <returns></returns>
    private bool LoadContrato(int idContrato)
    {
        bool retVal = false;
        try
        {
            /// LER TODO O CONTRATO
            AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
            DataSet Contrato = DB_AE.ContratoGet(idContrato);

            #region  PREENCHER OS CONTROLOS DOS CAMPOS FIXOS DO CONTRATO
            DataRow Ctr = Contrato.Tables["T_AE_CONTRATO"].Rows[0];

            #region PROPOSTA
            this.C_ddl_Emissor.SelectedValue = Ctr["ID_EMISSOR"].ToString();
            this.C_txt_Ano.Text = Ctr["ID_ANO"].ToString();
            this.C_txt_Serie.Text = Ctr["ID_NUM_SERIE"].ToString();
            this.C_txt_Numero.Text = Ctr["ID_NUMERO"].ToString();
            this.C_txt_Emissor.Text = Ctr["ID_EMISSOR"].ToString();
            this.C_Recepcao_dp_data.SelectedDate= (DateTime)Ctr["DATA_RECEPCAO"];
            this.C_ddl_CustomerService.SelectedValue = Ctr["ID_CUSTOMER_SERVICE"].ToString();
            this.C_lbl_NumeroProposta.Text = this.C_txt_Emissor.Text
                                      + " " + this.C_txt_Ano.Text
                                      + " " + this.C_txt_Serie.Text
                                      + " " + this.C_txt_Numero.Text;
            #endregion

            #region NIVEL
            this.C_cb_Nivel_LE.Checked = Ctr["NIVEL_CONTRATO"].ToString() == "LE";
            this.C_cb_Nivel_SGE.Checked = Ctr["NIVEL_CONTRATO"].ToString() == "SGE";
            this.C_cb_Nivel_GE.Checked = Ctr["NIVEL_CONTRATO"].ToString() == "GE";
            this.C_ddl_GrupoEconomico.SelectedValue = Ctr["GRUPO_ECONOMICO_COD"].ToString();
            this.C_ddl_GrupoEconomico_Changed(this.C_ddl_GrupoEconomico, new EventArgs());
            this.C_ddl_SubGrupoEconomico.SelectedValue = Ctr["SUBGRUPO_ECONOMICO_COD"].ToString();
            #endregion

            #region PONTO VENDAS
            this.C_PontoVenda_txt_Nome.Text = Ctr["NOME_FIRMA"].ToString();
            this.C_PontoVenda_txt_Morada.Text = Ctr["MORADA_SEDE"].ToString();
            this.C_PontoVenda_txt_Localidade.Text = Ctr["LOCALIDADE"].ToString();
            this.C_PontoVenda_txt_Contribuinte.Text = Ctr["NIF"].ToString();
            #endregion

            #region OBSERVA��ES
            this.C_Observacoes_txt.Text = Ctr["OBSERVACOES"].ToString();
            #endregion
            #endregion

            #region PREENCHER TABELAS ADICIONAIS

            this.T_AE_ABASTECIMENTO = Contrato.Tables["T_AE_ABASTECIMENTO"];
            this.T_AE_ACTIVIDADE = Contrato.Tables["T_AE_ACTIVIDADE"];
            this.T_AE_DEBITO = Contrato.Tables["T_AE_DEBITO"];
            this.T_AE_ENTIDADE = Contrato.Tables["T_AE_ENTIDADE"];
            this.T_AE_LOG = Contrato.Tables["T_AE_LOG"];
            this.T_AE_TPR = Contrato.Tables["T_AE_TPR"];
            #endregion

            retVal = true;

        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL LER OS DADOS DO CONTRATO", exp);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO", exp);
        }
        return retVal;
        return true;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoAnterior"></param>
    /// <param name="Classif"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private string InserirContrato(ref int newId)
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
        
        this.InsertLog("INSERIR CONTRATO");

        /// INSERIR O CONTRATO
        string numContrato = DB_AE.ContratoInsert(
            ref newId,
            this.C_ddl_Emissor.SelectedValue,
            this.C_txt_Ano.Text,
            this.C_txt_Serie.Text,
            this.C_txt_Numero.Text,
            this.C_Recepcao_dp_data.SelectedDate,
            this.C_ddl_CustomerService.SelectedValue,
            this.C_cb_Nivel_LE.Checked ? "LE" : this.C_cb_Nivel_SGE.Checked ? "SGE" : "GE",
            this.C_ddl_GrupoEconomico.SelectedValue,
            this.C_ddl_SubGrupoEconomico.SelectedValue,
            this.C_PontoVenda_txt_Nome.Text,
            this.C_PontoVenda_txt_Morada.Text,
            this.C_PontoVenda_txt_Localidade.Text,
            this.C_PontoVenda_txt_Contribuinte.Text,
            this.C_Observacoes_txt.Text,
            User.Identity.Name,
            this.T_AE_ENTIDADE,
            this.T_AE_ABASTECIMENTO,
            this.T_AE_ACTIVIDADE,
            this.T_AE_TPR,
            this.T_AE_DEBITO,
            this.T_AE_LOG).ToString();
        return numContrato;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoAnterior"></param>
    /// <param name="Classif"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private string ActualizarContrato(int newId)
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);

        this.InsertLog("ACTUALIZAR CONTRATO");

        /// ACTUALIZAR O CONTRATO
        string numContrato = DB_AE.ContratoUpdate(
            newId,
            this.C_ddl_Emissor.SelectedValue,
            this.C_txt_Ano.Text,
            this.C_txt_Serie.Text,
            this.C_txt_Numero.Text,
            this.C_Recepcao_dp_data.SelectedDate,
            this.C_ddl_CustomerService.SelectedValue,
            this.C_cb_Nivel_LE.Checked ? "LE" : this.C_cb_Nivel_SGE.Checked ? "SGE" : "GE",
            this.C_ddl_GrupoEconomico.SelectedValue,
            this.C_ddl_SubGrupoEconomico.SelectedValue,
            this.C_PontoVenda_txt_Nome.Text,
            this.C_PontoVenda_txt_Morada.Text,
            this.C_PontoVenda_txt_Localidade.Text,
            this.C_PontoVenda_txt_Contribuinte.Text,
            this.C_Observacoes_txt.Text,
            User.Identity.Name,
            this.T_AE_ENTIDADE,
            this.T_AE_ABASTECIMENTO,
            this.T_AE_ACTIVIDADE,
            this.T_AE_TPR,
            this.T_AE_DEBITO,
            this.T_AE_LOG).ToString();
        return numContrato;
    }
    #endregion

    #region PESQUISA
    /// <summary>
    /// 
    /// </summary>
    private void LoadPesquisa()
    {
        bool hasdata = (this.T_AE_ENTIDADE != null && this.T_AE_ENTIDADE.DefaultView.Count > 0);
        AE_Clientes DBClientes = new AE_Clientes(ConfigurationManager.AppSettings["DBCS"]);
        string[] SearchHeaders = ConfigurationManager.AppSettings["ClientAESearchColumns"].Replace(" ", "").Split(new char[] { ',' });
        string[] ResultHeaders = ConfigurationManager.AppSettings["ClientAEResultColumns"].Replace(" ", "").Split(new char[] { ',' });
        this.C_Entidade_Pesquisa.InputDataObject = DBClientes;
        this.C_Entidade_Pesquisa.SearchHeaders = SearchHeaders;
        this.C_Entidade_Pesquisa.ResultHeaders = ResultHeaders;
        this.C_Entidade_Pesquisa.ShowSearchResult = hasdata;
        this.C_Entidade_Pesquisa.ADVModeEnabled = hasdata;
        this.C_Entidade_Pesquisa.Opened = !hasdata;
        this.C_Entidade_Pesquisa.Limpar();
    }
    #endregion

    #region ENTIDADES
    /// <summary>
    /// 
    /// </summary>
    private void LoadClientes()
    {
        #region LOAD DATA
        if (this.T_AE_ENTIDADE == null)
        {
            this.T_AE_ENTIDADE = new DataTable();
        }
        this.C_dg_Entidades.DataSource = this.T_AE_ENTIDADE.DefaultView;
        if (this.C_dg_Entidades.CurrentPageIndex > 0 && ((float)this.T_AE_ENTIDADE.DefaultView.Count / (float)this.C_dg_Entidades.PageSize) <= ((float)this.C_dg_Entidades.CurrentPageIndex))
        {
            --this.C_dg_Entidades.CurrentPageIndex;
        }
        this.C_dg_Entidades.DataBind();
        #endregion

        #region VALIDATE DATA
        this.ValidateClientes();
        #endregion

        #region UPDATE INTERFACE
        string Text = "";
        if (this.T_AE_ENTIDADE.DefaultView.Count > 0)
        {
            if (this.C_dg_Entidades.AllowPaging)
            {
                Text = ((this.C_dg_Entidades.CurrentPageIndex) * this.C_dg_Entidades.PageSize + 1)
                       + " a " + Math.Min(this.T_AE_ENTIDADE.DefaultView.Count, (this.C_dg_Entidades.CurrentPageIndex + 1) * this.C_dg_Entidades.PageSize)
                       + " de " + this.T_AE_ENTIDADE.DefaultView.Count
                       + " Clientes/Locais de Entrega";
            }
            else
            {
                Text = "1 a " + this.T_AE_ENTIDADE.DefaultView.Count
                     + " de " + this.T_AE_ENTIDADE.DefaultView.Count
                     + " Clientes/Locais de Entrega";
            }
        }
        else
        {
            Text = "N�o existem Clientes/Locais de Entrega";
        }
        this.C_lbl_ClientesNum.Text = Text;
        this.C_Entidades_btn_Paginacao.Visible = this.T_AE_ENTIDADE != null
                                              && this.T_AE_ENTIDADE.DefaultView.Count > this.C_dg_Entidades.PageSize;
        if (this.C_dg_Entidades.AllowPaging == false)
        {
            this.C_Entidades_btn_Paginacao.Text = "Ver Lista Paginada";
            this.C_Entidades_btn_Paginacao.ToolTip = "Apresentar a lista com pagina��o";
            this.C_Entidades_btn_Paginacao.ImageUrl = "~/images/Page.ico";
        }
        else
        {
            this.C_Entidades_btn_Paginacao.Text = "Ver Lista Completa";
            this.C_Entidades_btn_Paginacao.ToolTip = "Apresentar a lista sem pagina��o";
            this.C_Entidades_btn_Paginacao.ImageUrl = "~/images/List.ico";
        }
        #endregion

    }
    /// <summary>
    /// 
    /// </summary>
    protected void RefreshClientes()
    {
        try
        {            
            if (this.C_cb_Nivel_GE.Checked)
            {
                AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
                this.T_AE_ENTIDADE  = DB_AE.ClientesGet("COD_GRUPO_ECONOMICO_LE = '" + this.C_ddl_GrupoEconomico.SelectedValue + "'");
            }
            else if (this.C_cb_Nivel_SGE.Checked)
            {
                AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
                this.T_AE_ENTIDADE  = DB_AE.ClientesGet("COD_GRUPO_ECONOMICO_LE = '" + this.C_ddl_GrupoEconomico.SelectedValue + "'"
                                                 + " AND COD_SUBGRUPO_ECONOMICO_LE = '" + this.C_ddl_SubGrupoEconomico.SelectedValue + "'");
            }
            this.LoadClientes();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ACTUALIZAR A LISTA DE LOCAIS DE ENTREGA.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteCliente(int idx)
    {
        this.T_AE_ENTIDADE.DefaultView[idx].Row.Delete();
        this.LoadClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void SelectCliente(int idx)
    {
        DataRowView dt = this.T_AE_ENTIDADE.DefaultView[idx];
        this.C_PontoVenda_txt_Nome.Text = dt["DES_CLIENTE"].ToString();
        this.C_PontoVenda_txt_Morada.Text = dt["MORADA_CLIENTE"].ToString();
        this.C_PontoVenda_txt_Localidade.Text = dt["DES_LOCALIDADE_CLI"].ToString();
        this.C_PontoVenda_txt_Contribuinte.Text = dt["NUM_CONTRIBUINTE"].ToString();
        Page.Validate("PontoVenda");
    }
    /// <summary>
    /// 
    /// </summary>
    private void ValidateClientes()
    {
        this.C_Nivel_lbl_ValGE.Text = "";
        this.C_Nivel_lbl_ValSGE.Text = "";
        if (this.T_AE_ENTIDADE.DefaultView.Count > 0)
        {
            string GE = this.C_ddl_GrupoEconomico.SelectedValue;
            string SGE = this.C_ddl_SubGrupoEconomico.SelectedValue;

            int GEs = this.T_AE_ENTIDADE.Select("COD_GRUPO_ECONOMICO_LE = '" + GE + "'", "", DataViewRowState.CurrentRows).Length;
            int SGEs = this.T_AE_ENTIDADE.Select("COD_SUBGRUPO_ECONOMICO_LE = '" + SGE + "'", "", DataViewRowState.CurrentRows).Length;

            if (GEs == 0)
            {
                this.C_Nivel_lbl_ValGE.Text = "N�o existem LE's do Grupo Econ�mico seleccionado.";
            }
            else if (GEs < this.T_AE_ENTIDADE.DefaultView.Count)
            {
                this.C_Nivel_lbl_ValGE.Text = "Existem LE's que n�o pertencem ao Grupo Econ�mico seleccionado.";
            }
            if (SGE != "0")
            {
                if (SGEs == 0)
                {
                    this.C_Nivel_lbl_ValSGE.Text = "N�o existem LE's do Sub-Grupo Econ�mico seleccionado.";
                }
                else if (SGEs < this.T_AE_ENTIDADE.DefaultView.Count)
                {
                    this.C_Nivel_lbl_ValSGE.Text = "Existem LE's que n�o pertencem ao Sub-Grupo Econ�mico seleccionado.";
                }
            }
        }
    }

    #endregion

    #region PONTO VENDA
    #endregion

    #region ABASTECIMENTO
    /// <summary>
    /// 
    /// </summary>
    private void LoadConcessionarios()
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
        this.LK_CONCESSIONARIO = DB_AE.ConcessionarioGet();
        foreach (DataRow dr in this.LK_CONCESSIONARIO.Rows)
        {
            if (this.T_AE_ABASTECIMENTO.Select("COD_CONCESSIONARIO = " + dr["ID_CONCESSIONARIO"].ToString()).Length > 0)
            {
                dr.Delete();
            }
        }
        this.C_Abastecimento_ddl_Conc.Items.Clear();
        this.C_Abastecimento_ddl_Conc.SelectedValue = null;
        this.C_Abastecimento_ddl_Conc.DataSource = this.LK_CONCESSIONARIO.DefaultView;
        this.C_Abastecimento_ddl_Conc.DataBind();
        if (this.C_Abastecimento_ddl_Conc.Items.Count > 0)
        {
            this.C_Abastecimento_ddl_Conc.Items.Insert(0, new ListItem(" - - - - - - - TODOS OS CONCESSION�RIOS - - - - - - - ", "ALL"));
            this.C_Abastecimento_ddl_Conc.SelectedIndex = 1;
            this.C_box_Abastecimento_Insert.Visible = this.C_Abastecimento_dg_Lista.Attributes["CanUse"]=="true";
        }
        else
        {
            this.C_box_Abastecimento_Insert.Visible = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void LoadAbastecimento()
    {
        this.T_AE_ABASTECIMENTO.DefaultView.Sort = "COD_CONCESSIONARIO ASC";
        this.C_Abastecimento_dg_Lista.DataSource = this.T_AE_ABASTECIMENTO.DefaultView;
        this.C_Abastecimento_dg_Lista.DataBind();

        this.LoadConcessionarios();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteAbastecimento(int idx)
    {
        try
        {
            this.T_AE_ABASTECIMENTO.DefaultView[idx].Row.Delete();
            this.LoadAbastecimento();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL REMOVER O CONCESSIONARIO.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    private void InsertAbastecimento()
    {
        try
        {
            if (this.C_Abastecimento_ddl_Conc.SelectedValue == "ALL")
            {
                foreach (ListItem li in this.C_Abastecimento_ddl_Conc.Items)
                {
                    if (li.Value!="ALL")
                    {
                        DataRow dr = this.T_AE_ABASTECIMENTO.NewRow();
                        dr["ID_CONTRATO"] = 0;
                        dr["COD_CONCESSIONARIO"] = li.Value;
                        dr["FACTURACAO"] = "Y";
                        dr["DISTRIBUICAO"] = "N";
                        dr["OBSERVACOES"] = "";
                        dr["DES_CONCESSIONARIO"] = li.Text.Split(new string[] { " - " }, StringSplitOptions.None)[1];
                        this.T_AE_ABASTECIMENTO.Rows.Add(dr);
                    }
                }
                Page.Validate("Abastecimento");
            }
            else
            {
                DataRow dr = this.T_AE_ABASTECIMENTO.NewRow();
                dr["ID_CONTRATO"] = 0;
                dr["COD_CONCESSIONARIO"] = this.C_Abastecimento_ddl_Conc.SelectedValue;
                dr["FACTURACAO"] = "Y";
                dr["DISTRIBUICAO"] = "N";
                dr["OBSERVACOES"] = "";
                dr["DES_CONCESSIONARIO"] = this.C_Abastecimento_ddl_Conc.SelectedItem.Text.Split(new string[] { " - " }, StringSplitOptions.None)[1];
                this.T_AE_ABASTECIMENTO.Rows.Add(dr);
            }
            this.LoadAbastecimento();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR O CONCESSIONARIO.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void ReadAbastecimento()
    {
        foreach (DataGridItem di in this.C_Abastecimento_dg_Lista.Items)
        {
            if (di.ItemType == ListItemType.Item || di.ItemType == ListItemType.AlternatingItem)
            { 
                DataRow dr = this.T_AE_ABASTECIMENTO.DefaultView[di.ItemIndex].Row;
                CheckBox cbf = (CheckBox)di.FindControl("C_Abastecimento_cb_Facturacao");
                CheckBox cbd = (CheckBox)di.FindControl("C_Abastecimento_cb_Distribuicao");
                TextBox obs = (TextBox)di.FindControl("C_Abastecimento_txt_Obs");

                dr["FACTURACAO"] = cbf.Checked ? "Y" : "N";
                dr["DISTRIBUICAO"] = cbd.Checked ? "Y" : "N";
                dr["OBSERVACOES"] = obs.Text;
            }
        }
    
    }
    #endregion

    #region ACTIVIDADE
    /// <summary>
    /// 
    /// </summary>
    private void LoadTipoActividade()
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
        this.LK_TIPO_ACTIVIDADE = DB_AE.TipoActividadeGet();
        foreach (DataRow dr in this.LK_TIPO_ACTIVIDADE.Rows)
        {
            if (this.T_AE_ACTIVIDADE.Select("ID_TIPO_ACTIVIDADE = " + dr["ID_TIPO_ACTIVIDADE"].ToString()).Length > 0)
            {
                dr.Delete();
            }
        }
        this.C_Actividade_ddl_Tipo.DataSource = this.LK_TIPO_ACTIVIDADE.DefaultView;
        this.C_Actividade_ddl_Tipo.DataBind();
        if (this.C_Actividade_ddl_Tipo.Items.Count > 0)
        {
            this.C_Actividade_ddl_Tipo.SelectedIndex = 0;
            this.C_Actividade_ddl_Tipo_Changed(this.C_Actividade_ddl_Tipo, new EventArgs());
            this.C_box_Actividade_Insert.Visible = this.C_Actividade_dg_Lista.Attributes["CanUse"] == "true";
        }
        else
        {
            this.C_box_Actividade_Insert.Visible = false;
        }
        
    }
    /// <summary>
    /// 
    /// </summary>
    private void LoadActividade()
    {
        this.T_AE_ACTIVIDADE.DefaultView.Sort = "ID_TIPO_ACTIVIDADE ASC";
        this.C_Actividade_dg_Lista.DataSource = this.T_AE_ACTIVIDADE.DefaultView;
        this.C_Actividade_dg_Lista.DataBind();

        this.LoadTipoActividade();
        this.LoadDebito();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteActividade(int idx)
    {
        try
        {
            this.T_AE_ACTIVIDADE.DefaultView[idx].Row.Delete();
            this.LoadActividade();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL REMOVER A ACTIVIDADE.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    private void InsertActividade()
    {
        try
        {
            if (!this.IsValid)
            {
                return;
            }
            DataRow dr = this.T_AE_ACTIVIDADE.NewRow();
            dr["ID_CONTRATO"] = 0;
            dr["ID_TIPO_ACTIVIDADE"] = this.C_Actividade_ddl_Tipo.SelectedValue;
            dr["NOME_TIPO_ACTIVIDADE"] = this.C_Actividade_ddl_Tipo.SelectedItem.Text;
            dr["NUM_ACORDO"] = this.C_Actividade_txt_Acordo.Text;
            double d = 0;
            if (this.C_Actividade_txt_Valor.Text != "")
                d = double.Parse(this.C_Actividade_txt_Valor.Text.Replace(".",","));
            dr["VALOR_ACORDO"] = Math.Round(d, 3);
            this.T_AE_ACTIVIDADE.Rows.Add(dr);
            this.C_Actividade_txt_Acordo.Text = "";
            this.C_Actividade_txt_Valor.Text = "";
            this.LoadActividade();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR UMA NOVA ACTIVIDADE.", exp);
        }
    }
    #endregion
    
    #region TPR
    /// <summary>
    /// 
    /// </summary>
    private void LoadTreeDetails()
    {
        foreach (TreeNode t in this.C_TPR_Tree.CheckedNodes)
        {
            if (myPlaceHolder.FindControl(t.ValuePath) == null)
            {
                ProductTreeDetails l = (ProductTreeDetails)Page.LoadControl("~/UserControls/ProducttreeDetails.ascx");
                myPlaceHolder.Controls.Add(l);
                l.ID = t.ValuePath;
                l.SourceTreeNode = t;
                l.isProduto = t.Depth == 5;

                AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
                DataTable prod = DB_AE.ProdutoSearch(t.Value, "");
                if (prod.Rows.Count == 1)
                {
                    DataRow dr = prod.Rows[0];
                    double d = 0;
                    if (double.TryParse(dr["PVP_PRODUTO"].ToString().Replace(".", ","), out d))
                    {
                        l.PVP = d;
                    }
                    else
                        l.PVP = 0;
                    l.BudgetVendas = 0;
                    l.BudgetMarketing = 0;
                    l.Quantidade = 0;
                    l.UnidadeBase = dr["UNIDADE_BASE"].ToString();
                }
                l.onDataApply += new EventHandler(C_TPR_TreeDetails_Apply);
                l.onDataSave += new EventHandler(C_TPR_TreeDetails_Insert);
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ptd"></param>
    /// <param name="treeNode"></param>
    private void ApplyDetails(ProductTreeDetails ptd, TreeNode tn)
    {
        tn.Expand();
        if (tn.Depth == 5)
        {
            ProductTreeDetails tptd;
            if (tn.Checked)
            {
                tptd = (ProductTreeDetails)this.myPlaceHolder.FindControl(tn.ValuePath);
            }
            else
            {
                tn.Checked = true;
                tptd = (ProductTreeDetails)Page.LoadControl("~/UserControls/ProducttreeDetails.ascx");
                myPlaceHolder.Controls.Add(tptd);
                tptd.ID = tn.ValuePath;
                tptd.SourceTreeNode = tn;
                tptd.isProduto = tn.Depth == 5;
            }
            if (!double.IsNaN(ptd.BudgetVendas))
            {
                tptd.BudgetVendas = ptd.BudgetVendas;
            }
            else if (double.IsNaN(tptd.BudgetVendas))
            {
                tptd.BudgetVendas = 0;
            }
            if (!double.IsNaN(ptd.BudgetMarketing))
            {
                tptd.BudgetMarketing = ptd.BudgetMarketing;
            }
            else if (double.IsNaN(tptd.BudgetMarketing))
            {
                tptd.BudgetMarketing = 0;
            }

            if (ptd.ActividadeInicio != DateTime.MinValue)
            {
                tptd.ActividadeInicio = ptd.ActividadeInicio;
            }
            if (ptd.ActividadeFim != DateTime.MinValue)
            {
                tptd.ActividadeFim = ptd.ActividadeFim;
            }

            if (ptd.ComprasInicio != DateTime.MinValue)
            {
                tptd.ComprasInicio = ptd.ComprasInicio;
            }

            if (ptd.ComprasFim != DateTime.MinValue)
            {
                tptd.ComprasFim = ptd.ComprasFim;
            }

            if (!double.IsNaN(ptd.PVP))
            {
                tptd.PVP = ptd.PVP;
            }
            else if (double.IsNaN(tptd.PVP))
            {
                AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
                DataTable prod = DB_AE.ProdutoSearch(tn.Value, "");
                if (prod.Rows.Count == 1)
                {
                    DataRow dr = prod.Rows[0];
                    double d = 0;
                    if (double.TryParse(dr["PVP_PRODUTO"].ToString().Replace(".", ","), out d))
                    {
                        tptd.PVP = d;

                    }
                    else
                    {
                        tptd.PVP = 0;
                    }
                    tptd.UnidadeBase = dr["UNIDADE_BASE"].ToString();
                }
                else
                {
                    tptd.PVP = 0;
                }
            }
            if (!double.IsNaN(ptd.Quantidade))
            {
                tptd.Quantidade = ptd.Quantidade;
            }
            else if (double.IsNaN(tptd.Quantidade))
            {
                tptd.Quantidade = 0;
            }
            tptd.onDataApply += new EventHandler(C_TPR_TreeDetails_Apply);
            tptd.onDataSave += new EventHandler(C_TPR_TreeDetails_Insert);
        }
        else
        {
            double pvp = ptd.PVP;
            double qtd = ptd.Quantidade;
            if (!ptd.PVPCopy && !double.IsNaN(ptd.PVP))
                ptd.PVP /= tn.ChildNodes.Count;
            if (!ptd.QuantidadeCopy && !double.IsNaN(ptd.Quantidade))
                ptd.Quantidade /= tn.ChildNodes.Count;
            foreach (TreeNode t in tn.ChildNodes)
            {
                ApplyDetails(ptd, t);
            }
            ptd.PVP = pvp;
            ptd.Quantidade = qtd;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void LoadTPR()
    {
        this.T_AE_TPR.DefaultView.Sort = "NOME_PRODUTO ASC";
        this.C_TPR_dg_Lista.DataSource = this.T_AE_TPR.DefaultView;
        this.C_TPR_dg_Lista.DataBind();
        this.C_TPR_btn_ReloadTree_Click(this.C_TPR_btn_ReloadTree, new EventArgs());
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    private void LoadTreeAgrupamento(TreeNodeEventArgs e)
    {
        ArrayList ValidIDs = new ArrayList();
        ValidIDs.Add("001");
        ValidIDs.Add("002");
        ValidIDs.Add("300");
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
        IList Agrup = (from DataRow a in DB_AE.AgrupamentosGet().Rows
                    where ValidIDs.Contains(a["ID_AGRUPAMENTO"].ToString())
                    select a).ToList();
        foreach (DataRow dr in Agrup)
        {
            TreeNode tn = new TreeNode(dr["ID_NOME_AGRUPAMENTO"].ToString(), dr["ID_AGRUPAMENTO"].ToString());
            tn.PopulateOnDemand = true;
            tn.NavigateUrl = "";
            tn.SelectAction = TreeNodeSelectAction.Expand;
            tn.ToolTip = "";
            e.Node.ChildNodes.Add(tn);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    private void LoadTreeGrupoProduto(TreeNodeEventArgs e)
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
        DataTable gp = DB_AE.GrupoProdutoGet(e.Node.Value);
        foreach (DataRow dr in gp.Rows)
        {
            TreeNode tn = new TreeNode(dr["ID_NOME_GRUPO_PRODUTO"].ToString(), dr["ID_GRUPO_PRODUTO"].ToString());
            tn.PopulateOnDemand = true;
            tn.NavigateUrl = "";
            tn.SelectAction = TreeNodeSelectAction.Expand;
            tn.ToolTip = "";
            e.Node.ChildNodes.Add(tn);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>     
    private void LoadTreeSubGrupoProduto(TreeNodeEventArgs e)
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
        DataTable sgp = DB_AE.SubGrupoProdutoGet(e.Node.Value);
        foreach (DataRow dr in sgp.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Value = dr["ID_SUBGRUPO_PRODUTO"].ToString();
            tn.Text = dr["ID_NOME_SUBGRUPO_PRODUTO"].ToString();
            tn.PopulateOnDemand = true;
            tn.ShowCheckBox = true;
            tn.NavigateUrl = "";
            tn.SelectAction = TreeNodeSelectAction.Expand;
            tn.ToolTip = "";
            e.Node.ChildNodes.Add(tn);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    private void LoadTreeSubAggProduto(TreeNodeEventArgs e)
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
        DataTable sp = DB_AE.SubAggProdutoGet(e.Node.Parent.Value, e.Node.Value);
        foreach (DataRow dr in sp.Rows)
        {
            TreeNode tn = new TreeNode(dr["ID_NOME_SUB_AGG_PRODUTO"].ToString(), dr["ID_SUB_AGG_PRODUTO"].ToString());
            tn.PopulateOnDemand = true;
            tn.ShowCheckBox = true;
            tn.NavigateUrl = "";
            tn.SelectAction = TreeNodeSelectAction.Expand;
            tn.ToolTip = "";
            e.Node.ChildNodes.Add(tn);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    private void LoadTreeProduto(TreeNodeEventArgs e)
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
        DataTable p = DB_AE.ProdutoGet(e.Node.Parent.Parent.Value, e.Node.Parent.Value, e.Node.Value);
        foreach (DataRow dr in p.Rows)
        {
            TreeNode tn = new TreeNode(dr["ID_NOME_PRODUTO"].ToString(), dr["ID_PRODUTO"].ToString());
            tn.PopulateOnDemand = false;
            tn.SelectAction = TreeNodeSelectAction.None;
            tn.Expand();
            tn.ShowCheckBox = true;
            tn.NavigateUrl = "";
            tn.ToolTip = "";
            e.Node.ChildNodes.Add(tn);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteTPR(int idx)
    {
        try
        {
            this.T_AE_TPR.DefaultView[idx].Row.Delete();
            this.LoadTPR();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL REMOVER A ACTIVIDADE.", exp);
        }
        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void InsertTPR()
    {
        foreach (TreeNode t in this.C_TPR_Tree.CheckedNodes)
        {
            if (myPlaceHolder.FindControl(t.ValuePath) != null)
            {
                ProductTreeDetails prod = (ProductTreeDetails)myPlaceHolder.FindControl(t.ValuePath);
                if (prod.isProduto && this.T_AE_TPR.Select("COD_PRODUTO = '"+prod.SourceTreeNode.Value+"'","",DataViewRowState.CurrentRows).Length==0)
                {
                    DataRow ndr = this.T_AE_TPR.NewRow();
                    ndr["COD_PRODUTO"] = prod.SourceTreeNode.Value;
                    ndr["NOME_PRODUTO"] = prod.SourceTreeNode.Text.Split(new string[]{" - "},StringSplitOptions.None)[1];
                    ndr["BUDGET_VENDAS"] = (double)prod.BudgetVendas;
                    ndr["BUDGET_MARKETING"] = (double)prod.BudgetMarketing;
                    ndr["ACTIVIDADE_INICIO"] = prod.ActividadeInicio;
                    ndr["ACTIVIDADE_FIM"] = prod.ActividadeFim;
                    ndr["COMPRAS_INICIO"] = prod.ComprasInicio;
                    ndr["COMPRAS_FIM"] = prod.ComprasFim;
                    ndr["PVP"] = double.Parse(prod.PVP.ToString());
                    ndr["UNIDADE_BASE"] = prod.UnidadeBase;
                    ndr["PREVISAO_QUANTIDADE"] = double.Parse(prod.Quantidade.ToString());
                    this.T_AE_TPR.Rows.Add(ndr);
                }
            }
        }
        this.LoadTPR();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    private void UpdateTPR(DataGridItem e)
    {
        DataRowView ndr = this.T_AE_TPR.DefaultView[e.ItemIndex];
        if(ndr!=null)
        {
            TextBox BudgetVendas = (TextBox)e.FindControl("C_txt_BudgetVendas");
            TextBox BudgetMarketing = (TextBox)e.FindControl("C_txt_BudgetMarketing");
            DatePicker C_dp_ActividadeInicio = (DatePicker)e.FindControl("C_dp_ActividadeInicio");
            DatePicker C_dp_ActividadeFim = (DatePicker)e.FindControl("C_dp_ActividadeFim");
            DatePicker C_dp_ComprasInicio = (DatePicker)e.FindControl("C_dp_ComprasInicio");
            DatePicker C_dp_ComprasFim = (DatePicker)e.FindControl("C_dp_ComprasFim");
            TextBox C_txt_PVP = (TextBox)e.FindControl("C_txt_PVP");
            TextBox C_txt_Qtd = (TextBox)e.FindControl("C_txt_Qtd");

            ndr["BUDGET_VENDAS"] = double.Parse(BudgetVendas.Text.Replace(".", ","));
            ndr["BUDGET_MARKETING"] = double.Parse(BudgetMarketing.Text.Replace(".", ","));
            ndr["ACTIVIDADE_INICIO"] = C_dp_ActividadeInicio.SelectedDate;
            ndr["ACTIVIDADE_FIM"] = C_dp_ActividadeFim.SelectedDate;
            ndr["COMPRAS_INICIO"] = C_dp_ComprasInicio.SelectedDate;
            ndr["COMPRAS_FIM"] = C_dp_ComprasFim.SelectedDate;
            ndr["PVP"] = double.Parse(C_txt_PVP.Text.Replace(".", ","));
            ndr["PREVISAO_QUANTIDADE"] = double.Parse(C_txt_Qtd.Text.Replace(".", ","));
        }
    }
    #endregion

    #region DEBITO
    /// <summary>
    /// 
    /// </summary>
    private void LoadDebito()
    {
        this.T_AE_DEBITO.DefaultView.Sort = "DATA_DEBITO ASC";
        this.C_Debito_dg_Lista.DataSource = this.T_AE_DEBITO.DefaultView;
        this.C_Debito_dg_Lista.DataBind();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteDebito(int idx)
    {
        this.T_AE_DEBITO.DefaultView[idx].Row.Delete();
        this.LoadDebito();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void InsertDebito()
    {
        try
        {
            if (!this.IsValid)
            {
                return;
            }
            DataRow dr = this.T_AE_DEBITO.NewRow();
            dr["ID_CONTRATO"] = 0;
            dr["DATA_DEBITO"] = this.C_Debito_dp_Data.SelectedDate;
            double d = 0;
            d = double.Parse(this.C_Debito_txt_Valor.Text.Replace(".", ","));
            dr["VALOR_DEBITO"] = Math.Round(d, 3);
            this.T_AE_DEBITO.Rows.Add(dr);
            this.C_Debito_txt_Valor.Text = "";
            this.Form.DefaultFocus = this.C_Debito_txt_Valor.ID;
            this.LoadDebito();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR UM NOVO D�BITO.", exp);
        }
    }
    #endregion

    #region LOG
    /// <summary>
    /// 
    /// </summary>
    private void LoadLog()
    {
        this.T_AE_LOG.DefaultView.Sort = "LOG_DATA DESC";
        this.C_dg_Log.DataSource = this.T_AE_LOG.DefaultView;
        if (this.C_dg_Log.CurrentPageIndex > 0 && ((float)this.T_AE_LOG.DefaultView.Count / (float)this.C_dg_Log.PageSize) <= ((float)this.C_dg_Log.CurrentPageIndex))
        {
            --this.C_dg_Log.CurrentPageIndex;
        }
        this.C_dg_Log.DataBind();
        string Text = "";
        if (this.T_AE_LOG.DefaultView.Count > 0)
        {
            if (this.C_dg_Log.AllowPaging)
            {
                Text = ((this.C_dg_Log.CurrentPageIndex) * this.C_dg_Log.PageSize + 1)
                       + " a " + Math.Min(this.T_AE_LOG.DefaultView.Count, (this.C_dg_Log.CurrentPageIndex + 1) * this.C_dg_Log.PageSize)
                       + " de " + this.T_AE_LOG.DefaultView.Count
                       + " Registos";
            }
            else
            {
                Text = "1 a " + this.T_AE_LOG.DefaultView.Count
                     + " de " + this.T_AE_LOG.DefaultView.Count
                     + " Registos";
            }
        }
        else
        {
            Text = "N�o existem registos";
        }
        this.C_lbl_LogNum.Text = Text;
        this.C_Log_btn_Paginacao.Visible = this.T_AE_LOG != null
                                              && this.T_AE_LOG.DefaultView.Count > this.C_dg_Log.PageSize;
        if (this.C_dg_Log.AllowPaging == false)
        {
            this.C_Log_btn_Paginacao.Text = "Ver Lista Paginada";
            this.C_Log_btn_Paginacao.ToolTip = "Apresentar a lista com pagina��o";
            this.C_Log_btn_Paginacao.ImageUrl = "~/images/Page.ico";
        }
        else
        {
            this.C_Log_btn_Paginacao.Text = "Ver Lista Completa";
            this.C_Log_btn_Paginacao.ToolTip = "Apresentar a lista sem pagina��o";
            this.C_Log_btn_Paginacao.ImageUrl = "~/images/List.ico";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void InsertLog()
    {
        try
        {
            if (!this.IsValid)
            {
                return;
            }
            DataRow dr = this.T_AE_LOG.NewRow();
            dr["ID_CONTRATO"] = 0;
            dr["LOG_DATA"] = DateTime.Now;
            dr["LOG_USER"] = User.Identity.Name;
            dr["MESSAGE"] = this.C_Log_txt_Valor.Text;
            
            this.T_AE_LOG.Rows.Add(dr);
            this.C_Log_txt_Valor.Text = "";
            this.Form.DefaultFocus = "C_Log_txt_Valor";
            this.LoadLog();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR A MENSAGEM.", exp);
        }
    }
    protected void InsertLog(string Message)
    {
        try
        {
            DataRow dr = this.T_AE_LOG.NewRow();
            dr["ID_CONTRATO"] = 1;
            dr["LOG_DATA"] = DateTime.Now;
            dr["LOG_USER"] = User.Identity.Name;
            dr["MESSAGE"] = Message;

            this.T_AE_LOG.Rows.Add(dr);
            this.C_Log_txt_Valor.Text = "";
            this.LoadLog();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR A MENSAGEM.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteLog(int idx)
    {
        this.T_AE_LOG.DefaultView[idx].Row.Delete();
        this.LoadLog();
    }
    #endregion

    #region OBSERVA��ES
    /// <summary>
    /// 
    /// </summary>
    private void loadObservacoes()
    {

    }
    #endregion
    #endregion

    #region PAGE EVENTS
   
    protected override object SaveViewState()
    {
        this.WriteSession();
        return base.SaveViewState();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, System.EventArgs e)
    {
        #region VALIDATE LOGIN
        if (User.Identity.IsAuthenticated == false)
        {
            Response.Redirect("~/logon.aspx",true);
        }
        #endregion

        this.MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            #region FIRST LOAD
            this.initializeContrato();
            this.Form.DefaultFocus = "C_txt_Serie";
            #endregion
        }
        else
        {
            #region RELOAD
            this.ReadSession();

            this.lblError.Text = "";
            this.lblWarning.Text = "";
            this.lblError.ToolTip = "";
            this.lblWarning.ToolTip = "";
            this.C_TPR_lbl_SearchResult.Text = "";
            this.LoadTreeDetails();
            #endregion
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!User.Identity.Name.Equals(""))
        {
            this.DataBind();
        }
        //ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), "sub", "MySubmit();");
        //this.WriteSession();
    }
    /// <summary>
    /// 
    /// </summary>
    public override void DataBind()
    {
        UserRoles UserLevel = UserRoles.Administrator; // ((Utilizador)this.Session["User"]).PermissionLevel();

        #region TITULO
        this.C_btn_Print.Visible = this.is_DBContrato;
        #endregion

        #region DADOS DO CONTRATO
        #region PROPOSTA
        this.C_txt_Emissor.Text = this.C_ddl_Emissor.SelectedValue;
        #endregion
        #endregion

        #region BOT�ES
        this.C_btn_Novo.Visible = UserLevel >= UserRoles.PowerUser;
        this.C_btn_Editar.Visible = this.is_DBContrato
                                 && UserLevel >= UserRoles.PowerUser;
        #endregion

    }
    #endregion

    #region CONTROL EVENTS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txt_Empty_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        Control t = cv;
        WebControl c = null;
        while (t != null && c == null)
        { 
            c=(WebControl)t.FindControl(cv.ControlToValidate);
            t = t.Parent;
        }
        if (c != null)
        {
            if (e.Value.Length == 0)
            {
                e.IsValid = false;
                if (!c.CssClass.Contains("errorField"))
                {
                    c.CssClass = c.CssClass + " errorField";
                }
            }
            else
            {
                e.IsValid = true;
                c.CssClass = c.CssClass.Replace(" errorField", "");
            }
        }
    }

    #region DADOS DO CONTRATO
    #region PROPOSTA
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    bool hasChanged = false;
    protected void C_txt_Numproposta_Changed(object sender, EventArgs e)
    {
        if (!hasChanged)
        {
            this.Validate("Proposta");
        }
        hasChanged = true;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txtAno_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        WebControl c = (WebControl)this.FindControl(cv.ControlToValidate);
        int i = 0;
        if (!int.TryParse(e.Value, out i) || i < 1900)
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }
        else
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txtSerie_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        TextBox c = (TextBox)this.FindControl(cv.ControlToValidate);
        int i = 0;
        if (!int.TryParse(e.Value, out i) || i < 0)
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }
        else
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
            c.Text = c.Text.PadLeft(c.MaxLength, '0');
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txtNumero_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        TextBox c = (TextBox)this.FindControl(cv.ControlToValidate);
        int i = 0;
        if (!int.TryParse(e.Value, out i) || i < 0)
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }
        else
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
            c.Text = c.Text.PadLeft(c.MaxLength, '0');
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_Proposta_validate(object sender, ServerValidateEventArgs e)
    {
        if (this.IsValid)
        {
            CustomValidator cv = (CustomValidator)sender;
            AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
            int idContrato = -1;
            if (DB_AE.ContratoExists(this.C_txt_Emissor.Text, this.C_txt_Ano.Text, this.C_txt_Serie.Text, this.C_txt_Numero.Text, out idContrato)
               && (!this.is_Editing ||( this.Request["idContrato"] != null && idContrato != Convert.ToInt32(this.Request["idContrato"]))))
            {
                e.IsValid = false;
                this.C_img_CheckProposta.ImageUrl = "~/images/Delete2.ico";
                this.C_lbl_CkeckProposta.Text = "J�&nbsp;EXISTE";
                this.C_btn_CheckEdit.NavigateUrl = "~/contratoAE.aspx?idContrato=" + idContrato;
                this.C_btn_CheckEdit.ToolTip = "Abrir o contrato com o n�mero '" + this.C_lbl_NumeroProposta.Text + "'";
                this.C_btn_CheckEdit.Visible = true;
                this.C_img_CheckProposta.Visible = true;
            }
            else
            {
                e.IsValid = true;
                this.C_img_CheckProposta.ImageUrl = "~/images/Check2.ico";
                this.C_lbl_CkeckProposta.Text = "V�LIDO";
                this.C_img_CheckProposta.Visible = true;
                this.C_btn_CheckEdit.Visible = false;
            }
            this.C_lbl_NumeroProposta.Text = this.C_txt_Emissor.Text
                                      + " " + this.C_txt_Ano.Text
                                      + " " + this.C_txt_Serie.Text
                                      + " " + this.C_txt_Numero.Text;
        }
        else
        {
            this.C_lbl_CkeckProposta.Text = "";
            this.C_img_CheckProposta.Visible = false;
            this.C_btn_CheckEdit.Visible = false;
            this.C_lbl_NumeroProposta.Text = "";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_ddl_Emissor_Changed(object sender, EventArgs e)
    {
        this.C_txt_Emissor.Text = this.C_ddl_Emissor.SelectedValue.PadLeft(2, '0');
        if (this.C_lbl_NumeroProposta.Text.Length > 0)
            this.C_txt_Numproposta_Changed(this.C_txt_Emissor, new EventArgs());
    }
    #endregion    
    #endregion

    #region NIVEL
    protected void C_cb_Nivel_Changed(object sender, EventArgs e)
    {
        try
        {
            RadioButton rb = (RadioButton)sender;
            string val = this.C_ddl_SubGrupoEconomico.SelectedValue;

            if (rb.ID.Contains("_LE"))
            {
                this.RefreshClientes();
                this.LoadPesquisa();
                this.C_box_Pesquisa.Visible = this.is_Editing || !this.is_DBContrato;
            }
            else
            {
                this.C_box_Pesquisa.Visible = false;
            }
            this.C_ddl_GrupoEconomico_Changed(this.C_ddl_GrupoEconomico, new EventArgs());
            if (this.C_ddl_SubGrupoEconomico.Items.FindByValue(val) != null)
            {
                this.C_ddl_SubGrupoEconomico.SelectedValue = val;
            }
            this.C_ddl_SubGrupoEconomico_Changed(this.C_ddl_GrupoEconomico, new EventArgs());
            if(!rb.ID.Contains("_LE"))
                Page.Validate("Entidades");            
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL MUDAR O N�VEL DO CONTRATO.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_ddl_GrupoEconomico_Changed(object sender, EventArgs e)
    {
        AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
        this.C_ddl_SubGrupoEconomico.Items.Clear();
        if (!this.C_cb_Nivel_SGE.Checked)
        {
            this.C_ddl_SubGrupoEconomico.Items.Add(new ListItem("- - - TODOS - - -", "0"));
        }
        this.C_ddl_SubGrupoEconomico.DataSource = DB_AE.SubGrupoEconomicoGet(this.C_ddl_GrupoEconomico.SelectedValue);
        this.C_ddl_SubGrupoEconomico.DataBind();
        if (this.C_cb_Nivel_GE.Checked)
        {
            this.RefreshClientes();
        }
        this.ValidateClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_ddl_SubGrupoEconomico_Changed(object sender, EventArgs e)
    {
        if (this.C_cb_Nivel_SGE.Checked)
        {
            this.RefreshClientes();
        }
        this.ValidateClientes();
    }
    #endregion

    #region PESQUISA
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="data"></param>
    protected void C_Entidade_Pesquisa_onSearchResult(Object sender, DataTable data)
    {
        if (data.Rows.Count > 0)
        {
            this.C_Entidade_Pesquisa.ShowSearchResult = true;
            this.C_Entidade_Pesquisa.Opened = false;
            this.C_Entidade_Pesquisa.ADVModeEnabled = true;
            this.C_Entidade_Pesquisa.Limpar();
            
            this.T_AE_ENTIDADE = data;
            this.LoadClientes();
        }
        else
        {
            this._M_ShowError(this.lblWarning, "N�O FORAM ENCONTRADOS CLIENTES COM OS PAR�METROS INDICADOS.", null);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="data"></param>
    protected void C_Entidade_Pesquisa_onSelectResult(Object sender, DataRow data)
    {
        if (this.T_AE_ENTIDADE.Select("COD_LOCAL_ENTREGA = " + data[0].ToString(), "", this.T_AE_ENTIDADE.DefaultView.RowStateFilter).Length == 0)
        {
            this.T_AE_ENTIDADE.Rows.Add(data.ItemArray);
            this.LoadClientes();
            Page.Validate("Entidades");
        }
    }
    #endregion

    #region ENTIDADES
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_Entidades_Empty_validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.T_AE_ENTIDADE.DefaultView.Count > 0;

        string css = this.C_dg_Entidades.CssClass;
        if (e.IsValid)
        {
            css = css.Replace(" errorField", "");
        }
        else
        {
            if (!css.Contains("errorField"))
            {
                css = css + " errorField";
            }
        }
        this.C_dg_Entidades.CssClass = css;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_dg_Entidades_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (this.C_dg_Entidades.Attributes["CanUse"] == "false")
            return;
        switch (e.CommandName)
        {
            case "Delete":
                this.DeleteCliente((this.C_dg_Entidades.CurrentPageIndex * this.C_dg_Entidades.PageSize) + e.Item.ItemIndex);
                Page.Validate("Entidades");
                break;
            case "DeleteAll":
                this.T_AE_ENTIDADE.Clear();
                this.C_Entidade_Pesquisa.ShowSearchResult = false;
                this.C_Entidade_Pesquisa.Opened = true;
                this.C_Entidade_Pesquisa.ADVModeEnabled = false;
                this.C_Entidade_Pesquisa.Limpar();            
                this.LoadClientes();
                break;
            case "Select":
                if (!this.C_PontoVenda_txt_Nome.Enabled)
                    return;
                this.SelectCliente((this.C_dg_Entidades.CurrentPageIndex * this.C_dg_Entidades.PageSize) + e.Item.ItemIndex);
                break;
            default:
                break;
        }
        if (this.T_AE_ENTIDADE.DefaultView.Count == 0)
        {
            this.C_Entidade_Pesquisa.ShowSearchResult = false;
            this.C_Entidade_Pesquisa.Opened = true;
            this.C_Entidade_Pesquisa.ADVModeEnabled = false;
            this.C_Entidade_Pesquisa.Limpar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_dg_Entidades_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        this.T_AE_ENTIDADE.DefaultView.Sort = e.SortExpression + (this.ClientesLEs_SortASC == true ? " ASC" : " DESC");
        this.ClientesLEs_SortASC = !this.ClientesLEs_SortASC;
        this.LoadClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_dg_Entidades_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        this.C_dg_Entidades.CurrentPageIndex = e.NewPageIndex;
        this.LoadClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Entidades_btn_Paginacao_Click(object source, EventArgs e)
    {
        this.C_dg_Entidades.AllowPaging = !this.C_dg_Entidades.AllowPaging;
        this.LoadClientes();
    }
    #endregion

    #region IDENTIFICA��O CONTRATO
   
    #endregion

    #region ABASTECIMENTO
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Abastecimento_btn_Load_Click(object source, EventArgs e)
    {
        foreach (DataRowView dr in this.T_AE_ENTIDADE.DefaultView)
        {
            string conc = dr["COD_CONCESSIONARIO"].ToString();

            ListItem li = this.C_Abastecimento_ddl_Conc.Items.FindByValue(conc);
            if (li != null)
            {
                this.C_Abastecimento_ddl_Conc.SelectedValue = conc;
                this.InsertAbastecimento();
            }
        }
        Page.Validate("Abastecimento");

    }    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_Abastecimento_dg_Lista_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView dr = (DataRowView)e.Item.DataItem;
            CheckBox C_Abastecimento_cb_Facturacao = (CheckBox)e.Item.FindControl("C_Abastecimento_cb_Facturacao");
            CheckBox C_Abastecimento_cb_Distribuicao = (CheckBox)e.Item.FindControl("C_Abastecimento_cb_Distribuicao");

            C_Abastecimento_cb_Facturacao.Attributes["onclick"] = "javascript:CanUncheck(this,'" + C_Abastecimento_cb_Distribuicao.ClientID + "')";
            C_Abastecimento_cb_Distribuicao.Attributes["onclick"] = "javascript:CanUncheck(this,'" + C_Abastecimento_cb_Facturacao.ClientID + "')";
            C_Abastecimento_cb_Facturacao.Checked = dr["FACTURACAO"].ToString() == "Y";
            C_Abastecimento_cb_Distribuicao.Checked = dr["DISTRIBUICAO"].ToString() == "Y";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Abastecimento_dg_Lista_ItemCommand(object sender, DataGridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteAbastecimento(e.Item.ItemIndex);
                break;
            case "ApagarTudo":
                this.T_AE_ABASTECIMENTO.Clear();
                this.LoadAbastecimento();
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Abastecimento_Add_ItemCommand(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Inserir":
                this.InsertAbastecimento();
                Page.Validate("Abastecimento");
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected bool loaded = false;
    protected void AbastecimentoChange(object sender, EventArgs e)
    {
        if (!loaded)
        { 
            this.ReadAbastecimento();
            loaded = true;
        }    
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_Abastecimento_val_Empty_validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.T_AE_ABASTECIMENTO.DefaultView.Count > 0;
        string css = this.C_box_Abastecimento_Lista.Attributes["Class"];
        if (e.IsValid)
        {
            css = css.Replace(" errorField", "");
        }
        else
        {
            if (!css.Contains("errorField"))
            {
                css = css + " errorField";
            }
        }
        this.C_box_Abastecimento_Lista.Attributes["Class"] = css;
    }
    #endregion

    #region ACTIVIDADE
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_Actividade_Empty_validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.T_AE_ACTIVIDADE.DefaultView.Count > 0;
        string css = this.C_box_Actividade_Lista.Attributes["Class"];
        if (e.IsValid)
        {
            css = css.Replace(" errorField", "");
        }
        else
        {
            if (!css.Contains("errorField"))
            {
                css = css + " errorField";
            }
        }
        this.C_box_Actividade_Lista.Attributes["Class"] = css;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_Actividade_val_txtValor_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        TextBox c = (TextBox)this.FindControl(cv.ControlToValidate);
        double i = 0;
        if (!double.TryParse(e.Value.Replace(".",","), out i) || i < 0 || i > 999999999.99)
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }
        else
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>   
    protected void C_Actividade_dg_Lista_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            double ActividadeVal = 0;
            Label C_Actividade_Total = (Label)e.Item.FindControl("C_Actividade_Total");
            
            foreach (DataRowView dr in this.T_AE_ACTIVIDADE.DefaultView)
            {
                ActividadeVal += double.Parse(dr["VALOR_ACORDO"].ToString());
            }
            C_Actividade_Total.Text = ActividadeVal.ToString("#0.00");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Actividade_dg_Lista_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteActividade(e.Item.ItemIndex);
                break;
            case "ApagarTudo":
                this.T_AE_ACTIVIDADE.Clear();
                this.LoadActividade();
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Actividade_Add_ItemCommand(object source, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Inserir":
                this.InsertActividade();
                Page.Validate("Actividade");
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_Actividade_ddl_Tipo_Changed(object sender, EventArgs e)
    {
        this.Panel_Actividade_Acordo.Visible = this.LK_TIPO_ACTIVIDADE.Select("ID_TIPO_ACTIVIDADE = " + this.C_Actividade_ddl_Tipo.SelectedValue)[0]["HAS_ACORDO"].ToString() == "1";
        this.Panel_Actividade_Valor.Visible = this.LK_TIPO_ACTIVIDADE.Select("ID_TIPO_ACTIVIDADE = " + this.C_Actividade_ddl_Tipo.SelectedValue)[0]["HAS_VALOR"].ToString() == "1";
        if (this.Panel_Actividade_Acordo.Visible)
            this.Form.DefaultFocus = this.C_Actividade_txt_Acordo.ID;
        else if (this.Panel_Actividade_Valor.Visible)
            this.Form.DefaultFocus = this.C_Actividade_txt_Valor.ID;
    }
    #endregion

    #region TPR
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_TPR_Empty_validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.T_AE_TPR.DefaultView.Count > 0;
        string css = this.C_box_TPR_Lista.Attributes["Class"];
        if (e.IsValid)
        {
            css = css.Replace(" errorField", "");
        }
        else
        {
            if (!css.Contains("errorField"))
            {
                css = css + " errorField";
            }
        }
        this.C_box_TPR_Lista.Attributes["Class"] = css;
    }

    #region LISTA
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_TPR_dg_Lista_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label C_TPR_Total_PVP = (Label)e.Item.FindControl("C_TPR_Total_PVP");
            Label C_TPR_Total_PrevisaoUn = (Label)e.Item.FindControl("C_TPR_Total_PrevisaoUn");
            Label C_TPR_Total_PrevisaoValor = (Label)e.Item.FindControl("C_TPR_Total_PrevisaoValor");
            Label C_TPR_Total_Vendas = (Label)e.Item.FindControl("C_TPR_Total_Vendas");
            Label C_TPR_Total_Marketing = (Label)e.Item.FindControl("C_TPR_Total_Marketing");
            Label C_TPR_Total_Total = (Label)e.Item.FindControl("C_TPR_Total_Total");

            double tmp_PVP = 0;
            double tmp_QTD = 0;
            double tmp_Valor = 0;
            double tmp_BVendas = 0;
            double tmp_BMarketing = 0;
            
            double PVP = 0;
            double QTD = 0;
            double Valor = 0;
            double Vendas = 0;
            double Marketing = 0;
            double Total = 0;

            foreach (DataRowView dr in this.T_AE_TPR.DefaultView)
            {
                tmp_PVP = double.Parse(dr["PVP"].ToString());
                tmp_QTD = double.Parse(dr["PREVISAO_QUANTIDADE"].ToString());
                tmp_BVendas = double.Parse(dr["BUDGET_VENDAS"].ToString()) / 100;
                tmp_BMarketing = double.Parse(dr["BUDGET_MARKETING"].ToString()) / 100;
                tmp_Valor = tmp_PVP * tmp_QTD;

                PVP += tmp_PVP;
                QTD += tmp_QTD;
                Valor += tmp_Valor;
                Vendas += tmp_Valor * tmp_BVendas;
                Marketing += tmp_Valor * tmp_BMarketing;
                Total += (tmp_Valor * tmp_BVendas) + (tmp_Valor * tmp_BMarketing);
            }
            C_TPR_Total_PVP.Text = PVP.ToString("#0.00");
            C_TPR_Total_PrevisaoUn.Text = QTD.ToString("#0.00");
            C_TPR_Total_PrevisaoValor.Text = Valor.ToString("#0.00");
            C_TPR_Total_Vendas.Text = Vendas.ToString("#0.00");
            C_TPR_Total_Marketing.Text = Marketing.ToString("#0.00");
            C_TPR_Total_Total.Text = Total.ToString("#0.00");

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_TPR_dg_Lista_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteTPR(e.Item.ItemIndex);
                break;
            case "ApagarTudo":
                this.T_AE_TPR.Clear();
                this.LoadTPR();
                break;
            case "Edit":
                this.C_TPR_dg_Lista.EditItemIndex = e.Item.ItemIndex;
                this.LoadTPR();
                break;
            case "Cancel":
                this.C_TPR_dg_Lista.EditItemIndex = -1;
                this.LoadTPR();
                break;
            case "Update":
                if (IsValid)
                {
                    DataGridItem d = e.Item;
                    this.UpdateTPR(d);
                    this.C_TPR_dg_Lista.EditItemIndex = -1;
                    this.LoadTPR();
                }                
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txt_Percent_validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        Control t = cv;
        WebControl c = null;
        while (t != null && c == null)
        { 
            c=(WebControl)t.FindControl(cv.ControlToValidate);
            t = t.Parent;
        }
        if (c != null)
        {
            double d = 0;
            if (double.TryParse(e.Value.Replace(".", ","), out d) && d >= 0 && d <= 100)
            {
                e.IsValid = true;
                c.CssClass = c.CssClass.Replace(" errorField", "");
            }
            else
            {
                e.IsValid = false;
                if (!c.CssClass.Contains("errorField"))
                {
                    c.CssClass = c.CssClass + " errorField";
                }
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txt_Value_validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        Control t = cv;
        WebControl c = null;
        while (t != null && c == null)
        { 
            c=(WebControl)t.FindControl(cv.ControlToValidate);
            t = t.Parent;
        }
        if (c != null)
        {
            double d = 0;
            if (double.TryParse(e.Value.Replace(".", ","), out d) && d >= 0 && d < 1000000)
            {
                e.IsValid = true;
                c.CssClass = c.CssClass.Replace(" errorField", "");
            }
            else
            {
                e.IsValid = false;
                if (!c.CssClass.Contains("errorField"))
                {
                    c.CssClass = c.CssClass + " errorField";
                }
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_ActividadeDatas_Validate(object sender, ServerValidateEventArgs e)
    {        
        Control t = (Control)sender;
        DatePicker C_dp_ActividadeInicio = null;
        DatePicker C_dp_ActividadeFim = null;
        while (t != null && C_dp_ActividadeInicio == null)
        {
            C_dp_ActividadeInicio = (DatePicker)t.FindControl("C_dp_ActividadeInicio");
            C_dp_ActividadeFim = (DatePicker)t.FindControl("C_dp_ActividadeFim");
            t = t.Parent;
        }
        if (C_dp_ActividadeInicio != null && C_dp_ActividadeFim != null)
        {

            string css = C_dp_ActividadeInicio.TextCssClass;
            if (!(C_dp_ActividadeInicio.IsClear || C_dp_ActividadeFim.IsClear) && C_dp_ActividadeInicio.SelectedDate > C_dp_ActividadeFim.SelectedDate)
            {
                e.IsValid = false;
                if (!css.Contains("errorField"))
                {
                    css = css + " errorField";
                }
            }
            else
            {
                e.IsValid = true;
                css = css.Replace(" errorField", "");
            }
            C_dp_ActividadeInicio.TextCssClass = css;
            C_dp_ActividadeFim.TextCssClass = css;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_ComprasDatas_Validate(object sender, ServerValidateEventArgs e)
    {
        Control t = (Control)sender;
        DatePicker C_dp_ComprasInicio = null;
        DatePicker C_dp_ComprasFim = null;
        while (t != null && C_dp_ComprasInicio == null)
        {
            C_dp_ComprasInicio = (DatePicker)t.FindControl("C_dp_ActividadeInicio");
            C_dp_ComprasFim = (DatePicker)t.FindControl("C_dp_ActividadeFim");
            t = t.Parent;
        }
        if (C_dp_ComprasInicio != null && C_dp_ComprasFim != null)
        {
            string css = C_dp_ComprasInicio.TextCssClass;
            if (!(C_dp_ComprasInicio.IsClear || C_dp_ComprasFim.IsClear) && C_dp_ComprasInicio.SelectedDate > C_dp_ComprasFim.SelectedDate)
            {
                e.IsValid = false;
                if (!css.Contains("errorField"))
                {
                    css = css + " errorField";
                }
            }
            else
            {
                e.IsValid = true;
                css = css.Replace(" errorField", "");
            }
            C_dp_ComprasInicio.TextCssClass = css;
            C_dp_ComprasFim.TextCssClass = css;
        }
    }
    #endregion

    #region PESQUISA
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_TPR_val_Pesquisa_Empty_Validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = (this.C_TPR_txt_PesquisaCodigo.Text + this.C_TPR_txt_PesquisaNome.Text).Length > 0;
        if (e.IsValid)
        {
            this.C_TPR_txt_PesquisaCodigo.CssClass = this.C_TPR_txt_PesquisaCodigo.CssClass.Replace(" errorField", "");
            this.C_TPR_txt_PesquisaNome.CssClass = this.C_TPR_txt_PesquisaNome.CssClass.Replace(" errorField", "");
        }
        else
        {
            if (!this.C_TPR_txt_PesquisaCodigo.CssClass.Contains("errorField"))
            {
                this.C_TPR_txt_PesquisaCodigo.CssClass = this.C_TPR_txt_PesquisaCodigo.CssClass + " errorField";
            }
            if (!this.C_TPR_txt_PesquisaNome.CssClass.Contains("errorField"))
            {
                this.C_TPR_txt_PesquisaNome.CssClass = this.C_TPR_txt_PesquisaNome.CssClass + " errorField";
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_TPR_val_PesquisaCodigo_Validate(object sender, ServerValidateEventArgs e)
    {
        TextBox c = this.C_TPR_txt_PesquisaCodigo;
        int i = 0;
        if (!int.TryParse(e.Value, out i))
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }
        else
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_TPR_btn_Pesquisa_Click(object sender, EventArgs e)
    {
        try
        {
            if (this.IsValid)
            {
                int count = 0;
                AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
                DataTable prods = DB_AE.ProdutoSearch(this.C_TPR_txt_PesquisaCodigo.Text, this.C_TPR_txt_PesquisaNome.Text);

                foreach (DataRow dr in prods.Rows)
                {
                    foreach (TreeNode Agrup in this.C_TPR_Tree.Nodes[0].ChildNodes)
                    {
                        if (Agrup.ChildNodes.Count == 0)
                        {
                            Agrup.Expand();
                            Agrup.Collapse();
                        }
                        TreeNode GP = this.C_TPR_Tree.FindNode(Agrup.ValuePath + "/" + dr["COD_GRUPO_PRODUTO"].ToString());
                        if (GP != null)
                        {
                            if (GP.ChildNodes.Count == 0)
                            {
                                GP.Expand();
                                GP.Collapse();
                            }
                            TreeNode SGP = this.C_TPR_Tree.FindNode(GP.ValuePath + "/" + dr["COD_SUBGRUPO_PRODUTO"].ToString());
                            if (SGP != null)
                            {
                                if (SGP.ChildNodes.Count == 0)
                                {
                                    SGP.Expand();
                                    SGP.Collapse();
                                }
                                TreeNode SAP = this.C_TPR_Tree.FindNode(SGP.ValuePath + "/" + dr["COD_SUB_AGG_PRODUTO"].ToString());
                                if (SAP != null)
                                {
                                    if (SAP.ChildNodes.Count == 0)
                                    {
                                        SAP.Expand();
                                        SAP.Collapse();
                                    }
                                    TreeNode PR = this.C_TPR_Tree.FindNode(SAP.ValuePath + "/" + dr["ID_PRODUTO"].ToString());
                                    if (PR != null)
                                    {
                                        PR.Expand();
                                        //PR.Checked = true;
                                        SAP.Expand();
                                        SGP.Expand();
                                        GP.Expand();
                                        Agrup.Expand();
                                        ++count;
                                    }
                                }
                            }
                        }
                    }
                }
                if (count > 0)
                {
                    this.C_TPR_lbl_SearchResult.Text = "FORAM ENCONTRADAS " + count + " CORRESPOND�NCIAS.";
                }
                else
                {
                    this.C_TPR_lbl_SearchResult.Text = "N�O FORAM ENCONTRADAS CORRESPOND�NCIAS";
                }
            }
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL EFECTUAR A PESQUISA PRETENDIDA. TENTE NOVAMENTE", exp);            
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO", exp);
        }
    }
    #endregion

    #region �RVORE
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>    
    protected void C_TPR_btn_ReloadTree_Click(object source, EventArgs e)
    {
        this.C_TPR_Tree.Nodes.Clear();
        this.myPlaceHolder.Controls.Clear();

        TreeNode root = new TreeNode("�RVORE DE PRODUTOS", "");
        root.PopulateOnDemand = true;
        root.NavigateUrl = "";
        root.SelectAction = TreeNodeSelectAction.Expand;
        this.C_TPR_Tree.Nodes.Add(root);
        this.C_TPR_Tree.DataBind();
        root.Expand();

        this.Session["ActividadeInicio"] = null;
        this.Session["ActividadeFim"] = null;
        this.Session["ComprasInicio"] = null;
        this.Session["ComprasFim"] = null;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_TPR_Tree_Populate(object sender, TreeNodeEventArgs e)
    {
        switch (e.Node.Depth)
        {
            case 0: //ROOT
                this.LoadTreeAgrupamento(e);
                break;
            case 1: // AGRUPAMENTO
                this.LoadTreeGrupoProduto(e);
                break;
            case 2: // GRUPO PRODUTO
                this.LoadTreeSubGrupoProduto(e);
                break;
            case 3: // SUB GRUPO PRODUTO
                this.LoadTreeSubAggProduto(e);
                break;
            case 4: // SUB AGG PRODUTO
                this.LoadTreeProduto(e);
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_TPR_Tree_CheckChanged(object sender, TreeNodeEventArgs e)
    {
        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_TPR_TreeDetails_Apply(object source, EventArgs e)
    {
        if (this.IsValid)
        {
            ProductTreeDetails ptd = (ProductTreeDetails)source;
            this.ApplyDetails(ptd, ptd.SourceTreeNode);
        }
        else
        {
            string script = "<script type='text/Jscript'> alert('Existem valores inv�lidos. Nenhum Produto foi actualizado'); </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "start", script, false);
        }
        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_TPR_TreeDetails_Insert(object source, EventArgs e)
    {

        if (this.IsValid)
        {
            this.InsertTPR();
            this.C_TPR_btn_ReloadTree_Click(C_TPR_btn_ReloadTree, new EventArgs());
            Page.Validate("TPR");
        }
        else
        {
            string script = "<script type='text/Jscript'> alert('Existem produtos com valores inv�lidos. Nenhum Produto foi inserido'); </script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "start", script, false);
        }
    }
    #endregion
    #endregion

    #region DEBITO
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_Debito_val_txtValor_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        TextBox c = (TextBox)this.FindControl(cv.ControlToValidate);
        double i = 0;
        if (!double.TryParse(e.Value.Replace(".",","), out i) || i < 0 || i > 999999999.99)
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }
        else
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_Debito_dg_Lista_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label C_Debito_Total = (Label)e.Item.FindControl("C_Debito_Total");
            Label C_Debito_Residual = (Label)e.Item.FindControl("C_Debito_Residual");
            double val = 0;
            double ActividadeVal = 0;
            foreach (DataRowView dr in this.T_AE_DEBITO.DefaultView)
            {
                val += double.Parse(dr["VALOR_DEBITO"].ToString());
            }
            C_Debito_Total.Text = val.ToString("#0.00");

            foreach (DataRowView dr in this.T_AE_ACTIVIDADE.DefaultView)
            {
                ActividadeVal += double.Parse(dr["VALOR_ACORDO"].ToString());
            }
            C_Debito_Residual.Text = (ActividadeVal - val).ToString("#0.00");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Debito_dg_Lista_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteDebito(e.Item.ItemIndex);
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Debito_Add_ItemCommand(object source, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Inserir":
                this.InsertDebito();
                Page.Validate("Debido");
                break;
            default:
                break;
        }
    }
    #endregion

    #region LOG
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_dg_Log_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (this.C_dg_Log.Attributes["CanUse"] == "false")
            return;
        switch (e.CommandName)
        {
            case "Delete":
                this.DeleteLog((this.C_dg_Log.CurrentPageIndex * this.C_dg_Log.PageSize) + e.Item.ItemIndex);
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_dg_Log_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView dr = (DataRowView)e.Item.DataItem;
            ImageButton C_btn_Log_Delete = (ImageButton)e.Item.FindControl("C_btn_Log_Delete");
            C_btn_Log_Delete.Visible = dr["ID_CONTRATO"].ToString() == "0" && this.C_dg_Log.Attributes["CanUse"]=="true";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_dg_Log_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        this.C_dg_Log.CurrentPageIndex = e.NewPageIndex;
        this.LoadLog();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Log_btn_Paginacao_Click(object source, EventArgs e)
    {
        this.C_dg_Log.AllowPaging = !this.C_dg_Log.AllowPaging;
        this.LoadLog();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Log_Add_ItemCommand(object source, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Inserir":
                this.InsertLog();
                break;
            default:
                break;
        }
    }

    #endregion

    #region BOT�ES
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>    
    protected void C_btn_Print_Click(object sender, System.EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "redirecionamento", "window.open('PrintAE.aspx?idContrato=" + this.Request["idContrato"].ToString() + "', 'new_window', 'toolbar=no, menubar=no, scrollbars=yes, resizable=yes ');", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_btn_New_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/contratoAE.aspx", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_btn_Insert_Click(object sender, System.EventArgs e)
    {
        int IDContrato = 0;
        string scriptAlert = "<script type='text/Jscript'> alert('[MESSAGE]');</script>";
        string scriptMessage = "<script type='text/Jscript'> alert('[MESSAGE]'); navigate('contratoAE.aspx?idContrato=[ID]');</script>";
        try
        {
            if (!this.ContratoValido())
            {
                this._M_ShowError(this.lblWarning, "O CONTRATO N�O EST� PREENCHIDO CORRECTAMENTE.", null);
                return;
            }            
            string NumContrato = this.InserirContrato(ref IDContrato);
            if (NumContrato.Equals(""))
                return;
            string error = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", scriptMessage.Replace("[MESSAGE]", "O CONTRATO FOI INSERIDO COM O N� " + NumContrato + ".     " + error).Replace("[ID]", IDContrato.ToString()), false);
        }
        catch (CodeExistsException exp)
        {
            this._M_ShowError(this.lblError, "O N� DE PROPOSTA INDICADO J� EXISTE.", exp);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", scriptAlert.Replace("[MESSAGE]", "O N� DE PROPOSTA INDICADO J� EXISTE."), false);
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL INSERIR O CONTRATO NA BASE DE DADOS.", exp);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO", exp);
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_btn_Save_Click(object sender, System.EventArgs e)
    {
        string scriptMessage = "<script type='text/Jscript'> alert('[MESSAGE]'); navigate('contratoAE.aspx?idContrato=[ID]');</script>";
        try
        {            
            if (!this.ContratoValido())
            {
                this._M_ShowError(this.lblWarning, "O CONTRATO N�O EST� PREENCHIDO CORRECTAMENTE.", null);
                return;
            }
            int IDContrato = 0;
            if (this.is_Editing)
            {
                IDContrato = int.Parse(this.Request["idContrato"]);
                string NumContrato = this.ActualizarContrato(IDContrato).ToString();
                if (NumContrato.Equals(""))
                    return;
                string error = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", scriptMessage.Replace("[MESSAGE]", "O CONTRATO COM O N� " + NumContrato + " FOI ACTUALIZADO.    " + error).Replace("[ID]", IDContrato.ToString()), false);
                this.is_Editing = false;
            }
            else
            {
                string NumContrato = this.InserirContrato(ref IDContrato).ToString();
                if (NumContrato.Equals(""))
                    return;
                string error = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", scriptMessage.Replace("[MESSAGE]", "O CONTRATO FOI INSERIDO COM O N� " + NumContrato + ".    " + error).Replace("[ID]", IDContrato.ToString()), false);
            }
        }
        catch (CodeExistsException exp)
        {
            this._M_ShowError(this.lblError, "O N� DE PROPOSTA INDICADO J� EXISTE.", exp);
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL GUARDAR O CONTRATO NA BASE DE DADOS.", exp);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_btn_Edit_Click(object sender, System.EventArgs e)
    {
        try
        {
            this.is_DBContrato = false;
            this.is_Editing = true;

            this.RefreshState();

            this.C_btn_Insert.Visible = false;
            this.C_btn_Editar.Visible = false;
            this.C_btn_Save.Visible = true;

            this.InsertLog("EDITAR CONTRATO");
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL PROCEDER � ALTERA��O DO CONTRATO. TENTE NOVAMENTE", exp);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>    
    protected void C_btn_Seguinte_Click(object sender, System.EventArgs e)
    {
        if (this.IsValid)
        {
            this.C_box_Recepcao.Visible = true;
            this.C_box_Nivel.Visible = true;
            this.C_box_Pesquisa.Visible = true;
            this.C_box_Entidade.Visible = true;
            this.C_box_PontoVenda.Visible = true;
            this.C_box_Abastecimento.Visible = true;
            this.C_box_Actividade.Visible = true;
            this.C_box_TPR.Visible = true;
            this.C_box_Debito.Visible = true;
            this.C_box_Observacoes.Visible = true;
            this.C_box_Log.Visible = true;
            this.C_btn_Insert.Visible = true;
            this.C_btn_Seguinte.Visible = false;

            this.ContratoEnabled(true);
            this.NivelEnabled(true);
            this.PesquisaEnabled(true);
            this.EntidadesEnabled(true);
            this.PontoVendaEnabled(true);
            this.AbastecimentoEnabled(true);
            this.ActividadeEnabled(true);
            this.TPREnabled(true);
            this.DebitoEnabled(true);
            this.ObservacoesEnabled(true);
            this.LogEnabled(true);
            
            this.C_cb_Nivel_Changed(this.C_cb_Nivel_LE, new EventArgs());
            this.LoadClientes();
            this.LoadAbastecimento();
            this.LoadActividade();
            this.LoadTPR();
            this.LoadDebito();
            this.LoadLog();
        }
    }
    
    #endregion
    #endregion
}