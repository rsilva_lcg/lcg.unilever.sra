﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SRA_BulkManager.aspx.cs" Inherits="SpiritucSI.Unilever.SRA.SRA_BulkManager" meta:resourcekey="Page" Theme="SRA" %>

<%@ Register Src = "~/UserControls/ucBulkManager.ascx" TagName="ucBulk" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
<%--onsubmit="javascript: return MySubmit();"--%>
    <form id="Form1" method="post"  runat="server">
            <ContentTemplate>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="boxTitulo" runat="server" CssClass="box" meta:resourcekey="boxTituloResource1">
                <UC:Header ID="Header1" runat="server" Title="CONTRATOS DE COOPERAÇÃO COMERCIAL"></UC:Header>
            </asp:Panel>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <asp:Panel ID="boxError" runat="server" CssClass="box" meta:resourcekey="boxErrorResource1">
                <table class="table" align="center">
                    <tr>
                        <td valign="middle" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="errorLabel" meta:resourcekey="lblErrorResource1"></asp:Label><br>
                            <asp:Label ID="lblWarning" runat="server" CssClass="warningLabel" meta:resourcekey="lblWarningResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%-- DASHBOARD  #################################################################### --%>
            <asp:Panel ID="boxDashboard" runat="server" CssClass="box">
                <table class="table" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td align="center">
                            <uc1:ucBulk ID ="ucBulk" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="boxFooter" runat="server" CssClass="box" meta:resourcekey="boxFooterResource1">
                <UC:Footer ID="Footer1" runat="server"></UC:Footer>
            </asp:Panel>
        </ContentTemplate>
    </form>
</body>
</html>
