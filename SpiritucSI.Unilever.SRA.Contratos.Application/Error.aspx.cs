using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.DirectoryServices;
using System.IO;


public partial class Error : System.Web.UI.Page
{
   
    #region PAGE EVENTS
        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            //ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), "sub", "MySubmit();");
            Exception ex = (Exception)this.Session["Error"];
            if (ex != null)
            {
                this.lblError.Text = "<br><br> O ERRO FOI REGISTADO PARA POSTERIOR AN�LISE.<br>";
            }
            else
            {
                this.lblError.Text = "Erro Desconhecido.";
            }
        }
    #endregion
}

