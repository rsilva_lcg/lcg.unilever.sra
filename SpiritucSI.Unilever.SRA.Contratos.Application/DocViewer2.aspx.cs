﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class DocViewer2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["path"] == null)
        {
            Response.Write("Erro a passar parâmetros.");
            return;
        }

        //Get the physical path to the file.
        //string FilePath = MapPath("Files/teste.pdf");
        string FilePath = Request.QueryString["path"].ToString();
        FileStream fs = File.OpenRead(FilePath);
        FileInfo tempFile = new  FileInfo(FilePath);
        Response.ContentType = "application/octet-stream";
        //Response.ContentType = "application/unknown"; //application/octet-stream
        Response.AppendHeader("Content-Disposition",
               "inline; " +
               "filename=\"" + tempFile.Name + "\"; " +
               "size=" + fs.Length.ToString() + "; " +
               "creation-date=" + DateTime.Now.ToString("R") + "; " +
               "modification-date=" + DateTime.Now.ToString("R") + "; " +
               "read-date=" + DateTime.Now.ToString("R") + "; ");

        Response.WriteFile(fs.Name);
        Response.End();
    }
}
