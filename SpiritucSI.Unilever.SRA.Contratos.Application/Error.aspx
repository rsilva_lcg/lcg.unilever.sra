<%@ Page Language="c#" CodeFile="Error.aspx.cs" AutoEventWireup="True" Inherits="Error" meta:resourcekey="PageResource1" Theme="SRA" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contratos - LOGIN</title>
</head>
<body>
    <form id="Form1" method="post" runat="server" onsubmit="javascript: return MySubmit();">

    <script language="javascript" type="text/javascript" src="Scripts/SRAScript.js"></script>

    <%-- JANELA DE PROCESSAMENTO  ########################################################################## --%>
    <AJAX:ScriptManager ID="ScriptManager1" runat="server">
    </AJAX:ScriptManager>
    <AJAX:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px; position: absolute;
                left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF; z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </AJAX:UpdateProgress>
    <%-- CONTE�DO ########################################################################################## --%>
    <AJAX:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="Box_Header" runat="server" CssClass="box" meta:resourcekey="Box_HeaderResource1">
                <UC:Header ID="Header1" runat="server"></UC:Header>
            </asp:Panel>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="box_Content" runat="server" CssClass="box" meta:resourcekey="box_ContentResource1">
                <br>
                <table class="table">
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblInfo" runat="server" CssClass="titulo_peq" meta:resourcekey="lblInfoResource1">Ocorreu um erro na aplica��o:</asp:Label>
                        </td>
                    </tr>
                    <tr style="border: solid 1px blue">
                        <td align="center">
                            <br />
                            <asp:Label ID="lblError" runat="server" CssClass="textValidator" meta:resourcekey="lblErrorResource1"></asp:Label>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <br>
                            <br>
                            <label class="txtNormal">
                                Para voltar ao menu inicial prima
                            </label>
                            <a class="button" href="default.aspx">AQUI</a>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%-- FOOTER  ################################################################################### --%>
            <asp:Panel ID="boxFooter" runat="server" CssClass="box" meta:resourcekey="boxFooterResource1">
                <table class="table">
                    <tr>
                        <td align="center" colspan="2">
                            <UC:Footer ID="Footer1" runat="server"></UC:Footer>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </AJAX:UpdatePanel>
    </form>
</body>
</html>
