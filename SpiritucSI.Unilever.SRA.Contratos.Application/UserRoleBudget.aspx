﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserRoleBudget.aspx.cs" Inherits="UserRoleBudget"
    Theme="SRA" %>

<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnablePartialRendering="true"
        AsyncPostBackTimeout="60000">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px; position: absolute;
                left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF; z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="boxTitulo" runat="server" CssClass="box" meta:resourcekey="boxTituloResource1">
                <UC:Header ID="Header1" runat="server"></UC:Header>
            </asp:Panel>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <asp:Panel ID="boxError" runat="server" CssClass="box" meta:resourcekey="boxErrorResource1">
                <table class="table" align="center">
                    <tr>
                        <td valign="middle" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="errorLabel" meta:resourcekey="lblErrorResource1"></asp:Label><br>
                            <asp:Label ID="lblWarning" runat="server" CssClass="warningLabel" meta:resourcekey="lblWarningResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table width="60%">
                <tr>
                    <td align="center">
                        <br />
                        <br />
                        <div id="List" runat="server" class="SubContentCenterTable">
                            <asp:UpdatePanel ID="up2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: left; width: 100%">
                                        <asp:DataGrid ID="C_dg_Entidades" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                            BorderStyle="None" GridLines="None" CellSpacing="2" OnItemCommand="C_dg_Entidades_ItemCommand" OnPageIndexChanged="C_dg_Entidades_PageIndexChanged"
                                            ShowFooter="true" OnSortCommand="C_dg_Entidades_SortCommand" OnItemDataBound="Entidade_ItemDataBound" PageSize="15" Width="100%">
                                            <HeaderStyle ForeColor="#00008B" BackColor="#99CCFF" Height="20px"/>
                                            <ItemStyle CssClass="ListRow" />
                                            <AlternatingItemStyle CssClass="AlternateListRow" />
                                            <PagerStyle ForeColor="#00008B" BackColor="Transparent" HorizontalAlign="Left" Mode="NumericPages" />
                                            <Columns>
                                                <asp:TemplateColumn>
                                                    <HeaderStyle ForeColor="Transparent" BackColor="Transparent" Width="1%" />
                                                    <ItemStyle VerticalAlign="Middle" BackColor="Transparent" HorizontalAlign="Right" Width="1%" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="C_btn_dgEntidades_Edit" runat="server" CommandName="Edit" CausesValidation="false"
                                                            ImageUrl="~/images/Pen.ico" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:ImageButton ID="C_btn_dgEntidades_Save" runat="server" CommandName="Save" CausesValidation="true"
                                                                        ImageUrl="~/images/Save.ico" ValidationGroup="Entidade_EDIT"/>
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton ID="C_btn_dgEntidades_Undo" runat="server" CommandName="Undo" CausesValidation="false"
                                                                        ImageUrl="~/images/Undo.ico" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn Visible="false">
                                                    <ItemTemplate>
                                                        <asp:label CssClass="textbox" ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BudgetMatrixId")%>'></asp:label>
                                                    </ItemTemplate>
                                                    <editItemTemplate>
                                                        <asp:label CssClass="textbox" ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BudgetMatrixId")%>'></asp:label>
                                                    </editItemTemplate> 
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Descrição" SortExpression="RoleName">
                                                    <ItemTemplate>
                                                        <%# GetZoneDescription(DataBinder.Eval(Container.DataItem, "RoleName").ToString()) %>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Panel ID="Panel_Entidade" runat="server" DefaultButton="Entidade_lnkInserir">
                                                            <asp:DropDownList ID="Entidade_lb" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEntidade_Change"
                                                                CssClass="txtNormal" Width="100%"  ValidationGroup="Entidade">
                                                            </asp:DropDownList>
                                                        </asp:Panel>
                                                     </FooterTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="ANO" SortExpression="YEAR">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "YEAR")%>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:label CssClass="textbox" ID="lbl1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "YEAR")%>'></asp:label>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Panel ID="Panel_Entidade_Ano" runat="server" DefaultButton="Entidade_lnkInserir">
                                                            <asp:TextBox ID="Entidade_txtAno" runat="server" CssClass="txtNormal" MaxLength="4" Style="text-align: right"
                                                                Width="100%"  ValidationGroup="Entidade"></asp:TextBox>
                                                            <asp:RangeValidator  ValidationGroup="Entidade" ID="Entidade_Val_txtAno" runat="server" ControlToValidate="Entidade_txtAno"
                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="true" ErrorMessage="Valor Inválido. {2000-2050}"
                                                                MaximumValue="2050" MinimumValue="2000" Type="Integer">Valor Inválido.<br>{2000-2050}</br>
                                                            </asp:RangeValidator>
                                                            <asp:RequiredFieldValidator  ValidationGroup="Entidade" ID="Material_Val_ReqtxtAno" runat="server" ControlToValidate="Entidade_txtAno"
                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="True" ErrorMessage="Indique um ano">Indique um ano</asp:RequiredFieldValidator>
                                                        </asp:Panel>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Máx. Invest. (%)" SortExpression="Invest_MaxPerc">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "Invest_MaxPerc")%>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox CssClass="textbox" ID="txb2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Invest_MaxPerc")%>'></asp:TextBox>
                                                        <asp:RangeValidator  ValidationGroup="Entidade_EDIT" ID="Entidade_Val_txb2" runat="server" ControlToValidate="txb2"
                                                            CssClass="textValidator" Display="Dynamic" EnableClientScript="true" ErrorMessage="Valor Inválido."
                                                            MaximumValue="1000000" MinimumValue="0" Type="Double">Valor Inválido.</asp:RangeValidator>
                                                        <asp:RequiredFieldValidator  ValidationGroup="Entidade_EDIT" ID="Material_Val_Reqtxb2" runat="server" ControlToValidate="txb2"
                                                            CssClass="textValidator" Display="Dynamic" EnableClientScript="True" ErrorMessage="Indique um valor">Indique um valor</asp:RequiredFieldValidator>                                                   
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Panel ID="Panel_Entidade_maxPERC" runat="server" DefaultButton="Entidade_lnkInserir">
                                                            <asp:TextBox ID="txb_maxPERC" runat="server" CssClass="txtNormal" MaxLength="2" Style="text-align: right"
                                                                Width="100%"  ValidationGroup="Entidade"></asp:TextBox>
                                                            <asp:RangeValidator  ValidationGroup="Entidade" ID="Entidade_Val_maxPERC" runat="server" ControlToValidate="txb_maxPERC"
                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="true" ErrorMessage="Valor Inválido."
                                                                MaximumValue="100" MinimumValue="0" Type="Double">Valor Inválido.</asp:RangeValidator>
                                                            <asp:RequiredFieldValidator  ValidationGroup="Entidade" ID="Material_Val_ReqmaxPERC" runat="server" ControlToValidate="txb_maxPERC"
                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="True" ErrorMessage="Indique um valor">Indique um valor</asp:RequiredFieldValidator>
                                                        </asp:Panel>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Máx. Invest. Médio Anual" SortExpression="ContractGSV_Max">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "ContractGSV_Max")%>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox CssClass="textbox" ID="txb3" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ContractGSV_Max")%>'></asp:TextBox>
                                                        <asp:RangeValidator  ValidationGroup="Entidade_EDIT" ID="Entidade_Val_txb3" runat="server" ControlToValidate="txb3"
                                                            CssClass="textValidator" Display="Dynamic" EnableClientScript="true" ErrorMessage="Valor Inválido."
                                                            MaximumValue="1000000" MinimumValue="0" Type="Double">Valor Inválido.</asp:RangeValidator>
                                                        <asp:RequiredFieldValidator  ValidationGroup="Entidade_EDIT" ID="Material_Val_Reqtxb3" runat="server" ControlToValidate="txb3"
                                                            CssClass="textValidator" Display="Dynamic" EnableClientScript="True" ErrorMessage="Indique um valor">Indique um valor</asp:RequiredFieldValidator>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Panel ID="Panel_Entidade_contMAX" runat="server" DefaultButton="Entidade_lnkInserir">
                                                            <asp:TextBox ID="txb_contMAX" runat="server" CssClass="txtNormal" MaxLength="10" Style="text-align: right"
                                                                Width="100%"  ValidationGroup="Entidade"></asp:TextBox>
                                                            <asp:RangeValidator  ValidationGroup="Entidade" ID="Entidade_Val_contMAX" runat="server" ControlToValidate="txb_contMAX"
                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="true" ErrorMessage="Valor Inválido."
                                                                MaximumValue="1000000" MinimumValue="0" Type="Double">Valor Inválido.</asp:RangeValidator>
                                                            <asp:RequiredFieldValidator  ValidationGroup="Entidade" ID="Material_Val_ReqcontMAX" runat="server" ControlToValidate="txb_contMAX"
                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="True" ErrorMessage="Indique um valor">Indique um valor</asp:RequiredFieldValidator>
                                                        </asp:Panel>
                                                    </FooterTemplate>                                                    
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Duração (anos)" SortExpression="ContractDuration">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "ContractDuration")%>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox CssClass="textbox" ID="txb4" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ContractDuration")%>'></asp:TextBox>
                                                        <asp:RangeValidator  ValidationGroup="Entidade_EDIT" ID="Entidade_Val_txb4" runat="server" ControlToValidate="txb4"
                                                            CssClass="textValidator" Display="Dynamic" EnableClientScript="true" ErrorMessage="Valor Inválido."
                                                            MaximumValue="1000000" MinimumValue="0" Type="Double">Valor Inválido.</asp:RangeValidator>
                                                        <asp:RequiredFieldValidator  ValidationGroup="Entidade_EDIT" ID="Material_Val_Reqtxb4" runat="server" ControlToValidate="txb4"
                                                            CssClass="textValidator" Display="Dynamic" EnableClientScript="True" ErrorMessage="Indique um valor">Indique um valor</asp:RequiredFieldValidator>                                                                                                                                                           
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Panel ID="Panel_Entidade_contDUR" runat="server" DefaultButton="Entidade_lnkInserir">
                                                            <asp:TextBox ID="txb_contDUR" runat="server" CssClass="txtNormal" MaxLength="1" Style="text-align: right"
                                                                Width="100%"  ValidationGroup="Entidade"></asp:TextBox>
                                                            <asp:RangeValidator  ValidationGroup="Entidade" ID="Entidade_Val_contDUR" runat="server" ControlToValidate="txb_contDUR"
                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="true" ErrorMessage="Valor Inválido."
                                                                MaximumValue="100" MinimumValue="0" Type="Double">Valor Inválido.</asp:RangeValidator>
                                                            <asp:RequiredFieldValidator  ValidationGroup="Entidade" ID="Material_Val_ReqcontDUR" runat="server" ControlToValidate="txb_contDUR"
                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="True" ErrorMessage="Indique um valor">Indique um valor</asp:RequiredFieldValidator>
                                                        </asp:Panel>
                                                    </FooterTemplate>                                                    
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                    <ItemStyle VerticalAlign="Middle" BackColor="Transparent" HorizontalAlign="Right" Width="1%" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="C_btn_dgClientes_Delete" runat="server" CommandName="Delete" CausesValidation="false"
                                                            ImageUrl="~/images/Delete.ico" OnClientClick="javascript: return confirm('Deseja mesmo apagar?');"/>&nbsp;&nbsp;
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <table>
                                                            <tr>
                                                                <td valign="middle">
                                                                    <asp:ImageButton ID="Entidade_lnkInserir" runat="server" CommandName="Inserir" CssClass="button" 
                                                                        ImageUrl="~/images/Plus_Small.ico" ToolTip="Inserir"  ValidationGroup="Entidade"/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                </tr>
            </table>
            <%-- FOOTER  ################################################################################### --%>
            <asp:Panel ID="boxFooter" runat="server" CssClass="box" meta:resourcekey="boxFooterResource1">
                <UC:Footer ID="Footer1" runat="server"></UC:Footer>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
