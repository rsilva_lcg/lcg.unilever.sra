﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class ResetWF : SuperPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Btt_click(object sender, EventArgs e)
    {
        SRADBInterface.SRA DBsra = new SRADBInterface.SRA(ConfigurationManager.AppSettings["DBCS"]);
        string query = " INSTANCE_ID IS NOT NULL";
        
        DataTable dt = DBsra.getContrato(query);

        int count = 0;
        foreach (DataRow r in dt.Rows)
        {
            //int id = 0;
            //Guid idWf;
            //if (Int32.TryParse(r["ID_CONTRATO"].ToString(), out id))
            //{
            //    idWf = this.RunWorkflow(id);
            //    count++;
            //}
                int id = 0;
                if (Int32.TryParse(r["ID_CONTRATO"].ToString(), out id) && DBsra.getContrato(id).Tables[0].Rows.Count > 0)
                {
                    var state = DBsra.getContrato(id).Tables[0].Rows[0]["INSTANCE_STATE"].ToString();
                    Guid idWf;
                    //idWf = this.RunWorkflow(id);
                    if (state != "")
                        idWf = this.RunWorkflow(id, state);
                    else
                        idWf = this.RunWorkflow(id);
                    count++;
                }
            }
                if (count == dt.Rows.Count)
                {
                    this.lbl.Visible = true;
                    this.lbl.DataBind();
                }

    }

    protected void Btt_One_click(object sender, EventArgs e)
    {
        SRADBInterface.SRA DBsra = new SRADBInterface.SRA(ConfigurationManager.AppSettings["DBCS"]);

        
        //int id = 0;
        //if (Txt_ID_CONTRACTO.Text != "" && Int32.TryParse(Txt_ID_CONTRACTO.Text, out id) && DBsra.getContrato(id).Tables[0].Rows.Count > 0)
        if (Txt_ID_CONTRACTO.Text != "")
        {
            try {
                List<string> conts = new List<string>();
                string txt = Txt_ID_CONTRACTO.Text;
                if (txt.Contains(';'))
                    conts = txt.Split(';').ToList();
                else
                    conts.Add(txt);
                int count = 0;
                //string[] sts ={"Draft", "RenewalDraft", "Printed", "Delegated", "Accepted", "Received", "InApproval", "ManagerApproval", "SalesApproval", "FinantialApproval", "TopApproval", "Approved", "Rejected"};
                foreach (string c in conts)
                {
                    int id = 0;
                    if (Int32.TryParse(c, out id) && DBsra.getContrato(id).Tables[0].Rows.Count > 0)
                    {
                        var state = DBsra.getContrato(id).Tables[0].Rows[0]["INSTANCE_STATE"].ToString();
                        Guid idWf;
                        //if(count <= sts.Length)
                        //    state = sts[count].ToUpper();
                        //idWf = this.RunWorkflow(id);
                        if (state != "")
                            idWf = this.RunWorkflow(id,state);
                        else
                            idWf = this.RunWorkflow(id);
                    }
                    count++;
                }
            this.lbl.Visible = true;
            this.lbl.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

}
