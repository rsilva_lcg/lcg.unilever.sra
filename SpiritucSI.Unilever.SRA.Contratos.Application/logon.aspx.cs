using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.DirectoryServices;
using System.Collections.Specialized;
using System.Globalization;
using System.Net.Mail;
using System.Configuration;
using System.Web.Security;

public partial class logon : System.Web.UI.Page
{
    #region METHODS
    /// <summary>
    /// APRESENTA UM ERRO/WARNING COM A MENSSAGEM INDICADA
    /// </summary>
    /// <param name="Destiny">LABEL DE DESTINO DA MENSSAGEM</param>
    /// <param name="Message">MENSSAGEM A APRESENTAR</param>
    /// <param name="e">EXCEP��O QUE DEU ORIGEM AO ERRO</param>
    private void _M_ShowError(WebControl Destiny, string Message, Exception e)
    {
        this.MaintainScrollPositionOnPostBack = false;
        ((Label)Destiny).Text = Message;
        Exception ex = e;
        while (ex != null)
        {
            Destiny.ToolTip += ex.Message + "\n";
            ex = ex.InnerException;
        }
    }
    #endregion

    #region PAGE EVENTS
    protected void Page_Load(object sender, EventArgs e)
    {
        //this.lblInfo.Text = "";
        //this.lblInfo.ToolTip = "";
        this.MaintainScrollPositionOnPostBack = true;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), "sub", "MySubmit();");
#if DEBUG
        this.Login2.UserName = "Consultor";
        //this.Login2.Password = "Unilever123!";
#endif
    }
    #endregion

    #region CONTROL EVENTS
        //protected void btSubmit_Click(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        //Utilizador user = new Utilizador(txtUser.Text, txtPassword.Text, txtDomain.Text);


        //        /// TODO: APAGAR - Usado apenas para desenvolvimento ######################
        //        Utilizador user = new Utilizador("Consultor", "Unilever123!", txtDomain.Text);
        //        /// ################################################################
                
                

        //        if (user.IsValidated)
        //        {
        //            ScriptManager.RegisterStartupScript(Page, typeof(Page), "redirecionamento", "window.location.href='./default.aspx';", true);
        //            return;
        //        }
        //        else
        //        {
        //            this._M_ShowError(this.lblInfo, "Utilizador Inv�lidado", null);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this._M_ShowError(this.lblInfo, "Autentica��o Inv�lida.", ex);
        //    }
        //}
    #endregion

}

