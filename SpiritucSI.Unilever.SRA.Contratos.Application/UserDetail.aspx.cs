﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SpiritucSI.Unilever.SRA.Business.Budget;

public partial class UserDetail : SuperPage
{
    #region PROPERTIES
    public Guid UserId
    {
        get
        {
            try
            {
                if (this.Request["uid"] != null)
                {
                    return new Guid(this.Request["uid"]);
                }
            }
            catch (Exception ex)
            {
                return Guid.Empty;
            }

            return Guid.Empty;
        }
    }
    public string UserName
    {
        get
        {
            try
            {
                if (this.Request["uname"] != null)
                {
                    return this.Request["uname"].ToString();
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

            return string.Empty;
        }
    }
    #endregion

    #region PAGE_EVENTS
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            this.lblError.Text = "";
            this.lblWarning.Text = "";
            this.lblError.ToolTip = "";
            this.lblWarning.ToolTip = "";
        }
        else
        {
            this.LoadCheckBoxListFunctionRoles();
            this.LoadCheckBoxListOrganizationRoles();

            this.txtUsername.Text = this.UserName;
            if (this.UserId == Guid.Empty)  //NEW USER
            {
                txtUsername.Enabled = true;
            }
            else                            //UPDATE USER
            {
                txtUsername.Enabled = false;
                this.Filldata();
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        this.Save(((Button)sender).CommandName == "Update", false);
    }
    #endregion

    #region METHODS
    /// <summary>
    /// Fill the data for the product.
    /// </summary>
    private void Filldata()
    {
        //SÓ SE APROVEITA O EMAIL! NÃO HÁ "NOME"???
        MembershipUser userFromAD = Membership.GetUser(this.UserName);
        if (userFromAD != null)
        {
            this.txtEmail.Text = userFromAD.Email;
            this.txtANotes.Text = userFromAD.Comment;
            this.chkApproved.Checked = userFromAD.IsApproved;
            this.chkBlocked.Checked = userFromAD.IsLockedOut;
        }

        foreach (ListItem li in this.ckblOrganizationRoles.Items)
        {
            if (Roles.GetRolesForUser(this.UserName).Any(x => x.ToString() == li.Value))
            {
                li.Selected = true;
            }
        }

        foreach (ListItem li in this.ckblFunctionRoles.Items)
        {
            if (Roles.GetRolesForUser(this.UserName).Any(x => x.ToString() == li.Value))
            {
                li.Selected = true;
            }
        }

        this.btnSave.CommandName = "Update";        
    }

    /// <summary>
    /// Saves the specified update.
    /// </summary>
    /// <param name="update">if set to <c>true</c> [update].</param>
    /// <param name="sendToFillPage">if set to <c>true</c> [send to fill page].</param>
    private void Save(bool update, bool sendToFillPage)
    {
        MembershipUser userFromAD = Membership.GetUser(txtUsername.Text.Trim());
        if (userFromAD != null)
        {
            foreach (ListItem cb in ckblFunctionRoles.Items)
            {
                if (cb.Selected)
                {
                    if (!Roles.IsUserInRole(txtUsername.Text.Trim(), cb.Value))
                    {
                        Roles.AddUserToRole(txtUsername.Text.Trim(), cb.Value);
                    }
                }
                else
                {
                    if (Roles.IsUserInRole(txtUsername.Text.Trim(), cb.Value))
                    {
                        Roles.RemoveUserFromRole(txtUsername.Text.Trim(), cb.Value);
                    }
                }
            }
            foreach (ListItem cb in ckblOrganizationRoles.Items)
            {
                if (cb.Selected)
                {
                    if (!Roles.IsUserInRole(txtUsername.Text.Trim(), cb.Value))
                    {
                        Roles.AddUserToRole(txtUsername.Text.Trim(), cb.Value);
                    }
                }
                else
                {
                    if (Roles.IsUserInRole(txtUsername.Text.Trim(), cb.Value))
                    {
                        Roles.RemoveUserFromRole(txtUsername.Text.Trim(), cb.Value);
                    }
                }
            }
            
            Response.Redirect("~/UserList.aspx");
        }
        else
        {
            lblError.Text = "Erro ao obter utilizador. Não existe na AD.";
        }
    }

    /// <summary>
    /// Loads the check box list function roles.
    /// </summary>
    private void LoadCheckBoxListFunctionRoles()
    {
        MyMembership m = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        this.ckblFunctionRoles.DataSource = m.GetFunctionalRoles(Roles.GetAllRoles());
        this.ckblFunctionRoles.DataBind();
    }

    /// <summary>
    /// Loads the check box list organization roles.
    /// </summary>
    private void LoadCheckBoxListOrganizationRoles()
    {
        MyMembership m = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        this.ckblOrganizationRoles.DataTextField = "Description";
        this.ckblOrganizationRoles.DataValueField = "Name";
        DataTable roleDescriptions = m.Roles_Get();
        DataTable source = m.GetOrganizationalRoles(Roles.GetAllRoles(), roleDescriptions);
        source.DefaultView.Sort = "Description ASC";
        this.ckblOrganizationRoles.DataSource = source;
        this.ckblOrganizationRoles.DataBind();
    }
    #endregion
}


//ClientValidationFunction="checkBoxClickedFunctionRoles" 

//<script type="text/javascript">
//    function checkBoxClickedFunctionRoles(source, args) {
//        var count = 0;
//        var els = document.getElementsByTagName('input');
//        for (var i = 0; i < els.length; ++i) {
//            var e = els[i];
//            if (e.type == 'checkbox'
//                && e.name.indexOf('<%=ckblFunctionRoles.UniqueID%>') > -1
//                && e.checked) {
//                ++count;
//            }
//        }
//        args.IsValid = (count > 0);
//    }
//    function checkBoxClickedOrganizationRoles(source, args) {
//        var count = 0;
//        var els = document.getElementsByTagName('input');
//        for (var i = 0; i < els.length; ++i) {
//            var e = els[i];
//            if (e.type == 'checkbox'
//                && e.name.indexOf('<%=ckblOrganizationRoles.UniqueID%>') > -1
//                && e.checked) {
//                ++count;
//            }
//        }
//        args.IsValid = (count > 0);
//    }       
//</script>