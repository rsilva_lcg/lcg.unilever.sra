<%@ Page Language="c#" CodeFile="contratoAE.aspx.cs" AutoEventWireup="true" EnableEventValidation="true"
    Inherits="contratoAE" Theme="SRA" %>

<%@ Register TagPrefix="UC" TagName="ProductTreeDetails" Src="~/UserControls/ProductTreeDetails.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contratos - Formul�rio</title>
</head>
<body>
    <form id="Form1" method="post" runat="server" onsubmit="javascript: return MySubmit();">

    <script language="javascript" type="text/javascript" src="Scripts/SRAScript.js"></script>

    <%-- JANELA DE PROCESSAMENTO  ########################################################################## --%>
    <AJAX:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </AJAX:ScriptManager>
    <AJAX:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="400" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px; position: absolute;
                left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF; z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </AJAX:UpdateProgress>
    <%-- CONTE�DO ########################################################################################## --%>
    <AJAX:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:LinkButton ID="Dummy" runat="server"></asp:LinkButton>
            <asp:PlaceHolder ID="myPlaceHolder" runat="server" />
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="Box_Header" runat="server" CssClass="box" DefaultButton="Dummy">
                <UC:Header ID="Header1" runat="server" meta:resourcekey="Header1"></UC:Header>
            </asp:Panel>
            <%-- TITULO  ################################################################################### --%>
            <asp:Panel ID="C_box_Titulo" runat="server" CssClass="box" DefaultButton="Dummy">
                <table class="table" align="center">
                    <tr>
                        <%-- BACK --%>
                        <td>
                            <UC:ImageTextButton ID="C_btn_Menu" runat="server" meta:resourcekey="C_btn_Menu" TextCssClass="button"
                                TextPosition="Bottom" CausesValidation="false" PostBackUrl="~/ae.aspx" />
                        </td>
                        <%-- TITULO --%>
                        <td align="center" width="100%">
                            <asp:Label ID="lbl_SubTitulo" runat="server" CssClass="tituloGrande"></asp:Label>
                        </td>
                        <%-- PRINT --%>
                        <td align="center">
                            <UC:ImageTextButton ID="C_btn_Print" runat="server" meta:resourcekey="C_btn_Print" TextCssClass="button"
                                TextPosition="Bottom" CausesValidation="false" OnClick="C_btn_Print_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <p>
                <asp:Panel ID="boxWarnings" runat="server" CssClass="box" HorizontalAlign="Left" DefaultButton="Dummy">
                    <asp:Label class="errorLabel" ID="lblError" runat="server"></asp:Label>
                    <asp:Label class="warningLabel" ID="lblWarning" runat="server"></asp:Label>
                    <asp:ValidationSummary ID="Valid_Summary_Proposta" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Proposta"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_Entidades" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Entidades"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_PontoVenda" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="PontoVenda"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_Abastecimento" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Abastecimento"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_Actividade" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Actividade"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_TPR" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="TPR"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_Debitos" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Debitos"></asp:ValidationSummary>
                </asp:Panel>
                <asp:CustomValidator ID="C_val_Contrato" runat="server" meta:resourcekey="C_val_Contrato" ValidationGroup="contrato"
                    Display="Dynamic" CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_Contrato_validate"></asp:CustomValidator>
            </p>
            <%-- DADOS DO CONTRATO  ######################################################################## --%>
            <p>
                <asp:Panel ID="C_box_Contrato" runat="server" CssClass="box" DefaultButton="C_btn_Seguinte">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <%-- GROUP HEADER  ######################################################### --%>
                        <tr onclick="javascript:ShowHide('proposta');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_proposta_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_Contrato.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr id="proposta">
                            <td class="backColor cell" colspan="3">
                                <table>
                                    <tr>
                                        <%-- PROPOSTA  ##################################################################### --%>
                                        <td width="20%" valign="top">
                                            <table id="C_box_Proposta" runat="server" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="tituloGrande tituloListagem">
                                                        <%= this.GetLocalResourceObject("Panel_Proposta.Header").ToString() %>
                                                    </td>
                                                </tr>
                                                <%-- CONTENT  ############################################################## --%>
                                                <tr>
                                                    <td class="readonly">
                                                        <table border="0" cellpadding="0" cellspacing="2" width="100%">
                                                            <%-- TITULO COLUNAS --%>
                                                            <tr>
                                                                <%-- EMISSOR --%>
                                                                <td class="titulo_peq" style="padding-right: 10px;">
                                                                    <%= this.GetLocalResourceObject("Panel_Proposta_Emissor.Header").ToString() %>
                                                                </td>
                                                                <%-- ANO --%>
                                                                <td class="titulo_peq" style="padding-right: 10px;">
                                                                    <%= this.GetLocalResourceObject("Panel_Proposta_Ano.Header").ToString() %>
                                                                </td>
                                                                <%-- N� SERIE --%>
                                                                <td class="titulo_peq" style="padding-right: 10px;">
                                                                    <%= this.GetLocalResourceObject("Panel_Proposta_Serie.Header").ToString() %>
                                                                </td>
                                                                <%-- NUMERO --%>
                                                                <td class="titulo_peq" style="">
                                                                    <%= this.GetLocalResourceObject("Panel_Proposta_Numero.Header").ToString() %>
                                                                </td>
                                                            </tr>
                                                            <%-- VALORES --%>
                                                            <tr>
                                                                <%-- EMISSOR --%>
                                                                <td align="center" style="padding-right: 10px;">
                                                                    <asp:TextBox ID="C_txt_Emissor" runat="server" meta:resourcekey="C_txt_Emissor" CssClass="textboxBig ReadOnly"
                                                                        ReadOnly="true" MaxLength="2" Width="100%" CausesValidation="true" AutoPostBack="false" OnTextChanged="C_txt_Numproposta_Changed"></asp:TextBox>
                                                                </td> 
                                                                <%-- ANO --%>
                                                                <td align="center" style="padding-right: 10px;">
                                                                    <asp:TextBox ID="C_txt_Ano" runat="server" meta:resourcekey="C_txt_Ano" CssClass="textboxBig" MaxLength="4"
                                                                        Width="70px" CausesValidation="true" AutoPostBack="false" OnTextChanged="C_txt_Numproposta_Changed"></asp:TextBox>
                                                                </td>
                                                                <%-- N� SERIE --%>
                                                                <td align="center" style="padding-right: 10px;">
                                                                    <asp:TextBox ID="C_txt_Serie" runat="server" meta:resourcekey="C_txt_Serie" CssClass="textboxBig" MaxLength="2"
                                                                        Width="100%" CausesValidation="true" AutoPostBack="false" OnTextChanged="C_txt_Numproposta_Changed"></asp:TextBox>
                                                                </td>
                                                                <%-- NUMERO --%>
                                                                <td align="center" style="padding-right: 10px;">
                                                                    <asp:TextBox ID="C_txt_Numero" runat="server" meta:resourcekey="C_txt_Numero" CssClass="textboxBig" MaxLength="4"
                                                                        Width="70px" CausesValidation="true" AutoPostBack="false" OnTextChanged="C_txt_Numproposta_Changed"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <%-- EMISSOR NAME --%>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:DropDownList ID="C_ddl_Emissor" runat="server" CssClass="textbox" Width="100%" DataTextField="COD_NOME_EMISSOR"
                                                                        DataValueField="ID_EMISSOR" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="C_ddl_Emissor_Changed">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <%-- VALIDATORS --%>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:CustomValidator ID="C_val_txtAno_Empty" runat="server" meta:resourcekey="C_val_txtAno_Empty" ControlToValidate="C_txt_Ano"
                                                                        ValidationGroup="Proposta" ValidateEmptyText="true" Display="Dynamic" EnableClientScript="false"
                                                                        CssClass="textValidator" SetFocusOnError="true" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                                                    <asp:CustomValidator ID="C_val_txtSerie_Empty" runat="server" meta:resourcekey="C_val_txtSerie_Empty"
                                                                        ControlToValidate="C_txt_Serie" ValidationGroup="Proposta" EnableClientScript="false" ValidateEmptyText="true"
                                                                        Display="Dynamic" CssClass="textValidator" SetFocusOnError="true" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                                                    <asp:CustomValidator ID="C_val_txtNumero_Empty" runat="server" meta:resourcekey="C_val_txtNumero_Empty"
                                                                        ControlToValidate="C_txt_Numero" ValidationGroup="Proposta" ValidateEmptyText="true" Display="Dynamic"
                                                                        CssClass="textValidator" SetFocusOnError="true" EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                                                    <asp:CustomValidator ID="C_val_txtAno" runat="server" meta:resourcekey="C_val_txtAno" ControlToValidate="C_txt_Ano"
                                                                        ValidationGroup="Proposta" ValidateEmptyText="false" Display="Dynamic" CssClass="textValidator" SetFocusOnError="true"
                                                                        OnServerValidate="C_val_txtAno_Validate" EnableClientScript="false"></asp:CustomValidator>
                                                                    <asp:CustomValidator ID="C_val_txtSerie" runat="server" meta:resourcekey="C_val_txtSerie" ControlToValidate="C_txt_Serie"
                                                                        ValidationGroup="Proposta" ValidateEmptyText="false" Display="Dynamic" CssClass="textValidator" SetFocusOnError="true"
                                                                        OnServerValidate="C_val_txtSerie_Validate" EnableClientScript="false"></asp:CustomValidator>
                                                                    <asp:CustomValidator ID="C_val_txtNumero" runat="server" meta:resourcekey="C_val_txtNumero" ControlToValidate="C_txt_Numero"
                                                                        ValidationGroup="Proposta" ValidateEmptyText="false" Display="Dynamic" CssClass="textValidator" SetFocusOnError="true"
                                                                        OnServerValidate="C_val_txtNumero_Validate" EnableClientScript="false"></asp:CustomValidator>
                                                                    <asp:CustomValidator ID="C_val_Proposta" runat="server" meta:resourcekey="C_val_Proposta" ValidationGroup="Proposta"
                                                                        Display="Dynamic" CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_Proposta_validate"></asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <%-- CHECK --%>
                                        <td align="center">
                                            <asp:Image ID="C_img_CheckProposta" runat="server" AlternateText="" ImageAlign="Middle" Visible="false" /><br />
                                            <asp:Label ID="C_lbl_CkeckProposta" runat="server"></asp:Label>
                                        </td>
                                        <%-- NUMERO GRANDE --%>
                                        <td width="60%" align="center">
                                            <asp:Label ID="C_lbl_NumeroProposta" runat="server" CssClass="tituloMenu" Style="font-size: 40px;"></asp:Label><br />
                                            <asp:HyperLink ID="C_btn_CheckEdit" runat="server" meta:resourcekey="C_btn_CheckEdit" CssClass="button"
                                                Style="font-size: 20px;" Visible="false"></asp:HyperLink>
                                        </td>
                                        <%-- RECEP��O --%>
                                        <td width="20%" valign="top">
                                            <table id="C_box_Recepcao" runat="server" border="0" cellpadding="0" cellspacing="0" visible="false">
                                                <%-- HEADER ################################################################ --%>
                                                <tr>
                                                    <td class="tituloGrande tituloListagem">
                                                        <%= this.GetLocalResourceObject("Panel_Recepcao.Header").ToString() %>
                                                    </td>
                                                </tr>
                                                <%-- CONTENT ############################################################### --%>
                                                <tr>
                                                    <td class="readonly">
                                                        <table width="200px">
                                                            <%-- CUSTOMER SERVICE --%>
                                                            <tr>
                                                                <td>
                                                                    <%= this.GetLocalResourceObject("Panel_Recepcao_Customer.Header").ToString() %><br />
                                                                    <asp:DropDownList ID="C_ddl_CustomerService" runat="server" Width="100%" CssClass="textbox" DataTextField="COD_NOME_CUSTOMER_SERVICE"
                                                                        DataValueField="ID_CUSTOMER_SERVICE" AppendDataBoundItems="true">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <%-- DATA --%>
                                                            <tr>
                                                                <td>
                                                                    <%= this.GetLocalResourceObject("Panel_Recepcao_Data.Header").ToString() %><br />
                                                                    <UC:DatePicker ID="C_Recepcao_dp_data" runat="server" AllowClear="false" width="100%" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- NIVEL  ######################################################################## --%>
            <p>
                <asp:Panel ID="C_box_Nivel" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <%-- GROUP HEADER  ######################################################### --%>
                        <tr onclick="javascript:ShowHide('Nivel');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_Nivel_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_Nivel.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr id="Nivel">
                            <td class="backColor cell" colspan="3">
                                <table width="80%">
                                    <%-- GRUPO ECON�MICO --%>
                                    <tr>
                                        <td width="150px">
                                            <asp:RadioButton ID="C_cb_Nivel_GE" runat="server" meta:resourcekey="C_cb_Nivel_GE" GroupName="Nivel"
                                                AutoPostBack="true" OnCheckedChanged="C_cb_Nivel_Changed" />
                                        </td>
                                        <td class="textbox">
                                            <asp:DropDownList ID="C_ddl_GrupoEconomico" runat="server" Width="100%" CssClass="textbox" DataTextField="ID_NOME_GRUPO_ECONOMICO"
                                                DataValueField="ID_GRUPO_ECONOMICO" AutoPostBack="true" OnSelectedIndexChanged="C_ddl_GrupoEconomico_Changed">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Label ID="C_Nivel_lbl_ValGE" runat="server" CssClass="warningLabel"></asp:Label>
                                        </td>
                                    </tr>
                                    <%-- SUBGRUPO ECON�MICO --%>
                                    <tr>
                                        <td width="150px">
                                            <asp:RadioButton ID="C_cb_Nivel_SGE" runat="server" meta:resourcekey="C_cb_Nivel_SGE" AutoPostBack="true"
                                                OnCheckedChanged="C_cb_Nivel_Changed" GroupName="Nivel" />
                                        </td>
                                        <td class="textbox">
                                            <asp:DropDownList ID="C_ddl_SubGrupoEconomico" runat="server" Width="100%" CssClass="textbox" DataTextField="ID_NOME_SUBGRUPO_ECONOMICO"
                                                AppendDataBoundItems="true" DataValueField="ID_SUBGRUPO_ECONOMICO" AutoPostBack="true" OnSelectedIndexChanged="C_ddl_SubGrupoEconomico_Changed">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Label ID="C_Nivel_lbl_ValSGE" runat="server" CssClass="warningLabel"></asp:Label>
                                        </td>
                                    </tr>
                                    <%-- LES --%>
                                    <tr>
                                        <td width="150px">
                                            <asp:RadioButton ID="C_cb_Nivel_LE" runat="server" meta:resourcekey="C_cb_Nivel_LE" AutoPostBack="true"
                                                OnCheckedChanged="C_cb_Nivel_Changed" GroupName="Nivel" />
                                        </td>
                                        <td class="textbox">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- PESQUISA DE CLIENTES  ##################################################################### --%>
            <p>
                <asp:Panel ID="C_box_Pesquisa" runat="server" CssClass="box" DefaultButton="Dummy">
                    <UC:Search ID="C_Entidade_Pesquisa" runat="server" meta:resourcekey="C_Entidade_Pesquisa" DeleteOnSelect="true"
                        OnSearchResult="C_Entidade_Pesquisa_onSearchResult" OnSelectResult="C_Entidade_Pesquisa_onSelectResult" />
                </asp:Panel>
            </p>
            <%-- LISTA DE ENTIDADES  ####################################################################### --%>
            <p>
                <asp:Panel ID="C_box_Entidade" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%-- HEADER ######################################################################## --%>
                        <tr onclick="javascript:ShowHide('Entidade');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_Entidade_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_Entidade.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <%-- CONTE�DO ###################################################################### --%>
                        <tr id="Entidade">
                            <td colspan="3">
                                <table border="0" cellpadding="0" cellspacing="0" class="table cell" width="100%">
                                    <%-- CONTAGEM / PAGINA��O --%>
                                    <tr class="backColor">
                                        <td class="backColor">
                                            <asp:Label CssClass="label" ID="C_lbl_ClientesNum" runat="server"></asp:Label>
                                        </td>
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td>
                                                        &nbsp;&nbsp;&nbsp;
                                                    </td>
                                                    <td>
                                                        <UC:ImageTextButton ID="C_Entidades_btn_Paginacao" runat="server" TextCssClass="button" TextPosition="Rigth"
                                                            CausesValidation="false" OnClick="C_Entidades_btn_Paginacao_Click" meta:resourcekey="C_Entidades_btn_Paginacao" />
                                                    </td>
                                                </tr>
                                            </table>
                                    </tr>
                                    <%-- LISTA --%>
                                    <tr>
                                        <td class="backColor" colspan="2">
                                            <asp:DataGrid ID="C_dg_Entidades" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                                BorderStyle="None" GridLines="None" CellSpacing="2" OnItemCommand="C_dg_Entidades_ItemCommand" OnPageIndexChanged="C_dg_Entidades_PageIndexChanged"
                                                OnSortCommand="C_dg_Entidades_SortCommand" PageSize="15" Width="100%">
                                                <HeaderStyle ForeColor="#00008B" BackColor="#99CCFF" Height="20px" />
                                                <ItemStyle CssClass="ListRow" />
                                                <AlternatingItemStyle CssClass="AlternateListRow" />
                                                <PagerStyle ForeColor="#00008B" BackColor="Transparent" HorizontalAlign="Left" Mode="NumericPages" />
                                                <Columns>
                                                    <asp:BoundColumn DataField="DES_GRUPO_ECONOMICO_LE" HeaderText="GRUPO" SortExpression="DES_GRUPO_ECONOMICO_LE">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DES_SUBGRUPO_ECONOMICO_LE" HeaderText="SUB GRUPO" SortExpression="DES_SUBGRUPO_ECONOMICO_LE">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DES_CONCESSIONARIO" HeaderText="CONCESSION�RIO" SortExpression="DES_CONCESSIONARIO">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DES_CLIENTE" HeaderText="CLIENTE" SortExpression="DES_CLIENTE"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="COD_LOCAL_ENTREGA" HeaderText="C�DIGO LE" SortExpression="COD_LOCAL_ENTREGA">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DES_LOCAL_ENTREGA" HeaderText="LOCAL DE ENTREGA" SortExpression="DES_LOCAL_ENTREGA">
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle ForeColor="Transparent" BackColor="Transparent" Width="1%" />
                                                        <ItemStyle VerticalAlign="Middle" BackColor="Transparent" HorizontalAlign="Right" Width="1%" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="C_Entidades_btn_Clear" runat="server" CommandName="DeleteAll" CausesValidation="false"
                                                                meta:resourcekey="C_Entidades_btn_Clear" Visible='<%# this.T_AE_ENTIDADE.DefaultView.Count >0 && this.C_dg_Entidades.Attributes["CanUse"]=="true" && this.C_cb_Nivel_LE.Checked %>' />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="C_btn_dgClientes_Delete" runat="server" CommandName="Delete" CausesValidation="false"
                                                                meta:resourcekey="C_btn_dgClientes_Delete" Visible='<%# this.C_dg_Entidades.Attributes["CanUse"]=="true" && this.C_cb_Nivel_LE.Checked %>' />&nbsp;&nbsp;<asp:ImageButton
                                                                    ID="C_btn_dgClientes_Select" runat="server" CommandName="Select" CausesValidation="false" meta:resourcekey="C_btn_dgClientes_Select"
                                                                    Visible='<%# this.C_dg_Entidades.Attributes["CanUse"]=="true" %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                            <asp:CustomValidator ID="C_val_Entidades_Empty" runat="server" meta:resourcekey="C_val_Entidades_Empty"
                                                ValidationGroup="Entidades" Display="Dynamic" EnableClientScript="false" CssClass="textValidator"
                                                OnServerValidate="C_val_Entidades_Empty_validate"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- IDENTIFICA��O PONTO VENDA  ################################################################ --%>
            <p>
                <asp:Panel ID="C_box_PontoVenda" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%-- HEADER ######################################################################## --%>
                        <tr onclick="javascript:ShowHide('PontoVenda');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_PontoVenda_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_PontoVenda.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <%-- CONTENT ####################################################################### --%>
                        <tr id="PontoVenda">
                            <td class="backColor cell" colspan="3">
                                <%-- TABELA COM CAMPOS DE IDENTIFICA��O PT VENDA ########################### --%>
                                <table cellpadding="2" class="table">
                                    <%-- NOME --%>
                                    <tr>
                                        <td class="label" width="14%" valign="top">
                                            Nome do cliente
                                        </td>
                                        <td class="textbox" colspan="3" valign="top">
                                            <asp:TextBox ID="C_PontoVenda_txt_Nome" runat="server" CssClass="textbox" MaxLength="50" Width="100%"></asp:TextBox>
                                            <asp:CustomValidator ID="C_PontoVenda_val_txtNome_Empty" runat="server" meta:resourcekey="C_PontoVenda_val_txtNome_Empty"
                                                ControlToValidate="C_PontoVenda_txt_Nome" ValidationGroup="PontoVenda" ValidateEmptyText="true" Display="Dynamic"
                                                CssClass="textValidator" SetFocusOnError="true" EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <%-- MORADA --%>
                                    <tr>
                                        <td class="label" valign="top">
                                            Morada
                                        </td>
                                        <td class="textbox" colspan="3" valign="top">
                                            <asp:TextBox ID="C_PontoVenda_txt_Morada" runat="server" CssClass="textbox" Height="50" MaxLength="255"
                                                TextMode="MultiLine" Width="100%" onkeyDown="return checkTextAreaMaxLength(this,event,'255');"></asp:TextBox>
                                            <asp:CustomValidator ID="C_PontoVenda_val_txtMorada_Empty" runat="server" meta:resourcekey="C_PontoVenda_val_txtMorada_Empty"
                                                ControlToValidate="C_PontoVenda_txt_Morada" ValidationGroup="PontoVenda" ValidateEmptyText="true"
                                                Display="Dynamic" CssClass="textValidator" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <%-- LOCALIDADE / NIF --%>
                                    <tr>
                                        <td class="label" valign="top">
                                            Localidade
                                        </td>
                                        <td class="textbox" valign="top" width="40%">
                                            <asp:TextBox ID="C_PontoVenda_txt_Localidade" runat="server" CssClass="textbox" MaxLength="50" Width="100%"></asp:TextBox>
                                            <asp:CustomValidator ID="C_PontoVenda_val_txtLocalidade_Empty" runat="server" meta:resourcekey="C_PontoVenda_val_txtLocalidade_Empty"
                                                ControlToValidate="C_PontoVenda_txt_Localidade" ValidationGroup="PontoVenda" ValidateEmptyText="true"
                                                Display="Dynamic" CssClass="textValidator" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                        </td>
                                        <td class="label" valign="top">
                                            N� Contribuite
                                        </td>
                                        <td class="textbox" valign="top" width="30%">
                                            <asp:TextBox ID="C_PontoVenda_txt_Contribuinte" runat="server" CssClass="textbox" MaxLength="9" Width="100%"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="C_PontoVenda_val_txtContribuinte" runat="server" meta:resourcekey="C_PontoVenda_val_txtContribuinte"
                                                ControlToValidate="C_PontoVenda_txt_Contribuinte" ValidationGroup="PontoVenda" Display="Dynamic"
                                                EnableClientScript="true" CssClass="textValidator" ValidationExpression="\d{9}"></asp:RegularExpressionValidator>
                                            <asp:CustomValidator ID="C_PontoVenda_val_txtContribuinte_Empty" runat="server" meta:resourcekey="C_PontoVenda_val_txtContribuinte_Empty"
                                                ControlToValidate="C_PontoVenda_txt_Contribuinte" ValidationGroup="PontoVenda" ValidateEmptyText="true"
                                                Display="Dynamic" CssClass="textValidator" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- ABASTECIMENTO  ############################################################################ --%>
            <p>
                <asp:Panel ID="C_box_Abastecimento" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%-- HEADER ######################################################################## --%>
                        <tr onclick="javascript:ShowHide('Abastecimento');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_Abastecimento_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_Abastecimento.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <%-- CONTENT ####################################################################### --%>
                        <tr id="Abastecimento">
                            <td class="backColor cell" colspan="3">
                                <table border="0" class="table">
                                    <%-- INSER��O --%>
                                    <tr id="C_box_Abastecimento_Insert" runat="server">
                                        <td valign="top" width="40%">
                                            <asp:Panel ID="Panel_Abastecimento_Conc" runat="server" DefaultButton="C_Abastecimento_btn_Insert">
                                                <asp:DropDownList ID="C_Abastecimento_ddl_Conc" runat="server" CssClass="textbox" Width="90%" EnableViewState="true"
                                                    DataTextField="ID_NOME_CONCESSIONARIO" DataValueField="ID_CONCESSIONARIO">
                                                </asp:DropDownList>
                                                &nbsp;&nbsp;<asp:ImageButton ID="C_Abastecimento_btn_Insert" runat="server" meta:resourcekey="C_Abastecimento_btn_Insert"
                                                    CssClass="button" CommandName="Inserir" OnCommand="C_Abastecimento_Add_ItemCommand" />
                                            </asp:Panel>
                                        </td>
                                        <td align="right">
                                            <UC:ImageTextButton ID="C_Abastecimento_btn_Load" runat="server" TextCssClass="button" TextPosition="Rigth"
                                                CausesValidation="false" OnClick="C_Abastecimento_btn_Load_Click" meta:resourcekey="C_Abastecimento_btn_Load" />
                                        </td>
                                    </tr>
                                    <%--####################################################--%>
                                    <%-- LISTA --%>
                                    <tr id="C_box_Abastecimento_Lista" runat="server" class="">
                                        <td align="left" colspan="2">
                                            <asp:DataGrid ID="C_Abastecimento_dg_Lista" runat="server" AutoGenerateColumns="False" BackColor="Transparent"
                                                BorderStyle="None" GridLines="None" CellSpacing="2" OnItemCommand="C_Abastecimento_dg_Lista_ItemCommand"
                                                OnItemDataBound="C_Abastecimento_dg_Lista_ItemDataBound" Width="100%">
                                                <HeaderStyle ForeColor="#00008B" BackColor="#99CCFF" HorizontalAlign="Center" Height="20px" />
                                                <ItemStyle CssClass="ListRow" />
                                                <AlternatingItemStyle CssClass="AlternateListRow" />
                                                <Columns>
                                                    <%-- CONCESSIONARIO --%>
                                                    <asp:TemplateColumn HeaderText="CONCESSION�RIO">
                                                        <HeaderStyle Width="25%" />
                                                        <ItemStyle Width="25%" />
                                                        <ItemTemplate>
                                                            <%# DataBinder.Eval(Container.DataItem,"COD_CONCESSIONARIO").ToString() + " - "+ DataBinder.Eval(Container.DataItem,"DES_CONCESSIONARIO") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- FACTURACAO --%>
                                                    <asp:TemplateColumn HeaderText="&nbsp;FACTURA��O&nbsp;">
                                                        <HeaderStyle Width="1%" />
                                                        <ItemStyle Width="1%" HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="C_Abastecimento_cb_Facturacao" runat="server" OnCheckedChanged="AbastecimentoChange"
                                                                Enabled='<%# this.C_Abastecimento_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- DISTRIBUI��O --%>
                                                    <asp:TemplateColumn HeaderText="&nbsp;DISTRIBUI��O&nbsp;">
                                                        <HeaderStyle Width="1%" />
                                                        <ItemStyle Width="1%" HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="C_Abastecimento_cb_Distribuicao" runat="server" OnCheckedChanged="AbastecimentoChange"
                                                                Enabled='<%# this.C_Abastecimento_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- OBSERVA��ES --%>
                                                    <asp:TemplateColumn HeaderText="OBSERVA��ES">
                                                        <HeaderStyle Width="60%" />
                                                        <ItemStyle Width="60%" />
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="C_Abastecimento_txt_Obs" runat="server" CssClass="textbox" MaxLength="255" Width="100%"
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"OBSERVACOES") %>' OnTextChanged="AbastecimentoChange"
                                                                Enabled='<%# this.C_Abastecimento_dg_Lista.Attributes["CanUse"]=="true" %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- BOT�ES --%>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle ForeColor="Transparent" BackColor="Transparent" Width="1%" />
                                                        <ItemStyle VerticalAlign="Middle" BackColor="Transparent" HorizontalAlign="Left" Width="1%" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="C_Abastecimento_btn_DeleteAll" runat="server" CommandName="ApagarTudo" CausesValidation="false"
                                                                meta:resourcekey="C_Abastecimento_btn_DeleteAll" Visible='<%# this.T_AE_ABASTECIMENTO.DefaultView.Count>0 && this.C_Abastecimento_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="C_Abastecimento_btn_Delete" runat="server" CommandName="Apagar" CausesValidation="false"
                                                                meta:resourcekey="C_Abastecimento_btn_Delete" Visible='<%# this.C_Abastecimento_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                            <asp:CustomValidator ID="C_Abastecimento_val_Empty" runat="server" meta:resourcekey="C_Abastecimento_val_Empty"
                                                ValidationGroup="Abastecimento" Display="Dynamic" EnableClientScript="false" CssClass="textValidator"
                                                OnServerValidate="C_Abastecimento_val_Empty_validate"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <%--#####################################################--%>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- ACTIVIDADE  ############################################################################### --%>
            <p>
                <asp:Panel ID="C_box_Actividade" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%-- HEADER ######################################################################## --%>
                        <tr onclick="javascript:ShowHide('Actividade');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_Actividade_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_Actividade.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <%-- CONTENT ####################################################################### --%>
                        <tr id="Actividade">
                            <td class="backColor cell" colspan="3">
                                <table class="table">
                                    <%-- INSER��O --%>
                                    <tr id="C_box_Actividade_Insert" runat="server">
                                        <td width="33%" valign="top">
                                            <asp:Panel ID="Panel_Actividade_Tipo" runat="server" DefaultButton="C_Actividade_btn_Insert">
                                                <asp:DropDownList ID="C_Actividade_ddl_Tipo" runat="server" CssClass="textbox" Width="100%" AutoPostBack="true"
                                                    OnSelectedIndexChanged="C_Actividade_ddl_Tipo_Changed" EnableViewState="true" DataTextField="NOME_TIPO_ACTIVIDADE"
                                                    DataValueField="ID_TIPO_ACTIVIDADE">
                                                </asp:DropDownList>
                                            </asp:Panel>
                                            <br />
                                        </td>
                                        <td width="33%" valign="top">
                                            <asp:Panel ID="Panel_Actividade_Acordo" runat="server" DefaultButton="C_Actividade_btn_Insert">
                                                <asp:TextBox ID="C_Actividade_txt_Acordo" runat="server" CssClass="textbox" MaxLength="20" Width="100%"></asp:TextBox><br />
                                                <asp:CustomValidator ID="C_Actividade_val_txtAcordo_Empty" runat="server" meta:resourcekey="C_Actividade_val_txtAcordo_Empty"
                                                    ControlToValidate="C_Actividade_txt_Acordo" ValidationGroup="Actividade_Insert" ValidateEmptyText="true"
                                                    Display="Dynamic" CssClass="textValidator" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                            </asp:Panel>
                                        </td>
                                        <td width="33%" valign="top">
                                            <asp:Panel ID="Panel_Actividade_Valor" runat="server" DefaultButton="C_Actividade_btn_Insert">
                                                <asp:TextBox ID="C_Actividade_txt_Valor" runat="server" CssClass="textbox" MaxLength="12" Width="100%"></asp:TextBox><br />
                                                <asp:CustomValidator ID="C_Actividade_val_txtValor_Empty" runat="server" meta:resourcekey="C_Actividade_val_txtValor_Empty"
                                                    ControlToValidate="C_Actividade_txt_Valor" ValidationGroup="Actividade_Insert" ValidateEmptyText="true"
                                                    Display="Dynamic" CssClass="textValidator" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                                <asp:CustomValidator ID="C_Actividade_val_txtValor" runat="server" meta:resourcekey="C_Actividade_val_txtValor"
                                                    ControlToValidate="C_Actividade_txt_Valor" ValidationGroup="Actividade_Insert" ValidateEmptyText="false"
                                                    Display="Dynamic" CssClass="textValidator" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="C_Actividade_val_txtValor_Validate"></asp:CustomValidator>
                                            </asp:Panel>
                                        </td>
                                        <td width="1%" valign="top" align="left" style="background-color: Transparent">
                                            <asp:ImageButton ID="C_Actividade_btn_Insert" runat="server" meta:resourcekey="C_Actividade_btn_Insert"
                                                CssClass="button" CommandName="Inserir" OnCommand="C_Actividade_Add_ItemCommand" ValidationGroup="Actividade_Insert" />
                                        </td>
                                    </tr>
                                    <%-- LISTA --%>
                                    <tr id="C_box_Actividade_Lista" runat="server" class="">
                                        <td colspan="4" align="left">
                                            <asp:DataGrid ID="C_Actividade_dg_Lista" runat="server" AutoGenerateColumns="False" BackColor="Transparent"
                                                BorderStyle="None" GridLines="None" CellSpacing="2" OnItemCommand="C_Actividade_dg_Lista_ItemCommand"
                                                OnItemDataBound="C_Actividade_dg_Lista_ItemDataBound" Width="100%" ShowFooter="true">
                                                <HeaderStyle ForeColor="#00008B" BackColor="#99CCFF" HorizontalAlign="Center" Height="20px" />
                                                <ItemStyle CssClass="ListRow" />
                                                <AlternatingItemStyle CssClass="AlternateListRow" />
                                                <FooterStyle BackColor="Transparent" VerticalAlign="Top" />
                                                <Columns>
                                                    <%-- TIPO ACTIVIDADE --%>
                                                    <asp:TemplateColumn HeaderText="TIPO DE ACTIVIDADE">
                                                        <ItemStyle Width="33%" />
                                                        <FooterStyle Width="33%" />
                                                        <ItemTemplate>
                                                            <%# DataBinder.Eval(Container.DataItem,"NOME_TIPO_ACTIVIDADE") %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <br />
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- ACORDO --%>
                                                    <asp:TemplateColumn HeaderText="N� ACORDO">
                                                        <ItemStyle Width="33%" />
                                                        <FooterStyle Width="33%" Font-Size="Larger" HorizontalAlign="Right" />
                                                        <ItemTemplate>
                                                            <%# DataBinder.Eval(Container.DataItem,"NUM_ACORDO") %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            TOTAL&nbsp;AC��O:
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- VALOR --%>
                                                    <asp:TemplateColumn HeaderText="VALOR">
                                                        <ItemStyle Width="33%" HorizontalAlign="Right" />
                                                        <FooterStyle Width="33%" HorizontalAlign="Right" Font-Size="Larger" BackColor="#6666BB" ForeColor="#FFFFFF" />
                                                        <ItemTemplate>
                                                            <%# double.Parse(DataBinder.Eval(Container.DataItem,"VALOR_ACORDO").ToString()).ToString("#0.00") + "&nbsp;�" %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="C_Actividade_Total" runat="server"></asp:Label>&nbsp;�
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- BOT�ES --%>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle ForeColor="Transparent" BackColor="Transparent" Width="1%" />
                                                        <ItemStyle VerticalAlign="Middle" BackColor="Transparent" HorizontalAlign="Left" Width="1%" />
                                                        <FooterStyle Width="1%" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="C_Actividade_btn_DeleteAll" runat="server" CommandName="ApagarTudo" CausesValidation="false"
                                                                meta:resourcekey="C_Actividade_btn_DeleteAll" Visible='<%# this.T_AE_ACTIVIDADE.DefaultView.Count>0 && this.C_Actividade_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="C_Actividade_btn_Delete" runat="server" CommandName="Apagar" CausesValidation="false"
                                                                meta:resourcekey="C_Actividade_btn_Delete" Visible='<%# this.C_Actividade_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <br />
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <%-- EMPTY VALIDATOR --%>
                                    <tr>
                                        <td>
                                            <asp:CustomValidator ID="C_val_Actividade_Empty" runat="server" meta:resourcekey="C_val_Actividade_Empty"
                                                ValidationGroup="Actividade" Display="Dynamic" CssClass="textValidator" EnableClientScript="false"
                                                OnServerValidate="C_val_Actividade_Empty_validate"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- TPR ####################################################################################### --%>
            <p>
                <asp:Panel ID="C_box_TPR" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%-- HEADER ######################################################################## --%>
                        <tr onclick="javascript:ShowHide('TPR');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_TPR_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_TPR.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <%-- CONTENT ####################################################################### --%>
                        <tr id="TPR">
                            <td class="backColor cell" colspan="3">
                                <table class="table">
                                    <%--LISTA --%>
                                    <tr id="C_box_TPR_Lista" runat="server" class="">
                                        <td>
                                            <asp:DataGrid ID="C_TPR_dg_Lista" runat="server" DataKeyField="COD_PRODUTO" AutoGenerateColumns="False"
                                                BackColor="Transparent" BorderStyle="None" GridLines="None" CellSpacing="2" OnItemCommand="C_TPR_dg_Lista_ItemCommand"
                                                OnItemDataBound="C_TPR_dg_Lista_ItemDataBound" Width="100%" ShowFooter="true">
                                                <HeaderStyle ForeColor="#00008B" BackColor="#99CCFF" HorizontalAlign="Center" Height="20px" VerticalAlign="Top" />
                                                <ItemStyle CssClass="ListRow" />
                                                <AlternatingItemStyle CssClass="AlternateListRow" />
                                                <FooterStyle BackColor="Transparent" VerticalAlign="Top" />
                                                <Columns>
                                                    <%-- BOT�ES 1 --%>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle ForeColor="Transparent" BackColor="Transparent" />
                                                        <ItemStyle VerticalAlign="Middle" BackColor="Transparent" HorizontalAlign="Right" Width="1%" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="C_TPR_btn_Edit" runat="server" CommandName="Edit" CausesValidation="false" meta:resourcekey="C_TPR_btn_Edit"
                                                                Visible='<%# this.C_TPR_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="C_TPR_btn_Undo" runat="server" CommandName="Cancel" CausesValidation="false" meta:resourcekey="C_TPR_btn_Undo" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="C_TPR_btn_Save" runat="server" CommandName="Update" ValidationGroup="TPR_Update"
                                                                            meta:resourcekey="C_TPR_btn_Save" CausesValidation="true" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- COD_PRODUTO --%>
                                                    <asp:TemplateColumn HeaderText="C�DIGO">
                                                        <ItemTemplate>
                                                            <%# DataBinder.Eval(Container.DataItem,"COD_PRODUTO") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- NOME --%>
                                                    <asp:TemplateColumn HeaderText="NOME">
                                                        <ItemTemplate>
                                                            <%# DataBinder.Eval(Container.DataItem,"NOME_PRODUTO") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- BUDGET --%>
                                                    <asp:TemplateColumn HeaderText="BUDGET">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        VENDAS:
                                                                    </td>
                                                                    <td align="right" valign="top">
                                                                        <%# double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_VENDAS").ToString()).ToString("#0.00") + "&nbsp;%"%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        MARKETING:
                                                                    </td>
                                                                    <td align="right" valign="top">
                                                                        <%# double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_MARKETING").ToString()).ToString("#0.00") + "&nbsp;%"%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        VENDAS:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="C_txt_BudgetVendas" runat="server" CssClass="TextBox" Style="text-align: right;" MaxLength="5"
                                                                            Width="40px" Text='<%# DataBinder.Eval(Container.DataItem, "BUDGET_VENDAS")%>'></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        MARKETING:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="C_txt_BudgetMarketing" runat="server" CssClass="TextBox" Style="text-align: right;"
                                                                            MaxLength="5" Width="40px" Text='<%# DataBinder.Eval(Container.DataItem, "BUDGET_MARKETING")%>'></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:CustomValidator ID="C_val_BudgetVendas_Empty" runat="server" meta:resourcekey="C_val_BudgetVendas_Empty"
                                                                ControlToValidate="C_txt_BudgetVendas" ValidateEmptyText="true" ValidationGroup="TPR_Update" Display="Dynamic"
                                                                CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                                            <asp:CustomValidator ID="C_val_BudgetVendas" runat="server" meta:resourcekey="C_val_BudgetVendas" ControlToValidate="C_txt_BudgetVendas"
                                                                ValidationGroup="TPR_Update" Display="Dynamic" CssClass="textValidator" EnableClientScript="false"
                                                                ValidateEmptyText="false" OnServerValidate="C_val_txt_Percent_validate"></asp:CustomValidator>
                                                            <asp:CustomValidator ID="C_val_BudgetMarketing_Empty" runat="server" meta:resourcekey="C_val_BudgetMarketing_Empty"
                                                                ControlToValidate="C_txt_BudgetMarketing" ValidateEmptyText="true" ValidationGroup="TPR_Update" Display="Dynamic"
                                                                CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                                            <asp:CustomValidator ID="C_val_BudgetMarketing" runat="server" meta:resourcekey="C_val_BudgetMarketing"
                                                                ControlToValidate="C_txt_BudgetMarketing" ValidationGroup="TPR_Update" Display="Dynamic" CssClass="textValidator"
                                                                EnableClientScript="false" ValidateEmptyText="false" OnServerValidate="C_val_txt_Percent_validate"></asp:CustomValidator>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- PERIODO ACTIVIDADE --%>
                                                    <asp:TemplateColumn HeaderText="PER�ODO&nbsp;ACTIVIDADE">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        INICIO:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <%# ((DateTime)DataBinder.Eval(Container.DataItem, "ACTIVIDADE_INICIO")).ToString("dd-MM-yyyy")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        FIM:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <%# ((DateTime)DataBinder.Eval(Container.DataItem, "ACTIVIDADE_FIM")).ToString("dd-MM-yyyy")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        IN�CIO:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <UC:DatePicker ID="C_dp_ActividadeInicio" runat="server" AllowClear="false" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "ACTIVIDADE_INICIO")%>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        FIM:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <UC:DatePicker ID="C_dp_ActividadeFim" runat="server" AllowClear="false" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "ACTIVIDADE_FIM")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:CustomValidator ID="C_val_ActividadeDatas" runat="server" meta:resourcekey="C_val_Datas" ValidationGroup="TPR_Update"
                                                                Display="Dynamic" CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_ActividadeDatas_Validate"></asp:CustomValidator>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- PERIODO COMPRAS --%>
                                                    <asp:TemplateColumn HeaderText="&nbsp;PER�ODO&nbsp;COMPRAS&nbsp;">
                                                        <FooterStyle Font-Size="Larger" HorizontalAlign="Right" />
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        INICIO:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <%# ((DateTime)DataBinder.Eval(Container.DataItem, "COMPRAS_INICIO")).ToString("dd-MM-yyyy")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        FIM:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <%# ((DateTime)DataBinder.Eval(Container.DataItem, "COMPRAS_FIM")).ToString("dd-MM-yyyy")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        IN�CIO:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <UC:DatePicker ID="C_dp_ComprasInicio" runat="server" AllowClear="false" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "COMPRAS_INICIO")%>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="top">
                                                                        FIM:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <UC:DatePicker ID="C_dp_ComprasFim" runat="server" AllowClear="false" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "COMPRAS_FIM")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:CustomValidator ID="C_val_ComprasDatas" runat="server" meta:resourcekey="C_val_Datas" ValidationGroup="TPR_Update"
                                                                Display="Dynamic" CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_ComprasDatas_Validate"></asp:CustomValidator>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            TOTAL:
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- PVP --%>
                                                    <asp:TemplateColumn HeaderText="PVP">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <FooterStyle HorizontalAlign="Right" VerticalAlign="Middle" BackColor="#6666BB" ForeColor="#FFFFFF" />
                                                        <ItemTemplate>
                                                            <%# double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString()).ToString("#0.00") + "&nbsp;�"%>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="C_txt_PVP" runat="server" CssClass="TextBox" Style="text-align: right;" MaxLength="10"
                                                                Width="70px" Text='<%# DataBinder.Eval(Container.DataItem,"PVP") %>'></asp:TextBox><br />
                                                            <asp:CustomValidator ID="C_val_PVP_Empty" runat="server" meta:resourcekey="C_val_PVP_Empty" ControlToValidate="C_txt_PVP"
                                                                ValidateEmptyText="true" ValidationGroup="TPR_Update" Display="Dynamic" CssClass="textValidator"
                                                                EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                                            <asp:CustomValidator ID="C_val_PVP" runat="server" meta:resourcekey="C_val_PVP" ControlToValidate="C_txt_PVP"
                                                                ValidationGroup="TPR_Update" Display="Dynamic" CssClass="textValidator" EnableClientScript="false"
                                                                ValidateEmptyText="false" OnServerValidate="C_val_txt_Value_validate"></asp:CustomValidator>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="C_TPR_Total_PVP" runat="server"></asp:Label>&nbsp;�
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- UNIDADE --%>
                                                    <asp:TemplateColumn HeaderText="UN.">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <%# DataBinder.Eval(Container.DataItem, "UNIDADE_BASE")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- PREVIS�O UN --%>
                                                    <asp:TemplateColumn HeaderText="PREVIS�O<br>(UN)">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <FooterStyle HorizontalAlign="Right" VerticalAlign="Middle" BackColor="#6666BB" ForeColor="#FFFFFF" />
                                                        <ItemTemplate>
                                                            <%# double.Parse(DataBinder.Eval(Container.DataItem, "PREVISAO_QUANTIDADE").ToString()).ToString("#0.00")%>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="C_TPR_Total_PrevisaoUn" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="C_txt_Qtd" runat="server" CssClass="TextBox" Style="text-align: right;" MaxLength="10"
                                                                Width="70px" Text='<%# DataBinder.Eval(Container.DataItem, "PREVISAO_QUANTIDADE") %>'></asp:TextBox><br />
                                                            <asp:CustomValidator ID="C_val_Qtd_Empty" runat="server" meta:resourcekey="C_val_Qtd_Empty" ControlToValidate="C_txt_Qtd"
                                                                ValidateEmptyText="true" ValidationGroup="TPR_Update" Display="Dynamic" CssClass="textValidator"
                                                                EnableClientScript="false" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                                            <asp:CustomValidator ID="C_val_Qtd" runat="server" meta:resourcekey="C_val_Qtd" ControlToValidate="C_txt_Qtd"
                                                                ValidationGroup="TPR_Update" Display="Dynamic" CssClass="textValidator" EnableClientScript="false"
                                                                ValidateEmptyText="false" OnServerValidate="C_val_txt_Value_validate"></asp:CustomValidator>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- PREVIS�O VALOR --%>
                                                    <asp:TemplateColumn HeaderText="PREVIS�O<br>(VALOR)">
                                                        <HeaderStyle ForeColor="#BBDDFF" BackColor="#00008B" />
                                                        <ItemStyle HorizontalAlign="Right" BackColor="#AAAAFF" />
                                                        <FooterStyle HorizontalAlign="Right" VerticalAlign="Middle" BackColor="#6666BB" ForeColor="#FFFFFF" />
                                                        <ItemTemplate>
                                                            <%# Math.Round(double.Parse(DataBinder.Eval(Container.DataItem,"PREVISAO_QUANTIDADE").ToString())
                                                              * double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString()), 3).ToString("#0.00") + "&nbsp;�"
                                                            %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="C_TPR_Total_PrevisaoValor" runat="server"></asp:Label>&nbsp;�
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- SUB TOTAL VENDAS --%>
                                                    <asp:TemplateColumn HeaderText="SUBTOTAL<br>VENDAS">
                                                        <HeaderStyle ForeColor="#BBDDFF" BackColor="#00008B" />
                                                        <ItemStyle HorizontalAlign="Right" BackColor="#AAAAFF" />
                                                        <FooterStyle HorizontalAlign="Right" VerticalAlign="Middle" BackColor="#6666BB" ForeColor="#FFFFFF" />
                                                        <ItemTemplate>
                                                            <%# Math.Round(double.Parse(DataBinder.Eval(Container.DataItem,"PREVISAO_QUANTIDADE").ToString())
                                                              * double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString())
                                                                                                                                                                                              * (double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_VENDAS").ToString()) / 100), 3).ToString("#0.00") + "&nbsp;�"
                                                            %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="C_TPR_Total_Vendas" runat="server"></asp:Label>&nbsp;�
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- SUB TOTAL MARKETING --%>
                                                    <asp:TemplateColumn HeaderText="SUBTOTAL<br>MARKETING">
                                                        <HeaderStyle ForeColor="#BBDDFF" BackColor="#00008B" />
                                                        <ItemStyle HorizontalAlign="Right" BackColor="#AAAAFF" />
                                                        <FooterStyle HorizontalAlign="Right" VerticalAlign="Middle" BackColor="#6666BB" ForeColor="#FFFFFF" />
                                                        <ItemTemplate>
                                                            <%# Math.Round(double.Parse(DataBinder.Eval(Container.DataItem, "PREVISAO_QUANTIDADE").ToString())
                                                              * double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString())
                                                                                                                                                                                              * (double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_MARKETING").ToString()) / 100), 3).ToString("#0.00") + "&nbsp;�"
                                                            %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="C_TPR_Total_Marketing" runat="server"></asp:Label>&nbsp;�
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- TOTAL --%>
                                                    <asp:TemplateColumn HeaderText="TOTAL">
                                                        <HeaderStyle ForeColor="#BBDDFF" BackColor="#00008B" />
                                                        <ItemStyle HorizontalAlign="Right" BackColor="#AAAAFF" />
                                                        <FooterStyle HorizontalAlign="Right" VerticalAlign="Middle" BackColor="#6666BB" ForeColor="#FFFFFF" />
                                                        <ItemTemplate>
                                                            <%# Math.Round(double.Parse(DataBinder.Eval(Container.DataItem,"PREVISAO_QUANTIDADE").ToString())
                                                              * double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString())
                                                                                                                                                                                              * ((double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_VENDAS").ToString()) + double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_MARKETING").ToString())) / 100), 3).ToString("#0.00") + "&nbsp;�"
                                                            %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="C_TPR_Total_Total" runat="server"></asp:Label>&nbsp;�
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- BOT�ES --%>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle ForeColor="Transparent" BackColor="Transparent" />
                                                        <ItemStyle VerticalAlign="Middle" BackColor="Transparent" HorizontalAlign="Left" Width="1%" />
                                                        <FooterStyle Width="1%" />
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="C_TPR_btn_DeleteAll" runat="server" CommandName="ApagarTudo" CausesValidation="false"
                                                                meta:resourcekey="C_TPR_btn_DeleteAll" Visible='<%# this.T_AE_TPR.DefaultView.Count>0 && this.C_TPR_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="C_TPR_btn_Delete" runat="server" CommandName="Apagar" CausesValidation="false" meta:resourcekey="C_TPR_btn_Delete"
                                                                Visible='<%# this.C_TPR_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <br />
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <%-- EMPTY VALIDATOR --%>
                                    <tr>
                                        <td>
                                            <asp:CustomValidator ID="C_val_TPR_Empty" runat="server" meta:resourcekey="C_val_TPR_Empty" ValidationGroup="TPR"
                                                Display="Dynamic" CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_TPR_Empty_validate"></asp:CustomValidator>
                                            <hr />
                                        </td>
                                    </tr>
                                    <%-- PESQUISA --%>
                                    <tr id="C_box_TPR_Pesquisa" runat="server">
                                        <td>
                                            <asp:Panel ID="sdf" runat="server" DefaultButton="C_TPR_btn_Pesquisa">
                                                <table>
                                                    <tr>
                                                        <td width="200px" class="titulo_peq">
                                                            PROCURAR PRODUTO POR
                                                        </td>
                                                        <td align="right">
                                                            C�DIGO:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="C_TPR_txt_PesquisaCodigo" Width="70px" runat="server" CssClass="Textbox" MaxLength="9"></asp:TextBox>
                                                        </td>
                                                        <td align="right" width="70px">
                                                            NOME:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="C_TPR_txt_PesquisaNome" Width="300px" runat="server" CssClass="Textbox" MaxLength="40"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="C_TPR_btn_Pesquisa" Width="16px" runat="server" ImageUrl="~/images/Search.ico" OnClick="C_TPR_btn_Pesquisa_Click"
                                                                ValidationGroup="TPR_pesquisa" CausesValidation="true" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:CustomValidator ID="C_TPR_val_Pesquisa_Empty" runat="server" meta:resourcekey="C_TPR_val_Pesquisa_Empty"
                                                                ValidationGroup="TPR_pesquisa" Display="Dynamic" CssClass="textValidator" SetFocusOnError="true"
                                                                OnServerValidate="C_TPR_val_Pesquisa_Empty_Validate"></asp:CustomValidator>
                                                            <asp:CustomValidator ID="C_TPR_val_PesquisaCodigo" runat="server" meta:resourcekey="C_TPR_val_PesquisaCodigo"
                                                                ValidationGroup="TPR_pesquisa" ControlToValidate="C_TPR_txt_PesquisaCodigo" Display="Dynamic" CssClass="textValidator"
                                                                SetFocusOnError="true" OnServerValidate="C_TPR_val_PesquisaCodigo_Validate"></asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:Label ID="C_TPR_lbl_SearchResult" runat="server" CssClass="warningLabel">Foram encontratos 2224 correspond�ncias</asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <%-- TREE --%>
                                    <tr id="C_box_TPR_Tree" runat="server">
                                        <td>
                                            <UC:ImageTextButton ID="C_TPR_btn_ReloadTree" runat="server" TextCssClass="button titulo" OnClick="C_TPR_btn_ReloadTree_Click"
                                                CausesValidation="false" ImageUrl="~/images/Refresh.ico" Text="Reiniciar a �rvore" TextPosition="Rigth" />
                                            <p>
                                                <asp:TreeView ID="C_TPR_Tree" onclick="TreeCkeckChange()" EnableClientScript="false" runat="server" ExpandDepth="0"
                                                    ShowLines="true" OnTreeNodePopulate="C_TPR_Tree_Populate" Width="100%" AutoGenerateDataBindings="false"
                                                    OnTreeNodeCheckChanged="C_TPR_Tree_CheckChanged" CssClass="TreeView" NodeStyle-VerticalPadding="">
                                                    <%--<NodeStyle CssClass="TreeNode" />--%>
                                                    <LevelStyles>
                                                        <asp:TreeNodeStyle CssClass="button" />
                                                        <asp:TreeNodeStyle CssClass="button" />
                                                        <asp:TreeNodeStyle CssClass="TreeNode" />
                                                        <asp:TreeNodeStyle CssClass="TreeNode" />
                                                        <asp:TreeNodeStyle CssClass="TreeNode" />
                                                    </LevelStyles>
                                                </asp:TreeView>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- DEBITOS  ################################################################################## --%>
            <p>
                <asp:Panel ID="C_box_Debito" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%-- HEADER ######################################################################## --%>
                        <tr onclick="javascript:ShowHide('Debito');" style="cursor: hand">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_Debito_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_Debito.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <%-- CONTENT ####################################################################### --%>
                        <tr id="Debito">
                            <td class="backColor cell" colspan="3">
                                <table class="table">
                                    <%-- INSER��O --%>
                                    <tr id="C_box_Debito_Insert" runat="server">
                                        <td width="33%" valign="top" align="right" style="text-align: right">
                                            <asp:Panel ID="Panel_Debito_Data" runat="server" HorizontalAlign="Right" DefaultButton="C_Debito_btn_Insert">
                                                <UC:DatePicker ID="C_Debito_dp_Data" runat="server" AllowClear="false" />
                                            </asp:Panel>
                                            <br />
                                        </td>
                                        <td width="33%" valign="top" align="right" style="text-align: right">
                                            <asp:Panel ID="Panel_Debito_Valor" runat="server" HorizontalAlign="Right" DefaultButton="C_Debito_btn_Insert">
                                                <asp:TextBox ID="C_Debito_txt_Valor" runat="server" CssClass="textbox" MaxLength="12" Width="100%"></asp:TextBox><br />
                                                <asp:CustomValidator ID="C_Debito_val_txtValor_Empty" runat="server" meta:resourcekey="C_Debito_val_txtValor_Empty"
                                                    ControlToValidate="C_Debito_txt_Valor" ValidationGroup="Debito_Insert" ValidateEmptyText="true" Display="Dynamic"
                                                    CssClass="textValidator" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                                <asp:CustomValidator ID="C_Debito_val_txtValor" runat="server" meta:resourcekey="C_Debito_val_txtValor"
                                                    ControlToValidate="C_Debito_txt_Valor" ValidationGroup="Debito_Insert" ValidateEmptyText="false"
                                                    Display="Dynamic" CssClass="textValidator" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="C_Debito_val_txtValor_Validate"></asp:CustomValidator>
                                            </asp:Panel>
                                        </td>
                                        <td width="1%" valign="top" align="left" style="background-color: Transparent">
                                            <asp:ImageButton ID="C_Debito_btn_Insert" runat="server" meta:resourcekey="C_Debito_btn_Insert" CssClass="button"
                                                CommandName="Inserir" OnCommand="C_Debito_Add_ItemCommand" ValidationGroup="Debito_Insert" />
                                        </td>
                                        <td width="60%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <%-- LISTA --%>
                                    <tr>
                                        <td colspan="3">
                                            <asp:DataGrid ID="C_Debito_dg_Lista" runat="server" AutoGenerateColumns="False" BackColor="Transparent"
                                                BorderStyle="None" GridLines="None" CellSpacing="2" OnItemCommand="C_Debito_dg_Lista_ItemCommand"
                                                OnItemDataBound="C_Debito_dg_Lista_ItemDataBound" Width="100%" ShowFooter="true">
                                                <HeaderStyle ForeColor="#00008B" BackColor="#99CCFF" HorizontalAlign="Center" Height="20px" />
                                                <ItemStyle CssClass="ListRow" />
                                                <AlternatingItemStyle CssClass="AlternateListRow" />
                                                <FooterStyle BackColor="Transparent" VerticalAlign="Top" />
                                                <Columns>
                                                    <%-- DATA --%>
                                                    <asp:TemplateColumn HeaderText="DATA">
                                                        <ItemStyle Width="33%" HorizontalAlign="Right" />
                                                        <FooterStyle Width="33%" Font-Size="Larger" HorizontalAlign="Right" />
                                                        <ItemTemplate>
                                                            <%# ((DateTime)DataBinder.Eval(Container.DataItem,"DATA_DEBITO")).ToString("dd-MM-yyyy") %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            TOTAL:<br />
                                                            TOTAL&nbsp;RESIDUAL:
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- VALOR --%>
                                                    <asp:TemplateColumn HeaderText="VALOR">
                                                        <ItemStyle Width="33%" HorizontalAlign="Right" />
                                                        <FooterStyle Width="33%" Font-Size="Larger" HorizontalAlign="Right" BackColor="#6666BB" ForeColor="#FFFFFF" />
                                                        <ItemTemplate>
                                                            <%# double.Parse(DataBinder.Eval(Container.DataItem,"VALOR_DEBITO").ToString()).ToString("#0.00")+"&nbsp;�" %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="C_Debito_Total" runat="server"></asp:Label>&nbsp;�<br />
                                                            <asp:Label ID="C_Debito_Residual" runat="server"></asp:Label>&nbsp;�
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- BOT�ES --%>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle ForeColor="Transparent" BackColor="Transparent" />
                                                        <ItemStyle VerticalAlign="Middle" BackColor="Transparent" HorizontalAlign="Left" Width="1%" />
                                                        <FooterStyle Width="1%" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="C_Debito_btn_Delete" runat="server" CommandName="Apagar" CausesValidation="false"
                                                                meta:resourcekey="C_Debito_btn_Delete" Visible='<%# this.C_Debito_dg_Lista.Attributes["CanUse"]=="true" %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- OBSERVA��ES  ############################################################################## --%>
            <p style="page-break-before: auto; page-break-after: auto">
                <asp:Panel ID="C_box_Observacoes" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%-- HEADER ######################################################################## --%>
                        <tr onclick="javascript:ShowHide('Observacoes');" style="cursor: hand">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_Observacoes_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_Observacoes.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <%-- CONTENT ####################################################################### --%>
                        <tr id="Observacoes">
                            <td class="backColor cell" colspan="3">
                                <table class="table">
                                    <%-- OBSERVA��ES GERAIS  ############################################### --%>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="C_Observacoes_txt" runat="server" CssClass="txtNormal" MaxLength="4000" Rows="10" TextMode="MultiLine"
                                                onkeyDown="return checkTextAreaMaxLength(this,event,'4000');" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- LOG  ################################################################################## --%>
            <p>
                <asp:Panel ID="C_box_Log" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%-- HEADER ######################################################################## --%>
                        <tr onclick="javascript:ShowHide('Log');" style="cursor: hand">
                            <td class="titulo_peq tituloBarra" width="1%">
                                <label id="C_btn_Log_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label>
                            </td>
                            <td class="titulo_peq tituloBarra">
                                <%= this.GetLocalResourceObject("Panel_Log.Header").ToString() %>
                            </td>
                            <td class="titulo_peq tituloBarra" width="1%">
                                &nbsp;
                            </td>
                        </tr>
                        <%-- CONTENT ####################################################################### --%>
                        <tr id="Log">
                            <td class="backColor cell" colspan="3">
                                <table class="table">
                                    <%-- INSER��O --%>
                                    <tr id="C_box_Log_Insert" runat="server">
                                        <td width="15%">
                                            &nbsp;
                                        </td>
                                        <td width="15%" align="right" valign="top">
                                            MENSAGEM:
                                        </td>
                                        <td width="70%" valign="top">
                                            <asp:Panel ID="Panel_Log_Valor" runat="server" HorizontalAlign="Right" DefaultButton="C_Log_btn_Insert">
                                                <asp:TextBox ID="C_Log_txt_Valor" runat="server" CssClass="textbox" MaxLength="200" Width="100%"></asp:TextBox><br />
                                                <asp:CustomValidator ID="C_Log_val_txtValor_Empty" runat="server" meta:resourcekey="C_Log_val_txtValor_Empty"
                                                    ControlToValidate="C_Log_txt_Valor" ValidationGroup="Log_Insert" ValidateEmptyText="true" Display="Dynamic"
                                                    CssClass="textValidator" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="C_val_txt_Empty_Validate"></asp:CustomValidator>
                                            </asp:Panel>
                                        </td>
                                        <td width="1%" valign="top" align="left" style="background-color: Transparent">
                                            <asp:ImageButton ID="C_Log_btn_Insert" runat="server" meta:resourcekey="C_Log_btn_Insert" CssClass="button"
                                                CommandName="Inserir" OnCommand="C_Log_Add_ItemCommand" ValidationGroup="Log_Insert" />
                                        </td>
                                    </tr>
                                    <%-- LISTA --%>
                                    <%-- CONTAGEM / PAGINA��O --%>
                                    <tr class="backColor">
                                        <td class="backColor" colspan="2">
                                            <asp:Label CssClass="label" ID="C_lbl_LogNum" runat="server"></asp:Label>
                                        </td>
                                        <td align="right" colspan="2">
                                            <UC:ImageTextButton ID="C_Log_btn_Paginacao" runat="server" TextCssClass="button" TextPosition="Rigth"
                                                CausesValidation="false" OnClick="C_Log_btn_Paginacao_Click" meta:resourcekey="C_Log_btn_Paginacao" />
                                        </td>
                                    </tr>
                                    <%-- LISTA --%>
                                    <tr>
                                        <td class="backColor" colspan="4">
                                            <asp:DataGrid ID="C_dg_Log" runat="server" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="None"
                                                GridLines="None" CellSpacing="2" OnItemCommand="C_dg_Log_ItemCommand" OnItemDataBound="C_dg_Log_ItemDataBound"
                                                OnPageIndexChanged="C_dg_Log_PageIndexChanged" PageSize="15" Width="100%">
                                                <HeaderStyle ForeColor="#00008B" BackColor="#99CCFF" Height="20px" />
                                                <ItemStyle CssClass="ListRow" />
                                                <AlternatingItemStyle CssClass="AlternateListRow" />
                                                <PagerStyle ForeColor="#00008B" BackColor="Transparent" HorizontalAlign="Left" Mode="NumericPages" />
                                                <Columns>
                                                    <%-- DATA --%>
                                                    <asp:TemplateColumn HeaderText="DATA">
                                                        <ItemStyle Width="15%" />
                                                        <ItemTemplate>
                                                            <%# ((DateTime)DataBinder.Eval(Container.DataItem,"LOG_DATA")).ToString("yyyy-MM-dd HH:mm:ss") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- USER --%>
                                                    <asp:TemplateColumn HeaderText="UTILIZADOR">
                                                        <ItemStyle Width="15%" />
                                                        <ItemTemplate>
                                                            <%# DataBinder.Eval(Container.DataItem,"LOG_USER") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- MESSAGE --%>
                                                    <asp:TemplateColumn HeaderText="MENSAGEM">
                                                        <ItemStyle Width="70%" />
                                                        <ItemTemplate>
                                                            <%# DataBinder.Eval(Container.DataItem,"MESSAGE") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- BOT�ES --%>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle ForeColor="Transparent" BackColor="Transparent" />
                                                        <ItemStyle VerticalAlign="Middle" BackColor="Transparent" HorizontalAlign="Right" Width="1%" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="C_btn_Log_Delete" runat="server" CommandName="Delete" CausesValidation="false" meta:resourcekey="C_btn_Log_Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- BOT�ES  ################################################################################### --%>
            <p>
                <br />
                <asp:Panel ID="boxBotoes" runat="server" CssClass="box" DefaultButton="Dummy">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%-- BOT�ES ############################################################################ --%>
                        <tr>
                            <td align="left" valign="top" width="5%">
                                <UC:ImageTextButton ID="C_btn_Novo" runat="server" TextCssClass="button" TextPosition="Bottom" meta:resourcekey="C_btn_Novo" CausesValidation="False"
                                      OnClick="C_btn_New_Click" />
                            </td>
                            <td align="left" valign="top" width="5%">
                            </td>
                            <td align="center" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="50%">
                                    <tr>
                                        <%-- INSERT --%>
                                        <td align="center" valign="top">
                                            <UC:ImageTextButton ID="C_btn_Insert" runat="server" TextCssClass="button" TextPosition="Bottom" Text="INSERIR"
                                                ToolTip="Inserir o contrato" ImageUrl="~/images/FormSave.ico" OnClick="C_btn_Insert_Click" />
                                        </td>
                                        <%-- SAVE --%>
                                        <td align="center" valign="top">
                                            <UC:ImageTextButton ID="C_btn_Save" runat="server" TextCssClass="button" TextPosition="Bottom" Text="GUARDAR"
                                                ToolTip="Guardar as altera��es do contrato" ImageUrl="~/images/FormSave.ico" OnClick="C_btn_Save_Click" />
                                        </td>
                                        <%-- EDIT --%>
                                        <td align="center" valign="top">
                                            <UC:ImageTextButton ID="C_btn_Editar" runat="server" TextCssClass="button" TextPosition="Bottom" CausesValidation="False"
                                                Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EDITAR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ToolTip="Editar o contrato"
                                                ImageUrl="~/images/FormEdit.ico" OnClick="C_btn_Edit_Click" />
                                        </td>
                                        <%-- SEGUINTE 1 --%>
                                        <td align="center" valign="top">
                                            <UC:ImageTextButton ID="C_btn_Seguinte" runat="server" TextCssClass="button titulo" TextPosition="Bottom"
                                                Text="SEGUINTE>>" ToolTip="Validar n�mero e avan�ar" CausesValidation="true" ValidationGroup="Proposta"
                                                ImageUrl="~/images/CheckBlue.ico" OnClick="C_btn_Seguinte_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right" valign="top" width="5%">
                                <UC:ImageTextButton ID="C_btn_Home" runat="server" meta:resourcekey="C_btn_Menu" TextCssClass="button"
                                    TextPosition="Bottom" CausesValidation="false" PostBackUrl="~/ae.aspx" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
            <%-- FOOTER  ################################################################################### --%>
            <asp:Panel ID="Panel1" runat="server" CssClass="box" DefaultButton="Dummy">
                <UC:Footer ID="Footer1" runat="server" />
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <AJAX:PostBackTrigger ControlID="C_TPR_Tree" />
        </Triggers>
    </AJAX:UpdatePanel>
    </form>
</body>
</html>
