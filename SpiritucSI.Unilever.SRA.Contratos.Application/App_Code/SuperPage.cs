﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Runtime;
using System.Workflow.Runtime.Hosting;
using SpiritucSI.Unilever.SRA.WF;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.Web.Security;
using SpiritucSI.Unilever.SRA.Business.Budget;
using SRADBInterface;
using System.Workflow.Activities;

public class SuperPage : System.Web.UI.Page
{
    private int _newID_Contrato;
    //public void ShowMessage(string message, Exception ex, ResponseMsgPanel.MessageType msgType)
    //{
    //    ((Main)this.Page.Master).ShowMessage(message, ex, msgType);

    //}
    public string BaseUrl
    {
        get
        {
            return this.Request.Url.Scheme + "://" + this.Request.Url.Host + ((ConfigurationManager.AppSettings["BaseURL"] != null) ? ConfigurationManager.AppSettings["BaseURL"].ToString() : "");
        }
    }
    //public User CurrentUser
    // {
    //     get
    //     {
    //         if (this.Context.Session["CurrentUser"] != null)    
    //             return (User)this.Context.Session["CurrentUser"];
    //         else
    //             this.Response.Redirect("~/Dashboard.aspx"); return null;
    //     }

    //     set
    //     {
    //         this.Context.Session["CurrentUser"] = value;
    //     }
    // }

    //public static string RedirectScript(string page, int timer)
    //{

    //    return "<script type=\"text/javascript\">\n" +
    //               "window.onload = \n" +
    //                "function() {\n" +
    //                    "setTimeout('window.location = \"" + page + "\"', " + timer + ");\n" +
    //                "};\n" +
    //               "</script>";
    //}

    //public int CurrentVersion(VersionType versionType)
    // {
    //     switch(versionType)
    //     {
    //         case VersionType.Development:
    //             return this.CurrentUser.DevelopmentVersion;
    //             break;
    //         case VersionType.Production:
    //             return 0;
    //             break;
    //     }

    //     return 0;

    // }
    protected void Page_Load(object sender, EventArgs e)
    {
        //RunWorkflow();
    }
    #region WORKFLOW
    #region WORKFLOW PROPERTIES
    public WorkflowRuntime WFRuntime
    {
        get
        {
            return (WorkflowRuntime)Application["WorkflowRuntime"];
        }
        set
        {
            Application["WorkflowRuntime"] = value;
        }
    }
    private Exception wfException { get; set; }
    #endregion
    #region WORKFLOW METHODS
    /// <summary>
    /// Serve para correr o workflow sobre uma determinada (uid) instância
    /// </summary>
    /// <param name="uid">guid da instância do workflow</param>
    public void RunWorkflow(Guid uid)
    {

        ExternalDataExchangeService dataService = (ExternalDataExchangeService)this.WFRuntime.GetService(typeof(ExternalDataExchangeService));
        if (dataService == null)
        {
            dataService = new ExternalDataExchangeService();
            WFRuntime.AddService(dataService);
        }

        SpiritucSI.Unilever.SRA.WF.WorkflowManager wfManager = (SpiritucSI.Unilever.SRA.WF.WorkflowManager)dataService.GetService(typeof(WorkflowManager));
        if (wfManager == null)
        {
            wfManager = new WorkflowManager();
            dataService.AddService(wfManager);
        }

        wfManager.InstanceStateChange += new EventHandler<EventArgs>(WF_SRA_OnInstanceStateChange);
        wfManager.WorkflowException += new EventHandler<EventArgs>(WF_SRA_OnWorkflowException);

        this.wfException = null;
        ManualWorkflowSchedulerService scheduler = this.WFRuntime.GetService<ManualWorkflowSchedulerService>();
        scheduler.RunWorkflow(uid);

        //// Faz update do objecto que está debaixo do workflow com o estado actual do mesmo
        //Proposals oProp = new Proposals();
        //oProp.UpdateCurrentState(this.idProposal, this.WFInstance.CurrentState.QualifiedName);

        wfManager.InstanceStateChange -= new EventHandler<EventArgs>(WF_SRA_OnInstanceStateChange);
        wfManager.WorkflowException -= new EventHandler<EventArgs>(WF_SRA_OnWorkflowException);

        if (this.wfException != null)
            throw (Exception)this.wfException;
    }
    /// <summary>
    /// Sem parâmetros serve para iniciar uma nova instância do workflow
    /// </summary>
    /// <returns>guid da instância recém criada</returns>
    public Guid RunWorkflow(int newID_Contrato)
    {
        this._newID_Contrato = newID_Contrato;
        //WorkflowManager wfManager = new WorkflowManager();

        ExternalDataExchangeService dataService = (ExternalDataExchangeService)this.WFRuntime.GetService(typeof(ExternalDataExchangeService));
        if (dataService == null)
        {
            dataService = new ExternalDataExchangeService();
            WFRuntime.AddService(dataService);
        }

        SpiritucSI.Unilever.SRA.WF.WorkflowManager wfManager = (SpiritucSI.Unilever.SRA.WF.WorkflowManager)dataService.GetService(typeof(WorkflowManager));
        if (wfManager == null)
        {
            wfManager = new WorkflowManager();
            dataService.AddService(wfManager);
        }

        wfManager.InstanceStateChange += new EventHandler<EventArgs>(WF_SRA_OnInstanceStateChange);
        wfManager.WorkflowException += new EventHandler<EventArgs>(WF_SRA_OnWorkflowException);

        Guid newuid = wfManager.StartWorkflow(this.WFRuntime);

        //// Faz update do objecto que está debaixo do workflow com o estado inicial do mesmo
        //Proposals oProp = new Proposals();
        //oProp.UpdateCurrentState(this.idProposal, this.WFInstance.CurrentState.QualifiedName);

        wfManager.InstanceStateChange -= new EventHandler<EventArgs>(WF_SRA_OnInstanceStateChange);
        wfManager.WorkflowException -= new EventHandler<EventArgs>(WF_SRA_OnWorkflowException);

        return newuid;
    }

    public Guid RunWorkflow(int newID_Contrato, string state)
    {
        this._newID_Contrato = newID_Contrato;



            
            ExternalDataExchangeService dataService = (ExternalDataExchangeService)this.WFRuntime.GetService(typeof(ExternalDataExchangeService));
            if (dataService == null)
            {
                dataService = new ExternalDataExchangeService();
                WFRuntime.AddService(dataService);
            }

            SpiritucSI.Unilever.SRA.WF.WorkflowManager wfManager = (SpiritucSI.Unilever.SRA.WF.WorkflowManager)dataService.GetService(typeof(WorkflowManager));
            if (wfManager == null)
            {
                wfManager = new WorkflowManager();
                dataService.AddService(wfManager);
            }


        wfManager.InstanceStateChange += new EventHandler<EventArgs>(WF_SRA_OnInstanceStateChange);
        wfManager.WorkflowException += new EventHandler<EventArgs>(WF_SRA_OnWorkflowException);

        Guid newuid = wfManager.StartWorkflow(this.WFRuntime, state);

        //// Faz update do objecto que está debaixo do workflow com o estado inicial do mesmo
        //Proposals oProp = new Proposals();
        //oProp.UpdateCurrentState(this.idProposal, this.WFInstance.CurrentState.QualifiedName);

        wfManager.InstanceStateChange -= new EventHandler<EventArgs>(WF_SRA_OnInstanceStateChange);
        wfManager.WorkflowException -= new EventHandler<EventArgs>(WF_SRA_OnWorkflowException);

        return newuid;
    }
    #endregion
    #region WORKFLOW EVENTS
    /// <summary>
    /// Método executado pelo workflow quando no momento do ManualWorkflowSchedulerService.RunWorkflow(GUID) 
    ///  seja despoletado o evento de mudança de estado
    /// </summary>
    void WF_SRA_OnInstanceStateChange(object sender, EventArgs e)
    {
        try
        {
            SRA bllSRA = new SRA(ConfigurationManager.AppSettings["DBCS"]);
            
            //PARA SINCRONIZAR EVENTOS, O INSTANCE ID TEM DE SER MANUAL NO ORACLE
            if (((InstanceStateChangeEventArgs)e).State.ToUpper().Equals("STARTED"))
            {
                bllSRA.UpdateContratoWF_ID(this._newID_Contrato, ((InstanceStateChangeEventArgs)e).InstanceID);
            }
            else
            {
                bllSRA.UpdateContratoWFState(((InstanceStateChangeEventArgs)e).InstanceID, ((InstanceStateChangeEventArgs)e).State.ToUpper());
            }

            string msg = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> NOVO ESTADO: " + ((InstanceStateChangeEventArgs)e).State + "\n";
            bllSRA.AppendContratoObsMotivo(((InstanceStateChangeEventArgs)e).InstanceID, msg);            
        }
        catch (Exception ex)
        {
            this.wfException = new Exception("Não foi possível enviar o email", ex);
            //ExceptionPolicy.HandleException(ex, "LogOnly");
        }
    }
    /// <summary>
    /// Método executado pelo workflow quando no momento do ManualWorkflowSchedulerService.RunWorkflow(GUID)
    ///  seja lançada uma excepção
    /// </summary>
    void WF_SRA_OnWorkflowException(object sender, EventArgs e)
    {
        try
        {
            this.wfException = ((ExceptionEventArgs)e).Exception;
            if (((ExceptionEventArgs)e).Exception.Message.Contains("EXCOD_01"))
                throw new WFProceedConditions(((ExceptionEventArgs)e).Exception.Message, ((ExceptionEventArgs)e).Exception);
            else
            {
                if (((ExceptionEventArgs)e).Exception.Message.Contains("EXCOD_02"))
                    throw new WFProceedConditions(((ExceptionEventArgs)e).Exception.Message, ((ExceptionEventArgs)e).Exception);
            }
        }
        catch (Exception ex)
        {
            ExceptionPolicy.HandleException(ex, "GlobalPolicy");
        }
    }
    #endregion
    #endregion

    #region DATABASE
    // USADO NO ODS QUE ALIMENTA A GRID!! NÃO MEXER!!
    public int UsersCount()
    {
        MyMembership ur = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        return ur.Users_Get().Rows.Count;
    }

    // USADO NO ODS QUE ALIMENTA A GRID!! NÃO MEXER!!
    //public DataTable Get(string Username, string Name, string Email, string orderBy, int maximumRows, int startRowIndex)
    public DataTable UsersGet()
    {
        ///TODO 10: corrigir
        //this.Users = UserService.GetAll(Username ?? "", Name ?? "", Email ?? "", startRowIndex, maximumRows, orderBy, this.CurrentUser);
        //return this.Users;

        MyMembership ur = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        return ur.Users_Get();
    } 
    #endregion

}

