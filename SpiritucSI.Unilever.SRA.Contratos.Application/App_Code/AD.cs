using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using System.DirectoryServices;
using System.Text;
using System.Configuration;


/// <summary>
/// Summary description for AD.
/// </summary>
public class AD : System.ComponentModel.Component
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.Container components = null;

    public AD(System.ComponentModel.IContainer container)
    {
        container.Add(this);
        InitializeComponent();
    }

    public AD()
    {
        InitializeComponent();
    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            if (components != null)
            {
                components.Dispose();
            }
        }
        base.Dispose(disposing);
    }


    #region Component Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
    }
    #endregion

    public bool AuthenticateUser(string domain, string username, string password)
    {
        string domainAndUsername = domain + @"\" + username;

        string LDAPPATH = "LDAP://flhsptgad/DC=PTG01,DC=li,DC=u2543,DC=unilever,DC=com";
        //string LDAPPATH = "LDAP://" + domain;

        DirectoryEntry entry = new DirectoryEntry(LDAPPATH, domainAndUsername, password);
        try
        {
            // Bind to the native AdsObject to force authentication.
            Object obj = entry.NativeObject;
            DirectorySearcher search = new DirectorySearcher(entry);
            search.Filter = "(SAMAccountName=" + username + ")";
            search.PropertiesToLoad.Add("cn");
            //search.PropertiesToLoad.Add("Groups");
            SearchResult result = search.FindOne();

            if (null == result)
            {
                return false;
            }

            // Update the new path to the user in the directory
            LDAPPATH = result.Path;
        }
        catch (Exception ex)
        {
            throw new Exception("ERRO AO AUTENTICAR O UTILIZADOR.", ex);
        }

        return true;
    }

    public ArrayList GetUserGroups(string _domain, string _username, string password)
    {
        System.DirectoryServices.DirectoryEntry myDE = new DirectoryEntry("LDAP://" + _domain, _username, password);
        DirectorySearcher mySearcher = new DirectorySearcher(myDE);
        mySearcher.Filter = "sAMAccountName=" + _username;
        mySearcher.PropertiesToLoad.Add("memberOf");

        int propertyCount;
        ArrayList al = new ArrayList();



        SearchResult myResult = mySearcher.FindOne();
        propertyCount = myResult.Properties["memberOf"].Count;

        string dn;
        int equalsIndex;
        int commaIndex;

        for (int i = 0; i < propertyCount; i++)
        {
            dn = myResult.Properties["memberOf"][i].ToString();
            equalsIndex = dn.IndexOf("=", 1);
            commaIndex = dn.IndexOf(",", 1);

            if (equalsIndex == -1)
                return null;
            else
                al.Add(dn.Substring(equalsIndex + 1, commaIndex - equalsIndex - 1));
        }


        return al;
    }





}


public class SecurityGroups
{
    DirectoryEntry _searchRoot;
    SearchResult _userResult;

    string[] _groups = new string[] { };

    public SecurityGroups(Guid userGuid, DirectoryEntry searchRoot)
    {
        if (searchRoot == null)
            throw new ArgumentNullException("searchRoot");

        _searchRoot = searchRoot;

        FindOne(String.Format("(objectGuid={0})", BuildOctetString(userGuid.ToByteArray())));
    }

    public SecurityGroups(string sAMAccountName, DirectoryEntry searchRoot)
    {
        if (searchRoot == null)
            throw new ArgumentNullException("searchRoot");

        _searchRoot = searchRoot;

        FindOne(String.Format("(&(objectCategory=person)(sAMAccountName={0}))", sAMAccountName));
    }

    private void FindOne(string ldapFilter)
    {
        DirectorySearcher ds = new DirectorySearcher(_searchRoot, ldapFilter);

        using (SearchResultCollection src = ds.FindAll())
        {
            if (src.Count > 0)
                _userResult = src[0];
        }

        if (_userResult == null)
            throw new ArgumentException("User not Found");
    }

    public string[] Groups
    {
        get
        {
            if (_groups.Length == 0)
            {
                StringBuilder sb = new StringBuilder();

                //we are building an '|' clause
                sb.Append("(|");

                using (DirectoryEntry user = _userResult.GetDirectoryEntry())
                {
                    //we must ask for this one first
                    user.RefreshCache(new string[] { "tokenGroups" });

                    foreach (byte[] sid in user.Properties["tokenGroups"])
                    {
                        //append each member into the filter
                        sb.AppendFormat("(objectSid={0})", BuildOctetString(sid));
                    }
                }

                //end our initial filter
                sb.Append(")");

                //now create and pull in one search
                using (DirectorySearcher ds = new DirectorySearcher(_searchRoot, sb.ToString(), new string[] { "distinguishedName" }))
                {
                    using (SearchResultCollection src = ds.FindAll())
                    {
                        string[] groups = new string[src.Count];

                        for (int i = 0; i < src.Count; i++)
                        {
                            //groups[i] = src[i].Properties["distinguishedName"][0].ToString();
                            groups[i] = src[i].Properties["distinguishedName"][0].ToString();

                        }

                        _groups = groups;
                    }
                }
            }

            return _groups;
        }
    }

    private string BuildOctetString(byte[] bytes)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < bytes.Length; i++)
        {
            sb.AppendFormat("\\{0}", bytes[i].ToString("X2"));
        }
        return sb.ToString();
    }

}


public class Utilizador
{
    #region variaveis
    public string username;
    string password;
    string domain;
    public ArrayList UserGroups;
    public bool IsValidated;

    #endregion


    public Utilizador(string Username, string Password, string Domain)
    {
        this.username = Username;
        this.password = Password;
        this.domain = Domain;

        IsValidated = IsValidUser();
        UserGroups = GetUserGroups();

        ///TODO: Comentar Linha. Usada apenas para teste
        if (Username.ToLower() == "consultor" && IsValidated)
        {
            UserGroups.Add("Admin-BO");
        }
    }

    private bool IsValidUser()
    {
        AD adUser = new AD();
        return adUser.AuthenticateUser(domain, username, password);
    }

    private ArrayList GetUserGroups()
    {
        string LDAPPATH = "LDAP://flhsptgad/DC=PTG01,DC=li,DC=u2543,DC=unilever,DC=com";
        //string LDAPPATH = "LDAP://" + domain;

        DirectoryEntry searchRoot = new DirectoryEntry(LDAPPATH, username, password, AuthenticationTypes.Secure);
        SecurityGroups SG = new SecurityGroups(username, searchRoot);

        ArrayList alUsersGroups = new ArrayList();

        foreach (string s in SG.Groups)
        {
            alUsersGroups.Add(s.Split(char.Parse(","))[0].Replace("CN=", ""));
        }

        return alUsersGroups;
    }

    public UserRoles PermissionLevel()
    {
        if (ValidateGroupRoles(ConfigurationManager.AppSettings["G_Administrator"].Split(char.Parse("|"))))
        {
            return UserRoles.Administrator;
        }
        else if (ValidateGroupRoles(ConfigurationManager.AppSettings["G_PowerUser"].Split(char.Parse("|"))))
        {
            return UserRoles.PowerUser;
        }
        else if (ValidateGroupRoles(ConfigurationManager.AppSettings["G_Viewer"].Split(char.Parse("|"))))
        {
            return UserRoles.Viewer;
        }
        else
        {
            return UserRoles.None;
        }
   }

    private bool ValidateGroupRoles(string[] roles)
    {
        foreach (string s in roles)
        {
            if (UserGroups.Contains(s))
            {
                return true;
            }

        }
        return false;
    }

}

public enum UserRoles
{
    Administrator = 3, PowerUser = 2, Viewer = 1, None = 0
}






