<%@ Page Language="c#" CodeFile="contrato.aspx.cs" AutoEventWireup="true" EnableEventValidation="true"
    Inherits="contrato" %>

<%@ Register TagPrefix="UC1" TagName="ucBudgetSummary" Src="~/UserControls/ucBudgetSummary.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link id="C_link_Style" runat="server" href='<%# (this.Request["PM"]!=null?"App_Themes/Unilever_Print.css":"App_Themes/SRA/Unilever.css") %>'
        type="text/css" rel="stylesheet" />
    <link href="App_Themes/SRA/SpiritucSI.css" type="text/css" rel="stylesheet" />
    <title>Contratos - Formul�rio</title>
</head>
<body>
    <form id="Form1" method="post" runat="server" onsubmit="javascript: return MySubmit();" enctype="multipart/form-data">

    <script language="javascript" type="text/javascript" src="Scripts/SRAScript.js"></script>

    <%-- JANELA DE PROCESSAMENTO  ########################################################################## --%>
    <AJAX:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" AsyncPostBackTimeout="6000">
    </AJAX:ScriptManager>
    <AJAX:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px; position: absolute;
                left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF; z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </AJAX:UpdateProgress>
    <%-- CONTE�DO ######################################################################################### --%>
    <AJAX:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:LinkButton ID="Dummy" runat="server"></asp:LinkButton>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="Box_Header" runat="server" CssClass="box" DefaultButton="Dummy">
                <UC:Header ID="Header1" runat="server" Title="CONTRATO DE COOPERA��O COMERCIAL"></UC:Header>
            </asp:Panel>
            <%-- TITULO  ################################################################################### --%>
            <asp:Panel ID="boxTitulo" runat="server" CssClass="box" DefaultButton="Dummy">
                <table class="table" align="center">
                    <tr>
                        <%-- TITULO --%>
                        <td align="center" width="100%" style='<%= "display:"+(this.is_PrintMode?"none": "Block") %>'>
                            <asp:Label ID="lbl_SubTitulo" runat="server" CssClass="tituloGrande"></asp:Label>
                        </td>
                        <%-- PRINT --%>
                        <td align="center">
                            <asp:Image ID="C_btn_print" runat="server" Visible='<%# !(this.Request["idContrato"]==null || this.is_PrintMode) %>'
                             meta:resourcekey="C_btn_print"></asp:Image>
                            <asp:Label ID="C_lbl_print" runat="server" CssClass="button" Visible='<%# !(this.Request["idContrato"]==null || this.is_PrintMode) %>'>IMPRIMIR</asp:Label>
                            <asp:RadioButtonList ID="rbPrintPDF" runat="server" OnSelectedIndexChanged="rbPrintPDF_SelectedIndexChanged" AutoPostBack="true" Visible='<%# !(this.Request["idContrato"]==null || this.is_PrintMode) %>'>
                                <asp:ListItem Text="Parcial" Value="header" Selected="false" Enabled="true"></asp:ListItem>
                                <asp:ListItem Text="Final" Value="full" Selected="false" Enabled="true"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:HyperLink ID="lnkPrintPDF" runat="server" Target="_blank" NavigateUrl="#" Visible='false'>PRINT PDF</asp:Hyperlink>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <p>
                &nbsp;<asp:Panel ID="Panel2" runat="server" CssClass="box" HorizontalAlign="Left" DefaultButton="Dummy">
                    <asp:Label class="errorLabel" ID="lblError" runat="server"></asp:Label>
                    <asp:Label class="warningLabel" ID="lblWarning" runat="server"></asp:Label>
                    <asp:ValidationSummary ID="Valid_Summary_Processo" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Processo"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_Entidades" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Entidades"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_PontoVenda" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="PontoVenda"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_Condicoes" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Condicoes"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_Contrapartidas" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Contrapartidas"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_ContrapartidasGlobais" runat="server" CssClass="textValidaTOR"
                        ShowSummary="True" DisplayMode="BulletList" ValidationGroup="ContrapartidasGlobais"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_TPR" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="TPR"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="Valid_Summary_Debitos" runat="server" CssClass="textValidaTOR" ShowSummary="True"
                        DisplayMode="BulletList" ValidationGroup="Debitos"></asp:ValidationSummary>
                </asp:Panel>
                <asp:CustomValidator ID="C_val_Contrato" runat="server" meta:resourcekey="C_val_Contrato" ValidationGroup="contrato"
                    Display="Dynamic" CssClass="textValidator" EnableClientScript="false" OnServerValidate="C_val_Contrato_validate"></asp:CustomValidator>
                <p>
                </p>
                <%-- DADOS DO CONTRATO  ######################################################################## --%>
                <p>
                    &nbsp;<asp:Panel ID="boxContrato" runat="server" CssClass="box" DefaultButton="Dummy">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <%-- HEADER  ######################################################### --%>
                            <tr onclick="javascript:ShowHide('processo');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                                <td class="titulo_peq tituloBarra" width="1%">
                                    <%if (!this.is_PrintMode)
                                  {%>
                                    <label ID="C_btn_processo_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                    �</label> <%} %>
                                </td>
                                <td class="titulo_peq tituloBarra">
                                    <%= this.GetLocalResourceObject("Panel_Contrato.Header").ToString() %>
                                </td>
                                <td class="titulo_peq tituloBarra" width="1%">
                                    &nbsp;
                                </td>
                            </tr>
                            <%-- CONTENT ######################################################### --%>
                            <tr ID="processo">
                                <td class="cell" colspan="3">
                                    <table class="table">
                                        <tr>
                                            <%-- PROCESSO  ##################################################################### --%>
                                            <td valign="top" width="33%">
                                                <table ID="C_box_Proposta" runat="server" border="0" cellpadding="0" cellspacing="0" width="99%">
                                                    <tr>
                                                        <td class="tituloGrande tituloListagem" width="2%">
                                                            <asp:ImageButton ID="C_btn_Process_Previous" runat="server" ImageUrl="~/images/TipLeft.ico" 
                                                                ToolTip="N� Processo Anterior" />
                                                        </td>
                                                        <td class="tituloGrande tituloListagem">
                                                            <%= this.GetLocalResourceObject("Panel_Processo.Header").ToString() %>
                                                        </td>
                                                        <td class="tituloGrande tituloListagem" width="2%">
                                                            <asp:ImageButton ID="C_btn_Process_Next" runat="server" ImageUrl="~/images/Tip.ico" 
                                                                ToolTip="N� Processo Seguinte" />
                                                        </td>
                                                    </tr>
                                                    <%-- CONTENT  ############################################################## --%>
                                                    <tr>
                                                        <td class="readonly" colspan="3">
                                                            <table border="0" cellpadding="0" cellspacing="2" width="100%">
                                                                <%-- PROCESSO ANTERIOR --%>
                                                                <tr ID="Contrato_lblProcessoAnterior" runat="server">
                                                                    <td align="right" class="titulo_peq" style="text-align: right" width="30%">
                                                                        <%= this.NumeroAnterior != "" ? this.GetLocalResourceObject("Panel_Processo.Anterior").ToString() : "&nbsp;"%>
                                                                    </td>
                                                                    <td align="left" width="70%">
                                                                        &nbsp;
                                                                        <asp:HyperLink ID="Contrato_txtProcessoAnterior" runat="server" CssClass="IMPORTED_FRONT"></asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                                <%-- PROCESSO ACTUAL --%>
                                                                <tr>
                                                                    <td align="right" class="titulo_peq" style="text-align: right" width="30%">
                                                                        <%= this.GetLocalResourceObject("Panel_Processo.Actual").ToString()%>
                                                                    </td>
                                                                    <td align="left" width="70%">
                                                                        <asp:TextBox ID="C_txt_Processo_Actual" runat="server" AutoPostBack="false" CausesValidation="true" 
                                                                            CssClass="textboxBig" MaxLength="15" meta:resourcekey="C_txt_Emissor" 
                                                                            OnTextChanged="C_txt_NumProcesso_Changed" Style="text-align: left;" Width="100%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <%-- PROCESSO SEGUINTE --%>
                                                                <tr ID="Contrato_lblProcessoSeguinte" runat="server">
                                                                    <td align="right" class="titulo_peq" style="text-align: right" width="30%">
                                                                        <%= this.Contrato_txtProcessoSeguinte.Text != "" ? this.GetLocalResourceObject("Panel_Processo.Seguinte").ToString() : "&nbsp;"%>
                                                                    </td>
                                                                    <td align="left" width="70%">
                                                                        &nbsp;
                                                                        <asp:HyperLink ID="Contrato_txtProcessoSeguinte" runat="server" CssClass="IMPORTED_FRONT"></asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                                <%-- VALIDATORS --%>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CustomValidator ID="C_Val_Contrato_txtProcesso" runat="server" 
                                                                            ControlToValidate="C_txt_Processo_Actual" CssClass="textValidator" Display="Dynamic" 
                                                                            EnableClientScript="false" meta:resourcekey="C_Val_Contrato_txtProcesso" 
                                                                            OnServerValidate="Contrato_val_txtProcesso_Validate" SetFocusOnError="true" 
                                                                            ValidateEmptyText="false" ValidationGroup="Processo"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%-- TIPO / CLASSIFICA��O  ##################################################################### --%>
                                            <td valign="top" width="33%">
                                                <table class="table" width="100%">
                                                    <tr>
                                                        <td align="right" class="titulo_peq" style="text-align: right" width="10%">
                                                            <%= this.GetLocalResourceObject("Panel_Contrato.Tipo").ToString()%>
                                                        </td>
                                                        <td align="left">
                                                            <asp:DropDownList ID="Contrato_ddlTipo" runat="server" CssClass="textbox" Width="98%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="titulo_peq" style="text-align: right" width="10%">
                                                            <%= this.GetLocalResourceObject("Panel_Contrato.Classificacao").ToString()%>
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="Contrato_txtProcessoClassif" runat="server" CssClass="txtNormal" text-align="left" 
                                                                Width="100%"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%-- DATAS  ##################################################################### --%>
                                            <td align="right" style="text-align: right;" valign="top" width="33%">
                                                <table class="table" width="100%">
                                                    <%-- DATA FIM EFECTIVO --%>
                                                    <tr>
                                                        <td align="right" class="titulo_peq" style="text-align: right" width="50%">
                                                            <asp:Label ID="Contrato_lblDataFimEfectivo" runat="server" CssClass="titulo_peq" 
                                                                meta:resourcekey="Contrato_lblDataFimEfectivo" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="Contrato_txtDataFimEfectivo" runat="server" CssClass="txtNormal"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%-- DATA_CONTRATO --%>
                                                    <tr>
                                                        <td align="right" class="titulo_peq" style="text-align: right" width="10%">
                                                            <%= this.GetLocalResourceObject("Panel_Contrato.Data").ToString()%>
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="C_Contrato_lbl_Data" runat="server" CssClass="txtNormal" Width="100%"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%-- STATUS --%>
                                                    <tr>
                                                        <td align="right" class="titulo_peq" style="text-align: right" width="10%">
                                                            <%= this.GetLocalResourceObject("Panel_Contrato.Estado").ToString()%>
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="Contrato_txtProcessoEstado" runat="server" CssClass="txtNormal" Width="100%"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <p>
                    </p>
                    <%-- PREVIS�O DE INVESTIMENTO  ###################################################################### --%>
                    <p style="page-break-before: auto; page-break-after: auto">
                        &nbsp;<asp:Panel ID="boxInvestimento" runat="server" CssClass="box" DefaultButton="Dummy">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- GROUP HEADER  ######################################################### --%>
                                <tr onclick="javascript:ShowHide('Investimento');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                                    <td class="titulo_peq tituloBarra" width="1%">
                                        <%if (!this.is_PrintMode)
                                  {%>
                                        <label ID="C_btn_Investimento_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                        �</label> <%}%>
                                    </td>
                                    <td class="titulo_peq tituloBarra">
                                        <%= this.GetLocalResourceObject("Panel_Investimento.Header").ToString() %>
                                    </td>
                                    <td class="titulo_peq tituloBarra" width="1%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <%-- CONTENT ####################################################################### --%>
                                <tr ID="Investimento">
                                    <td class="cell" colspan="3">
                                        <table class="table backColor" width="100%">
                                            <tr ID="investimentocell" runat="server">
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <%-- TABELA ############################################--%>
                                                            <td align="center" valign="top" width="30%">
                                                                <asp:DataGrid ID="C_dg_Investimento" runat="server" AutoGenerateColumns="False" 
                                                                    BackColor="Transparent" BorderStyle="None" CellSpacing="2" GridLines="None">
                                                                    <HeaderStyle BackColor="#99CCFF" ForeColor="#00008B" Height="20px" HorizontalAlign="Center" />
                                                                    <ItemStyle CssClass="ListRow" />
                                                                    <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                    <Columns>
                                                                        <%-- GAMA --%>
                                                                        <asp:TemplateColumn HeaderText="GAMA">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem, "GAMA")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%-- PREVIS�O --%>
                                                                        <asp:TemplateColumn HeaderText="PREVIS�O">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem, "PREVISAO") + " �"%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%-- DESCONTO --%>
                                                                        <asp:TemplateColumn HeaderText="DESCONTO">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem, "DESCONTO") + " �"%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%-- BONUS --%>
                                                                        <asp:TemplateColumn HeaderText="BONUS">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem, "BONUS") + " �"%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </td>
                                                            <%-- DURA��O ############################################--%>
                                                            <td align="left" valign="top" width="25%">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr valign="middle">
                                                                        <td align="right">
                                                                            <label class="titulo_peq" style="text-decoration: underline;">
                                                                            TEMPO DE CONTRATO:</label>&nbsp;&nbsp;
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="C_lbl_Invest_Tempo" runat="server" CssClass="titulo_peq"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="middle">
                                                                        <td align="right">
                                                                            <label class="titulo_peq" style="text-decoration: underline;">
                                                                            DATA FIM PREVISTA:</label>&nbsp;&nbsp;
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="C_lbl_Invest_Data" runat="server" CssClass="titulo_peq"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="middle">
                                                                        <td align="right">
                                                                            <label class="titulo_peq" style="text-decoration: underline;">
                                                                            PREVIS�O TOTAL:</label>&nbsp;&nbsp;
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="C_lbl_Invest_Previsao" runat="server" CssClass="titulo_peq"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <%-- RESUMO ############################################--%>
                                                            <td align="left" valign="top" width="35%">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr valign="middle">
                                                                        <td align="right">
                                                                            DESCONTOS:&nbsp;&nbsp;
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="C_lbl_Invest_Descontos" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td align="center">
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="C_lbl_Invest_Descontos_Perc" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="middle">
                                                                        <td align="right">
                                                                            BONUS POR GAMA:&nbsp;&nbsp;
                                                                        </td>
                                                                        <td align="right" style="border-bottom: solid 1px black;">
                                                                            +
                                                                        </td>
                                                                        <td align="right" style="border-bottom: solid 1px black;">
                                                                            <asp:Label ID="C_lbl_Invest_Bonus" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="font-weight: bold;" valign="middle">
                                                                        <td align="right">
                                                                            SUB-TOTAL:&nbsp;&nbsp;
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="C_lbl_Invest_Subt1" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="C_lbl_Invest_Subt1_Perc" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="middle">
                                                                        <td align="right">
                                                                            BONUS GLOBAL:&nbsp;&nbsp;
                                                                        </td>
                                                                        <td align="right" style="border-bottom: solid 1px black;">
                                                                            +
                                                                        </td>
                                                                        <td align="right" style="border-bottom: solid 1px black;">
                                                                            <asp:Label ID="C_lbl_Invest_BonusGlobal" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="font-weight: bold;" valign="middle">
                                                                        <td align="right">
                                                                            SUB-TOTAL:&nbsp;&nbsp;
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="C_lbl_Invest_Subt2" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="C_lbl_Invest_Subt2_Perc" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="middle">
                                                                        <td align="right">
                                                                            VERBAS:&nbsp;&nbsp;
                                                                        </td>
                                                                        <td align="right" style="border-bottom: solid 1px black;">
                                                                            +
                                                                        </td>
                                                                        <td align="right" style="border-bottom: solid 1px black;">
                                                                            <asp:Label ID="C_lbl_Invest_Verba" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="middle">
                                                                        <td align="right">
                                                                            <label class="titulo_peq" style="text-decoration: underline;">
                                                                            TOTAL&nbsp;INVESTIMENTO:</label>&nbsp;&nbsp;
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="C_lbl_Invest_Total" runat="server" CssClass="titulo_peq"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        </td>
                                                                        <td align="right" style="border: double 3px #00008B;">
                                                                            <asp:Label ID="C_lbl_Invest_Total_perc" runat="server" CssClass="titulo_peq"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr ID="investimentocell1" runat="server">
                                                <td>
                                                    <table ID="C_invest_Recalc" runat="server" style="background-color: #FFFF99; position: relative;
                                                float: left; top: -60px; left: 100px; text-align: center" visible="false" width="80%">
                                                        <tr>
                                                            <td style="font-size: large">
                                                                <%= this.GetLocalResourceObject("Panel_Investimento.Invalido").ToString() %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton ID="C_btn_InvestimentoRefresh" runat="server" CausesValidation="false" 
                                                                    Font-Size="Large" Font-Underline="false" ForeColor="#00008B" 
                                                                    meta:resourcekey="C_btn_InvestimentoRefresh" OnClick="C_btn_InvestimentoRefresh_Click"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <p>
                        </p>
                        <%-- NIVEL  ######################################################################## --%>
                        <p>
                            &nbsp;<asp:Panel ID="C_box_Nivel" runat="server" CssClass="box" DefaultButton="Dummy">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <%-- GROUP HEADER  ######################################################### --%>
                                    <tr onclick="javascript:ShowHide('Nivel');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                                        <td class="titulo_peq tituloBarra" width="1%">
                                            <%if (!this.is_PrintMode)
                                  {%>
                                            <label ID="C_btn_Nivel_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                            �</label> <%} %>
                                        </td>
                                        <td class="titulo_peq tituloBarra">
                                            <%= this.GetLocalResourceObject("Panel_Nivel.Header").ToString() %>
                                        </td>
                                        <td class="titulo_peq tituloBarra" width="1%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr ID="Nivel">
                                        <td class="backColor cell" colspan="3">
                                            <table width="80%">
                                                <%-- CONCESSIONARIO BASE --%>
                                                <tr>
                                                    <td width="150px">
                                                        <asp:Label ID="C_lbl_ZonaBase" runat="server" Style="padding-left: 20px;" Text="Zona Base"></asp:Label>
                                                    </td>
                                                    <td class="textbox">
                                                        <asp:DropDownList ID="DL_CONCESSIONARIO" runat="server" DataTextField="DES_ZONA" 
                                                            DataValueField="COD_ZONA" Width="100%">
                                                        </asp:DropDownList>
                                                        <asp:CustomValidator ID="C_val_Nivel_Zona" runat="server" CssClass="textValidator" 
                                                            Display="Dynamic" EnableClientScript="false" meta:resourcekey="RV_CONCESSIONARIO_Empty" 
                                                            OnServerValidate="C_val_Nivel_Zona_Validate" ValidationGroup="Entidades"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                                <%-- GRUPO ECON�MICO --%>
                                                <tr>
                                                    <td width="150px">
                                                        <asp:RadioButton ID="C_cb_Nivel_GE" runat="server" AutoPostBack="true" GroupName="Nivel" 
                                                            meta:resourcekey="C_cb_Nivel_GE" OnCheckedChanged="C_cb_Nivel_Changed" Style="cursor: pointer;" />
                                                    </td>
                                                    <td class="textbox">
                                                        <asp:DropDownList ID="C_ddl_GrupoEconomico" runat="server" AutoPostBack="true" CssClass="textbox" 
                                                            DataTextField="ID_NOME_GRUPO_ECONOMICO" DataValueField="ID_GRUPO_ECONOMICO" 
                                                            OnSelectedIndexChanged="C_ddl_GrupoEconomico_Changed" Style="cursor: pointer;" Width="100%">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="C_Nivel_lbl_ValGE" runat="server" CssClass="warningLabel"></asp:Label>
                                                    </td>
                                                </tr>
                                                <%-- SUBGRUPO ECON�MICO --%>
                                                <tr>
                                                    <td width="150px">
                                                        <asp:RadioButton ID="C_cb_Nivel_SGE" runat="server" AutoPostBack="true" GroupName="Nivel" 
                                                            meta:resourcekey="C_cb_Nivel_SGE" OnCheckedChanged="C_cb_Nivel_Changed" Style="cursor: pointer;" />
                                                    </td>
                                                    <td class="textbox">
                                                        <asp:DropDownList ID="C_ddl_SubGrupoEconomico" runat="server" AppendDataBoundItems="true" 
                                                            AutoPostBack="true" CssClass="textbox" DataTextField="ID_NOME_SUBGRUPO_ECONOMICO" 
                                                            DataValueField="ID_SUBGRUPO_ECONOMICO" OnSelectedIndexChanged="C_ddl_SubGrupoEconomico_Changed" 
                                                            Style="cursor: pointer;" Width="100%">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="C_Nivel_lbl_ValSGE" runat="server" CssClass="warningLabel"></asp:Label>
                                                    </td>
                                                </tr>
                                                <%-- LES --%>
                                                <tr>
                                                    <td width="150px">
                                                        <asp:RadioButton ID="C_cb_Nivel_LE" runat="server" AutoPostBack="true" GroupName="Nivel" 
                                                            meta:resourcekey="C_cb_Nivel_LE" OnCheckedChanged="C_cb_Nivel_Changed" Style="cursor: pointer;" />
                                                    </td>
                                                    <td class="textbox">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <p>
                            </p>
                            <%-- PESQUISA DE CLIENTES  ##################################################################### --%>
                            <p>
                                &nbsp;<asp:Panel ID="C_box_Pesquisa" runat="server" CssClass="box" DefaultButton="Dummy">
                                    <UC:Search ID="C_Entidade_Pesquisa" runat="server" DeleteOnSelect="true" 
                                        meta:resourcekey="C_Entidade_Pesquisa" OnSearchResult="C_Entidade_Pesquisa_onSearchResult" 
                                        OnSelectResult="C_Entidade_Pesquisa_onSelectResult" />
                                </asp:Panel>
                                <p>
                                </p>
                                <%-- LISTA DE ENTIDADES  ####################################################################### --%>
                                <p>
                                    &nbsp;<asp:Panel ID="C_box_Entidade" runat="server" CssClass="box" DefaultButton="Dummy">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <%-- HEADER ######################################################################## --%>
                                            <tr onclick="javascript:ShowHide('Entidade');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                                                <td class="titulo_peq tituloBarra" width="1%">
                                                    <%if (!this.is_PrintMode)
                                  {%>
                                                    <label ID="C_btn_Entidade_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                                    �</label> <%} %>
                                                </td>
                                                <td class="titulo_peq tituloBarra">
                                                    <%= this.GetLocalResourceObject("Panel_Entidade.Header").ToString() %>
                                                </td>
                                                <td class="titulo_peq tituloBarra" width="1%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <%-- CONTE�DO ###################################################################### --%>
                                            <tr ID="Entidade">
                                                <td colspan="3">
                                                    <table border="0" cellpadding="0" cellspacing="0" class="table cell" width="100%">
                                                        <%-- CONTAGEM / PAGINA��O --%>
                                                        <tr class="backColor">
                                                            <td class="backColor">
                                                                <asp:Label ID="C_lbl_ClientesNum" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td align="right">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;&nbsp;&nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <UC:ImageTextButton ID="C_Entidades_btn_Paginacao" runat="server" CausesValidation="false" 
                                                                                meta:resourcekey="C_Entidades_btn_Paginacao" OnClick="C_Entidades_btn_Paginacao_Click" 
                                                                                TextCssClass="button" TextPosition="Rigth" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <%-- LISTA --%>
                                                        <tr>
                                                            <td class="backColor" colspan="2">
                                                                <asp:DataGrid ID="C_dg_Entidades" runat="server" AllowPaging="True" AllowSorting="True" 
                                                                    AutoGenerateColumns="False" BorderStyle="None" CellSpacing="2" GridLines="None" 
                                                                    OnItemCommand="C_dg_Entidades_ItemCommand" OnPageIndexChanged="C_dg_Entidades_PageIndexChanged" 
                                                                    OnSortCommand="C_dg_Entidades_SortCommand" PageSize="15" Width="100%">
                                                                    <HeaderStyle BackColor="#99CCFF" ForeColor="#00008B" Height="20px" />
                                                                    <ItemStyle CssClass="ListRow" />
                                                                    <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                    <PagerStyle BackColor="Transparent" ForeColor="#00008B" HorizontalAlign="Left" 
                                                                        Mode="NumericPages" />
                                                                    <Columns>
                                                                        <asp:TemplateColumn>
                                                                            <HeaderStyle BackColor="Transparent" ForeColor="Transparent" Width="1%" />
                                                                            <ItemStyle BackColor="Transparent" HorizontalAlign="Right" VerticalAlign="Middle" Width="1%" />
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="C_btn_dgEntidades_Edit" runat="server" CausesValidation="false" 
                                                                                    CommandName="Edit" meta:resourcekey="C_btn_dgEntidades_Edit" />
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:ImageButton ID="C_btn_dgEntidades_Save" runat="server" CausesValidation="false" 
                                                                                                CommandName="Save" meta:resourcekey="C_btn_dgEntidades_Save" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:ImageButton ID="C_btn_dgEntidades_Undo" runat="server" CausesValidation="false" 
                                                                                                CommandName="Undo" meta:resourcekey="C_btn_dgEntidades_Undo" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </EditItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="DATA&nbsp;INICIO" SortExpression="DATA_INICIO">
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem, "DATA_INICIO")==System.DBNull.Value ? "":((DateTime)DataBinder.Eval(Container.DataItem, "DATA_INICIO")).ToString("dd-MM-yyyy")%>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                <UC:DatePicker ID="C_Entidades_Data_Inicio" runat="server" 
                                                                                    MinEnabledDate="<%# this.Condicoes_txtDataInicio.SelectedDate %>" 
                                                                                    SelectedDate='<%# DataBinder.Eval(Container.DataItem, "DATA_INICIO")==System.DBNull.Value ? DateTime.MinValue : DataBinder.Eval(Container.DataItem, "DATA_INICIO")%>' />
                                                                            </EditItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="&nbsp;DATA&nbsp;FIM&nbsp;" SortExpression="DATA_FIM">
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem, "DATA_FIM")==System.DBNull.Value ? "":((DateTime)DataBinder.Eval(Container.DataItem, "DATA_FIM")).ToString("dd-MM-yyyy")%>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                <UC:DatePicker ID="C_Entidades_Data_Fim" runat="server" 
                                                                                    MinEnabledDate="<%# this.Condicoes_txtDataInicio.SelectedDate %>" 
                                                                                    SelectedDate='<%# DataBinder.Eval(Container.DataItem, "DATA_FIM")==System.DBNull.Value ? DateTime.MinValue : DataBinder.Eval(Container.DataItem, "DATA_FIM")%>' />
                                                                            </EditItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="DES_GRUPO_ECONOMICO_LE" HeaderText="GRUPO" ReadOnly="true" 
                                                                            SortExpression="DES_GRUPO_ECONOMICO_LE"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DES_SUBGRUPO_ECONOMICO_LE" HeaderText="SUB GRUPO" ReadOnly="true" 
                                                                            SortExpression="DES_SUBGRUPO_ECONOMICO_LE"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DES_CONCESSIONARIO" HeaderText="CONCESSION�RIO" ReadOnly="true" 
                                                                            SortExpression="DES_CONCESSIONARIO"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DES_CLIENTE" HeaderText="CLIENTE" ReadOnly="true" 
                                                                            SortExpression="DES_CLIENTE"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="COD_LOCAL_ENTREGA" HeaderText="C�DIGO LE" ReadOnly="true" 
                                                                            SortExpression="COD_LOCAL_ENTREGA"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DES_LOCAL_ENTREGA" HeaderText="LOCAL DE ENTREGA" ReadOnly="true" 
                                                                            SortExpression="DES_LOCAL_ENTREGA"></asp:BoundColumn>
                                                                        <asp:TemplateColumn>
                                                                            <HeaderStyle BackColor="Transparent" ForeColor="Transparent" Width="1%" />
                                                                            <ItemStyle BackColor="Transparent" HorizontalAlign="Right" VerticalAlign="Middle" Width="1%" />
                                                                            <HeaderTemplate>
                                                                                <asp:ImageButton ID="C_Entidades_btn_Clear" runat="server" CausesValidation="false" 
                                                                                    CommandName="DeleteAll" meta:resourcekey="C_Entidades_btn_Clear" 
                                                                                    Visible='<%# this.dtblClientesLEs.DefaultView.Count >0 && this.C_dg_Entidades.Attributes["CanUse"]=="true" && this.C_cb_Nivel_LE.Checked %>' />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="C_btn_dgClientes_Delete" runat="server" CausesValidation="false" 
                                                                                    CommandName="Delete" meta:resourcekey="C_btn_dgClientes_Delete" 
                                                                                    Visible='<%# this.C_dg_Entidades.Attributes["CanUse"]=="true" && this.C_cb_Nivel_LE.Checked %>' />
                                                                                &nbsp;&nbsp;<asp:ImageButton ID="C_btn_dgClientes_Select" runat="server" CausesValidation="false" 
                                                                                    CommandName="Select" meta:resourcekey="C_btn_dgClientes_Select" 
                                                                                    Visible='<%# this.C_dg_Entidades.Attributes["CanUse"]=="true" %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                                <asp:CustomValidator ID="C_val_Entidades_Empty" runat="server" CssClass="textValidator" 
                                                                    Display="Dynamic" EnableClientScript="false" meta:resourcekey="C_val_Entidades_Empty" 
                                                                    OnServerValidate="C_val_Entidades_Empty_validate" ValidationGroup="Entidades"></asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <p>
                                    </p>
                                    <%-- IDENTIFICA��O PONTO VENDA  ################################################################ --%>
                                    <p>
                                        &nbsp;<asp:Panel ID="boxPontoVenda" runat="server" CssClass="box" DefaultButton="Dummy">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <%-- HEADER ######################################################################## --%>
                                                <tr onclick="javascript:ShowHide('PontoVenda');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                                                    <td class="titulo_peq tituloBarra" width="1%">
                                                        <%if (!this.is_PrintMode)
                                  {%>
                                                        <label ID="C_btn_PontoVenda_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                                        �</label> <%} %>
                                                    </td>
                                                    <td class="titulo_peq tituloBarra">
                                                        <%= this.GetLocalResourceObject("Panel_PontoVenda.Header").ToString() %>
                                                    </td>
                                                    <td class="titulo_peq tituloBarra" width="1%">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <%-- CONTE�DO ###################################################################### --%>
                                                <tr ID="PontoVenda">
                                                    <td class="backColor cell" colspan="3">
                                                        <%-- TABELA COM CAMPOS DE IDENTIFICA��O PT VENDA ########################### --%>
                                                        <table cellpadding="2" class="table">
                                                            <%-- NOME --%>
                                                            <tr>
                                                                <td class="label" valign="top" width="14%">
                                                                    Nome da Firma
                                                                </td>
                                                                <td class="textbox" colspan="3" valign="top">
                                                                    <asp:TextBox ID="PontoVenda_txtNome" runat="server" CssClass="textbox" MaxLength="50" Width="100%"></asp:TextBox>
                                                                    <asp:CustomValidator ID="C_PontoVenda_val_txtNome_Empty" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtNome" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="false" meta:resourcekey="C_PontoVenda_val_txtNome_Empty" 
                                                                        OnServerValidate="C_val_txt_Empty_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                        ValidationGroup="PontoVenda"></asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <%-- MORADA --%>
                                                            <tr>
                                                                <td class="label" valign="top">
                                                                    Morada da Sede
                                                                </td>
                                                                <td class="textbox" colspan="3" valign="top">
                                                                    <asp:TextBox ID="PontoVenda_txtMoradaSede" runat="server" CssClass="textbox" Height="50" 
                                                                        MaxLength="255" onkeyDown="return checkTextAreaMaxLength(this,event,'255');" TextMode="MultiLine" 
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:CustomValidator ID="C_PontoVenda_val_txtMorada_Empty" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtMoradaSede" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="false" meta:resourcekey="C_PontoVenda_val_txtMorada_Empty" 
                                                                        OnServerValidate="C_val_txt_Empty_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                        ValidationGroup="PontoVenda"></asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <%-- LOCALIDADE / NIF --%>
                                                            <tr>
                                                                <td class="label" valign="top">
                                                                    Localidade
                                                                </td>
                                                                <td class="textbox" valign="top" width="40%">
                                                                    <asp:TextBox ID="PontoVenda_txtLocalidade" runat="server" CssClass="textbox" MaxLength="50" 
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:CustomValidator ID="C_PontoVenda_val_txtLocalidade_Empty" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtLocalidade" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="false" meta:resourcekey="C_PontoVenda_val_txtLocalidade_Empty" 
                                                                        OnServerValidate="C_val_txt_Empty_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                        ValidationGroup="PontoVenda"></asp:CustomValidator>
                                                                </td>
                                                                <td class="label" valign="top" width="11%">
                                                                    Ramo de Actividade
                                                                </td>
                                                                <td class="textbox" valign="top" width="35%">
                                                                    <asp:TextBox ID="PontoVenda_txtRamo" runat="server" CssClass="textbox" MaxLength="255" Width="100%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <%-- MATRICULA / SOB N� --%>
                                                            <tr>
                                                                <td class="label" valign="top">
                                                                    Matricula na CRC
                                                                </td>
                                                                <td class="textbox" valign="top">
                                                                    <asp:TextBox ID="PontoVenda_txtMatricula" runat="server" CssClass="textbox" MaxLength="50" 
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:CustomValidator ID="C_PontoVenda_val_txtMatricula_Empty" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtMatricula" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="false" meta:resourcekey="C_PontoVenda_val_txtMatricula_Empty" 
                                                                        OnServerValidate="C_val_txt_EmptyGroup_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                        ValidationGroup="PontoVenda"></asp:CustomValidator>
                                                                </td>
                                                                <td class="label" valign="top">
                                                                    Sob o n�
                                                                </td>
                                                                <td class="textbox" valign="top">
                                                                    <asp:TextBox ID="PontoVenda_txtMatriculaNum" runat="server" CssClass="textbox" MaxLength="20" 
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:CustomValidator ID="C_PontoVenda_val_txtMatriculaNum_Empty" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtMatriculaNum" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="false" meta:resourcekey="C_PontoVenda_val_txtMatriculaNum_Empty" 
                                                                        OnServerValidate="C_val_txt_EmptyGroup_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                        ValidationGroup="PontoVenda"></asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <%-- CAPITAL / CONTRIBUINTE --%>
                                                            <tr>
                                                                <td class="label" valign="top">
                                                                    Capital Social
                                                                </td>
                                                                <td class="textbox" valign="top">
                                                                    <asp:TextBox ID="PontoVenda_txtCapital" runat="server" CssClass="textbox" MaxLength="20" 
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="C_PontoVenda_Val_txtCapital" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtCapital" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="true" meta:resourcekey="C_PontoVenda_Val_txtCapital" 
                                                                        ValidationExpression="\d{1,20}" ValidationGroup="PontoVenda"></asp:RegularExpressionValidator>
                                                                    <asp:CustomValidator ID="C_PontoVenda_val_txtCapital_Empty" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtCapital" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="false" meta:resourcekey="C_PontoVenda_val_txtCapital_Empty" 
                                                                        OnServerValidate="C_val_txt_EmptyGroup_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                        ValidationGroup="PontoVenda"></asp:CustomValidator>
                                                                </td>
                                                                <td class="label" valign="top">
                                                                    N� Contribuite
                                                                </td>
                                                                <td class="textbox" valign="top">
                                                                    <asp:TextBox ID="PontoVenda_txtContribuinte" runat="server" CssClass="textbox" MaxLength="9" 
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="C_PontoVenda_val_txtContribuinte" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtContribuinte" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="true" meta:resourcekey="C_PontoVenda_val_txtContribuinte" 
                                                                        ValidationExpression="\d{9}" ValidationGroup="PontoVenda"></asp:RegularExpressionValidator>
                                                                    <asp:CustomValidator ID="C_PontoVenda_val_txtContribuinte_Empty" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtContribuinte" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="false" meta:resourcekey="C_PontoVenda_val_txtContribuinte_Empty" 
                                                                        OnServerValidate="C_val_txt_Empty_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                        ValidationGroup="PontoVenda"></asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <%-- DESIGNA��O --%>
                                                            <tr>
                                                                <td class="label" valign="top">
                                                                    Designa��o Comercial
                                                                </td>
                                                                <td class="textbox" colspan="3" valign="top">
                                                                    <asp:TextBox ID="PontoVenda_txtDesignacao" runat="server" CssClass="textbox" MaxLength="255" 
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:CustomValidator ID="C_PontoVenda_val_txtDesignacao_Empty" runat="server" 
                                                                        ControlToValidate="PontoVenda_txtDesignacao" CssClass="textValidator" Display="Dynamic" 
                                                                        EnableClientScript="false" meta:resourcekey="C_PontoVenda_val_txtDesignacao_Empty" 
                                                                        OnServerValidate="C_val_txt_Empty_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                        ValidationGroup="PontoVenda"></asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="label" valign="top">
                                                                    Morada<br>(se diferente da sede) </br>
                                                                </td>
                                                                <td class="textbox" colspan="3" valign="top">
                                                                    <asp:TextBox ID="PontoVenda_txtMorada2" runat="server" CssClass="textbox" Height="50" 
                                                                        MaxLength="255" onkeyDown="return checkTextAreaMaxLength(this,event,'255');" TextMode="MultiLine" 
                                                                        Width="100%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <p>
                                        </p>
                                        <%-- CONDI��ES DO CONTRATO  #####################"""############################################ --%>
                                        <p style="page-break-before: auto">
                                            &nbsp;<asp:Panel ID="boxCondicoes" runat="server" CssClass="box" DefaultButton="Dummy">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <%-- HEADER ######################################################################## --%>
                                                    <tr onclick="javascript:ShowHide('Condicoes');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                                                        <td class="titulo_peq tituloBarra" width="1%">
                                                            <%if (!this.is_PrintMode)
                                  {%>
                                                            <label ID="C_btn_Condicoes_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                                            �</label> <%} %>
                                                        </td>
                                                        <td class="titulo_peq tituloBarra">
                                                            <%= this.GetLocalResourceObject("Panel_Condicoes.Header").ToString()%>
                                                        </td>
                                                        <td class="titulo_peq tituloBarra" width="1%">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <%-- CONTENT ####################################################################### --%>
                                                    <tr ID="Condicoes">
                                                        <td class="backColor cell" colspan="3">
                                                            <table border="0" class="table">
                                                                <%-- OBJECTIVO ######################################################### --%>
                                                                <tr>
                                                                    <td class="label" valign="top" width="160">
                                                                        Objectivo
                                                                    </td>
                                                                    <td colspan="4" valign="top">
                                                                        <asp:TextBox ID="Condicoes_txtObjectivo" runat="server" CssClass="textbox" MaxLength="50" 
                                                                            Width="100%"></asp:TextBox>
                                                                        <asp:CustomValidator ID="C_Condicoes_Val_txtObjectivo_Empty" runat="server" 
                                                                            ControlToValidate="Condicoes_txtObjectivo" CssClass="textValidator" Display="Dynamic" 
                                                                            EnableClientScript="false" meta:resourcekey="C_Condicoes_Val_txtObjectivo_Empty" 
                                                                            OnServerValidate="C_val_txt_Empty_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                            ValidationGroup="Condicoes"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <%-- DATAS / GAMAS ############################################################# --%>
                                                                <tr>
                                                                    <td class="label" valign="top" width="160">
                                                                        Data de In�cio
                                                                    </td>
                                                                    <td valign="top" width="120">
                                                                        <UC:DatePicker ID="Condicoes_txtDataInicio" runat="server" AllowClear="false" 
                                                                            OnSelectionChanged="Condicoes_txtDataInicio_Changed" />
                                                                    </td>
                                                                    <td align="left" class="label" style="text-align: left;" valign="top" width="120">
                                                                        <asp:RadioButton ID="Condicoes_rb_DataFim" runat="server" AutoPostBack="True" 
                                                                            GroupName="Condicoes_DataFim" meta:resourcekey="Condicoes_rb_DataFim" 
                                                                            OnCheckedChanged="Condicoes_DataFim_CheckChanged" />
                                                                    </td>
                                                                    <td align="left" style="text-align: left;" valign="top">
                                                                        <UC:DatePicker ID="Condicoes_txtDataFim" runat="server" Enabled="false" />
                                                                    </td>
                                                                    <td rowspan="5" valign="top">
                                                                        <fieldset>
                                                                            <legend class="label">Gamas a Considerar</legend>
                                                                            <asp:CheckBoxList ID="Condicoes_cbGamas" runat="server" RepeatColumns="6" 
                                                                                RepeatDirection="Horizontal" Width="99%">
                                                                            </asp:CheckBoxList>
                                                                            <asp:CustomValidator ID="C_Condicoes_Val_cbGamas_Empty" runat="server" CssClass="textValidator" 
                                                                                Display="Dynamic" EnableClientScript="false" Enabled="false" 
                                                                                meta:resourcekey="C_Condicoes_Val_cbGamas_Empty" 
                                                                                OnServerValidate="C_Condicoes_Val_cbGamas_Empty_Validate" ValidationGroup="Condicoes"></asp:CustomValidator>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                                <%-- RENOVA��O ######################################################### --%>
                                                                <tr>
                                                                    <td class="label" valign="top" width="160">
                                                                        Renova��o autom�tica at�
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="Condicoes_txtRenovacao" runat="server" CssClass="textbox" MaxLength="2" 
                                                                            Style="text-align: right" Width="30px"></asp:TextBox>
                                                                        &nbsp;Anos
                                                                        <asp:RangeValidator ID="Condicoes_Val_txtRenovacao" runat="server" 
                                                                            ControlToValidate="Condicoes_txtRenovacao" CssClass="textValidator" Display="Dynamic" 
                                                                            EnableClientScript="true" ErrorMessage="[ANOS RENOVA��O] -&gt; Valor Inv�lido. {1 - 99}" 
                                                                            MaximumValue="99" MinimumValue="0" Type="Integer">Valor Inv�lido. {1 - 99}</asp:RangeValidator>
                                                                    </td>
                                                                    <td align="left" class="label" style="text-align: left;" valign="top">
                                                                        <asp:RadioButton ID="Condicoes_rb_QuantiaFim" runat="server" AutoPostBack="True" 
                                                                            GroupName="Condicoes_DataFim" meta:resourcekey="Condicoes_rb_QuantiaFim" 
                                                                            OnCheckedChanged="Condicoes_DataFim_CheckChanged" />
                                                                        <asp:CustomValidator ID="C_Condicoes_Val_rbDataFim_Empty" runat="server" CssClass="textValidator" 
                                                                            Display="Dynamic" EnableClientScript="false" meta:resourcekey="C_Condicoes_Val_rbDataFim_Empty" 
                                                                            OnServerValidate="C_Condicoes_Val_rbDataFim_Empty_Validate" ValidationGroup="Condicoes"></asp:CustomValidator>
                                                                    </td>
                                                                    <td align="left" class="textbox" style="text-align: left;" valign="top" width="110">
                                                                        <asp:TextBox ID="Condicoes_txtQuandoAtingir" runat="server" CssClass="textbox" Enabled="False" 
                                                                            MaxLength="9" Width="90"></asp:TextBox>
                                                                        &nbsp;<label class="titulo_peq">�</label>
                                                                        <asp:RegularExpressionValidator ID="C_Condicoes_Val_txtQuantiaFim" runat="server" 
                                                                            ControlToValidate="Condicoes_txtQuandoAtingir" CssClass="textValidator" Display="Dynamic" 
                                                                            EnableClientScript="true" Enabled="false" meta:resourcekey="C_Condicoes_Val_txtQuantiaFim" 
                                                                            ValidationExpression="\d{1,9}" ValidationGroup="Condicoes"></asp:RegularExpressionValidator>
                                                                        <asp:CustomValidator ID="C_Condicoes_Val_txtQuantiaFim_Empty" runat="server" 
                                                                            ControlToValidate="Condicoes_txtQuandoAtingir" CssClass="textValidator" Display="Dynamic" 
                                                                            EnableClientScript="false" Enabled="false" meta:resourcekey="C_Condicoes_Val_txtQuantiaFim_Empty" 
                                                                            OnServerValidate="C_val_txt_Empty_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                            ValidationGroup="Condicoes"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <%-- TIPO CONTRATO  ############################################# --%>
                                                                <tr>
                                                                    <td class="label" valign="middle" width="160">
                                                                        Tipo Contrato
                                                                    </td>
                                                                    <td colspan="3" valign="top">
                                                                        <fieldset>
                                                                            <asp:CheckBox ID="Condicoes_ckPooc" runat="server" Text="Pooc" />
                                                                            &nbsp;
                                                                            <asp:CheckBox ID="Condicoes_ckContratoFormal" runat="server" Text="Contrato Formal" />
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                                <%-- EXPLORA��O ######################################################## --%>
                                                                <tr>
                                                                    <td class="label" valign="middle" width="160">
                                                                        Explora��o do Ponto de Venda
                                                                    </td>
                                                                    <td colspan="3" valign="top">
                                                                        <fieldset>
                                                                            <asp:CheckBoxList ID="Condicoes_cbExploracao" runat="server" ForeColor="Black" 
                                                                                RepeatDirection="Horizontal">
                                                                                <asp:ListItem Value="CL">Cliente</asp:ListItem>
                                                                                <asp:ListItem Value="CO">Concession�rio</asp:ListItem>
                                                                                <asp:ListItem Value="LG">U.J.M.</asp:ListItem>
                                                                                <asp:ListItem Value="TR">Terceiros</asp:ListItem>
                                                                            </asp:CheckBoxList>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                                <%-- FORNECIMENTO FRIO ################################################# --%>
                                                                <tr>
                                                                    <td class="label" valign="top" width="160">
                                                                        Fornecimento de Frio
                                                                    </td>
                                                                    <td colspan="3" valign="top">
                                                                        <fieldset>
                                                                            <legend>
                                                                                <asp:RadioButtonList ID="Condicoes_rbFrio" runat="server" AutoPostBack="True" 
                                                                                    OnSelectedIndexChanged="Condicoes_rbFrio_SelectedIndexChanged" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Value="F">Cliente</asp:ListItem>
                                                                                    <asp:ListItem Selected="True" Value="T">U.J.M.</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </legend>
                                                                            <asp:Panel ID="Condicoes_BoxEquipamentos" runat="server" DefaultButton="Dummy" Visible="true" 
                                                                                Width="100%">
                                                                                <table class="table" width="100%">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:DataGrid ID="Condicoes_dgEquipamentos" runat="server" AutoGenerateColumns="False" 
                                                                                                BackColor="Transparent" BorderStyle="None" CellSpacing="2" GridLines="None" 
                                                                                                OnItemCommand="Condicoes_dgEquipamentos_ItemCommand" 
                                                                                                OnItemDataBound="Condicoes_dgEquipamentos_ItemDataBound" 
                                                                                                ShowFooter='<%# this.Condicoes_dgEquipamentos.Attributes["CanUse"]=="True" %>' Width="100%">
                                                                                                <HeaderStyle BackColor="#99CCFF" ForeColor="#00008B" Height="20px" HorizontalAlign="Center" />
                                                                                                <ItemStyle CssClass="ListRow" />
                                                                                                <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                                                <FooterStyle BackColor="Transparent" VerticalAlign="Top" />
                                                                                                <Columns>
                                                                                                    <%-- TIPO EQUIPAMENTO --%>
                                                                                                    <asp:TemplateColumn HeaderText="TIPO DE EQUIPAMENTO">
                                                                                                        <ItemStyle Width="65%" />
                                                                                                        <FooterStyle Width="65%" />
                                                                                                        <ItemTemplate>
                                                                                                            <%# DataBinder.Eval(Container.DataItem,"DES_TIPO_EQUIPAMENTO") %>
                                                                                                        </ItemTemplate>
                                                                                                        <FooterTemplate>
                                                                                                            <br />
                                                                                                            <asp:Panel ID="Panel_Condicoes_Equipamento_Tipo" runat="server" 
                                                                                                                DefaultButton="Condicoes_Equipamento_lnkInserir">
                                                                                                                <asp:ListBox ID="Condicoes_Equipamento_LbTipoE" runat="server" CssClass="textbox" 
                                                                                                                    EnableViewState="True" Rows="1" Style="z-index: 0;" Width="100%"></asp:ListBox>
                                                                                                            </asp:Panel>
                                                                                                        </FooterTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <%-- QUANTIDADE --%>
                                                                                                    <asp:TemplateColumn HeaderText="QUANTIDADE">
                                                                                                        <ItemStyle HorizontalAlign="Right" Width="35%" />
                                                                                                        <FooterStyle Width="35%" />
                                                                                                        <ItemTemplate>
                                                                                                            <%# DataBinder.Eval(Container.DataItem,"QUANTIDADE") %>
                                                                                                        </ItemTemplate>
                                                                                                        <FooterTemplate>
                                                                                                            <br />
                                                                                                            <asp:Panel ID="Panel_Condicoes_Equipamento_Quantidade" runat="server" 
                                                                                                                DefaultButton="Condicoes_Equipamento_lnkInserir">
                                                                                                                <asp:TextBox ID="Condicoes_Equipamento_txtQtd" runat="server" CssClass="textbox" MaxLength="5" 
                                                                                                                    Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                                <br />
                                                                                                                <asp:RangeValidator ID="Condicoes_Equipamento_Val_txtQtd" runat="server" 
                                                                                                                    ControlToValidate="Condicoes_Equipamento_txtQtd" CssClass="textValidator" Display="Dynamic" 
                                                                                                                    EnableClientScript="true" ErrorMessage="[EQUIP. QUANTIDADE] -&gt; Valor Inv�lido. {1 - 99999}" 
                                                                                                                    MaximumValue="99999" MinimumValue="1" SetFocusOnError="true" Type="Integer" 
                                                                                                                    ValidationGroup="Equipamento">Valor Inv�lido. {1 - 99999}</asp:RangeValidator>
                                                                                                                <asp:RequiredFieldValidator ID="Condicoes_Equipamento_Val_ReqtxtQtd" runat="server" 
                                                                                                                    ControlToValidate="Condicoes_Equipamento_txtQtd" CssClass="textValidator" Display="Dynamic" 
                                                                                                                    EnableClientScript="true" ErrorMessage="[EQUIP. QUANTIDADE] -&gt; Indique uma quantidade" 
                                                                                                                    SetFocusOnError="true" ValidationGroup="Equipamento">Indique uma quantidade</asp:RequiredFieldValidator>
                                                                                                            </asp:Panel>
                                                                                                        </FooterTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn>
                                                                                                        <HeaderStyle BackColor="Transparent" ForeColor="Transparent" />
                                                                                                        <ItemStyle BackColor="Transparent" HorizontalAlign="Left" VerticalAlign="Middle" Width="1%" />
                                                                                                        <FooterStyle Width="1%" />
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="C_btn_dgEquipamentos_Delete" runat="server" CausesValidation="false" 
                                                                                                                CommandName="Apagar" meta:resourcekey="C_btn_dgEquipamentos_Delete" 
                                                                                                                OnClientClick="javascript: return confirm('Deseja mesmo remover o equipamento da lista?');" 
                                                                                                                Visible='<%# this.Condicoes_dgEquipamentos.Attributes["CanUse"]=="True" %>' />
                                                                                                        </ItemTemplate>
                                                                                                        <FooterTemplate>
                                                                                                            <br />
                                                                                                            <table cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td align="left" valign="middle">
                                                                                                                        <asp:ImageButton ID="Condicoes_Equipamento_lnkInserir" runat="server" CommandName="Inserir" 
                                                                                                                            CssClass="button" meta:resourcekey="Condicoes_Equipamento_lnkInserir" 
                                                                                                                            ValidationGroup="Equipamento" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </FooterTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                </Columns>
                                                                                            </asp:DataGrid>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                            <asp:CustomValidator ID="C_Condicoes_Val_dgEquipamentos_Empty" runat="server" 
                                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="false" 
                                                                                meta:resourcekey="C_Condicoes_Val_dgEquipamentos_Empty" 
                                                                                OnServerValidate="C_Condicoes_Val_dgEquipamentos_Empty_validate" ValidationGroup="Condicoes"></asp:CustomValidator>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <p>
                                            </p>
                                            <%-- CONTRAPARTIDAS FINANCEIRAS POR GAMA  ###################################################### --%>
                                            <p style="page-break-before: auto; page-break-after: auto">
                                                &nbsp;<asp:Panel ID="boxContrapartidas" runat="server" CssClass="box" DefaultButton="Dummy">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <%-- HEADER ######################################################################## --%>
                                                        <tr onclick="javascript:ShowHide('Contrapartidas');" 
                                                            style="cursor: hand; padding: 3px 3px 3px 3px;">
                                                            <td class="titulo_peq tituloBarra" width="1%">
                                                                <%if (!this.is_PrintMode)
                                  {%>
                                                                <label ID="C_btn_Contrapartidas_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                                                �</label><%} %>
                                                            </td>
                                                            <td class="titulo_peq tituloBarra">
                                                                <%= this.GetLocalResourceObject("Panel_Contrapartidas.Header").ToString()%>
                                                            </td>
                                                            <td class="titulo_peq tituloBarra" width="1%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <%-- CONTENT ####################################################################### --%>
                                                        <tr ID="Contrapartidas">
                                                            <td class="backColor cell" colspan="3">
                                                                <table border="0" class="table">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Contrapartidas_dgContrapartidas_Empty" runat="server" CssClass="tituloGrande">N�o Tem</asp:Label>
                                                                            <asp:DataGrid ID="Contrapartidas_dgContrapartidas" runat="server" AutoGenerateColumns="False" 
                                                                                BorderStyle="None" CellSpacing="2" GridLines="None" 
                                                                                OnItemCommand="Contrapartidas_dgContrapartidas_ItemCommand" 
                                                                                OnItemDataBound="Contrapartidas_dgContrapartidas_ItemDataBound" Width="100%">
                                                                                <HeaderStyle BackColor="#99CCFF" ForeColor="#00008B" Height="20px" HorizontalAlign="Center" />
                                                                                <ItemStyle CssClass="ListRow" />
                                                                                <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                                <FooterStyle BackColor="Transparent" VerticalAlign="Top" />
                                                                                <Columns>
                                                                                    <%-- GAMA ############################################## --%>
                                                                                    <asp:TemplateColumn FooterStyle-Width="25%" HeaderText="GAMA" ItemStyle-Width="25%">
                                                                                        <ItemTemplate>
                                                                                            <%# DataBinder.Eval(Container.DataItem, "PRODUCT_NAME") %>
                                                                                        </ItemTemplate>
                                                                                        <FooterTemplate>
                                                                                            <br />
                                                                                            <asp:Panel ID="Panel_Contrapartidas_Grupo" runat="server" DefaultButton="Contrapartidas_lnkInserir">
                                                                                                <asp:ListBox ID="Contrapartidas_lbGrupo" runat="server" CssClass="textbox" EnableViewState="True" 
                                                                                                    Rows="1" Width="100%"></asp:ListBox>
                                                                                            </asp:Panel>
                                                                                        </FooterTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%-- PREVIS�O DE VENDAS ################################ --%>
                                                                                    <asp:TemplateColumn FooterStyle-Width="10%" HeaderText="PREVIS�O&lt;br/&gt;VENDAS">
                                                                                        <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <%# DataBinder.Eval(Container.DataItem, "PREVISAO_VENDAS") + " �" %>
                                                                                        </ItemTemplate>
                                                                                        <FooterTemplate>
                                                                                            <br />
                                                                                            <asp:Panel ID="Panel_Contrapartidas_Previs�o" runat="server" 
                                                                                                DefaultButton="Contrapartidas_lnkInserir">
                                                                                                <asp:TextBox ID="Contrapartidas_txtPrevisao" runat="server" CssClass="txtNormal" MaxLength="9" 
                                                                                                    Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                <asp:RangeValidator ID="Contrapartidas_Val_txtPrevisao" runat="server" 
                                                                                                    ControlToValidate="Contrapartidas_txtPrevisao" CssClass="textValidator" Display="Dynamic" 
                                                                                                    EnableClientScript="True" ErrorMessage="[PREVIS�O VENDAS] -&gt; Valor Inv�lido. [0-999999999]" 
                                                                                                    MaximumValue="999999999" MinimumValue="0" Type="Integer" ValidationGroup="Contrapartidas">Valor Inv�lido.<br />[0-999999999}</asp:RangeValidator>
                                                                                                <asp:RequiredFieldValidator ID="Contrapartidas_Val_ReqtxtPrevisao" runat="server" 
                                                                                                    ControlToValidate="Contrapartidas_txtPrevisao" CssClass="textValidator" Display="Dynamic" 
                                                                                                    EnableClientScript="True" ErrorMessage="[PREVIS�O VENDAS] -&gt; Indique uma previs�o" 
                                                                                                    ValidationGroup="Contrapartidas">Indique uma previs�o</asp:RequiredFieldValidator>
                                                                                            </asp:Panel>
                                                                                        </FooterTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%-- DESCONTO NA FACTURA ############################### --%>
                                                                                    <asp:TemplateColumn FooterStyle-Width="10%" HeaderText="DESCONTO&lt;br/&gt;FACTURA">
                                                                                        <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <%# DataBinder.Eval(Container.DataItem, "DESCONTO_FACTURA").ToString().Length>0?DataBinder.Eval(Container.DataItem, "DESCONTO_FACTURA") +" %" : "- - - "%>
                                                                                        </ItemTemplate>
                                                                                        <FooterTemplate>
                                                                                            <br />
                                                                                            <asp:Panel ID="Panel_Contrapartidas_Desconto" runat="server" 
                                                                                                DefaultButton="Contrapartidas_lnkInserir">
                                                                                                <asp:TextBox ID="Contrapartidas_txtDesconto" runat="server" CssClass="txtNormal" MaxLength="5" 
                                                                                                    Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                <asp:RangeValidator ID="Contrapartidas_Val_txtDesconto" runat="server" 
                                                                                                    ControlToValidate="Contrapartidas_txtDesconto" CssClass="textValidator" Display="Dynamic" 
                                                                                                    EnableClientScript="true" ErrorMessage="[DESCONTO]     -&gt; Valor Inv�lido. [0,01-100]" 
                                                                                                    MaximumValue="100" MinimumValue="0,01" Type="Double" ValidationGroup="Contrapartidas">Valor Inv�lido.<br />[0,01-100]</asp:RangeValidator>
                                                                                            </asp:Panel>
                                                                                        </FooterTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%-- COMPARTICIPA��O ################################### --%>
                                                                                    <asp:TemplateColumn FooterStyle-Width="10%" HeaderText="COMPARTICIPA��O">
                                                                                        <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <%# DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO").ToString().Length>0? DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO") + " %" : "- - - " %>
                                                                                        </ItemTemplate>
                                                                                        <FooterTemplate>
                                                                                            <br />
                                                                                            <asp:Panel ID="Panel_Contrapartidas_Compart" runat="server" 
                                                                                                DefaultButton="Contrapartidas_lnkInserir">
                                                                                                <asp:TextBox ID="Contrapartidas_txtComparticipacao" runat="server" CssClass="txtNormal" 
                                                                                                    MaxLength="5" Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                <asp:RangeValidator ID="Contrapartidas_Val_txtComparticipacao" runat="server" 
                                                                                                    ControlToValidate="Contrapartidas_txtComparticipacao" CssClass="textValidator" Display="Dynamic" 
                                                                                                    EnableClientScript="true" ErrorMessage="[COMPARTICIPA��O] -&gt; Valor Inv�lido. [0,01-100]" 
                                                                                                    MaximumValue="100" MinimumValue="0,01" Type="Double" ValidationGroup="Contrapartidas">Valor Inv�lido.<br />[0,01-100}</asp:RangeValidator>
                                                                                            </asp:Panel>
                                                                                        </FooterTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%-- PAGAMENTO ######################################### --%>
                                                                                    <asp:TemplateColumn FooterStyle-Width="10%" HeaderText="PAGAMENTO">
                                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <%# this.PrazosPagamento()[DataBinder.Eval(Container.DataItem, "PRAZO_PAGAMENTO")].ToString() %>
                                                                                        </ItemTemplate>
                                                                                        <FooterTemplate>
                                                                                            <br />
                                                                                            <asp:Panel ID="Panel_Contrapartidas_Pagamento" runat="server" 
                                                                                                DefaultButton="Contrapartidas_lnkInserir">
                                                                                                <asp:DropDownList ID="Contrapartidas_ddlPrazo" runat="server" AutoPostBack="False" 
                                                                                                    CssClass="txtNormal" DataTextField="Value" DataValueField="Key" Width="100%">
                                                                                                </asp:DropDownList>
                                                                                            </asp:Panel>
                                                                                        </FooterTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%-- BONUS ############################################# --%>
                                                                                    <asp:TemplateColumn FooterStyle-Width="35%">
                                                                                        <ItemStyle HorizontalAlign="Center" Width="35%" />
                                                                                        <HeaderTemplate>
                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td align="center" colspan="4" style="color: #00008B">
                                                                                                        B�NUS FINAL
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="center" style="color: #FF6600" width="30%">
                                                                                                        VALOR
                                                                                                    </td>
                                                                                                    <td align="center" style="color: #FF6600" width="23%">
                                                                                                        DESCONTO
                                                                                                    </td>
                                                                                                    <td align="center" style="color: #FF6600" width="23%">
                                                                                                        COMPART
                                                                                                    </td>
                                                                                                    <td align="center" style="color: #FF6600" width="23%">
                                                                                                        TIPO
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Contrapartidas_dgBonus_Empty" runat="server">N�o Tem</asp:Label>
                                                                                            <asp:DataGrid ID="Contrapartidas_dgBonus" runat="server" AutoGenerateColumns="False" 
                                                                                                BackColor="#F0F0F0" BorderStyle="None" CellSpacing="1" GridLines="None" ShowFooter="false" 
                                                                                                ShowHeader="false" Width="100%">
                                                                                                <ItemStyle CssClass="ListRow" HorizontalAlign="Right" />
                                                                                                <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                                                <FooterStyle BackColor="Transparent" VerticalAlign="Top" />
                                                                                                <Columns>
                                                                                                    <%-- VALOR --%>
                                                                                                    <asp:TemplateColumn ItemStyle-Width="30%">
                                                                                                        <ItemTemplate>
                                                                                                            <%# DataBinder.Eval(Container.DataItem, "VALOR_LIMITE_MIN") + " �"%>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <%-- DESCONTO --%>
                                                                                                    <asp:TemplateColumn ItemStyle-Width="23%">
                                                                                                        <ItemTemplate>
                                                                                                            <%# DataBinder.Eval(Container.DataItem, "DESCONTO").ToString().Length>0? DataBinder.Eval(Container.DataItem, "DESCONTO") + " %":"- - - "%>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <%-- COMPARTICIPA��O --%>
                                                                                                    <asp:TemplateColumn ItemStyle-Width="23%">
                                                                                                        <ItemTemplate>
                                                                                                            <%# DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO").ToString().Length>0? DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO") + " %":"- - - "%>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <%-- TIPO --%>
                                                                                                    <asp:TemplateColumn ItemStyle-Width="23%">
                                                                                                        <ItemTemplate>
                                                                                                            <%# this.GetLocalResourceObject(DataBinder.Eval(Container.DataItem, "TIPO").ToString()).ToString()%>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                </Columns>
                                                                                            </asp:DataGrid>
                                                                                        </ItemTemplate>
                                                                                        <FooterTemplate>
                                                                                            <br />
                                                                                            <asp:Panel ID="Panel_Contrapartidas_Bonus" runat="server" DefaultButton="Contrapartidas_lnkInserir">
                                                                                                <asp:DataGrid ID="Contrapartidas_dgBonus_Insert" runat="server" AutoGenerateColumns="False" 
                                                                                                    BackColor="Transparent" BorderStyle="None" CellSpacing="1" GridLines="None" 
                                                                                                    OnItemCommand="Contrapartidas_dgBonus_Insert_ItemCommand" ShowFooter="true" ShowHeader="true" 
                                                                                                    Width="100%">
                                                                                                    <HeaderStyle BackColor="#99CCFF" ForeColor="#00008B" HorizontalAlign="Center" />
                                                                                                    <ItemStyle CssClass="ListRow" HorizontalAlign="Right" />
                                                                                                    <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                                                    <FooterStyle BackColor="Transparent" VerticalAlign="Top" />
                                                                                                    <Columns>
                                                                                                        <%-- BONUS VALOR --%>
                                                                                                        <asp:TemplateColumn FooterStyle-Width="30%" HeaderText="VALOR" ItemStyle-Width="30%">
                                                                                                            <ItemTemplate>
                                                                                                                <%# DataBinder.Eval(Container.DataItem, "VALOR_LIMITE_MIN") + " �"%>
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <asp:Panel ID="Panel_Contrapartidas_Bonus_Valor" runat="server" 
                                                                                                                    DefaultButton="Contrapartidas_Bonus_lnkInserir">
                                                                                                                    <asp:TextBox ID="Contrapartidas_Bonus_txtValor" runat="server" CssClass="txtNormal" MaxLength="6" 
                                                                                                                        Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                                    <asp:RangeValidator ID="Contrapartidas_Bonus_Val_txtValor" runat="server" 
                                                                                                                        ControlToValidate="Contrapartidas_Bonus_txtValor" CssClass="textValidator" Display="Dynamic" 
                                                                                                                        EnableClientScript="True" ErrorMessage="[VALOR BONUS] -&gt; Valor Inv�lido. {0-999999}" 
                                                                                                                        MaximumValue="999999" MinimumValue="0" Type="Integer" ValidationGroup="Contrapartidas_Bonus">Valor Inv�lido. {0-999999}
                                                                                    </asp:RangeValidator>
                                                                                                                    <asp:RequiredFieldValidator ID="Contrapartidas_Bonus_Val_ReqtxtValor" runat="server" 
                                                                                                                        ControlToValidate="Contrapartidas_Bonus_txtValor" CssClass="textValidator" Display="Dynamic" 
                                                                                                                        EnableClientScript="True" ErrorMessage="[VALOR BONUS] -&gt; Indique um valor" 
                                                                                                                        ValidationGroup="Contrapartidas_Bonus">Indique um valor</asp:RequiredFieldValidator>
                                                                                                                    <asp:CustomValidator ID="Contrapartidas_Bonus_Val_custxtValor" runat="server" 
                                                                                                                        ControlToValidate="Contrapartidas_Bonus_txtValor" CssClass="textValidator" Display="Dynamic" 
                                                                                                                        EnableClientScript="True" ErrorMessage="[VALOR BONUS] -&gt; O valor j� existe" 
                                                                                                                        OnServerValidate="Contrapartidas_Bonus_Val_custxtValor_validate" ValidateEmptyText="false" 
                                                                                                                        ValidationGroup="Contrapartidas_Bonus">O valor j� existe</asp:CustomValidator>
                                                                                                                </asp:Panel>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                        <%-- BONUS DESCONTO --%>
                                                                                                        <asp:TemplateColumn FooterStyle-Width="23%" HeaderText="DESCONTO" ItemStyle-Width="23%">
                                                                                                            <ItemTemplate>
                                                                                                                <%# DataBinder.Eval(Container.DataItem, "DESCONTO").ToString().Length>0? DataBinder.Eval(Container.DataItem, "DESCONTO") + " %":"- - - "%>
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <asp:Panel ID="Panel_Contrapartidas_Bonus_Desconto" runat="server" 
                                                                                                                    DefaultButton="Contrapartidas_Bonus_lnkInserir">
                                                                                                                    <asp:TextBox ID="Contrapartidas_Bonus_txtDesconto" runat="server" CssClass="txtNormal" 
                                                                                                                        MaxLength="5" Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                                    <asp:RangeValidator ID="Contrapartidas_Bonus_Val_txtDesconto" runat="server" 
                                                                                                                        ControlToValidate="Contrapartidas_Bonus_txtDesconto" CssClass="textValidator" Display="Dynamic" 
                                                                                                                        EnableClientScript="True" ErrorMessage="[DESCONTO BONUS] -&gt; Valor Inv�lido. [0,01-100]" 
                                                                                                                        MaximumValue="100" MinimumValue="0,01" Type="Double" ValidationGroup="Contrapartidas_Bonus">Valor Inv�lido. [0,01-100]
                                                                                    </asp:RangeValidator>
                                                                                                                    <asp:Label ID="C_lbl_Contrapartidas_Bonus_Error_Desconto" runat="server" CssClass="textValidator"></asp:Label>
                                                                                                                </asp:Panel>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                        <%-- BONUS COMPARTICIPA��O --%>
                                                                                                        <asp:TemplateColumn FooterStyle-Width="23%" HeaderText="COMPART" ItemStyle-Width="23%">
                                                                                                            <ItemTemplate>
                                                                                                                <%# DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO").ToString().Length>0? DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO") + " %":"- - - "%>
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <asp:Panel ID="Panel_Contrapartidas_Bonus_Compart" runat="server" 
                                                                                                                    DefaultButton="Contrapartidas_Bonus_lnkInserir">
                                                                                                                    <asp:TextBox ID="Contrapartidas_Bonus_txtComparticipacao" runat="server" CssClass="txtNormal" 
                                                                                                                        MaxLength="5" Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                                    <asp:RangeValidator ID="Contrapartidas_Bonus_Val_txtComparticipacao" runat="server" 
                                                                                                                        ControlToValidate="Contrapartidas_Bonus_txtComparticipacao" CssClass="textValidator" 
                                                                                                                        Display="Dynamic" EnableClientScript="True" 
                                                                                                                        ErrorMessage="[COMPARTICIPA��O BONUS] -&gt; Valor Inv�lido. [0,01-100]" MaximumValue="100" 
                                                                                                                        MinimumValue="0,01" Type="Double" ValidationGroup="Contrapartidas_Bonus">Valor Inv�lido.<br />[0,01-100]<br></br>
                                                                                    </asp:RangeValidator>
                                                                                                                </asp:Panel>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                        <%-- BONUS TIPO --%>
                                                                                                        <asp:TemplateColumn FooterStyle-Width="23%" HeaderText="TIPO" ItemStyle-Width="23%">
                                                                                                            <ItemTemplate>
                                                                                                                <%# this.GetLocalResourceObject(DataBinder.Eval(Container.DataItem, "TIPO").ToString()).ToString()%>
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <asp:Panel ID="Panel_Contrapartidas_Bonus_Tipo" runat="server" 
                                                                                                                    DefaultButton="Contrapartidas_Bonus_lnkInserir">
                                                                                                                    <asp:DropDownList ID="Contrapartidas_Bonus_Tipo" runat="server" CssClass="txtNormal" Width="100%">
                                                                                                                        <asp:ListItem Text="Acumulado" Value="Acc"></asp:ListItem>
                                                                                                                        <asp:ListItem Text="Excedente" Value="Exd"></asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                    <asp:CustomValidator ID="Contrapartidas_Bonus_Val_cusTipo" runat="server" 
                                                                                                                        ControlToValidate="Contrapartidas_Bonus_Tipo" CssClass="textValidator" Display="Dynamic" 
                                                                                                                        EnableClientScript="True" ErrorMessage="[TIPO BONUS] -&gt; Tipo Inv�lido para o valor indicado" 
                                                                                                                        OnServerValidate="Contrapartidas_Bonus_Val_cusTipo_validate" ValidateEmptyText="false" 
                                                                                                                        ValidationGroup="Contrapartidas_Bonus">O tipo � inv�lido para o valor indicado</asp:CustomValidator>
                                                                                                                </asp:Panel>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                        <%-- BONUS BOT�ES --%>
                                                                                                        <asp:TemplateColumn>
                                                                                                            <ItemStyle BackColor="Transparent" HorizontalAlign="Left" VerticalAlign="Top" Width="1%" />
                                                                                                            <HeaderStyle BackColor="Transparent" ForeColor="Transparent" />
                                                                                                            <FooterStyle Width="1%" />
                                                                                                            <ItemTemplate>
                                                                                                                <asp:ImageButton ID="C_btn_Contrapartidas_dgBonus_Delete" runat="server" CausesValidation="false" 
                                                                                                                    CommandName="Apagar" meta:resourcekey="C_btn_Contrapartidas_dgBonus_Delete" 
                                                                                                                    OnClientClick="javascript: return confirm('Deseja mesmo remover o bonus da Gama?');" />
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <table cellpadding="0">
                                                                                                                    <tr>
                                                                                                                        <td align="left" valign="middle">
                                                                                                                            <asp:ImageButton ID="Contrapartidas_Bonus_lnkInserir" runat="server" CommandName="Inserir" 
                                                                                                                                CssClass="button" meta:resourcekey="Contrapartidas_Bonus_lnkInserir" 
                                                                                                                                ValidationGroup="Contrapartidas_Bonus" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                    </Columns>
                                                                                                </asp:DataGrid>
                                                                                            </asp:Panel>
                                                                                        </FooterTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%-- BOT�ES ############################################ --%>
                                                                                    <asp:TemplateColumn>
                                                                                        <HeaderStyle BackColor="Transparent" ForeColor="Transparent" />
                                                                                        <ItemStyle BackColor="Transparent" HorizontalAlign="Left" VerticalAlign="Top" Width="1%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="C_btn_dgContrapartidas_Delete" runat="server" CausesValidation="false" 
                                                                                                CommandName="Apagar" meta:resourcekey="C_btn_dgContrapartidas_Delete" 
                                                                                                OnClientClick="javascript: return confirm('Deseja mesmo remover a gama das contrapartidas?');" 
                                                                                                Visible='<%# this.Condicoes_dgEquipamentos.Attributes["CanUse"]=="True" %>' />
                                                                                        </ItemTemplate>
                                                                                        <FooterTemplate>
                                                                                            <br />
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td valign="middle">
                                                                                                        <asp:ImageButton ID="Contrapartidas_lnkInserir" runat="server" CommandName="Inserir" 
                                                                                                            CssClass="button" meta:resourcekey="Contrapartidas_lnkInserir" ValidationGroup="Contrapartidas" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </FooterTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                            <asp:CustomValidator ID="C_Contrapartidas_val_dgContrapartidas" runat="server" 
                                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="false" 
                                                                                meta:resourcekey="C_Contrapartidas_val_dgContrapartidas" 
                                                                                OnServerValidate="C_val_dgContrapartidas_Validate" ValidationGroup="Condicoes"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <p>
                                                </p>
                                                <%-- CONTRAPARTIDAS FINANCEIRAS GLOBAIS  ####################################################### --%>
                                                <p style="page-break-before: auto; page-break-after: auto">
                                                    &nbsp;<asp:Panel ID="BoxContrapartidasGlobais" runat="server" CssClass="box" DefaultButton="Dummy">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <%-- HEADER ######################################################################## --%>
                                                            <tr onclick="javascript:ShowHide('ContrapartidasGlobais');" 
                                                                style="cursor: hand; padding: 3px 3px 3px 3px;">
                                                                <td class="titulo_peq tituloBarra" width="1%">
                                                                    <%if (!this.is_PrintMode)
                                  {%><label ID="C_btn_ContrapartidasGlobais_box" style="font-size: 12pt; color: white; font-family: Symbol"> �</label> <%} %>
                                                                </td>
                                                                <td class="titulo_peq tituloBarra">
                                                                    <%= this.GetLocalResourceObject("Panel_ContrapartidasGlobais.Header").ToString()%>
                                                                </td>
                                                                <td class="titulo_peq tituloBarra" width="1%">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <%-- CONTENT ####################################################################### --%>
                                                            <tr ID="ContrapartidasGlobais">
                                                                <td class="backColor cell" colspan="3">
                                                                    <table border="0" cellspacing="2" class="table" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="ContrapartidasGerais_dlFaseamento_Empty" runat="server" CssClass="tituloGrande">N�o Tem</asp:Label>
                                                                                <table ID="Table1" runat="server" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <%-- HEADERS ########################################################### --%>
                                                                                    <tr valign="middle">
                                                                                        <%-- GAMAS ######################################################### --%>
                                                                                        <td align="center" class="tituloListagem" width="45%">
                                                                                            GAMAS
                                                                                        </td>
                                                                                        <td style="background-color: Transparent; font-size: 5pt;">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <%-- PREVISAO ###################################################### --%>
                                                                                        <td align="center" class="tituloListagem" width="15%">
                                                                                            PREVIS�O DE VENDAS
                                                                                        </td>
                                                                                        <td style="background-color: Transparent; font-size: 5pt;">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <%-- PRAZO ######################################################### --%>
                                                                                        <td align="center" class="tituloListagem" width="10%">
                                                                                            PAGAMENTO
                                                                                        </td>
                                                                                        <td style="background-color: Transparent; font-size: 5pt;">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <%-- BONUS ######################################################### --%>
                                                                                        <td align="center" class="tituloListagem" width="30%">
                                                                                            B�NUS FINAL
                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="95%">
                                                                                                <tr valign="bottom">
                                                                                                    <td align="center" style="color: #FF6600" width="30%">
                                                                                                        VALOR
                                                                                                    </td>
                                                                                                    <td align="center" style="color: #FF6600" width="23%">
                                                                                                        DESCONTO
                                                                                                    </td>
                                                                                                    <td align="center" style="color: #FF6600" width="23%">
                                                                                                        COMPART
                                                                                                    </td>
                                                                                                    <td align="center" style="color: #FF6600" width="23%">
                                                                                                        TIPO
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%-- CONTENT ########################################################### --%>
                                                                                    <tr>
                                                                                        <%-- GAMAS ######################################################### --%>
                                                                                        <td valign="top">
                                                                                            <asp:Repeater ID="ContrapartidasGerais_Gamas_list" runat="server" 
                                                                                                OnItemDataBound="ContrapartidasGerais_Gamas_list_ItemDataBound">
                                                                                                <HeaderTemplate>
                                                                                                    <table width="100%">
                                                                                                    </table>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Literal ID="ContrapartidasGerais_Gamas_list_ItemTemplate" runat="server" Mode="PassThrough"></asp:Literal>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate>
                                                                                                    <asp:Literal ID="ContrapartidasGerais_Gamas_list_FooterTemplate" runat="server" Mode="PassThrough"></asp:Literal>
                                                                                                </FooterTemplate>
                                                                                            </asp:Repeater>
                                                                                            <asp:CheckBoxList ID="ContrapartidasGerais_cbGamas" runat="server" AutoPostBack="true" 
                                                                                                CellPadding="0" CellSpacing="0" 
                                                                                                OnSelectedIndexChanged="ContrapartidasGerais_cbGamas_SelectedIndexChanged" RepeatColumns="3" 
                                                                                                RepeatDirection="Vertical" Width="100%">
                                                                                            </asp:CheckBoxList>
                                                                                            <asp:CustomValidator ID="C_val_ContrapartidasGlobais_Gamas" runat="server" CssClass="textValidator" 
                                                                                                Display="Dynamic" EnableClientScript="false" meta:resourcekey="C_val_ContrapartidasGlobais_Gamas" 
                                                                                                OnServerValidate="C_val_ContrapartidasGlobais_Gamas_validate" 
                                                                                                ValidationGroup="ContrapartidasGlobais"></asp:CustomValidator>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <%-- PREVISAO ###################################################### --%>
                                                                                        <td align="center" valign="top">
                                                                                            <asp:Label ID="ContrapartidasGerais_txtPrevisao" runat="server" ForeColor="Black" Width="100%"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <%-- PRAZO ######################################################### --%>
                                                                                        <td align="center" valign="top">
                                                                                            <asp:Label ID="C_lbl_Prazo" runat="server"></asp:Label>
                                                                                            <asp:DropDownList ID="ContrapartidasGerais_ddlPrazo" runat="server" AutoPostBack="False" 
                                                                                                CssClass="txtNormal" DataTextField="Value" DataValueField="Key" Width="100%">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <%-- BONUS ######################################################### --%>
                                                                                        <td align="center" valign="top">
                                                                                            <asp:Panel ID="BonusGrid" runat="server" BorderStyle="None" BorderWidth="0">
                                                                                                <asp:DataGrid ID="ContrapartidasGerais_dgBonus" runat="server" AutoGenerateColumns="False" 
                                                                                                    BackColor="Transparent" BorderStyle="None" CellSpacing="2" GridLines="None" 
                                                                                                    OnItemCommand="ContrapartidasGerais_dgBonus_ItemCommand" 
                                                                                                    ShowFooter='<%# this.ContrapartidasGerais_dgBonus.Attributes["CanUse"]=="True" %>' 
                                                                                                    ShowHeader="false" Width="100%">
                                                                                                    <ItemStyle CssClass="ListRow" HorizontalAlign="Right" />
                                                                                                    <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                                                    <FooterStyle BackColor="Transparent" VerticalAlign="Top" />
                                                                                                    <Columns>
                                                                                                        <%-- BONUS VALOR --%>
                                                                                                        <asp:TemplateColumn FooterStyle-Width="30%" HeaderText="VALOR" ItemStyle-Width="30%">
                                                                                                            <ItemTemplate>
                                                                                                                <%# DataBinder.Eval(Container.DataItem, "VALOR_LIMITE_MIN") + " �"%>
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <asp:Panel ID="Panel_ContrapartidasGerais_Bonus_Previsao" runat="server" 
                                                                                                                    DefaultButton="ContrapartidasGerais_Bonus_lnkInserir">
                                                                                                                    <asp:TextBox ID="ContrapartidasGerais_Bonus_txtValor" runat="server" CssClass="txtNormal" 
                                                                                                                        MaxLength="6" Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                                    <asp:RangeValidator ID="ContrapartidasGerais_Bonus_Val_txtValor" runat="server" 
                                                                                                                        ControlToValidate="ContrapartidasGerais_Bonus_txtValor" CssClass="textValidator" Display="Dynamic" 
                                                                                                                        EnableClientScript="True" ErrorMessage="[VALOR BONUS] -&gt; Valor Inv�lido. {0-999999}" 
                                                                                                                        MaximumValue="999999" MinimumValue="0" Type="Integer" ValidationGroup="ContrapartidasGerais_Bonus">Valor Inv�lido. {0-999999}
                                                                                </asp:RangeValidator>
                                                                                                                    <asp:RequiredFieldValidator ID="ContrapartidasGerais_Bonus_Val_ReqtxtValor" runat="server" 
                                                                                                                        ControlToValidate="ContrapartidasGerais_Bonus_txtValor" CssClass="textValidator" Display="Dynamic" 
                                                                                                                        EnableClientScript="True" ErrorMessage="[VALOR BONUS] -&gt; Indique um valor" 
                                                                                                                        ValidationGroup="ContrapartidasGerais_Bonus">Indique um valor</asp:RequiredFieldValidator>
                                                                                                                    <asp:CustomValidator ID="ContrapartidasGerais_Bonus_Val_custxtValor" runat="server" 
                                                                                                                        ControlToValidate="ContrapartidasGerais_Bonus_txtValor" CssClass="textValidator" Display="Dynamic" 
                                                                                                                        EnableClientScript="True" ErrorMessage="[VALOR BONUS] -&gt; O valor j� existe" 
                                                                                                                        OnServerValidate="ContrapartidasGerais_Bonus_Val_custxtValor_validate" ValidateEmptyText="false" 
                                                                                                                        ValidationGroup="ContrapartidasGerais_Bonus">O valor j� existe</asp:CustomValidator>
                                                                                                                </asp:Panel>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                        <%-- BONUS DESCONTO --%>
                                                                                                        <asp:TemplateColumn FooterStyle-Width="23%" HeaderText="DESCONTO" ItemStyle-Width="23%">
                                                                                                            <ItemTemplate>
                                                                                                                <%# DataBinder.Eval(Container.DataItem, "DESCONTO").ToString().Length>0? DataBinder.Eval(Container.DataItem, "DESCONTO") + " %":"- - - "%>
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <asp:Panel ID="Panel_ContrapartidasGerais_Bonus_Desconto" runat="server" 
                                                                                                                    DefaultButton="ContrapartidasGerais_Bonus_lnkInserir">
                                                                                                                    <asp:TextBox ID="ContrapartidasGerais_Bonus_txtDesconto" runat="server" CssClass="txtNormal" 
                                                                                                                        MaxLength="5" Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                                    <asp:RangeValidator ID="ContrapartidasGerais_Bonus_Val_txtDesconto" runat="server" 
                                                                                                                        ControlToValidate="ContrapartidasGerais_Bonus_txtDesconto" CssClass="textValidator" 
                                                                                                                        Display="Dynamic" EnableClientScript="True" 
                                                                                                                        ErrorMessage="[DESCONTO BONUS] -&gt; Valor Inv�lido. {0,01-100}" MaximumValue="100" 
                                                                                                                        MinimumValue="0,01" Type="Double" ValidationGroup="ContrapartidasGerais_Bonus">Valor Inv�lido. {0,01-100}
                                                                                </asp:RangeValidator>
                                                                                                                    <asp:Label ID="C_lbl_ContrapartidasGerais_Bonus_Error_Desconto" runat="server" 
                                                                                                                        CssClass="textValidator"></asp:Label>
                                                                                                                </asp:Panel>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                        <%-- BONUS COMPARTICIPA��O --%>
                                                                                                        <asp:TemplateColumn FooterStyle-Width="23%" HeaderText="COMPART" ItemStyle-Width="23%">
                                                                                                            <ItemTemplate>
                                                                                                                <%# DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO").ToString().Length>0? DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO") + " %":"- - - "%>
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <asp:Panel ID="Panel_ContrapartidasGerais_Bonus_Compart" runat="server" 
                                                                                                                    DefaultButton="ContrapartidasGerais_Bonus_lnkInserir">
                                                                                                                    <asp:TextBox ID="ContrapartidasGerais_Bonus_txtComparticipacao" runat="server" CssClass="txtNormal" 
                                                                                                                        MaxLength="5" Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                                    <asp:RangeValidator ID="ContrapartidasGerais_Bonus_Val_txtComparticipacao" runat="server" 
                                                                                                                        ControlToValidate="ContrapartidasGerais_Bonus_txtComparticipacao" CssClass="textValidator" 
                                                                                                                        Display="Dynamic" EnableClientScript="True" 
                                                                                                                        ErrorMessage="[COMPARTICIPA��O BONUS] -&gt; Valor Inv�lido. {0,01-100}" MaximumValue="100" 
                                                                                                                        MinimumValue="0,01" Type="Double" ValidationGroup="ContrapartidasGerais_Bonus">Valor Inv�lido.<br>{0,01-100}</br>
                                                                                </asp:RangeValidator>
                                                                                                                </asp:Panel>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                        <%-- BONUS TIPO --%>
                                                                                                        <asp:TemplateColumn FooterStyle-Width="23%" HeaderText="TIPO" ItemStyle-Width="23%">
                                                                                                            <ItemTemplate>
                                                                                                                <%# this.GetLocalResourceObject(DataBinder.Eval(Container.DataItem, "TIPO").ToString()).ToString()%>
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <asp:Panel ID="Panel_Contrapartidasgerais_Bonus_Tipo" runat="server" 
                                                                                                                    DefaultButton="ContrapartidasGerais_Bonus_lnkInserir">
                                                                                                                    <asp:DropDownList ID="ContrapartidasGerais_Bonus_Tipo" runat="server" CssClass="txtNormal" 
                                                                                                                        Width="100%">
                                                                                                                        <asp:ListItem Text="Acumulado" Value="Acc"></asp:ListItem>
                                                                                                                        <asp:ListItem Text="Excedente" Value="Exd"></asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                    <asp:CustomValidator ID="ContrapartidasGerais_Bonus_Val_cusTipo" runat="server" 
                                                                                                                        ControlToValidate="ContrapartidasGerais_Bonus_Tipo" CssClass="textValidator" Display="Dynamic" 
                                                                                                                        EnableClientScript="True" ErrorMessage="[TIPO BONUS] -&gt; Tipo Inv�lido para o valor indicado" 
                                                                                                                        OnServerValidate="ContrapartidasGerais_Bonus_Val_cusTipo_validate" ValidateEmptyText="false" 
                                                                                                                        ValidationGroup="ContrapartidasGerais_Bonus">O tipo � inv�lido para o valor indicado</asp:CustomValidator>
                                                                                                                </asp:Panel>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                        <%-- BONUS BOT�ES --%>
                                                                                                        <asp:TemplateColumn>
                                                                                                            <ItemStyle BackColor="Transparent" HorizontalAlign="Left" VerticalAlign="Top" Width="1%" />
                                                                                                            <HeaderStyle BackColor="Transparent" ForeColor="Transparent" />
                                                                                                            <FooterStyle Width="1%" />
                                                                                                            <ItemTemplate>
                                                                                                                <asp:ImageButton ID="C_btn_ContrapartidasGerais_dgBonus_Delete" runat="server" 
                                                                                                                    CausesValidation="false" CommandName="Apagar" 
                                                                                                                    meta:resourcekey="C_btn_ContrapartidasGerais_dgBonus_Delete" 
                                                                                                                    OnClientClick="javascript: return confirm('Deseja mesmo remover o bonus da Contrapartida?');" 
                                                                                                                    Visible='<%# this.ContrapartidasGerais_dgBonus.Attributes["CanUse"]=="True" %>' />
                                                                                                            </ItemTemplate>
                                                                                                            <FooterTemplate>
                                                                                                                <table cellpadding="0">
                                                                                                                    <tr>
                                                                                                                        <td align="left" valign="middle">
                                                                                                                            <asp:ImageButton ID="ContrapartidasGerais_Bonus_lnkInserir" runat="server" CommandName="Inserir" 
                                                                                                                                CssClass="button" meta:resourcekey="ContrapartidasGerais_Bonus_lnkInserir" 
                                                                                                                                ValidationGroup="ContrapartidasGerais_Bonus" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </FooterTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                    </Columns>
                                                                                                </asp:DataGrid>
                                                                                            </asp:Panel>
                                                                                            <asp:CustomValidator ID="C_val_ContrapartidasGlobais_Bonus" runat="server" CssClass="textValidator" 
                                                                                                Display="Dynamic" EnableClientScript="false" meta:resourcekey="C_val_ContrapartidasGlobais_Bonus" 
                                                                                                OnServerValidate="C_val_ContrapartidasGlobais_Bonus_validate" 
                                                                                                ValidationGroup="ContrapartidasGlobais"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <p>
                                                    </p>
                                                    <%-- FASEAMENTO VERBA ACORDADA ################################################################# --%>
                                                    <p style="page-break-before: auto">
                                                        &nbsp;<asp:Panel ID="boxFaseamento" runat="server" CssClass="box" DefaultButton="Dummy">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <%-- HEADER ######################################################################## --%>
                                                                <tr onclick="javascript:ShowHide('Faseamento');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                                                                    <td class="titulo_peq tituloBarra" width="1%">
                                                                        <%if (!this.is_PrintMode)
                                  {%>
                                                                        <label ID="C_btn_Faseamento_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                                                        �</label> <%} %>
                                                                    </td>
                                                                    <td class="titulo_peq tituloBarra">
                                                                        <%= this.GetLocalResourceObject("Panel_Faseamento.Header").ToString()%>
                                                                    </td>
                                                                    <td class="titulo_peq tituloBarra" width="1%">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <%-- CONTENT ####################################################################### --%>
                                                                <tr ID="Faseamento">
                                                                    <td class="backColor cell" colspan="3">
                                                                        <table border="0" class="table">
                                                                            <%--LISTA FASEAMENTOS --%>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Panel ID="FaseamentoBox" runat="server">
                                                                                        <asp:Label ID="Faseamento_dlFaseamento_Empty" runat="server" CssClass="tituloGrande">N�o Tem</asp:Label>
                                                                                        <asp:DataGrid ID="Faseamento_dlFaseamento" runat="server" AutoGenerateColumns="False" 
                                                                                            BackColor="Transparent" BorderStyle="None" CellSpacing="2" GridLines="None" 
                                                                                            OnItemCommand="Faseamento_dlFaseamento_ItemCommand" 
                                                                                            OnItemDataBound="Faseamento_dlFaseamento_ItemDataBound" 
                                                                                            ShowFooter='<%# this.Faseamento_dlFaseamento.Attributes["CanUse"]=="True" %>' Width="100%">
                                                                                            <HeaderStyle BackColor="#99CCFF" ForeColor="#00008B" Height="20px" HorizontalAlign="Center" />
                                                                                            <ItemStyle CssClass="ListRow" HorizontalAlign="Right" />
                                                                                            <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                                            <FooterStyle BackColor="Transparent" HorizontalAlign="Right" VerticalAlign="Top" />
                                                                                            <Columns>
                                                                                                <%-- PAGAMENTO PREVISTO --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="15%" HeaderText="PAGAMENTO PREVISTO" ItemStyle-Width="15%">
                                                                                                    <ItemTemplate>
                                                                                                        <%# ((DateTime)DataBinder.Eval(Container.DataItem, "DATA_PAGAMENTO")).ToString("dd-MM-yyyy")%>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <asp:Panel ID="Panel_Faseamento_Grupo" runat="server" DefaultButton="Faseamento_lnkInserir">
                                                                                                            <UC:DatePicker ID="Faseamento_Data_Previsto" runat="server" AllowClear="false" 
                                                                                                                MaxEnabledDate="<%# this.EndDate %>" MinEnabledDate="<%# this.StartDate %>" 
                                                                                                                SelectedDate="<%# DateTime.Now %>" />
                                                                                                        </asp:Panel>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- PAGAMENTO EFECTUADO --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="15%" HeaderText="PAGAMENTO EFECTUADO" ItemStyle-Width="15%">
                                                                                                    <ItemTemplate>
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <asp:ImageButton ID="C_btn_dgFaseamento_Edit" runat="server" CausesValidation="false" 
                                                                                                                        CommandName="Edit" meta:resourcekey="C_btn_dgFaseamento_Edit" 
                                                                                                                        Visible='<%# this.Faseamento_dlFaseamento.Attributes["CanUse"]=="True" %>' />
                                                                                                                </td>
                                                                                                                <td align="right">
                                                                                                                    <%# DataBinder.Eval(Container.DataItem, "DATA_PAGAMENTO_EFECTUADO")==System.DBNull.Value ? "":((DateTime)DataBinder.Eval(Container.DataItem, "DATA_PAGAMENTO_EFECTUADO")).ToString("dd-MM-yyyy")%>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                    </FooterTemplate>
                                                                                                    <EditItemTemplate>
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <asp:ImageButton ID="C_btn_dgFaseamento_Save" runat="server" CausesValidation="false" 
                                                                                                                        CommandName="Save" meta:resourcekey="C_btn_dgFaseamento_Save" />
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="C_btn_dgFaseamento_Undo" runat="server" CausesValidation="false" 
                                                                                                                        CommandName="Undo" meta:resourcekey="C_btn_dgFaseamento_Undo" />
                                                                                                                </td>
                                                                                                                <td align="right">
                                                                                                                    <UC:DatePicker ID="Faseamento_Data_Efectuado" runat="server" 
                                                                                                                        SelectedDate='<%# DataBinder.Eval(Container.DataItem, "DATA_PAGAMENTO_EFECTUADO")==System.DBNull.Value ? DateTime.Now:DataBinder.Eval(Container.DataItem, "DATA_PAGAMENTO_EFECTUADO")%>' />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </EditItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- VALOR ################################ --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="20%" HeaderText="VALOR">
                                                                                                    <ItemStyle Width="20%" />
                                                                                                    <ItemTemplate>
                                                                                                        <%# DataBinder.Eval(Container.DataItem, "VALOR_PAGAMENTO") + " �"%>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <asp:Panel ID="Panel_Faseamento_Previsao" runat="server" DefaultButton="Faseamento_lnkInserir">
                                                                                                            <asp:TextBox ID="Faseamento_txtValor" runat="server" CssClass="txtNormal" MaxLength="9" 
                                                                                                                Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                            <asp:RangeValidator ID="Faseamento_Val_txtPrevisao" runat="server" 
                                                                                                                ControlToValidate="Faseamento_txtValor" CssClass="textValidator" Display="Dynamic" 
                                                                                                                EnableClientScript="True" ErrorMessage="[FASEAMENTO] -&gt; Valor Inv�lido" MaximumValue="999999999" 
                                                                                                                MinimumValue="-999999999" Type="Double" ValidationGroup="Faseamento">Valor Inv�lido</asp:RangeValidator>
                                                                                                            <asp:RequiredFieldValidator ID="Faseamento_Val_ReqtxtPrevisao" runat="server" 
                                                                                                                ControlToValidate="Faseamento_txtValor" CssClass="textValidator" Display="Dynamic" 
                                                                                                                EnableClientScript="True" ErrorMessage="[FASEAMENTO] -&gt; Indique um valor" 
                                                                                                                ValidationGroup="Faseamento">Indique um valor</asp:RequiredFieldValidator>
                                                                                                        </asp:Panel>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- COMPARTICIPA��O ################################### --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="15%" HeaderText="COMPARTICIPA��O">
                                                                                                    <ItemStyle Width="15%" />
                                                                                                    <ItemTemplate>
                                                                                                        <%# DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO").ToString().Length>0? DataBinder.Eval(Container.DataItem, "COMP_CONCESSIONARIO") + " %" : "- - - " %>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <asp:Panel ID="Panel_Faseamento_Compart" runat="server" DefaultButton="Faseamento_lnkInserir">
                                                                                                            <asp:TextBox ID="Faseamento_txtComparticipacao" runat="server" CssClass="txtNormal" MaxLength="5" 
                                                                                                                Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                            <asp:RangeValidator ID="Faseamento_Val_txtComparticipacao" runat="server" 
                                                                                                                ControlToValidate="Faseamento_txtComparticipacao" CssClass="textValidator" Display="Dynamic" 
                                                                                                                EnableClientScript="true" ErrorMessage="[COMPARTICIPA��O] -&gt; Valor Inv�lido. [0,01-100]" 
                                                                                                                MaximumValue="100" MinimumValue="0,01" Type="Double" ValidationGroup="Faseamento">Valor Inv�lido.<br />[0,01-100}</asp:RangeValidator>
                                                                                                        </asp:Panel>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- TIPO ######################################### --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="15%" HeaderText="TIPO">
                                                                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                                                    <ItemTemplate>
                                                                                                        <%# (DataBinder.Eval(Container.DataItem, "TIPO_PG").Equals("Pr")) ? "Produto" : (DataBinder.Eval(Container.DataItem, "TIPO_PG").Equals("Di")) ? "Dinheiro" : ""%>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <asp:Panel ID="Panel_Faseamento_Pagamento" runat="server" DefaultButton="Faseamento_lnkInserir">
                                                                                                            <asp:DropDownList ID="Faseamento_lbTipo" runat="server" AutoPostBack="False" CssClass="txtNormal" 
                                                                                                                Width="100%">
                                                                                                                <asp:ListItem Selected="True" Value="Pr">Produto</asp:ListItem>
                                                                                                                <asp:ListItem Value="Di">Dinheiro</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </asp:Panel>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- TTS ######################################### --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="20%" HeaderText="TTS">
                                                                                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                                                                    <ItemTemplate>
                                                                                                        <%# DataBinder.Eval(Container.DataItem, "TTS") %>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <asp:Panel ID="Panel_Faseamento_TTS" runat="server" DefaultButton="Faseamento_lnkInserir">
                                                                                                            <asp:DropDownList ID="Faseamento_lbTTS" runat="server" AutoPostBack="False" CssClass="txtNormal" 
                                                                                                                Width="100%">
                                                                                                            </asp:DropDownList>
                                                                                                        </asp:Panel>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- BOT�ES ############################################ --%>
                                                                                                <asp:TemplateColumn>
                                                                                                    <HeaderStyle BackColor="Transparent" ForeColor="Transparent" />
                                                                                                    <ItemStyle BackColor="Transparent" HorizontalAlign="Left" VerticalAlign="Top" Width="1%" />
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="C_btn_dgFaseamento_Delete" runat="server" CausesValidation="false" 
                                                                                                            CommandName="Apagar" meta:resourcekey="C_btn_dgFaseamento_Delete" 
                                                                                                            OnClientClick="javascript: return confirm('Deseja mesmo remover o faseamento?');" 
                                                                                                            Visible='<%# this.Faseamento_dlFaseamento.Attributes["CanUse"]=="True" %>' />
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td valign="middle">
                                                                                                                    <asp:ImageButton ID="Faseamento_lnkInserir" runat="server" CommandName="Inserir" CssClass="button" 
                                                                                                                        meta:resourcekey="Faseamento_lnkInserir" ValidationGroup="Faseamento" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                            </Columns>
                                                                                        </asp:DataGrid>
                                                                                    </asp:Panel>
                                                                                    <asp:CustomValidator ID="Faseamento_Val_txtData" runat="server" CssClass="textValidator" 
                                                                                        Display="Dynamic" EnableClientScript="false" meta:resourcekey="Faseamento_Val_txtData" 
                                                                                        OnServerValidate="C_val_txtData_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                                        ValidationGroup="TPR"></asp:CustomValidator>
                                                                                    <asp:CustomValidator ID="Faseamento_Val_txtComparticipacaoCompras_Empty" runat="server" 
                                                                                        ControlToValidate="Faseamento_txtComparticipacaoCompras" CssClass="textValidator" Display="Dynamic" 
                                                                                        EnableClientScript="false" meta:resourcekey="Faseamento_Val_txtComparticipacaoCompras_Empty" 
                                                                                        OnServerValidate="Faseamento_Val_txtComparticipacaoCompras_Empty_Validate" SetFocusOnError="true" 
                                                                                        ValidationGroup="TPR"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <%-- VERBA TOTAL --%>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <br />
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td width="30%">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td align="right" width="20%">
                                                                                                <asp:Label ID="Faseamento_lblVerba" runat="server" CssClass="titulo_peq"></asp:Label>
                                                                                            </td>
                                                                                            <td width="50%">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <hr />
                                                                                    <br />
                                                                                </td>
                                                                            </tr>
                                                                            <%-- IDENTIFICA��O BANC�RIA --%>
                                                                            <tr>
                                                                                <td>
                                                                                    <table class="table">
                                                                                        <tr>
                                                                                            <td align="right" valign="top" width="30%">
                                                                                                NIB:
                                                                                                <asp:TextBox ID="Faseamento_txtNIB" runat="server" CssClass="txtNormal" MaxLength="21" Width="65%"></asp:TextBox>
                                                                                                <br />
                                                                                                <asp:RegularExpressionValidator ID="Faseamento_Val_txtNIB" runat="server" 
                                                                                                    ControlToValidate="Faseamento_txtNIB" CssClass="textValidator" Display="Dynamic" 
                                                                                                    EnableClientScript="true" ErrorMessage="[NIB]          -&gt; Valor Inv�lido." 
                                                                                                    ValidationExpression="\d{21}"><br>Valor Inv�lido</asp:RegularExpressionValidator>
                                                                                                <asp:CustomValidator ID="Faseamento_Val_txtNIB_Empty" runat="server" 
                                                                                                    ControlToValidate="Faseamento_txtNIB" CssClass="textValidator" Display="Dynamic" 
                                                                                                    EnableClientScript="false" meta:resourcekey="Faseamento_Val_txtNIB_Empty" 
                                                                                                    OnServerValidate="C_val_txtNIB_EmptyGroup_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                                                    ValidationGroup="TPR"></asp:CustomValidator>
                                                                                            </td>
                                                                                            <td align="right" valign="top" width="40%">
                                                                                                Banco:
                                                                                                <asp:TextBox ID="Faseamento_txtBANCO" runat="server" CssClass="txtNormal" MaxLength="100" 
                                                                                                    Width="70%"></asp:TextBox>
                                                                                                <br />
                                                                                                <asp:CustomValidator ID="Faseamento_Val_txtBANCO" runat="server" 
                                                                                                    ControlToValidate="Faseamento_txtBANCO" CssClass="textValidator" Display="Dynamic" 
                                                                                                    EnableClientScript="false" meta:resourcekey="Faseamento_Val_txtBANCO_Empty" 
                                                                                                    OnServerValidate="C_val_txtNIB_EmptyGroup_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                                                    ValidationGroup="TPR"></asp:CustomValidator>
                                                                                                <br />
                                                                                                Depend�ncia:
                                                                                                <asp:TextBox ID="Faseamento_txtDEPENDENCIA" runat="server" CssClass="txtNormal" MaxLength="100" 
                                                                                                    Width="70%"></asp:TextBox>
                                                                                                <br />
                                                                                                <asp:CustomValidator ID="Faseamento_Val_txtDEPENDENCIA" runat="server" 
                                                                                                    ControlToValidate="Faseamento_txtDEPENDENCIA" CssClass="textValidator" Display="Dynamic" 
                                                                                                    EnableClientScript="false" meta:resourcekey="Faseamento_Val_txtDEPENDENCIA_Empty" 
                                                                                                    OnServerValidate="C_val_txtNIB_EmptyGroup_Validate" SetFocusOnError="true" ValidateEmptyText="true" 
                                                                                                    ValidationGroup="TPR"></asp:CustomValidator>
                                                                                            </td>
                                                                                            <td align="right" valign="top" width="30%">
                                                                                                Comparticipa��o em Compras:
                                                                                                <asp:TextBox ID="Faseamento_txtComparticipacaoCompras" runat="server" CssClass="txtNormal" 
                                                                                                    MaxLength="5" OnTextChanged="Faseamento_txtComparticipacaoCompras_Changed" Width="50px"></asp:TextBox>
                                                                                                %
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:RangeValidator ID="Faseamento_Val_txtComparticipacaoCompras" runat="server" 
                                                                                                                ControlToValidate="Faseamento_txtComparticipacaoCompras" CssClass="textValidator" Display="Dynamic" 
                                                                                                                EnableClientScript="true" ErrorMessage="[COMPARTICIPA��O COMPRAS] -&gt; Valor Inv�lido. {0-100}" 
                                                                                                                MaximumValue="100" MinimumValue="0" Type="Double"><br>Valor Inv�lido.{0-100}</br>
                                                                    </asp:RangeValidator>
                                                                                                            <asp:CustomValidator ID="Faseamento_Val_txtComparticipacao" runat="server" 
                                                                                                                ControlToValidate="Faseamento_txtComparticipacaoCompras" CssClass="textValidator" Display="Dynamic" 
                                                                                                                EnableClientScript="false" meta:resourcekey="Faseamento_Val_txtComparticipacao" 
                                                                                                                OnServerValidate="Faseamento_Val_txtComparticipacao_Validate" SetFocusOnError="true" 
                                                                                                                ValidateEmptyText="true" ValidationGroup="TPR"></asp:CustomValidator>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:CheckBox ID="Faseamento_cbComparticipacao" runat="server" Style="display: none;" 
                                                                                                                Text="Confirmo" TextAlign="Right" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <p>
                                                        </p>
                                                        <%-- MATERIAL VISIBILIDADE  #################################################################### --%>
                                                        <p style="page-break-before: auto; page-break-after: auto">
                                                            &nbsp;<asp:Panel ID="boxMaterial" runat="server" CssClass="box" DefaultButton="Dummy">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <%-- HEADER ######################################################################## --%>
                                                                    <tr onclick="javascript:ShowHide('Material');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                                                                        <td class="titulo_peq tituloBarra" width="1%">
                                                                            <%if (!this.is_PrintMode)
                                  {%>
                                                                            <label ID="C_btn_Material_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                                                            �</label> <%} %>
                                                                        </td>
                                                                        <td class="titulo_peq tituloBarra">
                                                                            <%= this.GetLocalResourceObject("Panel_Material.Header").ToString()%>
                                                                        </td>
                                                                        <td class="titulo_peq tituloBarra" width="1%">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <%-- CONTENT ####################################################################### --%>
                                                                    <tr ID="Material">
                                                                        <td class="backColor cell" colspan="3">
                                                                            <table border="0" class="table">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="Material_dlVisibilidade_Empty" runat="server" CssClass="tituloGrande">N�o Tem</asp:Label>
                                                                                        <asp:DataGrid ID="Material_dlVisibilidade" runat="server" AutoGenerateColumns="False" 
                                                                                            BackColor="Transparent" BorderStyle="None" CellSpacing="2" GridLines="None" 
                                                                                            OnItemCommand="Material_dlVisibilidade_ItemCommand" 
                                                                                            OnItemDataBound="Material_dlVisibilidade_ItemDataBound" 
                                                                                            ShowFooter='<%# this.Material_dlVisibilidade.Attributes["CanUse"]=="True" %>' Width="100%">
                                                                                            <HeaderStyle BackColor="#99CCFF" ForeColor="#00008B" Height="20px" HorizontalAlign="Center" />
                                                                                            <ItemStyle CssClass="ListRow" HorizontalAlign="Right" />
                                                                                            <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                                            <FooterStyle BackColor="Transparent" HorizontalAlign="Right" VerticalAlign="Top" />
                                                                                            <Columns>
                                                                                                <%-- MATERIAL --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="30%" HeaderText="MATERIAL">
                                                                                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                                                                    <ItemTemplate>
                                                                                                        <%# DataBinder.Eval(Container.DataItem, "DES_MATERIAL_POS")%>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <asp:Panel ID="Panel_Material_Material" runat="server" DefaultButton="Material_lnkInserir">
                                                                                                            <asp:DropDownList ID="Material_lbMaterial" runat="server" AutoPostBack="true" CssClass="txtNormal" 
                                                                                                                OnSelectedIndexChanged="Material_lbMaterial_Change" Width="100%">
                                                                                                            </asp:DropDownList>
                                                                                                        </asp:Panel>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- QUANTIDADE --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="10%" HeaderText="QUANTIDADE" ItemStyle-Width="10%">
                                                                                                    <ItemTemplate>
                                                                                                        <%# DataBinder.Eval(Container.DataItem, "QUANTIDADE")%>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <asp:Panel ID="Panel_Material_Quantidade" runat="server" DefaultButton="Material_lnkInserir">
                                                                                                            <asp:TextBox ID="Material_txtQuantidade" runat="server" CssClass="txtNormal" MaxLength="6" 
                                                                                                                Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                            <asp:RangeValidator ID="Material_Val_txtQuantidade" runat="server" 
                                                                                                                ControlToValidate="Material_txtQuantidade" CssClass="textValidator" Display="Dynamic" 
                                                                                                                EnableClientScript="true" ErrorMessage="[QUANT. MATERIAL] -&gt; Valor Inv�lido. {1-999999}" 
                                                                                                                MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="Material">Valor Inv�lido.<br>{1-999999}</br>
                                                                </asp:RangeValidator>
                                                                                                            <asp:RequiredFieldValidator ID="Material_Val_ReqtxtQuantidade" runat="server" 
                                                                                                                ControlToValidate="Material_txtQuantidade" CssClass="textValidator" Display="Dynamic" 
                                                                                                                EnableClientScript="True" ErrorMessage="[MATERIAL] -&gt; Indique uma quantidade" 
                                                                                                                ValidationGroup="Material">Indique uma quantidade</asp:RequiredFieldValidator>
                                                                                                        </asp:Panel>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- VALOR --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="10%" HeaderText="VALOR" ItemStyle-Width="10%">
                                                                                                    <ItemTemplate>
                                                                                                        <%# DataBinder.Eval(Container.DataItem, "VALOR")%>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <asp:Panel ID="Panel_Material_Valor" runat="server" DefaultButton="Material_lnkInserir">
                                                                                                            <asp:TextBox ID="Material_txtValor" runat="server" CssClass="txtNormal" ReadOnly="True" 
                                                                                                                Style="text-align: right" Width="100%"></asp:TextBox>
                                                                                                            <asp:RangeValidator ID="Material_Val_txtValor" runat="server" ControlToValidate="Material_txtValor" 
                                                                                                                CssClass="textValidator" Display="Dynamic" EnableClientScript="true" 
                                                                                                                ErrorMessage="[MATERIAL] -&gt; Valor Inv�lido.  {0-9999999999}" MaximumValue="9999999999" 
                                                                                                                MinimumValue="0" Type="Double" ValidationGroup="Material">Valor Inv�lido.<br>{0-9999999999}</br>
                                                                </asp:RangeValidator>
                                                                                                            <asp:RequiredFieldValidator ID="Material_Val_ReqtxtValor" runat="server" 
                                                                                                                ControlToValidate="Material_txtValor" CssClass="textValidator" Display="Dynamic" 
                                                                                                                EnableClientScript="True" ErrorMessage="[MATERIAL] -&gt; Indique um valor" 
                                                                                                                ValidationGroup="Material">Indique um valor</asp:RequiredFieldValidator>
                                                                                                        </asp:Panel>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- OBSERVA��ES --%>
                                                                                                <asp:TemplateColumn FooterStyle-Width="50%" HeaderText="OBSERVA��ES">
                                                                                                    <ItemStyle HorizontalAlign="Left" Width="50%" />
                                                                                                    <ItemTemplate>
                                                                                                        <%# DataBinder.Eval(Container.DataItem, "OBSERVACOES")%>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <asp:Panel ID="Panel_Material_Obs" runat="server" DefaultButton="Material_lnkInserir">
                                                                                                            <asp:TextBox ID="Material_txtObservacoes" runat="server" CssClass="txtNormal" MaxLength="200" 
                                                                                                                Width="100%"></asp:TextBox>
                                                                                                        </asp:Panel>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%-- BOT�ES ############################################ --%>
                                                                                                <asp:TemplateColumn>
                                                                                                    <HeaderStyle BackColor="Transparent" ForeColor="Transparent" />
                                                                                                    <ItemStyle BackColor="Transparent" HorizontalAlign="Left" VerticalAlign="Top" Width="1%" />
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="C_btn_dgMaterial_Delete" runat="server" CausesValidation="false" 
                                                                                                            CommandName="Apagar" meta:resourcekey="C_btn_dgMaterial_Delete" 
                                                                                                            OnClientClick="javascript: return confirm('Deseja mesmo remover o material?');" 
                                                                                                            Visible='<%# this.Material_dlVisibilidade.Attributes["CanUse"]=="True" %>' />
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        <br />
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td valign="middle">
                                                                                                                    <asp:ImageButton ID="Material_lnkInserir" runat="server" CommandName="Inserir" CssClass="button" 
                                                                                                                        meta:resourcekey="Material_lnkInserir" ValidationGroup="Material" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </FooterTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                            </Columns>
                                                                                        </asp:DataGrid>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <p>
                                                            </p>
                                                            <%-- OBSERVA��ES  ############################################################################## --%>
                                                            <p style="page-break-before: auto; page-break-after: auto">
                                                                &nbsp;<asp:Panel ID="boxObservacoes" runat="server" CssClass="box" DefaultButton="Dummy">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <%-- HEADER ######################################################################## --%>
                                                                        <tr onclick="javascript:ShowHide('Observacoes');" style="cursor: hand; padding: 3px 3px 3px 3px;">
                                                                            <td class="titulo_peq tituloBarra" width="1%">
                                                                                <%if (!this.is_PrintMode)
                                  {%>
                                                                                <label ID="C_btn_Observacoes_box" style="font-size: 12pt; color: white; font-family: Symbol">
                                                                                �</label> <%} %>
                                                                            </td>
                                                                            <td class="titulo_peq tituloBarra">
                                                                                <%= this.GetLocalResourceObject("Panel_Observacoes.Header").ToString()%>
                                                                            </td>
                                                                            <td class="titulo_peq tituloBarra" width="1%">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <%-- CONTENT ####################################################################### --%>
                                                                        <tr ID="Observacoes">
                                                                            <td class="backColor cell" colspan="3">
                                                                                <table border="0" class="table">
                                                                                    <%-- OBSERVA��ES GERAIS --%>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <label class="titulo_peq">
                                                                                            Gerais</label>
                                                                                            <asp:TextBox ID="Observacoes_txtObservacoes" runat="server" CssClass="txtNormal" MaxLength="4000" 
                                                                                                onkeyDown="return checkTextAreaMaxLength(this,event,'4000');" Rows="10" TextMode="MultiLine" 
                                                                                                Width="100%"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%-- MOTIVOS --%>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Panel ID="box_ObservacoesMotivo" runat="server" DefaultButton="Dummy">
                                                                                                <label class="titulo_peq">
                                                                                                Registo de Altera��es ao contrato</label>
                                                                                                <asp:TextBox ID="Observacoes_txtObservacoes_Motivo" runat="server" CssClass="txtNormal" 
                                                                                                    ReadOnly="true" Rows="10" TextMode="MultiLine" Width="100%" Wrap="true"></asp:TextBox>
                                                                                            </asp:Panel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Panel ID="ObservacoesAdd" runat="server" DefaultButton="C_btn_Observacoes_add">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:TextBox ID="Observacoes_Motivo_Add" runat="server" CssClass="txtNormal" MaxLength="300" 
                                                                                                                TextMode="SingleLine" Width="100%"></asp:TextBox>
                                                                                                            <asp:RequiredFieldValidator ID="Observacoes_Motivo_Add_val" runat="server" 
                                                                                                                ControlToValidate="Observacoes_Motivo_Add" CssClass="textValidator" Display="Dynamic" 
                                                                                                                EnableClientScript="true" ErrorMessage="[OBSERVA��ES_MOTIVO] -&gt; Indique um motivo" 
                                                                                                                ValidationGroup="Observacoes">Indique um motivo</asp:RequiredFieldValidator>
                                                                                                        </td>
                                                                                                        <td width="1%">
                                                                                                            <asp:ImageButton ID="C_btn_Observacoes_add" runat="server" meta:resourcekey="C_btn_Observacoes_add" 
                                                                                                                OnClick="C_btn_Observacoes_add_Click" ValidationGroup="Observacoes" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </asp:Panel>
                                                                                            <asp:HiddenField ID="C_lbl_Motivo" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                                <p>
                                                                </p>
                                                                <%-- FICHEIROS CONTRATO  ############################################################################## --%>
                                                                <p style="page-break-before: auto; page-break-after: auto">
                                                                    &nbsp;<asp:Panel ID="boxFicheiros" runat="server" CssClass="box" DefaultButton="Dummy" Visible="false">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:FileUpload ID="FileUpLoad1" runat="server" EnableViewState="true" />
                                                                                    <asp:Button ID="UploadBtn" runat="server" OnClick="UploadBtn_Click" Text="Upload File" 
                                                                                        Width="105px" />
                                                                                    <asp:Label ID="lblFile" runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <br />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:DataGrid ID="C_dg_Ficheiros" runat="server" AutoGenerateColumns="False" 
                                                                                        BackColor="Transparent" BorderStyle="None" CellSpacing="2" GridLines="None" 
                                                                                        OnItemCommand="Condicoes_dgFicheiros_ItemCommand">
                                                                                        <HeaderStyle BackColor="#99CCFF" ForeColor="#00008B" Height="20px" HorizontalAlign="Center" />
                                                                                        <ItemStyle CssClass="ListRow" />
                                                                                        <AlternatingItemStyle CssClass="AlternateListRow" />
                                                                                        <Columns>
                                                                                            <asp:BoundColumn DataField="ID_FILE" Visible="false"></asp:BoundColumn>
                                                                                            <asp:BoundColumn DataField="FILE_PATH" Visible="false"></asp:BoundColumn>
                                                                                            <asp:TemplateColumn HeaderText="FICHEIRO">
                                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                                                <ItemTemplate>
                                                                                                    <asp:HyperLink ID="lnkFILE" runat="server" 
                                                                                                        NavigateUrl='<%# "DocViewer2.aspx?path=" + DataBinder.Eval(Container.DataItem, "FILE_PATH").ToString() + DataBinder.Eval(Container.DataItem, "FILE_NAME").ToString() %>' 
                                                                                                        target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "FILE_NAME")%>'></asp:HyperLink>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateColumn>
                                                                                            <asp:TemplateColumn HeaderText="Data Inser��o">
                                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                                                <ItemTemplate>
                                                                                                    <%# DataBinder.Eval(Container.DataItem, "INSERT_DATE")%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateColumn>
                                                                                            <asp:TemplateColumn HeaderText="Utilizador">
                                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                                                <ItemTemplate>
                                                                                                    <%# DataBinder.Eval(Container.DataItem, "INSERT_USER")%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateColumn>
                                                                                            <asp:TemplateColumn>
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="C_btn_dgFicheiros_Delete" runat="server" CausesValidation="false" 
                                                                                                        CommandName="Apagar" meta:resourcekey="C_btn_dgFicheiros_Delete" 
                                                                                                        OnClientClick="javascript: return confirm('Deseja mesmo apagar o ficheiro?');" 
                                                                                                        visible='<%# User.IsInRole("SMI") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateColumn>
                                                                                        </Columns>
                                                                                    </asp:DataGrid>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                    <p>
                                                                    </p>
                                                                    <%-- BOT�ES  ################################################################################### --%>
                                                                    <asp:Panel ID="boxBotoes" runat="server" CssClass="box" DefaultButton="Dummy">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <%-- DATA ############################################################################## --%>
                                                                            <tr ID="box_dataFimEfectivo" runat="server">
                                                                                <td align="center" colspan="3">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label class="label button">
                                                                                                Data de fim efectivo:&nbsp;
                                                                                                </label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <UC:DatePicker ID="Botoes_txtDataEfectiva" runat="server" AllowClear="false" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <br />
                                                                                    <br />
                                                                                </td>
                                                                            </tr>
                                                                            <%-- BOT�ES ############################################################################ --%>
                                                                            <tr>
                                                                                <td align="left" valign="top" width="5%">
                                                                                </td>
                                                                                <td align="left" valign="top" width="5%">
                                                                                    <UC:ImageTextButton ID="C_btn_Editar" runat="server" CausesValidation="False" 
                                                                                        ImageUrl="~/images/FormChange.ico" OnClick="Botoes_btEditar_Click" 
                                                                                        Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EDITAR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" TextCssClass="button" 
                                                                                        TextPosition="Bottom" ToolTip="Editar o contrato" />
                                                                                </td>
                                                                                <td align="center" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="50%">
                                                                                        <tr>
                                                                                            <td align="center" valign="top">
                                                                                                <UC:ImageTextButton ID="Botoes_btValidar" runat="server" TextCssClass="button" TextPosition="Bottom"
                                                                                                    Text="VALIDAR" ToolTip="Validar o contrato" ImageUrl="~/images/FormValidate.ico" OnClick="Botoes_btValidar_Click" />
                                                                                            </td>
                                                                                            <td align="center" valign="top">
                                                                                                <UC:ImageTextButton ID="Botoes_btEnviar" runat="server" ImageUrl="~/images/FormSave.ico" 
                                                                                                    OnClick="Botoes_btEnviar_Click" Text="INSERIR" TextCssClass="button" TextPosition="Bottom" 
                                                                                                    ToolTip="Inserir o contrato" />
                                                                                            </td>
                                                                                            <td align="center" valign="top">
                                                                                                <UC:ImageTextButton ID="Botoes_btGuardar" runat="server" ImageUrl="~/images/FormSave.ico" 
                                                                                                    OnClick="Botoes_btGuardar_Click" Text="GUARDAR" TextCssClass="button" TextPosition="Bottom" 
                                                                                                    ToolTip="Guardar as altera��es do contrato" />
                                                                                            </td>
                                                                                            <td align="center" valign="top">
                                                                                                <UC:ImageTextButton ID="C_Botoes_btn_Seguinte" runat="server" CausesValidation="true" 
                                                                                                    meta:resourcekey="C_Botoes_btn_Seguinte" OnClick="C_Botoes_btn_Seguinte_Click" 
                                                                                                    TextCssClass="button titulo" TextPosition="Bottom" ValidationGroup="Entidades" />
                                                                                            </td>
                                                                                            <td align="center" style="border-top: gray 1px solid" valign="top">
                                                                                                <UC:ImageTextButton ID="Botoes_btSubstituirCanc" runat="server" 
                                                                                                    ImageUrl="~/images/FormReplace1.ico" OnClick="Botoes_btSubstituirCanc_Click" 
                                                                                                    Text="SUBSTITUIR&lt;br/&gt;(Cancelamento)" TextCssClass="button" TextPosition="Bottom" 
                                                                                                    ToolTip="Termina o contrato actual e substitui-o por cancelamento" />
                                                                                            </td>
                                                                                            <td align="center" style="border-top: gray 1px solid" valign="top">
                                                                                                <UC:ImageTextButton ID="Botoes_btTerminar" runat="server" ImageUrl="~/images/FormEnd.ico" 
                                                                                                    OnClick="Botoes_btTerminar_Click" Text="TERMINAR&lt;br/&gt;CONTRATO" TextCssClass="button" 
                                                                                                    TextPosition="Bottom" ToolTip="Termina e bloqueia o contrato" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                    <br />
                                                                    <%-- BOT�ES WF ################################################################################### --%>
                                                                    <asp:Panel ID="boxBotoes_WF" runat="server" CssClass="box" DefaultButton="Dummy">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <%-- BOT�ES WF ############################################################################ --%>
                                                                            <tr>
                                                                                <td align="center" colspan="4">
                                                                                    <asp:HyperLink ID="lnkVendas" runat="server" Target="_blank" 
                                                                                        Visible='<%# !(this.Request["idContrato"]==null || this.is_PrintMode) %>'>Resumo Vendas</asp:HyperLink>
                                                                                    <hr />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td ID="lblStateName" runat="server" align="left" colspan="2" valign="top" width="40%">
                                                                                    Estado actual:
                                                                                    <asp:Label ID="StateName" runat="server" Font-Bold="true"></asp:Label>
                                                                                </td>
                                                                                <td align="left" colspan="2" valign="top" width="60%">
                                                                                    <asp:Repeater ID="RepeaterWFActions" runat="server" OnItemCommand="C_Rpt_WFActions_ItemCommand" 
                                                                                        OnItemDataBound="C_Rpt_WFActions_ItemDataBound">
                                                                                        <ItemTemplate>
                                                                                            <asp:Button ID="ButtonWFAction" runat="server" CommandName="WFActionClick" />
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <br />
                                                                                    <br />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <UC1:ucBudgetSummary ID="C_BudgetSummary" runat="server" CausesValidation="false" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                    <%-- FOOTER  ################################################################################### --%>
                                                                    <asp:Panel ID="Panel1" runat="server" CssClass="box" DefaultButton="Dummy">
                                                                        <UC:Footer ID="Footer1" runat="server" />
                                                                    </asp:Panel>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                </p>
                                                            </p>
                                                        </p>
                                                    </p>
                                                </p>
                                            </p>
                                        </p>
                                    </p>
                                </p>
                            </p>
                        </p>
                    </p>
                </p>
            </p>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="UploadBtn" />
        </Triggers>
    </AJAX:UpdatePanel>
    </form>
</body>
</html>
