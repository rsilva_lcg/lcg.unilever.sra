using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Configuration;
using System.Threading;
using System.Resources;
using System.Reflection;
using SpiritucSI.UserControls.GenericControls;
using SpiritucSI.Unilever.SRA.WF;
using System.Collections.Generic;
using System.Workflow.Activities;
using System.Collections.ObjectModel;
using System.Workflow.Runtime;
using System.Linq;
using SRADBInterface;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Web.Security;


public partial class contrato : SuperPage
{
    #region FIELDS
    /// <summary>
    /// 
    /// </summary>
    protected bool is_DBContrato = false;
    /// <summary>
    /// 
    /// </summary>
    protected bool is_Editing = false;
    /// <summary>
    /// 
    /// </summary>
    protected bool is_PrintMode = false;
    /// <summary>
    /// 
    /// </summary>
    protected decimal ContratoTime = 1;
    /// <summary>
    /// 
    /// </summary>
    protected bool ClientesLEs_SortASC = false;
    /// <summary>
    /// 
    /// </summary>
    protected string NumeroAnterior = "";
    /// <summary>
    /// 
    /// </summary>
    protected string Contrato_Status = "";
    /// <summary>
    /// 
    /// </summary>
    protected string Contrato_Classif = "";
    /// <summary>
    /// 
    /// </summary>
    protected int Contrapartidas_dlNextID = 0;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtblInvestimento;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtblClientesLEs;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtbl_Condicoes_Equipamento;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtbl_Condicoes_Equipamento_Tipo;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtbl_Contrapartidas;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtbl_Contrapartidas_Bonus;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtbl_ContrapartidasGerais_Bonus;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtbl_Contrapartidas_Grupos;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtbl_Faseamento;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtbl_MaterialSource;
    /// <summary>
    /// 
    /// </summary>
    protected DataTable dtbl_Material;

    //public string uploadText = "Upload file:";
    //public string saveText = "Save as:";
    //public string statusText = "Status:";
    //public string submitText = "Upload File";
    //public string uploadFolder = "c:\\temp\\SRA\\";
    #endregion

    #region FIELDS WF
    public string uid { get; set; }
    public int stateid { get; set; }

    public List<WFAction> wfActions
    {
        get
        {
            if (Session["WorkflowActionsList"] != null)
                return (List<WFAction>)(Session["WorkflowActionsList"]);
            else
                return new List<WFAction>();
        }
        set
        {
            Session["WorkflowActionsList"] = value;
        }
    }
    private bool showInstructions;

    #endregion

    #region METHODS

    #region AUXILIAR
    /// <summary> APRESENTA UM ERRO/WARNING COM A MENSSAGEM INDICADA </summary>
    /// <param name="Destiny">  LABEL DE DESTINO DA MENSSAGEM</param>
    /// <param name="Message">  MENSSAGEM A APRESENTAR</param>
    /// <param name="e">        EXCEP��O QUE DEU ORIGEM AO ERRO</param>
    private void _M_ShowError(WebControl Destiny, string Message, Exception e)
    {
        this._M_ShowError(Destiny, Message, e, false, "");
    }
    /// <summary> APRESENTA UM ERRO/WARNING COM A MENSSAGEM INDICADA E POSS�VEL POPUP</summary>
    /// <param name="Destiny">  LABEL DE DESTINO DA MENSSAGEM</param>
    /// <param name="Message">  MENSSAGEM A APRESENTAR</param>
    /// <param name="e">        EXCEP��O QUE DEU ORIGEM AO ERRO</param>
    /// <param name="ShowPopup">TRUE: APRESENTA UM POPUP COM A MENSAGEM DE ERRO</param>
    private void _M_ShowError(WebControl Destiny, string Message, Exception e, bool ShowPopup)
    {
        this._M_ShowError(Destiny, Message, e, ShowPopup, "");
    }
    /// <summary> APRESENTA UM ERRO/WARNING COM A MENSSAGEM INDICADA, POSS�VEL POPUP E SALTA PARA UM NOVO URL</summary>
    /// <param name="Destiny">      LABEL DE DESTINO DA MENSSAGEM</param>
    /// <param name="Message">      MENSSAGEM A APRESENTAR</param>
    /// <param name="e">            EXCEP��O QUE DEU ORIGEM AO ERRO</param>
    /// <param name="ShowPopup">    TRUE: APRESENTA UM POPUP COM A MENSAGEM DE ERRO</param>
    /// <param name="NavigateUrl">  URL PARA ONDE SALTAR AP�S APRESENTAR O ERRO</param>
    private void _M_ShowError(WebControl Destiny, string Message, Exception e, bool ShowPopup, string NavigateUrl)
    {
        ((Label)Destiny).Text = Message;
        Exception ex = e;
        while (ex != null)
        {
            Destiny.ToolTip += ex.Message + "\n";
            ex = ex.InnerException;
        }
        if (ShowPopup || NavigateUrl.Length > 0)
        {
            string script = "<script type='text/Jscript'> ";

            if (ShowPopup)
            {
                script += "alert('" + Message + "'); ";
            }
            if (NavigateUrl.Length > 0)
            {
                script += "navigate('" + NavigateUrl + "');";
            }
            script += "</script>";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, false);
        }
    }
    /// <summary>
    /// TODO: 99 - PREENCHER DOCUMENTA��O
    /// </summary>
    /// <param name="source"></param>
    /// <param name="ControlID"></param>
    /// <returns></returns>
    private Control FindControlIN(Control source, string ControlID)
    {
        if (source.ID == ControlID)
            return source;
        foreach (Control c in source.Controls)
        {
            Control d = this.FindControlIN(c, ControlID);
            if (d != null)
                return d;
        }
        return null;
    }

    /// <summary>
    /// TODO: 99 - PREENCHER DOCUMENTA��O
    /// </summary>
    /// <param name="c"></param>
    /// <param name="readOnly"></param>
    private void SetReadOnly(Control c, bool readOnly)
    {
        Type t = c.GetType();
        PropertyInfo p1 = t.GetProperty("Enabled");
        PropertyInfo p2 = t.GetProperty("ReadOnly");
        PropertyInfo p3 = t.GetProperty("CssClass");
        PropertyInfo p4 = t.GetProperty("class");

        if (p2 != null)
        {
            p2.SetValue(c, readOnly, null);
            if (p3 != null)
            {
                string s = p3.GetValue(c, null).ToString();
                if (readOnly && !s.Contains("ReadOnly"))
                    s += " ReadOnly";
                if (!readOnly && s.Contains("ReadOnly"))
                    s = s.Replace(" ReadOnly", "");
                p3.SetValue(c, s, null);
            }
            if (p4 != null)
            {
                string s = p4.GetValue(c, null).ToString();
                if (readOnly && !s.Contains("ReadOnly"))
                    s += " ReadOnly";
                if (!readOnly && s.Contains("ReadOnly"))
                    s = s.Replace(" ReadOnly", "");
                p4.SetValue(c, s, null);
            }
        }
        else
        {
            if (p1 != null)
            {
                p1.SetValue(c, !readOnly, null);
            }
        }
    }
    #endregion

    #region SESSION
    /// <summary>
    /// 
    /// </summary>
    private void ReadSession()
    {
        #region READ SESSION VARIABLES
        this.is_DBContrato = (bool)this.Session["is_DBContrato"];
        this.is_Editing = (bool)this.Session["is_Editing"];
        this.ClientesLEs_SortASC = (bool)this.Session["ClientesLEs_SortASC"];
        this.NumeroAnterior = (string)this.Session["NumeroAnterior"];
        this.Contrato_Status = (string)this.Session["Contrato_Status"];
        this.Contrato_Classif = (string)this.Session["Contrato_Classif"];
        this.dtblInvestimento = (DataTable)this.Session["dtblInvestimento"];
        this.dtblClientesLEs = (DataTable)this.Session["dtblClientesLEs"];
        this.dtbl_Condicoes_Equipamento = (DataTable)this.Session["dtbl_Condicoes_Equipamento"];
        this.dtbl_Condicoes_Equipamento_Tipo = (DataTable)this.Session["dtbl_Condicoes_Equipamento_Tipo"];
        this.dtbl_Contrapartidas = (DataTable)this.Session["dtbl_Contrapartidas"];
        this.dtbl_Contrapartidas_Bonus = (DataTable)this.Session["dtbl_Contrapartidas_Bonus"];
        this.dtbl_ContrapartidasGerais_Bonus = (DataTable)this.Session["dtbl_ContrapartidasGerais_Bonus"];
        this.dtbl_Contrapartidas_Grupos = (DataTable)this.Session["dtbl_Contrapartidas_Grupos"];
        this.Contrapartidas_dlNextID = (int)this.Session["Contrapartidas_dlNextID"];
        this.dtbl_Faseamento = (DataTable)this.Session["dtbl_Faseamento"];
        this.dtbl_MaterialSource = (DataTable)this.Session["dtbl_MaterialSource"];
        this.dtbl_Material = (DataTable)this.Session["dtbl_Material"];
        #endregion

        #region CLEAR SESSION VARIABLES
        this.Session["is_DBContrato"] = null;
        this.Session["ClientesLEs_SortASC"] = null;
        this.Session["NumeroAnterior"] = null;
        this.Session["Contrato_Num_Sequencia"] = null;
        this.Session["Contrato_Status"] = null;
        this.Session["Contrato_Classif"] = null;
        this.Session["dtblInvestimento"] = null;
        this.Session["dtblClientesLEs"] = null;
        this.Session["dtbl_Condicoes_Equipamento"] = null;
        this.Session["dtbl_Condicoes_Equipamento_Tipo"] = null;
        this.Session["dtbl_Contrapartidas"] = null;
        this.Session["dtbl_Contrapartidas_Bonus"] = null;
        this.Session["dtbl_ContrapartidasGerais_Bonus"] = null;
        this.Session["dtbl_Contrapartidas_Grupos"] = null;
        this.Session["Contrapartidas_dlNextID"] = null;
        this.Session["dtbl_Faseamento"] = null;
        this.Session["dtbl_MaterialSource"] = null;
        this.Session["dtbl_Material"] = null;
        #endregion
    }
    /// <summary>
    /// 
    /// </summary>
    private void WriteSession()
    {
        this.Session["is_DBContrato"] = this.is_DBContrato;
        this.Session["is_Editing"] = this.is_Editing;
        this.Session["ClientesLEs_SortASC"] = this.ClientesLEs_SortASC;
        this.Session["NumeroAnterior"] = this.NumeroAnterior;
        this.Session["Contrato_Status"] = this.Contrato_Status;
        this.Session["Contrato_Classif"] = this.Contrato_Classif;
        this.Session["dtblInvestimento"] = this.dtblInvestimento;
        this.Session["dtblClientesLEs"] = this.dtblClientesLEs;
        this.Session["dtbl_Condicoes_Equipamento"] = this.dtbl_Condicoes_Equipamento;
        this.Session["dtbl_Condicoes_Equipamento_Tipo"] = this.dtbl_Condicoes_Equipamento_Tipo;
        this.Session["dtbl_Contrapartidas"] = this.dtbl_Contrapartidas;
        this.Session["dtbl_Contrapartidas_Bonus"] = this.dtbl_Contrapartidas_Bonus;
        this.Session["dtbl_ContrapartidasGerais_Bonus"] = this.dtbl_ContrapartidasGerais_Bonus;
        this.Session["dtbl_Contrapartidas_Grupos"] = this.dtbl_Contrapartidas_Grupos;
        this.Session["Contrapartidas_dlNextID"] = this.Contrapartidas_dlNextID;
        this.Session["dtbl_Faseamento"] = this.dtbl_Faseamento;
        this.Session["dtbl_MaterialSource"] = this.dtbl_MaterialSource;
        this.Session["dtbl_Material"] = this.dtbl_Material;
    }
    #endregion

    #region INIT
    /// <summary>
    /// TODO: 99 - PREENCHER DOCUMENTA��O
    /// </summary>
    /// <returns></returns>
    protected ListDictionary PrazosPagamento()
    {
        ListDictionary lst = new ListDictionary();
        lst.Add("C", "Contrato");
        lst.Add("A", "Anual");
        lst.Add("S", "Semestral");
        lst.Add("Q", "Quadrimestral");
        lst.Add("T", "Trimestral");
        lst.Add("B", "Bimensal");
        lst.Add("M", "Mensal");
        return lst;
    }
    /// <summary>
    /// 
    /// </summary>
    private void initializeStaticData()
    {
        SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);

        #region CONTRATO
        this.Contrato_ddlTipo.Items.Add(new ListItem("SRA � Service Rendered Agreement", "SRA"));
        this.Contrato_ddlTipo.Items.Add(new ListItem("CN  � Contrato Nacional", "CN"));
        this.Botoes_txtDataEfectiva.SelectedDate = DateTime.Now;
        this.Condicoes_txtDataInicio.SelectedDate = DateTime.Now;
        this.Condicoes_txtDataFim.MinEnabledDate = DateTime.Now;
        DataView dv = DBsra.getZonas().DefaultView;
        dv.Sort = "DES_ZONA ASC";
        this.DL_CONCESSIONARIO.DataSource = dv;
        foreach (DataRow row in dv.ToTable().Rows)
        {
            if ((User.IsInRole("FinantialDirector")) || (User.IsInRole("FinantialDirector")) || (User.IsInRole("FinantialDirector")))
            { // carrega todas
                DL_CONCESSIONARIO.Items.Add(new ListItem(row["DES_ZONA"].ToString(), row["COD_ZONA"].ToString()));
            }
            else
            { // carrega apenas as zonas a q o user pertence
                if (User.IsInRole(row["COD_ZONA"].ToString()))
                {
                    DL_CONCESSIONARIO.Items.Add(new ListItem(row["DES_ZONA"].ToString(), row["COD_ZONA"].ToString()));
                }
            }
        }
        //this.DL_CONCESSIONARIO.DataBind();
        this.DL_CONCESSIONARIO.Items.Insert(0, new ListItem("", string.Empty));
        #endregion

        #region NIVEL
        this.C_cb_Nivel_LE.Checked = true;
        this.C_ddl_GrupoEconomico.DataSource = DBsra.GrupoEconomicoGet();
        this.C_ddl_GrupoEconomico.DataBind();
        this.C_ddl_GrupoEconomico.Items.Insert(0, new ListItem("", string.Empty));
        this.C_cb_Nivel_Changed(this.C_cb_Nivel_LE, new EventArgs());
        #endregion

        #region CONDI��ES
        this.Condicoes_txtObjectivo.Text = ConfigurationManager.AppSettings["Equipamento_Objectivo"].ToString();

        if (!this.Condicoes_txtDataFim.Enabled)
            this.Condicoes_txtDataFim.Clear();
        else
            this.Condicoes_txtDataFim.SelectedDate = this.Condicoes_txtDataInicio.SelectedDate.AddYears(1).AddDays(-1);
        this.Condicoes_cbGamas.DataSource = DBsra.getGrupoProduto();
        this.Condicoes_cbGamas.DataTextField = Convert.ToString(ConfigurationManager.AppSettings["Contrapartidas_lbGrupoShortTextColumnName"]);
        this.Condicoes_cbGamas.DataValueField = Convert.ToString(ConfigurationManager.AppSettings["Contrapartidas_lbGrupoValueColumnName"]);
        this.Condicoes_cbGamas.DataBind();

        this.dtbl_Condicoes_Equipamento = new DataTable();
        string[] ListColumns = ConfigurationManager.AppSettings["Equipamento_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Condicoes_Equipamento.Columns.Add(column);
        }
        this.dtbl_Condicoes_Equipamento_Tipo = DBsra.getEquipamentoFrio();
        this.dtbl_Condicoes_Equipamento_Tipo.PrimaryKey = new DataColumn[] { this.dtbl_Condicoes_Equipamento_Tipo.Columns[Convert.ToString(ConfigurationManager.AppSettings["Equipamento_KeyColumnName"])] };
        this.dtbl_Condicoes_Equipamento_Tipo.DefaultView.Sort = Convert.ToString(ConfigurationManager.AppSettings["Equipamento_lbTextColumnName"]) + " ASC";
        #endregion

        #region CONTRAPARTIDAS
        this.dtbl_Contrapartidas_Grupos = DBsra.getGrupoProduto();
        this.dtbl_Contrapartidas_Grupos.Columns.Add("IS_VISIBLE");
        foreach (DataRow dr in this.dtbl_Contrapartidas_Grupos.Rows)
        {
            dr["IS_VISIBLE"] = "T";
        }
        this.dtbl_Contrapartidas_Grupos.DefaultView.RowFilter = "IS_VISIBLE = 'T'";


        this.dtbl_Contrapartidas = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["Contrapartidas_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Contrapartidas.Columns.Add(column);
        }

        this.dtbl_Contrapartidas_Bonus = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["Contrapartidas_Bonus_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Contrapartidas_Bonus.Columns.Add(column);
        }
        this.dtbl_Contrapartidas_Bonus.Columns["VALOR_LIMITE_MIN"].DataType = typeof(int);
        #endregion

        #region CONTRAPARTIDAS GERAIS
        this.dtbl_ContrapartidasGerais_Bonus = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["ContrapartidasGerais_Bonus_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_ContrapartidasGerais_Bonus.Columns.Add(column);
        }
        this.dtbl_ContrapartidasGerais_Bonus.Columns["VALOR_LIMITE_MIN"].DataType = typeof(int);

        this.dtbl_ContrapartidasGerais_Bonus.DefaultView.Sort = "VALOR_LIMITE_MIN ASC, TIPO ASC";
        this.ContrapartidasGerais_dgBonus.DataSource = this.dtbl_ContrapartidasGerais_Bonus.DefaultView;
        this.ContrapartidasGerais_dgBonus.DataBind();

        this.ContrapartidasGerais_cbGamas.DataSource = new DataView(this.dtbl_Contrapartidas_Grupos, "IS_VISIBLE='F'", "", DataViewRowState.CurrentRows);
        this.ContrapartidasGerais_cbGamas.DataTextField = Convert.ToString(ConfigurationManager.AppSettings["Contrapartidas_lbGrupoTextColumnName"]);
        this.ContrapartidasGerais_cbGamas.DataValueField = Convert.ToString(ConfigurationManager.AppSettings["Contrapartidas_lbGrupoValueColumnName"]);

        this.ContrapartidasGerais_ddlPrazo.DataSource = this.PrazosPagamento();
        this.ContrapartidasGerais_ddlPrazo.DataBind();
        this.ContrapartidasGerais_ddlPrazo.SelectedValue = "A";
        #endregion

        #region FASEAMENTO
        this.dtbl_Faseamento = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["Faseamento_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Faseamento.Columns.Add(column);
        }
        this.dtbl_Faseamento.Columns["DATA_PAGAMENTO"].DataType = typeof(DateTime);
        this.dtbl_Faseamento.Columns["DATA_PAGAMENTO_EFECTUADO"].DataType = typeof(DateTime);
        #endregion

        #region MATERIAL
        this.dtbl_MaterialSource = DBsra.getMaterial();
        this.dtbl_MaterialSource.PrimaryKey = new DataColumn[] { this.dtbl_MaterialSource.Columns[Convert.ToString(ConfigurationManager.AppSettings["Material_KeyColumnName"])] };

        this.dtbl_Material = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["Material_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Material.Columns.Add(column);
        }
        #endregion
    }
    /// <summary>
    /// 
    /// </summary>
    private void initializeContrato()
    {
        this.initializeStaticData();
        this.is_DBContrato = (this.Request["idContrato"] != null && this.LoadContrato(Convert.ToInt32(this.Request["idContrato"])));

        if (!is_DBContrato)
        {
            this.Contrato_Classif = "N";
            this.Contrato_Status = "I";
            this.Contrato_ddlTipo.SelectedValue = "SRA";
            this.C_Contrato_lbl_Data.Text = DateTime.Now.ToString("dd-MM-yyyy");
            this.ContrapartidasGerais_ddlPrazo.SelectedValue = "A";
            this.C_lbl_Prazo.Text = this.ContrapartidasGerais_ddlPrazo.SelectedItem.Text;
            this.C_lbl_Motivo.Value = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> NOVO CONTRATO" + "\n" + this.Observacoes_txtObservacoes_Motivo.Text;
            this.Observacoes_txtObservacoes_Motivo.Text = this.C_lbl_Motivo.Value;
        }
        this.lnkVendas.NavigateUrl = "~/SRA_Sales.aspx?idContrato=" + this.Request["idContrato"];
        this.RefreshState();
        this.loadNumAnterior();
        this.loadStatus();
        this.loadTipo();
        this.LoadPesquisa();
        this.LoadClientes();
        this.loadCondicoes();
        this.loadContrapartidas();
        this.loadContrapartidasGeraisBonus();
        this.ContrapartidasGerais_cbGamas.DataSource = new DataView(this.dtbl_Contrapartidas_Grupos, "IS_VISIBLE='F'", "", DataViewRowState.CurrentRows);
        this.ContrapartidasGerais_cbGamas.DataBind();
        this.loadContrapartidasGamas();
        this.RefreshGamas();
        this.loadFaseamento();
        this.loadMaterial();
        this.loadObservacoes();
        this.loadInvestimento();

        this.boxInvestimento.Visible = this.is_DBContrato;
        this.boxContrato.Visible = this.is_DBContrato;
        this.boxPontoVenda.Visible = this.is_DBContrato;
        this.boxCondicoes.Visible = this.is_DBContrato;
        this.boxContrapartidas.Visible = this.is_DBContrato;
        this.BoxContrapartidasGlobais.Visible = this.is_DBContrato;
        this.boxFaseamento.Visible = this.is_DBContrato;
        this.boxMaterial.Visible = this.is_DBContrato;
        //this.boxBotoes.Visible = !this.is_DBContrato;
        this.boxFicheiros.Visible = this.is_DBContrato;
        this.boxObservacoes.Visible = this.is_DBContrato;
        if (this.is_DBContrato)
            this.lbl_SubTitulo.Text = "VER / EDITAR CONTRATO";
        else
            this.lbl_SubTitulo.Text = "INSERIR CONTRATO";
        this.RefreshState();
        this.loadNumSeguinte();
        this.loadFimEfectivo();

        RefreshFicheiros();
    }

    private void RefreshFicheiros()
    {
        SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
        this.C_dg_Ficheiros.DataSource = DBsra.getContratoFiles(Convert.ToInt32(this.Request["idContrato"]));
        this.C_dg_Ficheiros.DataBind();

        this.C_dg_Ficheiros.Visible = (this.C_dg_Ficheiros.Items.Count > 0);
    }
    #endregion

    #region STATE
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected void C_val_Contrato_validate(object sender, ServerValidateEventArgs e)
    {
        this.loadInvestimento();
        e.IsValid = this.IsValid;
        Page.Validate("Processo");
        e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("Entidades");
        e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("PontoVenda");
        e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("Condicoes");
        e.IsValid = e.IsValid && this.IsValid;

        //Page.Validate("ContrapartidasGama");
        //e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("ContrapartidasGlobais");
        e.IsValid = e.IsValid && this.IsValid;

        Page.Validate("TPR");
        e.IsValid = e.IsValid && this.IsValid;

        if (!e.IsValid)
        {
            this._M_ShowError(this.lblWarning, "O FORMUL�RIO N�O EST� PREENCHIDO CORRECTAMENTE.", null, true);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private bool ContratoValido()
    {
        Page.Validate("contrato");
        return this.IsValid;
    }
    /// <summary>
    /// 
    /// </summary>
    private void RefreshState()
    {
        UserRoles UserLevel = UserRoles.Administrator; // ((Utilizador)this.Session["User"]).PermissionLevel();

        #region O CONTRATO N�O � EDIT�VEL
        if (this.is_DBContrato || UserLevel <= UserRoles.Viewer)
        {
            this.ContratoEnabled(false);
            this.Contrato_ddlTipo.Enabled = false;
            this.NivelEnabled(false);
            this.PesquisaEnabled(false);
            this.ClientesEnabled(false);
            this.PontoVendaEnabled(false);
            this.CondicoesEnabled(false);
            this.ContrapartidasEnabled(false);
            this.FaseamentoEnabled(false);
            this.MaterialEnabled(false);
            this.ObservacoesEnabled(false);
            this.ObservacoesMotivoEnabled(false);
        }
        #endregion

        #region O CONTRATO � EDIT�VEL PARA ALTERA��O
        else if (this.Contrato_Classif.Equals("A"))
        {
            this.ContratoEnabled(false);
            this.Contrato_ddlTipo.Enabled = true;
            this.NivelEnabled(true);
            this.PesquisaEnabled(true);
            this.ClientesEnabled(true);
            this.PontoVendaEnabled(false);
            this.CondicoesEnabled(false);
            this.ContrapartidasEnabled(false);
            this.FaseamentoEnabled(false);
            this.MaterialEnabled(true);
            this.ObservacoesEnabled(true);
            this.ObservacoesMotivoEnabled(true);
        }
        #endregion

        #region O CONTRATO � TOTALMENTE EDIT�VEL
        else
        {
            this.ContratoEnabled(true);
            this.Contrato_ddlTipo.Enabled = true;
            this.NivelEnabled(true);
            this.PesquisaEnabled(true);
            this.ClientesEnabled(true);
            this.PontoVendaEnabled(true);
            this.CondicoesEnabled(true);
            this.ContrapartidasEnabled(true);
            this.FaseamentoEnabled(true);
            this.MaterialEnabled(true);
            this.ObservacoesEnabled(true);
            this.ObservacoesMotivoEnabled(true);
        }
        #endregion

        #region ESTADO DOS BOT�ES
        this.Botoes_btEnviar.Visible = !this.is_DBContrato
                                                && !this.is_Editing
                                                && UserLevel >= UserRoles.PowerUser
                                                && !this.Contrato_Classif.Equals("A")
                                                && this.boxContrato.Visible;
        this.C_btn_Editar.Visible = this.is_DBContrato
                                                && UserLevel >= UserRoles.PowerUser
                                                && !this.Contrato_Status.Equals("T");
        this.Botoes_btGuardar.Visible = !is_DBContrato
                                                && UserLevel >= UserRoles.PowerUser
                                                && (this.Contrato_Classif.Equals("A") || this.is_Editing)
                                                && this.boxContrato.Visible;
        //this.Botoes_btSubstituirCanc.Attributes["onclick"] = ""
        //   + "javascript: if(confirm('Ao substituir este contrato, ser� criado um novo com um novo n�mero de processo.\\nO contrato actual ser� TERMINADO, ficando bloqueado e apenas dispon�vel para consulta.\\n\\nO contrato ser� TERMINADO com os seguintes dados:\\n\\n"
        //   + "                       PROCESSO:  " + this.C_txt_Processo_Actual.Text + "\\n"
        //   + "  DATA DE FIM EFECTIVO:  '+document.getElementById('Botoes_txtDataEfectiva').value+'\\n_____________________________________________________________________\\n\\nDeseja mesmo SUBSTITUIR o contrato, TERMINANDO o actual?'))"
        //   + " {var r = ''+ prompt('ESTE CONTRATO VAI SER SUBSTITUIDO PORQUE...','');"
        //   + " if(r.length > 0)"
        //   + " {document.getElementById('" + this.C_lbl_Motivo.ClientID + "').value = r;}"
        //   + " return true;}"
        //   + " return false;";
        //this.Botoes_btSubstituirCanc.Visible = this.is_DBContrato
        //                                        && UserLevel >= UserRoles.PowerUser
        //                                        && this.Contrato_Status.Equals("V");
        this.Botoes_btSubstituirCanc.Visible = false;

        this.Botoes_btTerminar.Attributes["onclick"] = ""
            + "javascript: if(confirm('Ao terminar o contrato este ser� bloqueado, ficando apenas dispon�vel para consulta.\\n\\nO contrato ser� TERMINADO com os seguintes dados:\\n\\n"
            + "                       PROCESSO:  " + this.C_txt_Processo_Actual.Text + "\\n"
            + "  DATA DE FIM EFECTIVO:  '+document.getElementById('Botoes_txtDataEfectiva').value+'\\n_____________________________________________________________________\\n\\nDeseja mesmo TERMINAR o contrato?'))"
            + " {var r = ''+ prompt('ESTE CONTRATO VAI SER TERMINADO PORQUE...','');"
            + " if(r.length > 0)"
            + " {document.getElementById('" + this.C_lbl_Motivo.ClientID + "').value = r;}"
            + " return true;}"
            + " return false;";
        this.Botoes_btTerminar.Visible = this.is_DBContrato
                                                && UserLevel >= UserRoles.Administrator
                                                && this.Contrato_Status.Equals("V");
        this.box_dataFimEfectivo.Visible = this.Botoes_btTerminar.Visible
                                                || this.Botoes_btSubstituirCanc.Visible;

        //this.C_Botoes_btn_Seguinte.Visible = !this.boxContrato.Visible;
        if (this.boxContrato.Visible)
        {
            this.C_Botoes_btn_Seguinte.Visible = false;
        }
        else
        {
            this.C_Botoes_btn_Seguinte.Visible = true;
        }
        //boxFicheiros.Visible = !this.Contrato_Status.Equals("T");
        #endregion
    }
    #endregion

    #region ACTIVA��O DOS BLOCOS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void ContratoEnabled(bool b)
    {
        this.SetReadOnly(this.C_txt_Processo_Actual, !b);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void NivelEnabled(bool b)
    {
        this.SetReadOnly(this.C_lbl_ZonaBase, !b);
        this.SetReadOnly(this.C_cb_Nivel_GE, !b);
        this.SetReadOnly(this.C_cb_Nivel_SGE, !b);
        this.SetReadOnly(this.C_cb_Nivel_LE, !b);
        this.SetReadOnly(this.C_ddl_GrupoEconomico, !b);
        this.SetReadOnly(this.C_ddl_SubGrupoEconomico, !b);
        this.SetReadOnly(this.DL_CONCESSIONARIO, !b);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void PesquisaEnabled(bool b)
    {
        this.C_box_Pesquisa.Visible = b && this.C_cb_Nivel_LE.Checked;
        if (this.C_box_Pesquisa.Visible)
            this.LoadPesquisa();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void ClientesEnabled(bool b)
    {
        this.C_dg_Entidades.Attributes["CanUse"] = b.ToString().ToLower();
        if (b)
            this.LoadClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void PontoVendaEnabled(bool b)
    {
        PontoVenda_txtNome.Enabled = b;
        PontoVenda_txtMoradaSede.Enabled = b;
        PontoVenda_txtLocalidade.Enabled = b;
        PontoVenda_txtRamo.Enabled = b;
        PontoVenda_txtMatricula.Enabled = b;
        PontoVenda_txtMatriculaNum.Enabled = b;
        PontoVenda_txtCapital.Enabled = b;
        PontoVenda_txtContribuinte.Enabled = b;
        PontoVenda_txtDesignacao.Enabled = b;
        PontoVenda_txtMorada2.Enabled = b;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void CondicoesEnabled(bool b)
    {
        Condicoes_txtObjectivo.Enabled = b;
        Condicoes_txtDataInicio.Enabled = b;
        Condicoes_rb_DataFim.Enabled = b;
        Condicoes_rb_QuantiaFim.Enabled = b;
        Condicoes_txtQuandoAtingir.Enabled = b && Condicoes_rb_QuantiaFim.Checked;
        Condicoes_cbGamas.Enabled = b && Condicoes_rb_QuantiaFim.Checked;
        Condicoes_txtRenovacao.Enabled = b;
        Condicoes_txtDataFim.Enabled = b && Condicoes_rb_DataFim.Checked;
        Condicoes_cbExploracao.Enabled = b;
        Condicoes_rbFrio.Enabled = b;
        this.Condicoes_dgEquipamentos.Attributes["CanUse"] = b.ToString();
        Condicoes_ckContratoFormal.Enabled = b;
        Condicoes_ckPooc.Enabled = b;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void ContrapartidasEnabled(bool b)
    {
        this.Table1.Visible = b || this.ContrapartidasGerais_Gamas_list.Items.Count > 0;
        this.ContrapartidasGerais_dlFaseamento_Empty.Visible = !this.Table1.Visible;
        this.Contrapartidas_dgContrapartidas.Attributes["CanUse"] = b.ToString();
        this.ContrapartidasGerais_cbGamas.Visible = b;
        this.ContrapartidasGerais_Gamas_list.Visible = !b;
        this.ContrapartidasGerais_ddlPrazo.Visible = b;
        this.C_lbl_Prazo.Visible = !b;
        this.ContrapartidasGerais_dgBonus.Attributes["CanUse"] = b.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void FaseamentoEnabled(bool b)
    {
        this.Faseamento_dlFaseamento.Attributes["CanUse"] = b.ToString();
        bool HasValue = this.Faseamento_lblVerba.Text != "VERBA TOTAL = 0 �";
        Faseamento_txtNIB.Enabled = HasValue && b;
        Faseamento_txtBANCO.Enabled = HasValue && b;
        Faseamento_txtDEPENDENCIA.Enabled = HasValue && b;
        Faseamento_txtComparticipacaoCompras.Enabled = b;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void MaterialEnabled(bool b)
    {
        Material_dlVisibilidade.Attributes["CanUse"] = b.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void ObservacoesEnabled(bool b)
    {
        Observacoes_txtObservacoes.ReadOnly = !b;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="b"></param>
    private void ObservacoesMotivoEnabled(bool b)
    {
        ObservacoesAdd.Visible = (b && (this.Contrato_Classif.Equals("N") || this.Contrato_Classif.Equals("A") || this.Contrato_Classif.Equals("S") || this.Contrato_Classif.Equals("C")));
    }
    #endregion

    #region CONTRATO
    private void InvalidateInvestimento()
    {
        this.investimentocell.BgColor = "#888888";
        this.investimentocell1.BgColor = "#888888";
        this.C_invest_Recalc.Visible = true;
    }
    private void loadInvestimento()
    {
        this.dtblInvestimento = new DataTable();
        this.dtblInvestimento.Columns.Add("GAMA");
        this.dtblInvestimento.Columns.Add("PREVISAO");
        this.dtblInvestimento.Columns.Add("DESCONTO");
        this.dtblInvestimento.Columns.Add("BONUS");
        this.dtblInvestimento.Columns["PREVISAO"].DataType = typeof(int);
        this.dtblInvestimento.Columns["DESCONTO"].DataType = typeof(decimal);
        this.dtblInvestimento.Columns["BONUS"].DataType = typeof(int);
        this.dtblInvestimento.DefaultView.Sort = "GAMA ASC";

        ContratoTime = 1;
        decimal TotalPrevisao = 0;
        decimal TotalPrevisaoGlobal = 0;
        decimal TotalDescontos = 0;
        decimal TotalBonus = 0;
        decimal TotalBonusGlobal = 0;
        decimal TotalVerba = 0;

        #region CALCULOS

        #region TEMPO CONTRATO
        if (this.Condicoes_rb_DataFim.Checked)
        {
            DateTime inicio = this.Condicoes_txtDataInicio.SelectedDate.Date;
            DateTime Fim = this.Condicoes_txtDataFim.SelectedDate.Date.AddDays(1);
            DateTime anos = this.Condicoes_txtDataInicio.SelectedDate;
            if (this.Condicoes_txtRenovacao.Text.Length > 0)
            {
                anos = anos.AddYears(int.Parse(this.Condicoes_txtRenovacao.Text)).AddDays(-1);
            }
            TimeSpan Data = Fim.Subtract(inicio);
            TimeSpan Anos = anos.Subtract(inicio);
            if (Data.TotalDays > Anos.TotalDays)
                ContratoTime = (decimal)((Data.TotalDays * 4) / 1461);
            else
                ContratoTime = (decimal)((Anos.TotalDays * 4) / 1461);
        }
        else if (this.Condicoes_rb_QuantiaFim.Checked)
        {
            foreach (ListItem c in this.Condicoes_cbGamas.Items)
            {
                if (c.Selected)
                {
                    DataRow[] gm = this.dtbl_Contrapartidas.Select("COD_GRUPO_PRODUTO = " + c.Value);
                    if (gm.Length > 0)
                    {
                        TotalPrevisao += decimal.Parse(gm[0]["PREVISAO_VENDAS"].ToString());
                    }
                }
            }
            decimal limite = decimal.Parse(this.Condicoes_txtQuandoAtingir.Text);
            ContratoTime = TotalPrevisao == 0 ? 1 : Math.Max(ContratoTime, limite / TotalPrevisao);
        }

        #endregion

        #region BONUS
        TotalPrevisao = 0;
        foreach (DataRowView dr in this.dtbl_Contrapartidas.DefaultView)
        {
            string gama = dr["PRODUCT_NAME"].ToString();

            decimal Previsao = decimal.Parse(dr["PREVISAO_VENDAS"].ToString());
            decimal PDesconto = 0;
            if (dr["DESCONTO_FACTURA"].ToString().Length > 0)
                PDesconto = decimal.Parse(dr["DESCONTO_FACTURA"].ToString());
            decimal Desconto = (decimal)Math.Round((Previsao * PDesconto) / 100, 0);
            decimal filtro = Previsao;
            switch (dr["PRAZO_PAGAMENTO"].ToString())
            {
                case "C":
                    filtro = (decimal)Math.Round(Previsao * ContratoTime, 0);
                    break;
                case "A":
                    break;
                case "S":
                    filtro = (decimal)Math.Round((decimal)Previsao / 2, 0);
                    break;
                case "Q":
                    filtro = (decimal)Math.Round((decimal)Previsao / 3, 0);
                    break;
                case "T":
                    filtro = (decimal)Math.Round((decimal)Previsao / 4, 0);
                    break;
                case "B":
                    filtro = (decimal)Math.Round((decimal)Previsao / 6, 0);
                    break;
                case "M":
                    filtro = (decimal)Math.Round((decimal)Previsao / 12, 0);
                    break;
            }
            DataRow[] bon = this.dtbl_Contrapartidas_Bonus.Select("ID_CONTRAPARTIDA = " + dr["ID_CONTRAPARTIDA"] + " AND TIPO = 'Acc' AND VALOR_LIMITE_MIN <= " + filtro, "VALOR_LIMITE_MIN DESC");
            decimal Bonus = 0;
            if (bon.Length > 0 && bon[0]["DESCONTO"].ToString().Length > 0)
            {
                Bonus = (decimal)Math.Round((Previsao * decimal.Parse(bon[0]["DESCONTO"].ToString())) / 100, 0);
            }

            TotalPrevisao += Previsao;
            TotalDescontos += Desconto;
            TotalBonus += Bonus;

            DataRow NewInvest = this.dtblInvestimento.NewRow();
            NewInvest["GAMA"] = gama;
            NewInvest["PREVISAO"] = Previsao;
            NewInvest["DESCONTO"] = Desconto;
            NewInvest["BONUS"] = Bonus;
            this.dtblInvestimento.Rows.Add(NewInvest);
        }
        #endregion

        #region BONUS GLOBAL
        if (this.ContrapartidasGerais_txtPrevisao.Text.Replace("�", "").Length > 0)
        {
            TotalPrevisaoGlobal = decimal.Parse(this.ContrapartidasGerais_txtPrevisao.Text.Replace("�", ""));
            decimal filtroglobal = TotalPrevisaoGlobal;
            switch (this.ContrapartidasGerais_ddlPrazo.SelectedValue)
            {
                case "C":
                    filtroglobal = (decimal)Math.Round(TotalPrevisaoGlobal * ContratoTime, 0);
                    break;
                case "A":
                    break;
                case "S":
                    filtroglobal = (decimal)Math.Round((decimal)TotalPrevisaoGlobal / 2, 0);
                    break;
                case "Q":
                    filtroglobal = (decimal)Math.Round((decimal)TotalPrevisaoGlobal / 3, 0);
                    break;
                case "T":
                    filtroglobal = (decimal)Math.Round((decimal)TotalPrevisaoGlobal / 4, 0);
                    break;
                case "B":
                    filtroglobal = (decimal)Math.Round((decimal)TotalPrevisaoGlobal / 6, 0);
                    break;
                case "M":
                    filtroglobal = (decimal)Math.Round((decimal)TotalPrevisaoGlobal / 12, 0);
                    break;
            }
            DataRow[] bonusGlobal = this.dtbl_ContrapartidasGerais_Bonus.Select("TIPO = 'Acc' AND VALOR_LIMITE_MIN <= " + filtroglobal, "VALOR_LIMITE_MIN DESC");
            if (bonusGlobal.Length > 0 && bonusGlobal[0]["DESCONTO"].ToString().Length > 0)
            {
                TotalBonusGlobal = (decimal)Math.Round((TotalPrevisaoGlobal * decimal.Parse(bonusGlobal[0]["DESCONTO"].ToString())) / 100, 0);
            }
        }
        #endregion

        #region VERBA
        foreach (DataRowView ver in this.dtbl_Faseamento.DefaultView)
        {
            decimal valor = decimal.Parse(ver["VALOR_PAGAMENTO"].ToString());
            TotalVerba += (decimal)Math.Round((decimal)valor / (ContratoTime < 1 ? 1 : ContratoTime), 0);
        }
        #endregion

        #region TOTAIS
        decimal Subt1 = TotalDescontos + TotalBonus;
        decimal Subt2 = Subt1 + TotalBonusGlobal;
        decimal Total = Subt2 + TotalVerba;

        decimal PercDescontos = TotalPrevisao == 0 ? 0 : Math.Round((decimal)(TotalDescontos * 100) / TotalPrevisao, 2);
        decimal PercSubt1 = TotalPrevisao == 0 ? 0 : Math.Round((decimal)(Subt1 * 100) / TotalPrevisao, 2);
        decimal PercSubt2 = TotalPrevisao == 0 ? 0 : Math.Round((decimal)(Subt2 * 100) / TotalPrevisao, 2);
        decimal percTotal = TotalPrevisao == 0 ? 0 : Math.Round((decimal)(Total * 100) / TotalPrevisao, 2);
        #endregion
        #endregion

        #region FILL INTERFACE
        this.C_dg_Investimento.DataSource = this.dtblInvestimento.DefaultView;
        this.C_dg_Investimento.DataBind();

        this.C_lbl_Invest_Tempo.Text = Math.Round(ContratoTime, 2).ToString() + " ANOS";
        this.C_lbl_Invest_Data.Text = this.EndDate.ToString("dd-MM-yyy");
        this.C_lbl_Invest_Previsao.Text = TotalPrevisao.ToString() + " �";
        this.C_lbl_Invest_Descontos.Text = TotalDescontos.ToString() + " �";
        this.C_lbl_Invest_Descontos_Perc.Text = PercDescontos.ToString() + " %";
        this.C_lbl_Invest_Bonus.Text = TotalBonus.ToString() + " �";
        this.C_lbl_Invest_Subt1.Text = Subt1.ToString() + " �";
        this.C_lbl_Invest_Subt1_Perc.Text = PercSubt1.ToString() + " %";
        this.C_lbl_Invest_BonusGlobal.Text = TotalBonusGlobal.ToString() + " �";
        this.C_lbl_Invest_Subt2.Text = Subt2.ToString() + " �";
        this.C_lbl_Invest_Subt2_Perc.Text = PercSubt2.ToString() + " %";
        this.C_lbl_Invest_Verba.Text = TotalVerba.ToString() + " �";
        this.C_lbl_Invest_Total.Text = Total.ToString() + " �";
        this.C_lbl_Invest_Total_perc.Text = percTotal.ToString() + " %";
        #endregion

        this.investimentocell.BgColor = "";
        this.investimentocell1.BgColor = "";
        this.C_invest_Recalc.Visible = false;
        this.loadFaseamento();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idContrato"></param>
    /// <returns></returns>
    private bool LoadContrato(int idContrato)
    {
        bool retVal = false;
        try
        {
            #region LER TODO O CONTRATO
            SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
            DataSet Contrato = DBsra.getContrato(idContrato);
            DataRow Ctrl = Contrato.Tables["CONTRATO"].Rows[0];
            #endregion

            #region  PREENCHER OS CONTROLOS COM OS DADOS DO CONTRATO
            #region PROCESSO
            this.C_txt_Processo_Actual.Text = Ctrl["ID_PROCESSO"].ToString();
            this.Contrato_Classif = Ctrl["CLASSIFICACAO_CONTRATO"].ToString();
            this.Contrato_ddlTipo.SelectedValue = Ctrl["TIPO_CONTRATO"].ToString();
            this.Contrato_Status = Ctrl["STATUS"].ToString();
            this.C_Contrato_lbl_Data.Text = ((DateTime)Ctrl["DATA_CONTRATO"]).ToString("dd-MM-yyyy");
            //this.Contrato_txtProcessoEstado.ToolTip = "INSERIDO EM: " + DateTime.Parse(Ctrl["INSERT_DATE"].ToString()/*,new CultureInfo("pt-PT", true)*/).ToString("dd-MM-yyyy") + "  POR:" + Ctrl["INSERT_USER"].ToString()
            //    + (Ctrl["ALTER_DATE"].ToString() != "" ? "  ALTERADO EM: " + DateTime.Parse(Ctrl["ALTER_DATE"].ToString()/*,new CultureInfo("pt-PT", true)*/).ToString("dd-MM-yyyy") + "  POR:" + Ctrl["ALTER_USER"].ToString() : "");
            this.NumeroAnterior = Ctrl["NUM_PROCESSO_ANTERIOR"].ToString();
            this.Contrato_txtProcessoSeguinte.Text = Ctrl["NUM_PROCESSO_SEGUINTE"].ToString();
            if (this.DL_CONCESSIONARIO.Items.FindByValue(Ctrl["DEFAULT_COD_CONC"].ToString()) == null)
            {
                DataTable zonas = DBsra.getZonas();
                DataRow[] rows = zonas.Select("COD_ZONA = " + Ctrl["DEFAULT_COD_CONC"].ToString());
                this.DL_CONCESSIONARIO.Items.Insert(1, new ListItem(rows[0]["DES_ZONA"].ToString(), rows[0]["COD_ZONA"].ToString()));
            }
            this.DL_CONCESSIONARIO.SelectedValue = Ctrl["DEFAULT_COD_CONC"].ToString();
            if (Ctrl["DATA_FIM_EFECTIVO"].ToString() != "")
            {
                this.Contrato_txtDataFimEfectivo.Text = DateTime.Parse(Ctrl["DATA_FIM_EFECTIVO"].ToString()).ToString("dd-MM-yyyy");
            }
            else
            {
                this.Contrato_txtDataFimEfectivo.Text = "";
            }
            Session["uid"] = Ctrl["INSTANCE_ID"].ToString();
            Session["instanceState"] = Ctrl["INSTANCE_STATE"].ToString();

            #endregion

            #region NIVEL
            this.C_cb_Nivel_LE.Checked = Ctrl["NIVEL_CONTRATO"].ToString() == "LE";
            this.C_cb_Nivel_SGE.Checked = Ctrl["NIVEL_CONTRATO"].ToString() == "SGE";
            this.C_cb_Nivel_GE.Checked = Ctrl["NIVEL_CONTRATO"].ToString() == "GE";
            this.C_ddl_GrupoEconomico.SelectedValue = Ctrl["COD_GRUPO_ECONOMICO"].ToString();
            this.C_ddl_GrupoEconomico_Changed(this.C_ddl_GrupoEconomico, new EventArgs());
            this.C_ddl_SubGrupoEconomico.SelectedValue = Ctrl["COD_SUBGRUPO_ECONOMICO"].ToString();
            #endregion

            #region PONTO DE VENDA
            this.PontoVenda_txtNome.Text = Ctrl["NOME_FIRMA"].ToString();
            this.PontoVenda_txtMoradaSede.Text = Ctrl["MORADA_SEDE"].ToString();
            this.PontoVenda_txtLocalidade.Text = Ctrl["LOCALIDADE"].ToString();
            this.PontoVenda_txtRamo.Text = Ctrl["RAMO_ACTIVIDADE"].ToString();
            this.PontoVenda_txtMatricula.Text = Ctrl["MATRICULA"].ToString();
            this.PontoVenda_txtMatriculaNum.Text = Ctrl["MATRICULA_NUM"].ToString();
            this.PontoVenda_txtCapital.Text = Ctrl["CAPITAL_SOCIAL"].ToString();
            this.PontoVenda_txtContribuinte.Text = Ctrl["NIF"].ToString();
            this.PontoVenda_txtDesignacao.Text = Ctrl["DESIGNACAO_PT_VENDA"].ToString();
            this.PontoVenda_txtMorada2.Text = Ctrl["MORADA_PT_VENDA"].ToString();
            #endregion

            #region CONDI��ES
            this.Condicoes_txtObjectivo.Text = Ctrl["OBJECTIVO"].ToString();
            this.Condicoes_txtDataInicio.SelectedDate = DateTime.Parse(Ctrl["DATA_INICIO"].ToString()/*,new CultureInfo("pt-PT", true)*/);
            this.Condicoes_txtDataFim.MinEnabledDate = this.Condicoes_txtDataInicio.SelectedDate;
            this.Botoes_txtDataEfectiva.MinEnabledDate = this.Condicoes_txtDataInicio.SelectedDate;
            this.Botoes_txtDataEfectiva.SelectedDate = this.Condicoes_txtDataInicio.SelectedDate;
            this.Condicoes_txtQuandoAtingir.Text = Ctrl["QUANTIA_FIM"].ToString();
            this.Condicoes_txtRenovacao.Text = Ctrl["ANOS_RENOVACAO"].ToString();
            this.Condicoes_ckContratoFormal.Checked = Ctrl["IS_CONTRATO_FORMAL"].Equals("T");
            this.Condicoes_ckPooc.Checked = Ctrl["IS_POOC"].Equals("T");
            this.Condicoes_rbFrio.SelectedValue = Ctrl["IS_FORN_FRIO_IGLO"].ToString();
            if (this.Condicoes_txtQuandoAtingir.Text.Equals(""))
                this.Condicoes_rb_DataFim.Checked = true;
            else
                this.Condicoes_rb_QuantiaFim.Checked = true;
            if (Ctrl["DATA_FIM"] != DBNull.Value)
                this.Condicoes_txtDataFim.SelectedDate = DateTime.Parse(Ctrl["DATA_FIM"].ToString()/*,new CultureInfo("pt-PT", true)*/);
            else
                this.Condicoes_txtDataFim.Clear();
            this.Session["GAMAS_QUANTIA_FIM"] = Contrato.Tables["GAMAS_QUANTIA_FIM"];
            this.Session["EXPLORACAO"] = Contrato.Tables["EXPLORACAO"];
            #endregion

            #region CONTRAPARTIDAS
            this.Session["CONTRAPARTIDAS_GERAIS_GAMAS"] = Contrato.Tables["GAMAS_GLOBAL"];
            this.ContrapartidasGerais_txtPrevisao.Text = Ctrl["PREVISAO_VENDAS_GLOBAL"].ToString() + " �";
            this.ContrapartidasGerais_ddlPrazo.SelectedValue = Ctrl["PRAZO_PAGAMENTO"].ToString();
            this.C_lbl_Prazo.Text = this.ContrapartidasGerais_ddlPrazo.SelectedItem.Text;
            #endregion

            #region FASEAMENTO
            this.Faseamento_txtComparticipacaoCompras.Text = Ctrl["COMP_CONC_COMPRAS"].ToString();
            this.Faseamento_txtNIB.Text = Ctrl["NIB_CLIENTE"].ToString();
            this.Faseamento_txtBANCO.Text = Ctrl["BANCO"].ToString();
            this.Faseamento_txtDEPENDENCIA.Text = Ctrl["BANCO_DEPENDENCIA"].ToString();
            this.Observacoes_txtObservacoes.Text = Ctrl["OBSERVACOES"].ToString();
            this.Observacoes_txtObservacoes_Motivo.Text = Ctrl["OBSERVACOES_MOTIVO"].ToString();
            #endregion
            #endregion

            #region LER TABELAS COMPLEMENTARES
            this.dtblClientesLEs = Contrato.Tables["ENTIDADE"];
            this.dtbl_Condicoes_Equipamento = Contrato.Tables["CONT_EQUIPAMENTO"];
            this.dtbl_Contrapartidas = Contrato.Tables["CONTRAPARTIDAS"];
            this.dtbl_Contrapartidas_Bonus = Contrato.Tables["BONUS"];
            this.dtbl_ContrapartidasGerais_Bonus = Contrato.Tables["BONUS_GLOBAL"];
            this.dtbl_Faseamento = Contrato.Tables["FASEAMENTO_PAG"];
            this.dtbl_Material = Contrato.Tables["CONT_MAT_VISIBILIDADE"];
            #endregion

            retVal = true;
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL LER OS DADOS DO CONTRATO", exp);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO", exp);
        }
        return retVal;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string getConcessionarioCode()
    {
        string Code = "999";
        if (this.dtblClientesLEs != null && this.dtblClientesLEs.DefaultView.Count > 0)
        {
            string i = this.dtblClientesLEs.DefaultView[0]["COD_CONCESSIONARIO"].ToString();
            if (this.dtblClientesLEs.DefaultView.Count == this.dtblClientesLEs.Select("COD_CONCESSIONARIO = " + i).Length)
            {
                Code = i.PadLeft(3, '0');
            }
        }
        return Code;
    }
    /// <summary>
    /// 
    /// </summary>
    private void loadStatus()
    {
        switch (this.Contrato_Status)
        {
            case "I":
                this.Contrato_txtProcessoEstado.Text = is_DBContrato ? "INSERIDO" : "???";
                this.boxContrato.CssClass = "box INSERTED_BACK";
                break;
            case "V":
                this.Contrato_txtProcessoEstado.Text = "VALIDADO";
                this.boxContrato.CssClass = "box VALID_BACK";
                break;
            case "P":
                this.Contrato_txtProcessoEstado.Text = "IMPORTADO";
                this.boxContrato.CssClass = "box IMPORTED_BACK";
                break;
            case "T":
                this.Contrato_txtProcessoEstado.Text = "TERMINADO";
                this.boxContrato.CssClass = "box TERMINATED_BACK";
                break;
            default:
                this.Contrato_txtProcessoEstado.Text = "??? (n�o atribuido)";
                this.boxContrato.CssClass = "box";
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void loadTipo()
    {
        switch (this.Contrato_Classif)
        {
            case "N":
                this.Contrato_txtProcessoClassif.Text = "ORIGINAL";
                break;
            case "A":
                this.Contrato_txtProcessoClassif.Text = "ALTERA��O";
                break;
            case "S":
                this.Contrato_txtProcessoClassif.Text = "SUBSTITUI��O (Continua��o)";
                break;
            case "C":
                this.Contrato_txtProcessoClassif.Text = "SUBSTITUI��O (Cancelamento)";
                break;
            default:
                this.Contrato_txtProcessoClassif.Text = "???(n�o atribuido)";
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void loadNumAnterior()
    {
        this.Contrato_txtProcessoAnterior.Text = this.NumeroAnterior;
        SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
        if (this.NumeroAnterior != "")
        {
            DataRow dr = DBsra.getContratoByProcess(this.NumeroAnterior);
            if (dr != null)
            {
                this.Contrato_txtProcessoAnterior.NavigateUrl = "~/contrato.aspx?idContrato=" + dr["ID_CONTRATO"];
                if (dr["DATA_FIM_EFECTIVO"] != DBNull.Value)
                {
                    this.Condicoes_txtDataInicio.MinEnabledDate = DateTime.Parse(dr["DATA_FIM_EFECTIVO"].ToString()).AddDays(1);
                }
                switch (dr["STATUS"].ToString())
                {
                    case "I":
                        this.Contrato_txtProcessoAnterior.CssClass = "INSERTED_FRONT";
                        break;
                    case "V":
                        this.Contrato_txtProcessoAnterior.CssClass = "VALID_FRONT";
                        break;
                    case "P":
                        this.Contrato_txtProcessoAnterior.CssClass = "IMPORTED_FRONT";
                        break;
                    case "T":
                        this.Contrato_txtProcessoAnterior.CssClass = "TERMINATED_FRONT";
                        break;
                    default:
                        this.Contrato_txtProcessoAnterior.CssClass = "";
                        break;
                }
            }
            else
                this.Contrato_txtProcessoAnterior.NavigateUrl = "#";
        }
        if (is_DBContrato)
        {
            int drn = DBsra.getPreviousContrato(Convert.ToInt32(this.Request["idContrato"]));
            if (drn > 0)
                this.C_btn_Process_Previous.OnClientClick = "OpenUrl('" + "/contrato.aspx?idContrato=" + drn + "');";
            else
                this.C_btn_Process_Previous.OnClientClick = "";
        }
        //this.Contrato_lblProcessoAnterior.Visible = this.NumeroAnterior != "";
        this.C_btn_Process_Previous.Visible = this.C_btn_Process_Previous.OnClientClick != "";
    }
    /// <summary>
    /// 
    /// </summary>
    private void loadNumSeguinte()
    {
        SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);

        if (this.Contrato_txtProcessoSeguinte.Text != "")
        {
            DataRow dr = DBsra.getContratoByProcess(this.Contrato_txtProcessoSeguinte.Text);
            if (dr != null)
            {
                this.Contrato_txtProcessoSeguinte.NavigateUrl = "~/contrato.aspx?idContrato=" + dr["ID_CONTRATO"];
                switch (dr["STATUS"].ToString())
                {
                    case "I":
                        this.Contrato_txtProcessoSeguinte.CssClass = "INSERTED_FRONT";
                        break;
                    case "V":
                        this.Contrato_txtProcessoSeguinte.CssClass = "VALID_FRONT";
                        break;
                    case "P":
                        this.Contrato_txtProcessoSeguinte.CssClass = "IMPORTED_FRONT";
                        break;
                    case "T":
                        this.Contrato_txtProcessoSeguinte.CssClass = "TERMINATED_FRONT";
                        break;
                    default:
                        this.Contrato_txtProcessoSeguinte.CssClass = "";
                        break;
                }
            }
            else
                this.Contrato_txtProcessoSeguinte.NavigateUrl = "#";
        }
        if (is_DBContrato)
        {
            int drn = DBsra.getNextContrato(Convert.ToInt32(this.Request["idContrato"]));
            if (drn > 0)
                this.C_btn_Process_Next.OnClientClick = "OpenUrl('" + "/contrato.aspx?idContrato=" + drn + "');";
            else
                this.C_btn_Process_Next.OnClientClick = "";
        }
        //this.Contrato_lblProcessoSeguinte.Visible = this.Contrato_txtProcessoSeguinte.Text != "";
        this.C_btn_Process_Next.Visible = this.C_btn_Process_Next.OnClientClick != "";
    }

    /// <summary>
    /// 
    /// </summary>
    private void loadFimEfectivo()
    {
        this.Contrato_lblDataFimEfectivo.Visible = this.Contrato_txtDataFimEfectivo.Text != "";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoAnterior"></param>
    /// <param name="Classif"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private string InserirContrato(ref int newId, string ContratoAnterior, string Classif, string Status)
    {
        SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);

        #region LER AS GAMAS SELECCIONADAS
        DataTable Gamas = new DataTable();
        Gamas.Columns.Add("COD_GRUPO_PRODUTO");
        foreach (ListItem c in this.Condicoes_cbGamas.Items)
        {
            if (c.Selected)
                Gamas.Rows.Add(new object[] { c.Value });
        }
        #endregion

        #region LER AS EXPLORA��ES SELECCIONADAS
        DataTable Exploracao = new DataTable();
        Exploracao.Columns.Add("COD_EXPLORACAO");
        foreach (ListItem c in this.Condicoes_cbExploracao.Items)
        {
            if (c.Selected)
                Exploracao.Rows.Add(new object[] { c.Value });
        }
        #endregion

        #region LER AS GAMAS SELECCIONADAS NAS CONTRAPARTIDAS
        DataTable ContrapartidasGamas = new DataTable();
        ContrapartidasGamas.Columns.Add("COD_GRUPO_PRODUTO");
        foreach (ListItem c in this.ContrapartidasGerais_cbGamas.Items)
        {
            if (c.Selected)
                ContrapartidasGamas.Rows.Add(new object[] { c.Value });
        }
        #endregion

        #region INSERIR O CONTRATO
        this.Observacoes_txtObservacoes_Motivo.Text = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> GUARDADO" + (Status == "V" ? " E VALIDADO" : "") + "\n" + this.Observacoes_txtObservacoes_Motivo.Text;

        ///TODO: 10 - CRIAR CODIGO PARA VALIDAR O INSERT DO CONTRATO
        ///SE O CONTRATO FALHOU APAGAR A INSTANCIA DO WORKFLOW

        string numContrato = DBsra.SaveContrato(
            ref newId,
            this.getConcessionarioCode(),
            this.C_txt_Processo_Actual.Text.Replace(" ", ""),
            ContratoAnterior,
            "",
            Classif,
            this.Contrato_ddlTipo.SelectedValue,
            DateTime.Parse(this.C_Contrato_lbl_Data.Text),
            this.C_cb_Nivel_LE.Checked ? "LE" : this.C_cb_Nivel_SGE.Checked ? "SGE" : "GE",
            this.C_ddl_GrupoEconomico.SelectedValue,
            this.C_ddl_SubGrupoEconomico.SelectedValue,
            Status,
            this.Observacoes_txtObservacoes.Text,
            this.Observacoes_txtObservacoes_Motivo.Text,
            this.PontoVenda_txtNome.Text,
            this.PontoVenda_txtMoradaSede.Text,
            this.PontoVenda_txtLocalidade.Text,
            this.PontoVenda_txtRamo.Text,
            this.PontoVenda_txtMatricula.Text,
            this.PontoVenda_txtMatriculaNum.Text,
            this.PontoVenda_txtCapital.Text,
            this.PontoVenda_txtContribuinte.Text,
            this.PontoVenda_txtDesignacao.Text,
            this.PontoVenda_txtMorada2.Text,
            this.Condicoes_txtObjectivo.Text,
            this.Condicoes_txtDataInicio.SelectedDate,
            this.Condicoes_rb_DataFim.Checked ? this.Condicoes_txtDataFim.SelectedDate : DateTime.MinValue,
            this.Condicoes_rb_QuantiaFim.Checked ? this.Condicoes_txtQuandoAtingir.Text : "",
            this.Condicoes_txtRenovacao.Text,
            this.Condicoes_ckContratoFormal.Checked == true ? "T" : "F",
            this.Condicoes_ckPooc.Checked == true ? "T" : "F",
            this.Condicoes_rbFrio.SelectedValue,
            this.ContrapartidasGerais_txtPrevisao.Text.Replace(" �", ""),
            this.ContrapartidasGerais_ddlPrazo.SelectedValue,
            ContrapartidasGamas,
            this.Faseamento_txtNIB.Text,
            this.Faseamento_txtBANCO.Text,
            this.Faseamento_txtDEPENDENCIA.Text,
            this.Faseamento_txtComparticipacaoCompras.Text,
            User.Identity.Name,
            this.dtblClientesLEs,
            Gamas,
            Exploracao,
            this.dtbl_Condicoes_Equipamento,
            this.dtbl_Contrapartidas,
            this.dtbl_Contrapartidas_Bonus,
            this.dtbl_ContrapartidasGerais_Bonus,
            this.dtbl_Faseamento,
            this.dtbl_Material,
            Guid.Empty,
            "",
            "F",
            this.DL_CONCESSIONARIO.SelectedValue,
            "F",
            "T",
            "F"
            );

        Guid uid = this.RunWorkflow(newId);
        //DBsra.UpdateContratoWF_ID(newId, uid);
        #endregion

        return numContrato;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ContratoAnterior"></param>
    /// <param name="Classif"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private string ActualizarContrato(ref int newId, string ContratoAnterior, string Classif, string Status)
    {
        SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);

        #region LER AS GAMAS SELECCIONADAS
        DataTable Gamas = new DataTable();
        Gamas.Columns.Add("COD_GRUPO_PRODUTO");
        foreach (ListItem c in this.Condicoes_cbGamas.Items)
        {
            if (c.Selected)
                Gamas.Rows.Add(new object[] { c.Value });
        }
        #endregion

        #region LER AS EXPLORA��ES SELECCIONADAS
        DataTable Exploracao = new DataTable();
        Exploracao.Columns.Add("COD_EXPLORACAO");
        foreach (ListItem c in this.Condicoes_cbExploracao.Items)
        {
            if (c.Selected)
                Exploracao.Rows.Add(new object[] { c.Value });
        }
        #endregion

        #region LER AS GAMAS SELECCIONADAS NAS CONTRAPARTIDAS
        DataTable ContrapartidasGamas = new DataTable();
        ContrapartidasGamas.Columns.Add("COD_GRUPO_PRODUTO");
        ContrapartidasGamas.Columns.Add("PRAZO_PAGAMENTO");
        foreach (ListItem c in this.ContrapartidasGerais_cbGamas.Items)
        {
            if (c.Selected)
                ContrapartidasGamas.Rows.Add(new object[] { c.Value, 'A' });
        }
        #endregion

        #region ACTUALIZAR CONTRATO
        int idcontrato = int.Parse(this.Request["idContrato"]);
        this.Observacoes_txtObservacoes_Motivo.Text = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> GUARDADO" + (Status == "V" ? " E VALIDADO" : "") + "\n" + this.Observacoes_txtObservacoes_Motivo.Text;



        DataTable contratoFromDB = DBsra.getContrato("ID_CONTRATO = " + idcontrato.ToString());
        //CHANGED BY RRAFAEL 08/02/2011
        // ORIGINAL: Guid wfID = new Guid(contratoFromDB.Rows[0]["INSTANCE_ID"].ToString());
        Guid? wfID = null;
        if(!contratoFromDB.Rows[0]["INSTANCE_ID"].ToString().Equals(String.Empty))
            wfID = new Guid(contratoFromDB.Rows[0]["INSTANCE_ID"].ToString());
        // END CHANGES
        string wfState = contratoFromDB.Rows[0]["INSTANCE_STATE"].ToString();
        string isAut = contratoFromDB.Rows[0]["IS_AUTHORIZED"].ToString();
        string issent = contratoFromDB.Rows[0]["IS_SENT_TO_RENEWAL"].ToString();

        string numContrato = DBsra.UpdateContrato(
            ref idcontrato,
            this.getConcessionarioCode(),
            this.C_txt_Processo_Actual.Text.Replace(" ", ""),
            ContratoAnterior,
            "",
            Classif,
            this.Contrato_ddlTipo.SelectedValue,
            DateTime.Parse(this.C_Contrato_lbl_Data.Text),
            this.C_cb_Nivel_LE.Checked ? "LE" : this.C_cb_Nivel_SGE.Checked ? "SGE" : "GE",
            this.C_ddl_GrupoEconomico.SelectedValue,
            this.C_ddl_SubGrupoEconomico.SelectedValue,
            Status,
            this.Observacoes_txtObservacoes.Text,
            this.Observacoes_txtObservacoes_Motivo.Text,
            this.PontoVenda_txtNome.Text,
            this.PontoVenda_txtMoradaSede.Text,
            this.PontoVenda_txtLocalidade.Text,
            this.PontoVenda_txtRamo.Text,
            this.PontoVenda_txtMatricula.Text,
            this.PontoVenda_txtMatriculaNum.Text,
            this.PontoVenda_txtCapital.Text,
            this.PontoVenda_txtContribuinte.Text,
            this.PontoVenda_txtDesignacao.Text,
            this.PontoVenda_txtMorada2.Text,
            this.Condicoes_txtObjectivo.Text,
            this.Condicoes_txtDataInicio.SelectedDate,
            this.Condicoes_rb_DataFim.Checked ? this.Condicoes_txtDataFim.SelectedDate : DateTime.MinValue,
            this.Condicoes_rb_QuantiaFim.Checked ? this.Condicoes_txtQuandoAtingir.Text : "",
            this.Condicoes_txtRenovacao.Text,
            this.Condicoes_ckContratoFormal.Checked == true ? "T" : "F",
            this.Condicoes_ckPooc.Checked == true ? "T" : "F",
            this.Condicoes_rbFrio.SelectedValue,
            this.ContrapartidasGerais_txtPrevisao.Text.Replace(" �", ""),
            this.ContrapartidasGerais_ddlPrazo.SelectedValue,
            ContrapartidasGamas,
            this.Faseamento_txtNIB.Text,
            this.Faseamento_txtBANCO.Text,
            this.Faseamento_txtDEPENDENCIA.Text,
            this.Faseamento_txtComparticipacaoCompras.Text,
            User.Identity.Name,
            this.dtblClientesLEs,
            Gamas,
            Exploracao,
            this.dtbl_Condicoes_Equipamento,
            this.dtbl_Contrapartidas,
            this.dtbl_Contrapartidas_Bonus,
            this.dtbl_ContrapartidasGerais_Bonus,
            this.dtbl_Faseamento,
            this.dtbl_Material,
            wfID,
            wfState,
            isAut,
            this.DL_CONCESSIONARIO.SelectedValue,
            issent,
            "T",
            "F"
            );
        newId = idcontrato;
        #endregion

        return numContrato;
    }
    #endregion

    #region PESQUISA
    /// <summary>
    /// 
    /// </summary>
    private void LoadPesquisa()
    {
        bool hasdata = (this.dtblClientesLEs != null && this.dtblClientesLEs.DefaultView.Count > 0);
        Clientes DBClientes = new Clientes(ConfigurationManager.AppSettings["DBCS"]);
        string[] SearchHeaders = ConfigurationManager.AppSettings["ClientSearchColumns"].Replace(" ", "").Split(new char[] { ',' });
        string[] ResultHeaders = ConfigurationManager.AppSettings["ClientResultColumns"].Replace(" ", "").Split(new char[] { ',' });
        this.C_Entidade_Pesquisa.InputDataObject = DBClientes;
        this.C_Entidade_Pesquisa.SearchHeaders = SearchHeaders;
        this.C_Entidade_Pesquisa.ResultHeaders = ResultHeaders;
        this.C_Entidade_Pesquisa.ShowSearchResult = hasdata;
        this.C_Entidade_Pesquisa.ADVModeEnabled = hasdata;
        this.C_Entidade_Pesquisa.Opened = !hasdata;
        this.C_Entidade_Pesquisa.Limpar();
    }
    #endregion

    #region CLIENTE
    /// <summary>
    /// 
    /// </summary>
    private void LoadClientes()
    {
        #region LOAD DATA

        if (this.dtblClientesLEs == null)
        {
            this.dtblClientesLEs = new DataTable();
        }
        if (this.C_dg_Entidades.CurrentPageIndex > 0 && ((float)this.dtblClientesLEs.DefaultView.Count / (float)this.C_dg_Entidades.PageSize) <= ((float)this.C_dg_Entidades.CurrentPageIndex))
        {
            --this.C_dg_Entidades.CurrentPageIndex;
        }
        bool isEditable = this.C_dg_Entidades.Attributes["CanUse"] == "true" && this.C_cb_Nivel_LE.Checked;
        this.C_dg_Entidades.Columns[0].Visible = isEditable;
        this.C_dg_Entidades.Columns[1].Visible = this.C_cb_Nivel_LE.Checked;
        this.C_dg_Entidades.Columns[2].Visible = this.C_cb_Nivel_LE.Checked;
        this.C_dg_Entidades.DataSource = this.dtblClientesLEs.DefaultView;
        this.C_dg_Entidades.DataBind();
        #endregion

        #region VALIDATE DATA
        this.ValidateClientes();
        #endregion

        #region UPDATE INTERFACE
        string Text = "";
        if (this.dtblClientesLEs.DefaultView.Count > 0)
        {
            if (this.C_dg_Entidades.AllowPaging)
            {
                Text = ((this.C_dg_Entidades.CurrentPageIndex) * this.C_dg_Entidades.PageSize + 1)
                       + " a " + Math.Min(this.dtblClientesLEs.DefaultView.Count, (this.C_dg_Entidades.CurrentPageIndex + 1) * this.C_dg_Entidades.PageSize)
                       + " de " + this.dtblClientesLEs.DefaultView.Count
                       + " Clientes/Locais de Entrega";
            }
            else
            {
                Text = "1 a " + this.dtblClientesLEs.DefaultView.Count
                     + " de " + this.dtblClientesLEs.DefaultView.Count
                     + " Clientes/Locais de Entrega";
            }
        }
        else
        {
            Text = "N�o existem Clientes/Locais de Entrega";
        }
        this.C_lbl_ClientesNum.Text = Text;
        this.C_Entidades_btn_Paginacao.Visible = this.dtblClientesLEs != null
                                              && this.dtblClientesLEs.DefaultView.Count > this.C_dg_Entidades.PageSize;
        if (this.C_dg_Entidades.AllowPaging == false)
        {
            this.C_Entidades_btn_Paginacao.Text = "Ver Lista Paginada";
            this.C_Entidades_btn_Paginacao.ToolTip = "Apresentar a lista com pagina��o";
            this.C_Entidades_btn_Paginacao.ImageUrl = "~/images/Page.ico";
        }
        else
        {
            this.C_Entidades_btn_Paginacao.Text = "Ver Lista Completa";
            this.C_Entidades_btn_Paginacao.ToolTip = "Apresentar a lista sem pagina��o";
            this.C_Entidades_btn_Paginacao.ImageUrl = "~/images/List.ico";
        }
        #endregion
    }
    /// <summary>
    /// 
    /// </summary>
    protected void RefreshClientes()
    {
        try
        {
            if (this.C_cb_Nivel_GE.Checked)
            {
                SRA DB = new SRA(ConfigurationManager.AppSettings["DBCS"]);
                this.dtblClientesLEs = DB.ClientesGet("COD_GRUPO_ECONOMICO_LE = '" + this.C_ddl_GrupoEconomico.SelectedValue + "'");
            }
            else if (this.C_cb_Nivel_SGE.Checked)
            {
                SRA DB = new SRA(ConfigurationManager.AppSettings["DBCS"]);
                this.dtblClientesLEs = DB.ClientesGet("COD_GRUPO_ECONOMICO_LE = '" + this.C_ddl_GrupoEconomico.SelectedValue + "'"
                                                 + " AND COD_SUBGRUPO_ECONOMICO_LE = '" + this.C_ddl_SubGrupoEconomico.SelectedValue + "'");
            }
            this.LoadClientes();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ACTUALIZAR A LISTA DE LOCAIS DE ENTREGA.", exp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteCliente(int idx)
    {
        this.dtblClientesLEs.DefaultView[idx].Row.Delete();
        this.LoadClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void SelectCliente(int idx)
    {
        DataRowView dt = this.dtblClientesLEs.DefaultView[idx];
        this.PontoVenda_txtNome.Text = dt["DES_CLIENTE"].ToString();
        this.PontoVenda_txtMoradaSede.Text = dt["MORADA_CLIENTE"].ToString();
        this.PontoVenda_txtLocalidade.Text = dt["DES_LOCALIDADE_CLI"].ToString();
        this.PontoVenda_txtRamo.Text = dt["DES_RAMO_ACTIVIDADE"].ToString();
        this.PontoVenda_txtContribuinte.Text = dt["NUM_CONTRIBUINTE"].ToString();
        this.PontoVenda_txtDesignacao.Text = dt["DESIGNACAO_COMERCIAL"].ToString();
        this.PontoVenda_txtMorada2.Text = dt["MORADA"].ToString();
        Page.Validate("PontoVenda");
    }
    /// <summary>
    /// 
    /// </summary>
    private void ValidateClientes()
    {
        this.C_Nivel_lbl_ValGE.Text = "";
        this.C_Nivel_lbl_ValSGE.Text = "";
        if (this.dtblClientesLEs.DefaultView.Count > 0)
        {
            string GE = this.C_ddl_GrupoEconomico.SelectedValue;
            string SGE = this.C_ddl_SubGrupoEconomico.SelectedValue;

            int GEs = this.dtblClientesLEs.Select("COD_GRUPO_ECONOMICO_LE = '" + GE + "'", "", DataViewRowState.CurrentRows).Length;
            int SGEs = this.dtblClientesLEs.Select("COD_SUBGRUPO_ECONOMICO_LE = '" + SGE + "'", "", DataViewRowState.CurrentRows).Length;
            if (GE != "")
            {
                if (GEs == 0)
                {
                    this.C_Nivel_lbl_ValGE.Text = "N�o existem LE's do Grupo Econ�mico seleccionado.";
                }
                else if (GEs < this.dtblClientesLEs.DefaultView.Count)
                {
                    this.C_Nivel_lbl_ValGE.Text = "Existem LE's que n�o pertencem ao Grupo Econ�mico seleccionado.";
                }
            }
            if (SGE != "0" && SGE != "")
            {
                if (SGEs == 0)
                {
                    this.C_Nivel_lbl_ValSGE.Text = "N�o existem LE's do Sub-Grupo Econ�mico seleccionado.";
                }
                else if (SGEs < this.dtblClientesLEs.DefaultView.Count)
                {
                    this.C_Nivel_lbl_ValSGE.Text = "Existem LE's que n�o pertencem ao Sub-Grupo Econ�mico seleccionado.";
                }
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected void UpdateCliente(DataGridItem e)
    {
        try
        {
            DatePicker C_Entidades_Data_Inicio = (DatePicker)e.FindControl("C_Entidades_Data_Inicio");
            DatePicker C_Entidades_Data_Fim = (DatePicker)e.FindControl("C_Entidades_Data_Fim");
            DataRow dr = dtblClientesLEs.DefaultView[e.ItemIndex].Row;
            if (C_Entidades_Data_Inicio.SelectedDate == DateTime.MinValue)
                dr["DATA_INICIO"] = DBNull.Value;
            else
                dr["DATA_INICIO"] = C_Entidades_Data_Inicio.SelectedDate.Date;

            if (C_Entidades_Data_Fim.SelectedDate == DateTime.MinValue)
                dr["DATA_FIM"] = DBNull.Value;
            else
                dr["DATA_FIM"] = C_Entidades_Data_Fim.SelectedDate.Date;

            dr.AcceptChanges();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ACTUALIZAR O LOCAL DE ENTREGA.", exp, true);
        }
    }
    #endregion

    #region PONTO VENDA
    #endregion

    #region CONDI��ES
    /// <summary>
    /// 
    /// </summary>
    private void loadCondicoes()
    {
        this.loadCondicoesEquipamento();
        if (!this.Condicoes_cbGamas.Enabled && this.Session["GAMAS_QUANTIA_FIM"] != null)
        {
            foreach (DataRow dr in ((DataTable)this.Session["GAMAS_QUANTIA_FIM"]).Rows)
            {
                this.Condicoes_cbGamas.Items.FindByValue(dr["COD_GRUPO_PRODUTO"].ToString()).Selected = true;
            }
        }
        else
        {
            this.Session["GAMAS_QUANTIA_FIM"] = null;
        }

        if (!this.Condicoes_cbExploracao.Enabled && this.Session["EXPLORACAO"] != null)
        {
            foreach (DataRow dr in ((DataTable)this.Session["EXPLORACAO"]).Rows)
            {
                this.Condicoes_cbExploracao.Items.FindByValue(dr["TIPO_EXPLORACAO"].ToString()).Selected = true;
            }
        }
        else
        {
            this.Session["EXPLORACAO"] = null;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void loadCondicoesEquipamento()
    {
        if (this.Condicoes_rbFrio.SelectedIndex == 0)
        {
            Condicoes_BoxEquipamentos.Visible = false;
            this.dtbl_Condicoes_Equipamento.Clear();
            this.C_Condicoes_Val_dgEquipamentos_Empty.Enabled = false;
        }
        else
        {
            this.dtbl_Condicoes_Equipamento.DefaultView.Sort = "DES_TIPO_EQUIPAMENTO ASC";
            Condicoes_BoxEquipamentos.Visible = true;
            this.Condicoes_dgEquipamentos.DataSource = this.dtbl_Condicoes_Equipamento.DefaultView;
            this.Condicoes_dgEquipamentos.DataBind();
            this.C_Condicoes_Val_dgEquipamentos_Empty.Enabled = true;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteCondicoes(int idx)
    {
        this.dtbl_Condicoes_Equipamento.DefaultView[idx].Row.Delete();
        this.loadCondicoesEquipamento();
    }

    private void DeleteFicheiro(string idFile, string filePath)
    {
        SRA DB = new SRA(ConfigurationManager.AppSettings["DBCS"]);
        DB.delContratoFiles(Convert.ToInt32(idFile));

        SpiritucSI.Unilever.SRA.Business.FileMonitor.Business.MainFileMonitor fileMon = new SpiritucSI.Unilever.SRA.Business.FileMonitor.Business.MainFileMonitor();
        fileMon.DeleteFile(filePath);

        RefreshFicheiros();
    }
    #endregion

    #region CONTRAPARTIDAS
    /// <summary>
    /// 
    /// </summary>
    private void loadContrapartidas()
    {
        if (this.Contrapartidas_dgContrapartidas.Attributes["IsValid"] == "True")
        {
            this.dtbl_Contrapartidas.DefaultView.Sort = "PRODUCT_NAME ASC";
            this.Contrapartidas_dgContrapartidas.DataSource = this.dtbl_Contrapartidas.DefaultView;
            this.Contrapartidas_dgContrapartidas.DataBind();
            this.Contrapartidas_dgContrapartidas.Visible = (this.dtbl_Contrapartidas.DefaultView.Count > 0 || this.Contrapartidas_dgContrapartidas.Attributes["CanUse"] == "True");
            this.Contrapartidas_dgContrapartidas_Empty.Visible = (this.dtbl_Contrapartidas.DefaultView.Count == 0 && this.Contrapartidas_dgContrapartidas.Attributes["CanUse"] == "False");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void loadContrapartidasGeraisBonus()
    {
        if (this.ContrapartidasGerais_dgBonus.Attributes["IsValid"] == "True")
        {
            this.dtbl_ContrapartidasGerais_Bonus.DefaultView.Sort = "VALOR_LIMITE_MIN ASC, TIPO ASC";
            this.ContrapartidasGerais_dgBonus.DataSource = this.dtbl_ContrapartidasGerais_Bonus.DefaultView;
            this.ContrapartidasGerais_dgBonus.DataBind();
            this.ContrapartidasGerais_dgBonus.Visible = (this.dtbl_ContrapartidasGerais_Bonus.DefaultView.Count > 0 || this.ContrapartidasGerais_dgBonus.Attributes["CanUse"] == "True");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void loadContrapartidasGamas()
    {
        if (this.Session["CONTRAPARTIDAS_GERAIS_GAMAS"] != null)
        {
            DataTable source = (DataTable)this.Session["CONTRAPARTIDAS_GERAIS_GAMAS"];
            if (!this.ContrapartidasGerais_cbGamas.Visible)
            {
                foreach (DataRow dr in source.Rows)
                {
                    ListItem li = this.ContrapartidasGerais_cbGamas.Items.FindByValue(dr["COD_GRUPO_PRODUTO"].ToString());
                    if (li != null)
                        li.Selected = true;
                }
            }
            DataView dv = new DataView(this.dtbl_Contrapartidas_Grupos, "IS_VISIBLE='F'", "", DataViewRowState.CurrentRows);
            ArrayList Values = new ArrayList();
            foreach (DataRowView drv in dv)
            {
                if (source.Select("COD_GRUPO_PRODUTO=" + drv.Row[Convert.ToString(ConfigurationManager.AppSettings["Contrapartidas_lbGrupoValueColumnName"])]).Length > 0)
                {
                    Values.Add(drv.Row[Convert.ToString(ConfigurationManager.AppSettings["Contrapartidas_lbGrupoTextColumnName"])]);
                }
            }
            this.ContrapartidasGerais_Gamas_list.DataSource = Values;
            this.ContrapartidasGerais_Gamas_list.DataBind();

            if (this.Contrapartidas_dgContrapartidas.Attributes["CanUse"] == "False" && Values.Count == 0)
            {
                Table1.Visible = false;
                ContrapartidasGerais_dlFaseamento_Empty.Visible = true;
            }
            else
            {
                Table1.Visible = true;
                ContrapartidasGerais_dlFaseamento_Empty.Visible = false;
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteContrapartida(int idx)
    {
        int idCont = int.Parse(this.dtbl_Contrapartidas.DefaultView[idx].Row["ID_CONTRAPARTIDA"].ToString());
        string grupovalue = this.dtbl_Contrapartidas.DefaultView[idx].Row["COD_GRUPO_PRODUTO"].ToString();
        foreach (DataRow dr in this.dtbl_Contrapartidas_Grupos.Rows)
        {
            if (dr["COD_GRUPO_PRODUTO"].ToString().Equals(grupovalue))
            {
                dr["IS_VISIBLE"] = "T";
                break;
            }
        }
        this.dtbl_Contrapartidas.DefaultView[idx].Row.Delete();
        foreach (DataRow bonus in this.dtbl_Contrapartidas_Bonus.Select("ID_CONTRAPARTIDA = " + idCont))
        {
            bonus.Delete();
        }

        this.ContrapartidasGerais_cbGamas.DataSource = new DataView(this.dtbl_Contrapartidas_Grupos, "IS_VISIBLE='F'", "", DataViewRowState.CurrentRows);
        this.ContrapartidasGerais_cbGamas.DataBind();
        this.RefreshGamas();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void InsertContrapartida(DataGridItem e)
    {
        if (!this.IsValid)
        {
            return;
        }
        try
        {
            ListBox Contrapartidas_lbGrupo = (ListBox)e.FindControl("Contrapartidas_lbGrupo");
            TextBox Contrapartidas_txtPrevisao = (TextBox)e.FindControl("Contrapartidas_txtPrevisao");
            TextBox Contrapartidas_txtDesconto = (TextBox)e.FindControl("Contrapartidas_txtDesconto");
            TextBox Contrapartidas_txtComparticipacao = (TextBox)e.FindControl("Contrapartidas_txtComparticipacao");
            DropDownList Contrapartidas_ddlPrazo = (DropDownList)e.FindControl("Contrapartidas_ddlPrazo");

            DataRow dr = dtbl_Contrapartidas.NewRow();
            dr["ID_CONTRAPARTIDA"] = this.Contrapartidas_dlNextID++;
            dr["COD_GRUPO_PRODUTO"] = Contrapartidas_lbGrupo.SelectedValue;
            dr["PRODUCT_NAME"] = Contrapartidas_lbGrupo.SelectedItem.Text;
            dr["PREVISAO_VENDAS"] = Contrapartidas_txtPrevisao.Text;
            if (!Contrapartidas_txtDesconto.Text.Equals(""))
                dr["DESCONTO_FACTURA"] = Contrapartidas_txtDesconto.Text;
            if (!Contrapartidas_txtComparticipacao.Text.Equals(""))
                dr["COMP_CONCESSIONARIO"] = Contrapartidas_txtComparticipacao.Text;
            dr["PRAZO_PAGAMENTO"] = Contrapartidas_ddlPrazo.SelectedValue;
            this.dtbl_Contrapartidas.Rows.Add(dr);
            this.dtbl_Contrapartidas_Grupos.Select("COD_GRUPO_PRODUTO=" + Contrapartidas_lbGrupo.SelectedValue)[0]["IS_VISIBLE"] = "F";

            /// ACTUALIZAR OS GRUPOS E AS GAMAS
            this.ContrapartidasGerais_cbGamas.DataSource = new DataView(this.dtbl_Contrapartidas_Grupos, "IS_VISIBLE='F'", "", DataViewRowState.CurrentRows);
            this.ContrapartidasGerais_cbGamas.DataBind();
            this.RefreshGamas();
            this.Contrapartidas_dgContrapartidas.Attributes["IsValid"] = "True";
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR UMA NOVA LINHA DE CONTRAPARTIDAS.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteContrapartidaBonus(int idx)
    {
        this.dtbl_Contrapartidas_Bonus.DefaultView[idx].Row.Delete();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void InsertContrapartidaBonus(DataGridItem e)
    {
        try
        {
            Label C_lbl_Contrapartidas_Bonus_Error_Desconto = (Label)e.FindControl("C_lbl_Contrapartidas_Bonus_Error_Desconto");
            TextBox Contrapartidas_Bonus_txtValor = (TextBox)e.FindControl("Contrapartidas_Bonus_txtValor");
            TextBox Contrapartidas_Bonus_txtDesconto = (TextBox)e.FindControl("Contrapartidas_Bonus_txtDesconto");
            TextBox Contrapartidas_Bonus_txtComparticipacao = (TextBox)e.FindControl("Contrapartidas_Bonus_txtComparticipacao");
            DropDownList Contrapartidas_Bonus_Tipo = (DropDownList)e.FindControl("Contrapartidas_Bonus_Tipo");

            C_lbl_Contrapartidas_Bonus_Error_Desconto.Text = "";
            if (!this.IsValid)
            {
                return;
            }
            if (Contrapartidas_Bonus_txtDesconto.Text.Length + Contrapartidas_Bonus_txtComparticipacao.Text.Length == 0)
            {
                this.Contrapartidas_dgContrapartidas.Attributes["IsValid"] = "False";
                C_lbl_Contrapartidas_Bonus_Error_Desconto.Text = "Indique um Desconto ou Comparticipa��o";
                return;
            }
            DataRow dr = dtbl_Contrapartidas_Bonus.NewRow();
            dr["ID_CONTRAPARTIDA"] = this.Contrapartidas_dlNextID;
            dr["VALOR_LIMITE_MIN"] = int.Parse(Contrapartidas_Bonus_txtValor.Text);
            if (!Contrapartidas_Bonus_txtDesconto.Text.Equals(""))
                dr["DESCONTO"] = Contrapartidas_Bonus_txtDesconto.Text;
            if (!Contrapartidas_Bonus_txtComparticipacao.Text.Equals(""))
                dr["COMP_CONCESSIONARIO"] = Contrapartidas_Bonus_txtComparticipacao.Text;
            dr["TIPO"] = Contrapartidas_Bonus_Tipo.SelectedValue;
            this.dtbl_Contrapartidas_Bonus.Rows.Add(dr);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR UMA NOVA LINHA DE BONUS.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void InsertContrapartidaGeralBonus(DataGridItem e)
    {
        try
        {
            Label C_lbl_ContrapartidasGerais_Bonus_Error_Desconto = (Label)e.FindControl("C_lbl_ContrapartidasGerais_Bonus_Error_Desconto");
            TextBox ContrapartidasGerais_Bonus_txtValor = (TextBox)e.FindControl("ContrapartidasGerais_Bonus_txtValor");
            TextBox ContrapartidasGerais_Bonus_txtDesconto = (TextBox)e.FindControl("ContrapartidasGerais_Bonus_txtDesconto");
            TextBox ContrapartidasGerais_Bonus_txtComparticipacao = (TextBox)e.FindControl("ContrapartidasGerais_Bonus_txtComparticipacao");
            DropDownList Contrapartidasgerais_Bonus_Tipo = (DropDownList)e.FindControl("ContrapartidasGerais_Bonus_Tipo");

            C_lbl_ContrapartidasGerais_Bonus_Error_Desconto.Text = "";
            if (!this.IsValid)
            {
                return;
            }
            if (ContrapartidasGerais_Bonus_txtDesconto.Text.Length + ContrapartidasGerais_Bonus_txtComparticipacao.Text.Length == 0)
            {
                this.ContrapartidasGerais_dgBonus.Attributes["IsValid"] = "false";
                C_lbl_ContrapartidasGerais_Bonus_Error_Desconto.Text = "Indique um Desconto ou Comparticipa��o";
                return;
            }
            DataRow dr = dtbl_ContrapartidasGerais_Bonus.NewRow();
            dr["ID_CONTRATO"] = 0;
            dr["VALOR_LIMITE_MIN"] = int.Parse(ContrapartidasGerais_Bonus_txtValor.Text);
            if (!ContrapartidasGerais_Bonus_txtDesconto.Text.Equals(""))
                dr["DESCONTO"] = ContrapartidasGerais_Bonus_txtDesconto.Text;
            if (!ContrapartidasGerais_Bonus_txtComparticipacao.Text.Equals(""))
                dr["COMP_CONCESSIONARIO"] = ContrapartidasGerais_Bonus_txtComparticipacao.Text;
            dr["TIPO"] = Contrapartidasgerais_Bonus_Tipo.SelectedValue;
            this.dtbl_ContrapartidasGerais_Bonus.Rows.Add(dr);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR UMA NOVA LINHA DE BONUS GLOBAL.", exp, true);
        }
        this.loadContrapartidasGeraisBonus();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteContrapartidaGeralBonus(int idx)
    {
        this.dtbl_ContrapartidasGerais_Bonus.DefaultView[idx].Row.Delete();
        this.loadContrapartidasGeraisBonus();
    }
    /// <summary>
    /// 
    /// </summary>
    private void RefreshGamas()
    {
        int total = 0;
        foreach (ListItem li in this.ContrapartidasGerais_cbGamas.Items)
        {
            if (li.Selected)
            {
                int val = int.Parse(this.dtbl_Contrapartidas.Select("COD_GRUPO_PRODUTO=" + li.Value)[0]["PREVISAO_VENDAS"].ToString());
                total += val;
            }
        }
        this.ContrapartidasGerais_txtPrevisao.Text = "" + total + " �";
    }
    #endregion

    #region FASEAMENTO
    /// <summary>
    /// 
    /// </summary>
    private void loadFaseamento()
    {
        this.dtbl_Faseamento.DefaultView.Sort = "DATA_PAGAMENTO ASC";
        this.Faseamento_dlFaseamento.DataSource = this.dtbl_Faseamento.DefaultView;
        this.Faseamento_dlFaseamento.DataBind();

        this.Faseamento_dlFaseamento.Visible = (this.dtbl_Faseamento.DefaultView.Count > 0 || this.Faseamento_dlFaseamento.Attributes["CanUse"] == "True");
        this.Faseamento_dlFaseamento_Empty.Visible = (this.dtbl_Faseamento.DefaultView.Count == 0 && this.Faseamento_dlFaseamento.Attributes["CanUse"] == "False");
        this.Faseamento_lblVerba.Visible = (this.dtbl_Faseamento.DefaultView.Count > 0 || this.Faseamento_dlFaseamento.Attributes["CanUse"] == "True");

        double val = 0.00;
        foreach (DataRowView dr in this.dtbl_Faseamento.DefaultView)
        {
            if (dr["VALOR_PAGAMENTO"] != DBNull.Value)
                val += double.Parse(dr["VALOR_PAGAMENTO"].ToString());
        }
        this.Faseamento_lblVerba.Text = "VERBA TOTAL = " + val + " �";
        this.Faseamento_txtNIB.Enabled = (val != 0) && (this.Faseamento_dlFaseamento.Attributes["CanUse"] == "True");
        this.Faseamento_txtBANCO.Enabled = this.Faseamento_txtNIB.Enabled;
        this.Faseamento_txtDEPENDENCIA.Enabled = this.Faseamento_txtNIB.Enabled;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteFaseamento(int idx)
    {
        try
        {
            this.dtbl_Faseamento.DefaultView[idx].Row.Delete();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL APAGAR A LINHA SELECCIONADA.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param> 
    protected void InsertFazeamento(DataGridItem e)
    {
        try
        {
            if (!this.IsValid)
            {
                return;
            }
            DatePicker Faseamento_Data_Previsto = (DatePicker)e.FindControl("Faseamento_Data_Previsto");
            TextBox Faseamento_txtValor = (TextBox)e.FindControl("Faseamento_txtValor");
            TextBox Faseamento_txtComparticipacao = (TextBox)e.FindControl("Faseamento_txtComparticipacao");
            DropDownList Faseamento_lbTipo = (DropDownList)e.FindControl("Faseamento_lbTipo");
            DropDownList Faseamento_lbTTS = (DropDownList)e.FindControl("Faseamento_lbTTS");

            DataRow dr = dtbl_Faseamento.NewRow();
            if (Faseamento_Data_Previsto.SelectedDate == DateTime.MinValue)
                dr["DATA_PAGAMENTO"] = DateTime.Now;
            else
                dr["DATA_PAGAMENTO"] = Faseamento_Data_Previsto.SelectedDate;
            dr["VALOR_PAGAMENTO"] = Faseamento_txtValor.Text;
            if (!Faseamento_txtComparticipacao.Text.Equals(""))
                dr["COMP_CONCESSIONARIO"] = Faseamento_txtComparticipacao.Text;
            dr["TIPO_PG"] = Faseamento_lbTipo.SelectedItem.Value;
            dr["TTS"] = Faseamento_lbTTS.SelectedItem.Text;
            this.dtbl_Faseamento.Rows.Add(dr);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR UMA NOVA LINHA DE FASEAMENTO.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected void UpdateFazeamento(DataGridItem e)
    {
        try
        {
            DatePicker Faseamento_Data_Efectuado = (DatePicker)e.FindControl("Faseamento_Data_Efectuado");
            DataRow dr = dtbl_Faseamento.DefaultView[e.ItemIndex].Row;
            if (Faseamento_Data_Efectuado.SelectedDate == DateTime.MinValue)
                dr["DATA_PAGAMENTO_EFECTUADO"] = DBNull.Value;
            else
                dr["DATA_PAGAMENTO_EFECTUADO"] = Faseamento_Data_Efectuado.SelectedDate;
            dr.AcceptChanges();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ACTUALIZAR A LINHA DE FASEAMENTO.", exp, true);
        }
    }
    #endregion

    #region MATERIAL
    /// <summary>
    /// 
    /// </summary>
    private void loadMaterial()
    {
        this.dtbl_Material.DefaultView.Sort = "DES_MATERIAL_POS ASC";
        this.Material_dlVisibilidade.DataSource = this.dtbl_Material.DefaultView;
        this.Material_dlVisibilidade.DataBind();

        this.Material_dlVisibilidade.Visible = (this.dtbl_Material.DefaultView.Count > 0 || this.Material_dlVisibilidade.Attributes["CanUse"] == "True");
        this.Material_dlVisibilidade_Empty.Visible = (this.dtbl_Material.DefaultView.Count == 0 && this.Material_dlVisibilidade.Attributes["CanUse"] == "False");
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idx"></param>
    private void DeleteMaterial(int idx)
    {
        this.dtbl_Material.DefaultView[idx].Row.Delete();
        this.loadMaterial();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void InsertMaterial(DataGridItem e)
    {
        try
        {
            if (!this.IsValid)
            {
                return;
            }

            DropDownList Material_lbMaterial = (DropDownList)e.FindControl("Material_lbMaterial");
            TextBox Material_txtValor = (TextBox)e.FindControl("Material_txtValor");
            TextBox Material_txtQuantidade = (TextBox)e.FindControl("Material_txtQuantidade");
            TextBox Material_txtObservacoes = (TextBox)e.FindControl("Material_txtObservacoes");

            DataRow dr = dtbl_Material.NewRow();
            dr["COD_MATERIAL"] = Material_lbMaterial.SelectedItem.Value;
            dr["DES_MATERIAL_POS"] = Material_lbMaterial.SelectedItem.Text;
            dr["QUANTIDADE"] = Material_txtQuantidade.Text;
            dr["VALOR"] = Material_txtValor.Text;
            dr["OBSERVACOES"] = Material_txtObservacoes.Text;
            this.dtbl_Material.Rows.Add(dr);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR UMA NOVA LINHA DE MATERIAL.", exp, true);
        }
        this.loadMaterial();
    }
    #endregion

    #region OBSERVA��ES
    /// <summary>
    /// 
    /// </summary>
    private void loadObservacoes()
    {

    }
    #endregion
    #endregion

    #region PROPERTIES
    protected DateTime StartDate
    {
        get { return this.Condicoes_txtDataInicio.SelectedDate.Date; }
    }
    protected DateTime EndDate
    {
        get
        {
            if (this.Condicoes_rb_DataFim.Checked)
                return this.Condicoes_txtDataFim.SelectedDate.Date;
            else
            {
                DateTime Start = this.Condicoes_txtDataInicio.SelectedDate.Date;
                int days = Convert.ToInt32(Math.Round((ContratoTime * 1461) / 4, 0));
                return Start.AddDays(days);
            }
        }
    }

    #endregion
    //--and b.COD_MES_FIM_PREVISTO >= '2010/10';
    #region PAGE EVENTS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, System.EventArgs e)
    {
        ////uploadFolder = Request.PhysicalApplicationPath;// Server.MapPath("//");
        //upSpan.InnerText = uploadText;
        //saveSpan.InnerText = saveText;
        //statusSpan.InnerText = statusText;
        //uploadBtn.Value = submitText;

        if (Convert.ToBoolean(ConfigurationManager.AppSettings["SMTP_DEBUG"].ToString()))
        {
            if (Request.QueryString["IDCONTRATO"] != null)
            {
                SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
                DBsra.ErrorLog(ConfigurationManager.AppSettings["MailLog_File"], "lelele");
                DBsra.SendMail(int.Parse(Request.QueryString["IDCONTRATO"]));
            }
        }

        if (!this.User.Identity.IsAuthenticated)
        {
            Response.Redirect("~/logon.aspx", true);
        }

        this.MaintainScrollPositionOnPostBack = true;
        this.is_PrintMode = this.Request["PM"] != null;
        this.ContrapartidasGerais_dgBonus.Attributes["IsValid"] = "True";
        this.Contrapartidas_dgContrapartidas.Attributes["IsValid"] = "True";
        if (IsPostBack)
        {
            this.lblError.Text = "";
            this.lblWarning.Text = "";
            this.lblError.ToolTip = "";
            this.lblWarning.ToolTip = "";
            this.lblFile.Text = "";

            this.ReadSession();
        }
        else
        {
            Session["uid"] = "";
            Session["instanceState"] = "";
            //Session["LastState"] = "";
            this.initializeContrato();
            this.lnkPrintPDF.NavigateUrl += "";
        }

        C_BudgetSummary.Username = User.Identity.Name;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void rbPrintPDF_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.rbPrintPDF.SelectedValue.Equals("header"))
        {
            lnkPrintPDF.NavigateUrl = "~/PrintPDF.aspx?mode=parcial&PARAM_ID_Contrato=" + this.Request["idContrato"];
        }
        else
        {
            lnkPrintPDF.NavigateUrl = "~/PrintPDF.aspx?mode=full&PARAM_ID_Contrato=" + this.Request["idContrato"];
        }
        lnkPrintPDF.Visible = true;
    }

    protected void UploadBtn_Click(object o, EventArgs e)
    {
        if (FileUpLoad1.HasFile)
        {
            //FileUpLoad1.SaveAs(uploadFolder + FileUpLoad1.FileName); //@"C:\temp\" 
            if (!Directory.Exists(@"C:\temp\"))
            {
                Directory.CreateDirectory(@"C:\temp\");
            }

            string file = @"C:\temp\" + this.Request["idContrato"] + "_" + FileUpLoad1.FileName;
            FileUpLoad1.SaveAs(file);

            string uploadFolder = ConfigurationManager.AppSettings["UploadFolder"].ToString();
            string sepChar = ConfigurationManager.AppSettings["SepChar"].ToString();
            SpiritucSI.Unilever.SRA.Business.FileMonitor.Business.MainFileMonitor fileMon = new SpiritucSI.Unilever.SRA.Business.FileMonitor.Business.MainFileMonitor();
            fileMon.ProcessFile(file, uploadFolder, sepChar);

            lblFile.Text = "Envio efectuado com sucesso. ";
            RefreshFicheiros();
        }
        else
        {
            lblFile.Text = "Nenhum ficheiro foi enviado.";
        }

        //  // make sure there is a file to upload
        //  if (savename.Value == "")
        //  {
        //      status.InnerHtml = "Missing a 'save as' name.";
        //      return;
        //  }

        //  // try save the file to the web server
        //  if (filename.PostedFile != null)
        //  {
        //      if (!Directory.Exists(uploadFolder))
        //      {
        //          Directory.CreateDirectory(uploadFolder);
        //      }
        //      string sPath = uploadFolder;

        //      //build file info for display
        //      string sFileInfo =
        //          "<br>FileName: " +
        //          filename.PostedFile.FileName +
        //          "<br>ContentType: " +
        //          filename.PostedFile.ContentType +
        //          "<br>ContentLength: " +
        //          filename.PostedFile.ContentLength.ToString();

        //      try
        //      {
        //          filename.PostedFile.SaveAs(sPath + savename.Value);
        //          status.InnerHtml = "File uploaded successfully." +
        //sFileInfo;
        //      }
        //      catch (Exception exc)
        //      {
        //          status.InnerHtml = "Error saving file" +
        //              sFileInfo + "<br>" + e.ToString();
        //      }
        //  }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.RenderWFActions();
        }

        //ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), "sub", "MySubmit();");
        if (is_PrintMode)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "print", "<script language=\"javascript\">window.print()</script>", false);

            this.PesquisaEnabled(false);
            this.C_dg_Entidades.AllowPaging = false;
            this.C_Entidades_btn_Paginacao.Visible = false;
            this.boxBotoes.Visible = false;
            this.boxFicheiros.Visible = false;
            this.Panel1.Visible = false;
            this.boxBotoes_WF.Visible = false;
            this.rbPrintPDF.Visible = false;
            this.lnkPrintPDF.Visible = false;
        }

        this.C_btn_print.DataBind();
        this.rbPrintPDF.DataBind();
        this.lnkPrintPDF.DataBind();
        this.C_lbl_print.DataBind();
        this.loadCondicoes();
        this.loadContrapartidas();
        this.loadContrapartidasGamas();
        this.WriteSession();

        this.C_link_Style.DataBind();
    }
    #endregion

    #region CONTROL EVENTS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txt_Empty_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        Control t = cv;
        Control c = null;
        while (t != null && c == null)
        {
            c = (Control)t.FindControl(cv.ControlToValidate);
            t = t.Parent;
        }
        if (c != null)
        {
            PropertyInfo pi = c.GetType().GetProperty("CssClass");
            if (pi != null)
            {
                string CssClass = pi.GetValue(c, null).ToString();
                if (e.Value.Length == 0)
                {
                    if (!CssClass.Contains("errorField"))
                    {
                        pi.SetValue(c, CssClass + " errorField", null);
                    }
                }
                else
                {
                    pi.SetValue(c, CssClass.Replace(" errorField", ""), null);
                }
            }
        }
        e.IsValid = e.Value.Length > 0;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txt_EmptyGroup_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        TextBox c = (TextBox)this.FindControl(cv.ControlToValidate);
        if ((this.PontoVenda_txtMatricula.Text
           + this.PontoVenda_txtMatriculaNum.Text
           + this.PontoVenda_txtCapital.Text).Length == 0 || c.Text.Length > 0)
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
        }
        else
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }
    }

    #region CONTRATO
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_txt_NumProcesso_Changed(object sender, EventArgs e)
    {
        this.Validate("Processo");
    }
    protected void Contrato_val_txtProcesso_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        TextBox c = (TextBox)this.FindControl(cv.ControlToValidate);
        double i = 0;
        if ((!double.TryParse(e.Value, out i)) || (e.Value.Length != 15) || (i < 0))
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }
        else
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
        }
    }
    #endregion

    #region INVESTIMENTO
    protected void C_btn_InvestimentoRefresh_Click(object sender, EventArgs e)
    {
        this.loadInvestimento();
    }
    #endregion

    #region NIVEL
    protected void C_cb_Nivel_Changed(object sender, EventArgs e)
    {
        try
        {
            RadioButton rb = (RadioButton)sender;
            string val = this.C_ddl_SubGrupoEconomico.SelectedValue;

            if (rb.ID.Contains("_LE"))
            {
                if (this.C_ddl_GrupoEconomico.Items.FindByText("") == null)
                {
                    this.C_ddl_GrupoEconomico.Items.Insert(0, new ListItem("", string.Empty));
                }
                this.RefreshClientes();
                this.LoadPesquisa();
                this.C_box_Pesquisa.Visible = this.is_Editing || !this.is_DBContrato;
            }
            else
            {
                this.C_ddl_GrupoEconomico.Items.Remove("");
                this.C_box_Pesquisa.Visible = false;
            }
            this.C_ddl_GrupoEconomico_Changed(this.C_ddl_GrupoEconomico, new EventArgs());
            if (this.C_ddl_SubGrupoEconomico.Items.FindByValue(val) != null)
            {
                this.C_ddl_SubGrupoEconomico.SelectedValue = val;
            }
            this.C_ddl_SubGrupoEconomico_Changed(this.C_ddl_GrupoEconomico, new EventArgs());
            if (!rb.ID.Contains("_LE"))
                Page.Validate("Entidades");
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL MUDAR O N�VEL DO CONTRATO.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_ddl_GrupoEconomico_Changed(object sender, EventArgs e)
    {
        SRA DB = new SRA(ConfigurationManager.AppSettings["DBCS"]);
        this.C_ddl_SubGrupoEconomico.Items.Clear();
        this.C_ddl_SubGrupoEconomico.SelectedValue = null;

        if (!this.C_cb_Nivel_SGE.Checked)
        {
            this.C_ddl_SubGrupoEconomico.Items.Add(new ListItem("", string.Empty));
            this.C_ddl_SubGrupoEconomico.Items.Add(new ListItem("- - - TODOS - - -", "0"));
        }
        this.C_ddl_SubGrupoEconomico.DataSource = DB.SubGrupoEconomicoGet(this.C_ddl_GrupoEconomico.SelectedValue);
        this.C_ddl_SubGrupoEconomico.DataBind();
        if (this.C_cb_Nivel_GE.Checked)
        {
            this.RefreshClientes();
        }
        this.ValidateClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_ddl_SubGrupoEconomico_Changed(object sender, EventArgs e)
    {
        if (this.C_cb_Nivel_SGE.Checked)
        {
            this.RefreshClientes();
        }
        this.ValidateClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_Nivel_Zona_Validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.DL_CONCESSIONARIO.SelectedIndex > 0;
        if (e.IsValid)
        {
            this.DL_CONCESSIONARIO.CssClass = this.DL_CONCESSIONARIO.CssClass.Replace(" errorField", "");
        }
        else
        {
            if (!this.DL_CONCESSIONARIO.CssClass.Contains("errorField"))
            {
                this.DL_CONCESSIONARIO.CssClass = this.DL_CONCESSIONARIO.CssClass + " errorField";
            }
        }
    }
    #endregion

    #region PESQUISA
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="data"></param>
    protected void C_Entidade_Pesquisa_onSearchResult(Object sender, DataTable data)
    {
        if (data.Rows.Count > 0)
        {
            this.C_Entidade_Pesquisa.ShowSearchResult = true;
            this.C_Entidade_Pesquisa.Opened = false;
            this.C_Entidade_Pesquisa.ADVModeEnabled = true;
            this.C_Entidade_Pesquisa.Limpar();

            this.dtblClientesLEs = data;
            this.LoadClientes();
            Page.Validate("Entidades");

        }
        else
        {
            this._M_ShowError(this.lblWarning, "N�O FORAM ENCONTRADOS CLIENTES COM OS PAR�METROS INDICADOS.", null, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="data"></param>
    protected void C_Entidade_Pesquisa_onSelectResult(Object sender, DataRow data)
    {
        if (this.dtblClientesLEs.Select("COD_LOCAL_ENTREGA = " + data[0].ToString(), "", this.dtblClientesLEs.DefaultView.RowStateFilter).Length == 0)
        {
            if (this.is_Editing)
            {
                data["DATA_INICIO"] = DateTime.Now.Date;
            }
            this.dtblClientesLEs.Rows.Add(data.ItemArray);
            this.LoadClientes();
            Page.Validate("Entidades");
        }
    }
    #endregion

    #region CLIENTES
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_Entidades_Empty_validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.dtblClientesLEs.DefaultView.Count > 0;

        string css = this.C_dg_Entidades.CssClass;
        if (e.IsValid)
        {
            css = css.Replace(" errorField", "");
        }
        else
        {
            if (!css.Contains("errorField"))
            {
                css = css + " errorField";
            }
        }
        this.C_dg_Entidades.CssClass = css;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_dg_Entidades_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (this.C_dg_Entidades.Attributes["CanUse"] == "False")
            return;
        switch (e.CommandName)
        {
            case "Delete":
                this.DeleteCliente((this.C_dg_Entidades.CurrentPageIndex * this.C_dg_Entidades.PageSize) + e.Item.ItemIndex);
                Page.Validate("Entidades");
                break;
            case "DeleteAll":
                this.dtblClientesLEs.Clear();
                this.LoadClientes();
                break;
            case "Select":
                if (!this.PontoVenda_txtNome.Enabled)
                    return;
                this.SelectCliente((this.C_dg_Entidades.CurrentPageIndex * this.C_dg_Entidades.PageSize) + e.Item.ItemIndex);
                break;
            case "Edit":
                this.C_dg_Entidades.EditItemIndex = e.Item.ItemIndex;
                this.LoadClientes();
                break;
            case "Save":
                this.UpdateCliente(e.Item);
                this.C_dg_Entidades.EditItemIndex = -1;
                this.LoadClientes();
                break;
            case "Undo":
                this.C_dg_Entidades.EditItemIndex = -1;
                this.LoadClientes();
                break;
            default:
                break;
        }
        if (this.dtblClientesLEs.DefaultView.Count == 0)
        {
            this.C_Entidade_Pesquisa.ShowSearchResult = false;
            this.C_Entidade_Pesquisa.Opened = true;
            this.C_Entidade_Pesquisa.ADVModeEnabled = false;
            this.C_Entidade_Pesquisa.Limpar();
        }
        if (this.C_dg_Entidades.CurrentPageIndex > 0 && ((float)this.dtblClientesLEs.Rows.Count / (float)this.C_dg_Entidades.PageSize) <= ((float)this.C_dg_Entidades.CurrentPageIndex))
        {
            --this.C_dg_Entidades.CurrentPageIndex;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_dg_Entidades_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        this.dtblClientesLEs.DefaultView.Sort = e.SortExpression + (this.ClientesLEs_SortASC == true ? " ASC" : " DESC");
        this.ClientesLEs_SortASC = !this.ClientesLEs_SortASC;
        this.LoadClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_dg_Entidades_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        this.C_dg_Entidades.CurrentPageIndex = e.NewPageIndex;
        this.LoadClientes();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void C_Entidades_btn_Paginacao_Click(object source, EventArgs e)
    {
        this.C_dg_Entidades.AllowPaging = !this.C_dg_Entidades.AllowPaging;
        this.LoadClientes();
    }
    #endregion

    #region CONDI��ES

    #region CONTROLOS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Condicoes_DataFim_CheckChanged(object sender, EventArgs e)
    {
        this.Condicoes_txtDataFim.Enabled = this.Condicoes_rb_DataFim.Checked;
        if (!this.Condicoes_txtDataFim.Enabled)
        {
            this.Condicoes_txtDataFim.TextCssClass = this.Condicoes_txtDataFim.TextCssClass.Replace(" errorField", "");
            this.Condicoes_txtDataFim.AllowClear = true;
            this.Condicoes_txtDataFim.Clear();
            this.Condicoes_txtQuandoAtingir.Focus();
        }
        else
        {
            this.Condicoes_txtQuandoAtingir.CssClass = this.Condicoes_txtQuandoAtingir.CssClass.Replace(" errorField", "");
            this.Condicoes_cbGamas.CssClass = this.Condicoes_cbGamas.CssClass.Replace(" errorField", "");
            this.Condicoes_txtDataFim.AllowClear = false;
            this.Condicoes_txtDataFim.Focus();
            this.Condicoes_txtDataFim.MinEnabledDate = this.Condicoes_txtDataInicio.SelectedDate;
            this.Condicoes_txtDataFim.SelectedDate = this.Condicoes_txtDataInicio.SelectedDate.AddYears(1).AddDays(-1);
        }
        this.Condicoes_txtQuandoAtingir.Enabled = this.Condicoes_rb_QuantiaFim.Checked;
        this.Condicoes_cbGamas.Enabled = this.Condicoes_rb_QuantiaFim.Checked;
        this.C_Condicoes_Val_txtQuantiaFim.Enabled = this.Condicoes_rb_QuantiaFim.Checked;
        this.C_Condicoes_Val_txtQuantiaFim_Empty.Enabled = this.Condicoes_rb_QuantiaFim.Checked;
        this.C_Condicoes_Val_cbGamas_Empty.Enabled = this.Condicoes_rb_QuantiaFim.Checked;
        this.Condicoes_txtQuandoAtingir.Text = "";
        this.Condicoes_cbGamas.ClearSelection();
        this.C_Condicoes_Val_rbDataFim_Empty.Validate();
        this.loadFaseamento();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Condicoes_rbFrio_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.loadCondicoesEquipamento();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Condicoes_Equipamento_lnkInserir_Click(object sender, DataGridCommandEventArgs e)
    {
        ListBox Condicoes_Equipamento_LbTipoE = (ListBox)e.Item.FindControl("Condicoes_Equipamento_LbTipoE");
        TextBox Condicoes_Equipamento_txtQtd = (TextBox)e.Item.FindControl("Condicoes_Equipamento_txtQtd");
        try
        {
            DataRow dr = dtbl_Condicoes_Equipamento.NewRow();
            dr["COD_TIPO_EQUIPAMENTO"] = Condicoes_Equipamento_LbTipoE.SelectedItem.Value;
            dr["DES_TIPO_EQUIPAMENTO"] = Condicoes_Equipamento_LbTipoE.SelectedItem.Text;
            dr["QUANTIDADE"] = Condicoes_Equipamento_txtQtd.Text;
            this.dtbl_Condicoes_Equipamento.Rows.Add(dr);

            Condicoes_Equipamento_LbTipoE.SelectedIndex = 0;
            Condicoes_Equipamento_txtQtd.Text = "";
            this.C_Condicoes_Val_dgEquipamentos_Empty.Validate();
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL ADICIONAR UMA NOVA LINHA DE EQUIPAMENTO.", exp, true);
        }
        this.loadCondicoesEquipamento();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void Condicoes_dgEquipamentos_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (this.Condicoes_dgEquipamentos.Attributes["CanUse"] == "False")
            return;
        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteCondicoes(e.Item.ItemIndex);
                break;
            case "Inserir":
                this.Condicoes_Equipamento_lnkInserir_Click(source, e);
                break;
            default:
                break;
        }
    }

    protected void Condicoes_dgFicheiros_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Apagar":
                // 
                this.DeleteFicheiro(e.Item.Cells[0].Text, ((HyperLink)e.Item.Cells[2].FindControl("lnkFILE")).NavigateUrl);
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void Condicoes_dgEquipamentos_ItemDataBound(object source, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            ListBox c = (ListBox)e.Item.FindControl("Condicoes_Equipamento_LbTipoE");
            c.DataSource = this.dtbl_Condicoes_Equipamento_Tipo.DefaultView;
            c.DataTextField = Convert.ToString(ConfigurationManager.AppSettings["Equipamento_lbTextColumnName"]);
            c.DataValueField = Convert.ToString(ConfigurationManager.AppSettings["Equipamento_lbValueColumnName"]);
            c.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void Condicoes_txtDataInicio_Changed(object source, EventArgs e)
    {
        this.Condicoes_txtDataFim.MinEnabledDate = this.Condicoes_txtDataInicio.SelectedDate;
        this.loadFaseamento();
    }
    #endregion

    #region VALIDATORS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_Condicoes_Val_rbDataFim_Empty_Validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.Condicoes_rb_DataFim.Checked || this.Condicoes_rb_QuantiaFim.Checked;
        if (e.IsValid)
        {
            this.Condicoes_rb_DataFim.CssClass = this.Condicoes_rb_DataFim.CssClass.Replace(" errorField", "");
            this.Condicoes_rb_QuantiaFim.CssClass = this.Condicoes_rb_DataFim.CssClass.Replace(" errorField", "");
        }
        else
        {
            this.Condicoes_rb_DataFim.CssClass += " errorField";
            this.Condicoes_rb_QuantiaFim.CssClass += " errorField";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_Condicoes_Val_cbGamas_Empty_Validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.Condicoes_rb_QuantiaFim.Checked && this.Condicoes_cbGamas.SelectedIndex >= 0;
        if (e.IsValid)
        {
            this.Condicoes_cbGamas.CssClass = this.Condicoes_cbGamas.CssClass.Replace(" errorField", "");
        }
        else
        {
            this.Condicoes_cbGamas.CssClass += " errorField";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_Condicoes_Val_dgEquipamentos_Empty_validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = this.Condicoes_rbFrio.SelectedIndex == 1 && this.dtbl_Condicoes_Equipamento.Rows.Count > 0;
        if (e.IsValid)
        {
            this.Condicoes_BoxEquipamentos.CssClass = this.Condicoes_BoxEquipamentos.CssClass.Replace(" errorField", "");
        }
        else
        {
            this.Condicoes_BoxEquipamentos.CssClass += " errorField";
        }
    }
    #endregion

    #endregion

    #region CONTRAPARTIDAS
    #region CONTROLOS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Contrapartidas_dgContrapartidas_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataGrid Contrapartidas_dgBonus = (DataGrid)e.Item.FindControl("Contrapartidas_dgBonus");
            Label Contrapartidas_dgBonus_Empty = (Label)e.Item.FindControl("Contrapartidas_dgBonus_Empty");

            if (e.Item.ItemType == ListItemType.Item)
            {
                Contrapartidas_dgBonus.ItemStyle.CssClass = "AlternateListRow";
                Contrapartidas_dgBonus.AlternatingItemStyle.CssClass = "ListRow";
            }
            else
            {
                Contrapartidas_dgBonus.ItemStyle.CssClass = "ListRow";
                Contrapartidas_dgBonus.AlternatingItemStyle.CssClass = "AlternateListRow";
            }
            string grupovalue = this.dtbl_Contrapartidas.DefaultView[e.Item.ItemIndex].Row["COD_GRUPO_PRODUTO"].ToString();
            this.dtbl_Contrapartidas_Grupos.Select("COD_GRUPO_PRODUTO=" + grupovalue)[0]["IS_VISIBLE"] = "F";
            if (Contrapartidas_dgBonus != null)
            {
                DataView dv = new DataView(this.dtbl_Contrapartidas_Bonus, "ID_CONTRAPARTIDA =" + this.dtbl_Contrapartidas.DefaultView[e.Item.ItemIndex]["ID_CONTRAPARTIDA"].ToString(), "VALOR_LIMITE_MIN ASC", DataViewRowState.CurrentRows);
                Contrapartidas_dgBonus.DataSource = dv;
                Contrapartidas_dgBonus.DataBind();

                Contrapartidas_dgBonus.Visible = (dv.Count > 0);
                Contrapartidas_dgBonus_Empty.Visible = (dv.Count == 0);
            }
        }
        if (e.Item.ItemType == ListItemType.Footer)
        {
            this.Contrapartidas_dgContrapartidas_LoadFooter(sender, e.Item);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>                
    private void Contrapartidas_dgContrapartidas_LoadFooter(object sender, DataGridItem e)
    {
        ListBox Contrapartidas_lbGrupo = (ListBox)e.FindControl("Contrapartidas_lbGrupo");
        DropDownList Contrapartidas_ddlPrazo = (DropDownList)e.FindControl("Contrapartidas_ddlPrazo");
        DataGrid Contrapartidas_dgBonus_Insert = (DataGrid)e.FindControl("Contrapartidas_dgBonus_Insert");
        ImageButton Contrapartidas_lnkInserir = (ImageButton)e.FindControl("Contrapartidas_lnkInserir");

        this.Contrapartidas_dgContrapartidas.ShowFooter = this.dtbl_Contrapartidas_Grupos.DefaultView.Count > 0 && this.Contrapartidas_dgContrapartidas.Attributes["CanUse"] == "True";

        Contrapartidas_lbGrupo.DataSource = this.dtbl_Contrapartidas_Grupos.DefaultView;
        Contrapartidas_lbGrupo.DataTextField = Convert.ToString(ConfigurationManager.AppSettings["Contrapartidas_lbGrupoTextColumnName"]);
        Contrapartidas_lbGrupo.DataValueField = Convert.ToString(ConfigurationManager.AppSettings["Contrapartidas_lbGrupoValueColumnName"]);
        Contrapartidas_lbGrupo.DataBind();
        Contrapartidas_ddlPrazo.DataSource = this.PrazosPagamento();
        Contrapartidas_ddlPrazo.DataBind();
        Contrapartidas_ddlPrazo.SelectedValue = "A";
        this.dtbl_Contrapartidas_Bonus.DefaultView.RowFilter = "ID_CONTRAPARTIDA = " + this.Contrapartidas_dlNextID;
        this.dtbl_Contrapartidas_Bonus.DefaultView.Sort = "VALOR_LIMITE_MIN, TIPO ASC";
        Contrapartidas_dgBonus_Insert.DataSource = this.dtbl_Contrapartidas_Bonus.DefaultView;
        Contrapartidas_dgBonus_Insert.DataBind();
        Contrapartidas_lnkInserir.Enabled = (Contrapartidas_lbGrupo.Items.Count > 0);

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void Contrapartidas_dgContrapartidas_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteContrapartida(e.Item.ItemIndex);
                this.InvalidateInvestimento();
                this.loadContrapartidasGamas();
                break;
            case "Inserir":
                this.InsertContrapartida(e.Item);
                this.InvalidateInvestimento();
                this.loadContrapartidasGamas();
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Contrapartidas_Bonus_Val_custxtValor_validate(object sender, ServerValidateEventArgs e)
    {
        DropDownList Contrapartidas_Bonus_Tipo = (DropDownList)((Control)sender).NamingContainer.FindControl("Contrapartidas_Bonus_Tipo");

        e.IsValid = dtbl_Contrapartidas_Bonus.Select("VALOR_LIMITE_MIN = " + e.Value + " AND ID_CONTRAPARTIDA = '" + this.Contrapartidas_dlNextID + "' AND TIPO = '" + Contrapartidas_Bonus_Tipo.SelectedValue + "'").Length == 0;
        this.Contrapartidas_dgContrapartidas.Attributes["IsValid"] = this.Contrapartidas_dgContrapartidas.Attributes["IsValid"].Length > 0 ? (this.Contrapartidas_dgContrapartidas.Attributes["IsValid"].Equals("True") && e.IsValid).ToString() : e.IsValid.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Contrapartidas_Bonus_Val_cusTipo_validate(object sender, ServerValidateEventArgs e)
    {
        DropDownList Contrapartidas_Bonus_Tipo = (DropDownList)((Control)sender).NamingContainer.FindControl("Contrapartidas_Bonus_Tipo");
        TextBox Contrapartidas_Bonus_txtValor = (TextBox)((Control)sender).NamingContainer.FindControl("Contrapartidas_Bonus_txtValor");
        if (Contrapartidas_Bonus_Tipo.SelectedValue == "Acc")
        {
            e.IsValid = dtbl_Contrapartidas_Bonus.Select("VALOR_LIMITE_MIN <" + Contrapartidas_Bonus_txtValor.Text + " AND TIPO = 'Exd'").Length == 0;
        }
        if (Contrapartidas_Bonus_Tipo.SelectedValue == "Exd")
        {
            e.IsValid = dtbl_Contrapartidas_Bonus.Select("VALOR_LIMITE_MIN >" + Contrapartidas_Bonus_txtValor.Text + " AND TIPO = 'Acc'").Length == 0;
        }
        this.Contrapartidas_dgContrapartidas.Attributes["IsValid"] = this.Contrapartidas_dgContrapartidas.Attributes["IsValid"].Length > 0 ? (this.Contrapartidas_dgContrapartidas.Attributes["IsValid"].Equals("True") && e.IsValid).ToString() : e.IsValid.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void Contrapartidas_dgBonus_Insert_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        DataGrid Contrapartidas_dgBonus_Insert = (DataGrid)source;

        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteContrapartidaBonus(e.Item.ItemIndex);
                break;
            case "Inserir":
                this.InsertContrapartidaBonus(e.Item);
                break;
            default:
                break;
        }
        if (this.Contrapartidas_dgContrapartidas.Attributes["IsValid"] == "True")
        {
            this.dtbl_Contrapartidas_Bonus.DefaultView.RowFilter = "ID_CONTRAPARTIDA = " + this.Contrapartidas_dlNextID;
            this.dtbl_Contrapartidas_Bonus.DefaultView.Sort = "VALOR_LIMITE_MIN, TIPO ASC";
            Contrapartidas_dgBonus_Insert.DataSource = this.dtbl_Contrapartidas_Bonus.DefaultView;
            Contrapartidas_dgBonus_Insert.DataBind();
            this.Contrapartidas_dgContrapartidas.Attributes["IsValid"] = "False";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ContrapartidasGerais_dgBonus_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (this.ContrapartidasGerais_dgBonus.Attributes["CanUse"] == "False")
            return;
        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteContrapartidaGeralBonus(e.Item.ItemIndex);
                this.InvalidateInvestimento();
                break;
            case "Inserir":
                this.InsertContrapartidaGeralBonus(e.Item);
                this.InvalidateInvestimento();
                break;
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ContrapartidasGerais_Bonus_Val_custxtValor_validate(object sender, ServerValidateEventArgs e)
    {
        DropDownList ContrapartidasGerais_Bonus_Tipo = (DropDownList)((Control)sender).NamingContainer.FindControl("ContrapartidasGerais_Bonus_Tipo");

        e.IsValid = dtbl_ContrapartidasGerais_Bonus.Select("VALOR_LIMITE_MIN = " + e.Value + " AND TIPO = '" + ContrapartidasGerais_Bonus_Tipo.SelectedValue + "'").Length == 0;
        this.ContrapartidasGerais_dgBonus.Attributes["IsValid"] = this.ContrapartidasGerais_dgBonus.Attributes["IsValid"].Length > 0 ? (this.ContrapartidasGerais_dgBonus.Attributes["IsValid"].Equals("True") && e.IsValid).ToString() : e.IsValid.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ContrapartidasGerais_Bonus_Val_cusTipo_validate(object sender, ServerValidateEventArgs e)
    {
        DropDownList ContrapartidasGerais_Bonus_Tipo = (DropDownList)((Control)sender).NamingContainer.FindControl("ContrapartidasGerais_Bonus_Tipo");
        TextBox ContrapartidasGerais_Bonus_txtValor = (TextBox)((Control)sender).NamingContainer.FindControl("ContrapartidasGerais_Bonus_txtValor");
        if (ContrapartidasGerais_Bonus_Tipo.SelectedValue == "Acc")
        {
            e.IsValid = dtbl_ContrapartidasGerais_Bonus.Select("VALOR_LIMITE_MIN <" + ContrapartidasGerais_Bonus_txtValor.Text + " AND TIPO = 'Exd'").Length == 0;
        }
        if (ContrapartidasGerais_Bonus_Tipo.SelectedValue == "Exd")
        {
            e.IsValid = dtbl_ContrapartidasGerais_Bonus.Select("VALOR_LIMITE_MIN >" + ContrapartidasGerais_Bonus_txtValor.Text + " AND TIPO = 'Acc'").Length == 0;
        }
        this.ContrapartidasGerais_dgBonus.Attributes["IsValid"] = this.ContrapartidasGerais_dgBonus.Attributes["IsValid"].Length > 0 ? (this.ContrapartidasGerais_dgBonus.Attributes["IsValid"].Equals("True") && e.IsValid).ToString() : e.IsValid.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ContrapartidasGerais_cbGamas_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.RefreshGamas();
        this.InvalidateInvestimento();
    }
    protected void ContrapartidasGerais_Gamas_list_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal ContrapartidasGerais_Gamas_list_ItemTemplate = (Literal)e.Item.FindControl("ContrapartidasGerais_Gamas_list_ItemTemplate");
            ContrapartidasGerais_Gamas_list_ItemTemplate.Text =
                (((e.Item.ItemIndex) % 3) == 0 ? "<tr><td width='33%' align='left'>" : "<td width='33%' align='left'>")
                + e.Item.DataItem.ToString()
                + ((e.Item.ItemIndex + 1 % 3) == 0 ? "</td></tr>" : "</td>");
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            Literal ContrapartidasGerais_Gamas_list_FooterTemplate = (Literal)e.Item.FindControl("ContrapartidasGerais_Gamas_list_FooterTemplate");
            ArrayList dv = (ArrayList)ContrapartidasGerais_Gamas_list.DataSource;
            string footer = "";
            switch (dv.Count % 3)
            {
                case 1:
                    footer = "<td width='33%' align='left'></td>";
                    goto case 2;
                case 2:
                    footer += "<td width='33%' align='left'></td>";
                    goto case 0;
                case 0:
                    footer += "</Table>";
                    break;
            }
            ContrapartidasGerais_Gamas_list_FooterTemplate.Text = footer;
        }
    }
    protected void C_val_dgContrapartidas_Validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = true;
        DataView dv = this.dtbl_Contrapartidas_Grupos.DefaultView;
        string filter = dv.RowFilter;
        foreach (ListItem cb in this.Condicoes_cbGamas.Items)
        {
            dv.RowFilter = filter + " AND COD_GRUPO_PRODUTO=" + cb.Value;
            if (cb.Selected && dv.Count > 0)
            {
                e.IsValid = false;
                break;
            }
        }

        if (e.IsValid)
        {
            this.Contrapartidas_dgContrapartidas.CssClass = this.Contrapartidas_dgContrapartidas.CssClass.Replace(" errorField", "");
        }
        else
        {
            this.Contrapartidas_dgContrapartidas.CssClass += " errorField";
        }
        dv.RowFilter = filter;
    }
    #endregion

    #region VALIDATORS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_ContrapartidasGlobais_Gamas_validate(object sender, ServerValidateEventArgs e)
    {
        int GamasCount = this.ContrapartidasGerais_cbGamas.SelectedIndex + 1;
        int BonusCount = this.dtbl_ContrapartidasGerais_Bonus.DefaultView.Count;

        e.IsValid = (GamasCount + BonusCount) == 0
        || GamasCount > 0;

        if (e.IsValid)
        {
            this.ContrapartidasGerais_cbGamas.CssClass = this.ContrapartidasGerais_cbGamas.CssClass.Replace(" errorField", "");
        }
        else
        {
            this.ContrapartidasGerais_cbGamas.CssClass += " errorField";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_ContrapartidasGlobais_Bonus_validate(object sender, ServerValidateEventArgs e)
    {
        int GamasCount = this.ContrapartidasGerais_cbGamas.SelectedIndex + 1;
        int BonusCount = this.dtbl_ContrapartidasGerais_Bonus.DefaultView.Count;

        e.IsValid = (GamasCount + BonusCount) == 0
        || BonusCount > 0;

        if (e.IsValid)
        {
            this.BonusGrid.CssClass = this.BonusGrid.CssClass.Replace(" errorField", "");
        }
        else
        {
            this.BonusGrid.CssClass += " errorField";
        }
    }
    #endregion
    #endregion

    #region FASEAMENTO
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void Faseamento_dlFaseamento_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (this.Faseamento_dlFaseamento.Attributes["CanUse"] == "False")
            return;
        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteFaseamento((this.Faseamento_dlFaseamento.CurrentPageIndex * this.Faseamento_dlFaseamento.PageSize) + e.Item.ItemIndex);
                this.loadFaseamento();
                this.InvalidateInvestimento();
                break;
            case "Inserir":
                this.InsertFazeamento(e.Item);
                this.loadFaseamento();
                this.InvalidateInvestimento();
                break;
            case "Edit":
                this.Faseamento_dlFaseamento.EditItemIndex = e.Item.ItemIndex;
                this.loadFaseamento();
                break;
            case "Save":
                this.UpdateFazeamento(e.Item);
                this.Faseamento_dlFaseamento.EditItemIndex = -1;
                this.loadFaseamento();
                break;
            case "Undo":
                this.Faseamento_dlFaseamento.EditItemIndex = -1;
                this.loadFaseamento();
                break;
            default:
                break;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Faseamento_dlFaseamento_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            DropDownList Faseamento_lbTTS = (DropDownList)e.Item.FindControl("Faseamento_lbTTS");

            SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
            Faseamento_lbTTS.DataSource = DBsra.getTTS();
            Faseamento_lbTTS.DataTextField = Convert.ToString(ConfigurationManager.AppSettings["Faseamento_lbTTSTextColumnName"]);
            Faseamento_lbTTS.DataValueField = Convert.ToString(ConfigurationManager.AppSettings["Faseamento_lbTTSValueColumnName"]);
            Faseamento_lbTTS.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txtNIB_EmptyGroup_Validate(object sender, ServerValidateEventArgs e)
    {
        CustomValidator cv = (CustomValidator)sender;
        TextBox c = (TextBox)this.FindControl(cv.ControlToValidate);
        if ((this.Faseamento_txtBANCO.Text
           + this.Faseamento_txtDEPENDENCIA.Text
           + this.Faseamento_txtNIB.Text).Length == 0 || c.Text.Length > 0)
        {
            e.IsValid = true;
            c.CssClass = c.CssClass.Replace(" errorField", "");
        }
        else
        {
            e.IsValid = false;
            if (!c.CssClass.Contains("errorField"))
            {
                c.CssClass = c.CssClass + " errorField";
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_val_txtData_Validate(object sender, ServerValidateEventArgs e)
    {
        DateTime start = this.StartDate;
        DateTime end = this.EndDate;
        e.IsValid = true;
        foreach (DataRowView dr in this.dtbl_Faseamento.DefaultView)
        {
            DateTime d = (DateTime)dr["DATA_PAGAMENTO"];
            if (d < start || d > end)
            {
                e.IsValid = false;
                break;
            }
        }
        if (e.IsValid)
        {
            this.FaseamentoBox.CssClass = this.FaseamentoBox.CssClass.Replace(" errorField", "");
        }
        else
        {
            if (!this.FaseamentoBox.CssClass.Contains("errorField"))
            {
                this.FaseamentoBox.CssClass = this.FaseamentoBox.CssClass + " errorField";
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Faseamento_txtComparticipacaoCompras_Changed(object sender, EventArgs e)
    {
        this.Faseamento_cbComparticipacao.Checked = false;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Faseamento_Val_txtComparticipacaoCompras_Empty_Validate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = false;
        foreach (DataRowView dr in this.dtbl_Faseamento.DefaultView)
        {
            if (dr["COMP_CONCESSIONARIO"] != null && dr["COMP_CONCESSIONARIO"].ToString().Length > 0)
            {
                e.IsValid = true;
                break;
            }
        }
        if (e.IsValid)
        {
            this.FaseamentoBox.CssClass = this.FaseamentoBox.CssClass.Replace(" errorField", "");
        }
        else
        {
            if (!this.FaseamentoBox.CssClass.Contains("errorField"))
            {
                this.FaseamentoBox.CssClass = this.FaseamentoBox.CssClass + " errorField";
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Faseamento_Val_txtComparticipacao_Validate(object sender, ServerValidateEventArgs e)
    {
        int val = 0;
        if (e.Value.Length > 0)
            val = int.Parse(e.Value);
        if (this.Faseamento_cbComparticipacao.Checked || val >= 5)
        {
            e.IsValid = true;
        }
        else
        {
            foreach (DataRowView dr in this.dtbl_Faseamento.DefaultView)
            {
                if (dr["COMP_CONCESSIONARIO"] != null && dr["COMP_CONCESSIONARIO"].ToString().Length > 0)
                {
                    e.IsValid = false;
                    break;
                }
            }
        }
        if (e.IsValid)
        {
            this.Faseamento_txtComparticipacaoCompras.CssClass = this.Faseamento_txtComparticipacaoCompras.CssClass.Replace(" errorField", "");
            this.Faseamento_cbComparticipacao.Style["display"] = "none";
        }
        else
        {
            if (!this.Faseamento_txtComparticipacaoCompras.CssClass.Contains("errorField"))
            {
                this.Faseamento_txtComparticipacaoCompras.CssClass = this.Faseamento_txtComparticipacaoCompras.CssClass + " errorField";
                this.Faseamento_cbComparticipacao.Style["display"] = "block";
            }
        }
    }


    #endregion

    #region MATERIAL
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void Material_dlVisibilidade_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (this.Material_dlVisibilidade.Attributes["CanUse"] == "False")
            return;
        switch (e.CommandName)
        {
            case "Apagar":
                this.DeleteMaterial((this.Material_dlVisibilidade.CurrentPageIndex * this.Material_dlVisibilidade.PageSize) + e.Item.ItemIndex);
                break;
            case "Inserir":
                this.InsertMaterial(e.Item);
                break;
            default:
                break;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Material_dlVisibilidade_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            DropDownList Material_lbMaterial = (DropDownList)e.Item.FindControl("Material_lbMaterial");
            TextBox Material_txtValor = (TextBox)e.Item.FindControl("Material_txtValor");

            Material_lbMaterial.DataSource = this.dtbl_MaterialSource;
            Material_lbMaterial.DataTextField = Convert.ToString(ConfigurationManager.AppSettings["Material_lbTextColumnName"]);
            Material_lbMaterial.DataValueField = Convert.ToString(ConfigurationManager.AppSettings["Material_KeyColumnName"]);
            Material_lbMaterial.DataBind();
            if (Material_lbMaterial.SelectedIndex < 0)
                Material_lbMaterial.SelectedIndex = 0;
            string ValueColumn = ConfigurationManager.AppSettings["Material_ValueColumnName"];
            DataRow mat = this.dtbl_MaterialSource.Rows.Find(Material_lbMaterial.SelectedValue);
            Material_txtValor.Text = mat[ValueColumn].ToString();
            Material_txtValor.ReadOnly = mat["VALOR_ALTERAVEL"].Equals("F");
        }
    }
    protected void Material_lbMaterial_Change(object sender, EventArgs e)
    {
        DropDownList Material_lbMaterial = (DropDownList)sender;
        DataGridItem dg = (DataGridItem)Material_lbMaterial.NamingContainer;
        TextBox Material_txtValor = (TextBox)dg.FindControl("Material_txtValor");
        if (Material_lbMaterial.SelectedIndex < 0)
            Material_lbMaterial.SelectedIndex = 0;
        string ValueColumn = ConfigurationManager.AppSettings["Material_ValueColumnName"];
        DataRow mat = this.dtbl_MaterialSource.Rows.Find(Material_lbMaterial.SelectedValue);
        Material_txtValor.Text = mat[ValueColumn].ToString();
        Material_txtValor.ReadOnly = mat["VALOR_ALTERAVEL"].Equals("F");
    }
    #endregion

    #region BOT�ES
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>    
    protected void C_btn_Home_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/sra.aspx", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkInserir_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/contrato.aspx", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Botoes_btEnviar_Click(object sender, System.EventArgs e)
    {
        int IDContrato = 0;
        try
        {
            if (!this.ContratoValido())
            {
                return;
            }

            string NumContrato = this.InserirContrato(ref IDContrato, this.NumeroAnterior, this.Contrato_Classif, Contrato_Status);
            if (NumContrato.Equals(""))
                return;
            if (this.NumeroAnterior.Length > 0)
            {
                SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
                DBsra.UpdateContratoStatus(Convert.ToInt32(this.Request["idContrato"]), "T", this.C_lbl_Motivo.Value.Replace("[NUMERO]", NumContrato), this.Botoes_txtDataEfectiva.SelectedDate, NumContrato, User.Identity.Name);
            }
            string body = "Foi inserido o contrato com o n�mero " + NumContrato + ", do tipo '" + this.Contrato_ddlTipo.SelectedItem.Text + "' e classifica��o '" + this.Contrato_txtProcessoClassif.Text + "', referente ao cliente " + this.PontoVenda_txtNome.Text + ".";
            string error = "";
            //if (!this.EnviarMail(body, this.Contrato_Classif))
            //{
            //    error = "N�o foi poss�vel enviar a notifica��o.";
            //}
            this._M_ShowError(this.lblWarning, "O CONTRATO FOI INSERIDO COM O N� " + NumContrato + ".     " + error, null, true, "contrato.aspx?idContrato=" + IDContrato.ToString());
        }
        catch (CodeExistsException exp)
        {
            this._M_ShowError(this.lblError, "O N� DE PROCESSO INDICADO J� EXISTE.", exp, true);
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL INSERIR O CONTRATO NA BASE DE DADOS.", exp, true);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Botoes_btEnviarValid_Click(object sender, EventArgs e)
    {
        int IDContrato = 0;
        try
        {
            if (!this.ContratoValido())
            {
                return;
            }

            string NumContrato = this.InserirContrato(ref IDContrato, this.NumeroAnterior, this.Contrato_Classif, "V");
            if (NumContrato.Equals(""))
                return;
            if (this.NumeroAnterior.Length > 0)
            {
                SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
                DBsra.UpdateContratoStatus(Convert.ToInt32(this.Request["idContrato"]), "T", this.C_lbl_Motivo.Value.Replace("[NUMERO]", NumContrato), this.Botoes_txtDataEfectiva.SelectedDate, NumContrato, User.Identity.Name);
            }
            string body = "Foi inserido e validado o contrato com o n�mero " + NumContrato + ", do tipo '" + this.Contrato_ddlTipo.SelectedItem.Text + "' e classifica��o '" + this.Contrato_txtProcessoClassif.Text + "', referente ao cliente " + this.PontoVenda_txtNome.Text + ".";
            string error = "";
            //if (!this.EnviarMail(body, this.Contrato_Classif))
            //{
            //    error = "N�o foi poss�vel enviar a notifica��o.";
            //}
            this._M_ShowError(this.lblWarning, "O CONTRATO FOI INSERIDO COM O N� " + NumContrato + ".     " + error, null, true, "contrato.aspx?idContrato=" + IDContrato.ToString());
        }
        catch (CodeExistsException exp)
        {
            this._M_ShowError(this.lblError, "O N� DE PROCESSO INDICADO J� EXISTE.", exp, true);
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL INSERIR O CONTRATO NA BASE DE DADOS.", exp, true);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Botoes_btGuardar_Click(object sender, System.EventArgs e)
    {
        if (!this.ContratoValido())
        {
            return;
        }
        try
        {
            int IDContrato = 0;
            if (this.is_Editing)
            {
                string NumContrato = this.ActualizarContrato(ref IDContrato, this.NumeroAnterior, this.Contrato_Classif, this.Contrato_Status);
                if (NumContrato.Equals(""))
                    return;
                string body = "Foi actualizado o contrato com o n�mero " + NumContrato + ", do tipo '" + this.Contrato_ddlTipo.SelectedItem.Text + "' e classifica��o '" + this.Contrato_txtProcessoClassif.Text + "', referente ao cliente " + this.PontoVenda_txtNome.Text + ".";
                string error = "";
                //if (!this.EnviarMail(body, this.Contrato_Classif))
                //{
                //    error = "N�o foi poss�vel enviar a notifica��o.";
                //}
                this._M_ShowError(this.lblWarning, "O CONTRATO FOI ACTUALIZADO COM O N� " + NumContrato + ".     " + error, null, true, "contrato.aspx?idContrato=" + IDContrato.ToString());
                this.is_Editing = false;
            }
            else
            {
                string NumContrato = this.InserirContrato(ref IDContrato, this.NumeroAnterior, this.Contrato_Classif, Contrato_Status);
                if (NumContrato.Equals(""))
                    return;
                if (this.NumeroAnterior.Length > 0)
                {
                    SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
                    DBsra.UpdateContratoStatus(Convert.ToInt32(this.Request["idContrato"]), "T", this.C_lbl_Motivo.Value.Replace("[NUMERO]", NumContrato), this.Botoes_txtDataEfectiva.SelectedDate, NumContrato, User.Identity.Name);
                }
                string body = "Foi inserido o contrato com o n�mero " + NumContrato + ", do tipo '" + this.Contrato_ddlTipo.SelectedItem.Text + "' e classifica��o '" + this.Contrato_txtProcessoClassif.Text + "', referente ao cliente " + this.PontoVenda_txtNome.Text + ".";
                string error = "";
                //if (!this.EnviarMail(body, this.Contrato_Classif))
                //{
                //    error = "N�o foi poss�vel enviar a notifica��o.";
                //}
                this._M_ShowError(this.lblWarning, "O CONTRATO FOI INSERIDO COM O N� " + NumContrato + ".     " + error, null, true, "contrato.aspx?idContrato=" + IDContrato.ToString());
            }
        }
        catch (CodeExistsException exp)
        {
            this._M_ShowError(this.lblError, "O N� DE PROCESSO INDICADO J� EXISTE.", exp, true);
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL GUARDAR O CONTRATO NA BASE DE DADOS.", exp, true);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Botoes_btGuardarValid_Click(object sender, EventArgs e)
    {
        if (!this.ContratoValido())
        {
            return;
        }
        try
        {
            int IDContrato = 0;
            if (this.is_Editing)
            {
                string NumContrato = this.ActualizarContrato(ref IDContrato, this.NumeroAnterior, this.Contrato_Classif, "V");
                if (NumContrato.Equals(""))
                    return;
                string body = "Foi actualizado e validado o contrato com o n�mero " + NumContrato + ", do tipo '" + this.Contrato_ddlTipo.SelectedItem.Text + "' e classifica��o '" + this.Contrato_txtProcessoClassif.Text + "', referente ao cliente " + this.PontoVenda_txtNome.Text + ".";
                string error = "";
                //if (!this.EnviarMail(body, this.Contrato_Classif))
                //{
                //    error = "N�o foi poss�vel enviar a notifica��o.";
                //}
                this._M_ShowError(this.lblWarning, "O CONTRATO FOI ACTUALIZADO COM O N� " + NumContrato + ".     " + error, null, true, "contrato.aspx?idContrato=" + IDContrato.ToString());
                this.is_Editing = false;
            }
            else
            {
                string NumContrato = this.InserirContrato(ref IDContrato, this.NumeroAnterior, this.Contrato_Classif, "V");
                if (NumContrato.Equals(""))
                    return;
                if (this.NumeroAnterior.Length > 0)
                {
                    SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
                    DBsra.UpdateContratoStatus(Convert.ToInt32(this.Request["idContrato"]), "T", this.C_lbl_Motivo.Value.Replace("[NUMERO]", NumContrato), this.Botoes_txtDataEfectiva.SelectedDate, NumContrato, User.Identity.Name);
                }
                string body = "Foi inserido e validado o contrato com o n�mero " + NumContrato + ", do tipo '" + this.Contrato_ddlTipo.SelectedItem.Text + "' e classifica��o '" + this.Contrato_txtProcessoClassif.Text + "', referente ao cliente " + this.PontoVenda_txtNome.Text + ".";
                string error = "";
                //if (!this.EnviarMail(body, this.Contrato_Classif))
                //{
                //    error = "N�o foi poss�vel enviar a notifica��o.";
                //}
                this._M_ShowError(this.lblWarning, "O CONTRATO FOI INSERIDO COM O N� " + NumContrato + ".     " + error, null, true, "contrato.aspx?idContrato=" + IDContrato.ToString());
            }
        }
        catch (CodeExistsException exp)
        {
            this._M_ShowError(this.lblError, "O N� DE PROCESSO INDICADO J� EXISTE.", exp, true);
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL GUARDAR O CONTRATO NA BASE DE DADOS.", exp, true);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Botoes_btEditar_Click(object sender, System.EventArgs e)
    {
        try
        {
            this.is_DBContrato = false;
            this.is_Editing = true;
            this.LoadPesquisa();

            /// BLOCO DE C�DIGO QUE SUBSTITUI O REFRESH DEVIDO AO NOVO ESTADO DE EDI��O DE CONTRATOS
            this.NivelEnabled(true);
            this.Contrato_ddlTipo.Enabled = true;
            this.PesquisaEnabled(true);
            this.ClientesEnabled(true);
            this.PontoVendaEnabled(true);
            this.CondicoesEnabled(true);
            this.ContrapartidasEnabled(true);
            this.FaseamentoEnabled(true);
            this.MaterialEnabled(true);
            this.ObservacoesEnabled(true);
            this.ObservacoesMotivoEnabled(true);

            this.Botoes_btEnviar.Visible = false;
            this.C_btn_Editar.Visible = false;
            this.Botoes_btGuardar.Visible = true;
            this.Botoes_btValidar.Visible = false;
            
            this.Botoes_btSubstituirCanc.Visible = false;
            this.Botoes_btTerminar.Visible = false;
            this.box_dataFimEfectivo.Visible = false;
            this.loadContrapartidasGamas();
            this.loadFaseamento();
            this.loadMaterial();
            this.RefreshGamas();
            this.loadContrapartidasGeraisBonus();

            this.C_lbl_Motivo.Value = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> EDITADO" + "\n" + this.Observacoes_txtObservacoes_Motivo.Text;
            this.Observacoes_txtObservacoes_Motivo.Text = this.C_lbl_Motivo.Value;
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL PROCEDER � ALTERA��O DO CONTRATO. TENTE NOVAMENTE", exp, true);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Botoes_btSubstituirCont_Click(object sender, System.EventArgs e)
    {
        this.NumeroAnterior = this.C_txt_Processo_Actual.Text;
        this.loadNumAnterior();
        this.C_txt_Processo_Actual.Text = "";
        this.Contrato_Classif = "S";
        this.Contrato_Status = "I";
        this.C_Contrato_lbl_Data.Text = DateTime.Now.ToString("dd-MM-yyyy");
        this.is_DBContrato = false;
        this.box_ObservacoesMotivo.Visible = true;
        this.Condicoes_txtDataInicio.MinEnabledDate = this.Botoes_txtDataEfectiva.SelectedDate.AddDays(1);
        this.Condicoes_txtDataInicio.SelectedDate = this.Condicoes_txtDataInicio.MinEnabledDate;
        this.Condicoes_txtDataFim.MinEnabledDate = this.Condicoes_txtDataInicio.MinEnabledDate;

        #region LIMPAR TABELAS
        #region DATAS DOS LES
        foreach (DataRow le in this.dtblClientesLEs.Rows)
        {
            le["DATA_INICIO"] = DBNull.Value;
            le["DATA_FIM"] = DBNull.Value;
        }
        #endregion

        #region CONTRAPARTIDAS
        this.dtbl_Contrapartidas = new DataTable();
        string[] ListColumns = ConfigurationManager.AppSettings["Contrapartidas_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Contrapartidas.Columns.Add(column);
        }
        #endregion

        #region CONTRAPARTIDAS_BONUS
        this.dtbl_Contrapartidas_Bonus = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["Contrapartidas_Bonus_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Contrapartidas_Bonus.Columns.Add(column);
        }
        this.dtbl_Contrapartidas_Bonus.Columns["VALOR_LIMITE_MIN"].DataType = typeof(int);
        #endregion

        #region CONTRAPARTIDAS_BONUS_GLOBAL
        this.dtbl_ContrapartidasGerais_Bonus = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["ContrapartidasGerais_Bonus_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_ContrapartidasGerais_Bonus.Columns.Add(column);
        }
        this.dtbl_ContrapartidasGerais_Bonus.Columns["VALOR_LIMITE_MIN"].DataType = typeof(int);
        #endregion

        #region FASEAMENTOS
        this.dtbl_Faseamento = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["Faseamento_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Faseamento.Columns.Add(column);
        }
        this.dtbl_Faseamento.Columns["DATA_PAGAMENTO"].DataType = typeof(DateTime);
        this.dtbl_Faseamento.Columns["DATA_PAGAMENTO_EFECTUADO"].DataType = typeof(DateTime);
        #endregion

        #region GAMAS
        foreach (DataRow dr in this.dtbl_Contrapartidas_Grupos.Rows)
        {
            dr["IS_VISIBLE"] = "T";
        }
        ((DataTable)this.Session["CONTRAPARTIDAS_GERAIS_GAMAS"]).Clear();
        #endregion
        #endregion

        this.RefreshState();
        this.LoadPesquisa();
        this.loadTipo();
        this.loadStatus();
        this.loadContrapartidas();
        this.loadContrapartidasGeraisBonus();
        this.ContrapartidasGerais_cbGamas.DataSource = new DataView(this.dtbl_Contrapartidas_Grupos, "IS_VISIBLE='F'", "", DataViewRowState.CurrentRows);
        this.ContrapartidasGerais_cbGamas.DataBind();
        this.RefreshGamas();
        this.loadFaseamento();
        this.loadMaterial();
        this.loadCondicoes();
        this.loadNumSeguinte();
        this.loadFimEfectivo();
        string Motivo = this.C_lbl_Motivo.Value;
        this.C_lbl_Motivo.Value = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> SUBSTITUIDO PELO CONTRATO '[NUMERO]' PORQUE: " + (Motivo.Length > 0 ? Motivo : "<sem motivo>") + "\n" + this.Observacoes_txtObservacoes_Motivo.Text;
        this.Observacoes_txtObservacoes_Motivo.Text = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> SUBSTITUI O CONTRATO '" + this.NumeroAnterior + "' PORQUE: " + (Motivo.Length > 0 ? Motivo : "<sem motivo>");
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Botoes_btSubstituirCanc_Click(object sender, System.EventArgs e)
    {
        this.NumeroAnterior = this.C_txt_Processo_Actual.Text;
        this.loadNumAnterior();
        this.C_txt_Processo_Actual.Text = "";
        this.Contrato_Classif = "C";
        this.Contrato_Status = "I";
        this.C_Contrato_lbl_Data.Text = DateTime.Now.ToString("dd-MM-yyyy");
        this.is_DBContrato = false;
        this.box_ObservacoesMotivo.Visible = true;
        this.Condicoes_txtDataInicio.MinEnabledDate = this.Botoes_txtDataEfectiva.SelectedDate.AddDays(1);
        this.Condicoes_txtDataInicio.SelectedDate = this.Condicoes_txtDataInicio.MinEnabledDate;
        this.Condicoes_txtDataFim.MinEnabledDate = this.Condicoes_txtDataInicio.MinEnabledDate;

        #region LIMPAR TABELAS
        #region CONTRAPARTIDAS
        this.dtbl_Contrapartidas = new DataTable();
        string[] ListColumns = ConfigurationManager.AppSettings["Contrapartidas_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Contrapartidas.Columns.Add(column);
        }
        #endregion

        #region CONTRAPARTIDAS_BONUS
        this.dtbl_Contrapartidas_Bonus = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["Contrapartidas_Bonus_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Contrapartidas_Bonus.Columns.Add(column);
        }
        this.dtbl_Contrapartidas_Bonus.Columns["VALOR_LIMITE_MIN"].DataType = typeof(int);
        #endregion

        #region CONTRAPARTIDAS_BONUS_GLOBAL
        this.dtbl_ContrapartidasGerais_Bonus = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["ContrapartidasGerais_Bonus_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_ContrapartidasGerais_Bonus.Columns.Add(column);
        }
        this.dtbl_ContrapartidasGerais_Bonus.Columns["VALOR_LIMITE_MIN"].DataType = typeof(int);
        #endregion

        #region FASEAMENTOS
        this.dtbl_Faseamento = new DataTable();
        ListColumns = ConfigurationManager.AppSettings["Faseamento_ListColumnsName"].Replace(" ", "").Split(new char[] { ',' });
        foreach (string column in ListColumns)
        {
            this.dtbl_Faseamento.Columns.Add(column);
        }
        this.dtbl_Faseamento.Columns["DATA_PAGAMENTO"].DataType = typeof(DateTime);
        this.dtbl_Faseamento.Columns["DATA_PAGAMENTO_EFECTUADO"].DataType = typeof(DateTime);
        #endregion

        #region GAMAS
        foreach (DataRow dr in this.dtbl_Contrapartidas_Grupos.Rows)
        {
            dr["IS_VISIBLE"] = "T";
        }
        ((DataTable)this.Session["CONTRAPARTIDAS_GERAIS_GAMAS"]).Clear();
        #endregion
        #endregion

        this.RefreshState();
        this.LoadPesquisa();
        this.loadTipo();
        this.loadStatus();
        this.loadContrapartidas();
        this.loadContrapartidasGeraisBonus();
        this.ContrapartidasGerais_cbGamas.DataSource = new DataView(this.dtbl_Contrapartidas_Grupos, "IS_VISIBLE='F'", "", DataViewRowState.CurrentRows);
        this.ContrapartidasGerais_cbGamas.DataBind();
        this.RefreshGamas();
        this.loadFaseamento();
        this.loadMaterial();
        this.loadCondicoes();
        this.loadNumSeguinte();
        this.loadFimEfectivo();
        string Motivo = this.C_lbl_Motivo.Value;
        this.C_lbl_Motivo.Value = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> SUBSTITUIDO PELO CONTRATO '[NUMERO]' PORQUE: " + (Motivo.Length > 0 ? Motivo : "<sem motivo>") + "\n" + this.Observacoes_txtObservacoes_Motivo.Text;
        this.Observacoes_txtObservacoes_Motivo.Text = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> SUBSTITUI O CONTRATO '" + this.NumeroAnterior + "' PORQUE: " + (Motivo.Length > 0 ? Motivo : "<sem motivo>");
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Botoes_btTerminar_Click(object sender, System.EventArgs e)
    {
        try
        {
            SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);

            DBsra.UpdateContratoStatus(Convert.ToInt32(this.Request["idContrato"]), "T", DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> TERMINADO PORQUE: " + (this.C_lbl_Motivo.Value.Length > 0 ? this.C_lbl_Motivo.Value : "<sem motivo>") + "\n" + this.Observacoes_txtObservacoes_Motivo.Text, this.Botoes_txtDataEfectiva.SelectedDate, User.Identity.Name);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "redirecionamento", "window.location.href='" + this.Request.Url.ToString() + "';", true);
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL TERMINAR O CONTRATO. TENTE NOVAMENTE", exp, true);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Botoes_btValidar_Click(object sender, EventArgs e)
    {
        UpdateContratoStatusValid();
    }

    public void UpdateContratoStatusValid()
    {
        try
        {
            SRA DBsra = new SRA(ConfigurationManager.AppSettings["DBCS"]);
            DBsra.UpdateContratoStatus(Convert.ToInt32(this.Request["idContrato"]), "V", DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> VALIDADO\n" + this.Observacoes_txtObservacoes_Motivo.Text, DateTime.Now, User.Identity.Name);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "redirecionamento", "window.location.href='" + this.Request.Url.ToString() + "';", true);
        }
        catch (myDBException exp)
        {
            this._M_ShowError(this.lblError, "N�O FOI POSS�VEL VALIDAR O CONTRATO. TENTE NOVAMENTE", exp, true);
        }
        catch (Exception exp)
        {
            this._M_ShowError(this.lblError, "OCORREU UM ERRO INESPERADO.", exp, true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void C_btn_Observacoes_add_Click(object sender, EventArgs e)
    {
        this.Observacoes_txtObservacoes_Motivo.Text = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (" + User.Identity.Name + ") -> REGISTO: " + this.Observacoes_Motivo_Add.Text + "\n" + this.Observacoes_txtObservacoes_Motivo.Text;
        this.Observacoes_Motivo_Add.Text = "";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>    
    protected void C_Botoes_btn_Seguinte_Click(object sender, System.EventArgs e)
    {
        if (this.IsValid)
        {
            this.boxContrato.Visible = true;
            this.boxInvestimento.Visible = true;
            this.C_box_Nivel.Visible = true;
            this.C_box_Entidade.Visible = true;
            this.C_box_Entidade.Visible = true;
            this.boxPontoVenda.Visible = true;
            this.boxCondicoes.Visible = true;
            this.boxContrapartidas.Visible = true;
            this.BoxContrapartidasGlobais.Visible = true;
            this.boxFaseamento.Visible = true;
            this.boxMaterial.Visible = true;
            this.boxObservacoes.Visible = true;
            this.boxBotoes.Visible = true;
            this.boxBotoes_WF.Visible = true;
            this.boxFicheiros.Visible = false;
            this.loadFaseamento();

            this.RefreshState();
        }
    }
    #endregion
    #endregion

    #region WORKFLOW EVENTS
    private void RenderWFActions()
    {
        lblStateName.Visible = !((Session["uid"] == null) || (Session["uid"] == ""));
        if ((Session["uid"] == null) || (Session["uid"] == ""))
        {
            UpdateButtonsVisibility("");
            return;
        }

        try
        {
            Guid _uid = new Guid(Session["uid"].ToString());
            string state;
            if (Session != null && Session["instanceState"] != null && (Session["instanceState"].ToString()=="APPROVED" || Session["instanceState"].ToString()=="REJECTED"))
            {   
                state= Session["instanceState"].ToString();
                this.StateName.Text = state;
            }
            //if (WFRuntime.GetLoadedWorkflows().Any(x => x.InstanceId == _uid))
            if (true)
            {
                WorkflowInstance instance = this.WFRuntime.GetWorkflow(_uid);
                StateMachineWorkflowInstance smwi = new StateMachineWorkflowInstance(this.WFRuntime, _uid);
                ReadOnlyCollection<WorkflowQueueInfo> queues = instance.GetWorkflowQueueData();
                this.RepeaterWFActions.Visible = false;

                UpdateButtonsVisibility(smwi.CurrentStateName.ToUpper());

                StateName.Text = smwi.CurrentStateName.ToUpper();

                // // PASSA A SER FEITO PELO SERVI�O
                //if ((this.Contrato_Status.Equals("I"))
                //    && (smwi.CurrentStateName.ToUpper() == "INAPPROVAL"))
                //{
                //    UpdateContratoStatusValid();
                //}

                //Session["LastState"] = smwi.CurrentStateName.ToUpper();

                Assembly assembly = instance.GetWorkflowDefinition().GetType().Assembly;
                Type type = assembly.GetType("SpiritucSI.Unilever.SRA.WF.WFEvents");

                //ExternalDataExchangeService dataService = new ExternalDataExchangeService();
                ExternalDataExchangeService s = (ExternalDataExchangeService)this.WFRuntime.GetService(typeof(ExternalDataExchangeService));
                if (s != null && type != null && s.GetService(type) == null)
                {
                    s.AddService(assembly.CreateInstance(type.FullName));
                }
                if (type == null)
                    type = typeof(WFEvents);

                wfActions = new List<WFAction>();
                foreach (var queue in queues)
                {
                    EventQueueName queueName = queue.QueueName as EventQueueName;

                    if (queueName != null)
                    {
                        var handler = (from EventInfo ei in type.GetEvents()
                                       where ei.Name == queueName.MethodName
                                       select ei).FirstOrDefault();

                        var objEventActivity = (from EventDrivenActivity obj in
                                                    (from obj2 in smwi.CurrentState.EnabledActivities where obj2 is EventDrivenActivity select obj2).ToList()
                                                where obj.EventActivity is HandleExternalEventActivity && ((HandleExternalEventActivity)obj.EventActivity).EventName == handler.Name
                                                select obj).FirstOrDefault();

                        bool isInRole = true;
                        if (((HandleExternalEventActivity)objEventActivity.EventActivity).Roles != null)
                            isInRole = ((HandleExternalEventActivity)objEventActivity.EventActivity).Roles.IncludesIdentity(this.Context.User.Identity.Name);

                        if (objEventActivity != null && objEventActivity.EventActivity != null
                            && objEventActivity.EventActivity is HandleExternalEventActivity
                            && isInRole)
                        {
                            ExternalDataEventArgs args = new ExternalDataEventArgs(_uid);
                            args.WaitForIdle = true;

                            var wfservice = this.WFRuntime.GetService(type);
                            MulticastDelegate eventDelegate =
                               (MulticastDelegate)type.GetField(handler.Name, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)
                               .GetValue(wfservice);
                            Delegate[] delegates = eventDelegate.GetInvocationList();

                            foreach (Delegate dlg in delegates)
                            {
                                WFAction wfAction = new WFAction();
                                wfAction.Deleg = dlg;
                                wfAction.Args = args;
                                wfAction.Servs = new WFEvents();
                                wfAction.Id = Guid.NewGuid();
                                wfAction.Text = objEventActivity.Description;

                                wfActions.Add(wfAction);
                                this.RepeaterWFActions.Visible = true;
                            }
                        }
                    }
                }

                this.RepeaterWFActions.DataSource = wfActions;
                this.RepeaterWFActions.DataBind();
            }
        }
        catch (Exception ex)
        {
            UpdateButtonsVisibility("");
            //Logg Exception || render reset wf button
        }
    }

    //conv�m ser aqui para haver um update qd o WF muda de estado (dado q h� 2 chamadas da f.)
    private void UpdateButtonsVisibility(string wfState)
    {
        switch (wfState)
        {
            case "DRAFT":
                C_btn_Editar.Visible = true;
                Botoes_btSubstituirCanc.Visible = false;
                box_dataFimEfectivo.Visible = false;
                Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "RENEWALDRAFT":
                C_btn_Editar.Visible = true;
                Botoes_btSubstituirCanc.Visible = false;
                box_dataFimEfectivo.Visible = false;
                Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "DELEGATED":
                C_btn_Editar.Visible = true;
                Botoes_btSubstituirCanc.Visible = false;
                box_dataFimEfectivo.Visible = false;
                Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "PRINTED":
                C_btn_Editar.Visible = true;
                Botoes_btSubstituirCanc.Visible = false;
                box_dataFimEfectivo.Visible = false;
                Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "RECEIVED":
                C_btn_Editar.Visible = false;
                Botoes_btSubstituirCanc.Visible = false;
                box_dataFimEfectivo.Visible = false;
                Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "ERRORVERIFICATION":
                C_btn_Editar.Visible = true;
                Botoes_btSubstituirCanc.Visible = false;
                box_dataFimEfectivo.Visible = false;
                Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "INAPPROVAL":
                C_btn_Editar.Visible = false;
                //Botoes_btSubstituirCanc.Visible = false;
                //box_dataFimEfectivo.Visible = false;
                //Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "MANAGERAPPROVAL":
                C_btn_Editar.Visible = false;
                //Botoes_btSubstituirCanc.Visible = false;
                //box_dataFimEfectivo.Visible = false;
                //Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "SALESAPPROVAL":
                C_btn_Editar.Visible = false;
                //Botoes_btSubstituirCanc.Visible = false;
                //box_dataFimEfectivo.Visible = false;
                //Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "FINANTIALAPPROVAL":
                C_btn_Editar.Visible = false;
                //Botoes_btSubstituirCanc.Visible = false;
                //box_dataFimEfectivo.Visible = false;
                //Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "TOPAPPROVAL":
                C_btn_Editar.Visible = false;
                //Botoes_btSubstituirCanc.Visible = false;
                //box_dataFimEfectivo.Visible = false;
                //Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            case "APPROVED":
                C_btn_Editar.Visible = false;
                //Botoes_btSubstituirCanc.Visible = true;
                //box_dataFimEfectivo.Visible = true;
                //Botoes_btTerminar.Visible = true;
                Botoes_btValidar.Visible = false;
                break;
            case "REJECTED":
                C_btn_Editar.Visible = false;
                Botoes_btSubstituirCanc.Visible = false;
                box_dataFimEfectivo.Visible = false;
                Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
            default: //ANTES DO CONTRATO SER INSERIDO
                C_btn_Editar.Visible = false;
                //Botoes_btSubstituirCanc.Visible = false;
                //box_dataFimEfectivo.Visible = false;
                //Botoes_btTerminar.Visible = false;
                Botoes_btValidar.Visible = false;
                break;
        }
        
        if (Roles.IsUserInRole(User.Identity.Name, "SMI"))
        {
           C_btn_Editar.Visible = true;
           //Botoes_btValidar.Visible = !this.Contrato_Status.Equals("V") ;
           try
           {
               string guid = Session["uid"] != null ? Session["uid"].ToString() :"" ;
               WFRuntime.GetWorkflow(new Guid(guid));
           }
           catch
           {
            Botoes_btValidar.Visible = !this.Contrato_Status.Equals("V");
            Botoes_btTerminar.Visible = !Botoes_btValidar.Visible;
           }

        }
         else if (Session["uid"] == null || string.IsNullOrEmpty(Session["uid"].ToString()))
         {
             C_btn_Editar.Visible = false;
             Botoes_btValidar.Visible = false;
             Botoes_btTerminar.Visible = false;
         }
         else
         {
             bool myIsAuthorized = this.IsAuthorized();
             C_btn_Editar.Visible = (C_btn_Editar.Visible && !myIsAuthorized);
             Botoes_btTerminar.Visible = false;
         }
    }

    protected bool IsAuthorized()
    {
        SRADBInterface.SRA DBsra = new SRADBInterface.SRA(ConfigurationSettings.AppSettings["DBCS"].ToString());
        return DBsra.IsContratoAuthorized(new Guid(Session["uid"].ToString()));
    }
    //Show WF Info  

    public void ShowWFInfo(string msg)
    {
        this._M_ShowError(this.lblWarning, msg, null, true, this.Page.Request.Url.AbsoluteUri);
    }

    #region CONTROL EVENTS WF
    protected void C_Rpt_WFActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Button btn = (Button)e.Item.FindControl("ButtonWFAction");
            btn.Visible = !this.showInstructions;
            btn.CommandArgument = ((SpiritucSI.Unilever.SRA.WF.WFAction)e.Item.DataItem).Id.ToString();
            btn.Text = ((SpiritucSI.Unilever.SRA.WF.WFAction)e.Item.DataItem).Text;
        }
    }

    protected void C_Rpt_WFActions_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        WFAction wfAction = (from WFAction obj in this.wfActions
                             where obj.Id == new Guid(e.CommandArgument.ToString())
                             select obj).FirstOrDefault();
        if (wfAction != null)
        {
            try
            {
                wfAction.Args.Identity = this.Context.User.Identity.Name;
                wfAction.Deleg.Method.Invoke(wfAction.Deleg.Target, new object[] { wfAction.Servs, wfAction.Args });

                this.RunWorkflow(wfAction.Args.InstanceId);

                RenderWFActions();
                //Response.Redirect("#");
                //((FrontOffice)this.Page.Master).ShowMessage("O processo foi actualizado com sucesso", null, SpiritucSI.Unilever.SRA.WF.WebSite.UserControls.Common.ResponseMsgPanel.MessageType.Success);
            }

            catch (WFProceedConditions ex)
            {
                if (ex.Message.Contains("EXCOD_01"))
                    this.ShowWFInfo("O processo ira prosseguir apesar de n�o estar autorizado");
                else
                {
                    if (ex.Message.Contains("EXCOD_02"))
                        this.ShowWFInfo("O processo n�o pode ser rejeitado por j� estar autorizado");
                    else
                        throw;
                }
            }

            catch (Exception ex)
            {
                throw;
                //if (ExceptionPolicy.HandleException(ex, "LogOnly"))
                //{ throw; }

                //((Master)this.Page.Master).ShowMessage(this.GetGlobalResourceObject("Messages", "ErrorMessage").ToString(), ex, MessageType.ErrorBox);
            }
            finally
            {
                //this.Load();
            }
        }
    }
    #endregion
    #endregion
}
