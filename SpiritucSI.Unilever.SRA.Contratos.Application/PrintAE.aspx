﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintAE.aspx.cs" Inherits="PrintAE" Theme="SRA" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body class="Print">

    <script language="javascript" type="text/javascript">window.print();</script>

    <form id="form1" runat="server">
    <table width="20 cm" style="width: 20 cm; background-color: White;" cellpadding="0" cellspacing="0" border="0">
        <%-- HEADER --%>
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <%-- NAME --%>
                        <td align="left" valign="top" style="width: 1%;">
                            <asp:Image ID="C_Header_img_Logo" runat="server" meta:resourcekey="C_Header_img_Logo" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <%-- ADDRESS --%>
                        <td valign="top">
                            <asp:Label ID="C_Header_lbl_Name" runat="server" class="tituloGrande" meta:resourcekey="C_Header_lbl_Name" /><br />
                            <asp:Label ID="C_Header_lbl_Address" runat="server" Style="font-weight: normal" meta:resourcekey="C_Header_lbl_Address" /><br />
                            <asp:Label ID="C_Header_lbl_Phone" runat="server" Style="font-weight: normal" meta:resourcekey="C_Header_lbl_Phone" /><br />
                            <br />
                            <p>
                                <asp:Label ID="C_Header_lbl_Info" runat="server" Style="font-size: 5pt;" meta:resourcekey="C_Header_lbl_Info" />
                            </p>
                        </td>
                        <%-- PROPOSTA --%>
                        <td align="right" valign="top">
                            <table id="C_box_proposta" runat="server" cellpadding="0" cellspacing="0" border="0" style="border: solid 2px black;">
                                <%-- HEADERS --%>
                                <tr>
                                    <td colspan="4" style="border-bottom: solid 2px black;" align="center">
                                        <asp:Label ID="C_Proposta_lbl_Header" runat="server" meta:resourcekey="C_Proposta_lbl_Header" CssClass="titulo" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-right: solid 2px black; border-bottom: solid 2px black;" align="center">
                                        &nbsp;<asp:Label ID="C_Proposta_lbl_Emissor" runat="server" meta:resourcekey="C_Proposta_lbl_Emissor"
                                            CssClass="label" />&nbsp;
                                    </td>
                                    <td style="border-right: solid 2px black; border-bottom: solid 2px black;" align="center">
                                        &nbsp;<asp:Label ID="C_Proposta_lbl_Ano" runat="server" meta:resourcekey="C_Proposta_lbl_Ano" CssClass="label" />&nbsp;
                                    </td>
                                    <td style="border-right: solid 2px black; border-bottom: solid 2px black;" align="center">
                                        &nbsp;<asp:Label ID="C_Proposta_lbl_Serie" runat="server" meta:resourcekey="C_Proposta_lbl_Serie" CssClass="label" />&nbsp;
                                    </td>
                                    <td style="border-bottom: solid 2px black;" align="center">
                                        &nbsp;<asp:Label ID="C_Proposta_lbl_Number" runat="server" meta:resourcekey="C_Proposta_lbl_Number" CssClass="label" />&nbsp;
                                    </td>
                                </tr>
                                <%-- Nº PROPOSTA --%>
                                <tr>
                                    <%-- EMISSOR --%>
                                    <td style="border-right: solid 2px black;" align="center">
                                        &nbsp;<asp:Label ID="C_Proposta_txt_Emissor" runat="server" CssClass="textboxBig"></asp:Label>&nbsp;
                                    </td>
                                    <%-- ANO --%>
                                    <td style="border-right: solid 2px black; border-bottom: solid 2px black;" align="center">
                                        &nbsp;<asp:Label ID="C_Proposta_txt_Ano" runat="server" CssClass="textboxBig"></asp:Label>&nbsp;
                                    </td>
                                    <%-- SÉRIE --%>
                                    <td style="border-right: solid 2px black; border-bottom: solid 2px black;" align="center">
                                        &nbsp;<asp:Label ID="C_Proposta_txt_Serie" runat="server" CssClass="textboxBig"></asp:Label>&nbsp;
                                    </td>
                                    <%-- NUMERO --%>
                                    <td style="border-bottom: solid 2px black;" align="center">
                                        &nbsp;<asp:Label ID="C_Proposta_txt_Numero" runat="server" CssClass="textboxBig"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <%-- EMISSOR --%>
                                <tr>
                                    <td colspan="4" style="border-bottom: solid 2px black;">
                                        &nbsp;&nbsp;<asp:Label ID="C_Proposta_txt_EmissorName" runat="server" CssClass="titulo_peq"></asp:Label>
                                    </td>
                                </tr>
                                <%-- RECEPCAO --%>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Label ID="C_Recepcao_lbl_Header" runat="server" meta:resourcekey="C_Recepcao_lbl_Header" CssClass="titulo" />
                                    </td>
                                </tr>
                                <%-- CUSTOMER SERVICE --%>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="C_Recepcao_txt_Name" runat="server" CssClass="titulo_peq"></asp:Label>
                                                </td>
                                                <td align="right">
                                                    &nbsp;&nbsp;<asp:Label ID="C_Recepcao_txt_Data" runat="server" CssClass="titulo_peq" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%-- TITULO --%>
        <tr>
            <td align="center">
                <asp:Label ID="C_Title_lbl" runat="server" CssClass="tituloMenu" meta:resourcekey="C_Title_lbl" />
            </td>
        </tr>
    </table>
    <table id="C_box_Content" runat="server" width="20 cm" style="width: 20 cm; background-color: White;"
        cellpadding="0" cellspacing="0" border="0">
        <%-- NIVEL CONTRATO --%>
        <tr>
            <td style="padding-top: 5px;">
                <asp:Label ID="C_Nivel_lbl_header" runat="server" class="titulo" meta:resourcekey="C_Nivel_lbl_header" />
            </td>
        </tr>
        <tr>
            <td style="border: solid 1px black; padding: 3px;">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                    <%-- GRUPO ECONÓMICO --%>
                    <tr>
                        <td width="1%" style="border: solid 1px black; padding: 1px;" align="center" valign="middle">
                            <asp:Image ID="C_Nivel_img_GE" Height="10px" runat="server" meta:resourcekey="C_img_Cross" />
                        </td>
                        <td width="140px">
                            &nbsp;&nbsp;<asp:Label ID="C_Nivel_lbl_GE" runat="server" CssClass="label" meta:resourcekey="C_Nivel_lbl_GE" />
                        </td>
                        <td class="textbox">
                            <asp:Label ID="C_Nivel_txt_GE" runat="server" CssClass="textbox"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 1pt;">
                            &nbsp;
                        </td>
                    </tr>
                    <%-- SUBGRUPO ECONÓMICO --%>
                    <tr>
                        <td width="1%" style="border: solid 1px black; padding: 1px;" align="center" valign="middle">
                            <asp:Image ID="C_Nivel_img_SGE" Height="10px" runat="server" meta:resourcekey="C_img_Cross" />
                        </td>
                        <td width="140px">
                            &nbsp;&nbsp;<asp:Label ID="C_Nivel_lbl_SGE" runat="server" CssClass="label" meta:resourcekey="C_Nivel_lbl_SGE" />
                        </td>
                        <td class="textbox">
                            <asp:Label ID="C_Nivel_txt_SGE" runat="server" CssClass="textbox"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 1pt;">
                            &nbsp;
                        </td>
                    </tr>
                    <%-- LES --%>
                    <tr>
                        <td width="1%" style="border: solid 1px black; padding: 1px;" align="center" valign="middle">
                            <asp:Image ID="C_Nivel_img_LE" Height="10px" runat="server" meta:resourcekey="C_img_Cross" />
                        </td>
                        <td width="140px">
                            &nbsp;&nbsp;<asp:Label ID="C_Nivel_lbl_LE" runat="server" CssClass="label" meta:resourcekey="C_Nivel_lbl_LE" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%-- LOCAIS ENTREGA --%>
        <tr>
            <td style="padding-top: 5px;">
                <asp:Label ID="C_LEs_lbl_header" runat="server" class="titulo" meta:resourcekey="C_LEs_lbl_header" />
            </td>
        </tr>
        <tr>
            <td style="border: solid 1px black; padding: 3px;">
                <asp:DataGrid ID="C_dg_Entidades" runat="server" BorderStyle="None" GridLines="None" CellSpacing="0"
                    AllowPaging="false" Width="100%" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundColumn DataField="DES_GRUPO_ECONOMICO_LE" HeaderText="&nbsp;GRUPO&nbsp;" SortExpression="DES_GRUPO_ECONOMICO_LE">
                            <ItemStyle CssClass="textbox PrintListRow" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DES_SUBGRUPO_ECONOMICO_LE" HeaderText="&nbsp;SUB GRUPO&nbsp;" SortExpression="DES_SUBGRUPO_ECONOMICO_LE">
                            <ItemStyle CssClass="textbox PrintListRow" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DES_CONCESSIONARIO" HeaderText="&nbsp;CONCESSIONÁRIO&nbsp;" SortExpression="DES_CONCESSIONARIO">
                            <ItemStyle CssClass="textbox PrintListRow" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DES_CLIENTE" HeaderText="&nbsp;CLIENTE&nbsp;" SortExpression="DES_CLIENTE">
                            <ItemStyle CssClass="textbox PrintListRow" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="COD_LOCAL_ENTREGA" HeaderText="&nbsp;CÓDIGO&nbsp;LE&nbsp;" SortExpression="COD_LOCAL_ENTREGA">
                            <ItemStyle CssClass="textbox PrintListRow" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DES_LOCAL_ENTREGA" HeaderText="&nbsp;LOCAL&nbsp;DE&nbsp;ENTREGA&nbsp;" SortExpression="DES_LOCAL_ENTREGA">
                            <ItemStyle CssClass="textbox PrintListRow" />
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <%-- PONTO VENDA --%>
        <tr>
            <td style="padding-top: 5px;">
                <asp:Label ID="C_PontoVenda_lbl_header" runat="server" class="titulo" meta:resourcekey="C_PontoVenda_lbl_header" />
            </td>
        </tr>
        <tr>
            <td style="border: solid 1px black; padding: 3px;">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                    <%-- NOME --%>
                    <tr>
                        <td align="right" width="1%">
                            <asp:Label ID="C_PontoVenda_lbl_Nome" runat="server" CssClass="label" meta:resourcekey="C_PontoVenda_lbl_Nome" />&nbsp;&nbsp;
                        </td>
                        <td class="textbox" colspan="3">
                            <asp:Label ID="C_PontoVenda_txt_Nome" runat="server" CssClass="textbox"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 1pt;">
                            &nbsp;
                        </td>
                    </tr>
                    <%-- MORADA --%>
                    <tr>
                        <td align="right">
                            <asp:Label ID="C_PontoVenda_lbl_Morada" runat="server" CssClass="label" meta:resourcekey="C_PontoVenda_lbl_Morada" />&nbsp;&nbsp;
                        </td>
                        <td class="textbox" colspan="3">
                            <asp:Label ID="C_PontoVenda_txt_Morada" runat="server" CssClass="textbox"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 1pt;">
                            &nbsp;
                        </td>
                    </tr>
                    <%-- LOCALIDADE / CONTRIBUINTE--%>
                    <tr>
                        <td align="right">
                            <asp:Label ID="C_PontoVenda_lbl_Localidade" runat="server" CssClass="label" meta:resourcekey="C_PontoVenda_lbl_Localidade" />&nbsp;&nbsp;
                        </td>
                        <td class="textbox">
                            <asp:Label ID="C_PontoVenda_txt_Localidade" runat="server" CssClass="textbox"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Label ID="C_PontoVenda_lbl_Contribuinte" runat="server" CssClass="label" meta:resourcekey="C_PontoVenda_lbl_Contribuinte" />&nbsp;&nbsp;
                        </td>
                        <td class="textbox">
                            <asp:Label ID="C_PontoVenda_txt_Conribuinte" runat="server" CssClass="textbox"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%-- ABASTECIMENTO --%>
        <tr>
            <td style="padding-top: 5px;">
                <asp:Label ID="C_Abastecimento_lbl_header" runat="server" class="titulo" meta:resourcekey="C_Abastecimento_lbl_header" />
            </td>
        </tr>
        <tr>
            <td style="border: solid 1px black; padding: 3px;">
                <asp:DataGrid ID="C_Abastecimento_dg_Lista" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    GridLines="None" CellSpacing="0" Width="100%">
                    <Columns>
                        <%-- CONCESSIONARIO --%>
                        <asp:TemplateColumn HeaderText="CONCESSIONÁRIO">
                            <ItemStyle CssClass="textbox PrintListRow" Width="25%" />
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "COD_CONCESSIONARIO").ToString() + " - " + DataBinder.Eval(Container.DataItem, "DES_CONCESSIONARIO")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%-- FACTURACAO --%>
                        <asp:TemplateColumn HeaderText="FACTURAÇÃO&nbsp;">
                            <ItemStyle HorizontalAlign="Center" Width="1%" CssClass="textbox PrintListRow" />
                            <ItemTemplate>
                                <asp:Image ID="C_Abastecimento_img_Facturacao" Height="10px" runat="server" meta:resourcekey="C_img_Cross"
                                    Visible='<%# DataBinder.Eval(Container.DataItem,"FACTURACAO").ToString() == "Y" %>' />&nbsp;
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%-- DISTRIBUIÇÃO --%>
                        <asp:TemplateColumn HeaderText="&nbsp;DISTRIBUIÇÃO">
                            <ItemStyle CssClass="textbox PrintListRow" HorizontalAlign="Center" Width="1%" />
                            <ItemTemplate>
                                <asp:Image ID="C_Abastecimento_img_Distribuicao" Height="10px" runat="server" meta:resourcekey="C_img_Cross"
                                    Visible='<%# DataBinder.Eval(Container.DataItem,"DISTRIBUICAO").ToString() == "Y" %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%-- OBSERVAÇÕES --%>
                        <asp:TemplateColumn HeaderText="OBSERVAÇÕES" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle CssClass="textbox PrintListRow" Width="65%" />
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "OBSERVACOES")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <%-- TIPO DE ACTIVIDADE / DEBITOS --%>
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <%-- TIPO DE ACTIVIDADE --%>
                        <td width="60%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding-top: 5px;">
                                        <table w>
                                        </table>
                                        <asp:Label ID="C_Actividade_lbl_header" runat="server" class="titulo" meta:resourcekey="C_Actividade_lbl_header" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: solid 1px black; padding: 3px;">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="C_Actividade_dg_Lista" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                        GridLines="None" CellSpacing="0" Width="100%">
                                                        <Columns>
                                                            <%-- TIPO ACTIVIDADE --%>
                                                            <asp:TemplateColumn HeaderText="TIPO&nbsp;DE&nbsp;ACTIVIDADE">
                                                                <ItemStyle CssClass="textbox PrintListRow" Width="33%" />
                                                                <ItemTemplate>
                                                                    <%# DataBinder.Eval(Container.DataItem, "NOME_TIPO_ACTIVIDADE")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%-- ACORDO --%>
                                                            <asp:TemplateColumn HeaderText="Nº&nbsp;ACORDO" HeaderStyle-HorizontalAlign="Right">
                                                                <ItemStyle CssClass="textbox PrintListRow" Width="33%" HorizontalAlign="Right" />
                                                                <ItemTemplate>
                                                                    <%# DataBinder.Eval(Container.DataItem, "NUM_ACORDO")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%-- VALOR --%>
                                                            <asp:TemplateColumn HeaderText="VALOR" HeaderStyle-HorizontalAlign="Right">
                                                                <ItemStyle CssClass="textbox PrintListRow" Width="33%" HorizontalAlign="Right" />
                                                                <ItemTemplate>
                                                                    <%# double.Parse(DataBinder.Eval(Container.DataItem, "VALOR_ACORDO").ToString()).ToString("#0.00") + "&nbsp;€"%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="C_Actividade_lbl_Total" runat="server" meta:resourcekey="C_Actividade_lbl_Total" CssClass="titulo_peq"></asp:Label>
                                                    <asp:Label ID="C_Actividade_txt_Total" runat="server" CssClass="titulo_peq"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <%-- DEBITOS --%>
                        <td width="40%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding-top: 5px;">
                                        <asp:Label ID="C_Debitos_lbl_header" runat="server" class="titulo" meta:resourcekey="C_Debitos_lbl_header" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: solid 1px black; padding: 3px;">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="C_Debito_dg_Lista" runat="server" AutoGenerateColumns="False" BorderStyle="None" GridLines="None"
                                                        CellSpacing="0" Width="100%">
                                                        <Columns>
                                                            <%-- DATA --%>
                                                            <asp:TemplateColumn HeaderText="DATA">
                                                                <ItemStyle CssClass="textbox PrintListRow" Width="33%" />
                                                                <ItemTemplate>
                                                                    <%# ((DateTime)DataBinder.Eval(Container.DataItem, "DATA_DEBITO")).ToString("dd-MM-yyyy")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%-- VALOR --%>
                                                            <asp:TemplateColumn HeaderText="VALOR" HeaderStyle-HorizontalAlign="Right">
                                                                <ItemStyle CssClass="textbox PrintListRow" Width="33%" HorizontalAlign="Right" />
                                                                <ItemTemplate>
                                                                    <%# double.Parse(DataBinder.Eval(Container.DataItem, "VALOR_DEBITO").ToString()).ToString("#0.00") + "&nbsp;€"%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="C_Debitos_lbl_Total" runat="server" meta:resourcekey="C_Debitos_lbl_Total" CssClass="titulo_peq"></asp:Label>
                                                    <asp:Label ID="C_Debitos_txt_Total" runat="server" CssClass="titulo_peq"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="C_Debitos_lbl_TotalResidual" runat="server" meta:resourcekey="C_Debitos_lbl_TotalResidual"
                                                        CssClass="titulo_peq"></asp:Label>
                                                    <asp:Label ID="C_Debitos_txt_TotalResidual" runat="server" CssClass="titulo_peq"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%-- TPR --%>
        <tr>
            <td style="padding-top: 5px;">
                <asp:Label ID="C_TPR_lbl_header" runat="server" class="titulo" meta:resourcekey="C_TPR_lbl_header" />
            </td>
        </tr>
        <tr>
            <td style="border: solid 1px black; padding: 3px;">
                <asp:DataGrid ID="C_TPR_dg_Lista" runat="server" AutoGenerateColumns="False" ShowFooter="true" BorderStyle="None"
                    GridLines="None" CellSpacing="0" Width="100%">
                    <Columns>
                        <%-- COD_PRODUTO --%>
                        <asp:TemplateColumn HeaderText="&nbsp;CÓDIGO&nbsp;" HeaderStyle-CssClass="PrintListcolumn">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" />
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "COD_PRODUTO")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%-- NOME --%>
                        <asp:TemplateColumn HeaderText="&nbsp;PRODUTO&nbsp;" HeaderStyle-CssClass="PrintListcolumn" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" />
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "NOME_PRODUTO")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%-- BUDGET --%>
                        <asp:TemplateColumn HeaderText="&nbsp;BUDGET&nbsp;" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="PrintListcolumn">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" />
                            <ItemTemplate>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="right" valign="top" class="textbox" style="font-size: 6pt;">VENDAS:</td>
                                        <td align="right" valign="top" class="textbox" style="font-size: 6pt;">
                                            <%# double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_VENDAS").ToString()).ToString("#0.00") + "%"%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top" class="textbox" style="font-size: 6pt;">MARKETING:</td>
                                        <td align="right" valign="top" class="textbox" style="font-size: 6pt;">
                                            <%# double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_MARKETING").ToString()).ToString("#0.00") + "%"%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%-- PERIODO ACTIVIDADE --%>
                        <asp:TemplateColumn HeaderText="PERÍODO ACTIVIDADE" HeaderStyle-CssClass="PrintListcolumn"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top" align="right" class="textbox" style="font-size: 6pt;">
                                            DE:<%# ((DateTime)DataBinder.Eval(Container.DataItem, "ACTIVIDADE_INICIO")).ToString("dd-MM-yyyy")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="right" class="textbox" style="font-size: 6pt;">
                                            A:<%# ((DateTime)DataBinder.Eval(Container.DataItem, "ACTIVIDADE_FIM")).ToString("dd-MM-yyyy")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%-- PERIODO COMPRAS --%>
                        <asp:TemplateColumn HeaderText="PERÍODO COMPRAS"
                            HeaderStyle-CssClass="PrintListcolumn" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" HorizontalAlign="Right" />
                            <FooterStyle CssClass="titulo_peq PrintListcolumn" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top" align="right" class="textbox" style="font-size: 6pt;">
                                            DE:<%# ((DateTime)DataBinder.Eval(Container.DataItem, "COMPRAS_INICIO")).ToString("dd-MM-yyyy")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="right" class="textbox" style="font-size: 6pt;">
                                            A:<%# ((DateTime)DataBinder.Eval(Container.DataItem, "COMPRAS_FIM")).ToString("dd-MM-yyyy")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <FooterTemplate>
                                TOTAL:
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <%-- PVP --%>
                        <asp:TemplateColumn HeaderText="&nbsp;PVP&nbsp;" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="PrintListcolumn">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" HorizontalAlign="Right" />
                            <FooterStyle CssClass="label PrintListcolumn" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%# double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString()).ToString("#0.00") + "&nbsp;€"%>
                            </ItemTemplate>
                            <FooterTemplate>
                               <%# this.PVP.ToString("#0.00") + "&nbsp;€"%>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <%-- UNIDADE --%>
                        <asp:TemplateColumn HeaderText="&nbsp;UN.&nbsp;" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="PrintListcolumn">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "UNIDADE_BASE")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%-- PREVISÃO UN--%>
                        <asp:TemplateColumn HeaderText="&nbsp;PREVISÃO&nbsp;<br>(UN)" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="PrintListcolumn">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" HorizontalAlign="Right" />
                            <FooterStyle CssClass="label PrintListcolumn" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%# double.Parse(DataBinder.Eval(Container.DataItem, "PREVISAO_QUANTIDADE").ToString()).ToString("#0.00")%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <%# this.QTD.ToString("#0.00")%>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <%-- PREVISÃO VALOR --%>
                        <asp:TemplateColumn HeaderText="PREVISÃO<br>(VALOR)">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" HorizontalAlign="Right" />
                           <FooterStyle CssClass="label PrintListcolumn" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%# Math.Round(double.Parse(DataBinder.Eval(Container.DataItem,"PREVISAO_QUANTIDADE").ToString())
                                                              * double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString()), 3).ToString("#0.00") + "&nbsp;€"
                                %>
                            </ItemTemplate>
                            <FooterTemplate>
                                <%# this.Valor.ToString("#0.00") + "&nbsp;€"%>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <%-- SUB TOTAL VENDAS --%>
                        <asp:TemplateColumn HeaderText="&nbsp;SUBTOTAL&nbsp;<br>VENDAS" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="PrintListcolumn">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" HorizontalAlign="Right" />
                            <FooterStyle CssClass="label PrintListcolumn" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%# Math.Round(double.Parse(DataBinder.Eval(Container.DataItem, "PREVISAO_QUANTIDADE").ToString())
                                                              * double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString())
                                                                                                                                                                                              * (double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_VENDAS").ToString()) / 100), 3).ToString("#0.00") + "&nbsp;€"
                                %>
                            </ItemTemplate>
                            <FooterTemplate>
                                <%# this.Vendas.ToString("#0.00") + "&nbsp;€"%>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <%-- SUB TOTAL MARKETING --%>
                        <asp:TemplateColumn HeaderText="&nbsp;SUBTOTAL&nbsp;<br>MARKETING" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="PrintListcolumn">
                            <ItemStyle CssClass="textbox PrintListRow PrintListcolumn" HorizontalAlign="Right" />
                            <FooterStyle CssClass="label PrintListcolumn" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%# Math.Round(double.Parse(DataBinder.Eval(Container.DataItem, "PREVISAO_QUANTIDADE").ToString())
                                                              * double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString())
                                                                                                                                                                                              * (double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_MARKETING").ToString()) / 100), 3).ToString("#0.00") + "&nbsp;€"
                                %>
                            </ItemTemplate>
                            <FooterTemplate>
                                <%# this.Marketing.ToString("#0.00")+"&nbsp;€"%>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <%-- TOTAL --%>
                        <asp:TemplateColumn HeaderText="&nbsp;TOTAL&nbsp;" HeaderStyle-HorizontalAlign="Right">
                            <ItemStyle CssClass="textbox PrintListRow" HorizontalAlign="Right" />
                            <FooterStyle CssClass="label" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%# Math.Round(double.Parse(DataBinder.Eval(Container.DataItem, "PREVISAO_QUANTIDADE").ToString())
                                                              * double.Parse(DataBinder.Eval(Container.DataItem, "PVP").ToString())
                                                                                                                                                                                              * ((double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_VENDAS").ToString()) + double.Parse(DataBinder.Eval(Container.DataItem, "BUDGET_MARKETING").ToString())) / 100), 3).ToString("#0.00") + "&nbsp;€"
                                %>
                            </ItemTemplate>
                            <FooterTemplate>
                                <%# this.Total.ToString("#0.00") + "&nbsp;€"%>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <%-- OBSERVAÇÕES --%>
        <tr>
            <td style="padding-top: 5px;">
                <asp:Label ID="C_Observacoes_lbl_header" runat="server" class="titulo" meta:resourcekey="C_Observacoes_lbl_header" />
            </td>
        </tr>
        <tr>
            <td style="border: solid 1px black; padding: 3px;">
                <asp:Label ID="C_Observacoes_txt_header" runat="server" class="label" meta:resourcekey="C_TPR_lbl_header" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
