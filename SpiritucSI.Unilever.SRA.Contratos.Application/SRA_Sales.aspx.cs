﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SpiritucSI.Unilever.SRA.Business.Budget;

public partial class SRA_Sales : SuperPage
{
    #region PROPERTIES
    bool Entidades_SortASC;
    #endregion

    #region PAGE_EVENTS
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["Sales_SORT"] = null;

        if (Request.QueryString["idContrato"] == null)
        {
            Response.Write("Erro a passar parâmetros!!!");
            return;
        }

        if (IsPostBack)
        {
            //this.Entidades_SortASC = Convert.ToBoolean(Session["Entidades_SortASC"]);
            this.lblError.Text = "";
            this.lblWarning.Text = "";
            this.lblError.ToolTip = "";
            this.lblWarning.ToolTip = "";
        }
        else
        {
            //Session["Entidades_SortASC"] = true;
            //this.Entidades_SortASC = true;
            LoadDataGrid();
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
    }
    #endregion

    #region CONTROL_EVENTS
    protected void LoadDataGrid()
    {
        //m = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        //roleDescriptions = m.Roles_Get();

        //SpiritucSI.Unilever.SRA.Business.Budget.Budget b = new SpiritucSI.Unilever.SRA.Business.Budget.Budget(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        //DataTable source = b.ZoneBudget_Get();
        //source.Columns.Add("RoleName_DESC");
        //foreach (DataRow row in source.Rows)
        //{
        //    row["RoleName_DESC"] = GetZoneDescription(row["RoleName"].ToString());
        //}
        //if (Session["Sales_SORT"]!=null)
        //{
        //    source.DefaultView.Sort = Session["Sales_SORT"].ToString();
        //}
        SRADBInterface.SRA DBsra = new SRADBInterface.SRA(ConfigurationManager.AppSettings["DBCS"]);
        int idContrato = 0;
        int.TryParse(Request.QueryString["idContrato"], out idContrato);
        if (idContrato > 0)
        {
            DataTable source = DBsra.getSalesHistory(idContrato);
            C_dg_Entidades.DataSource = source.DefaultView;
            C_dg_Entidades.DataBind();
        }
    }

    protected void C_dg_Entidades_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        //Budget b = new Budget(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        //double total=0, captured=0, authorized=0;
        //int id, year;
        //string role;
        //switch (e.CommandName)
        //{
        //    case "Delete":
        //        id = int.Parse(((Label)e.Item.FindControl("lblID")).Text);
        //        b.DeleteBudget(id);
        //        this.LoadDataGrid();
        //        break;
        //    case "Edit":
        //        this.C_dg_Entidades.EditItemIndex = e.Item.ItemIndex;
        //        this.LoadDataGrid();
        //        break;
        //    case "Inserir":
        //        role = ((DropDownList)e.Item.FindControl("Entidade_lb")).SelectedItem.Value.ToString();
        //        year = int.Parse(((TextBox)e.Item.FindControl("Entidade_txtAno")).Text);
        //        double.TryParse(((TextBox)e.Item.FindControl("txb_total")).Text, out total);
        //        double.TryParse(((TextBox)e.Item.FindControl("txb_captured")).Text, out captured);
        //        double.TryParse(((TextBox)e.Item.FindControl("txb_authorized")).Text, out authorized);
        //        b.InsertBudget(role, year, total, captured, authorized);
        //        this.LoadDataGrid();
        //        break;
        //    case "Save":
        //        id = int.Parse(((Label)e.Item.FindControl("lblid")).Text);
        //        double.TryParse(((TextBox)e.Item.FindControl("txb2")).Text, out total);
        //        double.TryParse(((TextBox)e.Item.FindControl("txb3")).Text, out captured);
        //        double.TryParse(((TextBox)e.Item.FindControl("txb4")).Text, out authorized);
        //        b.UpdateBudgetRAW(id, total, captured, authorized);
        //        this.C_dg_Entidades.EditItemIndex = -1;
        //        this.LoadDataGrid();
        //        break;
        //    case "Undo":
        //        this.C_dg_Entidades.EditItemIndex = -1;
        //        this.LoadDataGrid();
        //        break;
        //    default:
        //        break;
        //}
    }

    protected void ddlEntidade_Change(object sender, EventArgs e)
    {
        //DropDownList Material_lbMaterial = (DropDownList)sender;
        //DataGridItem dg = (DataGridItem)Material_lbMaterial.NamingContainer;
        //TextBox Material_txtValor = (TextBox)dg.FindControl("Material_txtValor");
        //if (Material_lbMaterial.SelectedIndex < 0)
        //    Material_lbMaterial.SelectedIndex = 0;
        //string ValueColumn = ConfigurationManager.AppSettings["Material_ValueColumnName"];
        //DataRow mat = this.dtbl_MaterialSource.Rows.Find(Material_lbMaterial.SelectedValue);
        //Material_txtValor.Text = mat[ValueColumn].ToString();
        //Material_txtValor.ReadOnly = mat["VALOR_ALTERAVEL"].Equals("F");
    }

    protected void Entidade_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Footer)
        //{
        //    DropDownList Entidade_lb = (DropDownList)e.Item.FindControl("Entidade_lb");
        //    MyMembership m = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        //    Entidade_lb.DataTextField = "Description";
        //    Entidade_lb.DataValueField = "Name";
        //    DataTable roleDescriptions = m.Roles_Get();
        //    DataTable source = m.GetOrganizationalRoles(Roles.GetAllRoles(), roleDescriptions);
        //    source.DefaultView.Sort = "Description ASC";
        //    Entidade_lb.DataSource = source;
        //    Entidade_lb.DataBind();

        //    //TextBox Material_txtValor = (TextBox)e.Item.FindControl("Material_txtValor");
        //    //Material_lbMaterial.DataSource = this.dtbl_MaterialSource;
        //    //Material_lbMaterial.DataTextField = Convert.ToString(ConfigurationManager.AppSettings["Material_lbTextColumnName"]);
        //    //Material_lbMaterial.DataValueField = Convert.ToString(ConfigurationManager.AppSettings["Material_KeyColumnName"]);
        //    //Material_lbMaterial.DataBind();
        //    //if (Material_lbMaterial.SelectedIndex < 0)
        //    //    Material_lbMaterial.SelectedIndex = 0;
        //    //string ValueColumn = ConfigurationManager.AppSettings["Material_ValueColumnName"];
        //    //DataRow mat = this.dtbl_MaterialSource.Rows.Find(Material_lbMaterial.SelectedValue);
        //    //Material_txtValor.Text = mat[ValueColumn].ToString();
        //    //Material_txtValor.ReadOnly = mat["VALOR_ALTERAVEL"].Equals("F");
        //}
    }

    protected void C_dg_Entidades_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //Session["Sales_SORT"] = e.SortExpression + (this.Entidades_SortASC == true ? " ASC" : " DESC");
        //this.Entidades_SortASC = !this.Entidades_SortASC;
        //Session["Entidades_SortASC"] = this.Entidades_SortASC;
        //this.LoadDataGrid();
    }

    protected void C_dg_Entidades_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //this.C_dg_Entidades.CurrentPageIndex = e.NewPageIndex;
        //this.LoadDataGrid();
    }
  
    #endregion


}

