﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SpiritucSI.Unilever.SRA.Business.Budget;

public partial class UserRoleBudget : SuperPage
{
    #region PROPERTIES
    bool Entidades_SortASC;
    #endregion

    #region PAGE_EVENTS
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["RoleBudget_SORT"] = null;
        if (IsPostBack)
        {
            this.Entidades_SortASC = Convert.ToBoolean(Session["EntidadesRole_SortASC"]);
            this.lblError.Text = "";
            this.lblWarning.Text = "";
            this.lblError.ToolTip = "";
            this.lblWarning.ToolTip = "";
        }
        else
        {
            Session["EntidadesRole_SortASC"] = true;
            this.Entidades_SortASC = true;
            LoadDataGrid();
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
    }
    #endregion

    #region CONTROL_EVENTS
    public string GetZoneDescription(string roleName)
    {
        return roleName;
    }

    protected void LoadDataGrid()
    {
        SpiritucSI.Unilever.SRA.Business.Budget.Budget b = new SpiritucSI.Unilever.SRA.Business.Budget.Budget(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        DataTable source = b.RoleBudget_Get();
        if (Session["RoleBudget_SORT"] != null)
        {
            source.DefaultView.Sort = Session["RoleBudget_SORT"].ToString();
        }
        C_dg_Entidades.DataSource = source.DefaultView; 
        C_dg_Entidades.DataBind();
    }

    protected void C_dg_Entidades_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Budget b = new Budget(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
        double maxPERC=0, contMAX=0, contDUR=0;
        int id, year;
        string role;
        switch (e.CommandName)
        {
            case "Delete":
                id = int.Parse(((Label)e.Item.FindControl("lblID")).Text);
                b.DeleteBudgetMatrix(id);
                this.LoadDataGrid();
                break;
            case "Edit":
                this.C_dg_Entidades.EditItemIndex = e.Item.ItemIndex;
                this.LoadDataGrid();
                break;
            case "Inserir":
                role = ((DropDownList)e.Item.FindControl("Entidade_lb")).SelectedItem.Text;
                year = int.Parse(((TextBox)e.Item.FindControl("Entidade_txtAno")).Text);
                double.TryParse(((TextBox)e.Item.FindControl("txb_maxPERC")).Text, out maxPERC);
                double.TryParse(((TextBox)e.Item.FindControl("txb_contMAX")).Text, out contMAX);
                double.TryParse(((TextBox)e.Item.FindControl("txb_contDUR")).Text, out contDUR);
                b.InsertBudgetMatrix(role, year, maxPERC, contMAX, contDUR);
                this.LoadDataGrid();
                break;
            case "Save":
                id = int.Parse(((Label)e.Item.FindControl("lblid")).Text);
                double.TryParse(((TextBox)e.Item.FindControl("txb2")).Text, out maxPERC);
                double.TryParse(((TextBox)e.Item.FindControl("txb3")).Text, out contMAX);
                double.TryParse(((TextBox)e.Item.FindControl("txb4")).Text, out contDUR);
                b.UpdateBudgetMatrixRAW(id, maxPERC, contMAX, contDUR);
                this.C_dg_Entidades.EditItemIndex = -1;
                this.LoadDataGrid();
                break;
            case "Undo":
                this.C_dg_Entidades.EditItemIndex = -1;
                this.LoadDataGrid();
                break;
            default:
                break;
        }
    }

    protected void ddlEntidade_Change(object sender, EventArgs e)
    {
        //DropDownList Material_lbMaterial = (DropDownList)sender;
        //DataGridItem dg = (DataGridItem)Material_lbMaterial.NamingContainer;
        //TextBox Material_txtValor = (TextBox)dg.FindControl("Material_txtValor");
        //if (Material_lbMaterial.SelectedIndex < 0)
        //    Material_lbMaterial.SelectedIndex = 0;
        //string ValueColumn = ConfigurationManager.AppSettings["Material_ValueColumnName"];
        //DataRow mat = this.dtbl_MaterialSource.Rows.Find(Material_lbMaterial.SelectedValue);
        //Material_txtValor.Text = mat[ValueColumn].ToString();
        //Material_txtValor.ReadOnly = mat["VALOR_ALTERAVEL"].Equals("F");
    }

    protected void Entidade_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            DropDownList Entidade_lb = (DropDownList)e.Item.FindControl("Entidade_lb");
            //MyMembership m = new MyMembership(ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString);
            //Entidade_lb.DataSource = m.GetFunctionalRoles(Roles.GetAllRoles());
            Entidade_lb.Items.Clear();
            Entidade_lb.Items.Add("SalesRep");
            Entidade_lb.Items.Add("Supervisor");
            Entidade_lb.Items.Add("RegManager");
        
            //TextBox Material_txtValor = (TextBox)e.Item.FindControl("Material_txtValor");
            //Material_lbMaterial.DataSource = this.dtbl_MaterialSource;
            //Material_lbMaterial.DataTextField = Convert.ToString(ConfigurationManager.AppSettings["Material_lbTextColumnName"]);
            //Material_lbMaterial.DataValueField = Convert.ToString(ConfigurationManager.AppSettings["Material_KeyColumnName"]);
            //Material_lbMaterial.DataBind();
            //if (Material_lbMaterial.SelectedIndex < 0)
            //    Material_lbMaterial.SelectedIndex = 0;
            //string ValueColumn = ConfigurationManager.AppSettings["Material_ValueColumnName"];
            //DataRow mat = this.dtbl_MaterialSource.Rows.Find(Material_lbMaterial.SelectedValue);
            //Material_txtValor.Text = mat[ValueColumn].ToString();
            //Material_txtValor.ReadOnly = mat["VALOR_ALTERAVEL"].Equals("F");
        }
    }

    protected void C_dg_Entidades_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        Session["RoleBudget_SORT"] = e.SortExpression + (this.Entidades_SortASC == true ? " ASC" : " DESC");
        this.Entidades_SortASC = !this.Entidades_SortASC;
        Session["EntidadesRole_SortASC"] = this.Entidades_SortASC;
        this.LoadDataGrid();
    }
 
    protected void C_dg_Entidades_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        this.C_dg_Entidades.CurrentPageIndex = e.NewPageIndex;
        this.LoadDataGrid();
    }

    #endregion


}

