﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserList.aspx.cs" Inherits="UserList" Theme="SRA"  %>

<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="obout" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="Form1" method="post" runat="server" onsubmit="javascript: return MySubmit();">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnablePartialRendering="true"
        AsyncPostBackTimeout="60000">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px; position: absolute;
                left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF; z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="boxTitulo" runat="server" CssClass="box" meta:resourcekey="boxTituloResource1">
                <UC:Header ID="Header1" runat="server"></UC:Header>
            </asp:Panel>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <asp:Panel ID="boxError" runat="server" CssClass="box" meta:resourcekey="boxErrorResource1">
                <table class="table" align="center">
                    <tr>
                        <td valign="middle" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="errorLabel" meta:resourcekey="lblErrorResource1"></asp:Label><br>
                            <asp:Label ID="lblWarning" runat="server" CssClass="warningLabel" meta:resourcekey="lblWarningResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table width="100%">
                <tr>
                    <td align="center">
                        <div id="SearchTemplate" runat="server" style="position: relative; margin: 0 auto;" class="SubContentCenter">
                            <fieldset>
                                <div class="ThreeColumn">
                                    <div class="Row">
                                        <div class="SearchText">
                                            <asp:Label ID="lblUsername" runat="server" Text='Utilizador'></asp:Label></div>
                                        <div class="SearchTextBoxDiv">
                                            <asp:TextBox runat="server" CssClass="SearchTextBox" ID="txtUsername"></asp:TextBox></div>
                                    </div>
                                </div>
                                <div class="ThreeColumn">
                                    <div class="Row">
                                        <div class="SearchText">
                                            </asp:Label></div>
                                        <div class="SearchTextBoxDiv">
                                            <div class="buttonSearchDivLeft">
                                                <asp:Button ID="btnClean" CssClass="button1" runat="server" Text="Clean" OnClick="btnClean_Click" CausesValidation="false" />
                                                <asp:Button ID="btnSearch" CssClass="button1" runat="server" Text="Search" OnClick="btnSearch_Click"
                                                    CausesValidation="false" />
                                                <asp:Button ID="btnCreate" CssClass="button1" runat="server" Text="Create" OnClick="btnCreate_Click"
                                                    CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                    <br />
                    <br />
                        <div id="List" runat="server" class="SubContentCenterTable">
                            <asp:UpdatePanel ID="up2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: left; width: 100%">
                                        <obout:Grid ID="grid1" Language="pt" AllowPaging="true" AllowFiltering="true" PageSize="10" AutoGenerateColumns="false"
                                            AllowMultiRecordSelection="false" AllowRecordSelection="false" AllowPageSizeSelection="false" AllowAddingRecords="false"
                                            runat="server" FolderStyle="~/App_Themes/Grid/Styles/Style_5" FolderLocalization="~/App_Themes/Grid"
                                            DataSourceID="ods1">
                                            <Columns>
                                                <obout:Column ID="Column0" Width="270" DataField="Username" HeaderText='Username' runat="server" TemplateId="RegularGridTemplate">
                                                </obout:Column>
                                                <obout:Column ID="ColumnEdit" Width="80" DataField="UserId" runat="server" TemplateId="GridTemplateEdit"
                                                    HeaderText=" ">
                                                </obout:Column>
                                                <obout:Column ID="ColumnDelete" Width="80" runat="server" TemplateId="GridTemplateDelete" HeaderText=" ">
                                                </obout:Column>
                                            </Columns>
                                            <Templates>
                                                <obout:GridTemplate runat="server" ID="RegularGridTemplate">
                                                    <Template>
                                                        <asp:Label ID="lbl1" runat="server" Text='<%# Container.Value %>'></asp:Label>
                                                    </Template>
                                                </obout:GridTemplate>
                                                <obout:GridTemplate runat="server" ID="GridTemplateEdit">
                                                    <Template>
                                                        <div style="float: left; padding-left: 1px">
                                                            <asp:HyperLink ID="lnk1" runat="server" Text="editar" CssClass="btnHyperlink" NavigateUrl='<%# BuildURL(Container.DataItem["UserId"], Container.DataItem["UserName"]) %>'>
                                                            </asp:HyperLink>
                                                        </div>
                                                    </Template>
                                                </obout:GridTemplate>
                                                <obout:GridTemplate runat="server" ID="GridTemplateDelete">
                                                    <Template>
                                                        <div style="float: left; padding-left: 1px">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="eliminar" CssClass="btnHyperlink" OnClick="lnkDelete_Click"
                                                                CommandArgument='<%# Container.DataItem["UserId"]  %>' OnClientClick='<%# "javascript:return window.confirm(&#39;Tem a certeza?&#39;);"%>'></asp:LinkButton>
                                                        </div>
                                                    </Template>
                                                </obout:GridTemplate>
                                            </Templates>
                                        </obout:Grid>
                                    </div>
                                </ContentTemplate>
                                <%--<Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" />
                <asp:AsyncPostBackTrigger ControlID="btnClean" />
            </Triggers>--%>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                </tr>
            </table>
            <div style="float: left; width: 100%; height: 10px">
                <br />
                <br />
                <br />
                <asp:ObjectDataSource runat="server" ID="ods1" TypeName="SuperPage" SelectMethod="UsersGet" EnablePaging="false"
                    SelectCountMethod="UsersCount"></asp:ObjectDataSource>
            </div>
            <%-- FOOTER  ################################################################################### --%>
            <asp:Panel ID="boxFooter" runat="server" CssClass="box" meta:resourcekey="boxFooterResource1">
                <UC:Footer ID="Footer1" runat="server"></UC:Footer>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
