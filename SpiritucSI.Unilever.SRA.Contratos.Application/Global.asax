﻿<%@ Application Language="C#" %>

<script runat="server">

    
    protected void Application_Start(object sender, EventArgs e)
    {
        this.StartWorkflow();
    }

    protected void Session_Start(object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

    protected void Application_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();

        if (ex is HttpUnhandledException)
        {
            ex = ex.InnerException;
        }

        Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionPolicy.HandleException(ex, "GlobalPolicy");

        //Response.Redirect("ErrorPage.aspx");
    }

    protected void Session_End(object sender, EventArgs e)
    {

    }

    protected void Application_End(object sender, EventArgs e)
    {
        this.StopWorkflow();
    }

    #region WORKFLOW EVENTS
    private void StartWorkflow()
    {
        //// Get the connection string from Web.config.
        String connectionString = ConfigurationManager.ConnectionStrings["Workflow"].ConnectionString;

        // Create the workflow runtime.
        System.Workflow.Runtime.WorkflowRuntime workflowRuntime = new System.Workflow.Runtime.WorkflowRuntime("WorkflowRuntime");

        workflowRuntime.WorkflowAborted += new EventHandler<System.Workflow.Runtime.WorkflowEventArgs>(workflowRuntime_WorkflowAborted);
        workflowRuntime.WorkflowTerminated += new EventHandler<System.Workflow.Runtime.WorkflowTerminatedEventArgs>(workflowRuntime_WorkflowTerminated);
        workflowRuntime.WorkflowUnloaded += new EventHandler<System.Workflow.Runtime.WorkflowEventArgs>(workflowRuntime_WorkflowUnloaded);
        workflowRuntime.WorkflowPersisted += new EventHandler<System.Workflow.Runtime.WorkflowEventArgs>(workflowRuntime_WorkflowPersisted);

        System.Workflow.Runtime.Hosting.SqlWorkflowPersistenceService swps = new System.Workflow.Runtime.Hosting.SqlWorkflowPersistenceService(connectionString);
        workflowRuntime.AddService(swps);

        System.Workflow.Runtime.Tracking.SqlTrackingService sts = new System.Workflow.Runtime.Tracking.SqlTrackingService(connectionString);
        workflowRuntime.AddService(sts);

        System.Workflow.Runtime.Hosting.ManualWorkflowSchedulerService mwss = new System.Workflow.Runtime.Hosting.ManualWorkflowSchedulerService();
        workflowRuntime.AddService(mwss);

        SpiritucSI.Unilever.SRA.WF.SqlAuditingService taas = new SpiritucSI.Unilever.SRA.WF.SqlAuditingService(connectionString);
        workflowRuntime.AddService(taas);

        // Add the communication service.
        System.Workflow.Activities.ExternalDataExchangeService dataService = new System.Workflow.Activities.ExternalDataExchangeService();
        workflowRuntime.AddService(dataService);
        dataService.AddService(new SpiritucSI.Unilever.SRA.WF.WFEvents());

        // Start the workflow runtime.
        // (The workflow runtime starts automatically when workflows are started, but will not start automatically when tracking data is requested)
        workflowRuntime.StartRuntime();

        // Add the runtime to the application.
        Application["WorkflowRuntime"] = workflowRuntime;
    }

    private void StopWorkflow()
    {
        System.Workflow.Runtime.WorkflowRuntime wr = (System.Workflow.Runtime.WorkflowRuntime)Application["WorkflowRuntime"];
        wr.StopRuntime();
    }

    private void workflowRuntime_WorkflowPersisted(object sender, System.Workflow.Runtime.WorkflowEventArgs e)
    {
        //throw new NotImplementedException();
    }

    private void workflowRuntime_WorkflowUnloaded(object sender, System.Workflow.Runtime.WorkflowEventArgs e)
    {
        //throw new NotImplementedException();
    }

    private void workflowRuntime_WorkflowTerminated(object sender, System.Workflow.Runtime.WorkflowTerminatedEventArgs e)
    {

    }

    private void workflowRuntime_WorkflowAborted(object sender, System.Workflow.Runtime.WorkflowEventArgs e)
    {
        throw new NotImplementedException();
    }
    #endregion
</script>
