<%@ Page Language="c#" CodeFile="logon.aspx.cs" AutoEventWireup="true" Inherits="logon"
    Theme="SRA" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contratos - LOGIN</title>
</head>
<body>
    <form id="Form1" method="post" runat="server" onsubmit="javascript: return MySubmit();">

    <script language="javascript" type="text/javascript" src="Scripts/SRAScript.js"></script>

    <%-- JANELA DE PROCESSAMENTO  ########################################################################## --%>
    <AJAX:ScriptManager ID="ScriptManager1" runat="server">
    </AJAX:ScriptManager>
    <AJAX:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="500" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px;
                position: absolute; left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF;
                z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </AJAX:UpdateProgress>
    <%-- CONTE�DO  ######################################################################################### --%>
    <AJAX:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:LinkButton ID="Dummy" runat="server"></asp:LinkButton>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="Box_Header" runat="server" CssClass="box" meta:resourcekey="Box_Header">
                <UC:Header ID="Header1" runat="server"></UC:Header>
            </asp:Panel>
            <%--  LOGIN  ################################################################################### --%>
            <asp:Panel ID="box_Logon" runat="server" CssClass="box" meta:resourcekey="box_Logon">
                <asp:Login ID="Login2" runat="server">
                </asp:Login>
            </asp:Panel>
            <%-- FOOTER  ################################################################################### --%>
            <asp:Panel ID="boxFooter" runat="server" CssClass="box" meta:resourcekey="boxFooter">
                <table class="table">
                    <tr>
                        <td align="center" colspan="2">
                            <UC:Footer ID="Footer1" runat="server"></UC:Footer>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </AJAX:UpdatePanel>
    </form>
</body>
</html>
