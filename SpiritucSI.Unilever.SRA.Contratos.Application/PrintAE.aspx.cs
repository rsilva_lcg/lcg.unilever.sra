﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintAE : System.Web.UI.Page
{
    #region FIELDS
    protected double PVP = 0;
    protected double QTD = 0;
    protected double Valor = 0;
    protected double Vendas = 0;
    protected double Marketing = 0;
    protected double Total = 0;            
    #endregion

    #region METHODS
    private bool LoadContrato(int idContrato)
    {
        bool retVal = false;
        try
        {
            /// LER TODO O CONTRATO
            AE DB_AE = new AE(ConfigurationManager.AppSettings["DBCS"]);
            DataSet Contrato = DB_AE.ContratoGet(idContrato);

            DataRow Ctr = Contrato.Tables["T_AE_CONTRATO"].Rows[0];

            #region PROPOSTA
            this.C_Proposta_txt_Emissor.Text = Ctr["ID_EMISSOR"].ToString();
            this.C_Proposta_txt_Ano.Text = Ctr["ID_ANO"].ToString();
            this.C_Proposta_txt_Serie.Text = Ctr["ID_NUM_SERIE"].ToString();
            this.C_Proposta_txt_Numero.Text = Ctr["ID_NUMERO"].ToString();
            this.C_Proposta_txt_EmissorName.Text = Ctr["ID_EMISSOR_NOME"].ToString();
            this.C_Recepcao_txt_Data.Text = ((DateTime)Ctr["DATA_RECEPCAO"]).ToString("(dd-MM-yyyy)");
            this.C_Recepcao_txt_Name.Text = Ctr["ID_CUSTOMER_SERVICE"].ToString() + " - " + Ctr["CUSTOMER_SERVICE_NOME"].ToString();
            #endregion

            #region NIVEL
            this.C_Nivel_img_LE.Visible = Ctr["NIVEL_CONTRATO"].ToString() == "LE";
            this.C_Nivel_img_SGE.Visible = Ctr["NIVEL_CONTRATO"].ToString() == "SGE";
            this.C_Nivel_img_GE.Visible = Ctr["NIVEL_CONTRATO"].ToString() == "GE";
            this.C_Nivel_txt_GE.Text = Ctr["GRUPO_ECONOMICO_COD"].ToString() + " - " + Ctr["GRUPO_ECONOMICO_DESC"].ToString();
            string cod = Ctr["SUBGRUPO_ECONOMICO_COD"].ToString();
            if (cod == "0")
            {
                this.C_Nivel_txt_SGE.Text = "- - - TODOS OS SUBGRUPOS - - -";
            }
            else
            {
                this.C_Nivel_txt_SGE.Text = cod + " - " + Ctr["SUBGRUPO_ECONOMICO_DESC"].ToString();
            }
            #endregion

            #region LOCAIS ENTREGA
            DataTable T_AE_ENTIDADE = Contrato.Tables["T_AE_ENTIDADE"];
            
            if (T_AE_ENTIDADE != null)
            {
                T_AE_ENTIDADE.DefaultView.Sort = "DES_GRUPO_ECONOMICO_LE, DES_SUBGRUPO_ECONOMICO_LE,DES_CONCESSIONARIO,DES_CLIENTE,COD_LOCAL_ENTREGA";
                this.C_dg_Entidades.DataSource = T_AE_ENTIDADE.DefaultView;
            }
            this.C_dg_Entidades.DataBind();
            #endregion

            #region PONTO VENDAS
            this.C_PontoVenda_txt_Nome.Text = Ctr["NOME_FIRMA"].ToString();
            this.C_PontoVenda_txt_Morada.Text = Ctr["MORADA_SEDE"].ToString();
            this.C_PontoVenda_txt_Localidade.Text = Ctr["LOCALIDADE"].ToString();
            this.C_PontoVenda_txt_Conribuinte.Text = Ctr["NIF"].ToString();
            #endregion

            #region ABASTECIMENTO
            DataTable T_AE_ABASTECIMENTO = Contrato.Tables["T_AE_ABASTECIMENTO"];

            if (T_AE_ABASTECIMENTO != null)
            {
                T_AE_ABASTECIMENTO.DefaultView.Sort = "COD_CONCESSIONARIO";
                this.C_Abastecimento_dg_Lista.DataSource = T_AE_ABASTECIMENTO.DefaultView;
            }
            this.C_Abastecimento_dg_Lista.DataBind();
            #endregion

            #region TIPO ACTIVIDADE
            DataTable T_AE_ACTIVIDADE = Contrato.Tables["T_AE_ACTIVIDADE"];
            double ActividadeVal = 0;

            if (T_AE_ACTIVIDADE != null)
            {
                T_AE_ACTIVIDADE.DefaultView.Sort = "NOME_TIPO_ACTIVIDADE";
                this.C_Actividade_dg_Lista.DataSource = T_AE_ACTIVIDADE.DefaultView;
                foreach (DataRowView dr in T_AE_ACTIVIDADE.DefaultView)
                {
                    ActividadeVal += double.Parse(dr["VALOR_ACORDO"].ToString());
                }
            }

            this.C_Actividade_dg_Lista.DataBind();            
            this.C_Actividade_txt_Total.Text = ActividadeVal.ToString("#0.00 €");
            #endregion

            #region DEBITOS
            DataTable T_AE_DEBITO = Contrato.Tables["T_AE_DEBITO"];
            double val = 0;

            if (T_AE_DEBITO != null)
            {
                T_AE_DEBITO.DefaultView.Sort = "DATA_DEBITO, VALOR_DEBITO";
                this.C_Debito_dg_Lista.DataSource = T_AE_DEBITO.DefaultView;
                foreach (DataRowView dr in T_AE_DEBITO.DefaultView)
                {
                    val += double.Parse(dr["VALOR_DEBITO"].ToString());
                }
            }

            this.C_Debito_dg_Lista.DataBind();            
            this.C_Debitos_txt_Total.Text = val.ToString("#0.00 €");
            this.C_Debitos_txt_TotalResidual.Text = (ActividadeVal - val).ToString("#0.00 €");
            #endregion

            #region TPR
            DataTable T_AE_TPR = Contrato.Tables["T_AE_TPR"];

            if (T_AE_TPR != null)
            {
                T_AE_TPR.DefaultView.Sort = "NOME_PRODUTO, COD_PRODUTO";
                this.C_TPR_dg_Lista.DataSource = T_AE_TPR.DefaultView;

                double tmp_PVP = 0;
                double tmp_QTD = 0;
                double tmp_Valor = 0;
                double tmp_BVendas = 0;
                double tmp_BMarketing = 0;

                foreach (DataRowView dr in T_AE_TPR.DefaultView)
                {
                    tmp_PVP = double.Parse(dr["PVP"].ToString());
                    tmp_QTD = double.Parse(dr["PREVISAO_QUANTIDADE"].ToString());
                    tmp_BVendas = double.Parse(dr["BUDGET_VENDAS"].ToString()) / 100;
                    tmp_BMarketing = double.Parse(dr["BUDGET_MARKETING"].ToString()) / 100;
                    tmp_Valor = tmp_PVP * tmp_QTD;

                    this.PVP += tmp_PVP;
                    this.QTD += tmp_QTD;
                    this.Valor += tmp_Valor;
                    this.Vendas += tmp_Valor * tmp_BVendas;
                    this.Marketing += tmp_Valor * tmp_BMarketing;
                    this.Total += (tmp_Valor * tmp_BVendas) + (tmp_Valor * tmp_BMarketing);
                }
            }
            this.C_TPR_dg_Lista.DataBind();
            
            #endregion

            #region OBSERVAÇÕES
            this.C_Observacoes_txt_header.Text = Ctr["OBSERVACOES"].ToString()+"&nbsp;";
            #endregion

            retVal = true;

        }
        catch (myDBException exp)
        {
            this.C_Title_lbl.Text = "<br><br>NÃO FOI POSSÍVEL LER OS DADOS DO CONTRATO";
        }
        catch (Exception exp)
        {
            this.C_Title_lbl.Text = "OCORREU UM ERRO INESPERADO";
        }
        return retVal;
    }
    #endregion

    #region PAGE EVENTS
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!User.Identity.IsAuthenticated)
        {
            Response.Redirect("~/logon.aspx", true);
        }
        if (Request["idContrato"] == null)
        {
            this.C_Title_lbl.Text = "NÃO FOI INDICADO NENHUM CONTRATO";
        }
        else 
        {
            this.LoadContrato(Convert.ToInt32(this.Request["idContrato"]));
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.C_box_proposta.Visible = Request["idContrato"] != null;
        this.C_box_Content.Visible = Request["idContrato"] != null;
    }
    #endregion
}
