<%@ Page Language="c#" CodeFile="AE.aspx.cs" AutoEventWireup="true" Inherits="AE_Page" meta:resourcekey="Page" Theme="SRA" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="Form1" method="post"  runat="server" onsubmit="javascript: return MySubmit();">

    <script language="javascript" type="text/javascript" src="Scripts/SRAScript.js"></script>

    <%-- JANELA DE PROCESSAMENTO  ########################################################################## --%>
    <AJAX:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </AJAX:ScriptManager> 
    <AJAX:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="progressBackgroundFilter">
            </div>
            <table id="IMGDIV" align="center" valign="middle" style="width: 200px; height: 200px; position: absolute;
                left: 48%; top: 48%; visibility: visible; background-color: #FFFFFF; z-index: 1001">
                <tr>
                    <td valign="middle" align="center">
                        <img src="images/Processing.gif" alt="" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        A Processar
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </AJAX:UpdateProgress>
    <%-- CONTE�DO  ######################################################################################### --%>
    <AJAX:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%-- HEADER  ################################################################################### --%>
            <asp:Panel ID="boxTitulo" runat="server" CssClass="box" meta:resourcekey="boxTituloResource1">
                <UC:Header ID="Header1" runat="server" Title="CONTRATOS DE ACTIVIDADES ESPECIAIS"></UC:Header>
            </asp:Panel>
            <%-- MENSAGENS DE ERRO  ######################################################################## --%>
            <asp:Panel ID="boxError" runat="server" CssClass="box" meta:resourcekey="boxErrorResource1">
                <table class="table" align="center">
                    <tr>
                        <td valign="middle" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="errorLabel" meta:resourcekey="lblErrorResource1"></asp:Label><br>
                            <asp:Label ID="lblWarning" runat="server" CssClass="warningLabel" meta:resourcekey="lblWarningResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%-- PESQUISA DE CONTRATOS  #################################################################### --%>
            <asp:Panel ID="boxPesquisa" runat="server" CssClass="box" meta:resourcekey="boxPesquisaResource1">
                <table class="table" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td align="center">
                            <UC:Search ID="ucPesquisa" runat="server" OnSelectResult="ucPesquisa_onSelectResult"></UC:Search>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%-- BOT�ES  ################################################################################### --%>
            <asp:Panel ID="boxBotoes" runat="server" CssClass="box" meta:resourcekey="boxBotoesResource1"> 
                <br>
                <UC:ImageTextButton ID="lnkInserir" runat="server" TextCssClass="button titulo" TextPosition="Rigth"  meta:resourcekey="lnkInserir" CausesValidation="False" OnClick="lnkInserir_Click" />
            </asp:Panel>
            <%-- FOOTER  ################################################################################### --%>
            <asp:Panel ID="boxFooter" runat="server" CssClass="box" meta:resourcekey="boxFooterResource1">
                <UC:Footer ID="Footer1" runat="server"></UC:Footer>
            </asp:Panel>
        </ContentTemplate>
    </AJAX:UpdatePanel>
    </form>
</body>
</html>
