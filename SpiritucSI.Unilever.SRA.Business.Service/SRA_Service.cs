﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace SpiritucSI.Unilever.SRA.Service
{
    public class SRA_Service
    {
        protected string ConnectionString;

        public SRA_Service(string ConnString)
        {
            this.ConnectionString = ConnString;
        }

        public void CheckValidated()
        {
            SRADBInterface.SRA sraDB = new SRADBInterface.SRA(ConnectionString);
            int idContrato;
            DataTable contratosAutorizados = sraDB.getContrato("is_authorized = 'T' AND status = 'I' AND ID_PROCESSO = '201204100025386'");
            foreach (DataRow row in contratosAutorizados.Rows)
            {
                idContrato =Convert.ToInt32(row["ID_Contrato"]);
                if (!sraDB.getContratoFiles(idContrato).Rows.Count.Equals(0))
                {
                    string mot="";
                    if (row["OBSERVACOES_MOTIVO"] != null)
                        mot = row["OBSERVACOES_MOTIVO"].ToString();
                    sraDB.UpdateContratoStatus(idContrato, "V",DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (wsSRA) -> VALIDADO \n"+ mot, DateTime.Now, "", "wsSRA");
                    sraDB.SendMail(idContrato, true);
                }
            }
        }

        public void ContratosRenewal(string username)
        {
            WorkflowFunctions wf = new WorkflowFunctions(ConnectionString);
            SRADBInterface.SRA sraDB = new SRADBInterface.SRA(ConnectionString);
            int idContrato;
            string dataPrevisaoStr;

            EventLog.WriteEntry("wsSRA - RENEWAL", "RENEWAL STARTED", EventLogEntryType.Information);
            //EventLog.WriteEntry("wsSRA - RENEWAL conn", ConnectionString, EventLogEntryType.Information);


            DataTable contratos = sraDB.getContratos_ToRenewal();

            //EventLog.WriteEntry("wsSRA - RENEWAL", "oracle query opened", EventLogEntryType.Information);

            foreach (DataRow row in contratos.Rows)
            {
                idContrato = Convert.ToInt32(row["ID_Contrato"]);
                dataPrevisaoStr = row["dataPrevisao"].ToString();
                DateTime dataPrevisao = DateTime.Now;
                if (dataPrevisaoStr != "")
                    dataPrevisao = DateTime.Parse(dataPrevisaoStr);
                DataSet dsContrato = sraDB.getContrato(idContrato);
                DataRow rowContrato = dsContrato.Tables["CONTRATO"].Rows[0];
                // criar novo contrato com NumProcessoAnterior
                int newIdContrato=0;

                string concessionario = rowContrato["ID_PROCESSO"].ToString().Substring(4,3);
                string contrato_num = "";
                string contrato_numAnterior = rowContrato["ID_PROCESSO"].ToString();
                string contrato_numSeguinte = "";
                string classif = rowContrato["CLASSIFICACAO_CONTRATO"].ToString();
                string tipo = rowContrato["TIPO_CONTRATO"].ToString();
                DateTime data = DateTime.Now.Date;
                string nivel = rowContrato["NIVEL_CONTRATO"].ToString();
                string grupoEconomico = rowContrato["COD_GRUPO_ECONOMICO"].ToString();
                string subGrupoEconomico = rowContrato["COD_SUBGRUPO_ECONOMICO"].ToString();
                string nomeFirma = rowContrato["NOME_FIRMA"].ToString();
                string moradaSede = rowContrato["MORADA_SEDE"].ToString();
                string localidade = rowContrato["LOCALIDADE"].ToString();
                string ramoActividade = rowContrato["RAMO_ACTIVIDADE"].ToString();
                string PV_Matricula = rowContrato["MATRICULA"].ToString();
                string PV_MatriculaNum = rowContrato["MATRICULA_NUM"].ToString();
                string PV_CapitalSocial = rowContrato["CAPITAL_SOCIAL"].ToString();
                string PV_Nif = rowContrato["NIF"].ToString();
                string PV_Designacao = rowContrato["DESIGNACAO_PT_VENDA"].ToString();
                string PV_MoradaPtVenda = rowContrato["MORADA_PT_VENDA"].ToString();
                string cond_objectivo = rowContrato["OBJECTIVO"].ToString();

                DateTime cond_dataInicio;
                DateTime cond_dataFim;
                string cond_quandoAtingir;
                int anosRenovacao=0;
                int anosRenovacaoOriginal = 0;
                if ((rowContrato["ANOS_RENOVACAO"] != DBNull.Value) && (!Convert.ToInt32(rowContrato["ANOS_RENOVACAO"]).Equals(0)))
                {
                    anosRenovacaoOriginal = Convert.ToInt32(rowContrato["ANOS_RENOVACAO"]);
                    anosRenovacao = anosRenovacaoOriginal - 1;
                }

                if ((rowContrato["QUANTIA_FIM"] != DBNull.Value) && (rowContrato["QUANTIA_FIM"].ToString().Length > 0))
                {
                    cond_quandoAtingir = rowContrato["QUANTIA_FIM"].ToString();
                    //cond_dataInicio = DateTime.Now.AddMonths(2).AddDays(-1);
                    //NOVO RRAFAEL: DATAINICIO = DATA PREVISAO FIM ANTERIOR 
                    cond_dataInicio = dataPrevisao.AddMonths(2).AddDays(1);
                    //cond_dataInicio = Convert.ToDateTime(rowContrato["DATA_FIM"]).AddDays(1);
                    cond_dataFim = DateTime.MinValue;
                }
                else
                {
                    cond_quandoAtingir = "";
                    cond_dataInicio = Convert.ToDateTime(rowContrato["DATA_FIM"]).AddDays(1);
                    TimeSpan periodoContAnt = Convert.ToDateTime(rowContrato["DATA_FIM"]).Subtract(Convert.ToDateTime(rowContrato["DATA_INICIO"]));
                    cond_dataFim = cond_dataInicio.Add(periodoContAnt).AddDays(1);
                }

                string cond_idContratoFormal = rowContrato["IS_CONTRATO_FORMAL"].ToString();
                string cond_isPooc = rowContrato["IS_POOC"].ToString();
                string cond_isFornoFrio = rowContrato["IS_FORN_FRIO_IGLO"].ToString();
                string cont_previsao = rowContrato["PREVISAO_VENDAS_GLOBAL"].ToString();
                string cont_prazopagamento = rowContrato["PRAZO_PAGAMENTO"].ToString();
                DataTable dtGamasGlobal = dsContrato.Tables["GAMAS_GLOBAL"];
                string fase_nib = rowContrato["NIB_CLIENTE"].ToString();
                string fase_banco = rowContrato["BANCO"].ToString();
                string fase_banco_dependencia = rowContrato["BANCO_DEPENDENCIA"].ToString();
                string fase_compCompras = rowContrato["COMP_CONC_COMPRAS"].ToString();
                string user = username;
                DataTable dtClientes = dsContrato.Tables["ENTIDADE"];
                DataTable dtGamas = dsContrato.Tables["GAMAS_QUANTIA_FIM"];
                DataTable dtExploracao = dsContrato.Tables["EXPLORACAO"];
                DataTable dtEquipamentos = dsContrato.Tables["CONT_EQUIPAMENTO"];
                DataTable dtContrapartidas = dsContrato.Tables["CONTRAPARTIDAS"];
                DataTable dtBonus = dsContrato.Tables["BONUS"];
                DataTable dtBonusGlobal = dsContrato.Tables["BONUS_GLOBAL"];
                DataTable dtVerbas = dsContrato.Tables["FASEAMENTO_PAG"];
                DataTable dtMaterial = dsContrato.Tables["CONT_MAT_VISIBILIDADE"];
                string defaultCodConc = rowContrato["DEFAULT_COD_CONC"].ToString();

                //EventLog.WriteEntry("wsSRA - RENEWAL", "before save", EventLogEntryType.Error);

                string numContrato = sraDB.SaveContrato(ref newIdContrato, concessionario, contrato_num, contrato_numAnterior, contrato_numSeguinte,
                    classif, tipo, data, nivel, grupoEconomico, subGrupoEconomico, "I", "", "", nomeFirma,
                    moradaSede, localidade, ramoActividade, PV_Matricula, PV_MatriculaNum, PV_CapitalSocial, PV_Nif, PV_Designacao,
                    PV_MoradaPtVenda, cond_objectivo, cond_dataInicio, cond_dataFim, cond_quandoAtingir, anosRenovacao.ToString(),
                    cond_idContratoFormal, cond_isPooc, cond_isFornoFrio, cont_previsao, cont_prazopagamento, dtGamasGlobal, fase_nib,
                    fase_banco, fase_banco_dependencia, fase_compCompras, user, dtClientes, dtGamas, dtExploracao, dtEquipamentos,
                    dtContrapartidas, dtBonus, dtBonusGlobal, dtVerbas, dtMaterial, Guid.Empty, "", "F", defaultCodConc, "F", "T", "F"); 
                // update is sent to renewal, numProcessoASeguinte

                //EventLog.WriteEntry("wsSRA - RENEWAL", "before upd contrato", EventLogEntryType.Information);

                //NOVO RRAFAEL: UPDATE DATA_FIM;
                string mot ="";
                if(rowContrato["OBSERVACOES_MOTIVO"] != null)
                    mot = rowContrato["OBSERVACOES_MOTIVO"].ToString();
                sraDB.UpdateContratoStatus(idContrato, "T", DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (wsSRA) -> TERMINADO \n" + mot, cond_dataInicio.AddDays(-1), numContrato, "wsSRA");
                //END NOVO

                sraDB.UpdateContrato_IS_SENTRENEWAL(idContrato, true);

                //EventLog.WriteEntry("wsSRA - RENEWAL", "before UpdateContratoWF_ID", EventLogEntryType.Information);
                Guid newWF = new Guid();
                try
                {
                    newWF = wf.RunWorkflow(newIdContrato);
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("wsSRA-CONFIG", ex.Message, EventLogEntryType.Information);
                }
                sraDB.UpdateContratoWF_ID(newIdContrato, newWF);

                string msg = string.Empty;

                if (anosRenovacaoOriginal > 0)
                {
                    sraDB.UpdateContratoWFState(newWF, "RENEWALDRAFT");
                    msg = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (wsSRA) -> NOVO ESTADO: RENEWALDRAFT\n";
                }
                else
                {
                    sraDB.UpdateContratoWFState(newWF, "DRAFT");
                    msg = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (wsSRA) -> NOVO ESTADO: DRAFT\n";
                }

                ////EventLog.WriteEntry("wsSRA - RENEWAL", "before UpdateContratoWFState", EventLogEntryType.Information);
                //sraDB.UpdateContratoWFState(newWF, "STARTED");
                ////EventLog.WriteEntry("wsSRA - RENEWAL", "before AppendContratoObsMotivo", EventLogEntryType.Information);
                //string msg = DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (wsSRA) -> NOVO ESTADO: STARTED\n";

                sraDB.AppendContratoObsMotivo(newWF, msg);

            }
        }

        public void ContratosClose()
        {
            SRADBInterface.SRA sraDB = new SRADBInterface.SRA(ConnectionString);
            int idContrato;
            EventLog.WriteEntry("wsSRA - CLOSING", "CLOSING CONTRACTS STARTED", EventLogEntryType.Information);
            DateTime dataInicio = DateTime.Now;
            DataTable contratos = sraDB.getContratos_ToClose();
            foreach (DataRow row in contratos.Rows)
            {
                idContrato = Convert.ToInt32(row["ID_Contrato"]);
                //dataPrevisaoFim = (row["DATA_FIM"]);
                DataTable contratoSeguinte = sraDB.getContrato("NUM_PROCESSO_ANTERIOR = '" + row["ID_PROCESSO"].ToString() + "'");
                string mot = "";
                if (row["OBSERVACOES_MOTIVO"] != null)
                    mot = row["OBSERVACOES_MOTIVO"].ToString();
                if (contratoSeguinte.Rows.Count > 0)
                {
                    dataInicio =  DateTime.Parse(contratoSeguinte.Rows[0]["DATA_INICIO"].ToString());
                    contratoSeguinte.DefaultView.Sort = "DATA_CONTRATO DESC";
                    //FALTA ALTERAR O SP PARA SO DEVOLVER CONTRACTOS QUE TENHAM O SEGUINTE APROVADO (IS_AUTHORIZED???)
                    //sraDB.UpdateContratoStatus(idContrato, "T", DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (wsSRA) -> TERMINADO \n", DateTime.Now, contratoSeguinte.Rows[0]["ID_PROCESSO"].ToString(), "wsSRA");
                    sraDB.UpdateContratoStatus(idContrato, "T", DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (wsSRA) -> UPDATE DATA DE FIM EFECTIVO \n" + mot , dataInicio.AddDays(-1), contratoSeguinte.Rows[0]["ID_PROCESSO"].ToString(), "wsSRA");
                }
                else
                {
                    sraDB.UpdateContratoStatus(idContrato, "T", DateTime.Now.ToString("dd-MM-yyyy - HH:mm:ss") + " (wsSRA) -> TERMINADO \n" + mot , DateTime.Now, "", "wsSRA");
                }
            }
        }
    }
}
