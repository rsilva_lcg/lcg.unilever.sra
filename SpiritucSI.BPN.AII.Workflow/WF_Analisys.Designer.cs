﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace SpiritucSI.BPN.AII.Workflow
{
    partial class WF_Analisys
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.Activities.CodeCondition codecondition1 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition2 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition3 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition4 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition5 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition6 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition7 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition8 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind4 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind5 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind6 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind7 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind8 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind9 = new System.Workflow.ComponentModel.ActivityBind();
            this.codeActivity18 = new System.Workflow.Activities.CodeActivity();
            this.codeActivity16 = new System.Workflow.Activities.CodeActivity();
            this.faultHandlerActivity12 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.codeActivity15 = new System.Workflow.Activities.CodeActivity();
            this.faultHandlerActivity11 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.codeActivity12 = new System.Workflow.Activities.CodeActivity();
            this.RaiseExceptionProceedInConclusion = new System.Workflow.Activities.CodeActivity();
            this.setStateActivity7 = new System.Workflow.Activities.SetStateActivity();
            this.codeActivity8 = new System.Workflow.Activities.CodeActivity();
            this.codeActivity14 = new System.Workflow.Activities.CodeActivity();
            this.RaiseExceptionRejectSupervisorValidation = new System.Workflow.Activities.CodeActivity();
            this.setStateActivity9 = new System.Workflow.Activities.SetStateActivity();
            this.codeActivity13 = new System.Workflow.Activities.CodeActivity();
            this.RaiseExceptionRejectAnalystInValidation = new System.Workflow.Activities.CodeActivity();
            this.setStateActivity8 = new System.Workflow.Activities.SetStateActivity();
            this.codeActivity7 = new System.Workflow.Activities.CodeActivity();
            this.RaiseExceptionProceedInValidation = new System.Workflow.Activities.CodeActivity();
            this.setStateActivity5 = new System.Workflow.Activities.SetStateActivity();
            this.codeActivity10 = new System.Workflow.Activities.CodeActivity();
            this.faultHandlersActivity14 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.RaiseExceptionRejectInSupervision = new System.Workflow.Activities.CodeActivity();
            this.faultHandlersActivity13 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity1 = new System.Workflow.Activities.SetStateActivity();
            this.codeActivity3 = new System.Workflow.Activities.CodeActivity();
            this.faultHandlersActivity12 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.cancellationHandlerActivity6 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.RaiseExceptionInSupervision = new System.Workflow.Activities.CodeActivity();
            this.faultHandlerActivity10 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlersActivity11 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.cancellationHandlerActivity5 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.setStateActivity3 = new System.Workflow.Activities.SetStateActivity();
            this.codeActivity2 = new System.Workflow.Activities.CodeActivity();
            this.RaiseExceptionReassignInAnalisys = new System.Workflow.Activities.CodeActivity();
            this.setStateActivity4 = new System.Workflow.Activities.SetStateActivity();
            this.codeActivity1 = new System.Workflow.Activities.CodeActivity();
            this.RaiseProceedExceptionInAnalisys = new System.Workflow.Activities.CodeActivity();
            this.setStateActivity2 = new System.Workflow.Activities.SetStateActivity();
            this.faultHandlerActivity7 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ElseProceedInConclusion = new System.Workflow.Activities.IfElseBranchActivity();
            this.IfProceedInConclusion = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity5 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.faultHandlerActivity9 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ElseRejectSupervisorValidation = new System.Workflow.Activities.IfElseBranchActivity();
            this.IfRejectSupervisorValidation = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity8 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ElseRejectAnalystInValidation = new System.Workflow.Activities.IfElseBranchActivity();
            this.IfRejectAnalystInValidation = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity4 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ElseProceedInValidation = new System.Workflow.Activities.IfElseBranchActivity();
            this.IfProceedInValidation = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity6 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ElseRejectInSupervision = new System.Workflow.Activities.IfElseBranchActivity();
            this.IfRejectInSupervision = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity2 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ElseProceedInSupervision = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlersActivity10 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.IfProceedInSup = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity3 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ElseReassignInAnalisys = new System.Workflow.Activities.IfElseBranchActivity();
            this.IfReassignInAnalisys = new System.Workflow.Activities.IfElseBranchActivity();
            this.faultHandlerActivity1 = new System.Workflow.ComponentModel.FaultHandlerActivity();
            this.ElseInAnalisys = new System.Workflow.Activities.IfElseBranchActivity();
            this.IfInAnalisys = new System.Workflow.Activities.IfElseBranchActivity();
            this.codeActivity11 = new System.Workflow.Activities.CodeActivity();
            this.faultHandlersActivity7 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ConditionProceedInConclusion = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity4 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity2 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.setStateActivity6 = new System.Workflow.Activities.SetStateActivity();
            this.handleExternalEventOnAlocatedAnalist = new System.Workflow.Activities.HandleExternalEventActivity();
            this.codeActivity6 = new System.Workflow.Activities.CodeActivity();
            this.faultHandlersActivity9 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ConditionRejectSupervisorValidation = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity6 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.faultHandlersActivity8 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ConditionRejectAnalystInValidation = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventValidationRejectedAnalyst = new System.Workflow.Activities.HandleExternalEventActivity();
            this.codeActivity5 = new System.Workflow.Activities.CodeActivity();
            this.faultHandlersActivity3 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ConditionProceedInValidation = new System.Workflow.Activities.IfElseActivity();
            this.HandleAnalisysAcceptence = new System.Workflow.Activities.HandleExternalEventActivity();
            this.cancellationHandlerActivity4 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.faultHandlersActivity6 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ConditionRejectInSupervision = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity2 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.cancellationHandlerActivity2 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.faultHandlersActivity4 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ConditionProceedInSupervision = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity1 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.codeActivity4 = new System.Workflow.Activities.CodeActivity();
            this.codeActivity9 = new System.Workflow.Activities.CodeActivity();
            this.cancellationHandlerActivity3 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.faultHandlersActivity5 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ConditionReassignInAnalisys = new System.Workflow.Activities.IfElseActivity();
            this.handleExternalEventActivity3 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.InitializeState = new System.Workflow.Activities.CodeActivity();
            this.cancellationHandlerActivity1 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.faultHandlersActivity1 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.ConditionInAnalisys = new System.Workflow.Activities.IfElseActivity();
            this.HandleAnalisysSubmitted = new System.Workflow.Activities.HandleExternalEventActivity();
            this.InConclusionInitialization = new System.Workflow.Activities.StateInitializationActivity();
            this.OnConclusionOccured = new System.Workflow.Activities.EventDrivenActivity();
            this.onAllocatedAnalyst = new System.Workflow.Activities.EventDrivenActivity();
            this.InAllocationStateInitialization = new System.Workflow.Activities.StateInitializationActivity();
            this.OnValidationRejectedSupervisor = new System.Workflow.Activities.EventDrivenActivity();
            this.OnValidationRejectedAnalyst = new System.Workflow.Activities.EventDrivenActivity();
            this.InValidationStateInitialization = new System.Workflow.Activities.StateInitializationActivity();
            this.OnValidationOccured = new System.Workflow.Activities.EventDrivenActivity();
            this.OnSupervisionReject = new System.Workflow.Activities.EventDrivenActivity();
            this.OnSupervisionAproval = new System.Workflow.Activities.EventDrivenActivity();
            this.InSupervisionStateInitialization = new System.Workflow.Activities.StateInitializationActivity();
            this.InFinishInitialization = new System.Workflow.Activities.StateInitializationActivity();
            this.OnAnalisysReassign = new System.Workflow.Activities.EventDrivenActivity();
            this.InAnalisysStateInitialization = new System.Workflow.Activities.StateInitializationActivity();
            this.OnAnalisysSubmited = new System.Workflow.Activities.EventDrivenActivity();
            this.InConclusion = new System.Workflow.Activities.StateActivity();
            this.InAllocation = new System.Workflow.Activities.StateActivity();
            this.InValidation = new System.Workflow.Activities.StateActivity();
            this.InSupervision = new System.Workflow.Activities.StateActivity();
            this.Finish = new System.Workflow.Activities.StateActivity();
            this.InAnalisys = new System.Workflow.Activities.StateActivity();
            // 
            // codeActivity18
            // 
            this.codeActivity18.Name = "codeActivity18";
            this.codeActivity18.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // codeActivity16
            // 
            this.codeActivity16.Name = "codeActivity16";
            this.codeActivity16.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // faultHandlerActivity12
            // 
            this.faultHandlerActivity12.Activities.Add(this.codeActivity18);
            this.faultHandlerActivity12.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity12.Name = "faultHandlerActivity12";
            // 
            // codeActivity15
            // 
            this.codeActivity15.Name = "codeActivity15";
            this.codeActivity15.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // faultHandlerActivity11
            // 
            this.faultHandlerActivity11.Activities.Add(this.codeActivity16);
            this.faultHandlerActivity11.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity11.Name = "faultHandlerActivity11";
            // 
            // codeActivity12
            // 
            this.codeActivity12.Name = "codeActivity12";
            this.codeActivity12.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // RaiseExceptionProceedInConclusion
            // 
            this.RaiseExceptionProceedInConclusion.Name = "RaiseExceptionProceedInConclusion";
            this.RaiseExceptionProceedInConclusion.ExecuteCode += new System.EventHandler(this.RaiseProceedExeption);
            // 
            // setStateActivity7
            // 
            this.setStateActivity7.Name = "setStateActivity7";
            this.setStateActivity7.TargetStateName = "Finish";
            // 
            // codeActivity8
            // 
            this.codeActivity8.Name = "codeActivity8";
            this.codeActivity8.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // codeActivity14
            // 
            this.codeActivity14.Name = "codeActivity14";
            this.codeActivity14.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // RaiseExceptionRejectSupervisorValidation
            // 
            this.RaiseExceptionRejectSupervisorValidation.Name = "RaiseExceptionRejectSupervisorValidation";
            this.RaiseExceptionRejectSupervisorValidation.ExecuteCode += new System.EventHandler(this.RaiseProceedExeption);
            // 
            // setStateActivity9
            // 
            this.setStateActivity9.Name = "setStateActivity9";
            this.setStateActivity9.TargetStateName = "InSupervision";
            // 
            // codeActivity13
            // 
            this.codeActivity13.Name = "codeActivity13";
            this.codeActivity13.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // RaiseExceptionRejectAnalystInValidation
            // 
            this.RaiseExceptionRejectAnalystInValidation.Name = "RaiseExceptionRejectAnalystInValidation";
            this.RaiseExceptionRejectAnalystInValidation.ExecuteCode += new System.EventHandler(this.RaiseProceedExeption);
            // 
            // setStateActivity8
            // 
            this.setStateActivity8.Name = "setStateActivity8";
            this.setStateActivity8.TargetStateName = "InAnalisys";
            // 
            // codeActivity7
            // 
            this.codeActivity7.Name = "codeActivity7";
            this.codeActivity7.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // RaiseExceptionProceedInValidation
            // 
            this.RaiseExceptionProceedInValidation.Name = "RaiseExceptionProceedInValidation";
            this.RaiseExceptionProceedInValidation.ExecuteCode += new System.EventHandler(this.RaiseProceedExeption);
            // 
            // setStateActivity5
            // 
            this.setStateActivity5.Name = "setStateActivity5";
            this.setStateActivity5.TargetStateName = "InConclusion";
            // 
            // codeActivity10
            // 
            this.codeActivity10.Name = "codeActivity10";
            this.codeActivity10.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // faultHandlersActivity14
            // 
            this.faultHandlersActivity14.Name = "faultHandlersActivity14";
            // 
            // RaiseExceptionRejectInSupervision
            // 
            this.RaiseExceptionRejectInSupervision.Name = "RaiseExceptionRejectInSupervision";
            this.RaiseExceptionRejectInSupervision.ExecuteCode += new System.EventHandler(this.RaiseProceedExeption);
            // 
            // faultHandlersActivity13
            // 
            this.faultHandlersActivity13.Name = "faultHandlersActivity13";
            // 
            // setStateActivity1
            // 
            this.setStateActivity1.Name = "setStateActivity1";
            this.setStateActivity1.TargetStateName = "InAnalisys";
            // 
            // codeActivity3
            // 
            this.codeActivity3.Name = "codeActivity3";
            this.codeActivity3.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // faultHandlersActivity12
            // 
            this.faultHandlersActivity12.Activities.Add(this.faultHandlerActivity12);
            this.faultHandlersActivity12.Name = "faultHandlersActivity12";
            // 
            // cancellationHandlerActivity6
            // 
            this.cancellationHandlerActivity6.Name = "cancellationHandlerActivity6";
            // 
            // RaiseExceptionInSupervision
            // 
            this.RaiseExceptionInSupervision.Name = "RaiseExceptionInSupervision";
            this.RaiseExceptionInSupervision.ExecuteCode += new System.EventHandler(this.RaiseProceedExeption);
            // 
            // faultHandlerActivity10
            // 
            this.faultHandlerActivity10.Activities.Add(this.codeActivity15);
            this.faultHandlerActivity10.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity10.Name = "faultHandlerActivity10";
            // 
            // faultHandlersActivity11
            // 
            this.faultHandlersActivity11.Activities.Add(this.faultHandlerActivity11);
            this.faultHandlersActivity11.Name = "faultHandlersActivity11";
            // 
            // cancellationHandlerActivity5
            // 
            this.cancellationHandlerActivity5.Name = "cancellationHandlerActivity5";
            // 
            // setStateActivity3
            // 
            this.setStateActivity3.Name = "setStateActivity3";
            this.setStateActivity3.TargetStateName = "InValidation";
            // 
            // codeActivity2
            // 
            this.codeActivity2.Name = "codeActivity2";
            this.codeActivity2.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // RaiseExceptionReassignInAnalisys
            // 
            this.RaiseExceptionReassignInAnalisys.Name = "RaiseExceptionReassignInAnalisys";
            this.RaiseExceptionReassignInAnalisys.ExecuteCode += new System.EventHandler(this.RaiseProceedExeption);
            // 
            // setStateActivity4
            // 
            this.setStateActivity4.Name = "setStateActivity4";
            this.setStateActivity4.TargetStateName = "InAllocation";
            // 
            // codeActivity1
            // 
            this.codeActivity1.Name = "codeActivity1";
            this.codeActivity1.ExecuteCode += new System.EventHandler(this.RaiseWorkflowException);
            // 
            // RaiseProceedExceptionInAnalisys
            // 
            this.RaiseProceedExceptionInAnalisys.Name = "RaiseProceedExceptionInAnalisys";
            this.RaiseProceedExceptionInAnalisys.ExecuteCode += new System.EventHandler(this.RaiseProceedExeption);
            // 
            // setStateActivity2
            // 
            this.setStateActivity2.Name = "setStateActivity2";
            this.setStateActivity2.TargetStateName = "InSupervision";
            // 
            // faultHandlerActivity7
            // 
            this.faultHandlerActivity7.Activities.Add(this.codeActivity12);
            this.faultHandlerActivity7.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity7.Name = "faultHandlerActivity7";
            // 
            // ElseProceedInConclusion
            // 
            this.ElseProceedInConclusion.Activities.Add(this.RaiseExceptionProceedInConclusion);
            this.ElseProceedInConclusion.Name = "ElseProceedInConclusion";
            // 
            // IfProceedInConclusion
            // 
            this.IfProceedInConclusion.Activities.Add(this.setStateActivity7);
            codecondition1.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.CanProcessProceed);
            this.IfProceedInConclusion.Condition = codecondition1;
            this.IfProceedInConclusion.Name = "IfProceedInConclusion";
            // 
            // faultHandlerActivity5
            // 
            this.faultHandlerActivity5.Activities.Add(this.codeActivity8);
            this.faultHandlerActivity5.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity5.Name = "faultHandlerActivity5";
            // 
            // faultHandlerActivity9
            // 
            this.faultHandlerActivity9.Activities.Add(this.codeActivity14);
            this.faultHandlerActivity9.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity9.Name = "faultHandlerActivity9";
            // 
            // ElseRejectSupervisorValidation
            // 
            this.ElseRejectSupervisorValidation.Activities.Add(this.RaiseExceptionRejectSupervisorValidation);
            this.ElseRejectSupervisorValidation.Name = "ElseRejectSupervisorValidation";
            // 
            // IfRejectSupervisorValidation
            // 
            this.IfRejectSupervisorValidation.Activities.Add(this.setStateActivity9);
            codecondition2.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.CanProcessBackward);
            this.IfRejectSupervisorValidation.Condition = codecondition2;
            this.IfRejectSupervisorValidation.Name = "IfRejectSupervisorValidation";
            // 
            // faultHandlerActivity8
            // 
            this.faultHandlerActivity8.Activities.Add(this.codeActivity13);
            this.faultHandlerActivity8.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity8.Name = "faultHandlerActivity8";
            // 
            // ElseRejectAnalystInValidation
            // 
            this.ElseRejectAnalystInValidation.Activities.Add(this.RaiseExceptionRejectAnalystInValidation);
            this.ElseRejectAnalystInValidation.Name = "ElseRejectAnalystInValidation";
            // 
            // IfRejectAnalystInValidation
            // 
            this.IfRejectAnalystInValidation.Activities.Add(this.setStateActivity8);
            codecondition3.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.CanProcessBackward);
            this.IfRejectAnalystInValidation.Condition = codecondition3;
            this.IfRejectAnalystInValidation.Name = "IfRejectAnalystInValidation";
            // 
            // faultHandlerActivity4
            // 
            this.faultHandlerActivity4.Activities.Add(this.codeActivity7);
            this.faultHandlerActivity4.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity4.Name = "faultHandlerActivity4";
            // 
            // ElseProceedInValidation
            // 
            this.ElseProceedInValidation.Activities.Add(this.RaiseExceptionProceedInValidation);
            this.ElseProceedInValidation.Name = "ElseProceedInValidation";
            // 
            // IfProceedInValidation
            // 
            this.IfProceedInValidation.Activities.Add(this.setStateActivity5);
            codecondition4.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.CanProcessProceed);
            this.IfProceedInValidation.Condition = codecondition4;
            this.IfProceedInValidation.Name = "IfProceedInValidation";
            // 
            // faultHandlerActivity6
            // 
            this.faultHandlerActivity6.Activities.Add(this.codeActivity10);
            this.faultHandlerActivity6.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity6.Name = "faultHandlerActivity6";
            // 
            // ElseRejectInSupervision
            // 
            this.ElseRejectInSupervision.Activities.Add(this.RaiseExceptionRejectInSupervision);
            this.ElseRejectInSupervision.Activities.Add(this.faultHandlersActivity14);
            this.ElseRejectInSupervision.Name = "ElseRejectInSupervision";
            // 
            // IfRejectInSupervision
            // 
            this.IfRejectInSupervision.Activities.Add(this.setStateActivity1);
            this.IfRejectInSupervision.Activities.Add(this.faultHandlersActivity13);
            codecondition5.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.CanProcessBackward);
            this.IfRejectInSupervision.Condition = codecondition5;
            this.IfRejectInSupervision.Name = "IfRejectInSupervision";
            // 
            // faultHandlerActivity2
            // 
            this.faultHandlerActivity2.Activities.Add(this.codeActivity3);
            this.faultHandlerActivity2.FaultType = typeof(SpiritucSI.BPN.AII.Workflow.WFException);
            this.faultHandlerActivity2.Name = "faultHandlerActivity2";
            // 
            // ElseProceedInSupervision
            // 
            this.ElseProceedInSupervision.Activities.Add(this.RaiseExceptionInSupervision);
            this.ElseProceedInSupervision.Activities.Add(this.cancellationHandlerActivity6);
            this.ElseProceedInSupervision.Activities.Add(this.faultHandlersActivity12);
            this.ElseProceedInSupervision.Name = "ElseProceedInSupervision";
            // 
            // faultHandlersActivity10
            // 
            this.faultHandlersActivity10.Activities.Add(this.faultHandlerActivity10);
            this.faultHandlersActivity10.Name = "faultHandlersActivity10";
            // 
            // IfProceedInSup
            // 
            this.IfProceedInSup.Activities.Add(this.setStateActivity3);
            this.IfProceedInSup.Activities.Add(this.cancellationHandlerActivity5);
            this.IfProceedInSup.Activities.Add(this.faultHandlersActivity11);
            codecondition6.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.CanProcessProceed);
            this.IfProceedInSup.Condition = codecondition6;
            this.IfProceedInSup.Name = "IfProceedInSup";
            // 
            // faultHandlerActivity3
            // 
            this.faultHandlerActivity3.Activities.Add(this.codeActivity2);
            this.faultHandlerActivity3.FaultType = typeof(System.Exception);
            this.faultHandlerActivity3.Name = "faultHandlerActivity3";
            // 
            // ElseReassignInAnalisys
            // 
            this.ElseReassignInAnalisys.Activities.Add(this.RaiseExceptionReassignInAnalisys);
            this.ElseReassignInAnalisys.Name = "ElseReassignInAnalisys";
            // 
            // IfReassignInAnalisys
            // 
            this.IfReassignInAnalisys.Activities.Add(this.setStateActivity4);
            codecondition7.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.CanProcessBackward);
            this.IfReassignInAnalisys.Condition = codecondition7;
            this.IfReassignInAnalisys.Name = "IfReassignInAnalisys";
            // 
            // faultHandlerActivity1
            // 
            this.faultHandlerActivity1.Activities.Add(this.codeActivity1);
            this.faultHandlerActivity1.FaultType = typeof(System.Exception);
            this.faultHandlerActivity1.Name = "faultHandlerActivity1";
            // 
            // ElseInAnalisys
            // 
            this.ElseInAnalisys.Activities.Add(this.RaiseProceedExceptionInAnalisys);
            this.ElseInAnalisys.Name = "ElseInAnalisys";
            // 
            // IfInAnalisys
            // 
            this.IfInAnalisys.Activities.Add(this.setStateActivity2);
            codecondition8.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.CanProcessProceed);
            this.IfInAnalisys.Condition = codecondition8;
            this.IfInAnalisys.Name = "IfInAnalisys";
            // 
            // codeActivity11
            // 
            this.codeActivity11.Description = "Este código é executado sempre que um estado é iniciado lançando um evento com es" +
                "sa informação";
            this.codeActivity11.Name = "codeActivity11";
            this.codeActivity11.ExecuteCode += new System.EventHandler(this.RaiseInstanceStateChange);
            // 
            // faultHandlersActivity7
            // 
            this.faultHandlersActivity7.Activities.Add(this.faultHandlerActivity7);
            this.faultHandlersActivity7.Name = "faultHandlersActivity7";
            // 
            // ConditionProceedInConclusion
            // 
            this.ConditionProceedInConclusion.Activities.Add(this.IfProceedInConclusion);
            this.ConditionProceedInConclusion.Activities.Add(this.ElseProceedInConclusion);
            this.ConditionProceedInConclusion.Name = "ConditionProceedInConclusion";
            activitybind1.Name = "WF_Analisys";
            activitybind1.Path = "ProcessManager";
            // 
            // handleExternalEventActivity4
            // 
            this.handleExternalEventActivity4.Description = "Validar Análise";
            this.handleExternalEventActivity4.EventName = "OnConclusionOccured";
            this.handleExternalEventActivity4.InterfaceType = typeof(SpiritucSI.BPN.AII.Workflow.IWFEvents);
            this.handleExternalEventActivity4.Name = "handleExternalEventActivity4";
            this.handleExternalEventActivity4.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            // 
            // faultHandlersActivity2
            // 
            this.faultHandlersActivity2.Activities.Add(this.faultHandlerActivity5);
            this.faultHandlersActivity2.Name = "faultHandlersActivity2";
            // 
            // setStateActivity6
            // 
            this.setStateActivity6.Name = "setStateActivity6";
            this.setStateActivity6.TargetStateName = "InAnalisys";
            activitybind2.Name = "WF_Analisys";
            activitybind2.Path = "PoolManager";
            // 
            // handleExternalEventOnAlocatedAnalist
            // 
            this.handleExternalEventOnAlocatedAnalist.Description = "Alocar Análise";
            this.handleExternalEventOnAlocatedAnalist.EventName = "OnAllocateAnalyst";
            this.handleExternalEventOnAlocatedAnalist.InterfaceType = typeof(SpiritucSI.BPN.AII.Workflow.IWFEvents);
            this.handleExternalEventOnAlocatedAnalist.Name = "handleExternalEventOnAlocatedAnalist";
            this.handleExternalEventOnAlocatedAnalist.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            // 
            // codeActivity6
            // 
            this.codeActivity6.Description = "Este código é executado sempre que um estado é iniciado lançando um evento com es" +
                "sa informação";
            this.codeActivity6.Name = "codeActivity6";
            this.codeActivity6.ExecuteCode += new System.EventHandler(this.RaiseInstanceStateChange);
            // 
            // faultHandlersActivity9
            // 
            this.faultHandlersActivity9.Activities.Add(this.faultHandlerActivity9);
            this.faultHandlersActivity9.Name = "faultHandlersActivity9";
            // 
            // ConditionRejectSupervisorValidation
            // 
            this.ConditionRejectSupervisorValidation.Activities.Add(this.IfRejectSupervisorValidation);
            this.ConditionRejectSupervisorValidation.Activities.Add(this.ElseRejectSupervisorValidation);
            this.ConditionRejectSupervisorValidation.Name = "ConditionRejectSupervisorValidation";
            activitybind3.Name = "WF_Analisys";
            activitybind3.Path = "ProcessManager";
            // 
            // handleExternalEventActivity6
            // 
            this.handleExternalEventActivity6.Description = "noBulk";
            this.handleExternalEventActivity6.EventName = "OnValidationRejectedSupervisor";
            this.handleExternalEventActivity6.InterfaceType = typeof(SpiritucSI.BPN.AII.Workflow.IWFEvents);
            this.handleExternalEventActivity6.Name = "handleExternalEventActivity6";
            this.handleExternalEventActivity6.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            // 
            // faultHandlersActivity8
            // 
            this.faultHandlersActivity8.Activities.Add(this.faultHandlerActivity8);
            this.faultHandlersActivity8.Name = "faultHandlersActivity8";
            // 
            // ConditionRejectAnalystInValidation
            // 
            this.ConditionRejectAnalystInValidation.Activities.Add(this.IfRejectAnalystInValidation);
            this.ConditionRejectAnalystInValidation.Activities.Add(this.ElseRejectAnalystInValidation);
            this.ConditionRejectAnalystInValidation.Name = "ConditionRejectAnalystInValidation";
            activitybind4.Name = "WF_Analisys";
            activitybind4.Path = "ProcessManager";
            // 
            // handleExternalEventValidationRejectedAnalyst
            // 
            this.handleExternalEventValidationRejectedAnalyst.Description = "noBulk";
            this.handleExternalEventValidationRejectedAnalyst.EventName = "OnValidationRejectedAnalyst";
            this.handleExternalEventValidationRejectedAnalyst.InterfaceType = typeof(SpiritucSI.BPN.AII.Workflow.IWFEvents);
            this.handleExternalEventValidationRejectedAnalyst.Name = "handleExternalEventValidationRejectedAnalyst";
            this.handleExternalEventValidationRejectedAnalyst.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind4)));
            // 
            // codeActivity5
            // 
            this.codeActivity5.Description = "Este código é executado sempre que um estado é iniciado lançando um evento com es" +
                "sa informação";
            this.codeActivity5.Name = "codeActivity5";
            this.codeActivity5.ExecuteCode += new System.EventHandler(this.RaiseInstanceStateChange);
            // 
            // faultHandlersActivity3
            // 
            this.faultHandlersActivity3.Activities.Add(this.faultHandlerActivity4);
            this.faultHandlersActivity3.Name = "faultHandlersActivity3";
            // 
            // ConditionProceedInValidation
            // 
            this.ConditionProceedInValidation.Activities.Add(this.IfProceedInValidation);
            this.ConditionProceedInValidation.Activities.Add(this.ElseProceedInValidation);
            this.ConditionProceedInValidation.Name = "ConditionProceedInValidation";
            activitybind5.Name = "WF_Analisys";
            activitybind5.Path = "ProcessManager";
            // 
            // HandleAnalisysAcceptence
            // 
            this.HandleAnalisysAcceptence.Description = "Validar Análise";
            this.HandleAnalisysAcceptence.EventName = "OnValidationOccured";
            this.HandleAnalisysAcceptence.InterfaceType = typeof(SpiritucSI.BPN.AII.Workflow.IWFEvents);
            this.HandleAnalisysAcceptence.Name = "HandleAnalisysAcceptence";
            this.HandleAnalisysAcceptence.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind5)));
            // 
            // cancellationHandlerActivity4
            // 
            this.cancellationHandlerActivity4.Name = "cancellationHandlerActivity4";
            // 
            // faultHandlersActivity6
            // 
            this.faultHandlersActivity6.Activities.Add(this.faultHandlerActivity6);
            this.faultHandlersActivity6.Name = "faultHandlersActivity6";
            // 
            // ConditionRejectInSupervision
            // 
            this.ConditionRejectInSupervision.Activities.Add(this.IfRejectInSupervision);
            this.ConditionRejectInSupervision.Activities.Add(this.ElseRejectInSupervision);
            this.ConditionRejectInSupervision.Name = "ConditionRejectInSupervision";
            activitybind6.Name = "WF_Analisys";
            activitybind6.Path = "Supervisor";
            // 
            // handleExternalEventActivity2
            // 
            this.handleExternalEventActivity2.Description = "noBulk";
            this.handleExternalEventActivity2.EventName = "OnSupervisionReject";
            this.handleExternalEventActivity2.InterfaceType = typeof(SpiritucSI.BPN.AII.Workflow.IWFEvents);
            this.handleExternalEventActivity2.Name = "handleExternalEventActivity2";
            this.handleExternalEventActivity2.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind6)));
            // 
            // cancellationHandlerActivity2
            // 
            this.cancellationHandlerActivity2.Name = "cancellationHandlerActivity2";
            // 
            // faultHandlersActivity4
            // 
            this.faultHandlersActivity4.Activities.Add(this.faultHandlerActivity2);
            this.faultHandlersActivity4.Name = "faultHandlersActivity4";
            // 
            // ConditionProceedInSupervision
            // 
            this.ConditionProceedInSupervision.Activities.Add(this.IfProceedInSup);
            this.ConditionProceedInSupervision.Activities.Add(this.faultHandlersActivity10);
            this.ConditionProceedInSupervision.Activities.Add(this.ElseProceedInSupervision);
            this.ConditionProceedInSupervision.Name = "ConditionProceedInSupervision";
            activitybind7.Name = "WF_Analisys";
            activitybind7.Path = "Supervisor";
            // 
            // handleExternalEventActivity1
            // 
            this.handleExternalEventActivity1.Description = "AllowBulk";
            this.handleExternalEventActivity1.EventName = "OnSupervisionAproval";
            this.handleExternalEventActivity1.InterfaceType = typeof(SpiritucSI.BPN.AII.Workflow.IWFEvents);
            this.handleExternalEventActivity1.Name = "handleExternalEventActivity1";
            this.handleExternalEventActivity1.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind7)));
            // 
            // codeActivity4
            // 
            this.codeActivity4.Description = "Este código é executado sempre que um estado é iniciado lançando um evento com es" +
                "sa informação";
            this.codeActivity4.Name = "codeActivity4";
            this.codeActivity4.ExecuteCode += new System.EventHandler(this.RaiseInstanceStateChange);
            // 
            // codeActivity9
            // 
            this.codeActivity9.Description = "Este código é executado sempre que um estado é iniciado lançando um evento com es" +
                "sa informação";
            this.codeActivity9.Name = "codeActivity9";
            this.codeActivity9.ExecuteCode += new System.EventHandler(this.RaiseInstanceStateChange);
            // 
            // cancellationHandlerActivity3
            // 
            this.cancellationHandlerActivity3.Name = "cancellationHandlerActivity3";
            // 
            // faultHandlersActivity5
            // 
            this.faultHandlersActivity5.Activities.Add(this.faultHandlerActivity3);
            this.faultHandlersActivity5.Name = "faultHandlersActivity5";
            // 
            // ConditionReassignInAnalisys
            // 
            this.ConditionReassignInAnalisys.Activities.Add(this.IfReassignInAnalisys);
            this.ConditionReassignInAnalisys.Activities.Add(this.ElseReassignInAnalisys);
            this.ConditionReassignInAnalisys.Name = "ConditionReassignInAnalisys";
            activitybind8.Name = "WF_Analisys";
            activitybind8.Path = "ProcessManager";
            // 
            // handleExternalEventActivity3
            // 
            this.handleExternalEventActivity3.Description = "noBulk";
            this.handleExternalEventActivity3.EventName = "OnAnalisysReassign";
            this.handleExternalEventActivity3.InterfaceType = typeof(SpiritucSI.BPN.AII.Workflow.IWFEvents);
            this.handleExternalEventActivity3.Name = "handleExternalEventActivity3";
            this.handleExternalEventActivity3.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind8)));
            // 
            // InitializeState
            // 
            this.InitializeState.Description = "Este código é executado sempre que um estado é iniciado lançando um evento com es" +
                "sa informação";
            this.InitializeState.Name = "InitializeState";
            this.InitializeState.ExecuteCode += new System.EventHandler(this.RaiseInstanceStateChange);
            // 
            // cancellationHandlerActivity1
            // 
            this.cancellationHandlerActivity1.Name = "cancellationHandlerActivity1";
            // 
            // faultHandlersActivity1
            // 
            this.faultHandlersActivity1.Activities.Add(this.faultHandlerActivity1);
            this.faultHandlersActivity1.Name = "faultHandlersActivity1";
            // 
            // ConditionInAnalisys
            // 
            this.ConditionInAnalisys.Activities.Add(this.IfInAnalisys);
            this.ConditionInAnalisys.Activities.Add(this.ElseInAnalisys);
            this.ConditionInAnalisys.Name = "ConditionInAnalisys";
            activitybind9.Name = "WF_Analisys";
            activitybind9.Path = "Analist";
            // 
            // HandleAnalisysSubmitted
            // 
            this.HandleAnalisysSubmitted.Description = "Submeter Aprovação";
            this.HandleAnalisysSubmitted.EventName = "OnAnalisysSubmited";
            this.HandleAnalisysSubmitted.InterfaceType = typeof(SpiritucSI.BPN.AII.Workflow.IWFEvents);
            this.HandleAnalisysSubmitted.Name = "HandleAnalisysSubmitted";
            this.HandleAnalisysSubmitted.SetBinding(System.Workflow.Activities.HandleExternalEventActivity.RolesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind9)));
            // 
            // InConclusionInitialization
            // 
            this.InConclusionInitialization.Activities.Add(this.codeActivity11);
            this.InConclusionInitialization.Name = "InConclusionInitialization";
            // 
            // OnConclusionOccured
            // 
            this.OnConclusionOccured.Activities.Add(this.handleExternalEventActivity4);
            this.OnConclusionOccured.Activities.Add(this.ConditionProceedInConclusion);
            this.OnConclusionOccured.Activities.Add(this.faultHandlersActivity7);
            this.OnConclusionOccured.Description = "Concluir Processo";
            this.OnConclusionOccured.Name = "OnConclusionOccured";
            // 
            // onAllocatedAnalyst
            // 
            this.onAllocatedAnalyst.Activities.Add(this.handleExternalEventOnAlocatedAnalist);
            this.onAllocatedAnalyst.Activities.Add(this.setStateActivity6);
            this.onAllocatedAnalyst.Activities.Add(this.faultHandlersActivity2);
            this.onAllocatedAnalyst.Description = "Alocar Análise";
            this.onAllocatedAnalyst.Name = "onAllocatedAnalyst";
            // 
            // InAllocationStateInitialization
            // 
            this.InAllocationStateInitialization.Activities.Add(this.codeActivity6);
            this.InAllocationStateInitialization.Name = "InAllocationStateInitialization";
            // 
            // OnValidationRejectedSupervisor
            // 
            this.OnValidationRejectedSupervisor.Activities.Add(this.handleExternalEventActivity6);
            this.OnValidationRejectedSupervisor.Activities.Add(this.ConditionRejectSupervisorValidation);
            this.OnValidationRejectedSupervisor.Activities.Add(this.faultHandlersActivity9);
            this.OnValidationRejectedSupervisor.Description = "Devolver ao Supervisor";
            this.OnValidationRejectedSupervisor.Name = "OnValidationRejectedSupervisor";
            // 
            // OnValidationRejectedAnalyst
            // 
            this.OnValidationRejectedAnalyst.Activities.Add(this.handleExternalEventValidationRejectedAnalyst);
            this.OnValidationRejectedAnalyst.Activities.Add(this.ConditionRejectAnalystInValidation);
            this.OnValidationRejectedAnalyst.Activities.Add(this.faultHandlersActivity8);
            this.OnValidationRejectedAnalyst.Description = "Devolver ao Analista";
            this.OnValidationRejectedAnalyst.Name = "OnValidationRejectedAnalyst";
            // 
            // InValidationStateInitialization
            // 
            this.InValidationStateInitialization.Activities.Add(this.codeActivity5);
            this.InValidationStateInitialization.Name = "InValidationStateInitialization";
            // 
            // OnValidationOccured
            // 
            this.OnValidationOccured.Activities.Add(this.HandleAnalisysAcceptence);
            this.OnValidationOccured.Activities.Add(this.ConditionProceedInValidation);
            this.OnValidationOccured.Activities.Add(this.faultHandlersActivity3);
            this.OnValidationOccured.Description = "Efectuar Validação";
            this.OnValidationOccured.Name = "OnValidationOccured";
            // 
            // OnSupervisionReject
            // 
            this.OnSupervisionReject.Activities.Add(this.handleExternalEventActivity2);
            this.OnSupervisionReject.Activities.Add(this.ConditionRejectInSupervision);
            this.OnSupervisionReject.Activities.Add(this.faultHandlersActivity6);
            this.OnSupervisionReject.Activities.Add(this.cancellationHandlerActivity4);
            this.OnSupervisionReject.Description = "Devolver Análise";
            this.OnSupervisionReject.Name = "OnSupervisionReject";
            // 
            // OnSupervisionAproval
            // 
            this.OnSupervisionAproval.Activities.Add(this.handleExternalEventActivity1);
            this.OnSupervisionAproval.Activities.Add(this.ConditionProceedInSupervision);
            this.OnSupervisionAproval.Activities.Add(this.faultHandlersActivity4);
            this.OnSupervisionAproval.Activities.Add(this.cancellationHandlerActivity2);
            this.OnSupervisionAproval.Description = "Aprovar Análise";
            this.OnSupervisionAproval.Name = "OnSupervisionAproval";
            // 
            // InSupervisionStateInitialization
            // 
            this.InSupervisionStateInitialization.Activities.Add(this.codeActivity4);
            this.InSupervisionStateInitialization.Name = "InSupervisionStateInitialization";
            // 
            // InFinishInitialization
            // 
            this.InFinishInitialization.Activities.Add(this.codeActivity9);
            this.InFinishInitialization.Name = "InFinishInitialization";
            // 
            // OnAnalisysReassign
            // 
            this.OnAnalisysReassign.Activities.Add(this.handleExternalEventActivity3);
            this.OnAnalisysReassign.Activities.Add(this.ConditionReassignInAnalisys);
            this.OnAnalisysReassign.Activities.Add(this.faultHandlersActivity5);
            this.OnAnalisysReassign.Activities.Add(this.cancellationHandlerActivity3);
            this.OnAnalisysReassign.Description = "Devolver Pool Manager";
            this.OnAnalisysReassign.Name = "OnAnalisysReassign";
            // 
            // InAnalisysStateInitialization
            // 
            this.InAnalisysStateInitialization.Activities.Add(this.InitializeState);
            this.InAnalisysStateInitialization.Name = "InAnalisysStateInitialization";
            // 
            // OnAnalisysSubmited
            // 
            this.OnAnalisysSubmited.Activities.Add(this.HandleAnalisysSubmitted);
            this.OnAnalisysSubmited.Activities.Add(this.ConditionInAnalisys);
            this.OnAnalisysSubmited.Activities.Add(this.faultHandlersActivity1);
            this.OnAnalisysSubmited.Activities.Add(this.cancellationHandlerActivity1);
            this.OnAnalisysSubmited.Description = "Submeter Aprovação";
            this.OnAnalisysSubmited.Name = "OnAnalisysSubmited";
            // 
            // InConclusion
            // 
            this.InConclusion.Activities.Add(this.OnConclusionOccured);
            this.InConclusion.Activities.Add(this.InConclusionInitialization);
            this.InConclusion.Name = "InConclusion";
            // 
            // InAllocation
            // 
            this.InAllocation.Activities.Add(this.InAllocationStateInitialization);
            this.InAllocation.Activities.Add(this.onAllocatedAnalyst);
            this.InAllocation.Description = "Pool Manager select an alnalyst";
            this.InAllocation.Name = "InAllocation";
            // 
            // InValidation
            // 
            this.InValidation.Activities.Add(this.OnValidationOccured);
            this.InValidation.Activities.Add(this.InValidationStateInitialization);
            this.InValidation.Activities.Add(this.OnValidationRejectedAnalyst);
            this.InValidation.Activities.Add(this.OnValidationRejectedSupervisor);
            this.InValidation.Name = "InValidation";
            // 
            // InSupervision
            // 
            this.InSupervision.Activities.Add(this.InSupervisionStateInitialization);
            this.InSupervision.Activities.Add(this.OnSupervisionAproval);
            this.InSupervision.Activities.Add(this.OnSupervisionReject);
            this.InSupervision.Name = "InSupervision";
            // 
            // Finish
            // 
            this.Finish.Activities.Add(this.InFinishInitialization);
            this.Finish.Name = "Finish";
            // 
            // InAnalisys
            // 
            this.InAnalisys.Activities.Add(this.OnAnalisysSubmited);
            this.InAnalisys.Activities.Add(this.InAnalisysStateInitialization);
            this.InAnalisys.Activities.Add(this.OnAnalisysReassign);
            this.InAnalisys.Name = "InAnalisys";
            // 
            // WF_Analisys
            // 
            this.Activities.Add(this.InAnalisys);
            this.Activities.Add(this.Finish);
            this.Activities.Add(this.InSupervision);
            this.Activities.Add(this.InValidation);
            this.Activities.Add(this.InAllocation);
            this.Activities.Add(this.InConclusion);
            this.CompletedStateName = null;
            this.DynamicUpdateCondition = null;
            this.InitialStateName = "InAllocation";
            this.Name = "WF_Analisys";
            this.CanModifyActivities = false;

        }

        #endregion

        private FaultHandlersActivity faultHandlersActivity14;
        private FaultHandlersActivity faultHandlersActivity13;
        private CodeActivity RaiseExceptionRejectInSupervision;
        private IfElseBranchActivity ElseRejectInSupervision;
        private IfElseBranchActivity IfRejectInSupervision;
        private IfElseActivity ConditionRejectInSupervision;
        private CodeActivity RaiseExceptionProceedInValidation;
        private IfElseBranchActivity ElseProceedInValidation;
        private IfElseBranchActivity IfProceedInValidation;
        private IfElseActivity ConditionProceedInValidation;
        private CodeActivity RaiseExceptionRejectAnalystInValidation;
        private IfElseBranchActivity ElseRejectAnalystInValidation;
        private IfElseBranchActivity IfRejectAnalystInValidation;
        private IfElseActivity ConditionRejectAnalystInValidation;
        private CodeActivity RaiseExceptionRejectSupervisorValidation;
        private IfElseBranchActivity ElseRejectSupervisorValidation;
        private IfElseBranchActivity IfRejectSupervisorValidation;
        private IfElseActivity ConditionRejectSupervisorValidation;
        private CodeActivity RaiseExceptionProceedInConclusion;
        private IfElseBranchActivity ElseProceedInConclusion;
        private IfElseBranchActivity IfProceedInConclusion;
        private IfElseActivity ConditionProceedInConclusion;
        private CodeActivity RaiseProceedExceptionInAnalisys;
        private IfElseBranchActivity ElseInAnalisys;
        private IfElseBranchActivity IfInAnalisys;
        private IfElseActivity ConditionInAnalisys;
        private IfElseBranchActivity ElseReassignInAnalisys;
        private IfElseBranchActivity IfReassignInAnalisys;
        private IfElseActivity ConditionReassignInAnalisys;
        private CodeActivity RaiseExceptionReassignInAnalisys;
        private CancellationHandlerActivity cancellationHandlerActivity6;
        private FaultHandlersActivity faultHandlersActivity12;
        private FaultHandlerActivity faultHandlerActivity12;
        private CodeActivity codeActivity18;
        private CodeActivity RaiseExceptionInSupervision;
        private IfElseBranchActivity ElseProceedInSupervision;
        private CodeActivity codeActivity15;
        private FaultHandlerActivity faultHandlerActivity10;
        private FaultHandlersActivity faultHandlersActivity10;
        private CodeActivity codeActivity16;
        private FaultHandlerActivity faultHandlerActivity11;
        private FaultHandlersActivity faultHandlersActivity11;
        private CancellationHandlerActivity cancellationHandlerActivity5;
        private IfElseBranchActivity IfProceedInSup;
        private IfElseActivity ConditionProceedInSupervision;
        private CodeActivity codeActivity12;
        private FaultHandlerActivity faultHandlerActivity7;
        private CodeActivity codeActivity11;
        private FaultHandlersActivity faultHandlersActivity7;
        private SetStateActivity setStateActivity7;
        private HandleExternalEventActivity handleExternalEventActivity4;
        private StateInitializationActivity InConclusionInitialization;
        private EventDrivenActivity OnConclusionOccured;
        private StateActivity InConclusion;
        private CodeActivity codeActivity13;
        private FaultHandlerActivity faultHandlerActivity8;
        private FaultHandlersActivity faultHandlersActivity8;
        private SetStateActivity setStateActivity8;
        private HandleExternalEventActivity handleExternalEventValidationRejectedAnalyst;
        private EventDrivenActivity OnValidationRejectedAnalyst;
        private CodeActivity codeActivity14;
        private FaultHandlerActivity faultHandlerActivity9;
        private FaultHandlersActivity faultHandlersActivity9;
        private SetStateActivity setStateActivity9;
        private HandleExternalEventActivity handleExternalEventActivity6;
        private EventDrivenActivity OnValidationRejectedSupervisor;
        private CodeActivity codeActivity10;
        private FaultHandlerActivity faultHandlerActivity6;
        private CancellationHandlerActivity cancellationHandlerActivity4;
        private FaultHandlersActivity faultHandlersActivity6;
        private SetStateActivity setStateActivity1;
        private HandleExternalEventActivity handleExternalEventActivity2;
        private EventDrivenActivity OnSupervisionReject;
        private CodeActivity codeActivity9;
        private StateInitializationActivity InFinishInitialization;
        private CodeActivity codeActivity8;
        private CodeActivity codeActivity7;
        private FaultHandlerActivity faultHandlerActivity5;
        private FaultHandlerActivity faultHandlerActivity4;
        private CodeActivity codeActivity4;
        private CodeActivity codeActivity6;
        private CodeActivity codeActivity5;
        private StateActivity InAllocation;
        private StateInitializationActivity InAllocationStateInitialization;
        private FaultHandlersActivity faultHandlersActivity2;
        private HandleExternalEventActivity handleExternalEventOnAlocatedAnalist;
        private CodeActivity InitializeState;
        private EventDrivenActivity onAllocatedAnalyst;
        private StateInitializationActivity InAnalisysStateInitialization;
        private StateActivity InAnalisys;
        private FaultHandlersActivity faultHandlersActivity3;
        private StateInitializationActivity InSupervisionStateInitialization;
        private CodeActivity codeActivity1;
        private FaultHandlerActivity faultHandlerActivity1;
        private CancellationHandlerActivity cancellationHandlerActivity1;
        private FaultHandlersActivity faultHandlersActivity1;
        private SetStateActivity setStateActivity2;
        private HandleExternalEventActivity HandleAnalisysSubmitted;
        private EventDrivenActivity OnAnalisysSubmited;
        private CodeActivity codeActivity2;
        private FaultHandlerActivity faultHandlerActivity3;
        private CancellationHandlerActivity cancellationHandlerActivity3;
        private FaultHandlersActivity faultHandlersActivity5;
        private SetStateActivity setStateActivity4;
        private HandleExternalEventActivity handleExternalEventActivity3;
        private EventDrivenActivity OnAnalisysReassign;
        private CodeActivity codeActivity3;
        private FaultHandlerActivity faultHandlerActivity2;
        private CancellationHandlerActivity cancellationHandlerActivity2;
        private FaultHandlersActivity faultHandlersActivity4;
        private SetStateActivity setStateActivity3;
        private HandleExternalEventActivity handleExternalEventActivity1;
        private EventDrivenActivity OnSupervisionAproval;
        private SetStateActivity setStateActivity5;
        private SetStateActivity setStateActivity6;
        private StateInitializationActivity InValidationStateInitialization;
        private HandleExternalEventActivity HandleAnalisysAcceptence;
        private EventDrivenActivity OnValidationOccured;
        private StateActivity InValidation;
        private StateActivity InSupervision;
        private StateActivity Finish;











































































































































































































    }
}
