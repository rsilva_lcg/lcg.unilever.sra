﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Workflow.Activities;

namespace SpiritucSI.BPN.AII.Workflow
{
    public class WorkflowAudit 
    {
        #region CONSTRUCTORS
        public WorkflowAudit()
        {
           
        }
        #endregion

        #region CRUD METHODS

        public void Insert( Guid InstanceID, int EventOrder, string UserName)
        {
            this.Insert(InstanceID, EventOrder, UserName, DateTime.Now);
        }
        public void Insert(Guid InstanceID, int EventOrder, string UserName, DateTime EventDate)
        {
            try
            {
                WFDBDataContext ctx = new WFDBDataContext();

                AuditingTrackingEvent ATE = new AuditingTrackingEvent();
                ATE.WorkflowID = InstanceID;
                ATE.EventOrder = EventOrder;
                ATE.Username = UserName;
                ATE.EventDate = EventDate;

                if (!ctx.AuditingTrackingEvents.Contains(ATE))
                {
                    ctx.AuditingTrackingEvents.InsertOnSubmit(ATE);
                }
                ctx.SubmitChanges();

            }
            catch (Exception ex)
            {
                throw new WFException ("BLL Unexpected Error", ex);
            }
        }
        
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="InstanceID"></param>
        ///// <returns></returns>
        //public List<vw_WorkflowAudit> SelectEvents( Guid InstanceID )
        //{
        //    try
        //    {
        //        return (
        //            from vw_WorkflowAudit wa in _context.vw_WorkflowAudit
        //            where wa.WorkflowInstanceId == InstanceID
        //            orderby wa.EventDateTime descending
        //            select wa
        //        ).ToList<vw_WorkflowAudit>();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new XeroxBusinessException("BLL Unexpected Error", ex);
        //    }
        //}
        //public List<vw_WorkflowAudit> SelectEvents( Guid InstanceID, Type type )
        //{
        //    try
        //    {
        //        return (
        //            from vw_WorkflowAudit wa in _context.vw_WorkflowAudit
        //            where (wa.WorkflowInstanceId == InstanceID)
        //               && (wa.TypeFullName == type.FullName)
        //            orderby wa.EventDateTime descending
        //            select wa
        //        ).ToList <vw_WorkflowAudit>();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new XeroxBusinessException("BLL Unexpected Error", ex);
        //    }
        //}
        //public List<vw_WorkflowAudit> SelectStates( Guid InstanceID )
        //{
        //    return SelectEvents(InstanceID,typeof(StateActivity));
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="Event"></param>
        ///// <returns></returns>
        //public List<vw_WorkflowAudit> SelectChildEvents( vw_WorkflowAudit Event )
        //{
        //    return this.SelectChildEvents(Event, false);
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="Event"></param>
        ///// <param name="AllLevels"></param>
        ///// <returns></returns>
        //public List<vw_WorkflowAudit> SelectChildEvents( vw_WorkflowAudit Event, bool AllLevels )
        //{
        //    try
        //    {
        //        List<vw_WorkflowAudit> res = new List<vw_WorkflowAudit>();
        //        if (Event.ParentContextGuid != Event.ContextGuid)
        //        {
        //            res = (
        //                from vw_WorkflowAudit wa in _context.vw_WorkflowAudit
        //                    where wa.ParentContextGuid == Event.ContextGuid
        //                    orderby wa.EventDateTime descending
        //                    select wa
        //            ).ToList<vw_WorkflowAudit>();

        //            if (AllLevels)
        //            {
        //                List<vw_WorkflowAudit> temp = new List<vw_WorkflowAudit>();
        //                foreach (vw_WorkflowAudit c in res)
        //                {
        //                    List<vw_WorkflowAudit> childs = this.SelectChildEvents(c, true);
        //                    temp = (
        //                        (from vw_WorkflowAudit v in temp
        //                         select v
        //                         ).Union(
        //                            from vw_WorkflowAudit vv in childs
        //                            select vv
        //                        )
        //                    ).ToList<vw_WorkflowAudit>();
        //                }
        //                res = ((from vw_WorkflowAudit v in res
        //                       select v
        //                        ).Union(
        //                            from vw_WorkflowAudit vv in temp
        //                            select vv
        //                        )).ToList<vw_WorkflowAudit>();                                
        //            }                    
        //        }
        //        return (
        //             from vw_WorkflowAudit a in res
        //             orderby a.EventDateTime descending
        //             select a
        //         ).ToList<vw_WorkflowAudit>();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new XeroxBusinessException("BLL Unexpected Error", ex);
        //    }
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="Event"></param>
        ///// <param name="type"></param>
        ///// <returns></returns>
        //public List<vw_WorkflowAudit> SelectChildEvents( vw_WorkflowAudit Event, Type type )
        //{
        //    return this.SelectChildEvents(Event, type, false);
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="Event"></param>
        ///// <param name="type"></param>
        ///// <param name="AllLevels"></param>
        ///// <returns></returns>
        //public List<vw_WorkflowAudit> SelectChildEvents( vw_WorkflowAudit Event, Type type, bool AllLevels )
        //{
        //    try
        //    {
        //        List<vw_WorkflowAudit> res = new List<vw_WorkflowAudit>();
        //        if (Event.ParentContextGuid != Event.ContextGuid)
        //        {
        //            res = (
        //                from vw_WorkflowAudit wa in _context.vw_WorkflowAudit
        //                where wa.ParentContextGuid == Event.ContextGuid
        //                orderby wa.EventDateTime descending
        //                select wa
        //            ).ToList<vw_WorkflowAudit>();

        //            if (AllLevels)
        //            {
        //                List<vw_WorkflowAudit> temp = new List<vw_WorkflowAudit>();
        //                foreach (vw_WorkflowAudit c in res)
        //                {
        //                    List<vw_WorkflowAudit> childs = this.SelectChildEvents(c, type, true);
        //                    temp = (
        //                        (from vw_WorkflowAudit v in temp
        //                         select v
        //                         ).Union(
        //                            from vw_WorkflowAudit vv in childs
        //                            select vv
        //                        )
        //                    ).ToList<vw_WorkflowAudit>();
        //                }
        //                res = ((from vw_WorkflowAudit v in res
        //                        select v
        //                        ).Union(
        //                            from vw_WorkflowAudit vv in temp
        //                            select vv
        //                        )).ToList<vw_WorkflowAudit>();  
        //            }   
        //        }
        //        return (
        //            from vw_WorkflowAudit a in res
        //            where a.TypeFullName == type.FullName
        //            orderby a.EventDateTime descending
        //            select a
        //        ).ToList<vw_WorkflowAudit>();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new XeroxBusinessException("BLL Unexpected Error", ex);
        //    }
        //}
      
        #endregion
    }
}
