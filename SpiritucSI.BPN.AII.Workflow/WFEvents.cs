﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities;

namespace SpiritucSI.BPN.AII.Workflow
{
    /// <summary>
    /// Implementação do interface abaixo com a lista de eventos disponíveis para o workflow
    /// </summary>
    [Serializable]
    public class WFEvents : IWFEvents
	{
        #region IWFEvents Members

            public event EventHandler<WFEventsArgs> OnAllocateAnalyst;
            public void AllocateAnalyst(Guid instanceId)
            {
                if (OnAllocateAnalyst != null)
                    OnAllocateAnalyst(null, new WFEventsArgs(instanceId));
            }

            public event EventHandler<WFEventsArgs> OnAnalisysSubmited;
            public void AnalisysSubmited(Guid instanceId)
            {
                if (OnAnalisysSubmited != null)
                    OnAnalisysSubmited(null, new WFEventsArgs(instanceId));
            }

            public event EventHandler<WFEventsArgs> OnAnalisysReassign;
            public void AnalisysReassign(Guid instanceId)
            {
                if (OnAnalisysReassign != null)
                    OnAnalisysReassign(null, new WFEventsArgs(instanceId));
            }

            public event EventHandler<WFEventsArgs> OnSupervisionAproval;
            public void SupervisionAproval(Guid instanceId)
            {
                if (OnSupervisionAproval != null)
                    OnSupervisionAproval(null, new WFEventsArgs(instanceId));
            }

            public event EventHandler<WFEventsArgs> OnSupervisionReject;
            public void SupervisionReject(Guid instanceId)
            {
                if (OnSupervisionReject != null)
                    OnSupervisionReject(null, new WFEventsArgs(instanceId));
            }

            public event EventHandler<WFEventsArgs> OnValidationOccured;
            public void ValidationOccured(Guid instanceId)
            {
                if (OnValidationOccured != null)
                    OnValidationOccured(null, new WFEventsArgs(instanceId));
            }

            public event EventHandler<WFEventsArgs> OnValidationRejectedAnalyst;
            public void ValidationRejectedAnalyst(Guid instanceId)
            {
                if (OnValidationRejectedAnalyst != null)
                    OnValidationRejectedAnalyst(null, new WFEventsArgs(instanceId));
            }
            public event EventHandler<WFEventsArgs> OnValidationRejectedSupervisor;
            public void ValidationRejectedSupervisor(Guid instanceId)
            {
                if (OnValidationRejectedSupervisor != null)
                    OnValidationRejectedSupervisor(null, new WFEventsArgs(instanceId));
            }

            public event EventHandler<WFEventsArgs> OnConclusionOccured;
            public void ConclusionOccured(Guid instanceId)
            {
                if (OnConclusionOccured != null)
                    OnConclusionOccured(null, new WFEventsArgs(instanceId));
            }
        #endregion
    }

    /// <summary>
    /// Declaração do Interface de Eventos a disponibilizar para o workflow
    /// </summary>
    [ExternalDataExchange]
    public interface IWFEvents
    {
        #region INALLOCATION EVENTS
        event EventHandler<WFEventsArgs> OnAllocateAnalyst;
        void AllocateAnalyst(Guid instanceId);
        #endregion

        #region INANALISYS EVENTS
        event EventHandler<WFEventsArgs> OnAnalisysSubmited;
        void AnalisysSubmited(Guid instanceId);
        
        event EventHandler<WFEventsArgs> OnAnalisysReassign;
        void AnalisysReassign(Guid instanceId);
        #endregion

        #region INSUPERVISION EVENTS
        event EventHandler<WFEventsArgs> OnSupervisionAproval;
        void SupervisionAproval(Guid instanceId);

        event EventHandler<WFEventsArgs> OnSupervisionReject;
        void SupervisionReject(Guid instanceId);
        #endregion

        #region INVALIDATION EVENTS
        event EventHandler<WFEventsArgs> OnValidationOccured;
        void ValidationOccured(Guid instanceId);
        event EventHandler<WFEventsArgs> OnValidationRejectedAnalyst;
        void ValidationRejectedAnalyst(Guid instanceId);
        event EventHandler<WFEventsArgs> OnValidationRejectedSupervisor;
        void ValidationRejectedSupervisor(Guid instanceId);
        #endregion

        #region INVALIDATION EVENTS
        event EventHandler<WFEventsArgs> OnConclusionOccured;
        void ConclusionOccured(Guid instanceId);
        #endregion
    }
}
