﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
using System.Workflow.Runtime.Hosting;
//using SpiritucSI.BPN.AII.Business.OtherClasses;
//using SpiritucSI.BPN.AII.Business;

namespace SpiritucSI.BPN.AII.Workflow
{
    public sealed partial class WF_Analisys : StateMachineWorkflowActivity
    {
        public WF_Analisys()
        {
            InitializeComponent();
        }


        #region ROLE COLLECTIONS
        /// <summary>
        /// Declaração da(s) WorkflowRoleCollection(s) 
        /// A estas WorkflowRoleCollections são adicionados WebWorkflowRoles que representam 
        ///  cada Role existente na aplicação.
        /// Posteriormente em Design podem configurar-se os HandleExternalEventActivity para 
        ///  que só possam ser executados pelos Roles presentes numa dada WorkflowRoleCollection
        /// </summary>
        //private WorkflowRoleCollection WrcRole1 = new WorkflowRoleCollection();
        //public WorkflowRoleCollection Role1
        //{
        //    get
        //    {
        //        // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
        //        //  de uma qualquer classe do business
        //        this.WrcRole1.Add(new WebWorkflowRole("NOME-DO-ROLE"));
        //        return this.WrcRole1;
        //    }
        //}

        private WorkflowRoleCollection WrcPoolManager = new WorkflowRoleCollection();
        public WorkflowRoleCollection PoolManager
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcPoolManager.Add(new WebWorkflowRole("PoolManager"));
                return this.WrcPoolManager;
            }
        }

        private WorkflowRoleCollection WrcAnalist = new WorkflowRoleCollection();
        public WorkflowRoleCollection Analist
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcAnalist.Add(new WebWorkflowRole("Analyst"));
                return this.WrcAnalist;
            }
        }

        private WorkflowRoleCollection WrcProcessManager = new WorkflowRoleCollection();
        public WorkflowRoleCollection ProcessManager
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcProcessManager.Add(new WebWorkflowRole("ProcessManager"));
                return this.WrcProcessManager;
            }
        }

        private WorkflowRoleCollection WrcSupervisor = new WorkflowRoleCollection();
        public WorkflowRoleCollection Supervisor
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSupervisor.Add(new WebWorkflowRole("Supervisor"));
                return this.WrcSupervisor;
            }
        }
        private WorkflowRoleCollection WrcDirector = new WorkflowRoleCollection();
        public WorkflowRoleCollection Director
        {
            get
            {
                // Podemos ter o nome do Role como variáveis num resx ou como propriedades estáticas
                //  de uma qualquer classe do business
                this.WrcSupervisor.Add(new WebWorkflowRole("Director"));
                return this.WrcSupervisor;
            }
        }

        #endregion

        #region EVENTS HANDLING
        /// <summary>
        /// Declaração dos event handlers do workflow 
        /// Posteriormente, no código onde se tratar da execução deste workflow
        ///  estes eventos devem ser registados para que possam ser apanhados caso sejam lançados
        /// </summary>
        public static event EventHandler<ExceptionEventArgs> OnWorkflowException;
        public static void WorkflowException(Exception e)
        {
            if (OnWorkflowException != null)
                OnWorkflowException(null, new ExceptionEventArgs(e));
        }

        public static event EventHandler<InstanceStateChangeEventArgs> OnInstanceStateChange;
        public static void InstanceStateChange(Guid InstanceID, StateActivity State)
        {
            if (OnInstanceStateChange != null)
                OnInstanceStateChange(null, new InstanceStateChangeEventArgs(InstanceID, State));
        }
        #endregion

        #region PRIVATE METHODS
        /// <summary>
        /// Declaração dos métodos disponíveis dentro do workflow
        /// Estes métodos podem servir para:
        ///     - lançar eventos que são depois apanhados no código onde se trata da execução do workflow
        ///     - executar uma série de outras acções (ex: consultar uma BD ou enviar um email)
        /// </summary>
        private void RaiseWorkflowException(object sender, EventArgs e)
        {
            FaultHandlerActivity faultHandler = ((Activity)sender).Parent as FaultHandlerActivity;
            WF_Analisys.WorkflowException(faultHandler.Fault);
        }

        private void RaiseInstanceStateChange(object sender, EventArgs e)
        {
            Activity c = (Activity)sender;
            while (c != null && c.GetType() != typeof(StateActivity))
            {
                c = c.Parent;
            }
            WF_Analisys.InstanceStateChange(this.WorkflowInstanceId, (StateActivity)c);
        }

        /// <summary>
        /// Neste método está um exemplo de um lançamento de uma excepção invocado não de um FaultHandler
        ///  mas de um método invocado por uma qualquer actividade do wf
        /// </summary>
        private void DoSomething(object sender, EventArgs e)
        {
            try
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage("root@spirituc.com", "lmota@spirituc.com");
                msg.Subject = "Mail sent by my workflow";
                msg.Body = "Mail sent by my workflow";

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("spif-email");
                smtp.Send(msg);
            }
            catch (Exception ex)
            {
                WFTreatedException treatedEx = new WFTreatedException("Erro a enviar email", ex);
                WF_Analisys.WorkflowException(treatedEx);
            }
        }

        private void RaiseProceedExeption(object sender, EventArgs e)
        {
            throw new WFProceedConditions("Cannot proceeed whith acction");
        }

        /// <summary>
        /// Declaração de um método para aplicar a lógicas de condição (IfElse, ConditionedActivityGroup, etc)
        /// A resposta deve ser enviado na propriedade Result do argumento e
        /// </summary>
        private void CanProcessProceed(object sender, ConditionalEventArgs e)
        {
            //e.Result = true;
            WorkflowConditionsBPN b = new WorkflowConditionsBPN();
            e.Result = b.canProcessProceed(this.WorkflowInstanceId);
        }

        private void CanProcessBackward(object sender, ConditionalEventArgs e)
        {
            //e.Result = true;
            WorkflowConditionsBPN b = new WorkflowConditionsBPN();
            e.Result = b.canProcessBackward(this.WorkflowInstanceId);
        }
        #endregion

    }

    [Serializable]
    public class WFAction
    {
        public ExternalDataEventArgs Args { get; set; }
        public Delegate Deleg { get; set; }
        public WFEvents Servs { get; set; }
        public string Text { get; set; }
        public Guid Id { get; set; }
        public bool Visibility { get; set; }
        public string Custom { get; set; }
    }

}
