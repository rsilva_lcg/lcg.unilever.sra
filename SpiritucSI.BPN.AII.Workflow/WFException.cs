﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Workflow.Activities;

namespace SpiritucSI.BPN.AII.Workflow
{
    [Serializable]
    public class WFException : System.Exception
    {
        public WFException(): base() { }
        public WFException( string message ): base(message) { }
        public WFException( string message , Exception ex ) : base(message, ex) { }
        public WFException( SerializationInfo info, StreamingContext context ) : base(info, context) { }
    }

    [Serializable]
    public class WFTreatedException : System.Exception
    {
        public WFTreatedException(): base() { }
        public WFTreatedException( string message ): base(message) { }
        public WFTreatedException( string message , Exception ex ) : base(message, ex) { }
        public WFTreatedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class WFInvalidActionException : WFException, ISerializable
    {
        public WFInvalidActionException() : base() { }
        public WFInvalidActionException( string message ) : base(message) { }
        public WFInvalidActionException( string message, Exception ex ) : base(message, ex) { }
        public WFInvalidActionException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class WFProceedConditions : WFException, ISerializable
    {
        public WFProceedConditions() : base() { }
        public WFProceedConditions(string message) : base(message) { }
        public WFProceedConditions(string message, Exception ex) : base(message, ex) { }
        public WFProceedConditions(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
