--  ################################################################## 
--  #  ESTE FICHEIRO DESTINA-SE A TRANSFORMAR O MODELO DE DADOS      # 
--  #  DA VERSÃO 2.1 PARA A VERSÃO 2.2                               #
--  #    - ADICIONAR AS DATAS DE INICIO E FIM DOS LE's               # 
--  #    - REMOVER NUMERO DE SEQUENCIA                               #
--  #    - TRANSFORMAR E MIGRAR OS DADOS PARA A NOVA VERSÂO          #
--  #                                                                # 
--  #  O SCRIPT AQUI DESENVOLVIDO DEVE SER EXECUTADO SEMPRE QUE FOR  # 
--  #  NECESSÁRIO ACTUALIZAR A VERSÃO 2.1 DA BASE DE DADOS           # 
--  #  PARA EXECUTAR PRIMIR (F5)                                     # 
--  #                                                                # 
--  ################################################################## 


-- #######################################################################################################
-- #######################################################################################################
-- ##                                                                                                   ##
-- ##  ALTERAR ESTRUTURA DAS TABELAS                                                                    ##
-- ##                                                                                                   ##
-- #######################################################################################################
-- #######################################################################################################

    -- TABELA T_ENTIDADE
    -- --------------------------------------------------------
        ALTER TABLE T_ENTIDADE
         ADD (DATA_INICIO  DATE);

        ALTER TABLE T_ENTIDADE
         ADD (DATA_FIM  DATE);
        

    -- TABELA T_CONTRATO
    -- --------------------------------------------------------
         --DROP INDEX IDX_T_CONTRATO_2;
         --DROP INDEX IDX_T_CONTRATO_4;

        ALTER TABLE T_CONTRATO
        RENAME COLUMN NIVEL_CODIGO TO COD_GRUPO_ECONOMICO;

        ALTER TABLE T_CONTRATO
        MODIFY(COD_GRUPO_ECONOMICO VARCHAR2(3 BYTE));

        ALTER TABLE T_CONTRATO
         ADD (COD_SUBGRUPO_ECONOMICO  VARCHAR2(3 BYTE));


    -- TABELA T_CONTRATO_APAGADO
    -- --------------------------------------------------------
        ALTER TABLE T_CONTRATO_APAGADO
        RENAME COLUMN NIVEL_CODIGO TO COD_GRUPO_ECONOMICO;

        ALTER TABLE T_CONTRATO_APAGADO
        MODIFY(COD_GRUPO_ECONOMICO VARCHAR2(3 BYTE));

        ALTER TABLE T_CONTRATO_APAGADO
         ADD (COD_SUBGRUPO_ECONOMICO  VARCHAR2(3 BYTE));
COMMIT;



-- #######################################################################################################
-- #######################################################################################################
-- ##                                                                                                   ##
-- ##  CRIAÇÃO DAS PROCEDURES PACKAGE SRA                                                               ##
-- ##                                                                                                   ##
-- #######################################################################################################
-- #######################################################################################################

CREATE OR REPLACE PACKAGE PK_SRA AS
-- ----------------------------------------------------------------------------
    PROCEDURE SP_BONUS_INS
    (
         V_ID_CONTRAPARTIDA     INTEGER
        ,V_VALOR_LIMITE_MIN     INTEGER
        ,V_DESCONTO             NUMBER
        ,V_COMP_CONCESSIONARIO  NUMBER
        ,V_TIPO                 CHAR
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_BONUS_GLOBAL_INS
    (
         V_ID_CONTRATO           INTEGER
        ,V_VALOR_LIMITE_MIN      INTEGER
        ,V_DESCONTO              NUMBER
        ,V_COMP_CONCESSIONARIO   NUMBER
        ,V_TIPO                  CHAR
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRAPARTIDA_INS
    (
         V_ID_CONTRATO                 INTEGER
        ,V_ID_CONTRAPARTIDA   IN  OUT  INTEGER
        ,V_COD_GRUPO_PRODUTO           VARCHAR2
        ,V_PREVISAO_VENDAS             INTEGER
        ,V_DESCONTO_FACTURA            NUMBER
        ,V_COMP_CONCESSIONARIO         NUMBER
        ,V_PRAZO_PAGAMENTO             CHAR
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_GET
    (
         V_ID_CONTRATO                   INT
        ,CUR_CONTRATO                OUT SYS_REFCURSOR
        ,CUR_ENTIDADE                OUT SYS_REFCURSOR
        ,CUR_EXPLORACAO              OUT SYS_REFCURSOR
        ,CUR_BONUS                   OUT SYS_REFCURSOR
        ,CUR_BONUS_GLOBAL            OUT SYS_REFCURSOR
        ,CUR_CONT_EQUIPAMENTO        OUT SYS_REFCURSOR
        ,CUR_CONT_MAT_VISIBILIDADE   OUT SYS_REFCURSOR
        ,CUR_CONTRAPARTIDAS          OUT SYS_REFCURSOR
        ,CUR_FASEAMENTO_PAG          OUT SYS_REFCURSOR
        ,CUR_GAMAS_QUANTIA_FIM       OUT SYS_REFCURSOR
        ,CUR_GAMAS_GLOBAL            OUT SYS_REFCURSOR
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATOS_GET 
    (
         WHERESTM    VARCHAR2
        ,CUR     OUT SYS_REFCURSOR 
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_GET_PREVIOUS
    (
         V_ID_CONTRATO           INT
        ,V_NEW_ID_CONTRATO  OUT  INT
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_GET_NEXT
    (
         V_ID_CONTRATO           INT
        ,V_NEW_ID_CONTRATO  OUT  INT
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_INS
    (
         V_CONCESSIONARIO            INT
        ,V_ID_CONTRATO       IN OUT  INT
        ,V_ID_PROCESSO       IN OUT  VARCHAR2
        ,V_CLASSIFICACAO_CONTRATO    CHAR
        ,V_TIPO_CONTRATO             VARCHAR2
        ,V_DATA_CONTRATO             DATE
        ,V_NIVEL_CONTRATO            VARCHAR2
        ,V_COD_GRUPO_ECONOMICO       VARCHAR2
        ,V_COD_SUBGRUPO_ECONOMICO    VARCHAR2
        ,V_NUM_PROCESSO_ANTERIOR     VARCHAR2
        ,V_NUM_PROCESSO_SEGUINTE     VARCHAR2
        ,V_STATUS                    CHAR
        ,V_OBSERVACOES               VARCHAR2
        ,V_OBSERVACOES_MOTIVO        VARCHAR2
        ,V_NOME_FIRMA                VARCHAR2
        ,V_MORADA_SEDE               VARCHAR2
        ,V_LOCALIDADE                VARCHAR2
        ,V_RAMO_ACTIVIDADE           VARCHAR2
        ,V_MATRICULA                 VARCHAR2
        ,V_MATRICULA_NUM             VARCHAR2
        ,V_CAPITAL_SOCIAL            VARCHAR2
        ,V_NIF                       VARCHAR2
        ,V_DESIGNACAO_PT_VENDA       VARCHAR2
        ,V_MORADA_PT_VENDA           VARCHAR2
        ,V_OBJECTIVO                 VARCHAR2
        ,V_IS_POOC                   CHAR
        ,V_DATA_INICIO               DATE
        ,V_DATA_FIM                  DATE
        ,V_DATA_FIM_EFECTIVO         DATE
        ,V_QUANTIA_FIM               INT
        ,V_ANOS_RENOVACAO            INT
        ,V_IS_CONTRATO_FORMAL        CHAR
        ,V_IS_FORN_FRIO_IGLO         CHAR
        ,V_PREVISAO_VENDAS_GLOBAL    INTEGER
        ,V_PRAZO_PAGAMENTO           CHAR
        ,V_NIB_CLIENTE               VARCHAR2
        ,V_BANCO                     VARCHAR2
        ,V_BANCO_DEPENDENCIA         VARCHAR2
        ,V_COMP_CONC_COMPRAS         NUMBER
        ,V_INSERT_USER               VARCHAR2
        ,V_EXISTS            OUT     INT
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_STATUS_UPD
    (
         V_ID_CONTRATO           INT
        ,V_STATUS                CHAR
        ,V_MOTIVO                VARCHAR2
        ,V_DATA_FIM_EFECTIVO     DATE
        ,V_NUM_PROCESSO_SEGUINTE VARCHAR2
        ,V_ALTER_USER            VARCHAR2
    );

-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_DEL
    (
         V_ID_CONTRATO INT
    );
    
-- ----------------------------------------------------------------------------    
    PROCEDURE SP_ENTIDADE_DEL 
    (
         V_ID_CONTRATO   INT
        ,V_COD_LE        VARCHAR2
    );
    
-- ----------------------------------------------------------------------------    
    PROCEDURE SP_ENTIDADE_GET 
    (
         V_ID_CONTRATO   INT
        ,CUR       OUT   SYS_REFCURSOR
    ); 
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_ENTIDADE_INS
    (
         V_ID_CONTRATO               INT
        ,V_DATA_INICIO               DATE
        ,V_DATA_FIM                  DATE
        ,V_COD_LE                    VARCHAR2
        ,V_COD_GRUPO_ECONOMICO       VARCHAR2
        ,V_COD_SUB_GRUPO_ECONOMICO   VARCHAR2
        ,V_COD_CLIENTE               VARCHAR2
    );
    
-- ----------------------------------------------------------------------------    
    PROCEDURE SP_ENTIDADES_DEL 
    (
        V_ID_CONTRATO   INT
    ); 
   
-- ----------------------------------------------------------------------------
    PROCEDURE SP_ENTIDADES_GET 
    (
         WHERESTM    VARCHAR2
        ,CUR     OUT SYS_REFCURSOR
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_EQUIPAMENTO_GET (CUR OUT SYS_REFCURSOR);

-- ----------------------------------------------------------------------------
    PROCEDURE SP_EQUIPAMENTO_INS
    (
         V_ID_CONTRATO       INT
        ,V_COD_EQUIPAMENTO   VARCHAR2
        ,V_QUANTIDADE        INTEGER
    );
  
-- ----------------------------------------------------------------------------
    PROCEDURE SP_EXPLORACAO_INS
    (
         V_ID_CONTRATO       INT
        ,V_TIPO_EXPLORACAO   VARCHAR2
    );
  
-- ----------------------------------------------------------------------------
    PROCEDURE SP_FASEAMENTO_INS
    (
         V_ID_CONTRATO               INT
        ,V_DATA_PAGAMENTO            DATE
        ,V_DATA_PAGAMENTO_EFECTUADO  DATE
        ,V_VALOR_PAGAMENTO           NUMBER
        ,V_COMP_CONCESSIONARIO       NUMBER
        ,V_TIPO_PG                   VARCHAR2
        ,V_TTS                       VARCHAR2
    );
 
-- ----------------------------------------------------------------------------
    PROCEDURE SP_GAMA_GLOBAL_INS
    (
         V_ID_CONTRATO       INT
        ,V_COD_GRUPO_PRODUTO VARCHAR2
    );
 
-- ----------------------------------------------------------------------------
    PROCEDURE SP_GAMA_INS
    (
         V_ID_CONTRATO       INT
        ,V_COD_GRUPO_PRODUTO VARCHAR2
    );
  
-- ----------------------------------------------------------------------------
    PROCEDURE SP_GRUPO_ECONOMICO_GET
    (
        CUR OUT SYS_REFCURSOR
    );

-- ----------------------------------------------------------------------------
    PROCEDURE SP_SUBGRUPO_ECONOMICO_GET
    (
         V_ID_GRUPO_ECONOMICO    VARCHAR2
        ,CUR             OUT     SYS_REFCURSOR
    );

-- ----------------------------------------------------------------------------
    PROCEDURE SP_GRUPO_PRODUTO_GET 
    (
        CUR OUT SYS_REFCURSOR
    );

-- ----------------------------------------------------------------------------
    PROCEDURE SP_MATERIAL_INS
    (
         V_ID_CONTRATO   INTEGER
        ,V_COD_MATERIAL  INTEGER
        ,V_QUNTIDADE     INTEGER
        ,V_VALOR         NUMBER
        ,V_OBSERVACOES   VARCHAR2
    );

-- ----------------------------------------------------------------------------
    PROCEDURE SP_MATERIAL_VISIBILIDADE_GET 
    (
        CUR OUT SYS_REFCURSOR
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_TTS_GET 
    (
        CUR OUT SYS_REFCURSOR
    );
    
-- ----------------------------------------------------------------------------
    PROCEDURE SSIS_LOAD_TMP_FACT_SRA_PL;
    
-- ----------------------------------------------------------------------------
    PROCEDURE SSIS_LOAD_TMP_SRA_PROMOCOES;
    
-- ----------------------------------------------------------------------------
    PROCEDURE SSIS_LOAD_TMP_SRA_VENDAS;
    
-- ----------------------------------------------------------------------------
    PROCEDURE SSIS_LOAD_TMP_SRA_VENDAS_T;

END PK_SRA;
/


-- ###################################################################################################
-- BODY
-- ###################################################################################################

CREATE OR REPLACE PACKAGE BODY PK_SRA AS

-- ----------------------------------------------------------------------------
    PROCEDURE SP_BONUS_INS
    (
         V_ID_CONTRAPARTIDA     INTEGER
        ,V_VALOR_LIMITE_MIN     INTEGER
        ,V_DESCONTO             NUMBER
        ,V_COMP_CONCESSIONARIO  NUMBER
        ,V_TIPO                 CHAR
    ) IS
    BEGIN
        INSERT INTO T_BONUS
        (
            ID_CONTRAPARTIDA,
            ID_BONUS,
            VALOR_LIMITE_MIN,
            DESCONTO,
            COMP_CONCESSIONARIO,
            TIPO
        )
        VALUES
        (
            V_ID_CONTRAPARTIDA,
            SEQ_ID_BONUS.NEXTVAL,
            V_VALOR_LIMITE_MIN,
            V_DESCONTO,
            V_COMP_CONCESSIONARIO,
            V_TIPO        
        );
    END;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_BONUS_GLOBAL_INS
    (
         V_ID_CONTRATO           INTEGER
        ,V_VALOR_LIMITE_MIN      INTEGER
        ,V_DESCONTO              NUMBER
        ,V_COMP_CONCESSIONARIO   NUMBER
        ,V_TIPO                  CHAR
    ) IS
    BEGIN
        INSERT INTO T_BONUS_GLOBAL
        (
            ID_CONTRATO,
            ID_BONUS,
            VALOR_LIMITE_MIN,
            DESCONTO,
            COMP_CONCESSIONARIO,
            TIPO
        )
        VALUES
        (
            V_ID_CONTRATO,
            SEQ_ID_BONUS.NEXTVAL,
            V_VALOR_LIMITE_MIN,
            V_DESCONTO,
            V_COMP_CONCESSIONARIO,
            V_TIPO
        );
    END;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRAPARTIDA_INS
    (
         V_ID_CONTRATO                 INTEGER
        ,V_ID_CONTRAPARTIDA   IN  OUT  INTEGER
        ,V_COD_GRUPO_PRODUTO           VARCHAR2
        ,V_PREVISAO_VENDAS             INTEGER
        ,V_DESCONTO_FACTURA            NUMBER
        ,V_COMP_CONCESSIONARIO         NUMBER
        ,V_PRAZO_PAGAMENTO             CHAR
    ) IS
        ID INT:=NULL;
    BEGIN
        SELECT SEQ_ID_CONTRAPARTIDA.NEXTVAL INTO ID FROM DUAL;

        INSERT INTO T_CONTRAPARTIDAS
        (
           ID_CONTRATO,
           ID_CONTRAPARTIDA,
           COD_GRUPO_PRODUTO,
           PREVISAO_VENDAS,
           DESCONTO_FACTURA,
           COMP_CONCESSIONARIO,
           PRAZO_PAGAMENTO
        )
        VALUES
        (
            V_ID_CONTRATO,
            ID,
            V_COD_GRUPO_PRODUTO,
            V_PREVISAO_VENDAS,
            V_DESCONTO_FACTURA,
            V_COMP_CONCESSIONARIO,
            V_PRAZO_PAGAMENTO
        );

        V_ID_CONTRAPARTIDA := ID;
    END;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_GET
    (
         V_ID_CONTRATO                   INT
        ,CUR_CONTRATO                OUT SYS_REFCURSOR
        ,CUR_ENTIDADE                OUT SYS_REFCURSOR
        ,CUR_EXPLORACAO              OUT SYS_REFCURSOR
        ,CUR_BONUS                   OUT SYS_REFCURSOR
        ,CUR_BONUS_GLOBAL            OUT SYS_REFCURSOR
        ,CUR_CONT_EQUIPAMENTO        OUT SYS_REFCURSOR
        ,CUR_CONT_MAT_VISIBILIDADE   OUT SYS_REFCURSOR
        ,CUR_CONTRAPARTIDAS          OUT SYS_REFCURSOR
        ,CUR_FASEAMENTO_PAG          OUT SYS_REFCURSOR
        ,CUR_GAMAS_QUANTIA_FIM       OUT SYS_REFCURSOR
        ,CUR_GAMAS_GLOBAL            OUT SYS_REFCURSOR
    ) IS
    BEGIN 
        SP_ENTIDADE_GET(V_ID_CONTRATO,CUR_ENTIDADE);
        
        OPEN CUR_CONTRATO  FOR  
            SELECT *       
            FROM T_CONTRATO                                  
            WHERE ID_CONTRATO = V_ID_CONTRATO;
            
        OPEN CUR_EXPLORACAO FOR  
            SELECT *       
            FROM T_EXPLORACAO                                
            WHERE ID_CONTRATO = V_ID_CONTRATO;
            
        OPEN CUR_BONUS FOR  
            SELECT B.*     
            FROM T_BONUS B,  T_CONTRAPARTIDAS C              
            WHERE ID_CONTRATO = V_ID_CONTRATO 
              AND B.ID_CONTRAPARTIDA = C.ID_CONTRAPARTIDA ;
                  
        OPEN CUR_BONUS_GLOBAL FOR  
            SELECT B.*     
            FROM T_BONUS_GLOBAL B                            
            WHERE ID_CONTRATO = V_ID_CONTRATO;
            
        OPEN CUR_CONT_EQUIPAMENTO FOR  
            SELECT T.COD_TIPO_EQUIPAMENTO, T.DES_TIPO_EQUIPAMENTO, C.QUANTIDADE, C.VALOR 
            FROM T_CONT_EQUIPAMENTO C, T_TIPO_EQUIPAMENTO T                                  
            WHERE ID_CONTRATO = V_ID_CONTRATO 
              AND C.COD_EQUIPAMENTO = T.COD_TIPO_EQUIPAMENTO;
              
        OPEN CUR_CONT_MAT_VISIBILIDADE FOR  
            SELECT C.COD_MATERIAL, T.DES_MATERIAL_POS, C.QUANTIDADE, C.VALOR, C.OBSERVACOES 
            FROM T_CONT_MAT_VISIBILIDADE C, TO_MATERIAL_POS T                                 
            WHERE ID_CONTRATO = V_ID_CONTRATO 
              AND C.COD_MATERIAL = T.COD_MATERIAL_POS;
              
        OPEN CUR_CONTRAPARTIDAS FOR  
            SELECT *       
            FROM T_CONTRAPARTIDAS C, T_GRUPO_PRODUTO G       
            WHERE ID_CONTRATO = V_ID_CONTRATO 
            AND C.COD_GRUPO_PRODUTO = G.COD_GRUPO_PRODUTO 
            ORDER BY C.COD_GRUPO_PRODUTO ASC;
            
        OPEN CUR_FASEAMENTO_PAG FOR  
            SELECT *       
            FROM T_FASEAMENTO_PAG                            
            WHERE ID_CONTRATO = V_ID_CONTRATO;
            
        OPEN CUR_GAMAS_QUANTIA_FIM FOR  
            SELECT *       
            FROM T_GAMAS_QUANTIA_FIM                         
            WHERE ID_CONTRATO = V_ID_CONTRATO;
            
        OPEN CUR_GAMAS_GLOBAL FOR  
            SELECT *       
            FROM T_GAMAS_GLOBAL                              
            WHERE ID_CONTRATO = V_ID_CONTRATO;
        COMMIT;
    END;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATOS_GET 
    (
         WHERESTM    VARCHAR2 
        ,CUR     OUT SYS_REFCURSOR 
    ) IS
        sqlstm VARCHAR2(2000);
    BEGIN
        sqlstm :=           'SELECT ID_CONTRATO,';
        sqlstm := sqlstm || ' ID_PROCESSO,';
        sqlstm := sqlstm || ' TIPO_CONTRATO,';
        sqlstm := sqlstm || ' CLASSIFICACAO_CONTRATO,';
        sqlstm := sqlstm || ' TO_CHAR(DATA_CONTRATO,''DD-MM-YYYY'') AS DATA_CONTRATO,';
        sqlstm := sqlstm || ' NIVEL_CONTRATO,';
        sqlstm := sqlstm || ' COD_GRUPO_ECONOMICO,';
        sqlstm := sqlstm || ' COD_SUBGRUPO_ECONOMICO,';
        sqlstm := sqlstm || ' NOME_FIRMA,';
        sqlstm := sqlstm || ' MORADA_SEDE,';
        sqlstm := sqlstm || ' LOCALIDADE,';
        sqlstm := sqlstm || ' RAMO_ACTIVIDADE,';
        sqlstm := sqlstm || ' MATRICULA,';
        sqlstm := sqlstm || ' MATRICULA_NUM,';
        sqlstm := sqlstm || ' CAPITAL_SOCIAL,';
        sqlstm := sqlstm || ' NIF,';
        sqlstm := sqlstm || ' DESIGNACAO_PT_VENDA,';
        sqlstm := sqlstm || ' MORADA_PT_VENDA,';
        sqlstm := sqlstm || ' NUM_PROCESSO_ANTERIOR,';
        sqlstm := sqlstm || ' NUM_PROCESSO_SEGUINTE,';
        sqlstm := sqlstm || ' OBJECTIVO,';
        sqlstm := sqlstm || ' IS_POOC,';
        sqlstm := sqlstm || ' TO_CHAR(DATA_INICIO,''DD-MM-YYYY'') AS DATA_INICIO,';
        sqlstm := sqlstm || ' TO_CHAR(DATA_FIM,''DD-MM-YYYY'') AS DATA_FIM, ';
        sqlstm := sqlstm || ' TO_CHAR(DATA_FIM_EFECTIVO,''DD-MM-YYYY'') as DATA_FIM_EFECTIVO,';
        sqlstm := sqlstm || ' QUANTIA_FIM,';
        sqlstm := sqlstm || ' ANOS_RENOVACAO,';
        sqlstm := sqlstm || ' IS_CONTRATO_FORMAL,';
        sqlstm := sqlstm || ' IS_FORN_FRIO_IGLO,';
        sqlstm := sqlstm || ' PREVISAO_VENDAS_GLOBAL,';
        sqlstm := sqlstm || ' PRAZO_PAGAMENTO,';
        sqlstm := sqlstm || ' NIB_CLIENTE,';
        sqlstm := sqlstm || ' BANCO,';
        sqlstm := sqlstm || ' BANCO_DEPENDENCIA,';
        sqlstm := sqlstm || ' STATUS,';
        sqlstm := sqlstm || ' COMP_CONC_COMPRAS,';
        sqlstm := sqlstm || ' OBSERVACOES,';
        sqlstm := sqlstm || ' OBSERVACOES_MOTIVO';
        sqlstm := sqlstm || ' FROM T_CONTRATO WHERE ';
        sqlstm := sqlstm || WHERESTM;

        OPEN CUR FOR sqlstm;
        COMMIT;
    END;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_GET_PREVIOUS
    (
         V_ID_CONTRATO           INT
        ,V_NEW_ID_CONTRATO  OUT  INT
    ) IS
    BEGIN
        SELECT  ID_CONTRATO INTO V_NEW_ID_CONTRATO
        FROM(
            SELECT  ID_CONTRATO
            FROM T_CONTRATO
            WHERE SUBSTR(ID_PROCESSO,8,8) <  
            (
                SELECT SUBSTR(ID_PROCESSO,8,8)
                FROM T_CONTRATO
                WHERE ID_CONTRATO = V_ID_CONTRATO    
            ) 
            ORDER BY SUBSTR(ID_PROCESSO,8,8) DESC
        ) A
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN 
        SELECT -1 INTO  V_NEW_ID_CONTRATO FROM DUAL;
    END;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_GET_NEXT
    (
         V_ID_CONTRATO           INT
        ,V_NEW_ID_CONTRATO  OUT  INT
    ) IS
    BEGIN        
        SELECT ID_CONTRATO INTO V_NEW_ID_CONTRATO
        FROM(
            SELECT  ID_CONTRATO
            FROM T_CONTRATO
            WHERE SUBSTR(ID_PROCESSO,8,8) >  
            (
                SELECT SUBSTR(ID_PROCESSO,8,8)
                FROM T_CONTRATO
                WHERE ID_CONTRATO = V_ID_CONTRATO    
            ) 
            ORDER BY SUBSTR(ID_PROCESSO,8,8)
        ) A
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN 
        SELECT -1 INTO  V_NEW_ID_CONTRATO FROM DUAL;
    END;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_INS
    (
         V_CONCESSIONARIO            INT
        ,V_ID_CONTRATO       IN OUT  INT
        ,V_ID_PROCESSO       IN OUT  VARCHAR2
        ,V_CLASSIFICACAO_CONTRATO    CHAR
        ,V_TIPO_CONTRATO             VARCHAR2
        ,V_DATA_CONTRATO             DATE
        ,V_NIVEL_CONTRATO            VARCHAR2
        ,V_COD_GRUPO_ECONOMICO       VARCHAR2
        ,V_COD_SUBGRUPO_ECONOMICO    VARCHAR2
        ,V_NUM_PROCESSO_ANTERIOR     VARCHAR2
        ,V_NUM_PROCESSO_SEGUINTE     VARCHAR2
        ,V_STATUS                    CHAR
        ,V_OBSERVACOES               VARCHAR2
        ,V_OBSERVACOES_MOTIVO        VARCHAR2
        ,V_NOME_FIRMA                VARCHAR2
        ,V_MORADA_SEDE               VARCHAR2
        ,V_LOCALIDADE                VARCHAR2
        ,V_RAMO_ACTIVIDADE           VARCHAR2
        ,V_MATRICULA                 VARCHAR2
        ,V_MATRICULA_NUM             VARCHAR2
        ,V_CAPITAL_SOCIAL            VARCHAR2
        ,V_NIF                       VARCHAR2
        ,V_DESIGNACAO_PT_VENDA       VARCHAR2
        ,V_MORADA_PT_VENDA           VARCHAR2
        ,V_OBJECTIVO                 VARCHAR2
        ,V_IS_POOC                   CHAR
        ,V_DATA_INICIO               DATE
        ,V_DATA_FIM                  DATE
        ,V_DATA_FIM_EFECTIVO         DATE
        ,V_QUANTIA_FIM               INT
        ,V_ANOS_RENOVACAO            INT
        ,V_IS_CONTRATO_FORMAL        CHAR
        ,V_IS_FORN_FRIO_IGLO         CHAR
        ,V_PREVISAO_VENDAS_GLOBAL    INTEGER
        ,V_PRAZO_PAGAMENTO           CHAR
        ,V_NIB_CLIENTE               VARCHAR2
        ,V_BANCO                     VARCHAR2
        ,V_BANCO_DEPENDENCIA         VARCHAR2
        ,V_COMP_CONC_COMPRAS         NUMBER
        ,V_INSERT_USER               VARCHAR2
        ,V_EXISTS            OUT     INT
    ) IS
        ID INT:=NULL;
        V_NUM NVARCHAR2(20):=NULL;
    BEGIN
        IF V_ID_PROCESSO IS NULL THEN
            SELECT  TO_NUMBER( MAX( SUBSTR (ID_PROCESSO, 8, 8))) INTO V_ID_PROCESSO FROM T_CONTRATO;

            IF V_ID_PROCESSO IS NULL THEN
                 V_ID_PROCESSO := TO_CHAR(SYSDATE,'YYYY') || LPAD(V_CONCESSIONARIO,3,0) ||'00000001';
            ELSE
                V_ID_PROCESSO := TO_CHAR(SYSDATE,'YYYY') || LPAD(V_CONCESSIONARIO,3,0) || LPAD(V_ID_PROCESSO  + 1,8,0);
            END IF;
        END IF;

        SELECT COUNT(*) INTO V_EXISTS FROM T_CONTRATO WHERE ID_PROCESSO=V_ID_PROCESSO;

        IF V_EXISTS = 0 THEN
            SELECT SEQ_ID_CONTRATO.NEXTVAL INTO ID FROM DUAL;

            INSERT INTO T_CONTRATO
            (
                 ID_CONTRATO
                ,ID_PROCESSO
                ,CLASSIFICACAO_CONTRATO
                ,TIPO_CONTRATO
                ,DATA_CONTRATO
                ,NIVEL_CONTRATO             
                ,COD_GRUPO_ECONOMICO        
                ,COD_SUBGRUPO_ECONOMICO     
                ,NOME_FIRMA                 
                ,MORADA_SEDE                
                ,LOCALIDADE                 
                ,RAMO_ACTIVIDADE            
                ,MATRICULA                  
                ,MATRICULA_NUM              
                ,CAPITAL_SOCIAL             
                ,NIF                        
                ,DESIGNACAO_PT_VENDA        
                ,MORADA_PT_VENDA            
                ,NUM_PROCESSO_ANTERIOR      
                ,NUM_PROCESSO_SEGUINTE      
                ,OBJECTIVO                  
                ,IS_POOC                    
                ,DATA_INICIO                
                ,DATA_FIM                   
                ,DATA_FIM_EFECTIVO          
                ,QUANTIA_FIM                
                ,ANOS_RENOVACAO             
                ,IS_CONTRATO_FORMAL         
                ,IS_FORN_FRIO_IGLO          
                ,PREVISAO_VENDAS_GLOBAL     
                ,PRAZO_PAGAMENTO            
                ,NIB_CLIENTE                
                ,BANCO                      
                ,BANCO_DEPENDENCIA          
                ,STATUS                     
                ,COMP_CONC_COMPRAS          
                ,OBSERVACOES                
                ,OBSERVACOES_MOTIVO         
                ,INSERT_USER                
                ,INSERT_DATE                
                ,IS_ACTIVE                  
                ,IS_IMPORTADO
            )
            VALUES
            (
                 ID                         
                ,V_ID_PROCESSO              
                ,V_CLASSIFICACAO_CONTRATO   
                ,V_TIPO_CONTRATO            
                ,V_DATA_CONTRATO            
                ,V_NIVEL_CONTRATO           
                ,V_COD_GRUPO_ECONOMICO      
                ,V_COD_SUBGRUPO_ECONOMICO   
                ,V_NOME_FIRMA               
                ,V_MORADA_SEDE              
                ,V_LOCALIDADE               
                ,V_RAMO_ACTIVIDADE          
                ,V_MATRICULA                
                ,V_MATRICULA_NUM            
                ,V_CAPITAL_SOCIAL           
                ,V_NIF                      
                ,V_DESIGNACAO_PT_VENDA      
                ,V_MORADA_PT_VENDA          
                ,V_NUM_PROCESSO_ANTERIOR    
                ,V_NUM_PROCESSO_SEGUINTE    
                ,V_OBJECTIVO                
                ,V_IS_POOC                  
                ,V_DATA_INICIO              
                ,V_DATA_FIM                 
                ,V_DATA_FIM_EFECTIVO        
                ,V_QUANTIA_FIM              
                ,V_ANOS_RENOVACAO           
                ,V_IS_CONTRATO_FORMAL       
                ,V_IS_FORN_FRIO_IGLO        
                ,V_PREVISAO_VENDAS_GLOBAL   
                ,V_PRAZO_PAGAMENTO          
                ,V_NIB_CLIENTE              
                ,V_BANCO                    
                ,V_BANCO_DEPENDENCIA        
                ,V_STATUS                   
                ,V_COMP_CONC_COMPRAS        
                ,V_OBSERVACOES              
                ,V_OBSERVACOES_MOTIVO       
                ,V_INSERT_USER              
                ,SYSDATE
                ,'T'    
                ,'F'    
            );
            
            V_ID_CONTRATO := ID;
        END IF;
    END;
   
-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_STATUS_UPD
    (
         V_ID_CONTRATO           INT
        ,V_STATUS                CHAR
        ,V_MOTIVO                VARCHAR2
        ,V_DATA_FIM_EFECTIVO     DATE
        ,V_NUM_PROCESSO_SEGUINTE VARCHAR2
        ,V_ALTER_USER            VARCHAR2
    )IS
    BEGIN
        UPDATE T_CONTRATO T SET T.STATUS = V_STATUS, T.ALTER_DATE = SYSDATE, T.ALTER_USER = V_ALTER_USER, T.NUM_PROCESSO_SEGUINTE = V_NUM_PROCESSO_SEGUINTE, T.IS_IMPORTADO = 'F' WHERE T.ID_CONTRATO = V_ID_CONTRATO;
        IF V_STATUS = 'T' THEN
               UPDATE T_CONTRATO T SET T.DATA_FIM_EFECTIVO = V_DATA_FIM_EFECTIVO WHERE T.ID_CONTRATO = V_ID_CONTRATO;
        END IF;
        IF V_STATUS = 'I' THEN
               UPDATE T_CONTRATO T SET T.DATA_FIM_EFECTIVO = NULL, IS_IMPORTADO='F' WHERE T.ID_CONTRATO = V_ID_CONTRATO;
        END IF;
        IF V_MOTIVO is not null THEN
               UPDATE T_CONTRATO T SET T.OBSERVACOES_MOTIVO = V_MOTIVO WHERE T.ID_CONTRATO = V_ID_CONTRATO;
        END IF; 
        COMMIT;
    END;
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_CONTRATO_DEL
    (
         V_ID_CONTRATO INT
    )IS
    BEGIN
        -- copiar o contrato para a tabela de apagados
        INSERT INTO T_CONTRATO_APAGADO
        SELECT *
        FROM T_CONTRATO
        WHERE ID_CONTRATO =V_ID_CONTRATO ;

        -- apagar os bonus
        DELETE FROM T_BONUS
        WHERE ID_BONUS
        IN
        (
            SELECT b.ID_BONUS 
            FROM T_BONUS b, T_CONTRAPARTIDAS c
            WHERE b.ID_CONTRAPARTIDA = c.ID_CONTRAPARTIDA 
              AND c.ID_CONTRATO = V_ID_CONTRATO
        );

        -- apagar as contrapartidas
        DELETE FROM T_CONTRAPARTIDAS  WHERE ID_CONTRATO = V_ID_CONTRATO ;
        -- apagar as entidades
        DELETE FROM T_ENTIDADE  WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar a exploracao
        DELETE FROM T_EXPLORACAO WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar a bonus global
        DELETE FROM T_BONUS_GLOBAL  WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar cont_equipamento
        DELETE FROM T_CONT_EQUIPAMENTO  WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar cont_mat_visibilidade
        DELETE FROM T_CONT_MAT_VISIBILIDADE WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar faseamento_pagamento
        DELETE FROM T_FASEAMENTO_PAG WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar a gamas_global
        DELETE FROM T_GAMAS_GLOBAL WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar a gamas_quantia_fim
        DELETE FROM T_GAMAS_QUANTIA_FIM WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar o contrato
        DELETE FROM T_CONTRATO WHERE ID_CONTRATO =V_ID_CONTRATO ;
    END;
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_ENTIDADE_DEL 
    (
         V_ID_CONTRATO   INT
        ,V_COD_LE        VARCHAR2
    ) IS
    BEGIN
        DELETE T_ENTIDADE
        WHERE ID_CONTRATO = V_ID_CONTRATO
          AND COD_LE = V_COD_LE;
    END;
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_ENTIDADE_GET 
    (
         V_ID_CONTRATO   INT
        ,CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
                SELECT LE.*,E.DATA_INICIO, E.DATA_FIM
                FROM T_CONTRATO C 
                INNER JOIN T_ENTIDADE E
                    ON(E.ID_CONTRATO = C.ID_CONTRATO)
                INNER JOIN MV_LOCAIS_ENTREGA LE
                    ON(LE.COD_LOCAL_ENTREGA = E.COD_LE)
                WHERE C.ID_CONTRATO = V_ID_CONTRATO
                  AND C.NIVEL_CONTRATO = 'LE'
            UNION 
                SELECT LE.*,TO_DATE('', 'dd-mm-yyyy') DATA_INICIO, TO_DATE('', 'dd-mm-yyyy') DATA_FIM 
                FROM T_CONTRATO C
                INNER JOIN MV_LOCAIS_ENTREGA LE
                    ON(LE.COD_GRUPO_ECONOMICO_LE = C.COD_GRUPO_ECONOMICO)
                WHERE C.ID_CONTRATO = V_ID_CONTRATO
                  AND C.NIVEL_CONTRATO = 'GE'
            UNION 
                SELECT LE.*,TO_DATE('', 'dd-mm-yyyy') DATA_INICIO, TO_DATE('', 'dd-mm-yyyy') DATA_FIM  
                FROM T_CONTRATO C
                INNER JOIN MV_LOCAIS_ENTREGA LE
                    ON(LE.COD_GRUPO_ECONOMICO_LE = C.COD_GRUPO_ECONOMICO)
                    AND(LE.COD_SUBGRUPO_ECONOMICO_LE = C.COD_SUBGRUPO_ECONOMICO)
                WHERE C.ID_CONTRATO = V_ID_CONTRATO
                  AND C.NIVEL_CONTRATO = 'SGE';
        COMMIT;    
    END;
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_ENTIDADE_INS
    (
         V_ID_CONTRATO               INT
        ,V_DATA_INICIO               DATE
        ,V_DATA_FIM                  DATE
        ,V_COD_LE                    VARCHAR2
        ,V_COD_GRUPO_ECONOMICO       VARCHAR2
        ,V_COD_SUB_GRUPO_ECONOMICO   VARCHAR2
        ,V_COD_CLIENTE               VARCHAR2
    ) IS
    BEGIN
        INSERT INTO T_ENTIDADE
        (
             ID_CONTRATO               
            ,DATA_INICIO               
            ,DATA_FIM                  
            ,COD_LE                    
            ,COD_GRUPO_ECONOMICO       
            ,COD_SUB_GRUPO_ECONOMICO   
            ,COD_CLIENTE               
            ,INSERT_DATE
        )
        VALUES
        (
             V_ID_CONTRATO                 
            ,V_DATA_INICIO                 
            ,V_DATA_FIM                    
            ,V_COD_LE                      
            ,V_COD_GRUPO_ECONOMICO         
            ,V_COD_SUB_GRUPO_ECONOMICO     
            ,V_COD_CLIENTE                 
            ,SYSDATE
        );
    END;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_ENTIDADES_DEL 
    (
        V_ID_CONTRATO   INT
    ) IS
    BEGIN
        DELETE T_ENTIDADE
        WHERE ID_CONTRATO = V_ID_CONTRATO;
    END;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_ENTIDADES_GET 
    (
         WHERESTM     VARCHAR2
        ,CUR         OUT SYS_REFCURSOR
    ) IS
        sqlstm VARCHAR2(1000);
    BEGIN 
        sqlstm := 'SELECT M.*,TO_DATE('''', ''dd-mm-yyyy'') DATA_INICIO,TO_DATE('''' ,''dd-mm-yyyy'') DATA_FIM FROM MV_LOCAIS_ENTREGA M WHERE ' || WHERESTM;
        OPEN CUR FOR sqlstm;
        COMMIT;
    END ;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_EQUIPAMENTO_GET 
    (
        CUR OUT SYS_REFCURSOR
    )IS
    BEGIN 
        OPEN CUR FOR SELECT * FROM T_TIPO_EQUIPAMENTO;
    COMMIT;
   END;
   
-- ----------------------------------------------------------------------------
    PROCEDURE SP_EQUIPAMENTO_INS
    (
        V_ID_CONTRATO         INT   
        ,V_COD_EQUIPAMENTO      VARCHAR2   
        ,V_QUANTIDADE            INTEGER
    )IS
        V_VALOR INT := NULL;
    BEGIN
        SELECT TE.VALOR INTO V_VALOR FROM T_TIPO_EQUIPAMENTO TE WHERE TE.COD_TIPO_EQUIPAMENTO = V_COD_EQUIPAMENTO;

        INSERT INTO T_CONT_EQUIPAMENTO
        (
             ID_CONTRATO         
            ,ID_CONT_EQUIPAMENTO 
            ,COD_EQUIPAMENTO     
            ,QUANTIDADE          
            ,VALOR
        )
        VALUES
        (
             V_ID_CONTRATO       
            ,SEQ_ID_CONT_EQUIPAMENTO.NEXTVAL
            ,V_COD_EQUIPAMENTO    
            ,V_QUANTIDADE         
            ,V_VALOR
        );
    END ;

-- ----------------------------------------------------------------------------
    PROCEDURE SP_EXPLORACAO_INS
    (
         V_ID_CONTRATO           INT         
        ,V_TIPO_EXPLORACAO      VARCHAR2
    )IS
    BEGIN
        INSERT INTO T_EXPLORACAO
        (
             ID_CONTRATO         
            ,ID_EXPLORACAO         
            ,TIPO_EXPLORACAO
        )
        VALUES
        (
             V_ID_CONTRATO             
            ,SEQ_ID_EXPLORACAO.NEXTVAL             
            ,V_TIPO_EXPLORACAO
        );
    END ;
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_FASEAMENTO_INS
    (
         V_ID_CONTRATO              INT      
        ,V_DATA_PAGAMENTO           DATE     
        ,V_DATA_PAGAMENTO_EFECTUADO DATE     
        ,V_VALOR_PAGAMENTO          NUMBER   
        ,V_COMP_CONCESSIONARIO      NUMBER   
        ,V_TIPO_PG                  VARCHAR2 
        ,V_TTS                      VARCHAR2
     )IS
     BEGIN
        INSERT INTO T_FASEAMENTO_PAG
        (
             ID_CONTRATO         
            ,ID_FASEAMENTO        
            ,DATA_PAGAMENTO       
            ,DATA_PAGAMENTO_EFECTUADO       
            ,VALOR_PAGAMENTO      
            ,COMP_CONCESSIONARIO 
            ,TIPO_PG             
            ,TTS
            ,INSERT_DATE
        )
        VALUES
        (
             V_ID_CONTRATO          
            ,SEQ_ID_FASEAMENTO.NEXTVAL         
            ,V_DATA_PAGAMENTO        
            ,V_DATA_PAGAMENTO_EFECTUADO        
            ,V_VALOR_PAGAMENTO       
            ,V_COMP_CONCESSIONARIO   
            ,V_TIPO_PG               
            ,V_TTS
            ,SYSDATE
        );
    END;
    
-- ----------------------------------------------------------------------------
    PROCEDURE SP_GAMA_GLOBAL_INS
    (
         V_ID_CONTRATO           INT            
        ,V_COD_GRUPO_PRODUTO      VARCHAR2
    )IS
    BEGIN
        INSERT INTO T_GAMAS_GLOBAL
        (
             ID_CONTRATO         
            ,ID_GAMAS_GLOBAL 
            ,COD_GRUPO_PRODUTO
        )
        VALUES
        (
             V_ID_CONTRATO             
            ,SEQ_ID_QUANTIA_FIM.NEXTVAL      
            ,V_COD_GRUPO_PRODUTO
        );
    END ;
    
-- ----------------------------------------------------------------------------   
    PROCEDURE SP_GAMA_INS
    (
         V_ID_CONTRATO           INT           
        ,V_COD_GRUPO_PRODUTO      VARCHAR2
    )IS
    BEGIN
        INSERT INTO T_GAMAS_QUANTIA_FIM
        (
             ID_CONTRATO         
            ,ID_GAMAS_QUANTIA_FIM 
            ,COD_GRUPO_PRODUTO
        )
        VALUES
        (
             V_ID_CONTRATO             
            ,SEQ_ID_QUANTIA_FIM.NEXTVAL      
            ,V_COD_GRUPO_PRODUTO
        );
    END ;
    
-- ----------------------------------------------------------------------------   
    PROCEDURE SP_GRUPO_ECONOMICO_GET
    (
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
                COD_GRUPO_ECONOMICO AS ID_GRUPO_ECONOMICO,
                DES_GRUPO_ECONOMICO AS NOME_GRUPO_ECONOMICO,
                (COD_GRUPO_ECONOMICO || ' - ' || DES_GRUPO_ECONOMICO) as ID_NOME_GRUPO_ECONOMICO
            FROM CINDY_DW.LK_GRUPO_ECONOMICO
            WHERE COD_GRUPO_ECONOMICO NOT IN ('SSS','000','999')
            ORDER BY COD_GRUPO_ECONOMICO;
        COMMIT;
    END;
    
-- ----------------------------------------------------------------------------   
    PROCEDURE SP_SUBGRUPO_ECONOMICO_GET
    (
         V_ID_GRUPO_ECONOMICO   VARCHAR2
        ,CUR    OUT             SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
                 COD_SUBGRUPO_ECONOMICO AS ID_SUBGRUPO_ECONOMICO,
                DES_SUBGRUPO_ECONOMICO AS NOME_SUBGRUPO_ECONOMICO,
                (COD_SUBGRUPO_ECONOMICO || ' - ' || DES_SUBGRUPO_ECONOMICO) as ID_NOME_SUBGRUPO_ECONOMICO
            FROM CINDY_DW.LK_SUBGRUPO_ECONOMICO
            WHERE COD_GRUPO_ECONOMICO = V_ID_GRUPO_ECONOMICO
             AND  COD_SUBGRUPO_ECONOMICO NOT IN ('SSS','000','999')
            ORDER BY COD_SUBGRUPO_ECONOMICO;
        COMMIT;
    END;
    
-- ----------------------------------------------------------------------------   
    PROCEDURE SP_GRUPO_PRODUTO_GET 
    (
        CUR OUT SYS_REFCURSOR
    )IS
    BEGIN
        OPEN CUR FOR SELECT * FROM T_GRUPO_PRODUTO ORDER BY COD_GRUPO_PRODUTO ASC;
        COMMIT;
    END;

-- ----------------------------------------------------------------------------   
    PROCEDURE SP_MATERIAL_INS
    (
         V_ID_CONTRATO       INTEGER
        ,V_COD_MATERIAL      INTEGER
        ,V_QUNTIDADE         INTEGER
        ,V_VALOR             NUMBER
        ,V_OBSERVACOES       VARCHAR2
    )IS
    BEGIN
        INSERT INTO T_CONT_MAT_VISIBILIDADE
        (
             ID_CONTRATO        
            ,ID_CONT_MATERIAL
            ,COD_MATERIAL   
            ,QUANTIDADE     
            ,VALOR          
            ,OBSERVACOES
        )
        VALUES
        (
             V_ID_CONTRATO                        
            ,SEQ_ID_CONT_MATERIAL.NEXTVAL  
            ,V_COD_MATERIAL       
            ,V_QUNTIDADE          
            ,V_VALOR              
            ,V_OBSERVACOES
        );
    END;
    
-- ----------------------------------------------------------------------------   
    PROCEDURE SP_MATERIAL_VISIBILIDADE_GET 
    (
        CUR OUT SYS_REFCURSOR
    )IS
    BEGIN
        OPEN CUR FOR SELECT * FROM TO_MATERIAL_POS;
        COMMIT;
   END;
   
-- ----------------------------------------------------------------------------   
    PROCEDURE SP_TTS_GET 
    (
        CUR OUT SYS_REFCURSOR
    )IS
    BEGIN 
        OPEN CUR FOR SELECT * FROM MV_TTS;
        COMMIT;
    END;

-- ----------------------------------------------------------------------------   
    PROCEDURE SSIS_LOAD_TMP_FACT_SRA_PL
    IS
    BEGIN
        INSERT INTO SSIS_TMP_FACT_SRA_PL
        (
            SELECT   b.cod_tts, a.ID_CONTRATO cod_sra_contrato,
                    c.cod_local_entrega cod_local_entrega, f.cod_mes cod_mes,
                    SUM (c.val_desconto_parcial) val_descontos
            FROM SSIS_TMP_CONTRATO a,
                    cindy_staging.etl_map_tts b,
                    T_ENTIDADE t,
                    cindy_dw.lk_grupo_tts gtts,
                    cindy_dw.lk_tts tts,
                    cindy_dw.fact_promocoes c,
                    cindy_dw.lk_dia f
            WHERE c.cod_local_entrega = t.cod_le
              AND t.id_contrato = a.ID_CONTRATO
              AND a.is_terminated = 'F'
              AND b.COD_TTS=tts.COD_TTS
              AND tts.COD_GRUPO_TTS=gtts.cod_GRUPO_TTS
              AND gtts.COD_SRA_TTS='0001'
              AND f.cod_dia = c.cod_dia
              AND c.cod_tipo_desconto = b.cod_tipo_desconto
              AND c.cod_dia >= a.data_inicio
              AND cod_sra_ambito_contrato = 'SRA'
              AND (c.cod_dia <= a.data_fim_efectivo
                 OR a.data_fim_efectivo IS NULL)
            GROUP BY b.cod_tts, a.ID_CONTRATO, cod_local_entrega, cod_mes
        );

       -- carregar os sra  e CN do ficheiro excel
        INSERT INTO SSIS_TMP_FACT_SRA_PL
        (
            SELECT  DISTINCT  c.cod_tts,
                a.ID_CONTRATO cod_sra_contrato,
                c.cod_local_entrega cod_local_entrega,
                f.cod_mes cod_mes,
                SUM (c.val_descontos) val_descontos
            FROM SSIS_TMP_CONTRATO a,
                       T_ENTIDADE t,
                    cindy_dw.fact_sra_pl_dia c,
                    cindy_dw.lk_dia f
            WHERE  c.cod_local_entrega = t.cod_le
              AND t.id_contrato = a.ID_CONTRATO
              AND c.cod_sra_contrato = a.ID_CONTRATO
              AND a.is_terminated = 'F'
              AND f.cod_dia = c.cod_dia
              AND c.cod_dia >= a.data_inicio
              AND (a.cod_sra_ambito_contrato = 'SRA' OR a.cod_sra_ambito_contrato = 'CN')
              AND (c.cod_dia <= a.data_fim_efectivo
                   OR a.data_fim_efectivo IS NULL)
           GROUP BY c.cod_tts, a.ID_CONTRATO, cod_local_entrega, cod_mes
        );

    /* Alterar Para carregar os Não SRA (do Cindy REAL)*/
       INSERT INTO SSIS_TMP_FACT_SRA_PL
          (SELECT   b.cod_tts, a.ID_CONTRATO cod_sra_contrato,
                    c.cod_local_entrega cod_local_entrega, f.cod_mes cod_mes,
                    SUM (c.val_desconto_parcial) val_descontos
               FROM SSIS_TMP_CONTRATO a,
                    cindy_staging.etl_map_tts b,
                    cindy_dw.lk_grupo_tts gtts,
                    cindy_dw.lk_tts tts,
                    cindy_dw.fact_promocoes c,
                    T_ENTIDADE t,
                    cindy_dw.lk_dia f
              WHERE c.cod_local_entrega = t.cod_le
                AND t.id_contrato = a.ID_CONTRATO
                AND a.is_terminated = 'F'
                AND b.COD_TTS=tts.COD_TTS
                AND tts.COD_GRUPO_TTS=gtts.cod_GRUPO_TTS
                AND gtts.COD_SRA_TTS='0002'
                AND f.cod_dia = c.cod_dia
                AND c.cod_tipo_desconto = b.cod_tipo_desconto
                AND c.cod_dia >= a.data_inicio
                AND cod_sra_ambito_contrato = 'CN'
                AND (c.cod_dia <= a.data_fim_efectivo
                     OR a.data_fim_efectivo IS NULL
                    )
           GROUP BY b.cod_tts, a.ID_CONTRATO, cod_local_entrega, cod_mes);

       INSERT INTO SSIS_TMP_FACT_SRA_PL
          (SELECT   '0026', a.ID_CONTRATO cod_sra_contrato,
                    c.cod_local_entrega cod_local_entrega, f.cod_mes cod_mes,
                    SUM (c.val_desconto_parcial) val_descontos
               FROM SSIS_TMP_CONTRATO a,
                    cindy_staging.etl_map_tts b,
                    cindy_dw.lk_grupo_tts gtts,
                    cindy_dw.lk_tts tts,
                    cindy_dw.fact_promocoes c,
                    T_ENTIDADE t,
                    cindy_dw.lk_dia f
              WHERE c.cod_local_entrega = t.cod_le
                AND t.id_contrato = a.ID_CONTRATO
                AND a.is_terminated = 'F'
                AND b.COD_TTS=tts.COD_TTS
                AND tts.COD_GRUPO_TTS=gtts.cod_GRUPO_TTS
                AND gtts.COD_SRA_TTS='0002'
                AND f.cod_dia = c.cod_dia
                AND c.cod_tipo_desconto = b.cod_tipo_desconto
                AND c.cod_dia >= a.data_inicio
                AND cod_sra_ambito_contrato = 'CN'
                AND (c.cod_dia <= a.data_fim_efectivo
                     OR a.data_fim_efectivo IS NULL
                    )
                AND c.cod_tipo_desconto NOT IN (SELECT b.cod_tipo_desconto
                                                  FROM cindy_staging.etl_map_tts)
           GROUP BY a.ID_CONTRATO, cod_local_entrega, cod_mes);

       COMMIT;
    END;
    
-- ----------------------------------------------------------------------------   
    PROCEDURE SSIS_LOAD_TMP_SRA_PROMOCOES
    IS
    BEGIN
       /* Carrega tabela que contém as vendas ao abrigo de cada contrato para cada LE */
       INSERT INTO SSIS_TMP_SRA_PROMOCOES
          (
          SELECT
            CONT.id_contrato ID_CONTRATO,
            DIA.cod_mes,
            FACT.cod_local_entrega LOCAL_ENTREGA,
            GAMA.cod_grupo_produto,
            SUM(NVL(FACT.val_desconto_PARCIAL,0)) VAL_DESCONTOS
        FROM
            SSIS_TMP_CONTRATO_RENT CONT
            INNER JOIN T_ENTIDADE ENT
                 ON (ENT.id_contrato = CONT.id_contrato)
            INNER JOIN v_bo_gamas_quantia_fim    GAMA
                ON (GAMA.id_contrato = CONT.id_contrato)
            INNER JOIN cindy_dw.lk_produto       PROD
                 ON (PROD.cod_grupo_produto = GAMA.cod_grupo_produto)
            INNER JOIN cindy_dw.fact_PROMOCOES   FACT
                 ON (FACT.cod_produto = PROD.cod_produto)
                 AND ( FACT.cod_local_entrega = ENT.cod_le )
                 AND ( FACT.cod_dia > CONT.data_inicio)
            INNER JOIN cindy_dw.lk_dia        DIA
                 ON (DIA.cod_dia = FACT.cod_dia)
         WHERE
             CONT.is_terminated = 'F' AND
            (FACT.COD_TIPO_DESCONTO = '190' OR
             FACT.COD_TIPO_DESCONTO = '290'
            )
        GROUP BY CONT.id_contrato,
                 DIA.cod_mes,
                 FACT.cod_local_entrega,
                 GAMA.cod_grupo_produto

        );
        COMMIT;
    END;
    
-- ----------------------------------------------------------------------------   
    PROCEDURE SSIS_LOAD_TMP_SRA_VENDAS
    IS
    BEGIN
        /* Carrega tabela que contém as vendas ao abrigo de cada contrato para cada LE */
       INSERT INTO CINDY_STAGING.SSIS_TMP_SRA_VENDAS
          (
          SELECT
            CONT.id_contrato ID_CONTRATO
            ,DIA.cod_mes
            ,FACT.cod_local_entrega LOCAL_ENTREGA
            ,GAMA.cod_grupo_produto
            ,SUM(NVL(FACT.VAL_VENDAS,0)) VAL_VENDAS
            ,SUM(NVL(FACT.VAL_DEVOLUCOES,0)) VAL_DEVOLUCOES
            ,SUM(NVL(FACT.VAL_DESCONTOS,0)) VAL_DESCONTOS
            ,SUM(NVL(FACT.LITROS_VENDIDOS,0)) LITROS_VENDIDOS
        FROM
            SSIS_TMP_CONTRATO_RENT CONT
            INNER JOIN T_ENTIDADE ENT
                 ON (ENT.id_contrato = CONT.id_contrato)
            INNER JOIN v_bo_gamas_quantia_fim    GAMA
                ON (GAMA.id_contrato = CONT.id_contrato)
            INNER JOIN cindy_dw.lk_produto       PROD
                 ON (PROD.cod_grupo_produto = GAMA.cod_grupo_produto)
            INNER JOIN cindy_dw.fact_VENDAS   FACT
                 ON (FACT.cod_produto = PROD.cod_produto)
                 AND ( FACT.cod_local_entrega = ENT.cod_le )
                 AND ( FACT.cod_dia > CONT.data_inicio)
            INNER JOIN cindy_dw.lk_dia        DIA
                 ON (DIA.cod_dia = FACT.cod_dia)
         WHERE
             CONT.is_terminated = 'F'
        GROUP BY CONT.id_contrato,
                 DIA.cod_mes,
                 FACT.cod_local_entrega,
                 GAMA.cod_grupo_produto
                 );

       COMMIT;
    END;

-- ----------------------------------------------------------------------------   
    PROCEDURE SSIS_LOAD_TMP_SRA_VENDAS_T
    IS
    BEGIN
       /* Carrega tabela que contém as vendas ao abrigo de cada contrato para cada LE, não   considerando apenas as gamas que terminam contrato, mas todas as envolvidas nas   contrapartidas */
       INSERT INTO CINDY_STAGING.SSIS_TMP_SRA_VENDAS_TOTAIS
          (
          SELECT
            CONT.id_contrato ID_CONTRATO
            ,DIA.cod_mes
            ,FACT.cod_local_entrega LOCAL_ENTREGA
            ,GRUPO.cod_grupo_produto
            ,SUM(NVL(FACT.VAL_VENDAS,0)) VAL_VENDAS
            ,SUM(NVL(FACT.VAL_DEVOLUCOES,0)) VAL_DEVOLUCOES
            ,SUM(NVL(FACT.VAL_DESCONTOS,0)) VAL_DESCONTOS
            ,SUM(NVL(FACT.LITROS_VENDIDOS,0)) LITROS_VENDIDOS
        FROM
            SSIS_TMP_CONTRATO_RENT CONT
            INNER JOIN T_ENTIDADE ENT
                 ON (ENT.id_contrato = CONT.id_contrato)
            INNER JOIN T_CONTRAPARTIDAS    GRUPO
                ON (GRUPO.id_contrato = CONT.id_contrato)
            INNER JOIN cindy_dw.lk_produto       PROD
                 ON (PROD.cod_grupo_produto = GRUPO.cod_grupo_produto)
            INNER JOIN cindy_dw.fact_VENDAS   FACT
                 ON (FACT.cod_produto = PROD.cod_produto)
                 AND ( FACT.cod_local_entrega = ENT.cod_le )
                 AND ( FACT.cod_dia > CONT.data_inicio)
            INNER JOIN cindy_dw.lk_dia        DIA
                 ON (DIA.cod_dia = FACT.cod_dia)
         WHERE
             CONT.is_terminated = 'F'
        GROUP BY CONT.id_contrato,
                 DIA.cod_mes,
                 FACT.cod_local_entrega,
                 GRUPO.cod_grupo_produto
                 );

       COMMIT;
    END;
    
-- ----------------------------------------------------------------------------   
 
END PK_SRA;
/

COMMIT;



    
-- #######################################################################################################
-- #######################################################################################################
-- ##                                                                                                   ##
-- ##  CRIAÇÃO DAS PROCEDURES PACKAGE AE                                                                ##
-- ##                                                                                                   ##
-- #######################################################################################################
-- #######################################################################################################
  

CREATE OR REPLACE PACKAGE PK_AE AS
    
-- 01S ################################################################### 
    PROCEDURE SP_AE_AGRUPAMENTO_PRODUTO_GET (CUR OUT SYS_REFCURSOR);
    
-- 02S ################################################################### 
    PROCEDURE SP_AE_GRUPO_PRODUTO_GET 
    (
        V_ID_AGRUPAMENTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    );
    
-- 03S ################################################################### 
    PROCEDURE SP_AE_SUB_GRUPO_PRODUTO_GET 
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,     
        CUR OUT SYS_REFCURSOR
    );
    
-- 04S ################################################################### 
    PROCEDURE SP_AE_SUB_AGG_PRODUTO_GET 
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,    
        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    );
    
-- 05S ################################################################### 
    PROCEDURE SP_AE_PRODUTO_GET
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,    
        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
        V_ID_SUB_AGG_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    );
    
-- 051S ################################################################### 
    PROCEDURE SP_AE_PRODUTO_SEARCH
    (
        V_ID_PRODUTO VARCHAR2,    
        V_NOME_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    );
    
-- 06S ################################################################### 
    PROCEDURE SP_AE_EMISSOR_GET(CUR OUT SYS_REFCURSOR);

-- 07S ################################################################### 
    PROCEDURE SP_AE_CUSTOMER_SERVICE_GET(CUR OUT SYS_REFCURSOR);

-- 075S ################################################################## 
    PROCEDURE SP_AE_GRUPO_ECONOMICO_GET(CUR OUT SYS_REFCURSOR);

-- 076S ################################################################## 
    PROCEDURE SP_AE_SUBGRUPO_ECONOMICO_GET
    (
        V_ID_GRUPO_ECONOMICO VARCHAR2,    
        CUR OUT SYS_REFCURSOR
    );
    
-- 077S ################################################################## 
    PROCEDURE SP_AE_CONCESSIONARIO_GET(CUR OUT SYS_REFCURSOR);
    
-- 08S ################################################################### 
    PROCEDURE SP_AE_TIPO_ACTIVIDADE_GET(CUR OUT SYS_REFCURSOR);

-- 09T ###################################################################       
    PROCEDURE SP_AE_CONTRATO_EXISTS
    (
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_EXISTS            OUT    INT
    );
    
-- 09S1 ###################################################################       
    PROCEDURE SP_AE_CONTRATOS_GET (WHERESTM VARCHAR2, CUR OUT SYS_REFCURSOR );
        
-- 09S2 ###################################################################       
    PROCEDURE SP_AE_CONTRATO_GET
    (
        V_ID_CONTRATO               VARCHAR2,
        CUR_CONTRATO                   OUT SYS_REFCURSOR,
        CUR_ENTIDADE                   OUT SYS_REFCURSOR,
        CUR_ABASTECIMENTO              OUT SYS_REFCURSOR,
        CUR_ACTIVIDADE                 OUT SYS_REFCURSOR,
        CUR_TPR                        OUT SYS_REFCURSOR,
        CUR_DEBITOS                    OUT SYS_REFCURSOR,
        CUR_LOG                        OUT SYS_REFCURSOR
    );
    
-- 09I ###################################################################   
    PROCEDURE SP_AE_CONTRATO_INS
    (
        V_ID_CONTRATO              IN OUT INT,
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_DATA_RECEPCAO            DATE,
        V_ID_CUSTOMER_SERVICE      VARCHAR2,
        V_NIVEL_CONTRATO           VARCHAR2,
        V_GRUPO_ECONOMICO_COD      VARCHAR2,
        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
        V_NOME_FIRMA               VARCHAR2,
        V_MORADA_SEDE              VARCHAR2,
        V_LOCALIDADE               VARCHAR2,
        V_NIF                      VARCHAR2,
        V_OBSERVACOES              VARCHAR2,
        V_INSERT_USER              VARCHAR2,
        V_EXISTS            OUT    INT
    );      
    
-- 09U ###################################################################       
    PROCEDURE SP_AE_CONTRATO_UPD
    (
        V_ID_CONTRATO              INT,
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_DATA_RECEPCAO            DATE,
        V_ID_CUSTOMER_SERVICE      VARCHAR2,
        V_NIVEL_CONTRATO           VARCHAR2,
        V_GRUPO_ECONOMICO_COD      VARCHAR2,
        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
        V_NOME_FIRMA               VARCHAR2,
        V_MORADA_SEDE              VARCHAR2,
        V_LOCALIDADE               VARCHAR2,
        V_NIF                      VARCHAR2,
        V_OBSERVACOES              VARCHAR2,
        V_INSERT_USER              VARCHAR2,
        V_EXISTS            OUT    INT
    );      

-- 10S1 ###################################################################       
    PROCEDURE SP_AE_ENTIDADES_GET 
    (
        WHERESTM     VARCHAR2, 
        CUR         OUT SYS_REFCURSOR
    );

-- 10S2 ###################################################################       
    PROCEDURE SP_AE_ENTIDADE_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );

-- 10I ###################################################################   
    PROCEDURE SP_AE_ENTIDADE_INS
    (
        V_ID_CONTRATO                 INT,
        V_COD_LE                   VARCHAR2,
        V_COD_GRUPO_ECONOMICO      VARCHAR2,
        V_COD_SUB_GRUPO_ECONOMICO  VARCHAR2,
        V_COD_CLIENTE              VARCHAR2
    );

-- 10D1 ##################################################################       
    PROCEDURE SP_AE_ENTIDADE_DEL 
    (
        V_ID_CONTRATO   INT,
        V_COD_LE             VARCHAR2
    ); 
    
-- 10D2 ##################################################################       
    PROCEDURE SP_AE_ENTIDADES_DEL 
    (
        V_ID_CONTRATO   INT
    );    
    
-- 11S ###################################################################       
    PROCEDURE SP_AE_ABASTECIMENTO_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );

-- 11I ###################################################################   
    PROCEDURE SP_AE_ABASTECIMENTO_INS
    (
        V_ID_CONTRATO              INT,
        V_COD_CONCESSIONARIO       VARCHAR2,
        V_FACTURACAO               CHAR,
        V_DISTRIBUICAO               CHAR,
        V_OBSERVACOES              VARCHAR2
    );
    
-- 11U ###################################################################    
    PROCEDURE SP_AE_ABASTECIMENTO_UPD
    (
        V_ID_CONTRATO              INT,
        V_COD_CONCESSIONARIO       VARCHAR2,
        V_FACTURACAO               CHAR,
        V_DISTRIBUICAO               CHAR,
        V_OBSERVACOES              VARCHAR2
    );

-- 11D ###################################################################       
    PROCEDURE SP_AE_ABASTECIMENTO_DEL 
    (
        V_ID_CONTRATO            INT,
        V_COD_CONCESSIONARIO    VARCHAR2
    );    
        
    
-- 12S ###################################################################       
    PROCEDURE SP_AE_ACTIVIDADE_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );
    
-- 12I ###################################################################    
    PROCEDURE SP_AE_ACTIVIDADE_INS
    (
        V_ID_CONTRATO               INT,
        V_ID_TIPO_ACTIVIDADE        INT,
        V_NUM_ACORDO                VARCHAR2,
        V_VALOR_ACORDO              NUMBER
    );
    
-- 12U ###################################################################    
    PROCEDURE SP_AE_ACTIVIDADE_UPD
    (
        V_ID_ACTIVIDADE                INT,
        V_ID_CONTRATO               INT,
        V_ID_TIPO_ACTIVIDADE        INT,
        V_NUM_ACORDO                VARCHAR2,
        V_VALOR_ACORDO              NUMBER
    );

-- 12D ###################################################################       
    PROCEDURE SP_AE_ACTIVIDADE_DEL 
    (
        V_ID_ACTIVIDADE   INT
    );    

-- 13S ###################################################################       
    PROCEDURE SP_AE_TPR_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );
    
-- 13I ###################################################################    
    PROCEDURE SP_AE_TPR_INS
    (
        V_ID_CONTRATO               INT,
        V_COD_PRODUTO               VARCHAR2,
        V_BUDGET_VENDAS             NUMBER,
        V_BUDGET_MARKETING          NUMBER,
        V_ACTIVIDADE_INICIO         DATE,
        V_ACTIVIDADE_FIM            DATE,
        V_COMPRAS_INICIO            DATE,
        V_COMPRAS_FIM               DATE,
        V_PVP                       NUMBER,
        V_UNIDADE_BASE                VARCHAR2,
        V_PREVISAO_QUANTIDADE        NUMBER
    );
    
-- 13U ###################################################################    
    PROCEDURE SP_AE_TPR_UPD
    (
        V_ID_TPR                    INT,
        V_ID_CONTRATO               INT,
        V_COD_PRODUTO               VARCHAR2,
        V_BUDGET_VENDAS             NUMBER,
        V_BUDGET_MARKETING          NUMBER,
        V_ACTIVIDADE_INICIO         DATE,
        V_ACTIVIDADE_FIM            DATE,
        V_COMPRAS_INICIO            DATE,
        V_COMPRAS_FIM               DATE,
        V_PVP                       NUMBER,
        V_UNIDADE_BASE                VARCHAR2,
        V_PREVISAO_QUANTIDADE        NUMBER
    );
          
-- 13D ###################################################################       
    PROCEDURE SP_AE_TPR_DEL 
    (
        V_ID_TPR   INT
    );    
    
-- 14S ###################################################################       
    PROCEDURE SP_AE_DEBITO_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );
    
-- 14I ###################################################################    
    PROCEDURE SP_AE_DEBITO_INS
    (
        V_ID_CONTRATO               INT,
        V_DATA_DEBITO               DATE,
        V_VALOR_DEBITO              NUMBER
    );

-- 14U ###################################################################    
    PROCEDURE SP_AE_DEBITO_UPD
    (
        V_ID_DEBITO                    INT,
        V_ID_CONTRATO               INT,
        V_DATA_DEBITO               DATE,
        V_VALOR_DEBITO              NUMBER
    );
    
-- 14D ###################################################################       
    PROCEDURE SP_AE_DEBITO_DEL 
    (
        V_ID_DEBITO   INT
    );    

-- 15S ###################################################################       
    PROCEDURE SP_AE_LOG_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );    
          
-- 15I ###################################################################    
    PROCEDURE SP_AE_LOG_INS
    (
        V_ID_CONTRATO            INT,
        V_LOG_DATA               DATE,
        V_LOG_USER               VARCHAR2,
        V_MESSAGE                VARCHAR2
    );
  
END PK_AE;
/

COMMIT;


--#################################################################################################
--#################################################################################################

CREATE OR REPLACE PACKAGE BODY PK_AE AS 
    
-- 01S ################################################################### 
    PROCEDURE SP_AE_AGRUPAMENTO_PRODUTO_GET (CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT DISTINCT
                AG.COD_AGRUPAMENTO_PRODUTO AS ID_AGRUPAMENTO,
                AG.DES_AGRUPAMENTO_PRODUTO AS NOME_AGRUPAMENTO,
                (AG.COD_AGRUPAMENTO_PRODUTO || ' - ' || AG.DES_AGRUPAMENTO_PRODUTO) AS ID_NOME_AGRUPAMENTO
            FROM CINDY_DW.LK_AGRUPAMENTO_PRODUTO AG
            INNER JOIN CINDY_DW.LK_GRUPO_PRODUTO GP
                ON(GP.COD_AGRUPAMENTO_PRODUTO = AG.COD_AGRUPAMENTO_PRODUTO)
            ORDER BY CAST(AG.COD_AGRUPAMENTO_PRODUTO AS INT);
        COMMIT;
    END;
      
-- 02S ################################################################### 
    PROCEDURE SP_AE_GRUPO_PRODUTO_GET 
    (
        V_ID_AGRUPAMENTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                GP.COD_GRUPO_PRODUTO AS ID_GRUPO_PRODUTO,
                GP.DES_GRUPO_PRODUTO AS NOME_GRUPO_PRODUTO,
                (GP.COD_GRUPO_PRODUTO || ' - ' || GP.DES_GRUPO_PRODUTO ) AS ID_NOME_GRUPO_PRODUTO
            FROM CINDY_DW.LK_GRUPO_PRODUTO GP
            INNER JOIN CINDY_DW.LK_PRODUTO P
                ON(P.COD_GRUPO_PRODUTO = GP.COD_GRUPO_PRODUTO)
            WHERE GP.COD_AGRUPAMENTO_PRODUTO = V_ID_AGRUPAMENTO
            ORDER BY CAST(GP.COD_GRUPO_PRODUTO AS INT);
            
        COMMIT;    
    END;
    
-- 03S ################################################################### 
    PROCEDURE SP_AE_SUB_GRUPO_PRODUTO_GET 
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,     
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                GP.COD_SUBGRUPO_PRODUTO AS ID_SUBGRUPO_PRODUTO,
                GP.DES_SUBGRUPO_PRODUTO AS NOME_SUBGRUPO_PRODUTO,
                (GP.COD_SUBGRUPO_PRODUTO || ' - ' || GP.DES_SUBGRUPO_PRODUTO) AS ID_NOME_SUBGRUPO_PRODUTO 
            FROM CINDY_DW.LK_SUBGRUPO_PRODUTO GP
            INNER JOIN CINDY_DW.LK_PRODUTO P
                ON(P.COD_SUBGRUPO_PRODUTO = GP.COD_SUBGRUPO_PRODUTO)
            WHERE P.COD_GRUPO_PRODUTO = V_ID_GRUPO_PRODUTO
            ORDER BY CAST(GP.COD_SUBGRUPO_PRODUTO  AS INT);
        COMMIT;    
    END;
        
-- 04S ################################################################### 
    PROCEDURE SP_AE_SUB_AGG_PRODUTO_GET 
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,    
        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                GP.COD_SUB_AGG_PRODUTO AS ID_SUB_AGG_PRODUTO,
                GP.DES_SUB_AGG_PRODUTO AS NOME_SUB_AGG_PRODUTO,
                (GP.COD_SUB_AGG_PRODUTO || ' - ' || GP.DES_SUB_AGG_PRODUTO ) AS ID_NOME_SUB_AGG_PRODUTO
            FROM CINDY_DW.LK_SUB_AGG_PRODUTO GP
            INNER JOIN CINDY_DW.LK_PRODUTO P
                ON(P.COD_SUB_AGG_PRODUTO = GP.COD_SUB_AGG_PRODUTO)
            WHERE P.COD_GRUPO_PRODUTO = V_ID_GRUPO_PRODUTO
              AND P.COD_SUBGRUPO_PRODUTO = V_ID_SUB_GRUPO_PRODUTO
              ORDER BY CAST(GP.COD_SUB_AGG_PRODUTO AS INT);
        COMMIT;    
    END;
    
-- 05S ################################################################### 
    PROCEDURE SP_AE_PRODUTO_GET
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,    
        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
        V_ID_SUB_AGG_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                P.COD_PRODUTO AS ID_PRODUTO,
                P.DES_PRODUTO AS NOME_PRODUTO,
                P.UNI_BAS_CINDY AS UNIDADE_BASE,
                P.PRECO_VEN_UNI_BAS AS PVP_PRODUTO,
                (P.COD_PRODUTO || ' - ' || P.DES_PRODUTO) AS ID_NOME_PRODUTO
            FROM CINDY_DW.LK_PRODUTO P
            WHERE P.COD_GRUPO_PRODUTO = V_ID_GRUPO_PRODUTO
              AND P.COD_SUBGRUPO_PRODUTO = V_ID_SUB_GRUPO_PRODUTO
              AND P.COD_SUB_AGG_PRODUTO = V_ID_SUB_AGG_PRODUTO
            ORDER BY P.COD_PRODUTO;
        COMMIT;    
    END;
    
-- 051S ################################################################### 
    PROCEDURE SP_AE_PRODUTO_SEARCH
    (
        V_ID_PRODUTO VARCHAR2,    
        V_NOME_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                P.COD_PRODUTO AS ID_PRODUTO,
                P.DES_PRODUTO AS NOME_PRODUTO,
                P.UNI_BAS_CINDY AS UNIDADE_BASE,
                P.PRECO_VEN_UNI_BAS AS PVP_PRODUTO,
                (P.COD_PRODUTO || ' - ' || P.DES_PRODUTO) AS ID_NOME_PRODUTO,
                P.COD_GRUPO_PRODUTO,
                P.COD_SUBGRUPO_PRODUTO,
                P.COD_SUB_AGG_PRODUTO
            FROM CINDY_DW.LK_PRODUTO P
            WHERE (P.COD_PRODUTO = V_ID_PRODUTO
                    OR V_ID_PRODUTO IS NULL)
              AND (UPPER(P.DES_PRODUTO) LIKE UPPER('%'|| V_NOME_PRODUTO || '%')
                    OR V_NOME_PRODUTO IS NULL)
            ORDER BY P.COD_PRODUTO;
        COMMIT;    
    END;
    
-- 06S ################################################################### 
    PROCEDURE SP_AE_EMISSOR_GET(CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
                ID_EMISSOR,
                NOME_EMISSOR,
                (ID_EMISSOR || ' - ' || NOME_EMISSOR) as COD_NOME_EMISSOR
            FROM LK_EMISSOR
            ORDER BY CAST(ID_EMISSOR as INT);
        COMMIT;
    END;
        
-- 07S ###################################################################     
    PROCEDURE SP_AE_CUSTOMER_SERVICE_GET(CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
                ID_CUSTOMER_SERVICE,
                NOME_CUSTOMER_SERVICE,
                (ID_CUSTOMER_SERVICE || ' - ' || NOME_CUSTOMER_SERVICE) as COD_NOME_CUSTOMER_SERVICE
            FROM LK_CUSTOMER_SERVICE
            ORDER BY CAST(ID_CUSTOMER_SERVICE as INT);
        COMMIT;
    END;
        
-- 075S ################################################################## 
    PROCEDURE SP_AE_GRUPO_ECONOMICO_GET(CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
                COD_GRUPO_ECONOMICO AS ID_GRUPO_ECONOMICO,
                DES_GRUPO_ECONOMICO AS NOME_GRUPO_ECONOMICO,
                (COD_GRUPO_ECONOMICO || ' - ' || DES_GRUPO_ECONOMICO) as ID_NOME_GRUPO_ECONOMICO
            FROM CINDY_DW.LK_GRUPO_ECONOMICO
            WHERE COD_GRUPO_ECONOMICO NOT IN ('SSS','000','999')
            ORDER BY COD_GRUPO_ECONOMICO;
        COMMIT;
    END;

-- 076S ################################################################## 
    PROCEDURE SP_AE_SUBGRUPO_ECONOMICO_GET
    (
        V_ID_GRUPO_ECONOMICO VARCHAR2,    
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
                 COD_SUBGRUPO_ECONOMICO AS ID_SUBGRUPO_ECONOMICO,
                DES_SUBGRUPO_ECONOMICO AS NOME_SUBGRUPO_ECONOMICO,
                (COD_SUBGRUPO_ECONOMICO || ' - ' || DES_SUBGRUPO_ECONOMICO) as ID_NOME_SUBGRUPO_ECONOMICO
            FROM CINDY_DW.LK_SUBGRUPO_ECONOMICO
            WHERE COD_GRUPO_ECONOMICO = V_ID_GRUPO_ECONOMICO
             AND  COD_SUBGRUPO_ECONOMICO NOT IN ('SSS','000','999')
            ORDER BY COD_SUBGRUPO_ECONOMICO;
        COMMIT;
    END;

-- 077S ##################################################################  
    PROCEDURE SP_AE_CONCESSIONARIO_GET(CUR OUT SYS_REFCURSOR)  IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
                COD_CONCESSIONARIO AS ID_CONCESSIONARIO,
                DES_CONCESSIONARIO AS NOME_CONCESSIONARIO,
                (COD_CONCESSIONARIO || ' - ' || DES_CONCESSIONARIO) as ID_NOME_CONCESSIONARIO
            FROM CINDY_DW.LK_CONCESSIONARIO
            ORDER BY COD_CONCESSIONARIO;
        COMMIT;
    END;    
    
-- 08S ###################################################################     
    PROCEDURE SP_AE_TIPO_ACTIVIDADE_GET(CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT *
            FROM LK_TIPO_ACTIVIDADE
        COMMIT;
    END;
        
-- 09T ###################################################################       
    PROCEDURE SP_AE_CONTRATO_EXISTS
    (
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_EXISTS            OUT    INT
    ) IS
    BEGIN
        SELECT NVL(MAX(ID_CONTRATO),-1) INTO V_EXISTS 
        FROM T_AE_CONTRATO 
        WHERE ID_NUM_PROPOSTA = (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO);        
    END;

-- 09S1 ###################################################################       
    PROCEDURE SP_AE_CONTRATOS_GET 
    (
        WHERESTM VARCHAR2, 
        CUR OUT SYS_REFCURSOR 
    ) IS
    sqlstm VARCHAR2(2000);
    BEGIN    
        sqlstm :=  'SELECT * '
                || 'FROM ('
                || ' SELECT C.ID_CONTRATO,'
                || ' C.ID_NUM_PROPOSTA,'
                || ' C.ID_EMISSOR,'
                || ' C.ID_ANO,'
                || ' C.ID_NUM_SERIE,'
                || ' C.ID_NUMERO,'
                || ' E.NOME_EMISSOR ID_EMISSOR_NOME,'
                || ' C.DATA_RECEPCAO,'
                || ' C.ID_CUSTOMER_SERVICE,'
                || ' CS.NOME_CUSTOMER_SERVICE CUSTOMER_SERVICE_NOME,'
                || ' C.NIVEL_CONTRATO NIVEL_CONTRATO,'
                || ' C.GRUPO_ECONOMICO_COD,'
                || ' GE.DES_GRUPO_ECONOMICO GRUPO_ECONOMICO_DESC,'
                || ' C.SUBGRUPO_ECONOMICO_COD,'
                || ' SGE.DES_SUBGRUPO_ECONOMICO SUBGRUPO_ECONOMICO_DESC,'
                || ' C.NOME_FIRMA,'
                || ' C.MORADA_SEDE,'
                || ' C.LOCALIDADE,'
                || ' C.NIF,'
                || ' C.OBSERVACOES,'
                || ' C.INSERT_DATE,'
                || ' C.INSERT_USER,'
                || ' C.ALTER_DATE,'
                || ' C.ALTER_USER  '
                || ' FROM T_AE_CONTRATO C '
                || ' LEFT JOIN LK_EMISSOR E'
                || '  ON(E.ID_EMISSOR = C.ID_EMISSOR) '
                || ' LEFT JOIN LK_CUSTOMER_SERVICE CS'
                || '  ON(CS.ID_CUSTOMER_SERVICE = C.ID_CUSTOMER_SERVICE)'
                || ' LEFT JOIN CINDY_DW.LK_GRUPO_ECONOMICO GE'
                || '  ON(GE.COD_GRUPO_ECONOMICO = C.GRUPO_ECONOMICO_COD)'
                || ' LEFT JOIN CINDY_DW.LK_SUBGRUPO_ECONOMICO SGE'
                || '  ON   (SGE.COD_SUBGRUPO_ECONOMICO = C.SUBGRUPO_ECONOMICO_COD)'
                || '   AND (SGE.COD_GRUPO_ECONOMICO = C.GRUPO_ECONOMICO_COD)'
                || ')'
                || ' WHERE '
                || WHERESTM;
        OPEN CUR FOR sqlstm;
        COMMIT;
    END;
          
-- 09S2 ###################################################################       
    PROCEDURE SP_AE_CONTRATO_GET
    (
        V_ID_CONTRATO            VARCHAR2,
        CUR_CONTRATO            OUT SYS_REFCURSOR,
        CUR_ENTIDADE            OUT SYS_REFCURSOR,
        CUR_ABASTECIMENTO       OUT SYS_REFCURSOR,
        CUR_ACTIVIDADE          OUT SYS_REFCURSOR,
        CUR_TPR                 OUT SYS_REFCURSOR,
        CUR_DEBITOS             OUT SYS_REFCURSOR,
        CUR_LOG                 OUT SYS_REFCURSOR
    ) IS
    BEGIN
        SP_AE_CONTRATOS_GET ('ID_CONTRATO = '||V_ID_CONTRATO,CUR_CONTRATO);
        SP_AE_ENTIDADE_GET(V_ID_CONTRATO,CUR_ENTIDADE);
        SP_AE_ABASTECIMENTO_GET(V_ID_CONTRATO,CUR_ABASTECIMENTO);
        SP_AE_ACTIVIDADE_GET(V_ID_CONTRATO,CUR_ACTIVIDADE);
        SP_AE_TPR_GET(V_ID_CONTRATO,CUR_TPR);
        SP_AE_DEBITO_GET(V_ID_CONTRATO,CUR_DEBITOS);
        SP_AE_LOG_GET(V_ID_CONTRATO,CUR_LOG);
        COMMIT;
    END;
    
-- 09I ################################################################### 
    PROCEDURE SP_AE_CONTRATO_INS
    (
        V_ID_CONTRATO              IN OUT INT,
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_DATA_RECEPCAO            DATE,
        V_ID_CUSTOMER_SERVICE      VARCHAR2,
        V_NIVEL_CONTRATO           VARCHAR2,
        V_GRUPO_ECONOMICO_COD      VARCHAR2,
        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
        V_NOME_FIRMA               VARCHAR2,
        V_MORADA_SEDE              VARCHAR2,
        V_LOCALIDADE               VARCHAR2,
        V_NIF                      VARCHAR2,
        V_OBSERVACOES              VARCHAR2,
        V_INSERT_USER              VARCHAR2,
        V_EXISTS            OUT    INT
    ) IS
    
    ID INT:=NULL;
    BEGIN    
        
        SELECT COUNT(*) INTO V_EXISTS 
        FROM T_AE_CONTRATO 
        WHERE ID_NUM_PROPOSTA = (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO);
        
        IF V_EXISTS = 0 THEN    
            
            SELECT SEQ_AE_ID_CONTRATO.NEXTVAL INTO ID FROM DUAL;
            
            INSERT INTO T_AE_CONTRATO
            (
                ID_CONTRATO              ,
                ID_NUM_PROPOSTA          ,
                ID_EMISSOR               ,
                ID_ANO                   ,
                ID_NUM_SERIE             ,
                ID_NUMERO                ,
                DATA_RECEPCAO            ,
                ID_CUSTOMER_SERVICE      ,
                NIVEL_CONTRATO             ,
                GRUPO_ECONOMICO_COD      ,
                SUBGRUPO_ECONOMICO_COD   ,
                NOME_FIRMA               ,
                MORADA_SEDE              ,
                LOCALIDADE               ,
                NIF                      ,
                OBSERVACOES              ,
                INSERT_DATE              ,
                INSERT_USER              ,
                ALTER_DATE               ,
                ALTER_USER
            )
            VALUES
            (
                ID,                        
                (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO),
                V_ID_EMISSOR             ,
                V_ID_ANO                 ,
                V_ID_NUM_SERIE           ,
                V_ID_NUMERO              ,
                V_DATA_RECEPCAO          ,
                V_ID_CUSTOMER_SERVICE    ,
                V_NIVEL_CONTRATO         ,
                V_GRUPO_ECONOMICO_COD    ,
                V_SUBGRUPO_ECONOMICO_COD ,
                V_NOME_FIRMA             ,
                V_MORADA_SEDE            ,
                V_LOCALIDADE             ,
                V_NIF                    ,
                V_OBSERVACOES            ,
                SYSDATE                  ,
                V_INSERT_USER            ,
                SYSDATE                  ,
                V_INSERT_USER          
            );
            
            V_ID_CONTRATO := ID;
            
        END IF;
    END;

-- 09U ###################################################################       
    PROCEDURE SP_AE_CONTRATO_UPD
    (
        V_ID_CONTRATO              INT,
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_DATA_RECEPCAO            DATE,
        V_ID_CUSTOMER_SERVICE      VARCHAR2,
        V_NIVEL_CONTRATO           VARCHAR2,
        V_GRUPO_ECONOMICO_COD      VARCHAR2,
        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
        V_NOME_FIRMA               VARCHAR2,
        V_MORADA_SEDE              VARCHAR2,
        V_LOCALIDADE               VARCHAR2,
        V_NIF                      VARCHAR2,
        V_OBSERVACOES              VARCHAR2,
        V_INSERT_USER              VARCHAR2,
        V_EXISTS            OUT    INT
    ) IS
    BEGIN    
        
        SELECT COUNT(*) INTO V_EXISTS 
        FROM T_AE_CONTRATO 
        WHERE ID_NUM_PROPOSTA = (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO)
        AND ID_CONTRATO <> V_ID_CONTRATO;
        
        IF V_EXISTS = 0 THEN    
            UPDATE T_AE_CONTRATO
            SET 
                ID_NUM_PROPOSTA          = (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO),
                ID_EMISSOR               = V_ID_EMISSOR                ,
                ID_ANO                   = V_ID_ANO                    ,
                ID_NUM_SERIE             = V_ID_NUM_SERIE           ,
                ID_NUMERO                = V_ID_NUMERO              ,
                DATA_RECEPCAO            = V_DATA_RECEPCAO          ,
                ID_CUSTOMER_SERVICE      = V_ID_CUSTOMER_SERVICE    ,
                NIVEL_CONTRATO             = V_NIVEL_CONTRATO            ,
                GRUPO_ECONOMICO_COD      = V_GRUPO_ECONOMICO_COD    ,
                SUBGRUPO_ECONOMICO_COD   = V_SUBGRUPO_ECONOMICO_COD ,
                NOME_FIRMA               = V_NOME_FIRMA             ,
                MORADA_SEDE              = V_MORADA_SEDE            ,
                LOCALIDADE               = V_LOCALIDADE             ,
                NIF                      = V_NIF                    ,
                OBSERVACOES              = V_OBSERVACOES            ,
                ALTER_DATE               = SYSDATE                    ,
                ALTER_USER               = V_INSERT_USER
            WHERE ID_CONTRATO = V_ID_CONTRATO;     
        END IF;
    END;
    
-- 10S1 ################################################################### 
    PROCEDURE SP_AE_ENTIDADES_GET 
    (
        WHERESTM     VARCHAR2, 
        CUR         OUT SYS_REFCURSOR
    ) IS
        sqlstm VARCHAR2(1000);
        BEGIN 
            sqlstm := 'SELECT * FROM MV_LOCAIS_ENTREGA WHERE ' || WHERESTM;
            OPEN CUR FOR sqlstm;
            COMMIT;
    END ;
  
-- 10S2 ###################################################################       
    PROCEDURE SP_AE_ENTIDADE_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
                SELECT LE.* 
                FROM T_AE_CONTRATO C 
                INNER JOIN T_AE_ENTIDADE E
                    ON(E.ID_CONTRATO = C.ID_CONTRATO)
                INNER JOIN MV_LOCAIS_ENTREGA LE
                    ON(LE.COD_LOCAL_ENTREGA = E.COD_LE)
                WHERE C.ID_CONTRATO = V_ID_CONTRATO
                  AND C.NIVEL_CONTRATO = 'LE'
            UNION 
                SELECT LE.* 
                FROM T_AE_CONTRATO C
                INNER JOIN MV_LOCAIS_ENTREGA LE
                    ON(LE.COD_GRUPO_ECONOMICO_LE = C.GRUPO_ECONOMICO_COD)
                WHERE C.ID_CONTRATO = V_ID_CONTRATO
                  AND C.NIVEL_CONTRATO = 'GE'
            UNION 
                SELECT LE.* 
                FROM T_AE_CONTRATO C
                INNER JOIN MV_LOCAIS_ENTREGA LE
                    ON(LE.COD_GRUPO_ECONOMICO_LE = C.GRUPO_ECONOMICO_COD)
                    AND(LE.COD_SUBGRUPO_ECONOMICO_LE = C.SUBGRUPO_ECONOMICO_COD)
                WHERE C.ID_CONTRATO = V_ID_CONTRATO
                  AND C.NIVEL_CONTRATO = 'SGE';
        COMMIT;    
    END;
    
-- 10I ###################################################################   
    PROCEDURE SP_AE_ENTIDADE_INS
    (
        V_ID_CONTRATO                 INT,
        V_COD_LE                   VARCHAR2,
        V_COD_GRUPO_ECONOMICO      VARCHAR2,
        V_COD_SUB_GRUPO_ECONOMICO  VARCHAR2,
        V_COD_CLIENTE              VARCHAR2
    ) IS
    BEGIN
        INSERT INTO T_AE_ENTIDADE
        (
           ID_CONTRATO               ,
           COD_LE                    ,
           COD_GRUPO_ECONOMICO       ,
           COD_SUB_GRUPO_ECONOMICO   ,
           COD_CLIENTE               ,
           INSERT_DATE
        )
        VALUES
        (
            V_ID_CONTRATO                 ,
            V_COD_LE                      ,
            V_COD_GRUPO_ECONOMICO         ,
            V_COD_SUB_GRUPO_ECONOMICO     ,
            V_COD_CLIENTE                 ,
            SYSDATE
        );
    END;

-- 10D1 ###################################################################       
    PROCEDURE SP_AE_ENTIDADE_DEL 
    (
        V_ID_CONTRATO   INT,
        V_COD_LE        VARCHAR2
    ) IS
    BEGIN
        DELETE T_AE_ENTIDADE
        WHERE ID_CONTRATO = V_ID_CONTRATO
          AND COD_LE = V_COD_LE;
    END;
    
-- 10D2 ###################################################################       
    PROCEDURE SP_AE_ENTIDADES_DEL 
    (
        V_ID_CONTRATO   INT
    ) IS
    BEGIN
        DELETE T_AE_ENTIDADE
        WHERE ID_CONTRATO = V_ID_CONTRATO;
    END;
   
   
-- 11S ###################################################################       
    PROCEDURE SP_AE_ABASTECIMENTO_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
                SELECT A.*, NVL(C.DES_CONCESSIONARIO,'') DES_CONCESSIONARIO 
                FROM T_AE_ABASTECIMENTO A 
                LEFT JOIN CINDY_DW.LK_CONCESSIONARIO C
                    ON(C.COD_CONCESSIONARIO = A.COD_CONCESSIONARIO)
                WHERE A.ID_CONTRATO = V_ID_CONTRATO;
        COMMIT;    
    END;

-- 11I ###################################################################   
    PROCEDURE SP_AE_ABASTECIMENTO_INS
    (
        V_ID_CONTRATO              INT,
        V_COD_CONCESSIONARIO       VARCHAR2,
        V_FACTURACAO               CHAR,
        V_DISTRIBUICAO               CHAR,
        V_OBSERVACOES              VARCHAR2
    ) IS
    BEGIN
        INSERT INTO T_AE_ABASTECIMENTO
        (
           ID_CONTRATO               ,
           COD_CONCESSIONARIO        ,
           FACTURACAO                 ,
           DISTRIBUICAO                 ,
           OBSERVACOES
        )
        VALUES
        (
            V_ID_CONTRATO            ,
            V_COD_CONCESSIONARIO     ,
            V_FACTURACAO             ,
            V_DISTRIBUICAO             ,
            V_OBSERVACOES
        );
    END;
    
-- 11U ###################################################################    
    PROCEDURE SP_AE_ABASTECIMENTO_UPD
    (
        V_ID_CONTRATO              INT,
        V_COD_CONCESSIONARIO       VARCHAR2,
        V_FACTURACAO               CHAR,
        V_DISTRIBUICAO               CHAR,
        V_OBSERVACOES              VARCHAR2
    ) IS
    BEGIN
        UPDATE T_AE_ABASTECIMENTO
        SET FACTURACAO         = V_FACTURACAO             ,
            DISTRIBUICAO       = V_DISTRIBUICAO           ,
            OBSERVACOES           = V_OBSERVACOES
        WHERE ID_CONTRATO        = V_ID_CONTRATO
          AND COD_CONCESSIONARIO = V_COD_CONCESSIONARIO;
    END; 

-- 11D ###################################################################       
    PROCEDURE SP_AE_ABASTECIMENTO_DEL 
    (
        V_ID_CONTRATO            INT,
        V_COD_CONCESSIONARIO    VARCHAR2
    ) IS
    BEGIN
        DELETE T_AE_ABASTECIMENTO
        WHERE ID_CONTRATO = V_ID_CONTRATO
          AND COD_CONCESSIONARIO = V_COD_CONCESSIONARIO;
    END;
   
-- 12S ###################################################################       
    PROCEDURE SP_AE_ACTIVIDADE_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR 
            SELECT *
            FROM T_AE_ACTIVIDADE AC
            INNER JOIN LK_TIPO_ACTIVIDADE TAC
                ON(TAC.ID_TIPO_ACTIVIDADE = AC.ID_TIPO_ACTIVIDADE)
            WHERE ID_CONTRATO = V_ID_CONTRATO;
        COMMIT;
    END;
    
-- 12I ###################################################################    
    PROCEDURE SP_AE_ACTIVIDADE_INS
    (
        V_ID_CONTRATO               INT,
        V_ID_TIPO_ACTIVIDADE        INT,
        V_NUM_ACORDO                VARCHAR2,
        V_VALOR_ACORDO              NUMBER
    ) IS
    BEGIN
        INSERT INTO T_AE_ACTIVIDADE
        (
            ID_ACTIVIDADE       ,
            ID_CONTRATO        , 
            ID_TIPO_ACTIVIDADE ,
            NUM_ACORDO         ,
            VALOR_ACORDO              
        )
        VALUES
        (
            SEQ_AE_ID_ACTIVIDADE.NEXTVAL ,
            V_ID_CONTRATO        ,
            V_ID_TIPO_ACTIVIDADE ,
            V_NUM_ACORDO         ,
            V_VALOR_ACORDO             
        );
    END;
    
-- 12U ###################################################################    
    PROCEDURE SP_AE_ACTIVIDADE_UPD
    (
        V_ID_ACTIVIDADE                INT,
        V_ID_CONTRATO               INT,
        V_ID_TIPO_ACTIVIDADE        INT,
        V_NUM_ACORDO                VARCHAR2,
        V_VALOR_ACORDO              NUMBER
    ) IS
    BEGIN
        UPDATE T_AE_ACTIVIDADE
        SET ID_CONTRATO        = V_ID_CONTRATO            ,        
            ID_TIPO_ACTIVIDADE = V_ID_TIPO_ACTIVIDADE    , 
            NUM_ACORDO         = V_NUM_ACORDO             ,
            VALOR_ACORDO       = V_VALOR_ACORDO           
        WHERE ID_ACTIVIDADE    = V_ID_ACTIVIDADE;        
    END;

-- 12D ###################################################################       
    PROCEDURE SP_AE_ACTIVIDADE_DEL 
    (
        V_ID_ACTIVIDADE   INT
    ) IS
    BEGIN
        DELETE T_AE_ACTIVIDADE
        WHERE ID_ACTIVIDADE = V_ID_ACTIVIDADE;
    END;    

-- 13S ###################################################################       
    PROCEDURE SP_AE_TPR_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT TPR.*, P.DES_PRODUTO NOME_PRODUTO
            FROM T_AE_TPR TPR
            INNER JOIN CINDY_DW.LK_PRODUTO P
                ON(P.COD_PRODUTO = TPR.COD_PRODUTO)
            WHERE TPR.ID_CONTRATO = V_ID_CONTRATO;
        COMMIT;                
    END;
    
-- 13I ###################################################################    
    PROCEDURE SP_AE_TPR_INS
    (
        V_ID_CONTRATO               INT,
        V_COD_PRODUTO               VARCHAR2,
        V_BUDGET_VENDAS             NUMBER,
        V_BUDGET_MARKETING          NUMBER,
        V_ACTIVIDADE_INICIO         DATE,
        V_ACTIVIDADE_FIM            DATE,
        V_COMPRAS_INICIO            DATE,
        V_COMPRAS_FIM               DATE,
        V_PVP                       NUMBER,
        V_UNIDADE_BASE                VARCHAR2,
        V_PREVISAO_QUANTIDADE        NUMBER
    ) IS
    BEGIN
        INSERT INTO T_AE_TPR
        (
            ID_TPR                ,
            ID_CONTRATO         ,
            COD_PRODUTO         ,
            BUDGET_VENDAS       ,
            BUDGET_MARKETING    ,
            ACTIVIDADE_INICIO   ,
            ACTIVIDADE_FIM      ,
            COMPRAS_INICIO      ,
            COMPRAS_FIM         ,
            PVP                 ,
            UNIDADE_BASE        ,
            PREVISAO_QUANTIDADE              
        )
        VALUES
        (
            SEQ_AE_ID_TPR.NEXTVAL ,
            V_ID_CONTRATO         ,
            V_COD_PRODUTO         ,
            V_BUDGET_VENDAS       ,
            V_BUDGET_MARKETING    ,
            V_ACTIVIDADE_INICIO   ,
            V_ACTIVIDADE_FIM      ,
            V_COMPRAS_INICIO      ,
            V_COMPRAS_FIM         ,
            V_PVP                 ,
            V_UNIDADE_BASE          ,
            V_PREVISAO_QUANTIDADE  
        );  
    END;
    
-- 13U ###################################################################    
    PROCEDURE SP_AE_TPR_UPD
    (
        V_ID_TPR                    INT,
        V_ID_CONTRATO               INT,
        V_COD_PRODUTO               VARCHAR2,
        V_BUDGET_VENDAS             NUMBER,
        V_BUDGET_MARKETING          NUMBER,
        V_ACTIVIDADE_INICIO         DATE,
        V_ACTIVIDADE_FIM            DATE,
        V_COMPRAS_INICIO            DATE,
        V_COMPRAS_FIM               DATE,
        V_PVP                       NUMBER,
        V_UNIDADE_BASE                VARCHAR2,
        V_PREVISAO_QUANTIDADE        NUMBER
    ) IS
    BEGIN
        UPDATE T_AE_TPR
        SET
            ID_CONTRATO        = V_ID_CONTRATO          ,
            COD_PRODUTO        = V_COD_PRODUTO          ,
            BUDGET_VENDAS      = V_BUDGET_VENDAS        ,
            BUDGET_MARKETING   = V_BUDGET_MARKETING     ,
            ACTIVIDADE_INICIO  = V_ACTIVIDADE_INICIO    ,
            ACTIVIDADE_FIM     = V_ACTIVIDADE_FIM       ,
            COMPRAS_INICIO     = V_COMPRAS_INICIO       ,
            COMPRAS_FIM        = V_COMPRAS_FIM          ,
            PVP                = V_PVP                  ,
            UNIDADE_BASE       = V_UNIDADE_BASE            ,
            PREVISAO_QUANTIDADE = V_PREVISAO_QUANTIDADE               
        WHERE ID_TPR = V_ID_TPR;
    END;
          
-- 13D ###################################################################       
    PROCEDURE SP_AE_TPR_DEL 
    (
        V_ID_TPR   INT
    ) IS
    BEGIN
        DELETE T_AE_TPR
        WHERE ID_TPR = V_ID_TPR;
    END;    
    
-- 14S ###################################################################       
    PROCEDURE SP_AE_DEBITO_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT * 
            FROM T_AE_DEBITO
            WHERE ID_CONTRATO = V_ID_CONTRATO;       
        COMMIT;            
    END;
    
-- 14I ###################################################################    
    PROCEDURE SP_AE_DEBITO_INS
    (
        V_ID_CONTRATO               INT,
        V_DATA_DEBITO               DATE,
        V_VALOR_DEBITO              NUMBER
    ) IS
    BEGIN
        INSERT INTO T_AE_DEBITO
        (
            ID_DEBITO        ,
            ID_CONTRATO     , 
            DATA_DEBITO     ,
            VALOR_DEBITO       
        )
        VALUES
        (
            SEQ_AE_ID_DEBITO.NEXTVAL ,
            V_ID_CONTRATO        ,
            V_DATA_DEBITO          ,
            V_VALOR_DEBITO
        );
    END;

-- 14U ###################################################################    
    PROCEDURE SP_AE_DEBITO_UPD
    (
        V_ID_DEBITO                    INT,
        V_ID_CONTRATO               INT,
        V_DATA_DEBITO               DATE,
        V_VALOR_DEBITO              NUMBER
    ) IS
    BEGIN
        UPDATE T_AE_DEBITO
        SET ID_CONTRATO   = V_ID_CONTRATO  , 
            DATA_DEBITO   = V_DATA_DEBITO  ,
            VALOR_DEBITO  = V_VALOR_DEBITO    
        WHERE ID_DEBITO = V_ID_DEBITO;
    END;
    
-- 14D ###################################################################       
    PROCEDURE SP_AE_DEBITO_DEL 
    (
        V_ID_DEBITO   INT
    ) IS
    BEGIN
        DELETE T_AE_DEBITO
        WHERE ID_DEBITO = V_ID_DEBITO;
    END;        

-- 15S ###################################################################       
    PROCEDURE SP_AE_LOG_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR 
            SELECT  *
            FROM T_AE_LOG
            WHERE ID_CONTRATO = V_ID_CONTRATO;
        COMMIT;
    END;
          
-- 15I ###################################################################    
    PROCEDURE SP_AE_LOG_INS
    (
        V_ID_CONTRATO            INT,
        V_LOG_DATA               DATE,
        V_LOG_USER               VARCHAR2,
        V_MESSAGE                VARCHAR2
    ) IS
    BEGIN
        INSERT INTO T_AE_LOG
        (
            ID_LOG            ,
            ID_CONTRATO     , 
            LOG_DATA        ,
            LOG_USER        ,
            MESSAGE
        )
        VALUES
        (
            SEQ_AE_ID_LOG.NEXTVAL ,
            V_ID_CONTRATO        ,
            V_LOG_DATA           ,
            V_LOG_USER             ,
            V_MESSAGE            
        );
    END;
END PK_AE;
/
COMMIT;


CREATE OR REPLACE FORCE VIEW V_SRA_T_ENTIDADE_ALL (id_contrato,
                                                   id_processo,
                                                   data_inicio,
                                                   data_fim,
                                                   cod_local_entrega,
                                                   des_local_entrega,
                                                   cod_cliente,
                                                   des_cliente,
                                                   cod_concessionario,
                                                   des_concessionario,
                                                   cod_ramo_actividade,
                                                   morada,
                                                   designacao_comercial,
                                                   nome_contacto,
                                                   telefone,
                                                   fax,
                                                   cod_grupo_economico_le,
                                                   des_grupo_economico_le,
                                                   cod_grupo_economico_cli,
                                                   des_grupo_economico_cli,
                                                   cod_subgrupo_economico_le,
                                                   des_subgrupo_economico_le,
                                                   cod_subgrupo_economico_cli,
                                                   des_subgrupo_economico_cli,
                                                   morada_cliente,
                                                   des_localidade_cli,
                                                   des_ramo_actividade,
                                                   num_contribuinte
                                                  )
AS
   SELECT c.id_contrato, c.id_processo, e.data_inicio, e.data_fim, le.*
     FROM t_contrato c INNER JOIN t_entidade e
          ON (e.id_contrato = c.id_contrato)
          INNER JOIN mv_locais_entrega le ON (le.cod_local_entrega = e.cod_le
                                             )
    WHERE c.nivel_contrato = 'LE'
   UNION
   SELECT c.id_contrato, c.id_processo, c.data_inicio,
          c.data_fim_efectivo data_fim, le.*
     FROM t_contrato c INNER JOIN mv_locais_entrega le
          ON (le.cod_grupo_economico_le = c.cod_grupo_economico)
    WHERE c.nivel_contrato = 'GE'
   UNION
   SELECT c.id_contrato, c.id_processo, c.data_inicio,
          c.data_fim_efectivo data_fim, le.*
     FROM t_contrato c INNER JOIN mv_locais_entrega le
          ON (le.cod_grupo_economico_le = c.cod_grupo_economico)
        AND (le.cod_subgrupo_economico_le = c.cod_subgrupo_economico)
    WHERE c.nivel_contrato = 'SGE';

COMMIT;




-- #######################################################################################################
-- #######################################################################################################
-- ##                                                                                                   ##
-- ##  MIGRAR DADOS                                                                                     ##
-- ##                                                                                                   ##
-- #######################################################################################################
-- #######################################################################################################


    -- ACTUALIZAR T_ENTIDADE DATAS INICIO E FIM = 01-01-1900
        UPDATE T_ENTIDADE
        SET DATA_INICIO = TO_DATE('1900-01-01', 'yyyy-mm-dd'),
            DATA_FIM = TO_DATE('1900-01-01', 'yyyy-mm-dd');


    -- ACTUALIZAR CODIGO DE NIVEL DO CONTRATO
    -- --------------------------------------------------------
        UPDATE T_CONTRATO
        SET NIVEL_CONTRATO = 'LE'
        WHERE NIVEL_CONTRATO = 'NA';


    -- ACTUALIZAR CODIGO DO GRUPO E SUBGRUPO ECONOMICO 
    -- --------------------------------------------------------
        UPDATE T_CONTRATO
        SET COD_SUBGRUPO_ECONOMICO  = COD_GRUPO_ECONOMICO
        WHERE NIVEL_CONTRATO = 'SGE';

        UPDATE T_CONTRATO C
        SET COD_GRUPO_ECONOMICO = (
            SELECT SGE.COD_GRUPO_ECONOMICO
            FROM CINDY_DW.LK_SUBGRUPO_ECONOMICO SGE
            WHERE SGE.COD_SUBGRUPO_ECONOMICO = C.COD_GRUPO_ECONOMICO 
        )
        WHERE NIVEL_CONTRATO = 'SGE';


    -- APAGAR LE'S DE CONTRATOS FEITOS AO GRUPO (GE E SGE)
    -- --------------------------------------------------------
        DELETE T_ENTIDADE
        WHERE ID_CONTRATO in (
            SELECT ID_CONTRATO
            FROM T_CONTRATO
            WHERE NIVEL_CONTRATO <> 'LE'
        );
        

    -- ACTUALIZAR LIGAÇÕES MAL FEITAS NUM_ANTERIOR -> NUM_SEGUINTE / NUM_SEGUINTE -> NUM_ANTERIOR
        -- Há contratos que têm ligações apenas num sentido A->B != B->A
    -- --------------------------------------------------------------
        UPDATE T_CONTRATO CA
        SET NUM_PROCESSO_ANTERIOR  =
        (
            SELECT CC.ID_PROCESSO||'/'||CC.ID_SEQUENCIA
            FROM T_CONTRATO CC
            WHERE CC.NUM_PROCESSO_SEGUINTE = CA.ID_PROCESSO||'/'||CA.ID_SEQUENCIA
            AND CA.NUM_PROCESSO_ANTERIOR is null
            AND CC.NUM_PROCESSO_SEGUINTE IS NOT NULL    
        )
        WHERE CA.NUM_PROCESSO_ANTERIOR is null;   
         
        UPDATE T_CONTRATO CA
        SET NUM_PROCESSO_SEGUINTE  =
        (
            SELECT CC.ID_PROCESSO||'/'||CC.ID_SEQUENCIA
            FROM T_CONTRATO CC
            WHERE CC.NUM_PROCESSO_ANTERIOR = CA.ID_PROCESSO||'/'||CA.ID_SEQUENCIA
            AND CA.NUM_PROCESSO_SEGUINTE is null
            AND CC.NUM_PROCESSO_ANTERIOR IS NOT NULL    
        )
        WHERE CA.NUM_PROCESSO_SEGUINTE is null;    
     
     
    -- LIMPAR CONTRATOS COM SEQUENCIAS ERRADAS
        -- Há contratos que são, ERRADAMENTE, seguintes de outros fazendo com que exista mais que 1 seguinte
    -- --------------------------------------------------------------
        DECLARE    v_ID_CONTRATO  integer;
            CURSOR contrato_cursor IS 
                 SELECT id_contrato
                 FROM T_CONTRATO C
                 WHERE NUM_PROCESSO_ANTERIOR IS NOT NULL
                 AND NOT EXISTS(
                    SELECT * FROM T_CONTRATO
                    WHERE NUM_PROCESSO_SEGUINTE = C.ID_PROCESSO || '/' || C.ID_SEQUENCIA
                    );
        BEGIN
           OPEN contrato_cursor;
               LOOP
                  FETCH contrato_cursor INTO v_ID_CONTRATO;
                  EXIT WHEN contrato_cursor%NOTFOUND;
                    PK_SRA.SP_CONTRATO_DEL ( v_ID_CONTRATO );
               END LOOP;
           CLOSE  contrato_cursor;
        END;


    -- ACTUALIZAR DATA_CONTRATO
    -- A data de contrato corresponde à data do /1
      -- Na nova versão esta data é automática (SYSDATE) e inalterável para cada NUM_PROCESSO
    -- --------------------------------------------------------------------
        UPDATE T_CONTRATO
        SET DATA_CONTRATO = TO_DATE(TO_CHAR(DATA_CONTRATO,'yyyy-mm-dd'),'yyyy-mm-dd');

        UPDATE T_CONTRATO C
        SET DATA_CONTRATO = (
            SELECT DATA_CONTRATO
            FROM T_CONTRATO CC
            WHERE SUBSTR(CC.ID_PROCESSO,8,8) = SUBSTR(C.ID_PROCESSO,8,8) AND CC.ID_SEQUENCIA = 1    
        )
        WHERE ID_SEQUENCIA > 1;


    -- ACTUALIZAR NUM_PROCESSO
        -- O Nº DE PROCESSO DOS NOVOS CONTRATOS É O Nº DE PROCESSO DO /1
    -- --------------------------------------------------------------------        
        UPDATE T_CONTRATO C
        SET ID_PROCESSO = 
        (
            SELECT ID_PROCESSO
            FROM T_CONTRATO
            WHERE ID_SEQUENCIA = 1
              AND SUBSTR (ID_PROCESSO , 8, 8) = SUBSTR (C.ID_PROCESSO , 8, 8) 
        )
        WHERE ID_SEQUENCIA > 1;    
        
    -- CORRIGIR E ACTUALIZAR REFERÊNCIAS NUM_ANTERIOR E NUM_SEGUINTE
        -- O Numero de processo anterior, caso exista, é o do contrato /1
    -- --------------------------------------------------------------------
        UPDATE T_CONTRATO C
        SET NUM_PROCESSO_ANTERIOR = (   
            SELECT C1.NUM_PROCESSO_ANTERIOR
            FROM T_CONTRATO C1   
            WHERE C1.ID_SEQUENCIA = 1
              AND SUBSTR (C1.ID_PROCESSO , 8, 8) =  SUBSTR (C.ID_PROCESSO , 8, 8)
        )
        WHERE C.ID_SEQUENCIA > 1;

        UPDATE T_CONTRATO
        SET NUM_PROCESSO_SEGUINTE = SUBSTR ( NUM_PROCESSO_SEGUINTE , 0, 15),
            NUM_PROCESSO_ANTERIOR = SUBSTR ( NUM_PROCESSO_ANTERIOR , 0, 15);
            

    -- ACTUALIZAR LE's
    -- A lista de LE's corresponde ao DISTINCT dos LE's de todas as sequências de /1 a /N;
      -- A data de inicio dos LE's novos corresponde ao dia seguinte à data de fim efectivo do contrato com sequencia anterior
      -- A data de fim dos LE's que terminam, corresponde á data de fim efectivo da última sequência onde constavam
      -- Os LE's que existem do inicio ao fim do contrato ficam com as datas a NULL
    -- --------------------------------------------------------------------
        DECLARE    
            V_EXISTS        INT;
            V_ID_CONTRATO  INTEGER;
            V_COD_LE       VARCHAR2(15);
            V_DATA_INICIO  DATE;
            V_DATA_FIM      DATE;
        
            CURSOR contrato_cursor IS 
                    SELECT MAXC.ID_CONTRATO, LE.COD_LE, LE.DATA_INICIO, LE.DATA_FIM
                    FROM
                    (
                        SELECT C.ID_CONTRATO, SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO
                        FROM T_CONTRATO C
                        INNER JOIN (
                            SELECT SUBSTR (ID_PROCESSO , 8, 8) ID_PROCESSO, MAX(ID_SEQUENCIA) AS ID_SEQUENCIA
                            FROM T_CONTRATO
                            GROUP BY SUBSTR (ID_PROCESSO , 8, 8) 
                        ) CG
                          ON   (CG.ID_PROCESSO =  SUBSTR (C.ID_PROCESSO , 8, 8)) 
                           AND (CG.ID_SEQUENCIA = C.ID_SEQUENCIA)       
                    ) MAXC
                    INNER JOIN
                    (
                        SELECT 
                            ID_PROCESSO,
                            COD_LE, 
                            CASE 
                                WHEN MIN(ID_SEQUENCIA) = 1 
                                  OR MIN(C_FIM_E_ANT) = MIN(C_INICIO) THEN NULL
                                ELSE MIN(C_FIM_E_ANT+1)   
                            END DATA_INICIO,
                            CASE 
                                WHEN MAX(ID_SEQUENCIA) = 1 
                                  OR MAX(ID_SEQUENCIA_SEG) IS NULL
                                  OR MAX(ID_SEQUENCIA_SEG) = MAX(ID_SEQUENCIA) THEN NULL
                                ELSE MAX(C_FIM_E)   
                            END DATA_FIM 
                        FROM
                        (    
                            SELECT 
                                SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO,
                                C.ID_SEQUENCIA,
                                C3.ID_SEQUENCIA ID_SEQUENCIA_SEG,    
                                E.COD_LE, 
                                C.DATA_INICIO C_INICIO,
                                C2.DATA_FIM_EFECTIVO C_FIM_E_ANT,
                                C.DATA_FIM_EFECTIVO C_FIM_E
                            FROM T_CONTRATO C
                            LEFT JOIN T_CONTRATO C2
                                ON   SUBSTR (C2.ID_PROCESSO , 8, 8) = SUBSTR (C.ID_PROCESSO , 8, 8) 
                                 AND C2.ID_SEQUENCIA = C.ID_SEQUENCIA-1
                             LEFT JOIN T_CONTRATO C3
                                ON   SUBSTR (C3.ID_PROCESSO,8,8) = SUBSTR (C.ID_PROCESSO,8,8)
                                 AND C3.ID_SEQUENCIA = C.ID_SEQUENCIA+1
                            INNER JOIN T_ENTIDADE E
                                ON E.ID_CONTRATO = C.ID_CONTRATO
                            ORDER BY SUBSTR (C.ID_PROCESSO,8,8) DESC, E.COD_LE, C.ID_SEQUENCIA DESC    
                        ) a
                        GROUP BY ID_PROCESSO,COD_LE        
                    ) LE
                        ON LE.ID_PROCESSO = MAXC.ID_PROCESSO;
        BEGIN
               OPEN contrato_cursor;
                   LOOP
                      FETCH contrato_cursor INTO V_ID_CONTRATO,V_COD_LE, V_DATA_INICIO, V_DATA_FIM;
                      EXIT WHEN contrato_cursor%NOTFOUND;
                        
                        SELECT COUNT(*) INTO V_EXISTS
                        FROM T_ENTIDADE
                        WHERE COD_LE = V_COD_LE
                            AND ID_CONTRATO = V_ID_CONTRATO;                  
                        
                        IF V_EXISTS > 0 THEN
                              UPDATE T_ENTIDADE
                              SET DATA_INICIO = V_DATA_INICIO,
                                  DATA_FIM = V_DATA_FIM
                              WHERE COD_LE =V_COD_LE
                                AND ID_CONTRATO = V_ID_CONTRATO;                  
                         ELSE
                             INSERT INTO T_ENTIDADE
                                (
                                   ID_CONTRATO               ,
                                   DATA_INICIO                 ,
                                   DATA_FIM                     ,
                                   COD_LE                    ,
                                   COD_GRUPO_ECONOMICO       ,
                                   COD_SUB_GRUPO_ECONOMICO   ,
                                   COD_CLIENTE               ,
                                   INSERT_DATE
                                )
                                SELECT V_ID_CONTRATO
                                    ,V_DATA_INICIO
                                    ,V_DATA_FIM
                                    ,V_COD_LE
                                    ,COD_GRUPO_ECONOMICO_LE 
                                    ,COD_SUBGRUPO_ECONOMICO_LE
                                    ,COD_CLIENTE
                                    ,SYSDATE
                                FROM MV_LOCAIS_ENTREGA 
                                WHERE COD_LOCAL_ENTREGA = V_COD_LE;                                
                            END IF;                             
                   END LOOP;
               CLOSE  contrato_cursor;
        END;

        DELETE FROM T_ENTIDADE WHERE DATA_INICIO = '1900-01-01';


    -- ACTUALIZAR EQUIPAMENTOS
        -- A lista de equipamentos de frio corresponde ao DISTINCT dos equipamentos de todas as sequências de /1 a /N, com o MAX da quantidade
    -- --------------------------------------------------------------------
        DECLARE    
            V_ID_CONTRATO  INTEGER;
            V_COD_EQUIPAMENTO VARCHAR2(200);
            V_QUANTIDADE NUMBER;
            V_VALOR      NUMBER;
        
            CURSOR contrato_cursor IS
                    SELECT MAXC.ID_CONTRATO, EQ.COD_EQUIPAMENTO, EQ.QUANTIDADE, EQ.VALOR
                    FROM
                    (
                        SELECT C.ID_CONTRATO, SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO
                        FROM T_CONTRATO C
                        INNER JOIN (
                            SELECT SUBSTR (ID_PROCESSO , 8, 8) ID_PROCESSO, MAX(ID_SEQUENCIA) AS ID_SEQUENCIA
                            FROM T_CONTRATO
                            GROUP BY SUBSTR (ID_PROCESSO , 8, 8) 
                        ) CG
                          ON   (CG.ID_PROCESSO =  SUBSTR (C.ID_PROCESSO , 8, 8)) 
                           AND (CG.ID_SEQUENCIA = C.ID_SEQUENCIA)       
                    ) MAXC
                    INNER JOIN
                    (                
                        SELECT 
                            ID_PROCESSO,
                            COD_EQUIPAMENTO, 
                            MAX(QUANTIDADE) QUANTIDADE,
                            MAX(VALOR) VALOR
                        FROM
                        (                                                         
                            SELECT 
                                SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO,
                                E.COD_EQUIPAMENTO,
                                E.QUANTIDADE,
                                E.VALOR 
                            FROM T_CONTRATO C
                            INNER JOIN (
                                SELECT ID_CONTRATO, COD_EQUIPAMENTO, SUM(QUANTIDADE) QUANTIDADE, MAX(VALOR) VALOR
                                FROM T_CONT_EQUIPAMENTO
                                GROUP BY ID_CONTRATO, COD_EQUIPAMENTO
                            ) E
                                ON E.ID_CONTRATO = C.ID_CONTRATO
                            ORDER BY SUBSTR (C.ID_PROCESSO,8,8) DESC, E.COD_EQUIPAMENTO, C.ID_SEQUENCIA DESC                                             
                        ) a
                        GROUP BY ID_PROCESSO,COD_EQUIPAMENTO                                                
                    ) EQ
                        ON EQ.ID_PROCESSO = MAXC.ID_PROCESSO;                    
        BEGIN
               OPEN contrato_cursor;
                   LOOP
                      FETCH contrato_cursor INTO V_ID_CONTRATO,V_COD_EQUIPAMENTO, V_QUANTIDADE, V_VALOR;
                      EXIT WHEN contrato_cursor%NOTFOUND;
                      
                          DELETE T_CONT_EQUIPAMENTO
                          WHERE ID_CONTRATO = V_ID_CONTRATO
                            AND COD_EQUIPAMENTO = V_COD_EQUIPAMENTO;
                            
                          INSERT INTO T_CONT_EQUIPAMENTO
                          (
                               ID_CONTRATO            ,
                               ID_CONT_EQUIPAMENTO     ,
                               COD_EQUIPAMENTO         ,
                               QUANTIDADE               ,
                               VALOR
                          )
                          VALUES
                          (
                                V_ID_CONTRATO       ,
                                SEQ_ID_CONT_EQUIPAMENTO.NEXTVAL,
                                V_COD_EQUIPAMENTO    ,
                                V_QUANTIDADE         ,
                                V_VALOR
                          );
                          
                   END LOOP;
               CLOSE  contrato_cursor;
        END;


    -- ACTUALIZAR VERBAS
        -- A lista de verbas corresponde ao DISTINCT das datas de pagamento previsto de todas as sequências de /1 a /N
    -- --------------------------------------------------------------------
        UPDATE T_FASEAMENTO_PAG
        SET DATA_PAGAMENTO = TO_DATE(TO_CHAR(DATA_PAGAMENTO,'yyyy-mm-dd'),'yyyy-mm-dd'),
            DATA_PAGAMENTO_EFECTUADO = TO_DATE(TO_CHAR(DATA_PAGAMENTO_EFECTUADO,'yyyy-mm-dd'),'yyyy-mm-dd');

        DECLARE    
            V_ID_CONTRATO              INTEGER;
            V_LAST_ID_CONTRATO         INTEGER;
            V_DATA_PAGAMENTO           DATE;
            V_VALOR_PAGAMENTO          NUMBER (10,2);
            V_COMP_CONCESSIONARIO      NUMBER (10,3);
            V_TIPO_PG                  VARCHAR2 (2);
            V_TTS                      VARCHAR2 (20);
            V_DATA_PAGAMENTO_EFECTUADO DATE;
      
      
            CURSOR contrato_cursor IS            
                  SELECT MAXC.ID_CONTRATO
                      ,EQ.DATA_PAGAMENTO
                      ,EQ.VALOR_PAGAMENTO
                      ,EQ.COMP_CONCESSIONARIO
                      ,EQ.TIPO_PG
                      ,EQ.TTS
                      ,EQ.DATA_PAGAMENTO_EFECTUADO 
                  FROM
                  (
                      SELECT C.ID_CONTRATO, SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO
                      FROM T_CONTRATO C
                      INNER JOIN (
                          SELECT SUBSTR (ID_PROCESSO , 8, 8) ID_PROCESSO, MAX(ID_SEQUENCIA) AS ID_SEQUENCIA
                          FROM T_CONTRATO
                          GROUP BY SUBSTR (ID_PROCESSO , 8, 8) 
                      ) CG
                        ON   (CG.ID_PROCESSO =  SUBSTR (C.ID_PROCESSO , 8, 8)) 
                         AND (CG.ID_SEQUENCIA = C.ID_SEQUENCIA)       
                  ) MAXC
                  INNER JOIN
                  (                                  
                      SELECT  DISTINCT
                          SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO
                          ,E.DATA_PAGAMENTO
                          ,E.VALOR_PAGAMENTO
                          ,E.COMP_CONCESSIONARIO
                          ,E.TIPO_PG
                          ,E.TTS
                          ,E.DATA_PAGAMENTO_EFECTUADO 
                      FROM T_CONTRATO C
                      INNER JOIN
                      (                             
                          SELECT ID_CONTRATO
                              ,DATA_PAGAMENTO
                              ,VALOR_PAGAMENTO
                              ,MAX(COMP_CONCESSIONARIO) COMP_CONCESSIONARIO
                              ,MAX(TIPO_PG) TIPO_PG
                              ,TTS
                              ,MAX(DATA_PAGAMENTO_EFECTUADO) DATA_PAGAMENTO_EFECTUADO
                          FROM T_FASEAMENTO_PAG
                          GROUP BY ID_CONTRATO,DATA_PAGAMENTO,VALOR_PAGAMENTO,TTS    
                      ) E
                          ON E.ID_CONTRATO = C.ID_CONTRATO
                  ) EQ
                      ON EQ.ID_PROCESSO = MAXC.ID_PROCESSO;   
        BEGIN
            SELECT 0 INTO V_LAST_ID_CONTRATO FROM DUAL;  
            OPEN contrato_cursor;
                LOOP
                    FETCH contrato_cursor INTO 
                         V_ID_CONTRATO             
                        ,V_DATA_PAGAMENTO          
                        ,V_VALOR_PAGAMENTO         
                        ,V_COMP_CONCESSIONARIO     
                        ,V_TIPO_PG                 
                        ,V_TTS
                        ,V_DATA_PAGAMENTO_EFECTUADO;                     
                            
                    EXIT WHEN contrato_cursor%NOTFOUND;
                    
                    IF V_LAST_ID_CONTRATO <> V_ID_CONTRATO THEN
                        BEGIN
                            DELETE T_FASEAMENTO_PAG
                            WHERE ID_CONTRATO = V_ID_CONTRATO;
                            
                            V_LAST_ID_CONTRATO := V_ID_CONTRATO;
                        END;
                    END IF;
                              
                    INSERT INTO T_FASEAMENTO_PAG
                    (
                         ID_CONTRATO         
                        ,ID_FASEAMENTO        
                        ,DATA_PAGAMENTO       
                        ,DATA_PAGAMENTO_EFECTUADO       
                        ,VALOR_PAGAMENTO      
                        ,COMP_CONCESSIONARIO  
                        ,TIPO_PG              
                        ,TTS
                        ,INSERT_DATE
                    )
                    VALUES
                    (
                         V_ID_CONTRATO          
                        ,SEQ_ID_FASEAMENTO.NEXTVAL         
                        ,V_DATA_PAGAMENTO        
                        ,V_DATA_PAGAMENTO_EFECTUADO        
                        ,V_VALOR_PAGAMENTO       
                        ,V_COMP_CONCESSIONARIO   
                        ,V_TIPO_PG               
                        ,V_TTS
                        ,SYSDATE
                    );                       
                                    
                END LOOP;
            CLOSE  contrato_cursor;
        END;
        

    -- ACTUALIZAR OBSERVAÇÕES
        -- As observações são a concatenação do DISTINCT de todas as sequências
    -- --------------------------------------------------------------------
      DECLARE    
            V_ID_CONTRATO   INTEGER;
            V_OBSERVACOES   VARCHAR2(4000);
            V_EXISTS        INTEGER;      
      
            CURSOR contrato_cursor IS            
                SELECT MAXC.ID_CONTRATO
                    ,EQ.OBSERVACOES
                FROM
                (
                    SELECT C.ID_CONTRATO, SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO
                    FROM T_CONTRATO C
                    INNER JOIN (
                        SELECT SUBSTR (ID_PROCESSO , 8, 8) ID_PROCESSO, MAX(ID_SEQUENCIA) AS ID_SEQUENCIA
                        FROM T_CONTRATO
                        GROUP BY SUBSTR (ID_PROCESSO , 8, 8) 
                    ) CG
                        ON   (CG.ID_PROCESSO =  SUBSTR (C.ID_PROCESSO , 8, 8)) 
                         AND (CG.ID_SEQUENCIA = C.ID_SEQUENCIA)       
                ) MAXC
                INNER JOIN
                (                         
                    SELECT  DISTINCT
                      SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO
                      ,OBSERVACOES
                    FROM T_CONTRATO C
                ) EQ
                    ON EQ.ID_PROCESSO = MAXC.ID_PROCESSO;  
        BEGIN
            OPEN contrato_cursor;
                LOOP
                    FETCH contrato_cursor INTO 
                        V_ID_CONTRATO             
                        ,V_OBSERVACOES;
                            
                    EXIT WHEN contrato_cursor%NOTFOUND;
                    
                    SELECT COUNT(*) INTO V_EXISTS
                    FROM T_CONTRATO
                    WHERE ID_CONTRATO = V_ID_CONTRATO
                      AND OBSERVACOES LIKE '%'||V_OBSERVACOES||'%';
                      
                    IF V_EXISTS = 0 AND LENGTH(V_OBSERVACOES) > 0 THEN
                        UPDATE T_CONTRATO
                        SET OBSERVACOES = OBSERVACOES || CHR(13) || CHR(10) || V_OBSERVACOES
                        WHERE ID_CONTRATO = V_ID_CONTRATO;
                    END IF;
                        
                END LOOP;
            CLOSE  contrato_cursor;
        END;
        

    -- ACTUALIZAR REGISTOS
    -- O registo é a concatenação de todas as sequências
    -- --------------------------------------------------------------------
        DECLARE    
            V_ID_CONTRATO           INTEGER;
            V_OBSERVACOES_MOTIVO    VARCHAR2(4000);
            V_EXISTS                INTEGER;      
      
            CURSOR contrato_cursor IS            
                SELECT MAXC.ID_CONTRATO
                    ,EQ.OBSERVACOES_MOTIVO
                FROM
                (
                    SELECT C.ID_CONTRATO, SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO
                    FROM T_CONTRATO C
                    INNER JOIN (
                        SELECT SUBSTR (ID_PROCESSO , 8, 8) ID_PROCESSO, MAX(ID_SEQUENCIA) AS ID_SEQUENCIA
                        FROM T_CONTRATO
                        GROUP BY SUBSTR (ID_PROCESSO , 8, 8) 
                    ) CG
                        ON   (CG.ID_PROCESSO =  SUBSTR (C.ID_PROCESSO , 8, 8)) 
                         AND (CG.ID_SEQUENCIA = C.ID_SEQUENCIA)       
                ) MAXC
                INNER JOIN
                (                         
                    SELECT  DISTINCT
                      SUBSTR (C.ID_PROCESSO , 8, 8) ID_PROCESSO
                      ,OBSERVACOES_MOTIVO
                    FROM T_CONTRATO C
                ) EQ
                    ON EQ.ID_PROCESSO = MAXC.ID_PROCESSO;  
        BEGIN
            OPEN contrato_cursor;
                LOOP
                    FETCH contrato_cursor INTO 
                        V_ID_CONTRATO             
                        ,V_OBSERVACOES_MOTIVO;
                            
                    EXIT WHEN contrato_cursor%NOTFOUND;
                    
                    SELECT COUNT(*) INTO V_EXISTS
                    FROM T_CONTRATO
                    WHERE ID_CONTRATO = V_ID_CONTRATO
                      AND OBSERVACOES_MOTIVO LIKE '%'||V_OBSERVACOES_MOTIVO||'%';
                      
                    IF V_EXISTS = 0 AND LENGTH(V_OBSERVACOES_MOTIVO) > 0 THEN
                        UPDATE T_CONTRATO
                        SET OBSERVACOES_MOTIVO = OBSERVACOES_MOTIVO || CHR(13) || CHR(10) || V_OBSERVACOES_MOTIVO
                        WHERE ID_CONTRATO = V_ID_CONTRATO;
                    END IF;
                        
                END LOOP;
            CLOSE  contrato_cursor;
        END;


    -- APAGAR CONTRATOS NÃO AGREGADOS
        -- Apagar os contratos com sequencia < /N
    -- --------------------------------------------------------------------
        DECLARE    
            V_ID_CONTRATO  INTEGER;
        
            CURSOR contrato_cursor IS
                    SELECT ID_CONTRATO 
                    FROM T_CONTRATO
                    WHERE ID_CONTRATO NOT IN 
                    (
                        SELECT C.ID_CONTRATO
                        FROM T_CONTRATO C
                        INNER JOIN (
                            SELECT SUBSTR (ID_PROCESSO , 8, 8) ID_PROCESSO, MAX(ID_SEQUENCIA) AS ID_SEQUENCIA
                            FROM T_CONTRATO
                            GROUP BY SUBSTR (ID_PROCESSO , 8, 8)
                        ) CG
                          ON   (CG.ID_PROCESSO =  SUBSTR (C.ID_PROCESSO , 8, 8)) 
                           AND (CG.ID_SEQUENCIA = C.ID_SEQUENCIA)
                    );               
        BEGIN
               OPEN contrato_cursor;
                   LOOP
                      FETCH contrato_cursor INTO V_ID_CONTRATO;
                      EXIT WHEN contrato_cursor%NOTFOUND;
                         PK_SRA.SP_CONTRATO_DEL ( V_ID_CONTRATO );
                   END LOOP;
               CLOSE  contrato_cursor;
        END;


    -- ACTUALIZAR ULTIMAS REFERÊNCIAS INVÁLIDAS
    -- ----------------------------------------------------------------------------------
        UPDATE T_CONTRATO CA
        SET NUM_PROCESSO_ANTERIOR  =
        (
            SELECT CC.ID_PROCESSO
            FROM T_CONTRATO CC
            WHERE CC.NUM_PROCESSO_SEGUINTE = CA.ID_PROCESSO
        )
        WHERE CA.NUM_PROCESSO_ANTERIOR IS NOT NULL
          AND CA.NUM_PROCESSO_ANTERIOR NOT IN (SELECT ID_PROCESSO FROM T_CONTRATO);

        
    -- ADICIONAR LOG DE MIGRAÇÃO
        -- Inserir no inicio do registo a data e a descrição 'IMPORTADO'"
    -- --------------------------------------------------------------------
        UPDATE T_CONTRATO
        SET OBSERVACOES_MOTIVO = '01-02-2010 - 00:00:00 (SYSTEM) -> MIGRAÇÃO' || CHR(13) || CHR(10) || OBSERVACOES_MOTIVO
        WHERE ID_SEQUENCIA = 1;
        
        UPDATE T_CONTRATO
        SET OBSERVACOES_MOTIVO = '01-02-2010 - 00:00:00 (SYSTEM) -> MIGRAÇÃO COM SEQUENCIA' || CHR(13) || CHR(10) || OBSERVACOES_MOTIVO
        WHERE ID_SEQUENCIA > 1;


    -- APAGAR COLUNA ID_SEQUENCIA
    -- -----------------------------------------------------
        ALTER TABLE T_CONTRATO 
        DROP COLUMN ID_SEQUENCIA;
        
        ALTER TABLE T_CONTRATO_APAGADO 
        DROP COLUMN ID_SEQUENCIA;

COMMIT;
