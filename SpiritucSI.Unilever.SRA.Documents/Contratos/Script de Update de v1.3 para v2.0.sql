--  ################################################################## 
--  #  ESTE FICHEIRO DESTINA-SE A TRANSFORMAR  O MODELO DE DADOS     # 
--  #  DA VERS�O 1.3 PARA A VERS�O 2.0                               # 
--  #                                                                # 
--  #  O SCRIPT AQUI DESENVOLVIDO DEVE SER EXECUTADO                 # 
--  #  APENAS UMA VEZ EM CADA BASE DE DADOS A ACTUALIZAR.            # 
--  #  PARA EXECUTAR PRIMIR (F5)                                     # 
--  #                                                                # 
--  ################################################################## 


-- ALTERAR AS TABELAS NECESS�RIAS 
-- ################################################################## 

-- TABELA "T_BONUS" 
ALTER TABLE T_BONUS
	ADD (TIPO  CHAR(3));
	
-- TABELA "T_BONUS_GLOBAL" 
ALTER TABLE T_BONUS_GLOBAL
	ADD (TIPO  CHAR(3));

-- TABELA "T_CONTRATO" 
ALTER TABLE T_CONTRATO
	MODIFY(OBSERVACOES VARCHAR2(4000 BYTE));

ALTER TABLE T_CONTRATO
	MODIFY(OBSERVACOES_MOTIVO VARCHAR2(4000 BYTE));

-- TABELA "T_CONTRATO_APAGADO" 
ALTER TABLE T_CONTRATO_APAGADO
MODIFY(OBSERVACOES VARCHAR2(4000 BYTE));

ALTER TABLE T_CONTRATO_APAGADO
MODIFY(OBSERVACOES_MOTIVO VARCHAR2(4000 BYTE));

-- TABELA "SSIS_TMP_CONTRATO" 
ALTER TABLE SSIS_TMP_CONTRATO
	MODIFY(OBSERVACOES VARCHAR2(4000 BYTE));

ALTER TABLE SSIS_TMP_CONTRATO
	MODIFY(OBSERVACOES_MOTIVO VARCHAR2(4000 BYTE));

-- TABELA "T_FASEAMENTO_PAG" 
ALTER TABLE T_FASEAMENTO_PAG
	ADD (DATA_PAGAMENTO_EFECTUADO  DATE);
	
 -- ________________________________________________________________________________________________________________
 
 

-- ACTUALIZAR TIPO PARA AS TABELAS DE BONUS ASSUMINDO QUE S�O TODOS ACUMULADO
-- ################################################################## 
UPDATE T_BONUS
SET
   TIPO = 'Acc';  

UPDATE T_BONUS_GLOBAL
SET
   TIPO = 'Acc';    



		
-- ALTERA��O DAS PROCEDURES  PACKAGE 
-- ################################################################### 
  
CREATE OR REPLACE PACKAGE SRA.PK_SRA AS

  PROCEDURE SP_ID_SEQUENCIA_GET  (V_ID_PROCESSO VARCHAR2, V_ID_SEQUENCIA OUT INT );
  PROCEDURE SP_EQUIPAMENTO_GET (CUR OUT SYS_REFCURSOR);
  PROCEDURE SP_GRUPO_PRODUTO_GET (CUR OUT SYS_REFCURSOR);
  PROCEDURE SP_TTS_GET (CUR OUT SYS_REFCURSOR);
  PROCEDURE SP_MATERIAL_VISIBILIDADE_GET (CUR OUT SYS_REFCURSOR);
  PROCEDURE SP_ENTIDADES_GET (wherestm VARCHAR2, CUR OUT SYS_REFCURSOR);

  PROCEDURE SP_CONTRATO_INS
   (
      V_CONCESSIONARIO IN       INT,
      V_ID_CONTRATO      IN OUT   INT,
        V_ID_PROCESSO   IN OUT   VARCHAR2,
      V_ID_SEQUENCIA  IN OUT   INT,
      V_CLASSIFICACAO_CONTRATO CHAR,
       V_TIPO_CONTRATO            VARCHAR2,
      V_DATA_CONTRATO           DATE,
         V_NIVEL_CONTRATO           VARCHAR2,
      V_NIVEL_CODIGO           VARCHAR2,
      V_NUM_PROCESSO_ANTERIOR  VARCHAR2,
      V_NUM_PROCESSO_SEGUINTE  VARCHAR2,
      V_STATUS                   CHAR,
      V_OBSERVACOES               VARCHAR2,
      V_OBSERVACOES_MOTIVO       VARCHAR2,
      V_NOME_FIRMA                VARCHAR2,
      V_MORADA_SEDE               VARCHAR2,
      V_LOCALIDADE               VARCHAR2,
      V_RAMO_ACTIVIDADE           VARCHAR2,
      V_MATRICULA               VARCHAR2,
      V_MATRICULA_NUM            VARCHAR2,
      V_CAPITAL_SOCIAL           VARCHAR2,
      V_NIF                       VARCHAR2,
      V_DESIGNACAO_PT_VENDA       VARCHAR2,
      V_MORADA_PT_VENDA           VARCHAR2,
      V_OBJECTIVO               VARCHAR2,
      V_IS_POOC                   CHAR,
      V_DATA_INICIO               DATE,
      V_DATA_FIM               DATE,
      V_DATA_FIM_EFECTIVO       DATE,
      V_QUANTIA_FIM               INT,
      V_ANOS_RENOVACAO           INT,
      V_IS_CONTRATO_FORMAL       CHAR,
      V_IS_FORN_FRIO_IGLO       CHAR,
      V_PREVISAO_VENDAS_GLOBAL INTEGER,
      V_PRAZO_PAGAMENTO           CHAR,
      V_NIB_CLIENTE               VARCHAR2,
      V_BANCO                   VARCHAR2,
      V_BANCO_DEPENDENCIA       VARCHAR2,
      V_COMP_CONC_COMPRAS       NUMBER,
      V_INSERT_USER               VARCHAR2,
      V_EXISTS            OUT    INT
   );

   PROCEDURE SP_ENTIDADE_INS
    (
        V_ID_CONTRATO                 INT,
        V_COD_LE                   VARCHAR2,
        V_COD_GRUPO_ECONOMICO      VARCHAR2,
        V_COD_SUB_GRUPO_ECONOMICO  VARCHAR2,
        V_COD_CLIENTE              VARCHAR2
    );

  PROCEDURE SP_MATERIAL_INS
  (
    V_ID_CONTRATO          INT,
    V_COD_MATERIAL      INTEGER,
    V_QUNTIDADE         INTEGER,
    V_VALOR             NUMBER,
    V_OBSERVACOES       VARCHAR2
  );

  PROCEDURE SP_FASEAMENTO_INS
  (
    V_ID_CONTRATO                 INT,
    V_DATA_PAGAMENTO       DATE         ,
    V_DATA_PAGAMENTO_EFECTUADO       DATE         ,
    V_VALOR_PAGAMENTO      NUMBER       ,
    V_COMP_CONCESSIONARIO  NUMBER      ,
    V_TIPO_PG              VARCHAR2     ,
    V_TTS                  VARCHAR2

 );

  PROCEDURE SP_GAMA_INS
  (
        V_ID_CONTRATO                 INT,
       V_COD_GRUPO_PRODUTO      VARCHAR2
  );
  PROCEDURE SP_GAMA_GLOBAL_INS
  (
       V_ID_CONTRATO                 INT,
       V_COD_GRUPO_PRODUTO      VARCHAR2
  );

  PROCEDURE SP_EXPLORACAO_INS
  (
     V_ID_CONTRATO                 INT,
     V_TIPO_EXPLORACAO      VARCHAR2
  );

  PROCEDURE SP_EQUIPAMENTO_INS
  (
     V_ID_CONTRATO                 INT,
     V_COD_EQUIPAMENTO      VARCHAR2   ,
     V_QUANTIDADE            INTEGER
  );

  PROCEDURE SP_CONTRAPARTIDA_INS
  (
     V_ID_CONTRATO          INT,
     V_ID_CONTRAPARTIDA    IN  OUT INTEGER     ,
     V_COD_GRUPO_PRODUTO   IN      VARCHAR2    ,
     V_PREVISAO_VENDAS     IN      INTEGER     ,
     V_DESCONTO_FACTURA    IN      NUMBER     ,
     V_COMP_CONCESSIONARIO IN      NUMBER,
     V_PRAZO_PAGAMENTO       IN       CHAR
  );

  PROCEDURE SP_BONUS_INS
  (
    V_ID_CONTRAPARTIDA     INTEGER ,
    V_VALOR_LIMITE_MIN     INTEGER ,
    V_DESCONTO             NUMBER ,
    V_COMP_CONCESSIONARIO  NUMBER,
    V_TIPO                 IN  CHAR
  );

  PROCEDURE SP_BONUS_GLOBAL_INS
  (
    V_ID_CONTRATO            INTEGER ,
    V_VALOR_LIMITE_MIN     INTEGER ,
    V_DESCONTO             NUMBER ,
    V_COMP_CONCESSIONARIO  NUMBER,
    V_TIPO                  IN CHAR
  );

  PROCEDURE SP_CONTRATOS_GET (wherestm VARCHAR2, CUR OUT SYS_REFCURSOR );

  PROCEDURE SP_CONTRATO_GET
    (
        V_ID_CONTRATO INT,
        CUR_CONTRATO                   OUT SYS_REFCURSOR,
        CUR_ENTIDADE                   OUT SYS_REFCURSOR,
        CUR_EXPLORACAO                 OUT SYS_REFCURSOR,
        CUR_BONUS                      OUT SYS_REFCURSOR,
        CUR_BONUS_GLOBAL               OUT SYS_REFCURSOR,
        CUR_CONT_EQUIPAMENTO           OUT SYS_REFCURSOR,
        CUR_CONT_MAT_VISIBILIDADE      OUT SYS_REFCURSOR,
        CUR_CONTRAPARTIDAS             OUT SYS_REFCURSOR,
        CUR_FASEAMENTO_PAG             OUT SYS_REFCURSOR,
        CUR_GAMAS_QUANTIA_FIM          OUT SYS_REFCURSOR,
        CUR_GAMAS_GLOBAL               OUT SYS_REFCURSOR
    );


    PROCEDURE SP_CONTRATO_STATUS_UPD
    (
         V_ID_CONTRATO INT,
        V_STATUS      CHAR,
        V_MOTIVO      VARCHAR2,
        V_DATA_FIM_EFECTIVO DATE,
        V_NUM_PROCESSO_SEGUINTE VARCHAR2,
        V_ALTER_USER  VARCHAR2
    );

    PROCEDURE SP_CONTRATO_DEL
    (
         V_ID_CONTRATO INT
    );

    PROCEDURE SSIS_LOAD_TMP_FACT_SRA_PL;
    PROCEDURE SSIS_LOAD_TMP_SRA_PROMOCOES;
    PROCEDURE SSIS_LOAD_TMP_SRA_VENDAS;
    PROCEDURE SSIS_LOAD_TMP_SRA_VENDAS_T;



END PK_SRA;
/

COMMIT;



CREATE OR REPLACE PACKAGE BODY SRA.PK_SRA AS

  PROCEDURE SP_ID_SEQUENCIA_GET  (V_ID_PROCESSO VARCHAR2, V_ID_SEQUENCIA OUT INT ) IS
  BEGIN

   SELECT MAX( ID_SEQUENCIA ) + 1 INTO V_ID_SEQUENCIA FROM T_CONTRATO WHERE ID_PROCESSO = V_ID_PROCESSO;
   COMMIT;

  END;

--   PROCEDURE SP_ID_PROCESSO_GET (V_ID_PROCESSO OUT VARCHAR2) IS
--    BEGIN
--
--        SELECT  TO_NUMBER( MAX( SUBSTR (ID_PROCESSO, 8, 8))) INTO V_ID_PROCESSO FROM T_CONTRATO;
--
--        IF V_ID_PROCESSO IS NULL THEN
--              V_ID_PROCESSO := '00000001';
--        ELSE
--          V_ID_PROCESSO := LPAD(V_ID_PROCESSO  + 1,8,0);
--        END IF;
--   END;

  PROCEDURE SP_EQUIPAMENTO_GET (CUR OUT SYS_REFCURSOR) IS

    BEGIN -- executable part starts here

        OPEN CUR FOR SELECT * FROM T_TIPO_EQUIPAMENTO;

    COMMIT;

   END;

    PROCEDURE SP_GRUPO_PRODUTO_GET (CUR OUT SYS_REFCURSOR) IS

    BEGIN -- executable part starts here

        OPEN CUR FOR SELECT * FROM T_GRUPO_PRODUTO ORDER BY COD_GRUPO_PRODUTO ASC;

    COMMIT;

   END;


    PROCEDURE SP_TTS_GET (CUR OUT SYS_REFCURSOR) IS

    BEGIN -- executable part starts here

        OPEN CUR FOR SELECT * FROM MV_TTS;

    COMMIT;

   END;

   PROCEDURE SP_MATERIAL_VISIBILIDADE_GET (CUR OUT SYS_REFCURSOR) IS

    BEGIN -- executable part starts here

        OPEN CUR FOR SELECT * FROM TO_MATERIAL_POS;

    COMMIT;

   END;

    PROCEDURE SP_ENTIDADES_GET (wherestm VARCHAR2, CUR OUT SYS_REFCURSOR) IS

        sqlstm VARCHAR2(1000);

        cursor_handle INTEGER;
        feedback INTEGER;

        BEGIN -- executable part starts here

            sqlstm := 'SELECT  * FROM MV_LOCAIS_ENTREGA WHERE ' || wherestm;
            OPEN CUR FOR sqlstm;

            COMMIT;

    END SP_ENTIDADES_GET;

   PROCEDURE SP_CONTRATO_INS
   (
         V_CONCESSIONARIO IN       INT,
         V_ID_CONTRATO      IN OUT   INT,
         V_ID_PROCESSO   IN OUT   VARCHAR2,
      V_ID_SEQUENCIA  IN OUT   INT,
      V_CLASSIFICACAO_CONTRATO CHAR,
       V_TIPO_CONTRATO            VARCHAR2,
      V_DATA_CONTRATO           DATE,
         V_NIVEL_CONTRATO           VARCHAR2,
      V_NIVEL_CODIGO           VARCHAR2,
      V_NUM_PROCESSO_ANTERIOR  VARCHAR2,
      V_NUM_PROCESSO_SEGUINTE  VARCHAR2,
      V_STATUS                   CHAR,
      V_OBSERVACOES               VARCHAR2,
      V_OBSERVACOES_MOTIVO       VARCHAR2,
      V_NOME_FIRMA                VARCHAR2,
      V_MORADA_SEDE               VARCHAR2,
      V_LOCALIDADE               VARCHAR2,
      V_RAMO_ACTIVIDADE           VARCHAR2,
      V_MATRICULA               VARCHAR2,
      V_MATRICULA_NUM            VARCHAR2,
      V_CAPITAL_SOCIAL           VARCHAR2,
      V_NIF                       VARCHAR2,
      V_DESIGNACAO_PT_VENDA       VARCHAR2,
      V_MORADA_PT_VENDA           VARCHAR2,
      V_OBJECTIVO               VARCHAR2,
      V_IS_POOC                   CHAR,
      V_DATA_INICIO               DATE,
      V_DATA_FIM               DATE,
      V_DATA_FIM_EFECTIVO       DATE,
      V_QUANTIA_FIM               INT,
      V_ANOS_RENOVACAO           INT,
      V_IS_CONTRATO_FORMAL       CHAR,
      V_IS_FORN_FRIO_IGLO       CHAR,
      V_PREVISAO_VENDAS_GLOBAL INTEGER,
      V_PRAZO_PAGAMENTO           CHAR,
      V_NIB_CLIENTE               VARCHAR2,
      V_BANCO                   VARCHAR2,
      V_BANCO_DEPENDENCIA       VARCHAR2,
      V_COMP_CONC_COMPRAS       NUMBER,
      V_INSERT_USER               VARCHAR2,
      V_EXISTS            OUT    INT
   )
   IS

   ID INT:=NULL;
   V_NUM NVARCHAR2(20):=NULL;
   V_SEQ CHAR(1):=NULL;

   BEGIN

    SELECT COUNT(*) INTO V_EXISTS FROM T_CONTRATO WHERE ID_PROCESSO=V_ID_PROCESSO  AND  ID_SEQUENCIA=V_ID_SEQUENCIA;

    IF V_EXISTS = 0 THEN

           IF V_ID_PROCESSO IS NULL THEN

           SELECT  TO_NUMBER( MAX( SUBSTR (ID_PROCESSO, 8, 8))) INTO V_ID_PROCESSO FROM T_CONTRATO;

           IF V_ID_PROCESSO IS NULL THEN
                 V_ID_PROCESSO := TO_CHAR(SYSDATE,'YYYY') || LPAD(V_CONCESSIONARIO,3,0) ||'00000001';
           ELSE
             V_ID_PROCESSO := TO_CHAR(SYSDATE,'YYYY') || LPAD(V_CONCESSIONARIO,3,0) || LPAD(V_ID_PROCESSO  + 1,8,0);
           END IF;

        END IF;

        SELECT SRA.SEQ_ID_CONTRATO.NEXTVAL INTO ID FROM DUAL;

           INSERT INTO T_CONTRATO
        (
        ID_CONTRATO                       ,
         ID_PROCESSO                  ,
          ID_SEQUENCIA                  ,
        CLASSIFICACAO_CONTRATO         ,
        TIPO_CONTRATO                  ,
         DATA_CONTRATO                 ,
         NIVEL_CONTRATO                 ,
         NIVEL_CODIGO                 ,
         NOME_FIRMA                      ,
         MORADA_SEDE                     ,
         LOCALIDADE                     ,
         RAMO_ACTIVIDADE                 ,
         MATRICULA                     ,
         MATRICULA_NUM                  ,
         CAPITAL_SOCIAL                 ,
         NIF                             ,
         DESIGNACAO_PT_VENDA             ,
         MORADA_PT_VENDA                 ,
         NUM_PROCESSO_ANTERIOR        ,
        NUM_PROCESSO_SEGUINTE         ,
         OBJECTIVO                     ,
         IS_POOC                         ,
          DATA_INICIO                     ,
         DATA_FIM                     ,
         DATA_FIM_EFECTIVO             ,
         QUANTIA_FIM                     ,
         ANOS_RENOVACAO                 ,
         IS_CONTRATO_FORMAL             ,
         IS_FORN_FRIO_IGLO             ,
        PREVISAO_VENDAS_GLOBAL         ,
        PRAZO_PAGAMENTO                 ,
         NIB_CLIENTE                     ,
         BANCO                         ,
         BANCO_DEPENDENCIA             ,
         STATUS                         ,
         COMP_CONC_COMPRAS             ,
         OBSERVACOES                     ,
        OBSERVACOES_MOTIVO             ,
         INSERT_USER                     ,
         INSERT_DATE                     ,
        IS_ACTIVE                     ,
        IS_IMPORTADO
        )
        VALUES
        (
         ID                                ,
         V_ID_PROCESSO                   ,
          V_ID_SEQUENCIA              ,
         V_CLASSIFICACAO_CONTRATO     ,
         V_TIPO_CONTRATO              ,
          V_DATA_CONTRATO             ,
         V_NIVEL_CONTRATO             ,
         V_NIVEL_CODIGO                   ,
         V_NOME_FIRMA                  ,
         V_MORADA_SEDE                 ,
         V_LOCALIDADE                 ,
         V_RAMO_ACTIVIDADE             ,
         V_MATRICULA                 ,
         V_MATRICULA_NUM              ,
         V_CAPITAL_SOCIAL             ,
         V_NIF                         ,
         V_DESIGNACAO_PT_VENDA         ,
         V_MORADA_PT_VENDA             ,
         V_NUM_PROCESSO_ANTERIOR     ,
         V_NUM_PROCESSO_SEGUINTE     ,
         V_OBJECTIVO                 ,
         V_IS_POOC                     ,
          V_DATA_INICIO                 ,
         V_DATA_FIM                      ,
         V_DATA_FIM_EFECTIVO         ,
         V_QUANTIA_FIM                 ,
         V_ANOS_RENOVACAO             ,
         V_IS_CONTRATO_FORMAL         ,
         V_IS_FORN_FRIO_IGLO         ,
         V_PREVISAO_VENDAS_GLOBAL     ,
         V_PRAZO_PAGAMENTO             ,
         V_NIB_CLIENTE                 ,
         V_BANCO                     ,
         V_BANCO_DEPENDENCIA         ,
         V_STATUS                     ,
         V_COMP_CONC_COMPRAS         ,
         V_OBSERVACOES                 ,
         V_OBSERVACOES_MOTIVO         ,
         V_INSERT_USER                 ,
         SYSDATE, --INSERT_DATE,
         'T',       --IS_ACTIVE
         'F'       --IS_IMPORTADO
        );

            V_ID_CONTRATO := ID;

        END IF;
   END;

    PROCEDURE SP_ENTIDADE_INS
    (
        V_ID_CONTRATO                 INT,
        V_COD_LE                   VARCHAR2,
        V_COD_GRUPO_ECONOMICO      VARCHAR2,
        V_COD_SUB_GRUPO_ECONOMICO  VARCHAR2,
        V_COD_CLIENTE              VARCHAR2
    )

    IS

    BEGIN
        INSERT INTO T_ENTIDADE
        (
           ID_CONTRATO                    ,
           COD_LE                           ,
           COD_GRUPO_ECONOMICO            ,
           COD_SUB_GRUPO_ECONOMICO        ,
           COD_CLIENTE                    ,
           INSERT_DATE
        )
        VALUES
        (
            V_ID_CONTRATO                    ,
            V_COD_LE                      ,
            V_COD_GRUPO_ECONOMICO         ,
            V_COD_SUB_GRUPO_ECONOMICO     ,
            V_COD_CLIENTE                 ,
            SYSDATE
        );
    END;


  PROCEDURE SP_MATERIAL_INS
  (
    V_ID_CONTRATO      INT,
    V_COD_MATERIAL      INTEGER,
    V_QUNTIDADE         INTEGER,
    V_VALOR             NUMBER,
    V_OBSERVACOES       VARCHAR2
  )

  IS
  BEGIN


    INSERT INTO T_CONT_MAT_VISIBILIDADE
    (
        ID_CONTRATO        ,
        ID_CONT_MATERIAL,
        COD_MATERIAL    ,
        QUANTIDADE       ,
        VALOR           ,
        OBSERVACOES
    )
    VALUES
    (
        V_ID_CONTRATO                        ,
        SRA.SEQ_ID_CONT_MATERIAL.NEXTVAL  ,
        V_COD_MATERIAL       ,
        V_QUNTIDADE          ,
        V_VALOR              ,
        V_OBSERVACOES
    );

    END;


     PROCEDURE SP_FASEAMENTO_INS
     (
        V_ID_CONTRATO            INT              ,
        V_DATA_PAGAMENTO       DATE         ,
        V_DATA_PAGAMENTO_EFECTUADO       DATE         ,
        V_VALOR_PAGAMENTO      NUMBER       ,
        V_COMP_CONCESSIONARIO  NUMBER      ,
        V_TIPO_PG              VARCHAR2     ,
        V_TTS                  VARCHAR2

     )
     IS
     BEGIN

        INSERT INTO T_FASEAMENTO_PAG
        (
            ID_CONTRATO         ,
            ID_FASEAMENTO        ,
            DATA_PAGAMENTO       ,
            DATA_PAGAMENTO_EFECTUADO       ,
            VALOR_PAGAMENTO      ,
            COMP_CONCESSIONARIO  ,
            TIPO_PG              ,
            TTS
        )
        VALUES
        (
            V_ID_CONTRATO          ,
            SRA.SEQ_ID_FASEAMENTO.NEXTVAL         ,
            V_DATA_PAGAMENTO        ,
            V_DATA_PAGAMENTO_EFECTUADO        ,
            V_VALOR_PAGAMENTO       ,
            V_COMP_CONCESSIONARIO   ,
            V_TIPO_PG               ,
            V_TTS
        );

       END;

  PROCEDURE SP_GAMA_INS
  (
       V_ID_CONTRATO           INT              ,
       V_COD_GRUPO_PRODUTO      VARCHAR2
  )
  IS
  BEGIN

    INSERT INTO T_GAMAS_QUANTIA_FIM
    (
       ID_CONTRATO         ,
       ID_GAMAS_QUANTIA_FIM ,
       COD_GRUPO_PRODUTO
    )
    VALUES
    (
        V_ID_CONTRATO             ,
        SRA.SEQ_ID_QUANTIA_FIM.NEXTVAL      ,
        V_COD_GRUPO_PRODUTO
    );

    END ;


      PROCEDURE SP_GAMA_GLOBAL_INS
  (
       V_ID_CONTRATO           INT              ,
       V_COD_GRUPO_PRODUTO      VARCHAR2
  )
  IS
  BEGIN

    INSERT INTO T_GAMAS_GLOBAL
    (
       ID_CONTRATO         ,
       ID_GAMAS_GLOBAL ,
       COD_GRUPO_PRODUTO
    )
    VALUES
    (
        V_ID_CONTRATO             ,
        SRA.SEQ_ID_QUANTIA_FIM.NEXTVAL      ,
        V_COD_GRUPO_PRODUTO
    );

    END ;



    PROCEDURE SP_EXPLORACAO_INS
      (
           V_ID_CONTRATO           INT              ,
           V_TIPO_EXPLORACAO      VARCHAR2
      )
      IS
      BEGIN

        INSERT INTO T_EXPLORACAO
        (
           ID_CONTRATO         ,
           ID_EXPLORACAO         ,
           TIPO_EXPLORACAO
        )
        VALUES
        (
            V_ID_CONTRATO             ,
            SRA.SEQ_ID_EXPLORACAO.NEXTVAL             ,
            V_TIPO_EXPLORACAO
        );

        END ;


    PROCEDURE SP_EQUIPAMENTO_INS
    (
      V_ID_CONTRATO         INT   ,
      V_COD_EQUIPAMENTO      VARCHAR2   ,
      V_QUANTIDADE            INTEGER
    )
    IS
    V_VALOR INT := NULL;

    BEGIN

    SELECT TE.VALOR INTO V_VALOR FROM T_TIPO_EQUIPAMENTO TE WHERE TE.COD_TIPO_EQUIPAMENTO = V_COD_EQUIPAMENTO;


    INSERT INTO T_CONT_EQUIPAMENTO
    (
       ID_CONTRATO            ,
       ID_CONT_EQUIPAMENTO     ,
       COD_EQUIPAMENTO         ,
       QUANTIDADE               ,
       VALOR
    )
    VALUES
    (
        V_ID_CONTRATO       ,
        SRA.SEQ_ID_CONT_EQUIPAMENTO.NEXTVAL,
        V_COD_EQUIPAMENTO    ,
        V_QUANTIDADE         ,
        V_VALOR
    );

    END ;



  PROCEDURE SP_CONTRAPARTIDA_INS
  (
     V_ID_CONTRATO        IN      INT     ,
     V_ID_CONTRAPARTIDA    IN  OUT INTEGER     ,
     V_COD_GRUPO_PRODUTO   IN      VARCHAR2    ,
     V_PREVISAO_VENDAS     IN      INTEGER     ,
     V_DESCONTO_FACTURA    IN      NUMBER     ,
     V_COMP_CONCESSIONARIO IN      NUMBER,
     V_PRAZO_PAGAMENTO       IN       CHAR
  )
  IS

      ID INT:=NULL;

  BEGIN

    SELECT SRA.SEQ_ID_CONTRAPARTIDA.NEXTVAL INTO ID FROM DUAL;

    INSERT INTO T_CONTRAPARTIDAS
    (
       ID_CONTRATO              ,
       ID_CONTRAPARTIDA          ,
       COD_GRUPO_PRODUTO         ,
       PREVISAO_VENDAS           ,
       DESCONTO_FACTURA          ,
       COMP_CONCESSIONARIO         ,
       PRAZO_PAGAMENTO
    )
    VALUES
    (
        V_ID_CONTRATO           ,
        ID                         ,
        V_COD_GRUPO_PRODUTO      ,
        V_PREVISAO_VENDAS        ,
        V_DESCONTO_FACTURA       ,
        V_COMP_CONCESSIONARIO     ,
        V_PRAZO_PAGAMENTO
    );

    V_ID_CONTRAPARTIDA := ID;

    END ;

  PROCEDURE SP_BONUS_INS
  (
    V_ID_CONTRAPARTIDA     INTEGER ,
    V_VALOR_LIMITE_MIN     INTEGER ,
    V_DESCONTO             NUMBER ,
    V_COMP_CONCESSIONARIO  NUMBER,
    V_TIPO                 IN  CHAR
  )
  IS

  BEGIN

    INSERT INTO T_BONUS
    (
        ID_CONTRAPARTIDA        ,
        ID_BONUS                ,
        VALOR_LIMITE_MIN        ,
        DESCONTO                ,
        COMP_CONCESSIONARIO        ,
        TIPO

    )
    VALUES
    (
        V_ID_CONTRAPARTIDA       ,
        SRA.SEQ_ID_BONUS.NEXTVAL ,
        V_VALOR_LIMITE_MIN       ,
        V_DESCONTO               ,
        V_COMP_CONCESSIONARIO     ,
        V_TIPO        

    );


    END ;


    PROCEDURE SP_BONUS_GLOBAL_INS
  (
    V_ID_CONTRATO          INTEGER ,
    V_VALOR_LIMITE_MIN     INTEGER ,
    V_DESCONTO             NUMBER ,
    V_COMP_CONCESSIONARIO  NUMBER,
    V_TIPO                 IN  CHAR
  )
  IS

  BEGIN

    INSERT INTO T_BONUS_GLOBAL
    (
        ID_CONTRATO                ,
        ID_BONUS                ,
        VALOR_LIMITE_MIN        ,
        DESCONTO                ,
        COMP_CONCESSIONARIO        ,
        TIPO

    )
    VALUES
    (
        V_ID_CONTRATO            ,
        SRA.SEQ_ID_BONUS.NEXTVAL ,
        V_VALOR_LIMITE_MIN       ,
        V_DESCONTO               ,
        V_COMP_CONCESSIONARIO     ,
        V_TIPO                   

    );


    END ;



    PROCEDURE SP_CONTRATOS_GET (WHERESTM VARCHAR2, CUR OUT SYS_REFCURSOR )
    IS

    sqlstm VARCHAR2(2000);

    BEGIN -- executable part starts here

        sqlstm := 'SELECT ID_CONTRATO,';
        sqlstm := sqlstm || ' ID_PROCESSO,';
        sqlstm := sqlstm || ' ID_SEQUENCIA,';
        sqlstm := sqlstm || ' TIPO_CONTRATO,';
        sqlstm := sqlstm || ' CLASSIFICACAO_CONTRATO,';
        sqlstm := sqlstm || ' TO_CHAR(DATA_CONTRATO,''DD-MM-YYYY'') AS DATA_CONTRATO,';
        sqlstm := sqlstm || ' NIVEL_CONTRATO,';
        sqlstm := sqlstm || ' NIVEL_CODIGO,';
        sqlstm := sqlstm || ' NOME_FIRMA,';
        sqlstm := sqlstm || ' MORADA_SEDE,';
        sqlstm := sqlstm || ' LOCALIDADE,';
        sqlstm := sqlstm || ' RAMO_ACTIVIDADE,';
        sqlstm := sqlstm || ' MATRICULA,';
        sqlstm := sqlstm || ' MATRICULA_NUM,';
        sqlstm := sqlstm || ' CAPITAL_SOCIAL,';
        sqlstm := sqlstm || ' NIF,';
        sqlstm := sqlstm || ' DESIGNACAO_PT_VENDA,';
        sqlstm := sqlstm || ' MORADA_PT_VENDA,';
        sqlstm := sqlstm || ' NUM_PROCESSO_ANTERIOR,';
        sqlstm := sqlstm || ' NUM_PROCESSO_SEGUINTE,';
        sqlstm := sqlstm || ' OBJECTIVO,';
        sqlstm := sqlstm || ' IS_POOC,';
        sqlstm := sqlstm || ' TO_CHAR(DATA_INICIO,''DD-MM-YYYY'') AS DATA_INICIO,';
        sqlstm := sqlstm || ' TO_CHAR(DATA_FIM,''DD-MM-YYYY'') AS DATA_FIM, ';
        sqlstm := sqlstm || ' TO_CHAR(DATA_FIM_EFECTIVO,''DD-MM-YYYY'') as DATA_FIM_EFECTIVO,';
        sqlstm := sqlstm || ' QUANTIA_FIM,';
        sqlstm := sqlstm || ' ANOS_RENOVACAO,';
        sqlstm := sqlstm || ' IS_CONTRATO_FORMAL,';
        sqlstm := sqlstm || ' IS_FORN_FRIO_IGLO,';
        sqlstm := sqlstm || ' PREVISAO_VENDAS_GLOBAL,';
        sqlstm := sqlstm || ' PRAZO_PAGAMENTO,';
        sqlstm := sqlstm || ' NIB_CLIENTE,';
        sqlstm := sqlstm || ' BANCO,';
        sqlstm := sqlstm || ' BANCO_DEPENDENCIA,';
        sqlstm := sqlstm || ' STATUS,';
        sqlstm := sqlstm || ' COMP_CONC_COMPRAS,';
        sqlstm := sqlstm || ' OBSERVACOES,';
        sqlstm := sqlstm || ' OBSERVACOES_MOTIVO';
        sqlstm := sqlstm || ' FROM T_CONTRATO WHERE ';
        sqlstm := sqlstm || WHERESTM;


        OPEN CUR FOR sqlstm;

        COMMIT;

    END;


    PROCEDURE SP_CONTRATO_GET
    (
        V_ID_CONTRATO                                          INT,
        CUR_CONTRATO                   OUT SYS_REFCURSOR,
        CUR_ENTIDADE                   OUT SYS_REFCURSOR,
        CUR_EXPLORACAO                 OUT SYS_REFCURSOR,
        CUR_BONUS                      OUT SYS_REFCURSOR,
        CUR_BONUS_GLOBAL               OUT SYS_REFCURSOR,
        CUR_CONT_EQUIPAMENTO           OUT SYS_REFCURSOR,
        CUR_CONT_MAT_VISIBILIDADE      OUT SYS_REFCURSOR,
        CUR_CONTRAPARTIDAS             OUT SYS_REFCURSOR,
        CUR_FASEAMENTO_PAG             OUT SYS_REFCURSOR,
        CUR_GAMAS_QUANTIA_FIM          OUT SYS_REFCURSOR,
        CUR_GAMAS_GLOBAL               OUT SYS_REFCURSOR
    )
    IS

        V_ID_PROCESSSO VARCHAR2(20);
        V_ID_SEQUENCIA INT;

    BEGIN -- executable part starts here


        --SELECT ID_PROCESSO, ID_SEQUENCIA INTO  V_ID_PROCESSSO, V_ID_SEQUENCIA FROM T_CONTRATO WHERE ID_CONTRATO = V_ID_CONTRATO;

        OPEN CUR_CONTRATO            FOR  SELECT * FROM T_CONTRATO                                                                     WHERE ID_CONTRATO = V_ID_CONTRATO;
        OPEN CUR_ENTIDADE            FOR  SELECT LE.* FROM T_ENTIDADE E, MV_LOCAIS_ENTREGA LE                                         WHERE ID_CONTRATO = V_ID_CONTRATO AND E.COD_LE = LE.COD_LOCAL_ENTREGA     ;
        OPEN CUR_EXPLORACAO          FOR  SELECT * FROM T_EXPLORACAO                                                                   WHERE ID_CONTRATO = V_ID_CONTRATO;
        OPEN CUR_BONUS                     FOR  SELECT B.*     FROM T_BONUS B,  T_CONTRAPARTIDAS C                                         WHERE ID_CONTRATO = V_ID_CONTRATO AND B.ID_CONTRAPARTIDA = C.ID_CONTRAPARTIDA ;
        OPEN CUR_BONUS_GLOBAL            FOR  SELECT B.*     FROM T_BONUS_GLOBAL B                                                       WHERE ID_CONTRATO = V_ID_CONTRATO;
        OPEN CUR_CONT_EQUIPAMENTO        FOR  SELECT T.COD_TIPO_EQUIPAMENTO, T.DES_TIPO_EQUIPAMENTO, C.QUANTIDADE, C.VALOR FROM T_CONT_EQUIPAMENTO C, T_TIPO_EQUIPAMENTO T                                  WHERE ID_CONTRATO = V_ID_CONTRATO AND C.COD_EQUIPAMENTO = T.COD_TIPO_EQUIPAMENTO     ;
        OPEN CUR_CONT_MAT_VISIBILIDADE   FOR  SELECT C.COD_MATERIAL, T.DES_MATERIAL_POS, C.QUANTIDADE, C.VALOR, C.OBSERVACOES FROM T_CONT_MAT_VISIBILIDADE C, TO_MATERIAL_POS T                                 WHERE ID_CONTRATO = V_ID_CONTRATO AND C.COD_MATERIAL = T.COD_MATERIAL_POS     ;
        OPEN CUR_CONTRAPARTIDAS          FOR  SELECT * FROM T_CONTRAPARTIDAS C, T_GRUPO_PRODUTO G     WHERE ID_CONTRATO = V_ID_CONTRATO AND C.COD_GRUPO_PRODUTO = G.COD_GRUPO_PRODUTO ORDER BY C.COD_GRUPO_PRODUTO ASC;
        OPEN CUR_FASEAMENTO_PAG          FOR  SELECT * FROM T_FASEAMENTO_PAG                                                            WHERE ID_CONTRATO = V_ID_CONTRATO;
        OPEN CUR_GAMAS_QUANTIA_FIM       FOR  SELECT * FROM T_GAMAS_QUANTIA_FIM                                                         WHERE ID_CONTRATO = V_ID_CONTRATO;
        OPEN CUR_GAMAS_GLOBAL            FOR  SELECT * FROM T_GAMAS_GLOBAL                                                                 WHERE ID_CONTRATO = V_ID_CONTRATO;

        COMMIT;


    END;

    PROCEDURE SP_CONTRATO_STATUS_UPD
    (
         V_ID_CONTRATO INT,
        V_STATUS      CHAR,
        V_MOTIVO      VARCHAR2,
        V_DATA_FIM_EFECTIVO DATE,
        V_NUM_PROCESSO_SEGUINTE VARCHAR2,
        V_ALTER_USER  VARCHAR2
    )IS
    BEGIN
        UPDATE T_CONTRATO T SET T.STATUS = V_STATUS, T.ALTER_DATE = SYSDATE, T.ALTER_USER = V_ALTER_USER, T.NUM_PROCESSO_SEGUINTE = V_NUM_PROCESSO_SEGUINTE, T.IS_IMPORTADO = 'F' WHERE T.ID_CONTRATO = V_ID_CONTRATO;
        IF V_STATUS = 'T' THEN
               UPDATE T_CONTRATO T SET T.DATA_FIM_EFECTIVO = V_DATA_FIM_EFECTIVO WHERE T.ID_CONTRATO = V_ID_CONTRATO;
        END IF;
        IF V_STATUS = 'I' THEN
               UPDATE T_CONTRATO T SET T.DATA_FIM_EFECTIVO = NULL, IS_IMPORTADO='F' WHERE T.ID_CONTRATO = V_ID_CONTRATO;
        END IF;
        IF V_MOTIVO is not null THEN
               UPDATE T_CONTRATO T SET T.OBSERVACOES_MOTIVO = V_MOTIVO WHERE T.ID_CONTRATO = V_ID_CONTRATO;
        END IF; 
     COMMIT;
    END;

    PROCEDURE SP_CONTRATO_DEL
    (
         V_ID_CONTRATO INT
    )IS
    BEGIN
         -- copiar o contrato para a tabela de apagados
         INSERT INTO T_CONTRATO_APAGADO
         SELECT *
         FROM T_CONTRATO
         WHERE ID_CONTRATO =V_ID_CONTRATO ;

        -- apagar os bonus
        DELETE FROM T_BONUS
        WHERE ID_BONUS
        IN
        (
         SELECT b.ID_BONUS FROM T_BONUS b, T_CONTRAPARTIDAS c
         WHERE b.ID_CONTRAPARTIDA = c.ID_CONTRAPARTIDA AND c.ID_CONTRATO = V_ID_CONTRATO
        );

        -- apagar as contrapartidas
        DELETE FROM T_CONTRAPARTIDAS  WHERE ID_CONTRATO = V_ID_CONTRATO ;
        -- apagar as entidades
        DELETE FROM T_ENTIDADE  WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar a exploracao
        DELETE FROM T_EXPLORACAO WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar a bonus global
        DELETE FROM T_BONUS_GLOBAL  WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar cont_equipamento
        DELETE FROM T_CONT_EQUIPAMENTO  WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar cont_mat_visibilidade
        DELETE FROM T_CONT_MAT_VISIBILIDADE WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar faseamento_pagamento
        DELETE FROM T_FASEAMENTO_PAG WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar o contrato
        DELETE FROM T_CONTRATO WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar a gamas_global
        DELETE FROM T_GAMAS_GLOBAL WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar a gamas_quantia_fim
        DELETE FROM T_GAMAS_QUANTIA_FIM WHERE ID_CONTRATO =V_ID_CONTRATO ;
        -- apagar o contrato
        DELETE FROM T_CONTRATO WHERE ID_CONTRATO =V_ID_CONTRATO ;
    END;

    PROCEDURE SSIS_LOAD_TMP_FACT_SRA_PL
    IS
    BEGIN
      INSERT INTO SSIS_TMP_FACT_SRA_PL
        (
          SELECT   b.cod_tts, a.ID_CONTRATO cod_sra_contrato,
                    c.cod_local_entrega cod_local_entrega, f.cod_mes cod_mes,
                    SUM (c.val_desconto_parcial) val_descontos
               FROM SSIS_TMP_CONTRATO a,
                    cindy_staging.etl_map_tts b,
                    T_ENTIDADE t,
                    cindy_dw.lk_grupo_tts gtts,
                    cindy_dw.lk_tts tts,
                    cindy_dw.fact_promocoes c,
                    cindy_dw.lk_dia f
              WHERE c.cod_local_entrega = t.cod_le
                AND t.id_contrato = a.ID_CONTRATO
                AND a.is_terminated = 'F'
                AND b.COD_TTS=tts.COD_TTS
                AND tts.COD_GRUPO_TTS=gtts.cod_GRUPO_TTS
                AND gtts.COD_SRA_TTS='0001'
                AND f.cod_dia = c.cod_dia
                AND c.cod_tipo_desconto = b.cod_tipo_desconto
                AND c.cod_dia >= a.data_inicio
                AND cod_sra_ambito_contrato = 'SRA'
                AND (c.cod_dia <= a.data_fim_efectivo
                     OR a.data_fim_efectivo IS NULL
                    )
           GROUP BY b.cod_tts, a.ID_CONTRATO, cod_local_entrega, cod_mes);

       -- carregar os sra  e CN do ficheiro excel
        INSERT INTO SSIS_TMP_FACT_SRA_PL
          (SELECT  DISTINCT  c.cod_tts,
          a.ID_CONTRATO cod_sra_contrato,
            c.cod_local_entrega cod_local_entrega,
            f.cod_mes cod_mes,
            SUM (c.val_descontos) val_descontos
               FROM SSIS_TMP_CONTRATO a,
                       T_ENTIDADE t,
                    cindy_dw.fact_sra_pl_dia c,
                    cindy_dw.lk_dia f
              WHERE
              c.cod_local_entrega = t.cod_le
                AND t.id_contrato = a.ID_CONTRATO
                AND c.cod_sra_contrato = a.ID_CONTRATO
                AND a.is_terminated = 'F'
                AND f.cod_dia = c.cod_dia
                AND c.cod_dia >= a.data_inicio
                AND (a.cod_sra_ambito_contrato = 'SRA' OR a.cod_sra_ambito_contrato = 'CN')
                AND (c.cod_dia <= a.data_fim_efectivo
                     OR a.data_fim_efectivo IS NULL
                    )
           GROUP BY c.cod_tts, a.ID_CONTRATO, cod_local_entrega, cod_mes);

    /* Alterar Para carregar os N�o SRA (do Cindy REAL)*/
       INSERT INTO SSIS_TMP_FACT_SRA_PL
          (SELECT   b.cod_tts, a.ID_CONTRATO cod_sra_contrato,
                    c.cod_local_entrega cod_local_entrega, f.cod_mes cod_mes,
                    SUM (c.val_desconto_parcial) val_descontos
               FROM SSIS_TMP_CONTRATO a,
                    cindy_staging.etl_map_tts b,
                    cindy_dw.lk_grupo_tts gtts,
                    cindy_dw.lk_tts tts,
                    cindy_dw.fact_promocoes c,
                    T_ENTIDADE t,
                    cindy_dw.lk_dia f
              WHERE c.cod_local_entrega = t.cod_le
                AND t.id_contrato = a.ID_CONTRATO
                AND a.is_terminated = 'F'
                AND b.COD_TTS=tts.COD_TTS
                AND tts.COD_GRUPO_TTS=gtts.cod_GRUPO_TTS
                AND gtts.COD_SRA_TTS='0002'
                AND f.cod_dia = c.cod_dia
                AND c.cod_tipo_desconto = b.cod_tipo_desconto
                AND c.cod_dia >= a.data_inicio
                AND cod_sra_ambito_contrato = 'CN'
                AND (c.cod_dia <= a.data_fim_efectivo
                     OR a.data_fim_efectivo IS NULL
                    )
           GROUP BY b.cod_tts, a.ID_CONTRATO, cod_local_entrega, cod_mes);

       INSERT INTO SSIS_TMP_FACT_SRA_PL
          (SELECT   '0026', a.ID_CONTRATO cod_sra_contrato,
                    c.cod_local_entrega cod_local_entrega, f.cod_mes cod_mes,
                    SUM (c.val_desconto_parcial) val_descontos
               FROM SSIS_TMP_CONTRATO a,
                    cindy_staging.etl_map_tts b,
                    cindy_dw.lk_grupo_tts gtts,
                    cindy_dw.lk_tts tts,
                    cindy_dw.fact_promocoes c,
                    T_ENTIDADE t,
                    cindy_dw.lk_dia f
              WHERE c.cod_local_entrega = t.cod_le
                AND t.id_contrato = a.ID_CONTRATO
                AND a.is_terminated = 'F'
                AND b.COD_TTS=tts.COD_TTS
                AND tts.COD_GRUPO_TTS=gtts.cod_GRUPO_TTS
                AND gtts.COD_SRA_TTS='0002'
                AND f.cod_dia = c.cod_dia
                AND c.cod_tipo_desconto = b.cod_tipo_desconto
                AND c.cod_dia >= a.data_inicio
                AND cod_sra_ambito_contrato = 'CN'
                AND (c.cod_dia <= a.data_fim_efectivo
                     OR a.data_fim_efectivo IS NULL
                    )
                AND c.cod_tipo_desconto NOT IN (SELECT b.cod_tipo_desconto
                                                  FROM cindy_staging.etl_map_tts)
           GROUP BY a.ID_CONTRATO, cod_local_entrega, cod_mes);

       COMMIT;
    END;


    PROCEDURE SSIS_LOAD_TMP_SRA_PROMOCOES
    IS
    BEGIN
       /* Carrega tabela que cont�m as vendas ao abrigo de cada contrato para cada LE */
       INSERT INTO SSIS_TMP_SRA_PROMOCOES
          (
          SELECT
            CONT.id_contrato ID_CONTRATO,
            DIA.cod_mes,
            FACT.cod_local_entrega LOCAL_ENTREGA,
            GAMA.cod_grupo_produto,
            SUM(NVL(FACT.val_desconto_PARCIAL,0)) VAL_DESCONTOS
        FROM
            SSIS_TMP_CONTRATO_RENT CONT
            INNER JOIN T_ENTIDADE ENT
                 ON (ENT.id_contrato = CONT.id_contrato)
            INNER JOIN v_bo_gamas_quantia_fim    GAMA
                ON (GAMA.id_contrato = CONT.id_contrato)
            INNER JOIN cindy_dw.lk_produto       PROD
                 ON (PROD.cod_grupo_produto = GAMA.cod_grupo_produto)
            INNER JOIN cindy_dw.fact_PROMOCOES   FACT
                 ON (FACT.cod_produto = PROD.cod_produto)
                 AND ( FACT.cod_local_entrega = ENT.cod_le )
                 AND ( FACT.cod_dia > CONT.data_inicio)
            INNER JOIN cindy_dw.lk_dia        DIA
                 ON (DIA.cod_dia = FACT.cod_dia)
         WHERE
             CONT.is_terminated = 'F' AND
            (FACT.COD_TIPO_DESCONTO = '190' OR
             FACT.COD_TIPO_DESCONTO = '290'
            )
        GROUP BY CONT.id_contrato,
                 DIA.cod_mes,
                 FACT.cod_local_entrega,
                 GAMA.cod_grupo_produto

        );
        COMMIT;
    END;

    PROCEDURE SSIS_LOAD_TMP_SRA_VENDAS
    IS
    BEGIN
        /* Carrega tabela que cont�m as vendas ao abrigo de cada contrato para cada LE */
       INSERT INTO CINDY_STAGING.SSIS_TMP_SRA_VENDAS
          (
          SELECT
            CONT.id_contrato ID_CONTRATO
            ,DIA.cod_mes
            ,FACT.cod_local_entrega LOCAL_ENTREGA
            ,GAMA.cod_grupo_produto
            ,SUM(NVL(FACT.VAL_VENDAS,0)) VAL_VENDAS
            ,SUM(NVL(FACT.VAL_DEVOLUCOES,0)) VAL_DEVOLUCOES
            ,SUM(NVL(FACT.VAL_DESCONTOS,0)) VAL_DESCONTOS
            ,SUM(NVL(FACT.LITROS_VENDIDOS,0)) LITROS_VENDIDOS
        FROM
            SSIS_TMP_CONTRATO_RENT CONT
            INNER JOIN T_ENTIDADE ENT
                 ON (ENT.id_contrato = CONT.id_contrato)
            INNER JOIN v_bo_gamas_quantia_fim    GAMA
                ON (GAMA.id_contrato = CONT.id_contrato)
            INNER JOIN cindy_dw.lk_produto       PROD
                 ON (PROD.cod_grupo_produto = GAMA.cod_grupo_produto)
            INNER JOIN cindy_dw.fact_VENDAS   FACT
                 ON (FACT.cod_produto = PROD.cod_produto)
                 AND ( FACT.cod_local_entrega = ENT.cod_le )
                 AND ( FACT.cod_dia > CONT.data_inicio)
            INNER JOIN cindy_dw.lk_dia        DIA
                 ON (DIA.cod_dia = FACT.cod_dia)
         WHERE
             CONT.is_terminated = 'F'
        GROUP BY CONT.id_contrato,
                 DIA.cod_mes,
                 FACT.cod_local_entrega,
                 GAMA.cod_grupo_produto
                 );

       COMMIT;
    END;

    PROCEDURE SSIS_LOAD_TMP_SRA_VENDAS_T
    IS
    BEGIN
       /* Carrega tabela que cont�m as vendas ao abrigo de cada contrato para cada LE, n�o   considerando apenas as gamas que terminam contrato, mas todas as envolvidas nas   contrapartidas */
       INSERT INTO CINDY_STAGING.SSIS_TMP_SRA_VENDAS_TOTAIS
          (
          SELECT
            CONT.id_contrato ID_CONTRATO
            ,DIA.cod_mes
            ,FACT.cod_local_entrega LOCAL_ENTREGA
            ,GRUPO.cod_grupo_produto
            ,SUM(NVL(FACT.VAL_VENDAS,0)) VAL_VENDAS
            ,SUM(NVL(FACT.VAL_DEVOLUCOES,0)) VAL_DEVOLUCOES
            ,SUM(NVL(FACT.VAL_DESCONTOS,0)) VAL_DESCONTOS
            ,SUM(NVL(FACT.LITROS_VENDIDOS,0)) LITROS_VENDIDOS
        FROM
            SSIS_TMP_CONTRATO_RENT CONT
            INNER JOIN T_ENTIDADE ENT
                 ON (ENT.id_contrato = CONT.id_contrato)
            INNER JOIN T_CONTRAPARTIDAS    GRUPO
                ON (GRUPO.id_contrato = CONT.id_contrato)
            INNER JOIN cindy_dw.lk_produto       PROD
                 ON (PROD.cod_grupo_produto = GRUPO.cod_grupo_produto)
            INNER JOIN cindy_dw.fact_VENDAS   FACT
                 ON (FACT.cod_produto = PROD.cod_produto)
                 AND ( FACT.cod_local_entrega = ENT.cod_le )
                 AND ( FACT.cod_dia > CONT.data_inicio)
            INNER JOIN cindy_dw.lk_dia        DIA
                 ON (DIA.cod_dia = FACT.cod_dia)
         WHERE
             CONT.is_terminated = 'F'
        GROUP BY CONT.id_contrato,
                 DIA.cod_mes,
                 FACT.cod_local_entrega,
                 GRUPO.cod_grupo_produto
                 );

       COMMIT;
    END;

END;
/
COMMIT;
