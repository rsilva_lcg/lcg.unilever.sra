--  ################################################################## 
--  #  ESTE FICHEIRO DESTINA-SE A TRANSFORMAR O MODELO DE DADOS      # 
--  #  DA VERS�O 2.0 PARA A VERS�O 2.1  ADICIONANDO AS AE            # 
--  #                                                                # 
--  #  O SCRIPT AQUI DESENVOLVIDO DEVE SER EXECUTADO                 # 
--  #  SEMPRE QUE FOR NECESS�RIO ACTUALIZAR A BASE DE DADOS          # 
--  #  TENDO EM CONTA QUE SER�O PERDIDOS TODOS OS DADOS.             # 
--  #  PARA EXECUTAR PRIMIR (F5)                                     # 
--  #                                                                # 
--  ################################################################## 


-- CRIAR NOVAS TABELAS DO MODELO OPERACIONAL
-- ###################################################################################################### 

-- TABELA DE EMISSORES
-- ###################################################################
DROP TABLE SRA.LK_EMISSOR CASCADE CONSTRAINTS;
CREATE TABLE SRA.LK_EMISSOR
(
  ID_EMISSOR               VARCHAR2(2 BYTE)     NOT NULL,
  NOME_EMISSOR             VARCHAR2(50 BYTE)
)

TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_LK_EMISSOR ON SRA.LK_EMISSOR
(ID_EMISSOR)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.LK_EMISSOR ADD (
  CONSTRAINT PK_LK_EMISSOR
 PRIMARY KEY
 (ID_EMISSOR)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);


-- TABELA DE CUSTOMER SERVICES
-- ###################################################################
DROP TABLE SRA.LK_CUSTOMER_SERVICE CASCADE CONSTRAINTS;
CREATE TABLE SRA.LK_CUSTOMER_SERVICE
(
  ID_CUSTOMER_SERVICE               VARCHAR2(3 BYTE)     NOT NULL,
  NOME_CUSTOMER_SERVICE             VARCHAR2(50 BYTE)
)

TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_LK_CUSTOMER_SERVICE ON SRA.LK_CUSTOMER_SERVICE
(ID_CUSTOMER_SERVICE)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.LK_CUSTOMER_SERVICE ADD (
  CONSTRAINT PK_LK_CUSTOMER_SERVICE
 PRIMARY KEY
 (ID_CUSTOMER_SERVICE)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);



-- TABELA DE TIPO DE ACTIVIDADE
-- ###################################################################
DROP TABLE SRA.LK_TIPO_ACTIVIDADE CASCADE CONSTRAINTS;
CREATE TABLE SRA.LK_TIPO_ACTIVIDADE
(
  ID_TIPO_ACTIVIDADE               INTEGER              NOT NULL,
  NOME_TIPO_ACTIVIDADE             VARCHAR2(50 BYTE),
  HAS_ACORDO                       CHAR(1 BYTE)         NOT NULL,
  HAS_VALOR                        CHAR(1 BYTE)         NOT NULL
)

TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_LK_TIPO_ACTIVIDADE ON SRA.LK_TIPO_ACTIVIDADE
(ID_TIPO_ACTIVIDADE)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.LK_TIPO_ACTIVIDADE ADD (
  CONSTRAINT PK_LK_TIPO_ACTIVIDADE
 PRIMARY KEY
 (ID_TIPO_ACTIVIDADE)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);


-- TABELA DE CONTRATOS
-- ###################################################################
DROP TABLE SRA.T_AE_CONTRATO CASCADE CONSTRAINTS;
CREATE TABLE SRA.T_AE_CONTRATO
(
  ID_CONTRATO              INTEGER              NOT NULL,
  ID_NUM_PROPOSTA          VARCHAR2(12 BYTE),
  ID_EMISSOR               VARCHAR2(2 BYTE),
  ID_ANO                   VARCHAR2(4 BYTE),
  ID_NUM_SERIE             VARCHAR2(2 BYTE),
  ID_NUMERO                VARCHAR2(4 BYTE),
  DATA_RECEPCAO            DATE,
  ID_CUSTOMER_SERVICE      VARCHAR2(3 BYTE),
  NIVEL_CONTRATO		   VARCHAR2(3 BYTE),
  GRUPO_ECONOMICO_COD      VARCHAR2(3 BYTE),
  SUBGRUPO_ECONOMICO_COD   VARCHAR2(3 BYTE),
  NOME_FIRMA               VARCHAR2(50 BYTE),
  MORADA_SEDE              VARCHAR2(255 BYTE),
  LOCALIDADE               VARCHAR2(50 BYTE),
  NIF                      VARCHAR2(20 BYTE),
  OBSERVACOES              VARCHAR2(4000 BYTE),
  INSERT_DATE              DATE     DEFAULT SYSDATE,
  INSERT_USER              VARCHAR2(20 BYTE),
  ALTER_DATE               DATE,
  ALTER_USER               VARCHAR2(20 BYTE)
)

TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_T_AE_CONTRATO ON SRA.T_AE_CONTRATO
(ID_CONTRATO)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.T_AE_CONTRATO ADD (
  CONSTRAINT PK_T_AE_CONTRATO
 PRIMARY KEY
 (ID_CONTRATO)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);

DROP SEQUENCE SRA.SEQ_AE_ID_CONTRATO;
CREATE SEQUENCE SRA.SEQ_AE_ID_CONTRATO
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


-- TABELA DE LOCAIS DE ENTREGA
-- ###################################################################
DROP TABLE SRA.T_AE_ENTIDADE CASCADE CONSTRAINTS;
CREATE TABLE SRA.T_AE_ENTIDADE
(
  ID_CONTRATO              INTEGER,
  COD_LE                   VARCHAR2(15 BYTE),
  COD_GRUPO_ECONOMICO      VARCHAR2(3 BYTE),
  COD_SUB_GRUPO_ECONOMICO  VARCHAR2(3 BYTE),
  COD_CLIENTE              VARCHAR2(9 BYTE),
  INSERT_DATE              DATE                 DEFAULT SYSDATE
)
TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_T_AE_ENTIDADE ON SRA.T_AE_ENTIDADE
(ID_CONTRATO, COD_LE)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.T_AE_ENTIDADE ADD (
  CONSTRAINT PK_T_AE_ENTIDADE
 PRIMARY KEY
 (ID_CONTRATO, COD_LE)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);


-- TABELA DE ABASTECIMENTOS
-- ###################################################################
DROP TABLE SRA.T_AE_ABASTECIMENTO CASCADE CONSTRAINTS;
CREATE TABLE SRA.T_AE_ABASTECIMENTO
(
  ID_CONTRATO              INTEGER,
  COD_CONCESSIONARIO       VARCHAR2(3 BYTE),
  FACTURACAO		       CHAR(1)          NOT NULL,
  DISTRIBUICAO		       CHAR(1)          NOT NULL,
  OBSERVACOES              VARCHAR2(255 BYTE)
)
TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_T_AE_ABASTECIMENTO ON SRA.T_AE_ABASTECIMENTO
(ID_CONTRATO, COD_CONCESSIONARIO)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.T_AE_ABASTECIMENTO ADD (
  CONSTRAINT PK_T_AE_ABASTECIMENTO
 PRIMARY KEY
 (ID_CONTRATO, COD_CONCESSIONARIO)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);


-- TABELA DE ACTIVIDADES
-- ###################################################################
DROP TABLE SRA.T_AE_ACTIVIDADE CASCADE CONSTRAINTS;
CREATE TABLE SRA.T_AE_ACTIVIDADE
(
  ID_ACTIVIDADE             INTEGER          NOT NULL,
  ID_CONTRATO               INTEGER          NOT NULL,  
  ID_TIPO_ACTIVIDADE        INTEGER          NOT NULL,
  NUM_ACORDO                VARCHAR2(20 BYTE),
  VALOR_ACORDO              NUMBER(20,9)
)

TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_T_AE_ACTIVIDADE ON SRA.T_AE_ACTIVIDADE
(ID_ACTIVIDADE)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.T_AE_ACTIVIDADE ADD (
  CONSTRAINT PK_T_AE_ACTIVIDADE
 PRIMARY KEY
 (ID_ACTIVIDADE)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);

DROP SEQUENCE SRA.SEQ_AE_ID_ACTIVIDADE;
CREATE SEQUENCE SRA.SEQ_AE_ID_ACTIVIDADE
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


-- TABELA DE PRODUTOS  (TPR)
-- ###################################################################
DROP TABLE SRA.T_AE_TPR CASCADE CONSTRAINTS;
CREATE TABLE SRA.T_AE_TPR
(
  ID_TPR                    INTEGER            NOT NULL,
  ID_CONTRATO               INTEGER            NOT NULL,
  COD_PRODUTO               VARCHAR2(9 BYTE)   NOT NULL,
  BUDGET_VENDAS             NUMBER(11,3)        NOT NULL,
  BUDGET_MARKETING          NUMBER(11,3)        NOT NULL,
  ACTIVIDADE_INICIO         DATE               NOT NULL,
  ACTIVIDADE_FIM            DATE               NOT NULL,
  COMPRAS_INICIO            DATE               NOT NULL,
  COMPRAS_FIM               DATE               NOT NULL,
  PVP                       NUMBER(20,9)       NOT NULL,
  UNIDADE_BASE				VARCHAR2(3 BYTE)   NOT NULL,
  PREVISAO_QUANTIDADE        NUMBER(20,9)       NOT NULL
)

TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_T_AE_TPR ON SRA.T_AE_TPR
(ID_TPR)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.T_AE_TPR ADD (
  CONSTRAINT PK_T_AE_TPR
 PRIMARY KEY
 (ID_TPR)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);

DROP SEQUENCE SRA.SEQ_AE_ID_TPR;
CREATE SEQUENCE SRA.SEQ_AE_ID_TPR
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


-- TABELA DE DEBITOS
-- ###################################################################
DROP TABLE SRA.T_AE_DEBITO CASCADE CONSTRAINTS;
CREATE TABLE SRA.T_AE_DEBITO
(
  ID_DEBITO                 INTEGER            NOT NULL,
  ID_CONTRATO               INTEGER            NOT NULL,
  DATA_DEBITO               DATE               NOT NULL,
  VALOR_DEBITO              NUMBER(20,9)       NOT NULL
)

TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_T_AE_DEBITO ON SRA.T_AE_DEBITO
(ID_DEBITO)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.T_AE_DEBITO ADD (
  CONSTRAINT PK_T_AE_DEBITO
 PRIMARY KEY
 (ID_DEBITO)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);

DROP SEQUENCE SRA.SEQ_AE_ID_DEBITO;
CREATE SEQUENCE SRA.SEQ_AE_ID_DEBITO
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


-- TABELA DE LOGS
-- ###################################################################
DROP TABLE SRA.T_AE_LOG CASCADE CONSTRAINTS;
CREATE TABLE SRA.T_AE_LOG
(
  ID_LOG                INTEGER             NOT NULL,
  ID_CONTRATO           INTEGER             NOT NULL,
  LOG_DATA              DATE                NOT NULL,
  LOG_USER              VARCHAR2(50 BYTE)   NOT NULL,
  MESSAGE               VARCHAR2(200 BYTE)  NOT NULL
)

TABLESPACE SRA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX PK_T_AE_LOG ON SRA.T_AE_LOG
(ID_LOG)
LOGGING
TABLESPACE SRA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SRA.T_AE_LOG ADD (
  CONSTRAINT PK_T_AE_LOG
 PRIMARY KEY
 (ID_LOG)
    USING INDEX 
    TABLESPACE SRA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               )
);

DROP SEQUENCE SRA.SEQ_AE_ID_LOG;
CREATE SEQUENCE SRA.SEQ_AE_ID_LOG
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;





-- ######################################################################################################
-- #  CRIAR NOVAS TABELAS DO MODELO ANALITICO                                                           #
-- ###################################################################################################### 


-- TABELA DE LOCKUP DE CONTRATOS
-- ###################################################################
DROP TABLE CINDY_DW.LK_AE_CONTRATO CASCADE CONSTRAINTS;
CREATE TABLE CINDY_DW.LK_AE_CONTRATO
(
  ID_CONTRATO              INTEGER              NOT NULL,
  ID_NUM_PROPOSTA          VARCHAR2(12 BYTE),
  ID_ANO                   VARCHAR2(4 BYTE),
  ID_NUM_SERIE             VARCHAR2(2 BYTE),
  ID_NUMERO                VARCHAR2(4 BYTE),
  ID_EMISSOR_COD           VARCHAR2(2 BYTE),
  ID_EMISSOR_NOME          VARCHAR2(50 BYTE),
  DATA_RECEPCAO            DATE,
  CUSTOMER_SERVICE_COD     VARCHAR2(3 BYTE),
  CUSTOMER_SERVICE_NOME    VARCHAR2(50 BYTE),
  NIVEL_CONTRATO		   VARCHAR2(3 BYTE),
  GRUPO_ECONOMICO_COD      VARCHAR2(3 BYTE),
  GRUPO_ECONOMICO_DESC     VARCHAR2(30 BYTE),
  SUBGRUPO_ECONOMICO_COD   VARCHAR2(3 BYTE),
  SUBGRUPO_ECONOMICO_DESC  VARCHAR2(30 BYTE),
  NOME_FIRMA               VARCHAR2(50 BYTE),
  MORADA_SEDE              VARCHAR2(255 BYTE),
  LOCALIDADE               VARCHAR2(50 BYTE),
  NIF                      VARCHAR2(20 BYTE),
  ACCAO_MAX_VAL            NUMBER(20,9),
  ACTIVIDADE_INICIO        DATE,
  ACTIVIDADE_FIM           DATE,
  COMPRAS_INICIO           DATE,
  COMPRAS_FIM              DATE,
  QUANTIDADE_TOTAL_CX      NUMBER(20,9),
  VALOR_TOTAL              NUMBER(20,9),
  DEBITO_TOTAL             NUMBER(20,9),
  DEBITO_RESIDUAL          NUMBER(20,9),
  OBSERVACOES              VARCHAR2(4000 BYTE),
  INSERT_YEAR              VARCHAR2(4 BYTE),
  INSERT_YEAR_MONTH        VARCHAR2(7 BYTE),
  INSERT_DATE              DATE,
  INSERT_USER              VARCHAR2(20 BYTE),
  LAST_ALTER_YEAR          VARCHAR2(4 BYTE),
  LAST_ALTER_YEAR_MONTH    VARCHAR2(7 BYTE),
  LAST_ALTER_DATE          DATE,
  LAST_ALTER_USER          VARCHAR2(20 BYTE)
)
TABLESPACE DW_DIMENSOES
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE UNIQUE INDEX LK_AE_CONTRATO_PK ON CINDY_DW.LK_AE_CONTRATO
(ID_CONTRATO)
LOGGING
TABLESPACE DW_BO
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE CINDY_DW.LK_AE_CONTRATO ADD (
  CONSTRAINT LK_AE_CONTRATO_PK
 PRIMARY KEY
 (ID_CONTRATO)
    USING INDEX 
    TABLESPACE DW_BO
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
                FREELISTS        1
                FREELIST GROUPS  1
               ));


-- TABELA DE LOCKUP DOS TIPOS DE ACTIVIDADE
-- ###################################################################
DROP TABLE CINDY_DW.LK_AE_TIPO_ACTIVIDADE CASCADE CONSTRAINTS;
CREATE TABLE CINDY_DW.LK_AE_TIPO_ACTIVIDADE
(
  ID_CONTRATO              INTEGER              NOT NULL,
  TIPO_ACTIVIDADE          VARCHAR2(50 BYTE),
  NUM_ACORDO               VARCHAR2(50 BYTE),
  VALOR                    NUMBER(20,9)
)
TABLESPACE DW_DIMENSOES
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;


-- TABELA DE LOCKUP DOS ABASTECIMENTOS
-- ###################################################################
DROP TABLE CINDY_DW.LK_AE_ABASTECIMENTO CASCADE CONSTRAINTS;
CREATE TABLE CINDY_DW.LK_AE_ABASTECIMENTO
(
  ID_CONTRATO              INTEGER              NOT NULL,
  COD_CONCESSIONARIO       VARCHAR2(3 BYTE),
  DES_CONCESSIONARIO       VARCHAR2(40 BYTE),
  FACTURACAO		       CHAR(1)          NOT NULL,
  DISTRIBUICAO		       CHAR(1)          NOT NULL,
  OBSERVACOES              VARCHAR2(255 BYTE)
)
TABLESPACE DW_DIMENSOES
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

-- TABELA DE FACTOS COM OS VALORES DO C�LCULO DAS AE
-- ###################################################################
DROP TABLE CINDY_DW.FACT_AE_PRODUTO CASCADE CONSTRAINTS;
CREATE TABLE CINDY_DW.FACT_AE_PRODUTO
(
    ID_CONTRATO                     INTEGER              NOT NULL,
    ID_NUM_PROPOSTA                 VARCHAR2(12 BYTE)    NOT NULL,
    ID_EMISSOR                      VARCHAR2(2 BYTE)     NOT NULL,
    ID_ANO                          VARCHAR2(4 BYTE)     NOT NULL,
    ID_NUM_SERIE                    VARCHAR2(2 BYTE)     NOT NULL,
    ID_NUMERO                       VARCHAR2(4 BYTE)     NOT NULL,
    COD_MES                         VARCHAR2(7 BYTE)     NOT NULL,
    LOCAL_ENTREGA_COD               VARCHAR2(15 BYTE)    NOT NULL,
    LOCAL_ENTREGA_NOME              VARCHAR2(50 BYTE)    NOT NULL,
    PRODUTO_COD                     VARCHAR2(9 BYTE)     NOT NULL,
    PRODUTO_NOME                    VARCHAR2(40 BYTE)    NOT NULL,
    BUDGET_VENDAS                   NUMBER(11,3)         NOT NULL,
    BUDGET_MARKETING                NUMBER(11,3)         NOT NULL,
    ACTIVIDADE_INICIO               DATE                 NOT NULL,
    ACTIVIDADE_FIM                  DATE                 NOT NULL,
    COMPRAS_INICIO                  DATE                 NOT NULL,
    COMPRAS_FIM                     DATE                 NOT NULL,
    PVP                             NUMBER(20,9)         NOT NULL,
    UNIDADE_BASE					VARCHAR2(3 BYTE)	 NOT NULL,
    PREVISAO_QUANTIDADE             NUMBER(20,9)         NOT NULL,
    PREVISAO_VALOR                  NUMBER(20,9)         NOT NULL,
    PREVISAO_DESCONTO_VENDAS        NUMBER(20,9)         NOT NULL,
    PREVISAO_DESCONTO_MARKETING     NUMBER(20,9)         NOT NULL,
    PREVISAO_DESCONTO_TOTAL         NUMBER(20,9)         NOT NULL,
    GSV                             NUMBER(20,9)         NOT NULL,
    VALOR_VENDAS                    NUMBER(20,9)         NOT NULL,
    VALOR_MARKETING                 NUMBER(20,9)         NOT NULL,
    VALOR_TOTAL                     NUMBER(20,9)         NOT NULL 
)
TABLESPACE DW_FACTOS
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON CINDY_DW.LK_AE_CONTRATO         TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON CINDY_DW.LK_AE_CONTRATO         TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON CINDY_DW.LK_AE_TIPO_ACTIVIDADE  TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON CINDY_DW.LK_AE_TIPO_ACTIVIDADE  TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON CINDY_DW.LK_AE_ABASTECIMENTO    TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON CINDY_DW.LK_AE_ABASTECIMENTO    TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON CINDY_DW.FACT_AE_PRODUTO        TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON CINDY_DW.FACT_AE_PRODUTO        TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.LK_EMISSOR                  TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.LK_EMISSOR                  TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.LK_CUSTOMER_SERVICE         TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.LK_CUSTOMER_SERVICE         TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.LK_TIPO_ACTIVIDADE          TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.LK_TIPO_ACTIVIDADE          TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_CONTRATO               TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_CONTRATO               TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_ENTIDADE               TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_ENTIDADE               TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_ACTIVIDADE             TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_ACTIVIDADE             TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_ABASTECIMENTO          TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_ABASTECIMENTO          TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_TPR                    TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_TPR                    TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_DEBITO                 TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_DEBITO                 TO SRA;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_LOG                    TO CINDY_ETL;
GRANT DELETE, INSERT, SELECT, UPDATE ON SRA.T_AE_LOG                    TO SRA;

COMMIT;


--############################################################################################################
--############################################################################################################
--############################################################################################################
        
-- CRIA��O DAS PROCEDURES PACKAGE AE
-- ################################################################### 
  

CREATE OR REPLACE PACKAGE SRA.PK_AE AS
    
-- 01S ################################################################### 
    PROCEDURE SP_AE_AGRUPAMENTO_PRODUTO_GET (CUR OUT SYS_REFCURSOR);
    
-- 02S ################################################################### 
    PROCEDURE SP_AE_GRUPO_PRODUTO_GET 
    (
        V_ID_AGRUPAMENTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    );
    
-- 03S ################################################################### 
    PROCEDURE SP_AE_SUB_GRUPO_PRODUTO_GET 
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,     
        CUR OUT SYS_REFCURSOR
    );
    
-- 04S ################################################################### 
    PROCEDURE SP_AE_SUB_AGG_PRODUTO_GET 
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,    
        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    );
    
-- 05S ################################################################### 
    PROCEDURE SP_AE_PRODUTO_GET
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,    
        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
        V_ID_SUB_AGG_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    );
    
-- 051S ################################################################### 
    PROCEDURE SP_AE_PRODUTO_SEARCH
    (
        V_ID_PRODUTO VARCHAR2,    
        V_NOME_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    );
    
-- 06S ################################################################### 
    PROCEDURE SP_AE_EMISSOR_GET(CUR OUT SYS_REFCURSOR);

-- 07S ################################################################### 
    PROCEDURE SP_AE_CUSTOMER_SERVICE_GET(CUR OUT SYS_REFCURSOR);

-- 075S ################################################################## 
    PROCEDURE SP_AE_GRUPO_ECONOMICO_GET(CUR OUT SYS_REFCURSOR);

-- 076S ################################################################## 
    PROCEDURE SP_AE_SUBGRUPO_ECONOMICO_GET
    (
        V_ID_GRUPO_ECONOMICO VARCHAR2,    
        CUR OUT SYS_REFCURSOR
    );
    
-- 077S ################################################################## 
    PROCEDURE SP_AE_CONCESSIONARIO_GET(CUR OUT SYS_REFCURSOR);
    
-- 08S ################################################################### 
    PROCEDURE SP_AE_TIPO_ACTIVIDADE_GET(CUR OUT SYS_REFCURSOR);

-- 09T ###################################################################       
    PROCEDURE SP_AE_CONTRATO_EXISTS
    (
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_EXISTS            OUT    INT
    );
    
-- 09S1 ###################################################################       
    PROCEDURE SP_AE_CONTRATOS_GET (WHERESTM VARCHAR2, CUR OUT SYS_REFCURSOR );
        
-- 09S2 ###################################################################       
    PROCEDURE SP_AE_CONTRATO_GET
    (
        V_ID_CONTRATO               VARCHAR2,
        CUR_CONTRATO                   OUT SYS_REFCURSOR,
        CUR_ENTIDADE                   OUT SYS_REFCURSOR,
        CUR_ABASTECIMENTO              OUT SYS_REFCURSOR,
        CUR_ACTIVIDADE                 OUT SYS_REFCURSOR,
        CUR_TPR                        OUT SYS_REFCURSOR,
        CUR_DEBITOS                    OUT SYS_REFCURSOR,
        CUR_LOG                        OUT SYS_REFCURSOR
    );
    
-- 09I ###################################################################   
    PROCEDURE SP_AE_CONTRATO_INS
    (
        V_ID_CONTRATO              IN OUT INT,
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_DATA_RECEPCAO            DATE,
        V_ID_CUSTOMER_SERVICE      VARCHAR2,
        V_NIVEL_CONTRATO		   VARCHAR2,
        V_GRUPO_ECONOMICO_COD      VARCHAR2,
        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
        V_NOME_FIRMA               VARCHAR2,
        V_MORADA_SEDE              VARCHAR2,
        V_LOCALIDADE               VARCHAR2,
        V_NIF                      VARCHAR2,
        V_OBSERVACOES              VARCHAR2,
        V_INSERT_USER              VARCHAR2,
        V_EXISTS            OUT    INT
    );      
    
-- 09U ###################################################################       
    PROCEDURE SP_AE_CONTRATO_UPD
    (
        V_ID_CONTRATO              INT,
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_DATA_RECEPCAO            DATE,
        V_ID_CUSTOMER_SERVICE      VARCHAR2,
        V_NIVEL_CONTRATO		   VARCHAR2,
        V_GRUPO_ECONOMICO_COD      VARCHAR2,
        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
        V_NOME_FIRMA               VARCHAR2,
        V_MORADA_SEDE              VARCHAR2,
        V_LOCALIDADE               VARCHAR2,
        V_NIF                      VARCHAR2,
        V_OBSERVACOES              VARCHAR2,
        V_INSERT_USER              VARCHAR2,
        V_EXISTS            OUT    INT
    );      

-- 10S1 ###################################################################       
    PROCEDURE SP_AE_ENTIDADES_GET 
    (
        WHERESTM     VARCHAR2, 
        CUR         OUT SYS_REFCURSOR
    );

-- 10S2 ###################################################################       
    PROCEDURE SP_AE_ENTIDADE_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );

-- 10I ###################################################################   
    PROCEDURE SP_AE_ENTIDADE_INS
    (
        V_ID_CONTRATO                 INT,
        V_COD_LE                   VARCHAR2,
        V_COD_GRUPO_ECONOMICO      VARCHAR2,
        V_COD_SUB_GRUPO_ECONOMICO  VARCHAR2,
        V_COD_CLIENTE              VARCHAR2
    );

-- 10D1 ##################################################################       
    PROCEDURE SP_AE_ENTIDADE_DEL 
    (
        V_ID_CONTRATO   INT,
        V_COD_LE             VARCHAR2
    ); 
    
-- 10D2 ##################################################################       
    PROCEDURE SP_AE_ENTIDADES_DEL 
    (
        V_ID_CONTRATO   INT
    );    
    
-- 11S ###################################################################       
    PROCEDURE SP_AE_ABASTECIMENTO_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );

-- 11I ###################################################################   
    PROCEDURE SP_AE_ABASTECIMENTO_INS
    (
        V_ID_CONTRATO              INT,
        V_COD_CONCESSIONARIO       VARCHAR2,
        V_FACTURACAO		       CHAR,
        V_DISTRIBUICAO			   CHAR,
        V_OBSERVACOES              VARCHAR2
    );
    
-- 11U ###################################################################    
    PROCEDURE SP_AE_ABASTECIMENTO_UPD
    (
        V_ID_CONTRATO              INT,
        V_COD_CONCESSIONARIO       VARCHAR2,
        V_FACTURACAO		       CHAR,
        V_DISTRIBUICAO			   CHAR,
        V_OBSERVACOES              VARCHAR2
    );

-- 11D ###################################################################       
    PROCEDURE SP_AE_ABASTECIMENTO_DEL 
    (
        V_ID_CONTRATO			INT,
        V_COD_CONCESSIONARIO    VARCHAR2
    );    
        
    
-- 12S ###################################################################       
    PROCEDURE SP_AE_ACTIVIDADE_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );
    
-- 12I ###################################################################    
    PROCEDURE SP_AE_ACTIVIDADE_INS
    (
        V_ID_CONTRATO               INT,
        V_ID_TIPO_ACTIVIDADE        INT,
        V_NUM_ACORDO                VARCHAR2,
        V_VALOR_ACORDO              NUMBER
    );
    
-- 12U ###################################################################    
    PROCEDURE SP_AE_ACTIVIDADE_UPD
    (
        V_ID_ACTIVIDADE                INT,
        V_ID_CONTRATO               INT,
        V_ID_TIPO_ACTIVIDADE        INT,
        V_NUM_ACORDO                VARCHAR2,
        V_VALOR_ACORDO              NUMBER
    );

-- 12D ###################################################################       
    PROCEDURE SP_AE_ACTIVIDADE_DEL 
    (
        V_ID_ACTIVIDADE   INT
    );    

-- 13S ###################################################################       
    PROCEDURE SP_AE_TPR_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );
    
-- 13I ###################################################################    
    PROCEDURE SP_AE_TPR_INS
    (
        V_ID_CONTRATO               INT,
        V_COD_PRODUTO               VARCHAR2,
        V_BUDGET_VENDAS             NUMBER,
        V_BUDGET_MARKETING          NUMBER,
        V_ACTIVIDADE_INICIO         DATE,
        V_ACTIVIDADE_FIM            DATE,
        V_COMPRAS_INICIO            DATE,
        V_COMPRAS_FIM               DATE,
        V_PVP                       NUMBER,
        V_UNIDADE_BASE				VARCHAR2,
        V_PREVISAO_QUANTIDADE        NUMBER
    );
    
-- 13U ###################################################################    
    PROCEDURE SP_AE_TPR_UPD
    (
        V_ID_TPR                    INT,
        V_ID_CONTRATO               INT,
        V_COD_PRODUTO               VARCHAR2,
        V_BUDGET_VENDAS             NUMBER,
        V_BUDGET_MARKETING          NUMBER,
        V_ACTIVIDADE_INICIO         DATE,
        V_ACTIVIDADE_FIM            DATE,
        V_COMPRAS_INICIO            DATE,
        V_COMPRAS_FIM               DATE,
        V_PVP                       NUMBER,
        V_UNIDADE_BASE				VARCHAR2,
        V_PREVISAO_QUANTIDADE        NUMBER
    );
          
-- 13D ###################################################################       
    PROCEDURE SP_AE_TPR_DEL 
    (
        V_ID_TPR   INT
    );    
    
-- 14S ###################################################################       
    PROCEDURE SP_AE_DEBITO_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );
    
-- 14I ###################################################################    
    PROCEDURE SP_AE_DEBITO_INS
    (
        V_ID_CONTRATO               INT,
        V_DATA_DEBITO               DATE,
        V_VALOR_DEBITO              NUMBER
    );

-- 14U ###################################################################    
    PROCEDURE SP_AE_DEBITO_UPD
    (
        V_ID_DEBITO                    INT,
        V_ID_CONTRATO               INT,
        V_DATA_DEBITO               DATE,
        V_VALOR_DEBITO              NUMBER
    );
    
-- 14D ###################################################################       
    PROCEDURE SP_AE_DEBITO_DEL 
    (
        V_ID_DEBITO   INT
    );    

-- 15S ###################################################################       
    PROCEDURE SP_AE_LOG_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    );    
          
-- 15I ###################################################################    
    PROCEDURE SP_AE_LOG_INS
    (
        V_ID_CONTRATO            INT,
        V_LOG_DATA               DATE,
        V_LOG_USER               VARCHAR2,
        V_MESSAGE                VARCHAR2
    );
  
END PK_AE;
/

COMMIT;


--#################################################################################################
--#################################################################################################

CREATE OR REPLACE PACKAGE BODY SRA.PK_AE AS 
    
-- 01S ################################################################### 
    PROCEDURE SP_AE_AGRUPAMENTO_PRODUTO_GET (CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT DISTINCT
                AG.COD_AGRUPAMENTO_PRODUTO AS ID_AGRUPAMENTO,
                AG.DES_AGRUPAMENTO_PRODUTO AS NOME_AGRUPAMENTO,
                (AG.COD_AGRUPAMENTO_PRODUTO || ' - ' || AG.DES_AGRUPAMENTO_PRODUTO) AS ID_NOME_AGRUPAMENTO
            FROM CINDY_DW.LK_AGRUPAMENTO_PRODUTO AG
            INNER JOIN CINDY_DW.LK_GRUPO_PRODUTO GP
                ON(GP.COD_AGRUPAMENTO_PRODUTO = AG.COD_AGRUPAMENTO_PRODUTO)
            ORDER BY CAST(AG.COD_AGRUPAMENTO_PRODUTO AS INT);
        COMMIT;
    END;
      
-- 02S ################################################################### 
    PROCEDURE SP_AE_GRUPO_PRODUTO_GET 
    (
        V_ID_AGRUPAMENTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                GP.COD_GRUPO_PRODUTO AS ID_GRUPO_PRODUTO,
                GP.DES_GRUPO_PRODUTO AS NOME_GRUPO_PRODUTO,
                (GP.COD_GRUPO_PRODUTO || ' - ' || GP.DES_GRUPO_PRODUTO ) AS ID_NOME_GRUPO_PRODUTO
            FROM CINDY_DW.LK_GRUPO_PRODUTO GP
            INNER JOIN CINDY_DW.LK_PRODUTO P
                ON(P.COD_GRUPO_PRODUTO = GP.COD_GRUPO_PRODUTO)
            WHERE GP.COD_AGRUPAMENTO_PRODUTO = V_ID_AGRUPAMENTO
            ORDER BY CAST(GP.COD_GRUPO_PRODUTO AS INT);
            
        COMMIT;    
    END;
    
-- 03S ################################################################### 
    PROCEDURE SP_AE_SUB_GRUPO_PRODUTO_GET 
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,     
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                GP.COD_SUBGRUPO_PRODUTO AS ID_SUBGRUPO_PRODUTO,
                GP.DES_SUBGRUPO_PRODUTO AS NOME_SUBGRUPO_PRODUTO,
                (GP.COD_SUBGRUPO_PRODUTO || ' - ' || GP.DES_SUBGRUPO_PRODUTO) AS ID_NOME_SUBGRUPO_PRODUTO 
            FROM CINDY_DW.LK_SUBGRUPO_PRODUTO GP
            INNER JOIN CINDY_DW.LK_PRODUTO P
                ON(P.COD_SUBGRUPO_PRODUTO = GP.COD_SUBGRUPO_PRODUTO)
            WHERE P.COD_GRUPO_PRODUTO = V_ID_GRUPO_PRODUTO
            ORDER BY CAST(GP.COD_SUBGRUPO_PRODUTO  AS INT);
        COMMIT;    
    END;
        
-- 04S ################################################################### 
    PROCEDURE SP_AE_SUB_AGG_PRODUTO_GET 
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,    
        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                GP.COD_SUB_AGG_PRODUTO AS ID_SUB_AGG_PRODUTO,
                GP.DES_SUB_AGG_PRODUTO AS NOME_SUB_AGG_PRODUTO,
                (GP.COD_SUB_AGG_PRODUTO || ' - ' || GP.DES_SUB_AGG_PRODUTO ) AS ID_NOME_SUB_AGG_PRODUTO
            FROM CINDY_DW.LK_SUB_AGG_PRODUTO GP
            INNER JOIN CINDY_DW.LK_PRODUTO P
                ON(P.COD_SUB_AGG_PRODUTO = GP.COD_SUB_AGG_PRODUTO)
            WHERE P.COD_GRUPO_PRODUTO = V_ID_GRUPO_PRODUTO
              AND P.COD_SUBGRUPO_PRODUTO = V_ID_SUB_GRUPO_PRODUTO
              ORDER BY CAST(GP.COD_SUB_AGG_PRODUTO AS INT);
        COMMIT;    
    END;
    
-- 05S ################################################################### 
    PROCEDURE SP_AE_PRODUTO_GET
    (
        V_ID_GRUPO_PRODUTO VARCHAR2,    
        V_ID_SUB_GRUPO_PRODUTO VARCHAR2,
        V_ID_SUB_AGG_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                P.COD_PRODUTO AS ID_PRODUTO,
                P.DES_PRODUTO AS NOME_PRODUTO,
                P.UNI_BAS_CINDY AS UNIDADE_BASE,
                P.PRECO_VEN_UNI_BAS AS PVP_PRODUTO,
                (P.COD_PRODUTO || ' - ' || P.DES_PRODUTO) AS ID_NOME_PRODUTO
            FROM CINDY_DW.LK_PRODUTO P
            WHERE P.COD_GRUPO_PRODUTO = V_ID_GRUPO_PRODUTO
              AND P.COD_SUBGRUPO_PRODUTO = V_ID_SUB_GRUPO_PRODUTO
              AND P.COD_SUB_AGG_PRODUTO = V_ID_SUB_AGG_PRODUTO
            ORDER BY P.COD_PRODUTO;
        COMMIT;    
    END;
    
-- 051S ################################################################### 
    PROCEDURE SP_AE_PRODUTO_SEARCH
    (
        V_ID_PRODUTO VARCHAR2,    
        V_NOME_PRODUTO VARCHAR2,
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT DISTINCT 
                P.COD_PRODUTO AS ID_PRODUTO,
                P.DES_PRODUTO AS NOME_PRODUTO,
                P.UNI_BAS_CINDY AS UNIDADE_BASE,
                P.PRECO_VEN_UNI_BAS AS PVP_PRODUTO,
                (P.COD_PRODUTO || ' - ' || P.DES_PRODUTO) AS ID_NOME_PRODUTO,
                P.COD_GRUPO_PRODUTO,
                P.COD_SUBGRUPO_PRODUTO,
                P.COD_SUB_AGG_PRODUTO
            FROM CINDY_DW.LK_PRODUTO P
            WHERE (P.COD_PRODUTO = V_ID_PRODUTO
					OR V_ID_PRODUTO IS NULL)
			  AND (UPPER(P.DES_PRODUTO) LIKE UPPER('%'|| V_NOME_PRODUTO || '%')
					OR V_NOME_PRODUTO IS NULL)
            ORDER BY P.COD_PRODUTO;
        COMMIT;    
    END;
    
-- 06S ################################################################### 
    PROCEDURE SP_AE_EMISSOR_GET(CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
                ID_EMISSOR,
                NOME_EMISSOR,
                (ID_EMISSOR || ' - ' || NOME_EMISSOR) as COD_NOME_EMISSOR
            FROM SRA.LK_EMISSOR
            ORDER BY CAST(ID_EMISSOR as INT);
        COMMIT;
    END;
        
-- 07S ###################################################################     
    PROCEDURE SP_AE_CUSTOMER_SERVICE_GET(CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
                ID_CUSTOMER_SERVICE,
                NOME_CUSTOMER_SERVICE,
                (ID_CUSTOMER_SERVICE || ' - ' || NOME_CUSTOMER_SERVICE) as COD_NOME_CUSTOMER_SERVICE
            FROM SRA.LK_CUSTOMER_SERVICE
            ORDER BY CAST(ID_CUSTOMER_SERVICE as INT);
        COMMIT;
    END;
        
-- 075S ################################################################## 
    PROCEDURE SP_AE_GRUPO_ECONOMICO_GET(CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
				COD_GRUPO_ECONOMICO AS ID_GRUPO_ECONOMICO,
				DES_GRUPO_ECONOMICO AS NOME_GRUPO_ECONOMICO,
				(COD_GRUPO_ECONOMICO || ' - ' || DES_GRUPO_ECONOMICO) as ID_NOME_GRUPO_ECONOMICO
			FROM CINDY_DW.LK_GRUPO_ECONOMICO
			WHERE COD_GRUPO_ECONOMICO NOT IN ('SSS','000','999')
			ORDER BY COD_GRUPO_ECONOMICO;
        COMMIT;
    END;

-- 076S ################################################################## 
    PROCEDURE SP_AE_SUBGRUPO_ECONOMICO_GET
    (
        V_ID_GRUPO_ECONOMICO VARCHAR2,    
        CUR OUT SYS_REFCURSOR
    ) IS
    BEGIN
		OPEN CUR FOR 
			SELECT 
				 COD_SUBGRUPO_ECONOMICO AS ID_SUBGRUPO_ECONOMICO,
                DES_SUBGRUPO_ECONOMICO AS NOME_SUBGRUPO_ECONOMICO,
                (COD_SUBGRUPO_ECONOMICO || ' - ' || DES_SUBGRUPO_ECONOMICO) as ID_NOME_SUBGRUPO_ECONOMICO
            FROM CINDY_DW.LK_SUBGRUPO_ECONOMICO
            WHERE COD_GRUPO_ECONOMICO = V_ID_GRUPO_ECONOMICO
             AND  COD_SUBGRUPO_ECONOMICO NOT IN ('SSS','000','999')
            ORDER BY COD_SUBGRUPO_ECONOMICO;
		COMMIT;
    END;

-- 077S ##################################################################  
	PROCEDURE SP_AE_CONCESSIONARIO_GET(CUR OUT SYS_REFCURSOR)  IS
    BEGIN
        OPEN CUR FOR 
            SELECT 
				COD_CONCESSIONARIO AS ID_CONCESSIONARIO,
				DES_CONCESSIONARIO AS NOME_CONCESSIONARIO,
				(COD_CONCESSIONARIO || ' - ' || DES_CONCESSIONARIO) as ID_NOME_CONCESSIONARIO
			FROM CINDY_DW.LK_CONCESSIONARIO
			ORDER BY COD_CONCESSIONARIO;
        COMMIT;
    END;    
    
-- 08S ###################################################################     
    PROCEDURE SP_AE_TIPO_ACTIVIDADE_GET(CUR OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN CUR FOR 
            SELECT *
            FROM SRA.LK_TIPO_ACTIVIDADE
        COMMIT;
    END;
        
-- 09T ###################################################################       
    PROCEDURE SP_AE_CONTRATO_EXISTS
    (
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_EXISTS            OUT    INT
    ) IS
    BEGIN
        SELECT NVL(MAX(ID_CONTRATO),-1) INTO V_EXISTS 
        FROM T_AE_CONTRATO 
        WHERE ID_NUM_PROPOSTA = (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO);        
    END;

-- 09S1 ###################################################################       
    PROCEDURE SP_AE_CONTRATOS_GET 
    (
        WHERESTM VARCHAR2, 
        CUR OUT SYS_REFCURSOR 
    ) IS
    sqlstm VARCHAR2(2000);
    BEGIN    
        sqlstm :=  'SELECT * '
				|| 'FROM ('
				|| ' SELECT C.ID_CONTRATO,'
                || ' C.ID_NUM_PROPOSTA,'
                || ' C.ID_EMISSOR,'
                || ' C.ID_ANO,'
                || ' C.ID_NUM_SERIE,'
                || ' C.ID_NUMERO,'
                || ' E.NOME_EMISSOR ID_EMISSOR_NOME,'
                || ' C.DATA_RECEPCAO,'
                || ' C.ID_CUSTOMER_SERVICE,'
                || ' CS.NOME_CUSTOMER_SERVICE CUSTOMER_SERVICE_NOME,'
                || ' C.NIVEL_CONTRATO NIVEL_CONTRATO,'
                || ' C.GRUPO_ECONOMICO_COD,'
                || ' GE.DES_GRUPO_ECONOMICO GRUPO_ECONOMICO_DESC,'
                || ' C.SUBGRUPO_ECONOMICO_COD,'
                || ' SGE.DES_SUBGRUPO_ECONOMICO SUBGRUPO_ECONOMICO_DESC,'
                || ' C.NOME_FIRMA,'
                || ' C.MORADA_SEDE,'
                || ' C.LOCALIDADE,'
                || ' C.NIF,'
                || ' C.OBSERVACOES,'
                || ' C.INSERT_DATE,'
                || ' C.INSERT_USER,'
                || ' C.ALTER_DATE,'
                || ' C.ALTER_USER  '
                || ' FROM SRA.T_AE_CONTRATO C '
                || ' LEFT JOIN SRA.LK_EMISSOR E'
                || '  ON(E.ID_EMISSOR = C.ID_EMISSOR) '
                || ' LEFT JOIN SRA.LK_CUSTOMER_SERVICE CS'
                || '  ON(CS.ID_CUSTOMER_SERVICE = C.ID_CUSTOMER_SERVICE)'
                || ' LEFT JOIN CINDY_DW.LK_GRUPO_ECONOMICO GE'
                || '  ON(GE.COD_GRUPO_ECONOMICO = C.GRUPO_ECONOMICO_COD)'
                || ' LEFT JOIN CINDY_DW.LK_SUBGRUPO_ECONOMICO SGE'
                || '  ON   (SGE.COD_SUBGRUPO_ECONOMICO = C.SUBGRUPO_ECONOMICO_COD)'
                || '   AND (SGE.COD_GRUPO_ECONOMICO = C.GRUPO_ECONOMICO_COD)'
                || ')'
                || ' WHERE '
                || WHERESTM;
        OPEN CUR FOR sqlstm;
        COMMIT;
    END;
          
-- 09S2 ###################################################################       
    PROCEDURE SP_AE_CONTRATO_GET
    (
        V_ID_CONTRATO			VARCHAR2,
        CUR_CONTRATO            OUT SYS_REFCURSOR,
        CUR_ENTIDADE            OUT SYS_REFCURSOR,
        CUR_ABASTECIMENTO       OUT SYS_REFCURSOR,
        CUR_ACTIVIDADE          OUT SYS_REFCURSOR,
        CUR_TPR                 OUT SYS_REFCURSOR,
        CUR_DEBITOS             OUT SYS_REFCURSOR,
        CUR_LOG                 OUT SYS_REFCURSOR
    ) IS
    BEGIN
        SP_AE_CONTRATOS_GET ('ID_CONTRATO = '||V_ID_CONTRATO,CUR_CONTRATO);
        SP_AE_ENTIDADE_GET(V_ID_CONTRATO,CUR_ENTIDADE);
        SP_AE_ABASTECIMENTO_GET(V_ID_CONTRATO,CUR_ABASTECIMENTO);
        SP_AE_ACTIVIDADE_GET(V_ID_CONTRATO,CUR_ACTIVIDADE);
        SP_AE_TPR_GET(V_ID_CONTRATO,CUR_TPR);
        SP_AE_DEBITO_GET(V_ID_CONTRATO,CUR_DEBITOS);
        SP_AE_LOG_GET(V_ID_CONTRATO,CUR_LOG);
        COMMIT;
    END;
    
-- 09I ################################################################### 
    PROCEDURE SP_AE_CONTRATO_INS
    (
        V_ID_CONTRATO              IN OUT INT,
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_DATA_RECEPCAO            DATE,
        V_ID_CUSTOMER_SERVICE      VARCHAR2,
        V_NIVEL_CONTRATO		   VARCHAR2,
        V_GRUPO_ECONOMICO_COD      VARCHAR2,
        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
        V_NOME_FIRMA               VARCHAR2,
        V_MORADA_SEDE              VARCHAR2,
        V_LOCALIDADE               VARCHAR2,
        V_NIF                      VARCHAR2,
        V_OBSERVACOES              VARCHAR2,
        V_INSERT_USER              VARCHAR2,
        V_EXISTS            OUT    INT
    ) IS
    
    ID INT:=NULL;
    BEGIN    
        
        SELECT COUNT(*) INTO V_EXISTS 
        FROM T_AE_CONTRATO 
        WHERE ID_NUM_PROPOSTA = (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO);
        
        IF V_EXISTS = 0 THEN    
            
            SELECT SRA.SEQ_AE_ID_CONTRATO.NEXTVAL INTO ID FROM DUAL;
            
            INSERT INTO T_AE_CONTRATO
            (
                ID_CONTRATO              ,
                ID_NUM_PROPOSTA          ,
                ID_EMISSOR               ,
                ID_ANO                   ,
                ID_NUM_SERIE             ,
                ID_NUMERO                ,
                DATA_RECEPCAO            ,
                ID_CUSTOMER_SERVICE      ,
                NIVEL_CONTRATO			 ,
                GRUPO_ECONOMICO_COD      ,
                SUBGRUPO_ECONOMICO_COD   ,
                NOME_FIRMA               ,
                MORADA_SEDE              ,
                LOCALIDADE               ,
                NIF                      ,
                OBSERVACOES              ,
                INSERT_DATE              ,
                INSERT_USER              ,
                ALTER_DATE               ,
                ALTER_USER
            )
            VALUES
            (
                ID,                        
                (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO),
                V_ID_EMISSOR             ,
                V_ID_ANO                 ,
                V_ID_NUM_SERIE           ,
                V_ID_NUMERO              ,
                V_DATA_RECEPCAO          ,
                V_ID_CUSTOMER_SERVICE    ,
                V_NIVEL_CONTRATO		 ,
                V_GRUPO_ECONOMICO_COD    ,
                V_SUBGRUPO_ECONOMICO_COD ,
                V_NOME_FIRMA             ,
                V_MORADA_SEDE            ,
                V_LOCALIDADE             ,
                V_NIF                    ,
                V_OBSERVACOES            ,
                SYSDATE                  ,
                V_INSERT_USER            ,
                SYSDATE                  ,
                V_INSERT_USER          
            );
            
            V_ID_CONTRATO := ID;
            
        END IF;
    END;

-- 09U ###################################################################       
    PROCEDURE SP_AE_CONTRATO_UPD
    (
        V_ID_CONTRATO              INT,
        V_ID_EMISSOR               VARCHAR2,
        V_ID_ANO                   VARCHAR2,
        V_ID_NUM_SERIE             VARCHAR2,
        V_ID_NUMERO                VARCHAR2,
        V_DATA_RECEPCAO            DATE,
        V_ID_CUSTOMER_SERVICE      VARCHAR2,
        V_NIVEL_CONTRATO		   VARCHAR2,
        V_GRUPO_ECONOMICO_COD      VARCHAR2,
        V_SUBGRUPO_ECONOMICO_COD   VARCHAR2,
        V_NOME_FIRMA               VARCHAR2,
        V_MORADA_SEDE              VARCHAR2,
        V_LOCALIDADE               VARCHAR2,
        V_NIF                      VARCHAR2,
        V_OBSERVACOES              VARCHAR2,
        V_INSERT_USER              VARCHAR2,
        V_EXISTS            OUT    INT
    ) IS
    BEGIN    
        
        SELECT COUNT(*) INTO V_EXISTS 
        FROM T_AE_CONTRATO 
        WHERE ID_NUM_PROPOSTA = (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO)
        AND ID_CONTRATO <> V_ID_CONTRATO;
        
        IF V_EXISTS = 0 THEN    
            UPDATE T_AE_CONTRATO
            SET 
                ID_NUM_PROPOSTA          = (V_ID_EMISSOR||V_ID_ANO||V_ID_NUM_SERIE||V_ID_NUMERO),
                ID_EMISSOR               = V_ID_EMISSOR                ,
                ID_ANO                   = V_ID_ANO                    ,
                ID_NUM_SERIE             = V_ID_NUM_SERIE           ,
                ID_NUMERO                = V_ID_NUMERO              ,
                DATA_RECEPCAO            = V_DATA_RECEPCAO          ,
                ID_CUSTOMER_SERVICE      = V_ID_CUSTOMER_SERVICE    ,
                NIVEL_CONTRATO			 = V_NIVEL_CONTRATO		    ,
                GRUPO_ECONOMICO_COD      = V_GRUPO_ECONOMICO_COD    ,
                SUBGRUPO_ECONOMICO_COD   = V_SUBGRUPO_ECONOMICO_COD ,
                NOME_FIRMA               = V_NOME_FIRMA             ,
                MORADA_SEDE              = V_MORADA_SEDE            ,
                LOCALIDADE               = V_LOCALIDADE             ,
                NIF                      = V_NIF                    ,
                OBSERVACOES              = V_OBSERVACOES            ,
                ALTER_DATE               = SYSDATE                    ,
                ALTER_USER               = V_INSERT_USER
            WHERE ID_CONTRATO = V_ID_CONTRATO;     
        END IF;
    END;
    
-- 10S1 ################################################################### 
    PROCEDURE SP_AE_ENTIDADES_GET 
    (
        WHERESTM     VARCHAR2, 
        CUR         OUT SYS_REFCURSOR
    ) IS
        sqlstm VARCHAR2(1000);
        BEGIN 
            sqlstm := 'SELECT * FROM MV_LOCAIS_ENTREGA WHERE ' || WHERESTM;
            OPEN CUR FOR sqlstm;
            COMMIT;
    END ;
  
-- 10S2 ###################################################################       
    PROCEDURE SP_AE_ENTIDADE_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
				SELECT LE.* 
				FROM SRA.T_AE_CONTRATO C 
				INNER JOIN SRA.T_AE_ENTIDADE E
					ON(E.ID_CONTRATO = C.ID_CONTRATO)
				INNER JOIN SRA.MV_LOCAIS_ENTREGA LE
					ON(LE.COD_LOCAL_ENTREGA = E.COD_LE)
				WHERE C.ID_CONTRATO = V_ID_CONTRATO
				  AND C.NIVEL_CONTRATO = 'LE'
			UNION 
				SELECT LE.* 
				FROM SRA.T_AE_CONTRATO C
				INNER JOIN SRA.MV_LOCAIS_ENTREGA LE
					ON(LE.COD_GRUPO_ECONOMICO_LE = C.GRUPO_ECONOMICO_COD)
				WHERE C.ID_CONTRATO = V_ID_CONTRATO
				  AND C.NIVEL_CONTRATO = 'GE'
			UNION 
				SELECT LE.* 
				FROM SRA.T_AE_CONTRATO C
				INNER JOIN SRA.MV_LOCAIS_ENTREGA LE
					ON(LE.COD_GRUPO_ECONOMICO_LE = C.GRUPO_ECONOMICO_COD)
					AND(LE.COD_SUBGRUPO_ECONOMICO_LE = C.SUBGRUPO_ECONOMICO_COD)
				WHERE C.ID_CONTRATO = V_ID_CONTRATO
				  AND C.NIVEL_CONTRATO = 'SGE';
        COMMIT;    
    END;
    
-- 10I ###################################################################   
    PROCEDURE SP_AE_ENTIDADE_INS
    (
        V_ID_CONTRATO                 INT,
        V_COD_LE                   VARCHAR2,
        V_COD_GRUPO_ECONOMICO      VARCHAR2,
        V_COD_SUB_GRUPO_ECONOMICO  VARCHAR2,
        V_COD_CLIENTE              VARCHAR2
    ) IS
    BEGIN
        INSERT INTO T_AE_ENTIDADE
        (
           ID_CONTRATO               ,
           COD_LE                    ,
           COD_GRUPO_ECONOMICO       ,
           COD_SUB_GRUPO_ECONOMICO   ,
           COD_CLIENTE               ,
           INSERT_DATE
        )
        VALUES
        (
            V_ID_CONTRATO                 ,
            V_COD_LE                      ,
            V_COD_GRUPO_ECONOMICO         ,
            V_COD_SUB_GRUPO_ECONOMICO     ,
            V_COD_CLIENTE                 ,
            SYSDATE
        );
    END;

-- 10D1 ###################################################################       
    PROCEDURE SP_AE_ENTIDADE_DEL 
    (
        V_ID_CONTRATO   INT,
        V_COD_LE        VARCHAR2
    ) IS
    BEGIN
        DELETE T_AE_ENTIDADE
        WHERE ID_CONTRATO = V_ID_CONTRATO
          AND COD_LE = V_COD_LE;
    END;
    
-- 10D2 ###################################################################       
    PROCEDURE SP_AE_ENTIDADES_DEL 
    (
        V_ID_CONTRATO   INT
    ) IS
    BEGIN
        DELETE T_AE_ENTIDADE
        WHERE ID_CONTRATO = V_ID_CONTRATO;
    END;
   
   
-- 11S ###################################################################       
    PROCEDURE SP_AE_ABASTECIMENTO_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
				SELECT A.*, NVL(C.DES_CONCESSIONARIO,'') DES_CONCESSIONARIO 
				FROM SRA.T_AE_ABASTECIMENTO A 
				LEFT JOIN CINDY_DW.LK_CONCESSIONARIO C
					ON(C.COD_CONCESSIONARIO = A.COD_CONCESSIONARIO)
				WHERE A.ID_CONTRATO = V_ID_CONTRATO;
        COMMIT;    
    END;

-- 11I ###################################################################   
    PROCEDURE SP_AE_ABASTECIMENTO_INS
    (
        V_ID_CONTRATO              INT,
        V_COD_CONCESSIONARIO       VARCHAR2,
        V_FACTURACAO		       CHAR,
        V_DISTRIBUICAO			   CHAR,
        V_OBSERVACOES              VARCHAR2
    ) IS
    BEGIN
        INSERT INTO T_AE_ABASTECIMENTO
        (
           ID_CONTRATO               ,
           COD_CONCESSIONARIO        ,
           FACTURACAO			     ,
           DISTRIBUICAO			     ,
           OBSERVACOES
        )
        VALUES
        (
            V_ID_CONTRATO            ,
            V_COD_CONCESSIONARIO     ,
            V_FACTURACAO		     ,
            V_DISTRIBUICAO			 ,
            V_OBSERVACOES
        );
    END;
    
-- 11U ###################################################################    
    PROCEDURE SP_AE_ABASTECIMENTO_UPD
    (
        V_ID_CONTRATO              INT,
        V_COD_CONCESSIONARIO       VARCHAR2,
        V_FACTURACAO		       CHAR,
        V_DISTRIBUICAO			   CHAR,
        V_OBSERVACOES              VARCHAR2
    ) IS
    BEGIN
        UPDATE T_AE_ABASTECIMENTO
        SET FACTURACAO         = V_FACTURACAO             ,
            DISTRIBUICAO       = V_DISTRIBUICAO           ,
            OBSERVACOES		   = V_OBSERVACOES
        WHERE ID_CONTRATO        = V_ID_CONTRATO
          AND COD_CONCESSIONARIO = V_COD_CONCESSIONARIO;
    END; 

-- 11D ###################################################################       
    PROCEDURE SP_AE_ABASTECIMENTO_DEL 
    (
        V_ID_CONTRATO			INT,
        V_COD_CONCESSIONARIO    VARCHAR2
    ) IS
    BEGIN
        DELETE T_AE_ABASTECIMENTO
        WHERE ID_CONTRATO = V_ID_CONTRATO
          AND COD_CONCESSIONARIO = V_COD_CONCESSIONARIO;
    END;
   
-- 12S ###################################################################       
    PROCEDURE SP_AE_ACTIVIDADE_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR 
            SELECT *
            FROM SRA.T_AE_ACTIVIDADE AC
            INNER JOIN SRA.LK_TIPO_ACTIVIDADE TAC
                ON(TAC.ID_TIPO_ACTIVIDADE = AC.ID_TIPO_ACTIVIDADE)
            WHERE ID_CONTRATO = V_ID_CONTRATO;
        COMMIT;
    END;
    
-- 12I ###################################################################    
    PROCEDURE SP_AE_ACTIVIDADE_INS
    (
        V_ID_CONTRATO               INT,
        V_ID_TIPO_ACTIVIDADE        INT,
        V_NUM_ACORDO                VARCHAR2,
        V_VALOR_ACORDO              NUMBER
    ) IS
    BEGIN
        INSERT INTO T_AE_ACTIVIDADE
        (
            ID_ACTIVIDADE       ,
            ID_CONTRATO        , 
            ID_TIPO_ACTIVIDADE ,
            NUM_ACORDO         ,
            VALOR_ACORDO              
        )
        VALUES
        (
            SRA.SEQ_AE_ID_ACTIVIDADE.NEXTVAL ,
            V_ID_CONTRATO        ,
            V_ID_TIPO_ACTIVIDADE ,
            V_NUM_ACORDO         ,
            V_VALOR_ACORDO             
        );
    END;
    
-- 12U ###################################################################    
    PROCEDURE SP_AE_ACTIVIDADE_UPD
    (
        V_ID_ACTIVIDADE                INT,
        V_ID_CONTRATO               INT,
        V_ID_TIPO_ACTIVIDADE        INT,
        V_NUM_ACORDO                VARCHAR2,
        V_VALOR_ACORDO              NUMBER
    ) IS
    BEGIN
        UPDATE T_AE_ACTIVIDADE
        SET ID_CONTRATO        = V_ID_CONTRATO            ,        
            ID_TIPO_ACTIVIDADE = V_ID_TIPO_ACTIVIDADE    , 
            NUM_ACORDO         = V_NUM_ACORDO             ,
            VALOR_ACORDO       = V_VALOR_ACORDO           
        WHERE ID_ACTIVIDADE    = V_ID_ACTIVIDADE;        
    END;

-- 12D ###################################################################       
    PROCEDURE SP_AE_ACTIVIDADE_DEL 
    (
        V_ID_ACTIVIDADE   INT
    ) IS
    BEGIN
        DELETE T_AE_ACTIVIDADE
        WHERE ID_ACTIVIDADE = V_ID_ACTIVIDADE;
    END;    

-- 13S ###################################################################       
    PROCEDURE SP_AE_TPR_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT TPR.*, P.DES_PRODUTO NOME_PRODUTO
            FROM SRA.T_AE_TPR TPR
            INNER JOIN CINDY_DW.LK_PRODUTO P
                ON(P.COD_PRODUTO = TPR.COD_PRODUTO)
            WHERE TPR.ID_CONTRATO = V_ID_CONTRATO;
        COMMIT;                
    END;
    
-- 13I ###################################################################    
    PROCEDURE SP_AE_TPR_INS
    (
        V_ID_CONTRATO               INT,
        V_COD_PRODUTO               VARCHAR2,
        V_BUDGET_VENDAS             NUMBER,
        V_BUDGET_MARKETING          NUMBER,
        V_ACTIVIDADE_INICIO         DATE,
        V_ACTIVIDADE_FIM            DATE,
        V_COMPRAS_INICIO            DATE,
        V_COMPRAS_FIM               DATE,
        V_PVP                       NUMBER,
        V_UNIDADE_BASE				VARCHAR2,
        V_PREVISAO_QUANTIDADE        NUMBER
    ) IS
    BEGIN
        INSERT INTO T_AE_TPR
        (
            ID_TPR                ,
            ID_CONTRATO         ,
            COD_PRODUTO         ,
            BUDGET_VENDAS       ,
            BUDGET_MARKETING    ,
            ACTIVIDADE_INICIO   ,
            ACTIVIDADE_FIM      ,
            COMPRAS_INICIO      ,
            COMPRAS_FIM         ,
            PVP                 ,
            UNIDADE_BASE		,
            PREVISAO_QUANTIDADE              
        )
        VALUES
        (
            SRA.SEQ_AE_ID_TPR.NEXTVAL ,
            V_ID_CONTRATO         ,
            V_COD_PRODUTO         ,
            V_BUDGET_VENDAS       ,
            V_BUDGET_MARKETING    ,
            V_ACTIVIDADE_INICIO   ,
            V_ACTIVIDADE_FIM      ,
            V_COMPRAS_INICIO      ,
            V_COMPRAS_FIM         ,
            V_PVP                 ,
            V_UNIDADE_BASE		  ,
            V_PREVISAO_QUANTIDADE  
        );  
    END;
    
-- 13U ###################################################################    
    PROCEDURE SP_AE_TPR_UPD
    (
        V_ID_TPR                    INT,
        V_ID_CONTRATO               INT,
        V_COD_PRODUTO               VARCHAR2,
        V_BUDGET_VENDAS             NUMBER,
        V_BUDGET_MARKETING          NUMBER,
        V_ACTIVIDADE_INICIO         DATE,
        V_ACTIVIDADE_FIM            DATE,
        V_COMPRAS_INICIO            DATE,
        V_COMPRAS_FIM               DATE,
        V_PVP                       NUMBER,
        V_UNIDADE_BASE				VARCHAR2,
        V_PREVISAO_QUANTIDADE        NUMBER
    ) IS
    BEGIN
        UPDATE T_AE_TPR
        SET
            ID_CONTRATO        = V_ID_CONTRATO          ,
            COD_PRODUTO        = V_COD_PRODUTO          ,
            BUDGET_VENDAS      = V_BUDGET_VENDAS        ,
            BUDGET_MARKETING   = V_BUDGET_MARKETING     ,
            ACTIVIDADE_INICIO  = V_ACTIVIDADE_INICIO    ,
            ACTIVIDADE_FIM     = V_ACTIVIDADE_FIM       ,
            COMPRAS_INICIO     = V_COMPRAS_INICIO       ,
            COMPRAS_FIM        = V_COMPRAS_FIM          ,
            PVP                = V_PVP                  ,
            UNIDADE_BASE	   = V_UNIDADE_BASE			,
            PREVISAO_QUANTIDADE = V_PREVISAO_QUANTIDADE               
        WHERE ID_TPR = V_ID_TPR;
    END;
          
-- 13D ###################################################################       
    PROCEDURE SP_AE_TPR_DEL 
    (
        V_ID_TPR   INT
    ) IS
    BEGIN
        DELETE T_AE_TPR
        WHERE ID_TPR = V_ID_TPR;
    END;    
    
-- 14S ###################################################################       
    PROCEDURE SP_AE_DEBITO_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR
            SELECT * 
            FROM SRA.T_AE_DEBITO
            WHERE ID_CONTRATO = V_ID_CONTRATO;       
        COMMIT;            
    END;
    
-- 14I ###################################################################    
    PROCEDURE SP_AE_DEBITO_INS
    (
        V_ID_CONTRATO               INT,
        V_DATA_DEBITO               DATE,
        V_VALOR_DEBITO              NUMBER
    ) IS
    BEGIN
        INSERT INTO T_AE_DEBITO
        (
            ID_DEBITO        ,
            ID_CONTRATO     , 
            DATA_DEBITO     ,
            VALOR_DEBITO       
        )
        VALUES
        (
            SRA.SEQ_AE_ID_DEBITO.NEXTVAL ,
            V_ID_CONTRATO        ,
            V_DATA_DEBITO          ,
            V_VALOR_DEBITO
        );
    END;

-- 14U ###################################################################    
    PROCEDURE SP_AE_DEBITO_UPD
    (
        V_ID_DEBITO                    INT,
        V_ID_CONTRATO               INT,
        V_DATA_DEBITO               DATE,
        V_VALOR_DEBITO              NUMBER
    ) IS
    BEGIN
        UPDATE T_AE_DEBITO
        SET ID_CONTRATO   = V_ID_CONTRATO  , 
            DATA_DEBITO   = V_DATA_DEBITO  ,
            VALOR_DEBITO  = V_VALOR_DEBITO    
        WHERE ID_DEBITO = V_ID_DEBITO;
    END;
    
-- 14D ###################################################################       
    PROCEDURE SP_AE_DEBITO_DEL 
    (
        V_ID_DEBITO   INT
    ) IS
    BEGIN
        DELETE T_AE_DEBITO
        WHERE ID_DEBITO = V_ID_DEBITO;
    END;        

-- 15S ###################################################################       
    PROCEDURE SP_AE_LOG_GET 
    (
        V_ID_CONTRATO   INT,
        CUR             OUT SYS_REFCURSOR
    ) IS
    BEGIN
        OPEN CUR FOR 
            SELECT  *
            FROM SRA.T_AE_LOG
            WHERE ID_CONTRATO = V_ID_CONTRATO;
        COMMIT;
    END;
          
-- 15I ###################################################################    
    PROCEDURE SP_AE_LOG_INS
    (
        V_ID_CONTRATO            INT,
        V_LOG_DATA               DATE,
        V_LOG_USER               VARCHAR2,
        V_MESSAGE                VARCHAR2
    ) IS
    BEGIN
        INSERT INTO T_AE_LOG
        (
            ID_LOG            ,
            ID_CONTRATO     , 
            LOG_DATA        ,
            LOG_USER        ,
            MESSAGE
        )
        VALUES
        (
            SRA.SEQ_AE_ID_LOG.NEXTVAL ,
            V_ID_CONTRATO        ,
            V_LOG_DATA           ,
            V_LOG_USER             ,
            V_MESSAGE            
        );
    END;
END PK_AE;
/
COMMIT;


--############################################################################################################
--############################################################################################################
--############################################################################################################
        
-- INSERIR VALORES BASE DAS TABELAS DE LOCKUP
-- ################################################################### 

-- EMISSORES
-- ################################################################### 
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(10,'Jos� Correia');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(11,'Joaquim Maia');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(12,'Andreia Maia');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(13,'Jorge Machado');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(14,'Sandra Santos');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(15,'Jo�o Botelho');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(16,'Edgar Paiva');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(20,'Jorge Dias Neves');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(21,'Paiva Monteiro');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(22,'Jorge Pinheiro');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(23,'Jos� Augusto');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(24,'Eduardo Duque');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(25,'Gon�alo Sampaio');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(30,'Paulo Marques');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(31,'Rui Oliveira');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(32,'Jorge Rebelo Neves');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(33,'Francisco Silva');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(34,'Antonio Prezado');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(38,'Pedro Tavares');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(39,'Miguel Nereu');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(40,'Jo�o Barradas');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(41,'Carlos Ferreira');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(42,'Nuno Figueiredo');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(50,'M�rio Pinto');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(51,'F�bio Freire');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(52,'Daniel Barroso');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(53,'Antonio Jorge');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(54,'Rui Valeroso');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(55,'Pedro Verdades');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(56,'Lu�s Valerio');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(60,'Paulo Jorge Paulino');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(61,'Joaquim Barbosa');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(62,'Francisco Garcia');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(70,'Ivo Noe');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(71,'Ivo Noe (Madeira)');
INSERT INTO SRA.LK_EMISSOR(ID_EMISSOR, NOME_EMISSOR) VALUES(72,'Ivo Noe (A�ores'); 

-- CUSTOMER SERVICES
-- ################################################################### 
INSERT INTO SRA.LK_CUSTOMER_SERVICE(ID_CUSTOMER_SERVICE, NOME_CUSTOMER_SERVICE) VALUES(1,'Paula Martins');
INSERT INTO SRA.LK_CUSTOMER_SERVICE(ID_CUSTOMER_SERVICE, NOME_CUSTOMER_SERVICE) VALUES(2,'Dina Oliveira');
INSERT INTO SRA.LK_CUSTOMER_SERVICE(ID_CUSTOMER_SERVICE, NOME_CUSTOMER_SERVICE) VALUES(3,'Joao Teixeira');
INSERT INTO SRA.LK_CUSTOMER_SERVICE(ID_CUSTOMER_SERVICE, NOME_CUSTOMER_SERVICE) VALUES(13,'Pedro Lopes');
 
-- TIPO ACTIVIDADE
-- ################################################################### 
INSERT INTO SRA.LK_TIPO_ACTIVIDADE (ID_TIPO_ACTIVIDADE, NOME_TIPO_ACTIVIDADE, HAS_ACORDO,HAS_VALOR) VALUES (1,'Promo��o P.V.',0,0);
INSERT INTO SRA.LK_TIPO_ACTIVIDADE (ID_TIPO_ACTIVIDADE, NOME_TIPO_ACTIVIDADE, HAS_ACORDO,HAS_VALOR) VALUES (2,'Anivers�rio',1,1);
INSERT INTO SRA.LK_TIPO_ACTIVIDADE (ID_TIPO_ACTIVIDADE, NOME_TIPO_ACTIVIDADE, HAS_ACORDO,HAS_VALOR) VALUES (3,'Folheto',1,1);
INSERT INTO SRA.LK_TIPO_ACTIVIDADE (ID_TIPO_ACTIVIDADE, NOME_TIPO_ACTIVIDADE, HAS_ACORDO,HAS_VALOR) VALUES (4,'Referencia��o',1,1);
INSERT INTO SRA.LK_TIPO_ACTIVIDADE (ID_TIPO_ACTIVIDADE, NOME_TIPO_ACTIVIDADE, HAS_ACORDO,HAS_VALOR) VALUES (5,'Topos',1,1);
INSERT INTO SRA.LK_TIPO_ACTIVIDADE (ID_TIPO_ACTIVIDADE, NOME_TIPO_ACTIVIDADE, HAS_ACORDO,HAS_VALOR) VALUES (6,'Oferta Produto',1,1);

COMMIT;
